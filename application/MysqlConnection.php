<?php

require_once 'PHPMailer-master/PHPMailerAutoload.php';

class MysqlConnection {

    public static $DELETE = "<BR/><div class='alert alert-error' id='delete'><strong>Do you want to delete this record ??</strong></div>";
    public static $SERVERPATH = "C:\\xampp\\htdocs\\application\\download\\";
   // public static $SERVERPATH = "E:\\xampp\\htdocs\\roundwrap\\trunk\\application\\download\\";
    public static $REQUIRED = "<b style='color: red'>*</b>";
    ///SUCCESS - MESSAGE 

    public static $MSG_SU = "<div class='alert alert-success' style='border: solid 1px #CDCDCD;height:15px;' id='myElem'><b>SUCCESS!</b> Email has been sent Successfully!!!</div>";
    public static $MSG_DE = "<div class='alert alert-success' style='border: solid 1px #CDCDCD;height:15px;' id='myElem'><b>SUCCESS!</b> Record has been Deleted Successfully !!!</div>";
    public static $MSG_UP = "<div class='alert alert-success' style='border: solid 1px #CDCDCD;height:15px;' id='myElem'><b>SUCCESS!</b> Record has been Updated Successfully !!!</div>";
    public static $MSG_AD = "<div class='alert alert-success' style='border: solid 1px #CDCDCD;height:15px;' id='myElem'><b>SUCCESS!</b> Record has been Added Successfully !!!</div>";
    public static $MSG_CL = "<div class='alert alert-error' style='border: solid 1px #CDCDCD;height:15px;' id='myElem'><b>Alert!</b> Selected order is already closed!!</div>";
    public static $MSG_RV = "<div class='alert alert-success' style='border: solid 1px #CDCDCD;height:15px;' id='myElem'><b>SUCCESS!</b>Order Received Successfully !!!</div>";

    ///
    static function connect() {
        $DB_NAME = "rw";
        $DB_HOST = "localhost";
        $DB_USER = "root";
        $DB_PASS = "";
        return mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
    }

    static function connectToDiffrent($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME) {
        return mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
    }

    static function executeQuery($query) {
        $connect = MysqlConnection::connect();
        return mysqli_query($connect, $query);
    }

    static function getPrimary($tbl = "") {
        $mysqlprimary = MysqlConnection::fetchCustom("show index from $tbl where Key_name = 'PRIMARY'");
        return $mysqlprimary[0]["Column_name"];
    }

    static function getNextValue($type = "") {
        if ($type == "PO") {
            $sql = "SELECT `purchaseOrderId` as next FROM `purchase_order` ORDER BY `idindex` DESC LIMIT 0,1";
        } else {
            $sql = "SELECT `sono`  as next FROM `sales_order` ORDER BY `idindex` DESC LIMIT 0,1";
        }
        $resultset = MysqlConnection::fetchCustom($sql);
        $result = $resultset[0];
        if ($result["next"] == "") {
            return "1000";
        } else {
            return $result["next"] + 1;
        }
    }

    static function insert($tbl = "", $data = array()) {
        $connect = MysqlConnection::connect();
        $promarykeycolumn = MysqlConnection::getPrimary($tbl);
        $primarykey = md5(time() . "" . rand(0000000, 9999999));
        $data[$promarykeycolumn] = $primarykey;
        try {
            $keysset = "";
            $valuesset = "";
            foreach ($data as $key => $values) {
                $keysset .= "`" . $key . "`,";
                $valuesset .= "'" . mysqli_real_escape_string($connect, trim($values)) . "',";
            }
            $query = " INSERT INTO $tbl (" . substr($keysset, 0, strlen($keysset) - 1) . ") VALUES (" . substr($valuesset, 0, strlen($valuesset) - 1) . ");";
            MysqlConnection::executeQuery($query);
            return $primarykey;
        } catch (Exception $exc) {
            echo "<span style='color:red'>SQL QUERY ERROR !!! INSERT !!!" . $exc->getMessage() . "<span>";
        }
    }

    static function edit($tbl = "", $data = array(), $where = "") {
        $connect = MysqlConnection::connect();
        try {
            $update = "";
            foreach ($data as $key => $values) {
                $update .= "`" . $key . "` = " . "'" . mysqli_real_escape_string($connect, trim($values)) . "',";
            }
            $query = " UPDATE $tbl SET " . substr($update, 0, strlen($update) - 1) . " WHERE $where ";
            return MysqlConnection::executeQuery($query);
        } catch (Exception $exc) {
            echo "<span style='color:red'>SQL QUERY ERROR !!! EDIT !!!" . $exc->getMessage() . "<span>";
        }
    }

    static function delete($query) {
        try {
            MysqlConnection::executeQuery($query);
            return "";
        } catch (Exception $exc) {
            return $exc->getMessage();
        }
    }

    static function toArrays($resource) {
        $arrayList = array();
        while ($rows = mysqli_fetch_assoc($resource)) {
            array_push($arrayList, $rows);
        }
        return $arrayList;
    }

    static function fetchCustom($query) {
        $resource = MysqlConnection::executeQuery($query);
        return MysqlConnection::toArrays($resource);
    }

    static function exchangeArray($POST, $unset_array = array()) {
        foreach ($unset_array as $keys) {
            unset($POST[$keys]);
        }
        return $POST;
    }

    static function uploadFile($objfile, $destination) {
        $modifiedName = str_replace(" ", "_", $objfile["name"]);
        $fname = $destination . time() . "_" . $modifiedName;
        move_uploaded_file($objfile["tmp_name"], $fname);
        return empty($objfile["name"]) ? "" : $fname;
    }

    static function readFile($file_name) {
        $handle = fopen($file_name, 'rb') or die("error opening file");
        $contains = fread($handle, filesize($file_name));
        fclose($handle) or die("error closing file handle");
        return $contains;
    }

    /// MASTER DATA LIST 

    static function generateNumber($numberfor) {
        $resultset = MysqlConnection::fetchCustom("SELECT $numberfor FROM `order_number` WHERE identity = 'Y' ");
        $nextnumber = $resultset[0][$numberfor] + 1;
//        MysqlConnection::delete("UPDATE `order_number` SET $numberfor = $nextnumber WHERE identity = 'Y' ");
        return $nextnumber;
    }

    static function getAddedBy($userid) {
        $resultset = MysqlConnection::fetchCustom("SELECT `firstName`,`lastName` FROM `user_master` WHERE `user_id` = '$userid'");
        return $resultset[0]["firstName"] . " " . $resultset[0]["lastName"];
    }

    static function getItemDataById($itemid) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM item_master WHERE item_id = '$itemid'");
        return $resultset[0];
    }

    static function getGenericById($genericid = "") {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` where id = '$genericid'");
        return $resultset[0];
    }

    static function getTaxInfoById($taxid = "") {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table WHERE id = '$taxid'");
        return $resultset[0];
    }

    static function getSupplierDetails($suppId) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `supplier_master` WHERE supp_id = '$suppId' ");
        return $resultset[0];
    }

    static function getSupplierContactsDetails($suppId) {
        return MysqlConnection::fetchCustom("SELECT * FROM `supplier_contact` WHERE supp_id = '$suppId' ");
    }

    static function getCustomerDetails($customerId) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `customer_master` WHERE id = '$customerId' ");
        return $resultset[0];
    }

    static function getCustomerEmailContacts($customerId) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `customer_master` WHERE id = '$customerId' ");
        return $resultset[0];
    }

    static function getCustomerContactDetails($customerid) {
        return MysqlConnection::fetchCustom("SELECT * FROM  `customer_contact` WHERE cust_id = '$customerid'");
    }

    static function getPackSlipFromId($packslipid) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM packslip WHERE ps_id = '$packslipid' ORDER BY indexid DESC ");
        return $resultset[0];
    }

    static function getPackSlipDetailsFromId($packslipid) {
        return MysqlConnection::fetchCustom("SELECT * FROM `packslipdetail` WHERE `ps_id` = '$packslipid' ORDER BY type ");
    }

    static function getEmailConfigDetail() {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `email_setup` WHERE active = 'Y' AND isTest = 'Y' ");
        return $resultset[0];
    }

    static function getPortfolioProfileByProfId($profId) {
        $sql = "SELECT * from tbl_mapping_profile_label WHERE ppprimary = '$profId' ORDER BY  profilecounter , profilelabel ";
        return MysqlConnection::fetchCustom($sql);
    }

    static function getlabelsandpriceByProfId($profId) {
        $sql = "SELECT * from tbl_mapping_profile_label WHERE ppprimary = '$profId' && profilecounter != '0' ORDER BY  profilecounter , profilelabel ";
        return MysqlConnection::fetchCustom($sql);
    }

    static function getPortfolioProfileById($profId) {
        $resultset = MysqlConnection::fetchCustom("select * from tbl_portfolioprofile where id = '$profId' ");
        return $resultset[0];
    }

    static function getPortfolios() {
        return MysqlConnection::fetchCustom("SELECT * FROM tbl_portfolioprofile ORDER BY indexid DESC");
    }

    static function getProfilePrice() {
        return MysqlConnection::fetchCustom("SELECT * FROM profile_price ");
    }

    static function getProfileLabelValuePrice($profilelabel, $profilelabelname) {
        return MysqlConnection::fetchCustom("SELECT `profile_label_name`,`profile_label_price` "
                        . "FROM `profile_price` WHERE `portfolio_label` = '$profilelabelname' && portfolio_name = '$profilelabel'");
    }

    static function getProfilePriceById($priceid) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `profile_price` WHERE `id` = '$priceid' ");
        return $resultset[0];
    }

    static function getPurchaseOrderData($purchaseorderid) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `purchase_order` WHERE `id` = '$purchaseorderid' ");
        return $resultset[0];
    }

    static function getPurchaseItemData($purchaseorderid) {
        return MysqlConnection::fetchCustom("SELECT * FROM `purchase_item` WHERE `po_id` = '$purchaseorderid' ");
    }

    static function getPortfolioLabelsByProfId($profId) {
        return MysqlConnection::fetchCustom("SELECT * FROM `tbl_profilelabel` where portfolio_id = '$profId' ");
    }

    static function getSalesOrderDetailsById($salesorderid) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `sales_order` WHERE `id` = '$salesorderid'");
        return $resultset[0];
    }

    static function getSalesOrderDetailsByInvoice($invoiceno) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `sales_order` WHERE `invoiceno` = '$invoiceno'");
        return $resultset[0];
    }

    static function getCreditNoteByInvoice($invoiceno) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `tbl_restockinvoice_history` WHERE `reinvoice` = '$invoiceno'");
        return $resultset[0];
    }

    static function getSalesItemsDetailsById($salesorderid) {
        return MysqlConnection::fetchCustom("SELECT * FROM `sales_item` WHERE so_id = '$salesorderid' ");
    }

    static function getReceivingOrderByPoId($poid) {
        return MysqlConnection::fetchCustom("SELECT * FROM `tbl_receiving_items` WHERE `po_id` LIKE '$poid'");
    }

    static function sendEmail($targetemail, $attachment, $subject, $bodymatter, $title) {
        $emaildetails = MysqlConnection::getEmailConfigDetail();
        if (count($emaildetails) != 0) {
            $host = $emaildetails[""] == "" ? "mjbstaffhub.com" : $emaildetails["hostname"];
            $port = $emaildetails["portnumber"] == "" ? "465" : $emaildetails["portnumber"];
            $username = $emaildetails["username"] == "" ? "testroundwrap@mjbstaffhub.com" : $emaildetails["username"];
            $password = $emaildetails["pswd"] == "" ? "testroundwrap" : $emaildetails["pswd"];
            $setfrom = $emaildetails["sendfrom"] == "" ? "testroundwrap@mjbstaffhub.com" : $emaildetails["sendfrom"];
            date_default_timezone_set('Etc/UTC');
            $mail = new PHPMailer;
            $mail->isSMTP();
            $mail->SMTPDebug = 2;
            $mail->Debugoutput = 'html';
            $mail->Host = $host;
            $mail->addAttachment($attachment);
            $mail->Port = $port;
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth = true;
            $mail->Username = $username;
            $mail->Password = $password;
            $mail->setFrom($setfrom, $title);
            $mail->addAddress($targetemail, $subject);
            $mail->Subject = $subject;
            $mail->Title = $title;
            $mail->msgHTML($bodymatter);
            if (!$mail->send()) {
                //echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                //echo "Message sent!";
            }
        }
    }

    static function generateBgColor($index) {
        return $index % 2 == 0 ? "white" : "#eaeaea";
    }

    static function validateItems() {
        $result = MysqlConnection::fetchCustom("SELECT count(item_id) as item_id FROM `item_master`");
        if ($result[0]["item_id"] == "0" || count($result[0]) == 0) {
            return TRUE;
        }
        return FALSE;
    }

    ///DATE UNITITLY 
    /*
     * use this function to convert ui date into database date
     */
    static function convertToDBDate($date) {
        return date('Y-m-d', strtotime($date));
    }

    static function convertToDBTypeDate($date) {
        $explod = explode("-", $date);
        return $explod[2] . "-" . $explod[1] . "-" . $explod[0];
    }

    /*
     * use this function to convert database date into ui preference date
     */

    static function convertToPreferenceDate($date) {
        if ($date == "") {
            return "";
        }
        //$resultset = MysqlConnection::fetchCustom("SELECT name FROM `generic_entry` WHERE `type` = 'dateformat' LIMIT 0,1");
        //$format = "Y-m-d";
        //if (count($resultset[0]) != 0) {
        //$format = $resultset[0]["name"];
        //}
        return date("d-m-Y", strtotime($date));
    }

//    static function convertToDBTime($time) {
//        date_default_timezone_set('America/Vancouver');
//        $time = time("d h:i:s a");
////echo  
//        $explode = explode(" ", $date);
//        echo $explode[0];
//        echo "<br/>";
//        echo $explode[1] . " " . $explode[2];
//    }

    static function updateItem($itemid, $count, $status) {
        $query = "SELECT `onhand`, totalvalue FROM `item_master` WHERE `item_id` = '$itemid'";
        $resultset = MysqlConnection::fetchCustom($query);
        $item = $resultset[0];
        if ($status == 'Y') { // add item
            $updatedQty = $item["onhand"] + $count;
            $updatedSalesQty = $item["totalvalue"] + $count;
        } else { // remove item
            $updatedQty = $item["onhand"] - $count;
            $updatedSalesQty = $item["totalvalue"] - $count;
        }
        MysqlConnection::executeQuery("UPDATE item_master SET onhand = $updatedQty, totalvalue = $updatedSalesQty  WHERE item_id = '$itemid' ");
    }

    // when we store this to database 
    static function formatToBRAddress($address) {
        $exp = explode("\n", $address);
        $buildaddress = "";
        foreach ($exp as $value) {
            $buildaddress = $buildaddress . "" . trim($value) . "<br/>";
        }
        return preg_replace('#(<br */?>\s*)+#i', '<br />', $buildaddress);
    }

    // when we retrive this from database  
    static function formatToSlashNAddress($address) {
        $exp = explode("<br />", $address);
        $buildaddress = "";
        foreach ($exp as $value) {
            $buildaddress = $buildaddress . "" . trim($value) . "\n";
        }
        return preg_replace("/[\r\n]+/", "\n", $buildaddress);
    }

    static function updateOnSaleItem($itemid, $count, $status) {
        $query = "SELECT `totalvalue` FROM `item_master` WHERE `item_id` = '$itemid'";
        $resultset = MysqlConnection::fetchCustom($query);
        $item = $resultset[0];
        if ($status == 'Y') { // add item
            $updatedQty = $item["totalvalue"] + $count;
        } else { // remove item
            $updatedQty = $item["totalvalue"] - $count;
        }
        MysqlConnection::executeQuery("UPDATE item_master SET totalvalue = $updatedQty WHERE item_id = '$itemid' ");
    }

    static function getWorkOrderLastStatus($workorderid) {
        $paclslipdetails = MysqlConnection::getPackSlipFromId($workorderid);
        $profileid = $paclslipdetails["prof_id"];
        $portfoliolabels = MysqlConnection::getPortfolioProfileById($profileid);
        $portfolioname = $portfoliolabels["portfolio_name"];
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `tbl_profile_scanner_integration` WHERE profileid = '$portfolioname' ORDER BY sequence ASC ");
        return $resultset[0];
    }

}
