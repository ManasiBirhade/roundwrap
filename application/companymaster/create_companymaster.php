<?php
$cmpid = filter_input(INPUT_GET, "cmpid");

if (!empty($cmpid)) {
    $resultset = MysqlConnection::fetchCustom("SELECT * FROM company_master WHERE cmp_id =  '$cmpid'");
    $cmp = $resultset[0];
}


$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
if (isset($btnSubmitFullForm)) {
    unset($_POST["btnSubmitFullForm"]);
    if (isset($cmpid)) {
        MysqlConnection::edit("company_master", $_POST, " cmp_id = '$cmpid' ");
    } else {
        MysqlConnection::insert("company_master", $_POST);
    }
    header("location:index.php?pagename=manage_companymaster");
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">MANAGE COMPANY INFO</h5>
    </div>
    <div class="widget-box" >
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Company Information </a></li>
            </ul>
        </div>
        <form name="frmCmpSubmit"  enctype="multipart/form-data" id="frmCmpSubmit" method="post">
            <div class="widget-content tab-content">
                
                    <table class="display nowrap sortable"  style="width: 80%;vertical-align: top" border="0">
                        <tr>
                            <td><label class="control-label">Company Name</label></td>
                            <td><input style="width: 220px" type="text" name="cmp_name" required="" autofocus="" value="<?php echo $cmp["cmp_name"] ?>" id="cmpName" minlenght="2" maxlength="30"></td>
                            <td><label class="control-label">Email</label></td>
                            <td><input style="width: 220px" type="email" name="cmp_email" id="cmp_email"  value="<?php echo $cmp["cmp_email"] ?>" ></td>
                            <td><label class="control-label">Contact No</label></td>
                            <td><input style="width: 220px" type="text" name="phno" id="phno"  value="<?php echo trim($cmp["phno"]) ?>" ></td>
                        </tr> 
                        <tr>
                            <td><label class="control-label">Fax No</label></td>
                            <td><input style="width: 220px" type="text" name="cmp_fax" id="cmp_fax"  value="<?php echo trim($cmp["cmp_fax"]) ?>" ></td>
                            <td><label class="control-label">Website</label></td>
                            <td><input style="width: 220px" type="url" name="cmp_website" id="cmp_website" minlenght="2" maxlength="30"  value="<?php echo $cmp["cmp_website"] ?>" ></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Street No</label></td>
                            <td><input style="width: 220px" type="text" name="streetNo" id="streetNo"  minlenght="2" maxlength="30" value="<?php echo $cmp["streetNo"] ?>" ></td>
                            <td><label class="control-label">Street Name</label></td>
                            <td><input style="width: 220px" type="text" name="streetName" id="streetName" minlenght="2" maxlength="30" plceholder="Enter Street Name" value="<?php echo $cmp["streetName"] ?>" ></td>
                            <td><label class="control-label">Postal Code</label></td>
                            <td><input style="width: 220px" type="text" name="postal_code" id="postal_code" minlenght="2" maxlength="30"  value="<?php echo $cmp["postal_code"] ?>" ></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">City</label></td>
                            <td><input style="width: 220px" type="text" name="city" id="city"  minlenght="2" maxlength="30" value="<?php echo $cmp["city"] ?>" ></td>
                            <td><label class="control-label">Province</label></td>
                            <td><input style="width: 220px" type="text" name="province" id="province"  minlenght="2" maxlength="30" value="<?php echo $cmp["province"] ?>" ></td>
                            <td><label class="control-label">Country</label></td>
                            <td><input style="width: 220px" type="text" name="country" id="country"  minlenght="2" maxlength="30" value="<?php echo $cmp["country"] ?>" ></td>
                        </tr> 
                    </table>
               
                <hr/>
                <input type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info" value="SUBMIT">
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div> 
<script>
    $(document).ready(function($) {
        $("#phno").mask("(999) 999-9999");
        $("#cmp_fax").mask("(999) 999-9999");

    });
</script>