<?php
$listofcompany = MysqlConnection::fetchCustom("SELECT * FROM company_master");
$cmpid = filter_input(INPUT_GET, "cmpid");
?>
<style>
    .customtable{
        width: 100%;
        height: auto;
        min-height: 50%;
        font-family: verdana;
        border: solid 1px gray;
        border-color: gray;
    }
    .customtable tr{
        height: 25px;
        border-color: gray;
    }
    .customtable tr td{
        /*padding: 5px;*/
        border-color: gray;
    }
    .thead{
        height: 35px;
    }
    .brdright{
        border-right: solid 1px rgb(220,220,220);
    }   

</style>
<link href="css/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.min_1.11.3.js"></script>
<script src="js/jquery.contextMenu.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<link href="css/tablelayout.css" rel="stylesheet" type="text/css">
<script>
    $("#liveTableSearch").on("keyup", function() {
        var value = $(this).val();
        $("table tr").each(function(index) {
            if (index !== 0) {
                $row = $(this);
                var id = $row.find("td:first").text();
                if (id.indexOf(value) !== 0) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            }
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">COMPANY LIST</h5>
    </div>
    <br/>
    <table>
        <tr >
            <td ><a class="btn btn-info"  href="index.php?pagename=create_companymaster" ><i class="icon icon-list"></i>&nbsp;&nbsp;ADD COMPANY INFO</a></td>
            <td>&nbsp;|&nbsp;</td>
            <td style="width: 80%;vertical-align: bottom">
                <b>&nbsp;SEARCH&nbsp;:&nbsp;</b>
                <input type="text" id="searchinput" onkeyup="searchData()" 
                       placeholder="Search for Companyname" 
                       name="searchinput" style="width: 80%;height: 25px;margin-top: 3px;"/>
            </td>
        </tr>
    </table>
    <br/>

    <!--    <div id="tableContainer" class="tableContainer">-->
    <section class="">
        <div class="scroll">
            <table id="data">
                <thead>
                    <tr class="header">
                        <th  style="text-align: center;width: 10px;"><div>#</div></th>
                <th ><div>Company Name</div></th>
                <th><div>Address</div></th>
                <th ><div>Website</div></th>
                <th ><div>Contact No</div></th>

                </tr>
                </thead>
                <tbody style="border-color: #76323F">
                    <?php
                    $index = 1;
                    foreach ($listofcompany as $key => $value) {
                        if ($index % 2 == 0) {
                            $bg = $even;
                        } else {
                            $bg = $odd;
                        }
                        $buildaddress = getAddress($value);
                        ?>
                        <tr id="<?php echo $value["cmp_id"] ?>" style="<?php echo $bg ?>;border-bottom: solid 1px rgb(220,220,220);text-align: left;height: 35px;"  class="context-menu-one">
                            <td style="text-align: center">&nbsp;<?php echo $index ?></td>
                            <td >&nbsp;&nbsp;<?php echo $value["cmp_name"] ?></td>
                            <td ><?php echo $value["streetNo"] ?>,&nbsp;<?php echo $value["streetName"] ?>,&nbsp;<?php echo $value["city"] ?>,&nbsp;<?php echo $value["province"] ?>,&nbsp;<?php echo $value["country"] ?></td>
                            <td ><?php echo $value["cmp_website"] ?>&nbsp;&nbsp;</td>
                            <td ><?php echo $value["phno"] ?></td>

                        </tr>
                        <?php
                        $index++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </section>

</div>
<script type="text/javascript">
    $(function() {
        $('#data').tablesorter();
    });
</script>
<!--
    <div >
        <table>
            <td ><a href="index.php?pagename=manage_usermanagement&status=active" id="btnSubmitFullForm" class="btn btn-info">VIEW ACTIVATE</a></td>
            <td></td>
            <td ><a href="index.php?pagename=manage_usermanagement&status=inactive" id="btnSubmitFullForm" class="btn btn-info">VIEW INACTIVE</a></td>
            <td></td>
            <td ><a href="index.php?pagename=manage_usermanagement&status=all" id="btnSubmitFullForm" class="btn btn-info">VIEW ALL</a></td>
            <td></td>
        </table>
    </div>-->


<?php

function getAddress($value) {
    return $value["streetNo"];
}
?>
<script>
    $("#deleteThis").click(function() {
        var dataString = "deleteId=" + $('#deleteId').val();
        $.ajax({
            type: 'POST',
            url: 'usermanagement/usermanagement_ajax.php',
            data: dataString
        }).done(function(data) {
//            location.reload();
        }).fail(function() {
        });
        location.reload();
    });

    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }

</script>
<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_company":
                        window.location = "index.php?pagename=view_companymaster&cmpid=" + id;
                        break;
                    case "create_company":
                        window.location = "index.php?pagename=create_companymaster";
                        break;
                    case "edit_company":
                        window.location = "index.php?pagename=create_companymaster&cmpid=" + id;
                        break;
                    case "delete_company":
                        window.location = "index.php?pagename=view_companymaster&cmpid=" + id + "&flag=yes";
                        break;

                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_companymaster";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_company": {name: "VIEW COMPANY INFO", icon: ""},
                "edit_company": {name: "EDIT COMPANY INFO", icon: ""},
                "delete_company": {name: "DELETE COMPANY INFO", icon: ""},
                "sep0": "---------",
                "quit": {name: "QUIT", icon: function() {
                        return '';
                    }}
            }
        });
    });
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_companymaster&cmpid=" + id;
        }
    });
</script>