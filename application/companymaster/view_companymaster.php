<?php
$cmpid = filter_input(INPUT_GET, "cmpid");
$flag = filter_input(INPUT_GET, "flag");
if (!empty($cmpid)) {
    $resultset = MysqlConnection::fetchCustom("SELECT * FROM company_master WHERE cmp_id =  '$cmpid'");
    $cmp = $resultset[0];
}

if (isset($_POST["deleteItem"])) {
    MysqlConnection::delete("DELETE FROM `company_master` WHERE cmp_id = '$cmpid'");
    header("location:index.php?pagename=manage_companymaster");
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">VIEW COMPANY INFO</h5>
    </div>
    <div class="widget-box" >
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Company Information </a></li>
            </ul>
        </div>

        <input type="hidden" name="cmpId" id="cmpId" value="<?php echo $cmp["cmp_id"] ?>" />
        <div class="widget-content tab-content">
            <fieldset class="well the-fieldset">
                <table  style="width: 80%;vertical-align: top" border="0">
                    <tr>
                        <td><label class="control-label">Company Name</label></td>
                        <td><input type="text"  readonly="" value="<?php echo $cmp["cmp_name"] ?>" ></td>
                        <td><label class="control-label">Email</label></td>
                        <td><input type="email" readonly="" value="<?php echo $cmp["cmp_email"] ?>" ></td>
                        <td><label class="control-label">Contact No </label></td>
                        <td><input type="text" readonly="" value="<?php echo trim($cmp["phno"]) ?>" ></td>
                    </tr> 
                    <tr>
                        <td><label class="control-label">Fax No</label></td>
                        <td><input type="text" readonly=""  value="<?php echo trim($cmp["cmp_fax"]) ?>" ></td>
                        <td><label class="control-label">Website</label></td>
                        <td><input type="text" readonly="" value="<?php echo $cmp["cmp_website"] ?>" ></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Street No</label></td>
                        <td><input type="text" readonly="" value="<?php echo $cmp["streetNo"] ?>" ></td>
                        <td><label class="control-label">Street Name</label></td>
                        <td><input type="text" readonly="" value="<?php echo $cmp["streetName"] ?>" ></td>
                        <td><label class="control-label">Postal Code</label></td>
                        <td><input type="text" readonly=""  value="<?php echo $cmp["postal_code"] ?>" ></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">City</label></td>
                        <td><input type="text" readonly=""  value="<?php echo $cmp["city"] ?>" ></td>
                        <td><label class="control-label">Province</label></td>
                        <td><input type="text"  readonly="" value="<?php echo $cmp["province"] ?>" ></td>
                        <td><label class="control-label">Country</label></td>
                        <td><input type="text" readonly="" value="<?php echo $cmp["country"] ?>" ></td>
                    </tr> 
                </table>

                <hr/>
                <?php
                if (isset($flag) && $flag != "") {
                    ?>
                    <form name="frmDeleteItem" id="frmDeleteItem" method="post">
                        <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info" style="background-color: #76323F"/>
                        <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                    </form>
                    <?php
                } else {
                    ?>
                    <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                    <?php
                }
                ?>
            </fieldset>
        </div>
    </div>
</div> 
