<?php
error_reporting(0);
session_start();
ob_start();

include './MysqlConnection.php';
include './ApplicationUtil.php';


$useremail = filter_input(INPUT_GET, "token");
$expdata = explode("@@", $useremail);
$email = base64_decode($expdata[0]);
$userid = $expdata[1];

if (isset($_POST["go"])) {
    $new = md5(filter_input(INPUT_POST, "newpassword"));
    $reset = md5(filter_input(INPUT_POST, "repassword"));
    if ($new == $reset) {
        MysqlConnection::delete("UPDATE user_master SET  password = '$new' WHERE  username='$email' AND user_id = '$userid' ");
        header("location:login.php");
    } else {
        $error = "Your password and confirm password did not match";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
        <title>LOGIN | ROUND WRAP INDUSTRIES LTD RICHMOND BC</title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css" />
    </head>
    <body>
        <section class="container" style="margin-top: 80px;">
            <section class="login-form">
                <form method="post" role="login">
                    <h2>Change Password</h2>
                    <p></p>
                    <p style="color: red"><?php echo $error ?></p>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="text-primary glyphicon glyphicon-lock"></span></div>
                            <input type="password" name="newpassword" placeholder="New Password" required class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="text-primary glyphicon glyphicon-lock"></span></div>
                            <input type="password" name="repassword" placeholder="Confirm Password" required class="form-control" />
                        </div>
                    </div>
                    <button type="submit" name="go" class="btn btn-block btn-success">Reset</button>
                </form>
            </section>
        </section>
    </body>
</html>