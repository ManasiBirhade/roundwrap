<?php
$portfolios = MysqlConnection::getPortfolios();
$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");

if (isset($_POST["submit"])) {
    $profiles = explode("*", $_POST["profileid"]);
    $profileid = urlencode($profiles[0]);
    $profilename = urlencode($profiles[1]);
    header("location:index.php?pagename="
            . "add_profileprice"
            . "&profileid=$profileid"
            . "&profilename=$profilename");
}
?>
<div class="container-fluid" >
    <div class="cutomheader"><h5 style="font-family: verdana;font-size: 12px;">PROFILE PRICE</h5></div>
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
        <form name="frmItemsSubmit" id="frmItemsSubmit"  method="post" action="">
            <div class="widget-content tab-content"  style="margin: 0 auto">
                <table class="display nowrap sortable" style="width: 40%;">
                    <tr>
                        <td style="width: 25%;"><label  class="control-label">SELECT CATEGORY&nbsp;:</label></td>
                        <td style="width: 60%;">
                            <select name="profileid" id="profileid" style="width: 100%" >
                                <option>NONE</option>
                                <?php
                                foreach ($portfolios as $value) {
                                    ?>
                                    <option value="<?php echo $value["id"] ?>*<?php echo $value["profile_name"] ?>">
                                        <?php echo strtoupper($value["portfolio_name"] . " - " . $value["profile_name"]) ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td><input type="submit" id="submit" name="submit"  class="btn btn-info" value="CREATE"></td>
                    </tr>
                </table>
            </div>  
        </form>
    </div>
</div>