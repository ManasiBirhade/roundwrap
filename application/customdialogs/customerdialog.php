<?php
$sqlcustomertypepredata = MysqlConnection::fetchCustom("SELECT id,name FROM generic_entry where type = 'customer_type' ORDER BY id DESC ;");
$sqlpaymenttermdata = MysqlConnection::fetchCustom("SELECT id,name,code FROM generic_entry where type = 'paymentterm' ORDER BY id DESC ;");
$sqlrepresentativetermdata = MysqlConnection::fetchCustom("SELECT id,name,code, description FROM generic_entry where type = 'representative' ORDER BY id DESC ;");
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY id DESC ;");
?>
<script>
    $(document).ready(function($) {
        $("#phno").mask("(999) 999-9999");
        $("#cust_fax").mask("(999) 999-9999");
        $("#mobileno").mask("(999) 999-9999");
        $("#alterno").mask("(999) 999-9999");

    });
</script>
<div class="container-fluid" id="tabs">
    <div class="widget-box" >
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">COMPANY INFORMATION </a></li>
                <li id="acTab2"><a data-toggle="tab" href="#tab2">ADDITIONAL CONTACTS</a></li>
                <li id="tdTab3"><a data-toggle="tab" href="#tab3">TAX AND DISCOUNT</a></li>
                <li id="dpiTab4"><a data-toggle="tab" href="#tab4">DEPOSITS AND PAYMENT INFORMATION</a></li>
            </ul>
        </div>
        <form name="frmCustomerSubmit"  enctype="multipart/form-data" id="frmCustomerSubmit" method="post"  >
            <div class="widget-content tab-content">
                <div id="tab1" class="tab-pane active">
                    <table class="display nowrap sortable"  style="width: 100%;vertical-align: top" border="0">
                        <tr>
                            <td><label class="control-label">First Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text" name="firstname" id="firstname" onkeyup="fillAddress()"  value="<?php echo $customer["firstname"] ?>" autofocus="" required="true" minlenght="2" maxlength="30" ></td>
                            <td><label class="control-label">Last Name</label></td>
                            <td><input style="width: 220px" type="text" name="lastname" id="lastname" onkeyup="fillAddress()"  value="<?php echo $customer["lastname"] ?>" minlenght="2" maxlength="30" ></td>
                            <td><label class="control-label">Company Name<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
                            <td><input style="width: 220px" type="text" name="cust_companyname" onfocusout="validateDuplicate()"  onkeyup="fillAddress()"  value="<?php echo $customer["cust_companyname"] ?>" id="cust_companyname" minlenght="2" maxlength="50" required="true"></td>
                        </tr>
                        <tr>

                            <td><label class="control-label">Email<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
                            <td><input style="width: 220px" required="" type="email" name="cust_email" id="cust_email" value="<?php echo $customer["cust_email"] ?>" ></td>

                            <td><label class="control-label">Mobile No</label></label></td>
                            <td>
                                <input type="text" style="width: 40px;"  name="mobcode" id="mobcode"  value="<?php echo trim($customer["mobcode"]) == "" ? "+1" : $customer["mobcode"] ?>">
                                <input style="width: 160px" type="text" name="mobileno"  id="mobileno"    value="<?php echo trim($customer["mobileno"]) ?>" >
                            </td>

                            <td><label class="control-label">Phone No</label></label></td>
                            <td>
                                <input type="text" style="width: 40px;"  name="phoneCode" id="phoneCode"  value="<?php echo trim($customer["phoneCode"]) == "" ? "+1" : $customer["phoneCode"] ?>">
                                <input style="width: 160px" type="text" name="phno"  id="phno"  value="<?php echo trim($customer["phno"]) ?>" >
                            </td>
                        </tr> 
                        <tr >
                            <td><label class="control-label">Fax</label></td>
                            <td>
                                <input type="text" style="width: 40px;"   name="faxCode" id="faxCode"  value="<?php echo trim($customer["faxCode"]) == "" ? "+1" : $customer["faxCode"] ?>">
                                <input style="width: 160px" type="text" name="cust_fax" id="cust_fax"  value="<?php echo $customer["cust_fax"] ?>" >
                            </td>
                            <td><label class="control-label">Web Site</label></td>
                            <td><input style="width: 220px" type="text" name="website" id="website" maxlength="30" plceholder="Enter Company Website" value="<?php echo $customer["website"] ?>" ></td>
                            <td><label class="control-label">Customer Status</label></td>
                            <td style="vertical-align: middle">
                                <input  type="checkbox" name="status" <?php echo $customer["status"] == "Y" ? "checked" : "" ?> id="status" value="Y" />
                                Is customer active ?
                            </td>
                        </tr>

                        <tr>
                            <td><label class="control-label">Address<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
                            <td><input style="width: 220px" required="" type="text" name="streetNo" id="streetNo" onkeyup="fillAddress()"  minlenght="2" maxlength="60" value="<?php echo $customer["streetNo"] ?> " ></td>
                            <td><label class="control-label">City<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
                            <td><input style="width: 220px" required="" type="text" name="city" id="city"  minlenght="2" onkeyup="fillAddress()"  maxlength="30" value="<?php echo $customer["city"] ?>" ></td>
                            <td><label class="control-label">Province<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
                            <td><input style="width: 220px" required="" type="text" name="cust_province" id="cust_province" onkeyup="fillAddress()"   minlenght="2" maxlength="30" value="<?php echo $customer["cust_province"] ?>" ></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Postal Code<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
                            <td><input style="width: 220px" type="text" required="" name="postal_code" id="postal_code" onkeyup="fillAddress()"  minlenght="2" maxlength="30"  value="<?php echo $customer["postal_code"] ?>" ></td>
                            <td><label class="control-label">Country<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
                            <td><input style="width: 220px" type="text" name="country" id="country"  minlenght="2" required="" onkeyup="fillAddress()"  maxlength="30" value="<?php echo $customer["country"] ?>" ></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr style="vertical-align: top">
                            <td><label class="control-label">Bill To</label></td>
                            <td><textarea style="height: 100px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize" name="billto" onfocus="fillAddress()"  id="billto" ><?php echo $customer["billto"] ?></textarea></td>
                            <td><label class="control-label">Ship To </label></td>
                            <td><textarea   style="height: 100px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize"  name="shipto" id="shipto"><?php echo $customer["shipto"] ?></textarea></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a onclick="copyOrRemove('1')" style="cursor: pointer"  title="Copy billing address to shipping address">COPY >></a></td>
                            <td></td>
                            <td><a onclick="copyOrRemove('0')" style="cursor: pointer" title="Remove shipping address"><< REMOVE</a></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                    <hr/>
                    <input type="hidden" value="<?php echo $customerid ?>" name="customerid"/>
                    <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                    <input type="button" id="btnCmpNext1" value="NEXT" class="btn btn-info" />
                </div>
                <div id="tab2" class="tab-pane ">

                    <table id="addcontacts"  border="0" class="display nowrap sortable table-bordered"  style="width: 100%"> 
                        <tr style="background-color: #A9CDEC;color: white">
                            <td>Name</td>
                            <td>Email</td>
                            <td>Phone</td>
                            <td>Designation</td>
                            <td>Mail</td>
                            <td></td>
                        </tr>
                        <?php
                        if (count($customercontactarray) != 0) {
                            $index = 1;
                            foreach ($customercontactarray as $key => $value) {
                                if ($value["mail"] == "Y") {
                                    $check = "checked=''";
                                } else {
                                    $check = "";
                                }
                                ?>
                                <tr style="vertical-align: bottom">
                                    <td><input type="text" name="contact_person[]" value="<?php echo $value["person_name"] ?>" minlenght="2" maxlength="30"  id="alter_contact"></td>
                                    <td><input type="email" name="email[]" autofocus=""  value="<?php echo $value["person_email"] ?>" id="email"></td>
                                    <td>
                                        <input type="text" style="width: 50px;" name="code[]" id="code"  value="<?php echo $value["code"] == "" ? "+1" : $value["code"] ?>" >
                                        <input type="text" name="alternos[]" id="alterno"  value="<?php echo $value["person_phoneNo"] ?>" >
                                    </td>
                                    <td><input type="text" name="designation[]" id="designation"  value="<?php echo $value["designation"] ?>" ></td>
                                    <td><input type="checkbox" name="mail[]" <?php echo $check ?> id="mail"  value="Y" /></td>
                                    <td>
                                        <?php
                                        if ($index == 1) {
                                            echo '<a  class="icon-plus" href="#" id="icon-plus" ></a>';
                                        } else {
                                            echo '<a class="icon-trash" href="#" id="icon-trash" ></a>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $index++;
                            }
                        } else {
                            ?>
                            <tr style="vertical-align: bottom">
                                <td><input style="width: 220px" type="text" name="contact_person[]" value="" minlenght="2" maxlength="30"  id="alter_contact"></td>
                                <td><input style="width: 220px" type="email" name="email[]" autofocus=""  value="" id="email"></td>
                                <td>
                                    <input type="text" style="width: 50px;" name="code[]" id="code"  value="<?php echo $value["code"] == "" ? "+1" : $value["code"] ?>" >
                                    <input style="width: 220px" type="tel" name="alternos[]" id="alterno"  value="" >
                                </td>
                                <td><input style="width: 220px" type="text" name="designation[]" id="designation"  value="" maxlength="45"></td>
                                <td><input type="checkbox" name="mail[]" id="mail"  value="Y" ></td>
                                <td><a class="icon-plus" href="#" id="icon-plus" ></a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table> 
                    <hr/>
                    <input type="button" id="btnCmpPrev1" value="PREVIOUS" class="btn btn-info" href="#tab1" >
                    <input type="button" id="btnCmpNext2" value="NEXT" class="btn btn-info" href="#tab2"  >
                </div>
                <div id="tab3" class="tab-pane">

                    <table  class="display nowrap sortable" style="width: 100%;vertical-align: top" border="0"> 
                        <tr>
                            <td><label class="control-label">Customer Type<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td>
                                <select style="width: 225px" name="cust_type" id="cust_type" required=""> 
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php foreach ($sqlcustomertypepredata as $key => $value) { ?>
                                        <option <?php echo $value["id"] == $customer["cust_type"] ? "selected" : "" ?>
                                            value="<?php echo $value["id"] ?>"  ><?php echo $value["name"] ?></option>
                                        <?php } ?>
                                </select>
                            </td>
                            <td><label class="control-label">Discount</label></td>
                            <td>
                                <input type="text" name="discount" id="discount"  style="width: 225px"  onkeypress="return chkNumericKey(event)" maxlength="2">
                            </td>
                            <td><label class="control-label">Term</label></td>
                            <td>
                                <select style="width: 225px" name="paymentTerm" id="paymentTerm" style="margin-top: 5px;">
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php foreach ($sqlpaymenttermdata as $key => $value) { ?>
                                        <option <?php echo $value["id"] == $customer["paymentterm"] ? "selected" : "" ?>
                                            value="<?php echo $value["id"] ?>"><?php echo $value["code"] ?> - <?php echo $value["name"] ?></option>
                                        <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Representative</label></td>
                            <td>
                                <select style="width: 225px" name="sales_person_name" id="sales_person_name">
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php
                                    foreach ($sqlrepresentativetermdata as $key => $value) {
                                        echo $value["id"];
                                        echo "<br/>";
                                        echo $customer["sales_person_name"];
                                        ?>
                                        <option <?php echo trim($value["id"]) == trim($customer["sales_person_name"]) ? "selected" : "" ?>
                                            value="<?php echo $value["id"] ?>">
                                                <?php echo $value["name"] ?> <?php echo $value["description"] ?>
                                        </option>  
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                            <td><label class="control-label"> Good Tax</label></td>
                            <td>
                                <select style="width: 225px" name="taxInformation" id="taxInformation">
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                                        <option   <?php echo $value["id"] == $customer["taxInformation"] ? "selected" : "" ?>
                                            value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                                        <?php } ?>
                                </select>
                            </td>
                            <td><label class="control-label"> Shipping Tax</label></td>
                            <td>
                                <select style="width: 225px" name="shippingtax" id="shippingtax">
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                                        <option   <?php echo $value["id"] == $customer["shippingtax"] ? "selected" : "" ?>
                                            value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                                        <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input  type="checkbox" id="taxcheckbox">&nbsp;Use customer tax code</td>
                            <td><label class="control-label">Business No</label></td>
                            <td><input style="width: 220px" type="text" name="businessno" minlenght="2" maxlength="20" id="businessno"  value="<?php echo $customer["businessno"] ?>" ></td>
                            <td><label class="control-label">Certificate</label></td>
                            <td>
                                <input style="width: 220px" type="file" name="certificate" id="certificate">
                                <?php
                                if ($customer["certificate"] != "") {
                                    ?>
                                    <br/>
                                    <a href="" target="_blank">view</a>
                                    <?php
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <input type="button" id="btnCmpPrev2" value="PREVIOUS" class="btn btn-info" href="#tab2">
                    <input type="button" id="btnCmpNext3" value="NEXT" class="btn btn-info" href="#tab4">
                </div>
                <div id="tab4" class="tab-pane">

                    <table class="display nowrap sortable" style="width: 100%;vertical-align: top" border="0" id="payment"> 
                        <tr>
                            <td><label class="control-label">Account No</label></td>
                            <td><input style="width: 180px" type="text" name="cust_accnt_no" onkeypress="return chkNumericKey(event)" autofocus="" value="<?php echo $customer["cust_accnt_no"] ?>" minlenght="2" maxlength="13" id="cust_accnt_no"></td>

                            <td><label class="control-label">Currency</label></td>
                            <td>
                                <select style="width: 190px" name="currency"  id="currency" value="" style="text-transform: capitalize">
                                    <?php
                                    $currencyarr = getcurrency();
                                    foreach ($currencyarr as $key => $cvalue) {
                                        ?>
                                        <option value="<?php echo $key ?>" <?php echo $customer["currency"] == $key ? "selected" : "" ?> ><?php echo $cvalue ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>

                            <td><label class="control-label">Balance</label></td>
                            <td><input style="width: 180px" type="text" name="balance" onkeypress="return chkNumericKey(event)" minlenght="2" maxlength="50" id="balance"  value="<?php echo $customer["balance"] ?>" ></td>
                            <td><label class="control-label">Credit Limit</label></td>
                            <td><input style="width: 180px" type="text" name="creditlimit" onkeypress="return chkNumericKey(event)" autofocus="" value="<?php echo $customer["creditlimit"] ?>" minlenght="2" maxlength="" id="creditlimit"></td>
                        </tr>
                        <?php
                        if (count($customerpaymentarray) != 0) {
                            $index = 1;
                            foreach ($customerpaymentarray as $key => $value) {
                                ?>
                                <tr>
                                    <td><label class="control-label">Credit Card No.</label></td>
                                    <td><input style="width: 180px;" type="text" name="cardnumber[]" onkeypress="return chkNumericKey(event)" id="cardnumber" minlenght="2" maxlength="13"  value="<?php echo $value["cardnumber"] ?>" ></td>
                                    <td><label class="control-label">Name on Card</label></td>
                                    <td><input style="width: 180px;" type="text" name="nameoncard[]" id="nameoncard"  minlenght="2" maxlength="30"  value="<?php echo $value["nameoncard"] ?>" ></td>
                                    <td><label class="control-label">Exp. Date</label></td>
                                    <td><input style="width: 180px" type="text" name="expdate[]" id="datepicker"  value="<?php echo MysqlConnection::convertToPreferenceDate($value["expdate"]) ?>" ></td>
                                    <td><label class="control-label">CVV</label></td>
                                    <td>
                                        <input style="width: 40px" type="text" onkeypress="return chkNumericKey(event)" name="cvvno[]" id="cvvno" minlength="2" maxlength="3" value="<?php echo $value["cvvno"] ?>" >
                                        <?php
                                        if ($index == 1) {
                                            echo '<a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-plus" href="#" id="paymentInfoFrm" ></a>';
                                        } else {
                                            echo '<a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-trash" href="#" id="icon-trash" ></a>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $index++;
                            }
                        } else {
                            ?>
                            <tr>
                                <td><label class="control-label">Credit Card No.</label></td>
                                <td><input style="width: 180px;" type="text" name="cardnumber[]" onkeypress="return chkNumericKey(event)" id="cardnumber" minlenght="2" maxlength="16"  value="<?php echo $value["cardnumber"] ?>" ></td>
                                <td><label class="control-label">Name on Card</label></td>
                                <td><input style="width: 180px;" type="text" name="nameoncard[]" id="nameoncard"  minlenght="2" maxlength="30"  value="<?php echo $value["nameoncard"] ?>" ></td>
                                <td><label class="control-label">Exp. Date</label></td>
                                <td><input style="width: 180px;" type="text" name="expdate[]" id="datepicker"  value="<?php echo $value["expdate"] ?>" ></td>
                                <td><label class="control-label">CVV</label></td>
                                <td>
                                    <input style="width: 40px" type="text" onkeypress="return chkNumericKey(event)" name="cvvno[]" id="cvvno" minlength="2" maxlength="3" value="<?php echo $value["cvvno"] ?>" >
                                    <a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-plus" href="#" id="paymentInfoFrm" ></a>
                                </td>
                            </tr>
                        <?php }
                        ?>
                    </table> 

                    <hr/>
                    <input type="hidden" value="customerid" value="<?php echo $customerid ?>"/>
                    <input type="hidden" name="isDialog" value="yes"/>

                    <input type="button" id="btnCmpPrev3" value="PREVIOUS" class="btn btn-info" href="#tab1"/>
                    <button type="submit" onclick="return submitCustomer()" id="btnSubmitFullForm" class="btn btn-info">SUBMIT</button>
                    <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                </div>
            </div>  
            <input type="hidden" value="<?php echo $customerid ?>" name="customerid">
        </form>
    </div>
</div>

<script>
//    btnCmpPrev1  btnCmpNext2
    $('#btnCmpNext1').on('click', function() {
        $('#ciTab1').removeClass('active');
        $('#acTab2').addClass('active');

        $('#tab1').removeClass('active');
        $('#tab2').addClass('active');
    });

    $('#btnCmpPrev1').on('click', function() {
        $('#acTab2').removeClass('active');
        $('#ciTab1').addClass('active');
        $('#tab2').removeClass('active');
        $('#tab1').addClass('active');

    });
    $('#btnCmpNext2').on('click', function() {
        $('#acTab2').removeClass('active');
        $('#tdTab3').addClass('active');
        $('#tab2').removeClass('active');
        $('#tab3').addClass('active');
    });


    $('#btnCmpPrev2').on('click', function() {
        $('#tdTab3').removeClass('active');
        $('#acTab2').addClass('active');
        $('#tab3').removeClass('active');
        $('#tab2').addClass('active');
    });
    $('#btnCmpNext3').on('click', function() {
        $('#tdTab3').removeClass('active');
        $('#dpiTab4').addClass('active');
        $('#tab3').removeClass('active');
        $('#tab4').addClass('active');
    });
    $('#btnCmpPrev3').on('click', function() {
        $('#dpiTab4').removeClass('active');
        $('#tdTab3').addClass('active');
        $('#tab4').removeClass('active');
        $('#tab3').addClass('active');
    });
</script>
<script>
    function  fillAddress()
    {
        document.getElementById("billto").value = "";
        var cust_companyname = document.getElementById("cust_companyname").value;
        var firstname = document.getElementById("firstname").value === "" ? "" : document.getElementById("firstname").value;
        var lastname = document.getElementById("lastname").value === "" ? "" : document.getElementById("lastname").value;

        var streetNo = document.getElementById("streetNo").value === "" ? "" : document.getElementById("streetNo").value + "";
        var city = document.getElementById("city").value === "" ? "" : document.getElementById("city").value + "";
        var cust_province = document.getElementById("cust_province").value === "" ? "" : document.getElementById("cust_province").value + "";
        var country = document.getElementById("country").value === "" ? "" : document.getElementById("country").value + "";
        var postal_code = document.getElementById("postal_code").value === "" ? "" : document.getElementById("postal_code").value + "";

        document.getElementById("billto").value = cust_companyname + "\n" +
                "" + firstname + " " + lastname + ""
                + "\n" + streetNo + "\n" + city + "," + cust_province + "\n" + postal_code + " " + country;

        document.getElementById("shipto").value = document.getElementById("billto").value;
    }
    function copyOrRemove(flag) {
        if (flag === "1") {
            document.getElementById("shipto").value = document.getElementById("billto").value;
        } else {
            document.getElementById("shipto").value = "";
        }
    }

    function validateDuplicate() {
        var dataString = "companyname=" + $("#cust_companyname").val();
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function(data) {
            var obj = JSON.parse(data);
            var isavailable = obj.cust_companyname;
            if (isavailable !== "" && isavailable !== undefined && isavailable !== "undefined") {
                $('#duplicatecustomer').modal('show');
                $("#cust_companyname").val("");
                $("#cust_companyname").focus();
            }
        }).fail(function() {
        });
    }
</script>
<!-- this is custom model dialog --->
<div id="duplicatecustomer" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <h3>DUPLICATION!!!</h3>
    </div>
    <div class="modal-body">
        <h5 style="color: red">
            Customer with this company name already exist.<br/>
        </h5>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
    </div>
</div>
<!-- this is model dialog --->
<script type="text/javascript">
    jQuery(function() {
        var counter = 1;
        jQuery('#icon-plus').click(function(event) {
            event.preventDefault();
            var newRow = jQuery('<tr><td><input type="text" style="width: 220px;" minlenght="2" maxlength="30" name="contact_person[]"  id="contact_person"></td>' +
                    '<td><input style="width: 220px;" type="email" name="email[]" autofocus=""  id="email"></td>' +
                    '<td><input type="text" style="width: 50px;" name="code[]" id="code" value="+1"> <input style="width: 220px;" type="tel" name="alternos[]" id="alterno' + counter + '"  ></td>' +
                    '<td><input style="width: 220px;" type="text" maxlength="30" name="designation[]" id="designation' + counter + '" ></td>' +
                    '<td><input type="checkbox" name="mail[]" id="mail' + counter + '"  value="Y" ></td></td>' +
                    '<td><a class="icon-trash" href="#" id="icon-trash" ></a></td>');
            $("#alterno" + counter).mask("(999) 999-9999");
            counter++;
            jQuery('#addcontacts').append(newRow);
        });
    });

    $(document).ready(function() {
        $("#addcontacts").on('click', '#icon-trash', function() {
            $(this).closest('tr').remove();
        });
    });

</script>

<script>
    function chkNumericKey(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode >= 48 && charCode <= 57) || charCode === 46 || charCode === 45) {
            return true;
        } else {
            return false;
        }
    }
    $(document).ready(function($) {
        $("#cardnumber").mask("9999-9999-9999-9999");
    });
</script>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>
<script type="text/javascript">
    jQuery(function() {
        var counter = 1;
        jQuery('#paymentInfoFrm').click(function(event) {
            event.preventDefault();
            var newRow = jQuery('<tr><td><label class="control-label">Credit Card No.</label></td><td><input style="width: 180px;" type="text" name="cardnumber[]" onkeypress="return chkNumericKey(event)" minlenght="2" maxlength="16" id="cardnumber' + counter + '"></td>' +
                    counter + '<td><label class="control-label">Name on Card</label></td><td><input style="width: 180px;" type="text" name="nameoncard[]" minlenght="2" maxlength="30"  id="nameoncard"></td>' +
                    counter + '<td><label class="control-label">Exp. Date</label></td><td><input style="width: 180px;" type="text" name="expdate[]" id="datepicker1"></td>' +
                    counter + '<td><label class="control-label">CVV</label></td><td><input style="width: 40px" type="text" onkeypress="return chkNumericKey(event)" minlenght="2" maxlength="3" name="cvvno[]" id="cvvno">\n\
                                <a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-trash" href="#"  ></a></td>');
            $("#cardnumber" + counter).mask("9999-9999-9999-9999");
            counter++;
            jQuery('#payment').append(newRow);
        });
    });
    $(document).ready(function() {

        $("#payment").on('click', 'a.icon-trash', function() {
            $(this).closest('tr').remove();
        });
    });</script>

<script>
    function submitCustomer() {
        var cust_companyname = document.getElementById('cust_companyname').value;
        var firstname = document.getElementById('firstname').value;
        var cust_email = document.getElementById('cust_email').value;
        var phno = document.getElementById('phno').value;
        var streetNo = document.getElementById('streetNo').value;
        var city = document.getElementById('city').value;
        var cust_province = document.getElementById('cust_province').value;
        var postal_code = document.getElementById('postal_code').value;
        var country = document.getElementById('country').value;

        //
        var cust_type = document.getElementById('cust_type').value;

        if (cust_companyname === "" || firstname === "" || cust_email === "" || phno === "" || streetNo === "" ||
                city === "" || cust_province === "" || postal_code === "" || country === "") {
            $('#ciTab1').addClass('active');
            $('#tab1').addClass('active');
            $('#tab4').removeClass('active');
            $('#dpiTab4').removeClass('active');

        } else if (cust_type === "") {
            $('#tdTab3').addClass('active');
            $('#tab3').addClass('active');
            $('#tab4').removeClass('active');
            $('#dpiTab4').removeClass('active');
        } else {
            document.getElementById("frmCustomerSubmit").action = "customermaster/savecustomermaster_ajax.php";
            $("#frmCustomerSubmit").submit();
            return true;
        }
    }
</script>