<script>
    $(document).ready(function($) {
        $("#supp_phoneNo").mask("(999) 999-9999");
        $("#alterno").mask("(999) 999-9999");
        $("#mobileno").mask("(999) 999-9999");
        $("#mobno").mask("(999) 999-9999");
        $("#supp_fax").mask("(999) 999-9999");
        $("#creditcardno").mask("9999-9999-9999-9999");
    });
</script>
<div class="container-fluid" >
    <br/>
    <div class="widget-box" style="width: 100%;">
        <div class="widget-title">
            <ul class="nav nav-tabs">
                <li id="siTab1" class="active"><a data-toggle="tab" href="#tab1">VENDOR INFORMATION</a></li>
                <li id="adTab2"><a data-toggle="tab" href="#tab2">ADDITIONAL CONTACTS</a></li>
            </ul>
        </div>
        <form name="frmCustomerSubmit" id="frmSupplierSubmit" method="post" action="suppliermaster/save_supplierajax.php">
            <div class="widget-content tab-content">
                <div id="tab1" class="tab-pane active">

                    <table class="display nowrap sortable" style="width: 100%; vertical-align: top" >
                        <tr>
                            <td><label class="control-label" style="float: left">First Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text" name="firstname"  onkeyup="fillAddress()"  autofocus="" required="" value="<?php echo $supplier["firstname"] ?>"  id="firstname" minlength="2" maxlength="30" ></td>
                            <td><label class="control-label" style="float: left">Last Name </label></td>
                            <td><input  style="width: 220px"type="text" name="lastname" autofocus=""  onkeyup="fillAddress()"   value="<?php echo $supplier["lastname"] ?>" id="lastname" minlength="2" maxlength="30" ></td>
                            <td><label class="control-label"  style="float: left">Company Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text" name="companyname"  onkeyup="fillAddress()"  id="companyname" onfocusout="validateDuplicate()"   value="<?php echo $supplier["companyname"] ?>" required=""  minlength="2" maxlength="30" ></td>
                        </tr>
                        <tr>

                            <td><label class="control-label"  style="float: left">Email<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input  style="width: 220px"type="email" name="supp_email" id="supp_email"   required="" value="<?php echo $supplier["supp_email"] ?>"  minlength="2" maxlength="30" ></td>
                            <td><label class="control-label" style="float: left">Mobile No</label></td>
                            <td>
                                <input type="text" style="width: 40px;" value="<?php echo $supplier["mobcode"] == "" ? "+1" : $supplier["mobcode"] ?>" name="mobcode" id="mobcode" >
                                <input style="width: 160px" type="tel" name="mobileno"  value="<?php echo $supplier["mobileno"] ?>"  id="mobileno" minlength="2" maxlength="20" >
                            </td>
                            <td><label class="control-label" style="float: left">Phone No</label></td>
                            <td>
                                <input type="text" style="width: 40px;" value="<?php echo $supplier["phonecode"] == "" ? "+1" : $supplier["phonecode"] ?>" name="phonecode" id="phonecode" >
                                <input style="width: 160px" type="tel" name="supp_phoneNo"  value="<?php echo $supplier["supp_phoneNo"] ?>"  id="supp_phoneNo" minlength="2" maxlength="20" >
                            </td>
                        </tr>
                        <tr>
                            <td><label class="control-label"  style="float: left">Fax </label></td>
                            <td>
                                <input type="text" style="width: 40px;"  value="<?php echo $supplier["faxcode"] == "" ? "+1" : $supplier["faxcode"] ?>" name="faxcode" id="faxcode" >
                                <input style="width: 160px" type="text" name="supp_fax" id="supp_fax"    value="<?php echo $supplier["supp_fax"] ?>" minlength="2" maxlength="20">
                            </td>
                            <td><label class="control-label" style="float: left">Website</label></td>
                            <td><input style="width: 220px" type="text" name="supp_website" id="supp_website"  value="<?php echo $supplier["supp_website"] == "" ? "-" : $supplier["supp_website"] ?>" minlength="2" maxlength="30"></td>
                            <td><label class="control-label" style="float: left">Currency</label></td>
                            <td>
                                <select style="width: 225px;height: 26px" name="currency"  id="currency" value="">
                                    <?php
                                    $currencyarr = getcurrency();
                                    foreach ($currencyarr as $key => $value) {
                                        ?>
                                        <option value="<?php echo $key ?>" <?php echo $supplier["currency"] == $key ? "selected" : "" ?> ><?php echo $value ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td><label class="control-label"  style="float: left"> Good Tax<?php echo MysqlConnection::$REQUIRED ?></label> </td>
                            <td>
                                <select style="width: 225px" name="taxInformation" id="taxInformation" required="">
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                                        <option   <?php echo $value["id"] == $supplier["taxInformation"] ? "selected" : "" ?>
                                            value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                                        <?php } ?>
                                </select>
                            </td>
                            <td> <label class="control-label"  style="float: left">Shipping Tax<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td>
                                <select style="width: 225px" name="shippingtaxinfo" id="shippingtaxinfo" required="">
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                                        <option   <?php echo $value["id"] == $supplier["shippingtaxinfo"] ? "selected" : "" ?>
                                            value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                                        <?php } ?>
                                </select>
                            </td>

                            <td><label class="control-label" style="float: left">Exchange Rate</label></td>
                            <td><input style="width: 220px" type="text" name="exchange_rate" id="exchange_rate" onkeypress="return chkNumericKey(event)" value="<?php echo $supplier["exchange_rate"] ?>" maxlength="2"></td>

                        </tr>
                        <tr>
                            <td><label class="control-label">Address<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text" name="supp_streetNo"   onkeyup="fillAddress()" required="" id="supp_streetNo"    minlenght="2" maxlength="70" value="<?php echo $supplier["supp_streetNo"] ?> <?php echo $supplier["supp_streetName"] ?>" ></td>

                            <td><label class="control-label">City<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text"  onkeyup="fillAddress()"  name="supp_city" id="supp_city"  required=""  minlenght="2" maxlength="30" value="<?php echo $supplier["supp_city"] ?>" ></td>

                            <td><label class="control-label">Province<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text"  onkeyup="fillAddress()"  name="supp_province" id="supp_province"  required=""  minlenght="2" maxlength="30" value="<?php echo $supplier["supp_province"] ?>" ></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Postal Code<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text"  onkeyup="fillAddress()"  name="postal_code" id="postal_code" required=""  minlenght="2" maxlength="30"  value="<?php echo $supplier["postal_code"] ?>" ></td>
                            <td><label class="control-label">Country<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input style="width: 220px" type="text" onkeyup="fillAddress()"  name="supp_country" id="supp_country"  required=""  minlenght="2" maxlength="30" value="<?php echo $supplier["supp_country"] ?>" ></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="vertical-align: middle">
                            <td><label class="control-label">Address</label></td>
                            <td><textarea style="height: 130px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize" name="billto" onfocus="fillAddress()"  id="billto" ><?php echo $supplier["billto"] ?></textarea></td>
                            <td><label class="control-label">Pick Up address</label></td>
                            <td><textarea   style="height: 130px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize"  name="shipto" id="shipto"><?php echo $supplier["shipto"] ?></textarea></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a onclick="copyOrRemove('1')" style="cursor: pointer"  title="Copy address to pickup address">COPY >></a></td>
                            <td></td>
                            <td><a onclick="copyOrRemove('0')" style="cursor: pointer" title="Remove pickup address"><< REMOVE</a></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table> 
                    <hr/>
                    <input type="button" id="btnVenNext1" value="NEXT"   class="btn btn-info" >

                    <script>
                        function copyOrRemove(flag) {
                            if (flag === "1") {
                                document.getElementById("shipto").value = document.getElementById("billto").value;
                            } else {
                                document.getElementById("shipto").value = "";
                            }
                        }
                        function validateDuplicate() {
                            var dataString = "companyname=" + $("#companyname").val();
                            $.ajax({
                                type: 'POST',
                                url: 'suppliermaster/searchsupplier_ajax.php',
                                data: dataString
                            }).done(function(data) {
                                var obj = JSON.parse(data);
                                var isavailable = obj.companyname;
                                if (isavailable !== "" && isavailable !== undefined && isavailable !== "undefined") {
                                    $('#duplicatecustomer').modal('show');
                                    $("#companyname").val("");
                                    $("#companyname").focus();
                                }
                            }).fail(function() {
                            });
                        }
                    </script>

                    <!-- this is custom model dialog --->
                    <div id="duplicatecustomer" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
                        <div class="modal-header">
                            <h3>DUPLICATION!!!</h3>
                        </div>
                        <div class="modal-body">
                            <h5 style="color: red">
                                Vendor with this company name already exist.<br/>
                            </h5>
                        </div>
                        <div class="modal-footer"> 
                            <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
                        </div>
                    </div>
                    <!-- this is model dialog --->


                </div>
                <div id="tab2" class="tab-pane ">

                    <table class="display nowrap sortable table-bordered" style="width: 100%;" class="display nowrap sortable" id="supplierInfo" vertical-align="top">
                        <tr style="background-color: #76323F;color: white">
                            <td>Name</td>
                            <td>Email</td>
                            <td>Mobile No</td>
                            <td>Phone No</td>
                            <td>Designation</td>

                        </tr>
                        <tr>
                            <td><input style="width: 180px" type="text" name="contact_person[]"  id="contact_person" minlength="2" maxlength="30"></td>
                            <td><input style="width: 180px" type="email" name="email[]" autofocus=""  id="email" minlength="2" maxlength="30"></td>
                            <td>
                                <input type="text" style="width: 30px;" name="mobcode[]" id="mobcode"  value="<?php echo $value["mobcode"] == "" ? "+1" : $value["mobcode"] ?>" >
                                <input style="width: 170px" type="text" id="mobno" name="mobno[]"  minlength="2" maxlength="20">
                            </td>
                            <td>
                                <input type="text" style="width: 30px;" name="code[]" id="code"  value="<?php echo $value["code"] == "" ? "+1" : $value["code"] ?>" >
                                <input style="width: 170px" type="text" id="alterno" name="alterno[]"  minlength="2" maxlength="20">
                            </td>
                            <td>
                                <input style="width: 180px" type="text" id="designation" name="designation[]"  minlength="2" maxlength="20">
                                <a style="margin-left: 20px;margin-bottom: 10px;" class="icon-plus" href="#"  ></a>
                            </td>
                        </tr>
                    </table>
                    <hr/>
                    <input type="button" id="btnVenPrev1" value="PREVIOUS" class="btn btn-info" href="#tab1">
                    <input type="submit" id="btnSubmiVendor" class="btn btn-info" onclick="validateVendorSubmit()" value="SUBMIT"/>
                    <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>

                </div>
            </div>   
            <input type="hidden" name="supplierid" id="supplierid" value="<?php echo $supplierid ?>">
        </form>
    </div>
</div>
<script>
    $('#btnVenNext1').on('click', function() {
        $('#siTab1').removeClass('active');
        $('#adTab2').addClass('active');
        $('#tab1').removeClass('active');
        $('#tab2').addClass('active');
    });
    $('#btnVenPrev1').on('click', function() {
        $('#adTab2').removeClass('active');
        $('#siTab1').addClass('active');
        $('#tab2').removeClass('active');
        $('#tab1').addClass('active');
    });

</script>
<script type="text/javascript">
    jQuery(function() {
        var counter = 1;
        jQuery('a.icon-plus').click(function(event) {
            event.preventDefault();
            var newRow = jQuery('<tr><td><input style="width: 180px" type="text" name="contact_person[]" value="<?php echo filter_input(INPUT_POST, "contact_person") ?>"  id="contact_person' + counter + '" minlength="2" maxlength="30"></td>' +
                    counter + '<td><input style="width: 180px" type="email" name="email[]" autofocus="" value="<?php echo filter_input(INPUT_POST, "email") ?>" id="email' + counter + '" minlength="2" maxlength="30"></td>' +
                    counter + '<td><input type="text" style="width: 30px;" name="mobcode[]" id="mobcode"  value="+1" > <input style="width: 170px" type="text" id="mobno1' + counter + '" name="mobno[]"  value="<?php echo filter_input(INPUT_POST, "mobno") ?>" minlength="2" maxlength="20"></td>' +
                    counter + ' <td><input type="text" style="width: 30px;" name="code[]" id="code"  value="+1" > <input style="width: 170px" type="text" id="alterno' + counter + '" name="alterno[]"  value="<?php echo filter_input(INPUT_POST, "alterno") ?>" minlength="2" maxlength="20"></td>' +
                    counter + '<td><input style="width: 180px" type="text" id="designation" name="designation[]"  value="<?php echo filter_input(INPUT_POST, "designation[]") ?>" minlength="2" maxlength="20"> <a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-trash" href="#"  ></a></td>');
            $("#alterno" + counter).mask("(999) 999-9999");
            $("#mobno1" + counter).mask("(999) 999-9999");
            counter++;
            jQuery('#supplierInfo').append(newRow);
        });
    });

    $(document).ready(function() {
        $("#supplierInfo").on('click', 'a.icon-trash', function() {
            $(this).closest('tr').remove();
        });
    });

    function validateVendorSubmit() {
        var firstname = $("firstname").val();
        var companyname = $("companyname").val();
        var supp_email = $("supp_email").val();
        var taxInformation = $("taxInformation").val();
        var supp_phoneNo = $("supp_phoneNo").val();
        var shippingtaxinfo = $("shippingtaxinfo").val();
        var supp_streetNo = $("supp_streetNo").val();
        var supp_city = $("supp_city").val();
        var supp_province = $("supp_province").val();
        var postal_code = $("postal_code").val();
        var supp_country = $("supp_country").val();

        if (firstname === undefined || companyname === undefined || supp_email === undefined || taxInformation === undefined ||
                supp_phoneNo === undefined || shippingtaxinfo === undefined || supp_streetNo === undefined || supp_city === undefined ||
                supp_province === undefined || postal_code === undefined || supp_country === undefined) {
            $('#siTab1').addClass('active');
            $('#tab1').addClass('active');
            $('#adTab2').removeClass('active');
            $('#tab2').removeClass('active');
        }

    }

    function  fillAddress() {
        document.getElementById("billto").value = "";
        var cust_companyname = document.getElementById("companyname").value;
        var firstname = document.getElementById("firstname").value === "" ? "" : document.getElementById("firstname").value;
        var lastname = document.getElementById("lastname").value === "" ? "" : document.getElementById("lastname").value;

        var streetNo = document.getElementById("supp_streetNo").value === "" ? "" : document.getElementById("supp_streetNo").value + "";
        var city = document.getElementById("supp_city").value === "" ? "" : document.getElementById("supp_city").value + "";
        var cust_province = document.getElementById("supp_province").value === "" ? "" : document.getElementById("supp_province").value + "";
        var country = document.getElementById("supp_country").value === "" ? "" : document.getElementById("supp_country").value + "";
        var postal_code = document.getElementById("postal_code").value === "" ? "" : document.getElementById("postal_code").value + "";

        document.getElementById("billto").value = cust_companyname + "\n" +
                "" + firstname + " " + lastname + ""
                + "\n" + streetNo + "\n" + city + "," + cust_province + "\n" + postal_code + " " + country;

        document.getElementById("shipto").value = document.getElementById("billto").value;
    }
</script>