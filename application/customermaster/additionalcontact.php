<?php
if (!empty($customerid)) {
    $customercontactarray = MysqlConnection::fetchCustom("SELECT * FROM customer_contact WHERE cust_id = '$customerid'");
}
?>
<script>
    $(document).ready(function($) {
        $("#alterno").mask("(999) 999-9999");
        $("#mobileno").mask("(999) 999-9999");
        $("#mobileno1").mask("(999) 999-9999");
        $("#mobileno2").mask("(999) 999-9999");
    });
</script>
<style>
    .ctable{

    }
    .ctable tr td{
        height: 30px;
        vertical-align: middle;
    }
    .ctable tr td input{
        margin-top: 5px;
    }
</style>
<table id="addcontacts"  border="0" class="display nowrap sortable table-bordered"  style="width: 100%"> 
    <tr style="background-color: #A9CDEC;color: white">
        <td>Name</td>
        <td>Email</td>
        <td>Mobile No</td>
        <td>Phone No</td>
        <td>Designation</td>
        <td>EMail</td>
        <td>Delivery EMail</td>
        <td ></td>


    </tr>
    <?php
    if (count($customercontactarray) != 0) {
        $index = 1;
        foreach ($customercontactarray as $key => $value) {
            if ($value["mail"] == "Y" || $value["othermail"] == "Y" ) {
                $check = "checked=''";
            } else {
                $check = "";
            }
            ?>
            <tr style="vertical-align: bottom">
                <td><input type="text" name="contact_person[]" value="<?php echo $value["person_name"] ?>" minlenght="2" maxlength="30"  id="alter_contact"></td>
                <td><input   type="email" name="email[]" autofocus=""  value="<?php echo $value["person_email"] ?>" id="email"></td>
                <td>
                    <input type="text" style="width: 50px;" name="mobcode[]" id="code"  value="<?php echo $value["mobcode"] == "" ? "+1" : $value["mobcode"] ?>" >
                    <input  type="text" name="mobileno[]" id="mobileno"  value="<?php echo $value["mobileno"] ?>" >
                </td>
                <td>
                    <input type="text" style="width: 50px;" name="code[]" id="code"  value="<?php echo $value["code"] == "" ? "+1" : $value["code"] ?>" >
                    <input  type="text" name="alternos[]" id="alterno"  value="<?php echo $value["person_phoneNo"] ?>" >
                </td>
                <td><input type="text" name="designation[]" id="designation"  value="<?php echo $value["designation"] ?>" ></td>
                <td><input type="checkbox" name="mail[]" <?php echo $check ?> id="mail"  value="Y" /></td>
                <td><input type="checkbox" name="othermail[]" <?php echo $check ?> id="othermail"  value="Y" /></td>
                <td>
                    <?php
                    if ($index == 1) {
                        echo '<a  class="icon-plus" href="#" id="icon-plus" ></a>';
                    } else {
                        echo '<a class="icon-trash" href="#" id="icon-trash" ></a>';
                    }
                    ?>
                </td>
            </tr>
            <?php
            $index++;
        }
    } else {
        ?>
        <tr style="vertical-align: bottom">
            <td><input style="width: 220px" type="text" name="contact_person[]" value="" minlenght="2" maxlength="30"  id="alter_contact"></td>
            <td><input style="width: 220px" type="email" name="email[]" autofocus=""  value="" id="email"></td>
            <td>
                <input type="text" style="width: 50px;" name="mobcode[]" id="code"  value="<?php echo $value["mobcode"] == "" ? "+1" : $value["mobcode"] ?>" >
                <input type="text" name="mobileno[]" id="mobileno"  value="<?php echo $value["mobileno"] ?>" >
            </td>
            <td>
                <input type="text" style="width: 50px;" name="code[]" id="code"  value="<?php echo $value["code"] == "" ? "+1" : $value["code"] ?>" >
                <input style="width: 220px" type="tel" name="alternos[]" id="alterno"  value="" >
            </td>
            <td><input style="width: 220px" type="text" name="designation[]" id="designation"  value="" maxlength="45"></td>


            <td><input type="checkbox" name="mail[]" id="mail"  value="Y" ></td>
            <td><input type="checkbox" name="othermail[]" id="othermail"  value="Y" ></td>
            <td><span  data-toggle="tooltip" data-original-title="Click here to add more contact details"><a class="icon-plus" href="#" id="icon-plus" ></a></span></td>
        </tr>
        <?php
    }
    ?>
</table> 

<hr/>
<input type="button" id="btnCmpPrev1" value="PREVIOUS" class="btn btn-info" href="#tab1" >

<?php
if (isset($customerid) && $customerid != "") {
    ?>
    <button type="submit" onclick="return submitCustomer()" id="btnSubmitFullForm" class="btn btn-info">SAVE</button>      
    <?php
}
?>
<input type="button" id="btnCmpNext2" value="NEXT" class="btn btn-info" href="#tab2"  >




<script type="text/javascript">

    jQuery(function() {
        var counter = 1;
        jQuery('#icon-plus').click(function(event) {
            event.preventDefault();
            var newRow = jQuery('<tr><td><input type="text" style="width: 220px;" minlenght="2" maxlength="30" name="contact_person[]"  id="contact_person"></td>' +
                    '<td><input style="width: 220px;" type="email" name="email[]" autofocus=""  id="email"></td>' +
                    '<td><input type="text" style="width: 50px;" name="mobcode[]" id="mobcode" value="+1"> <input style="width: 220px;" type="tel" name="mobileno[]" id="mobileno2' + counter + '"  ></td>' +
                    '<td><input type="text" style="width: 50px;" name="code[]" id="code" value="+1"> <input style="width: 220px;" type="tel" name="alternos[]" id="alterno' + counter + '"  ></td>' +
                    '<td><input style="width: 220px;" type="text" maxlength="30" name="designation[]" id="designation' + counter + '" ></td>' +
                    '<td><input type="checkbox" name="mail[]" id="mail' + counter + '"  value="Y" ></td></td>' +
                    '<td><input type="checkbox" name="othermail[]" id="othermail' + counter + '"  value="Y" ></td></td>' +
                    '<td><a class="icon-trash" href="#" id="icon-trash" ></a></td>');
            $("#alterno" + counter).mask("(999) 999-9999");
            $("#mobileno2" + counter).mask("(999) 999-9999");
            counter++;
            jQuery('#addcontacts').append(newRow);
        });
    });

    $(document).ready(function() {
        $("#addcontacts").on('click', '#icon-trash', function() {
            $(this).closest('tr').remove();
        });
    });

</script>




