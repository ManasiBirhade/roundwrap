<?php
$arrsalutations = MysqlConnection::fetchCustom("SELECT distinct(`salutation`) as salutation FROM `customer_master` WHERE salutation!=''");


$onhold = $customer["onhold"];
$onholdreason = $customer["onholdreason"];
?>
<style>
    tbody {
        height: auto;
    }
    td{
        padding-left:   10px;
        padding-right:   10px;
    }
    select {
        width: 212px;
        height: 24px;
    }
    tr{
    }
</style>
<script>
    $(document).ready(function($) {
        $("#phno").mask("(999) 999-9999");
        $("#cust_fax").mask("(999) 999-9999");
        $("#mobileno").mask("(999) 999-9999");
        $('textarea[name="onholdreason"]').hide();
        <?php
        if ($onhold == "Y" || $onhold == "y") {
            echo " $('textarea[name='onholdreason']').show();";
        }
        ?>
    });
    $(function() {
        $("#cust_companyname").autocomplete({
            source: "customermaster/customerautocomplete.php",
        });
    });
</script>

<table class="display nowrap sortable"  style="width: 100%;vertical-align: top" border="0">
    <tr>
        <td ><label class="control-label">First Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input tabindex="1" style="width: 220px" type="text"  name="firstname" id="firstname" onkeyup="fillAddress()"  value="<?php echo $customer["firstname"] ?>" autofocus="" required="true" minlenght="2" maxlength="30" ></td>
        <td><label class="control-label">Last Name</label></td>
        <td><input tabindex="2" style="width: 220px" type="text" name="lastname" id="lastname" onkeyup="fillAddress()"  value="<?php echo $customer["lastname"] ?>" minlenght="2" maxlength="30" ></td>
        <td><label class="control-label">Company Name<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
        <td><input tabindex="3" style="width: 220px" type="text" name="cust_companyname" onfocusout="validateDuplicate()"  onkeyup="fillAddress()"  value="<?php echo $customer["cust_companyname"] ?>" id="cust_companyname" minlenght="2" maxlength="50" required="true"></td>
    </tr>
    <tr>

        <td><label class="control-label">Email<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
        <td><input tabindex="4" style="width: 220px" required=""  type="email" name="cust_email" id="cust_email" value="<?php echo $customer["cust_email"] ?>" ></td>

        <td><label class="control-label">Mobile No</label></label></td>
        <td>
            <input type="text" style="width: 40px;"  name="mobcode1" id="mobcode1"  value="<?php echo trim($customer["mobcode"]) == "" ? "+1" : $customer["mobcode"] ?>">
            <input tabindex="5" style="width: 160px" type="text" name="mobileno1"  id="mobileno1"    value="<?php echo trim($customer["mobileno"]) ?>" >
        </td>

        <td><label class="control-label">Phone</label></label></td>
        <td>
            <input type="text" style="width: 40px;"  name="phoneCode" id="phoneCode"  value="<?php echo trim($customer["phoneCode"]) == "" ? "+1" : $customer["phoneCode"] ?>">
            <input tabindex="6" style="width: 160px" type="text" name="phno"  id="phno"  value="<?php echo trim($customer["phno"]) ?>" >
        </td>
    </tr> 
    <tr >
        <td><label class="control-label">Fax</label></td>
        <td>
            <input type="text" style="width: 40px;"   name="faxCode" id="faxCode"  value="<?php echo trim($customer["faxCode"]) == "" ? "+1" : $customer["faxCode"] ?>">
            <input tabindex="7" style="width: 160px" type="text" name="cust_fax" id="cust_fax"  value="<?php echo $customer["cust_fax"] ?>" >
        </td>
        <td><label class="control-label">Web Site</label></td>
        <td><input tabindex="8" style="width: 220px" type="text" name="website" id="website" maxlength="30"  value="<?php echo $customer["website"] ?>" ></td>
        <td><label class="control-label">Customer Status</label></td>
        <td style="vertical-align: middle;width: 270px">
            <input style="width: " tabindex="9" type="checkbox" name="status" <?php echo $customer["status"] == "Y" ? "checked" : "" ?> id="status" value="Y" />
            Is customer active ?
            <input style="margin-left: 33px;" tabindex="9" type="checkbox" name="onhold" <?php echo $customer["onhold"] == "Y" ? "checked" : "" ?> id="onhold"  />
            On hold ?
        </td>
    </tr>

    <tr>
        <td><label class="control-label">Address<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
        <td><input tabindex="10" style="width: 220px"  required="" type="text" name="streetNo" id="streetNo" onkeyup="fillAddress()"  minlenght="2" maxlength="60" value="<?php echo $customer["streetNo"] ?> " ></td>
        <td><label class="control-label">City<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
        <td><input tabindex="11"  style="width: 220px" required="" type="text" name="city" id="city"  minlenght="2" onkeyup="fillAddress()"  maxlength="30" value="<?php echo $customer["city"] ?>" ></td>
        <td><label class="control-label">Province<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
        <td><input tabindex="12" style="width: 220px" required="" type="text" name="cust_province" id="cust_province" onkeyup="fillAddress()"   minlenght="2" maxlength="30" value="<?php echo $customer["cust_province"] ?>" ></td>
    </tr>
    <tr>
        <td><label class="control-label">Postal Code<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
        <td><input tabindex="13" style="width: 220px" type="text" required="" name="postal_code" id="postal_code" onkeyup="fillAddress()"  minlenght="2" maxlength="30"  value="<?php echo $customer["postal_code"] ?>" ></td>
        <td><label class="control-label">Country<?php echo MysqlConnection::$REQUIRED ?></label></label></td>
        <td><input tabindex="14" style="width: 220px" type="text" name="country" id="country"  minlenght="2" required="" onkeyup="fillAddress()"  maxlength="30" value="<?php echo $customer["country"] ?>" ></td>
        <td></td>
        <td></td>
    </tr>

    <tr style="vertical-align: top">
        <td><label class="control-label">Billing Address</label></td>
        <td><textarea tabindex="15" style="height: 130px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize" name="billto" onfocus="fillAddress()"  id="billto" ><?php echo MysqlConnection::formatToSlashNAddress($customer["billto"]) ?></textarea></td>
        <td><label class="control-label">Shipping Address </label></td>
        <td><textarea tabindex="16"  style="height: 130px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize"  name="shipto" id="shipto"><?php echo MysqlConnection::formatToSlashNAddress($customer["shipto"]) ?></textarea></td>
        <td><label class="control-label">On hold reason</label></td>
        <td><textarea tabindex="17"  style="height: 130px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize" name="onholdreason" id="onholdreason"><?php echo $customer["onholdreason"] ?></textarea></td>
    </tr>
    <tr>
        <td></td>
        <td><span  data-toggle="tooltip" data-original-title="Copy billing address to shipping address"><a onclick="copyOrRemove('1')" style="cursor: pointer" >COPY >></a></span></td>
        <td></td>
        <td><span  data-toggle="tooltip" data-original-title="Remove shipping address"><a onclick="copyOrRemove('0')" style="cursor: pointer" ><< REMOVE</a></span></td>
        <td></td>
        <td></td>
    </tr>
</table>

<hr/>
<input type="hidden" value="<?php echo $customerid ?>" name="customerid"/>
<a href="javascript:history.back()" class="btn btn-info">CANCEL</a>

<?php
if (isset($customerid) && $customerid != "") {
    ?>
    <button type="submit" onclick="return submitCustomer()" id="btnSubmitFullForm" class="btn btn-info">SAVE</button>      
    <?php
}
?>


<input tabindex="17" type="button" id="btnCmpNext1" value="NEXT" class="btn btn-info" />
<script src="index.js"></script>
<style type="text/css">
    .bs-example{
        margin: 50px;
    }
    .bs-example a{
        font-size: 22px;        
        text-decoration: none;
        margin: 0 10px;
        background-color: 1px solid #76323F;
    }

</style>
<script>
    function  fillAddress()
    {
        document.getElementById("billto").value = "";
        var cust_companyname = document.getElementById("cust_companyname").value;
        var firstname = document.getElementById("firstname").value === "" ? "" : document.getElementById("firstname").value;
        var lastname = document.getElementById("lastname").value === "" ? "" : document.getElementById("lastname").value;

        var streetNo = document.getElementById("streetNo").value === "" ? "" : document.getElementById("streetNo").value + "";
        var city = document.getElementById("city").value === "" ? "" : document.getElementById("city").value + "";
        var cust_province = document.getElementById("cust_province").value === "" ? "" : document.getElementById("cust_province").value + "";
        var country = document.getElementById("country").value === "" ? "" : document.getElementById("country").value + "";
        var postal_code = document.getElementById("postal_code").value === "" ? "" : document.getElementById("postal_code").value + "";

        document.getElementById("billto").value = cust_companyname + "\n" +
                "" + firstname + " " + lastname + ""
                + "\n" + streetNo + "\n" + city + "," + cust_province + "\n" + postal_code + " " + country;

        document.getElementById("shipto").value = document.getElementById("billto").value;
    }
    function copyOrRemove(flag) {
        if (flag === "1") {
            document.getElementById("shipto").value = document.getElementById("billto").value;
        } else {
            document.getElementById("shipto").value = "";
        }
    }

    function validateDuplicate() {
        var dataString = "companyname=" + $("#cust_companyname").val();
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function(data) {
            var obj = JSON.parse(data);
            var isavailable = obj.cust_companyname;
            if (isavailable !== "" && isavailable !== undefined && isavailable !== "undefined") {
                $('#duplicatecustomer').modal('show');
                $("#cust_companyname").val("");
                $("#cust_companyname").focus();
            }
        }).fail(function() {
        });
    }


    $('input[name=onhold]').click(function() {
        if (this.checked) {
            $('#onholdreason').show();
        } else {
            $('#onholdreason').hide();
        }
    });
</script>

<script>

</script>

<!-- this is custom model dialog --->
<div id="duplicatecustomer" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <h3>DUPLICATION !!!</h3>
    </div>
    <div class="modal-body">
        <h5 style="color: red">
            Customer with this company name already exist.<br/>
        </h5>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
    </div>
</div>
<!-- this is model dialog --->