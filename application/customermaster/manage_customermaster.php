<?php
$status = filter_input(INPUT_GET, "status");
$action = filter_input(INPUT_GET, "action");
$walkin = filter_input(INPUT_GET, "walkin");
if ($status == "active") {
    $query = "SELECT * FROM customer_master WHERE status = 'Y'    ORDER BY `cust_companyname` ASC  ";
} else if ($status == "inactive") {
    $query = " SELECT * FROM customer_master WHERE status = 'N'   ORDER BY `cust_companyname` ASC  ";
} else if ($status == "all") {
    $query = "SELECT * FROM customer_master  ORDER BY `cust_companyname` ASC   ";
} else if ($walkin == "yes") {
    $query = "SELECT * FROM customer_master WHERE isCash = 'Y'   ORDER BY `cust_companyname` ASC   ";
} else {
    $query = "SELECT * FROM customer_master ORDER BY `cust_companyname` ASC  ";
}

$listofcustomers = MysqlConnection::fetchCustom($query);


$customerid = filter_input(INPUT_GET, "customerId");
if (isset($customerid) && $customerid != "") {
    $customerd = MysqlConnection::fetchCustom("SELECT status FROM customer_master WHERE id = '$customerid'");
    if ($customerd[0]["status"] == "Y") {
        MysqlConnection::delete("UPDATE customer_master SET status = 'N' WHERE id = '$customerid' ");
    } else {
        MysqlConnection::delete("UPDATE customer_master SET status = 'Y' WHERE id = '$customerid' ");
    }
    header("location:index.php?pagename=manage_customermaster&status=active&action=delete");
}
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid" >
    <br/>
    <?php
    if ($action == "add") {
        echo MysqlConnection::$MSG_AD;
    } elseif ($action == "delete") {
        echo MysqlConnection::$MSG_DE;
    } elseif ($action == "update") {
        echo MysqlConnection::$MSG_UP;
    }
    ?>
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">CUSTOMER LIST</h5>
    </div>
    <br/>
    <table style="width: 100%" border="0">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=create_customermaster" ><i class="icon icon-user"></i>&nbsp;&nbsp;ADD&nbsp;CUSTOMER</a>
                <a href="index.php?pagename=manage_customermaster&status=active" id="btnSubmitFullForm" class="btn btn-info">VIEW ACTIVE</a>
                <a href="index.php?pagename=manage_customermaster&status=inactive" id="btnSubmitFullForm" class="btn btn-info">VIEW INACTIVE</a>
                <a href="index.php?pagename=manage_customermaster&status=all" id="btnSubmitFullForm" class="btn btn-info">VIEW ALL</a>
                <a href="index.php?pagename=manage_customermaster&walkin=yes" id="btnSubmitFullForm" class="btn btn-info">VIEW WALK IN</a>
            </td>
            <td style="float: right">
                <a href="sample/customer-master.xlsx" id="btnSubmitFullForm" class="btn btn-info">EXCEL</a>
            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>
                <td>Company Name</td>
                <td>Address</td>
                <td>Contact Person</td>
                <td>Mobile No</td>
                <td>Contact No</td>
                <td>Email</td>
                <td>Currency</td>
                <td>Balance</td>
                <td>Sales Person</td>
                <td>Note</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listofcustomers as $key => $value) {
                $sales_person_name = MysqlConnection::getGenericById($value["sales_person_name"]);
                $bgcolor = MysqlConnection::generateBgColor($index);
                ?>
                <tr id="<?php echo $value["id"] ?>" class="context-menu-one" style="background-color: <?php echo $bgcolor ?>;">
                    <td ><?php echo $index ?></td>
                    <td ><?php echo $value["cust_companyname"] ?></td>
                    <td >
                        <?php echo buildAddress($value) ?>
                    </td>
                    <td ><?php echo $value["firstname"] ?>&nbsp;<?php echo $value["lastname"] ?></td>
                    <td ><a href="tel:<?php echo $value["mobcode"] . " " . $value["mobileno"] ?>"><?php echo $value["mobcode"] . " " . $value["mobileno"] ?></a></td>
                    <td ><a href="tel:<?php echo $value["phoneCode"] . " " . $value["phno"] ?>"><?php echo $value["phoneCode"] . " " . $value["phno"] ?></a></td>
                    <td >
                        <a href="mailto:<?php echo $value["cust_email"] ?>?Subject=Welcome, <?php echo ucwords($value["cust_companyname"]) ?> " target="_top">
                            &nbsp;<?php echo $value["cust_email"] ?>
                        </a>
                    </td>
                    <td >&nbsp;&nbsp;<?php echo $value["currency"] ?></td>
                    <td ><?php echo $value["balance"] ?>&nbsp;&nbsp;</td>
                    <td><?php echo $sales_person_name["code"] == "" ? $sales_person_name["name"] : $sales_person_name["code"] ?></td>
                    <td>
                        <?php
                        $counter = MysqlConnection::fetchCustom("SELECT count(`id`) as id FROM `customer_notes` where `cust_id` = '" . $value["id"] . "'");
                        if ($counter[0]["id"] != 0) {
                            ?>
                            <a id="click" href="#" onclick="showNotes('<?php echo $value["id"] ?>')">
                                <i class="icon-book"></i>
                            </a>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table> 
</div>


<script>
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_customer":
                        window.location = "index.php?pagename=view_customermaster&customerId=" + id;
                        break;
                    case "create_customer":
                        window.location = "index.php?pagename=create_customermaster";
                        break;
                    case "create_note":
                        window.location = "index.php?pagename=note_customermaster&customerId=" + id;
                        break;
                    case "edit_customer":
                        window.location = "index.php?pagename=create_customermaster&customerId=" + id;
                        break;
                    case "delete_customer":
                        window.location = "index.php?pagename=view_customermaster&customerId=" + id + "&flag=yes";
                        break;
                    case "create_sales_order":
                        window.location = "index.php?pagename=create_salesorder&customerId=" + id;
                        break;
                    case "create_packingslip":
                        window.location = "index.php?pagename=select_packingslip&customerId=" + id;
                        break;
                    case "active_customer":
                        window.location = "index.php?pagename=manage_customermaster&customerId=" + id;
                        break;
                    case "create_payment":
                        window.location = "index.php?pagename=customer_payment&customerId=" + id;
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    default:
                        window.location = "index.php?pagename=manage_customermaster";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_customer": {name: "VIEW CUSTOMER", icon: "+"},
                "edit_customer": {name: "EDIT CUSTOMER", icon: "context-menu-icon-add"},
                "delete_customer": {name: "DELETE CUSTOMER", icon: ""},
                "active_customer": {name: "ACTIVE/IN ACTIVE CUSTOMER", icon: ""},
                "sep1": "---------",
                "create_note": {name: "CREATE NOTE", icon: ""},
                "create_sales_order": {name: "CREATE ORDER", icon: ""},
                "create_packingslip": {name: "CREATE PACKING SLIP", icon: ""},
                "create_payment": {name: "MAKE PAYMENT", icon: ""}
            }
        });
    });

    $('tr').dblclick(function () {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_customermaster&customerId=" + id;
        }
    });

    function showNotes(customerid) {
        var dataString = "customerid=" + customerid;
        $("div#divLoading").addClass('show');
        $('#noteinformation').modal('show');
        var dataString = "customerid=" + customerid;
        $.ajax({
            type: 'POST',
            url: 'customermaster/ajaxcustomernotes.php',
            data: dataString
        }).done(function (data) {
            $("#notedata").html(data);
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    }

    $("#myElem").show().delay(3000).fadeOut();
</script> 
<!-- this is model dialog --->
<?php

function buildAddress($value) {
    return $value["streetNo"]
            . " " . $value["streetName"] . "<br/>"
            . " " . $value["city"]
            . " " . $value["postal_code"] . "<br/>"
            . " " . $value["cust_province"]
            . " <b>" . $value["country"] . "</b>";
}
?>

<!-- this is note model dialog --->
<div id="noteinformation" class="modal hide" style="top: 10%;left: 50%;">
    <div class="modal-header" style="text-align: center">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>NOTES</h3>
    </div>
    <div class="modal-body">
        <div class="control-group">
            <div id="notedata"></div>
        </div>
    </div>
    <div class="modal-footer" style="text-align: center"> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelti" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is note model dialog --->