<?php
$customerid = filter_input(INPUT_GET, "customerId");

$salesorderid = filter_input(INPUT_GET, "salesorderid");
if ($salesorderid) {
    $sodetails = MysqlConnection::getSalesOrderDetailsById($salesorderid);
    $customerid = $sodetails["customer_id"];
}


$flag = filter_input(INPUT_GET, "flag");
$type = filter_input(INPUT_GET, "type");

$customer = MysqlConnection::getCustomerDetails($customerid);
$arrcustomernote = MysqlConnection::fetchCustom("SELECT * FROM  `customer_notes` WHERE cust_id = '$customerid'  ORDER BY adddate  LIMIT 0,20");

if (isset($_POST["btnSubmitFullForm"])) {
    unset($_POST["btnSubmitFullForm"]);
    $_POST["cust_id"] = $customerid;

    $_POST["reminderdate"] = MysqlConnection::convertToDBDate($_POST["reminderdate"]);

    $_POST["adddate"] = date("Y-m-d");

    if ($_POST["reminderdate"]) {
        $_POST["isRead"] = "N";
    }

    MysqlConnection::insert("customer_notes", $_POST);
    header("location:index.php?pagename=note_customermaster&customerId=$customerid");
}

$noteid = filter_input(INPUT_GET, "noteid");
if (isset($noteid)) {
    MysqlConnection::delete("DELETE FROM `customer_notes` WHERE `id` = '$noteid' ");
    header("location:index.php?pagename=note_customermaster&customerId=$customerid");
}
?>
<style>
    .widget-box input{
        background-color: white;
        background: white;
    }
    .widget-box textarea{
        background-color: white;
        background: white;
    }
    input, textarea{
        width: 90%;
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>
<form name="frmCustomerNote" id="frmCustomerNote" method="post">
    <div class="container-fluid" id="tabs">
        <div class="cutomheader">
            <h5 style="font-family: verdana;font-size: 12px;">ADD CUSTOMER NOTE</h5>
        </div>
        <div class="widget-box" >
            <div class="widget-title" >
                <ul class="nav nav-tabs" >
                    <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Company Information </a></li>
                </ul>
            </div>
            <div class="widget-content tab-content">
                <div id="tab1" class="tab-pane active">
                    <table  style="width: 100%;vertical-align: top" border="0"  class="display nowrap sortable">
                        <tr>
                            <td><label class="control-label">First Name</label></td>
                            <td><input type="text" readonly="" value="<?php echo $customer["firstname"] ?>" /></td>
                            <td><label class="control-label">Last Name</label></td>
                            <td><input type="text" readonly="" value="<?php echo $customer["lastname"] ?>" /></td>
                            <td><label class="control-label">Company Name</label></td>
                            <td><input type="text"  value="<?php echo $customer["cust_companyname"] ?>" readonly=""/></td>
                        </tr>
                        <?php if ($type != "notedisplay") { ?>
                            <tr style="vertical-align: middle">
                                <td><label class="control-label">Remind me on &nbsp;</label></td>
                                <td>  

                                    <input  style="width: 220px" type="text"  name="reminderdate"  id="datepicker" class="datepicker" >

                                <td><label class="control-label">Note / Comment</label></td>
                                <td colspan="3">
                                    <textarea autofocus="" required="" name="note" style="height: 70px;line-height: 18px;overflow: auto;resize: none;width: 97.5%"></textarea>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="6">
                                <div style="height:  180px;overflow: auto;background: white;width: 100%;float: right">
                                    <table  style="width: 100%;vertical-align: top" border="0">
                                        <tr style="height: 30px;background-color: rgb(240,240,240);">
                                            <th style="width: 15px;">#</th>
                                            <th style="width: 100px;">&nbsp;DATE</th>
                                            <th>&nbsp;LAST NOTES</th>
                                            <th style="width:100px;">REMIND ON</th>
                                        </tr>
                                        <?php
                                        foreach ($arrcustomernote as $key => $value) {
                                            ?>
                                            <tr style="border-bottom: solid 1px rgb(220,220,220);vertical-align: top;padding: 3px;">
                                                <td style="vertical-align: middle;width: 15px;" >
                                                    <a onclick="return confirm('Do you want to delete this note ??')" href="index.php?pagename=note_customermaster&customerId=<?php echo $customerid ?>&noteid=<?php echo $value["id"] ?>">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <p style="padding: 3px;">
                                                        <?php
                                                        $explod = explode(" ", $value["adddate"]);
                                                        echo MysqlConnection::convertToPreferenceDate($explod[0]);
                                                        ?>
                                                    </p>
                                                </td>
                                                <td><p style="padding: 3px;text-align: justify"><?php echo $value["note"] ?></p></td>

                                                <td><?php echo $value["reminderdate"] ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>  
            <div class="modal-footer " style="text-align: center">
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SAVE</button>
                <a href="index.php?pagename=manage_customermaster&status=active" class="btn btn-info">CANCEL</a>
            </div> 
        </div>
    </div>
</form>