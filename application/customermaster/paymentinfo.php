<?php
if (!empty($customerid)) {
    $customerpaymentarray = MysqlConnection::fetchCustom("SELECT * FROM customer_payment WHERE cust_id = '$customerid'");
}
?>
<script>
    function chkNumericKey(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode >= 48 && charCode <= 57) || charCode === 46 || charCode === 45) {
            return true;
        } else {
            return false;
        }
    }
    $(document).ready(function($) {
        $("#cardnumber").mask("9999-9999-9999-9999");
    });
</script>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>

<table class="display nowrap sortable" style="width: 100%;vertical-align: top" border="0" id="payment"> 
    <tr>
        <td><label class="control-label">Account No</label></td>
        <td><input style="width: 220px" type="text" name="cust_accnt_no" onkeypress="return chkNumericKey(event)" autofocus="" value="<?php echo $customer["cust_accnt_no"] ?>" minlenght="2" maxlength="13" id="cust_accnt_no"></td>

        <td><label class="control-label">Currency</label></td>
        <td>
            <select style="width: 225px" name="currency"  id="currency" value="" style="text-transform: capitalize">
                <?php
                $currencyarr = getcurrency();
                foreach ($currencyarr as $key => $cvalue) {
                    ?>
                    <option value="<?php echo $key ?>" <?php echo $customer["currency"] == $key ? "selected" : "" ?> ><?php echo $cvalue ?></option>
                    <?php
                }
                ?>
            </select>
        </td>

        <td><label class="control-label">Balance</label></td>
        <td><input style="width: 220px" type="text" name="balance" onkeypress="return chkNumericKey(event)" minlenght="2" maxlength="50" id="balance"  value="<?php echo $customer["balance"] ?>" ></td>
        <td><label class="control-label">Credit Limit</label></td>
        <td><input style="width: 220px" type="text" name="creditlimit" onkeypress="return chkNumericKey(event)" autofocus="" value="<?php echo $customer["creditlimit"] ?>" minlenght="2" maxlength="" id="creditlimit"></td>
    </tr>
    <?php
    if (count($customerpaymentarray) != 0) {
        $index = 1;
        foreach ($customerpaymentarray as $key => $value) {
            ?>
            <tr>
                <td><label class="control-label">Credit Card No.</label></td>
                <td><input style="width: 220px;" type="text" name="cardnumber[]" onkeypress="return chkNumericKey(event)" id="cardnumber" minlenght="2" maxlength="13"  value="<?php echo $value["cardnumber"] ?>" ></td>
                <td><label class="control-label">Name on Card</label></td>
                <td><input style="width: 220px;" type="text" name="nameoncard[]" id="nameoncard"  minlenght="2" maxlength="30"  value="<?php echo $value["nameoncard"] ?>" ></td>
                <td><label class="control-label">Exp. Date</label></td>
                <td><input style="width: 220px" type="text" name="expdate[]" id="datepicker"  value="<?php echo MysqlConnection::convertToPreferenceDate($value["expdate"]) ?>" ></td>
                <td><label class="control-label">CVV</label></td>
                <td>
                    <input style="width: 40px" type="text" onkeypress="return chkNumericKey(event)" name="cvvno[]" id="cvvno" minlength="2" maxlength="3" value="<?php echo $value["cvvno"] ?>" >
                    <?php
                    if ($index == 1) {
                        echo '<a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-plus" href="#" id="paymentInfoFrm" ></a>';
                    } else {
                        echo '<a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-trash" href="#" id="icon-trash" ></a>';
                    }
                    ?>
                </td>
            </tr>
            <?php
            $index++;
        }
    } else {
        ?>
        <tr>
            <td><label class="control-label">Credit Card No.</label></td>
            <td><input style="width: 220px;" type="text" name="cardnumber[]" onkeypress="return chkNumericKey(event)" id="cardnumber" minlenght="2" maxlength="16"  value="<?php echo $value["cardnumber"] ?>" ></td>
            <td><label class="control-label">Name on Card</label></td>
            <td><input style="width: 220px;" type="text" name="nameoncard[]" id="nameoncard"  minlenght="2" maxlength="30"  value="<?php echo $value["nameoncard"] ?>" ></td>
            <td><label class="control-label">Exp. Date</label></td>
            <td><input style="width: 220px;" type="text" name="expdate[]" id="datepicker"  value="<?php echo $value["expdate"] ?>" ></td>
            <td><label class="control-label">CVV</label></td>
            <td>
                <input style="width: 40px" type="text" onkeypress="return chkNumericKey(event)" name="cvvno[]" id="cvvno" minlength="2" maxlength="3" value="<?php echo $value["cvvno"] ?>" >
                <span  data-toggle="tooltip" data-original-title="Click here to add more card details"> <a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-plus" href="#" id="paymentInfoFrm" ></a></span>
            </td>
        </tr>
    <?php }
    ?>
</table> 

<hr/>
<input type="hidden" value="customerid" value="<?php echo $customerid ?>"/>
<input type="button" id="btnCmpPrev3" value="PREVIOUS" class="btn btn-info" href="#tab1"/>
<button type="submit" onclick="return submitCustomer()" id="btnSubmitFullForm" class="btn btn-info">SAVE</button>
<a href="index.php?pagename=manage_customermaster&status=active" class="btn btn-info">CANCEL</a>
<script type="text/javascript">
    jQuery(function() {
        var counter = 1;
        jQuery('#paymentInfoFrm').click(function(event) {
            event.preventDefault();
            var newRow = jQuery('<tr><td><label class="control-label">Credit Card No.</label></td><td><input style="width: 220px;" type="text" name="cardnumber[]" onkeypress="return chkNumericKey(event)" minlenght="2" maxlength="16" id="cardnumber' + counter + '"></td>' +
                    counter + '<td><label class="control-label">Name on Card</label></td><td><input style="width: 220px;" type="text" name="nameoncard[]" minlenght="2" maxlength="30"  id="nameoncard"></td>' +
                    counter + '<td><label class="control-label">Exp. Date</label></td><td><input style="width: 220px;" type="text" name="expdate[]" id="datepicker1"></td>' +
                    counter + '<td><label class="control-label">CVV</label></td><td><input style="width: 40px" type="text" onkeypress="return chkNumericKey(event)" minlenght="2" maxlength="3" name="cvvno[]" id="cvvno">\n\
                                <a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-trash" href="#"  ></a></td>');
            $("#cardnumber" + counter).mask("9999-9999-9999-9999");
            counter++;
            jQuery('#payment').append(newRow);
        });
    });
    $(document).ready(function() {

        $("#payment").on('click', 'a.icon-trash', function() {
            $(this).closest('tr').remove();
        });
    });</script>

<script>
    function submitCustomer() {
        var cust_companyname = document.getElementById('cust_companyname').value;
        var firstname = document.getElementById('firstname').value;
        var cust_email = document.getElementById('cust_email').value;
        var phno = document.getElementById('phno').value;
        var streetNo = document.getElementById('streetNo').value;
        var city = document.getElementById('city').value;
        var cust_province = document.getElementById('cust_province').value;
        var postal_code = document.getElementById('postal_code').value;
        var country = document.getElementById('country').value;
  
        

        //
        var cust_type = document.getElementById('cust_type').value;

        if (cust_companyname === "" || firstname === "" || cust_email === "" || phno === "" || streetNo === "" ||
                city === "" || cust_province === "" || postal_code === "" || country === "") {
            $('#ciTab1').addClass('active');
            $('#tab1').addClass('active');
            $('#tab4').removeClass('active');
            $('#dpiTab4').removeClass('active');

        } else if (cust_type === "") {
            $('#tdTab3').addClass('active');
            $('#tab3').addClass('active');
            $('#tab4').removeClass('active');
            $('#dpiTab4').removeClass('active');
        } else {
            document.getElementById("frmCustomerSubmit").action = "customermaster/savecustomermaster_ajax.php";
            $("#frmCustomerSubmit").submit();
            return true;
        }
    }
</script>