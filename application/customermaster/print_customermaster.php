<?php
include '../MysqlConnection.php';
error_reporting(0);
$customerid = filter_input(INPUT_GET, "customerid");
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
//$pdfheader = "RoundWrap Industries 1680 Savage Rd, Richmond, BC V6V 3A9, Canada <br/><b>Phone:</b> +1 604-278-1002";

$customer = MysqlConnection::getCustomerDetails($customerid);
$customercontactdetails = MysqlConnection::getCustomerContactDetails($customerid);
?>

<script type="text/javascript">
<!--
    window.print();
//-->
</script>
<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0;  }
    #items th { background: #eee; }
    table td, table th {  padding: 5px; }

    end_last_page div
    {
        height: 27mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<style>
    *{
        padding: 0px;
        margin: 5px;
    }
    table {
        border-collapse: collapse;
        width: 100%;
    }
    table tr{
        height: 40px;
    }

    .widget-box input{
        background-color: white;
        background: white;
    }
    .widget-box textarea{
        background-color: white;
        background: white;
    }


</style> 
<page style="font-size: 16pt" pageset="old">
    <table style="width: 100%; ">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
        </tr>

    </table>
    <hr style="border: solid .2px;"/>
    <table style="width: 100%;height: 100%;background-color: white;" border="0">
        <tbody>
            <tr >
                <td>
                    <div id="content">
                        <div class="cutomheader">
                            <h5 style="font-family: verdana;font-size: 12px;">CUSTOMER INFORMATION</h5>
                        </div>
                        <div class="widget-box">
                            <div class="widget-content tab-content">
                                <div id="tab1" class="tab-pane active">
                                    <table class="display nowrap sortable table-bordered" style="width: 100%;vertical-align: top;background-color: white;text-align: left" border="1">
                                        <tbody>
                                            <tr>
                                                <td><label class="control-label"><b>First Name</b></label></td>
                                                <td><?php echo $customer["firstname"] ?> </td>
                                                <td><label class="control-label"><b>Last Name</b></label></td>
                                                <td><?php echo $customer["lastname"] ?> </td>
                                                <td><label class="control-label"><b>Company Name</b></label></td>
                                                <td><?php echo $customer["cust_companyname"] ?> </td>
                                            </tr>
                                            <tr>
                                                <td><label class="control-label"><b>Email</b></label></td>
                                                <td><?php echo $customer["cust_email"] ?></td>
                                                <td><label class="control-label"><b>Mobile Number</b></label></td>
                                                <td><?php echo $customer["mobileno"] ?></td>
                                                <td><label class="control-label"><b>Phone Number</b></label></td>
                                                <td><?php echo $customer["phno"] ?></td>
                                            </tr>
                                            <tr>
                                                <td><label class="control-label"><b>Fax Number</b></label></td>
                                                <td><?php echo $customer["cust_fax"] ?></td>
                                                <td><label class="control-label"><b>Web Site</b></label></td>
                                                <td><?php echo $customer["website"] ?></td>
                                                <td><label class="control-label"><b>Customer Status</b></label></td>
                                                <td style="vertical-align: middle">
                                                    <?php echo $customer["status"] ?>
                                                </td>
                                            </tr>
                                            <tr style="vertical-align: top">
                                                <td><label class="control-label"><b>Bill To</b></label></td>
                                                <td colspan="5" style="text-transform: capitalize"><?php echo $customer["billto"] ?></td>

                                            </tr>
                                            <tr>
                                                <td><label class="control-label"><b>Ship To</b></label></td>
                                                <td colspan="5" style="text-transform: capitalize"><?php echo $customer["shipto"] ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <br/>
                                <div class="cutomheader">
                                    <h5 style="font-family: verdana;font-size: 12px;">ADDITIONAL CONTACTS</h5>
                                </div>
                                <hr/>
                                <div class="tab-pane ">
                                    <table class="display nowrap sortable table-bordered" id="addcontacts" border="0" style="width: 100%;background-color: white"> 
                                        <tbody>
                                            <tr style="background-color: #A9CDEC;color: black">
                                                <td><b>Name</b></td>
                                                <td><b>Email</b></td>
                                                <td><b>Mobile No</b></td>
                                                <td><b>Phone No</b></td>
                                                <td><b>Designation</b></td>
                                            </tr>
                                            <?php
                                            foreach ($customercontactdetails as $key => $value) {
                                                $custid = $value1["cust_id"];
                                                $items = MysqlConnection::getItemDataById($itemid);
                                                if ($k % 2 == 0) {
                                                    $back = "rgb(240,240,240)";
                                                } else {
                                                    $back = "white";
                                                }
                                                ?>
                                                <tr style="vertical-align: bottom">
                                                    <td><?php echo $value["person_name"] ?></td>
                                                    <td><?php echo $value["person_email"] ?></td>
                                                    <td><?php echo $value["mobileno"] ?></td>
                                                    <td><?php echo $value["person_phoneNo"] ?></td>
                                                    <td><?php echo $value["designation"] ?></td>

                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</page>

