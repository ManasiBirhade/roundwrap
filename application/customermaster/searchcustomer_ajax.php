<?php

error_reporting(0);
include '../MysqlConnection.php';

$companyname = trim($_POST["companyname"]);
if ($companyname != "") {
    $details = MysqlConnection::fetchCustom("SELECT * FROM `customer_master` WHERE `cust_companyname` = '$companyname' OR `id` = '$companyname' ");

    $detailstax = MysqlConnection::getTaxInfoById($details[0]["taxInformation"]);
    $details[0]["taxid"] = $detailstax["id"];
    $details[0]["taxname"] = $detailstax["taxname"];
    $details[0]["taxValue"] = $detailstax["taxvalues"];

    $shipdetailstax = MysqlConnection::getTaxInfoById($details[0]["shippingtax"]);
    $details[0]["staxid"] = $shipdetailstax["id"];
    $details[0]["staxname"] = $shipdetailstax["taxname"];
    $details[0]["staxpercent"] = $shipdetailstax["taxvalues"];


    $detailssalesperson = MysqlConnection::getGenericById($details[0]["sales_person_name"]);
    $details[0]["sales_person_name"] = $detailssalesperson["name"] . " " . $detailssalesperson["description"];


    if (empty($details[0])) {
        echo json_encode(array());
    } else {
        echo json_encode($details[0]);
    }
} else {
    echo json_encode(array());
}
