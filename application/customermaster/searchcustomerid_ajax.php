<?php

include '../MysqlConnection.php';

$companyname = trim($_POST["companyname"]);
if ($companyname != "") {
    $customer = MysqlConnection::getCustomerDetails($companyname);
    $tax = MysqlConnection::getTaxInfoById($customer["taxInformation"]);
    $shipdetailstax = MysqlConnection::getTaxInfoById($customer["shippingtax"]);


    foreach ($tax as $key => $value) {
        $customer[$key] = $value;
    }

    $customer["staxname"] = $shipdetailstax["taxname"];
    $customer["staxpercent"] = $shipdetailstax["taxvalues"];

    if (empty($customer)) {
        echo json_encode(array($customer));
    } else {
        echo json_encode($customer);
    }
} else {
    echo json_encode(array());
}
