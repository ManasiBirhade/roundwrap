<style>
    select{
        text-transform: uppercase;
    }
</style>


<?php
$sqlcustomertypepredata = MysqlConnection::fetchCustom("SELECT id,name FROM generic_entry where type = 'customer_type' ORDER BY id DESC ;");
$sqlpaymenttermdata = MysqlConnection::fetchCustom("SELECT id,name,code FROM generic_entry where type = 'paymentterm' ORDER BY id DESC ;");
$sqlrepresentativetermdata = MysqlConnection::fetchCustom("SELECT id,name,code, description FROM generic_entry where type = 'representative' ORDER BY id DESC ;");
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY id DESC ;");
?>

<table  class="display nowrap sortable" style="width: 100%;vertical-align: top" border="0"> 
    <tr>
        <td><label class="control-label">Customer Type<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td>
            <span  data-toggle="tooltip" data-original-title="Select Customer Type in the list">
            <select  style="width: 225px" name="cust_type" id="cust_type" required="" > 
                <option value="">&nbsp;&nbsp;</option>
                <option value="0"title="Click here to add new customer type"><< ADD NEW >></option>
                <?php foreach ($sqlcustomertypepredata as $key => $value) { ?>
                    <option <?php echo $value["id"] == $customer["cust_type"] ? "selected" : "" ?>
                        value="<?php echo $value["id"] ?>"  ><?php echo $value["name"] ?></option>
                    <?php } ?>
            </select></span>
        </td>
        <td><label class="control-label">Discount %</label></td>
        <td>
            <input type="text" name="discount" id="discount"  style="width: 225px" value="<?php echo $customer["discount"] ?>" onkeypress="return chkNumericKey(event)" maxlength="2">
        </td>
        <td><label class="control-label">Term</label></td>
        <td>
            <span  data-toggle="tooltip" data-original-title="Select payment terms in the list">
                <select style="width: 225px"  name="paymentTerm" id="paymentTerm" style="margin-top: 5px;">
                    <option value="">&nbsp;&nbsp;</option>
                    <option value="0" title="Click here to add new payment terms" ><< ADD NEW >></option>
                    <?php foreach ($sqlpaymenttermdata as $key => $value) { ?>
                        <option <?php echo $value["id"] == $customer["paymentterm"] ? "selected" : "" ?>
                            value="<?php echo $value["id"] ?>"><?php echo $value["code"] ?> - <?php echo $value["name"] ?></option>
                        <?php } ?>
                </select></span>
        </td>
    </tr>
    <tr>
        <td><label class="control-label">Representative</label></td>
        <td>
            <span  data-toggle="tooltip" data-original-title="Select Representative in the list">
                <select style="width: 225px" name="sales_person_name" id="sales_person_name">
                    <option value="">&nbsp;&nbsp;</option>
                    <option value="0" title="Click here to add new reprentative"><< ADD NEW >></option>
                    <?php
                    foreach ($sqlrepresentativetermdata as $key => $value) {
                        echo $value["id"];
                        echo "<br/>";
                        echo $customer["sales_person_name"];
                        ?>
                        <option <?php echo trim($value["id"]) == trim($customer["sales_person_name"]) ? "selected" : "" ?>
                            value="<?php echo $value["id"] ?>">
                                <?php echo $value["name"] ?> <?php echo $value["description"] ?>
                        </option>  
                        <?php
                    }
                    ?>
                </select></span>
        </td>
        <td><label class="control-label"> Good Tax</label></td>
        <td>
            <span  data-toggle="tooltip" data-original-title="Select goods tax in the list">
                <select style="width: 225px" name="taxInformation" id="taxInformation">
                    <option value="">&nbsp;&nbsp;</option>
                    <option value="0" ><< ADD NEW >></option>
                    <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                        <option   <?php echo $value["id"] == $customer["taxInformation"] ? "selected" : "" ?>
                            value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                        <?php } ?>
                </select></span>
        </td>
        <td><label class="control-label"> Shipping Tax</label></td>
        <td>
               <span  data-toggle="tooltip" data-original-title="Select Shipping Tax in the list">
            <select style="width: 225px" name="shippingtax" id="shippingtax">
                <option value="">&nbsp;&nbsp;</option>
                <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                    <option   <?php echo $value["id"] == $customer["shippingtax"] ? "selected" : "" ?>
                        value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                    <?php } ?>
            </select></span>
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
        <td><input  type="checkbox" name="isExempt1" id="isExempt1" <?php echo $customer["isExempt"] == "Y" ? "checked" : "" ?> >&nbsp;Is Exempt?</td>
        <td><label class="control-label">Business No</label></td>
        <td>
            <input style="width: 220px" type="text" name="businessno" minlenght="2" maxlength="20" id="businessno"  value="<?php echo $customer["businessno"] ?>" >
        </td>
        <td><label class="control-label">Certificate</label></td>
        <td>
            <input style="width: 220px" type="file" name="certificate" id="certificate">
            <?php
            if ($customer["certificate"] != "") {
                ?>
                <br/>
                <a href="" target="_blank">view</a>
                <?php
            }
            ?>
        </td>
    </tr>

</table>

<hr/>
<input type="button" id="btnCmpPrev2" value="PREVIOUS" class="btn btn-info" href="#tab2">
<?php
if (isset($customerid) && $customerid != "") {
    ?>
    <button type="submit" onclick="return submitCustomer()" id="btnSubmitFullForm" class="btn btn-info">SAVE</button>      
    <?php
}
?>
<input type="button" id="btnCmpNext3" value="NEXT" class="btn btn-info" href="#tab4">

<!-- this is custom model dialog --->
<div id="custtypemodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD NEW CUSTOMER TYPE</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="cust_type" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable"> 
                <tr>
                    <td>CUSTOMER&nbsp;TYPE</td>
                    <td>&nbsp;:&nbsp</td>
                    <td><input type="text" autofocus="" required="true" maxlength="30" minlength="5" name="cuscusttype" id="cuscusttype"></td>
                </tr>
                <tr>
                    <td>DESCRIPTION</td>
                    <td>&nbsp;:&nbsp</td>
                    <td><input type="text" name="adddescription" id="adddescription"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="modal-footer">
        <center>
            <a id="saveCustomerType" data-dismiss="modal" class="btn btn-info">SAVE </a> 
            <a data-dismiss="modal" class="btn btn-info" id="cancelct" href="#">CANCEL</a> 
        </center>
    </div>
</div>
<!-- this is model dialog --->

<!-- this is custom model dialog --->
<div id="addRepresentative" class="modal hide" style="top: 10%;left: 50%;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD NEW REPRESENTATIVE TYPE</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="addRepresentative" id="addRepresentative" novalidate="novalidate">
            <div class="control-group">
                <label class="control-label">First Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp</label>
                <div class="controls">
                    <input type="text" name="firstname" id="firstname1">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Last Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp</label>
                <div class="controls">
                    <input type="text" name="lastname" id="lastname1">
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer"> 
        <center>
            <a id="saveRepresentative" data-dismiss="modal" class="btn btn-info">SAVE</a> 
            <a data-dismiss="modal" id="cancelr" class="btn btn-info" href="#">CANCEL</a> 
        </center>
    </div>
</div>
<!-- this is model dialog --->

<!-- this is custom model dialog --->
<div id="addPaymentTerm" class="modal hide" style="top: 10%;left: 50%;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD PAYMENT TERM</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="addPaymentT" id="addPaymentT" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%;vertical-align: top">
                <tr style="height: 35px">
                    <td style="width: 100px;">Code</td>
                    <td>&nbsp;:&nbsp</td>
                    <td><input type="text" autofocus="" required="" minlength="3" maxlength="15" name="termcode" id="termcode" style="width: 40%"></td>
                    <td></td>
                </tr>
                <tr style="height: 35px">
                    <td>Term</td>
                    <td>&nbsp;:&nbsp</td>
                    <td><input type="text" name="termname" id="termname"  minlength="3" maxlength="30"  style="width: 90%"></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>&nbsp;:&nbsp</td>
                    <td colspan="2">
                        <textarea  name="termdescription" id="termdescription" style="resize: none;height: 50px;width: 90%"></textarea>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <div class="modal-footer" style="text-align: center"> 
        <a id="savePaymentT" data-dismiss="modal" class="btn btn-info">SAVE</a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelpt" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->
<!-- this is custom model dialog --->
<div id="addTaxInformation" class="modal hide" style="top: 10%;left: 50%;">
    <div class="modal-header" style="text-align: center">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD TAX INFORMATION</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="addTaxInformation" id="addTaxInformation" novalidate="novalidate">
            <div class="control-group">
                <table class="display nowrap sortable" id="addtax" style="width: 100%">
                    <tr>
                        <td>Code</td>
                        <td>Tax Name</td>
                        <td>Percent</td>
                        <td>Exempt</td>
                        <td></td>
                    </tr>
                    <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                        <tr>
                            <td><input type="text" name="taxcode[]" autofocus=""   style="width: 25px;" id="taxtaxname[]" value="<?php echo $value["taxcode"] ?>"></td>
                            <td><input type="text" name="taxtaxname[]" style="width: 75px;" id="taxtaxname[]" value="<?php echo $value["taxname"] ?>"></td>
                            <td><input type="text" name="taxtaxvalues[]"  id="taxtaxvalues[]" value="<?php echo $value["taxvalues"] ?>"></td>
                            <td><input type="checkbox" name="taxgstexempt[]" id="taxgstexempt[]" value="<?php echo $value["isExempt"] ?>"></td>
                            <td></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td><input type="text" name="taxcode[]" style="width: 25px;" id="taxtaxname[]" ></td>
                        <td><input type="text" name="taxtaxname[]" style="width: 75px;" id="taxtaxname[]" ></td>
                        <td><input type="text" name="taxtaxvalues[]" id="taxtaxvalues[]" ></td>
                        <td><input  type="checkbox" name="taxisExempt[]" id="taxisExempt[]"></td>
                        <td><a class="icon-plus" href="#"  ></a></td>
                    </tr>
                </table>
            </div>
        </form>
    </div>

    <div class="modal-footer" style="text-align: center"> 
        <a id="saveTaxInformation" data-dismiss="modal" class="btn btn-info"">SAVE</a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelti" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->
<script src="index.js"></script>

<script>
        jQuery(function() {
            var counter = 1;
            jQuery('a.icon-plus').click(function(event) {
                event.preventDefault();
                var newRow = jQuery('<tr>'
                        + '<td><input type="text" name="taxcode[]" style="width: 25px;" id="taxtaxname[]" ></td>'
                        + '<td><input type="text" name="taxtaxname[]" style="width: 75px;" id="taxtaxname[]" ></td>'
                        + '<td><input type="text" name="taxtaxvalues[]" id="taxtaxvalues[]" ></td>'
                        + '<td><input  type="checkbox" name="taxisExempt[]" id="taxisExempt[]"></td>'
                        + '<td><a class="icon-trash" href="#"  ></a></td>'
                        + '</tr>');
                counter++;
                jQuery('#addtax').append(newRow);
            });
        });

        $(document).ready(function() {
            $("#addtax").on('click', 'a.icon-trash', function() {
                $(this).closest('tr').remove();
            });
            $('#taxcheckbox').click('#taxInformation', toggleSelection);
        });
        $("#cust_type").click(function() {
            var valueModel = $("#cust_type").val();
            if (valueModel === "0") {
                $('#custtypemodel').modal('show');
            }
        });
        $("#saveCustomerType").click(function() {
            var type = "customer_type";
            var name = $("#cuscusttype").val();
            var description = $("#adddescription").val();

            var dataString = "type=" + type + "&name=" + name + "&description=" + description;
            $.ajax({
                type: 'POST',
                url: 'customermaster/savecustomertype_ajax.php',
                data: dataString
            }).done(function(data) {
                $('#cust_type').append(data);
                $("#cuscusttype").val("");
                $("#adddescription").val("");
            }).fail(function() {
            });
        });

        $("#sales_person_name").click(function() {
            var valueModel = $("#sales_person_name").val();
            if (valueModel === "0") {
                $('#addRepresentative').modal('show');
            }
        });
        $("#saveRepresentative").click(function() {
            var type = "representative";
            var firstname = $("#firstname1").val();
            var lastname = $("#lastname1").val();
            var dataString = "type=" + type + "&name=" + firstname + "&description=" + lastname;
            $("div#divLoading").addClass('show');
            $.ajax({
                type: 'POST',
                url: 'customermaster/saverepresentative_ajax.php',
                data: dataString
            }).done(function(data) {
                $('#sales_person_name').append(data);
                $("#firstname1").val("");
                $("#lastname1").val("");
            }).fail(function() {
            });
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        });

        $("#paymentTerm").click(function() {
            var valueModel = $("#paymentTerm").val();
            if (valueModel === "0") {
                $('#addPaymentTerm').modal('show');
            }
        });
        $("#savePaymentT").click(function() {
            var type = "paymentterm";
            var code = $("#termcode").val();
            var name = $("#termname").val();
            var description = $("#termdescription").val();
            $("div#divLoading").addClass('show');
            var dataString = "type=" + type + "&name=" + name + "&description=" + description + "&code=" + code;
            $.ajax({
                type: 'POST',
                url: 'customermaster/savepayterm_ajax.php',
                data: dataString
            }).done(function(data) {
                $('#paymentTerm').append(data);
                $("#termcode").val("");
                $("#termname").val("");
                $("#termdescription").val("");
                $("div#divLoading").addClass('show');
                $("div#divLoading").removeClass('show');
            }).fail(function() {
            });
        });

        $("#taxInformation").click(function() {
            var valueModel = $("#taxInformation").val();
            if (valueModel === "0") {
                $('#addTaxInformation').modal('show');
            }
        });
        $("#saveTaxInformation").click(function() {
            var taxcode = $("input[name='taxcode[]']").map(function() {
                return $(this).val();
            }).get();
            var taxtaxname = $("input[name='taxtaxname[]']").map(function() {
                return $(this).val();
            }).get();
            var taxtaxvalues = $("input[name='taxtaxvalues[]']").map(function() {
                return $(this).val();
            }).get();
            var taxisExempt = $("input[name='taxisExempt[]']").map(function() {
                return $(this).val();
            }).get();
            var dataString = "taxcode=" + taxcode + "&taxtaxname=" + taxtaxname + "&taxtaxvalues=" + taxtaxvalues + "&taxisExempt=" + taxisExempt;
            $("div#divLoading").addClass('show');
            $.ajax({
                type: 'POST',
                url: 'customermaster/savetaxinfo_ajax.php',
                data: dataString
            }).done(function(data) {
                $("input[name='taxcode[]']").val("");
                $("input[name='taxtaxname[]']").val("");
                $("input[name='taxtaxvalues[]']").val("");
                $("input[name='taxisExempt[]']").val("");
                $('#taxInformation').append(data);
                $("div#divLoading").addClass('show');
                $("div#divLoading").removeClass('show');
            }).fail(function() {
            });
        });

        $(function() {
            $('#taxcheckbox').click('#taxInformation', toggleSelection);
        });

        function toggleSelection(element) {
            if (this.checked) {
                $(element.data).removeAttr('disabled');
            } else {
                $(element.data).attr('disabled', 'desabled')
            }
        }

        //reset index
        $("#cancelct").click(function() {
            $("#cust_type").val("");
        });
        $("#cancelr").click(function() {
            $("#sales_person_name").val("");
        });
        $("#cancelpt").click(function() {
            $("#paymentTerm").val("");
        });
        $("#cancelti").click(function() {
            $("#taxInformation").val("");

        });

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip({
                placement: 'bottom'
            });
        });
</script>