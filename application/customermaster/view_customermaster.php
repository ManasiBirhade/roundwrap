<?php
$customerid = filter_input(INPUT_GET, "customerId");
$flag = filter_input(INPUT_GET, "flag");

$customer = MysqlConnection::getCustomerDetails($customerid);
$customercontactarray = MysqlConnection::getCustomerContactDetails($customerid);
$customerpaymentarray = MysqlConnection::fetchCustom("SELECT * FROM  `customer_payment` WHERE  cust_id = '$customerid' ");
$arrcustomernote = MysqlConnection::fetchCustom("SELECT * FROM  `customer_notes` WHERE cust_id = '$customerid'  ORDER BY ID DESC LIMIT 0,20");


if (isset($_POST["deleteItem"])) {
    MysqlConnection::delete("DELETE FROM `customer_payment` WHERE cust_id = '$customerid'");
    MysqlConnection::delete("DELETE FROM `customer_contact` WHERE cust_id = '$customerid'");
    MysqlConnection::delete("DELETE FROM `customer_notes` WHERE cust_id = '$customerid'");
    MysqlConnection::delete("DELETE FROM `customer_master` WHERE id = '$customerid'");
    header("location:index.php?pagename=manage_customermaster&status=active&action=delete");
}
?>
<style>
    .widget-box input{
        background-color: white;
        background: white;
    }
    .widget-box textarea{
        background-color: white;
        background: white;
    }

</style>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">CUSTOMER VIEW</h5>
    </div>

    <?php
    if ($flag == "yes") {
        echo MysqlConnection::$DELETE;
    }
    ?>
    <div class="widget-box" >

        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">COMPANY INFORMATION </a></li>
                <li id="acTab2"><a data-toggle="tab" href="#tab2">ADDITIONAL CONTACTS</a></li>
                <li id="tdTab3"><a data-toggle="tab" href="#tab3">TAX AND DISCOUNT</a></li>
                <li id="dpiTab4"><a data-toggle="tab" href="#tab4">DEPOSITS AND PAYMENT INFORMATION</a></li>
                <li id="noteTab5"><a data-toggle="tab" href="#tab5">NOTES & COMMENTS</a></li>
            </ul>
        </div>
        <div class="widget-content tab-content">
            <div id="tab1" class="tab-pane active">
                <table class="display nowrap sortable table-bordered" style="width: 100%;vertical-align: top;background-color: white" border="0">
                    <tr>
                        <td><label class="control-label"><b>First Name</b></label></td>
                        <td><?php echo $customer["firstname"] ?></td>
                        <td><label class="control-label"><b>Last Name</b></label></td>
                        <td><?php echo $customer["lastname"] ?></td>
                        <td><label class="control-label"><b>Company Name</b></label></td>
                        <td><?php echo $customer["cust_companyname"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Email</b></label></td>
                        <td><?php echo $customer["cust_email"] ?></td>
                        <td><label class="control-label"><b>Mobile Number</b></label></td>
                        <td><?php echo trim($customer["mobcode"]) . " " . trim($customer["mobileno"]) ?></td>
                        <td><label class="control-label"><b>Phone Number</b></label></td>
                        <td><?php echo trim($customer["phoneCode"]) . " " . trim($customer["phno"]) ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Fax Number</b></label></td>
                        <td><?php echo trim($customer["faxCode"]) . " " . $customer["cust_fax"] ?></td>
                        <td><label class="control-label"><b>Web Site</b></label></td>
                        <td><?php echo $customer["website"] ?></td>
                        <td><label class="control-label"><b>Customer Status</b></label></td>
                        <td style="vertical-align: middle">
                            <input type="checkbox"  <?php echo $customer["status"] == "Y" ? "checked" : "" ?>   readonly=""/>
                            Is customer active ?
                        </td>
                    </tr>

                    <tr>
                        <td><label class="control-label"><b>Address</b></label></td>
                        <td><?php echo $customer["streetNo"] ?></td>
                        <td><label class="control-label"><b>City</b></label></td>
                        <td><?php echo $customer["city"] ?></td>
                        <td><label class="control-label"><b>Province</b></label></td>
                        <td><?php echo $customer["cust_province"] ?></td>

                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Postal Code</b></label></td>
                        <td><?php echo $customer["postal_code"] ?></td>
                        <td><label class="control-label"><b>Country</b></label></td>
                        <td><?php echo $customer["country"] ?></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr style="vertical-align: top">
                        <td><b>Bill To</b></td>
                        <td colspan="5" style="text-transform: capitalize"><?php echo $customer["billto"] ?></td>

                    </tr>
                    <tr>
                        <td><b>Ship To</b></td>
                        <td colspan="5" style="text-transform: capitalize"><?php echo $customer["shipto"] ?></td>
                    </tr>

                </table>
                <hr/>
                <?php
                if (isset($flag) && $flag != "") {
                    ?>
                    <form name="frmDeleteItem" id="frmDeleteItem" method="post">
                        <input type="hidden" value="<?php echo $customerid ?>" name="customerid"/>
                        <a href="manage_customermaster&status=active" class="btn btn-info">CANCEL</a>
                        <input type="hidden" value="customerid" value="<?php echo $customerid ?>"/>
                        <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info" style="background-color: #A9CDEC"/>
                        <input type="button" id="btnCmpNext1" value="NEXT" class="btn btn-info"  style="background-color: #A9CDEC" />
                    </form>
                    <?php
                } else {
                    ?>
                    <a href="customermaster/print_customermaster.php?customerid=<?php echo $customerid ?>" target="_blank" class="btn btn-info">PRINT</a>
                    <a href="index.php?index.php?pagename=manage_customermaster&status=active" class="btn btn-info">CANCEL</a>
                    <input type="button" id="btnCmpNext1" value="NEXT" class="btn btn-info"  style="background-color: #A9CDEC"/>
                    <?php
                }
                ?>


            </div>
            <div id="tab2" class="tab-pane " >
                <table class="display nowrap sortable table-bordered" id="addcontacts"  border="0" class="ctable" style="width: 100%;background-color: white"> 
                    <tr style="background-color: #A9CDEC;color: white">
                        <td><b>Name</b></td>
                        <td><b>Email</b></td>
                        <td><b>Mobile No</b></td>
                        <td><b>Phone No</b></td>
                        <td><b>Designation</b></td>
                        <td><b>EMail ??</b></td>
                        <td><b>EMail[Other] ??</b></td>
                    </tr>
                    <?php
                    foreach ($customercontactarray as $key => $value) {
                        ?>
                        <tr style="vertical-align: bottom">
                            <td><?php echo $value["person_name"] == "" ? $customer["cust_companyname"] : $value["person_name"] ?></td>
                            <td><?php echo $value["person_email"] ?></td>
                            <td><?php echo $value["mobcode"] . " " . $value["mobileno"] ?></td>
                            <td><?php echo $value["code"] . " " . $value["person_phoneNo"] ?></td>
                            <td><?php echo $value["designation"] ?></td>
                            <td><?php echo $value["mail"] ?></td>
                            <td><?php echo $value["othermail"] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <hr/>
                <input  style="background-color: #A9CDEC" type="button" id="btnCmpPrev1" value="PREVIOUS" class="btn btn-info" href="#tab1" >
                <input  style="background-color: #A9CDEC" type="button" id="btnCmpNext2" value="NEXT" class="btn btn-info" href="#tab2"  >


            </div>
            <div id="tab3" class="tab-pane">
                <?php
                $customertype = MysqlConnection::getGenericById($customer["cust_type"]);
                $taxinfo = MysqlConnection::getTaxInfoById($customer["taxInformation"]);
                $paymentterm = MysqlConnection::getGenericById($customer["paymentterm"]);
                $representivename = MysqlConnection::getGenericById($customer["sales_person_name"]);
                ?>

                <table class="display nowrap sortable table-bordered" style="width: 100%;vertical-align: top;background-color: white" border="0"> 
                    <tr>
                        <td style="width: 15%"><label class="control-label"><b>Customer Type</b></label></td>
                        <td><?php echo $customertype["name"] ?></td>
                        <td style="width: 15%"><label class="control-label"><b>Discount</b></label></td>
                        <td><?php echo $customer["discount"] ?></td>
                        <td style="width: 15%"><label class="control-label"><b>Term</b></label></td>
                        <td><?php echo $paymentterm["code"] . " " . $paymentterm["name"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Representative</b></label></td>
                        <td><?php echo $representivename["name"] ?> <?php echo $representivename["description"] ?></td>
                        <td><label class="control-label"><b>Business No</b></label></td>
                        <td><?php echo $customer["businessno"] ?></td>
                        <td><label class="control-label"><b>Certificate</b></label></td>
                        <td><a href="<?php echo str_replace("../", "/", $customer["certificate"]) ?>" target="_blank">view</a></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Tax Code</b></label></td>
                        <td><?php echo $taxinfo["taxcode"] . "-" . $taxinfo["taxname"] . "-" . $taxinfo["taxvalues"] ?></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <hr/>
                <input  style="background-color: #A9CDEC"  type="button" id="btnCmpPrev2" value="PREVIOUS" class="btn btn-info" href="#tab2">
                <input  style="background-color: #A9CDEC" type="button" id="btnCmpNext3" value="NEXT" class="btn btn-info" href="#tab4">


            </div>
            <div id="tab4" class="tab-pane">

                <table class="display nowrap sortable  table-bordered"  style="width: 100%;vertical-align: top;background-color: white" border="0" id="payment"> 
                    <tr>
                        <td><label class="control-label"><b>Account No</b></label></td>
                        <td><?php echo $customer["cust_accnt_no"] ?></td>
                        <td><label class="control-label"><b>Currency</b></label></td>
                        <td><?php echo getcurrency($customer["currency"]) . " " . ($customer["currency"]) ?></td>
                        <td><b>Balance</b></td>
                        <td><?php echo $customer["balance"] ?></td>
                        <td><label class="control-label"><b>Credit Limit</b></label></td>
                        <td><?php echo $customer["creditlimit"] ?></td>
                    </tr>
                    <?php foreach ($customerpaymentarray as $key => $value) { ?>
                        <tr>
                            <td><label class="control-label"><b>Credit Card No.</b></label></td>
                            <td><?php echo $value["cardnumber"] ?></td>
                            <td><label class="control-label"><b>Name on Card</b></label></td>
                            <td><?php echo $value["nameoncard"] ?></td>
                            <td><label class="control-label"><b>Exp. Date</b></label></td>
                            <td><?php echo MysqlConnection::convertToPreferenceDate($value["expdate"]) ?></td>
                            <td><label class="control-label"><b>CVV</b></label></td>
                            <td><?php echo $value["cvvno"] ?></td>

                        </tr>
                    <?php } ?>
                </table> 
                <hr/>
                <input type="hidden" value="customerid" value="<?php echo $customerid ?>"/>
                <input  style="background-color: #A9CDEC" type="button" id="btnCmpPrev3" value="PREVIOUS" class="btn btn-info" href="#tab1">
                <input   style="background-color: #A9CDEC" type="button" id="btnCmpNext4" value="NEXT" class="btn btn-info" href="#tab5">

            </div>
            <div id="tab5" class="tab-pane">

                <div style="height:  230px;overflow: auto;background: white;width: 100%;float: right">
                    <table class="display nowrap sortable" style="width: 100%;vertical-align: top" border="0">
                        <tr style="height: 30px;background-color: rgb(240,240,240);">
                            <th style="width: 100px;">&nbsp;DATE</th>
                            <th>&nbsp;LAST NOTES</th>
                        </tr>
                        <?php
                        foreach ($arrcustomernote as $key => $value) {
                            ?>
                            <tr style="border-bottom: solid 1px rgb(220,220,220);">
                                <td>&nbsp;
                                    <?php
                                    $explod = explode(" ", $value["adddate"]);
                                    echo MysqlConnection::convertToPreferenceDate($explod[0]);
                                    ?>
                                </td>
                                <td><p style="padding: 3px;text-align: justify"><?php echo $value["note"] ?></p></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                </div>

                <hr/>
                <input  style="background-color: #A9CDEC" type="button" id="btnCmpPrev4" value="PREVIOUS" class="btn btn-info" href="#tab1">
                <a href="index.php?pagename=manage_customermaster&status=active" class="btn btn-info">CANCEL</a>
            </div>
        </div>  
    </div>
</div>

<script>

    var button_my_button = "#myButton";
    $(button_my_button).click(function() {
        window.location.href = 'Students.html';
    });


    function deleteCustomer(id) {
        var dataString = "deleteId=" + id;
        alert(id);
        $.ajax({
            type: 'POST',
            url: 'customermaster/customermaster_ajax.php',
            data: dataString
        }).done(function(data) {
        }).fail(function() {
        });
    }

    $('#btnCmpNext1').on('click', function() {
        $('#ciTab1').removeClass('active');
        $('#acTab2').addClass('active');

        $('#tab1').removeClass('active');
        $('#tab2').addClass('active');
    });

    $('#btnCmpPrev1').on('click', function() {
        $('#acTab2').removeClass('active');
        $('#ciTab1').addClass('active');
        $('#tab2').removeClass('active');
        $('#tab1').addClass('active');

    });
    $('#btnCmpNext2').on('click', function() {
        $('#acTab2').removeClass('active');
        $('#tdTab3').addClass('active');
        $('#tab2').removeClass('active');
        $('#tab3').addClass('active');
    });


    $('#btnCmpPrev2').on('click', function() {
        $('#tdTab3').removeClass('active');
        $('#acTab2').addClass('active');
        $('#tab3').removeClass('active');
        $('#tab2').addClass('active');
    });
    $('#btnCmpNext3').on('click', function() {
        $('#tdTab3').removeClass('active');
        $('#dpiTab4').addClass('active');
        $('#tab3').removeClass('active');
        $('#tab4').addClass('active');
    });
    $('#btnCmpPrev3').on('click', function() {
        $('#dpiTab4').removeClass('active');
        $('#tdTab3').addClass('active');
        $('#tab4').removeClass('active');
        $('#tab3').addClass('active');
    });

    $('#btnCmpNext4').on('click', function() {
        $('#dpiTab4').removeClass('active');
        $('#noteTab5').addClass('active');
        $('#tab4').removeClass('active');
        $('#tab5').addClass('active');
    });
    $('#btnCmpPrev4').on('click', function() {
        $('#noteTab5').removeClass('active');
        $('#dpiTab4').addClass('active');
        $('#tab5').removeClass('active');
        $('#tab4').addClass('active');
    });
</script>

