<?php
include '../MysqlConnection.php';

$workorderscan = filter_input(INPUT_POST, "workorderscan");

$mysqlresult = MysqlConnection::fetchCustom("SELECT * FROM `packslip` WHERE `outCode` = '$workorderscan' OR inCode = '$workorderscan'   ");

$customerdetail = MysqlConnection::getCustomerDetails($mysqlresult[0]["cust_id"]);

$trackresult = MysqlConnection::fetchCustom("SELECT * FROM `tbl_track_ps` where psid = '" . $mysqlresult[0]["ps_id"] . "' ");

$profiles = MysqlConnection::getPortfolioProfileByProfId($mysqlresult[0]["prof_id"]);

$profilesteps = MysqlConnection::fetchCustom("SELECT * FROM `tbl_profile_scanner_integration` WHERE `profileid` = '" . $profiles["portfolio_name"] . "' ORDER BY `sequence` ASC");
?>
<table id="example" class="display nowrap sortable table-bordered" style="width:40%">
    <tr class="header" style="background-color:  #76323F">
        <td  colspan="3" style="color: white">WORK ORDER TRACK</td>
    </tr>
    <tr>
        <td>Customer Name</td>
        <td><?php echo $customerdetail["cust_companyname"] ?></td>
        <td></td>
    </tr>
    <tr>
        <td>Pack Slip Number</td>
        <td><?php echo $mysqlresult[0]["po_no"] ?></td>
        <td><?php echo $mysqlresult[0]["req_date"] ?></td>
    </tr>
    <tr>
        <td>Quotation Number</td>
        <td><?php echo $mysqlresult[0]["quot_id"] ?></td>
        <td><?php echo $mysqlresult[0]["req_date"] ?></td>
    </tr>
    <tr>
        <td>Work Order Number</td>
        <td><?php echo $mysqlresult[0]["workOrd_Id"] ?></td>
        <td><?php echo $mysqlresult[0]["req_date"] ?></td>
    </tr>

    <?php
    foreach ($trackresult as $value) {
        ?>
        <tr>
            <td colspan="2"><?php echo $value["mode"] ?></td>
            <td><?php echo $value["date"] ?></td>
        </tr>
        <?php
    }
    ?>
    <?php
    $index = 1;
    foreach ($profilesteps as $value) {
        ?>
        <tr>
            <td>Step - <?php echo $index++ ?></td>
            <td><?php echo $value["stepname"] ?></td>
            <td></td>
        </tr>
        <?php
    }
    ?>
</table>
<br/>
