<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$querypvc = "SELECT `id`,`portfolio_name`,`profile_name` FROM `tbl_portfolioprofile` WHERE `portfolio_name` = 'PVC' AND profile_name!='' ORDER BY `profile_name`";
$resultsetpvc = MysqlConnection::fetchCustom($querypvc);
?>

<div id="pvcmodel" class="modal hide" style="top: 26%;left: 42%;width: 50%;height: 60%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>CREATE PACKING SLIP FOR PVC</h3>
    </div>
    <div style="width: 100%;">
        <DIV style="height: 350px;overflow: auto">
            <table id="example" class="itemtable" style="width:100%">
                <thead style="line-height: 13px">
                    <tr >
                        <td style="width: 5%;color: white;line-height: 12px;width: 100px">#</td>
                        <td>Profile Name</td>
                    </tr>
                </thead>
                <tbody >
                    <?php
                    $index1 = 0;
                    foreach ($resultsetpvc as $pvcprofile) {
                        $index1++;
                        ?>
                        <tr>
                            <td><?php echo $index1 ?> </td>
                            <td >
                                <a href="index.php?pagename=create_packingslip&profilefor=<?php echo $pvcprofile["id"] ?>&profilename=<?php echo $pvcprofile["profile_name"] ?>">
                                    <?php echo $pvcprofile["profile_name"] ?>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </DIV>
    </div>

</div>
