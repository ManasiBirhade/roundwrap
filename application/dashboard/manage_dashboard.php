<?php
error_reporting(0);
session_start();
ob_start();

$arraycategory = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'productioncategory' ORDER BY `indexid` ");

$deleteNoteId = filter_input(INPUT_POST, "deleteNoteId");
if ($deleteNoteId != "") {
    MysqlConnection::delete("DELETE FROM quicknote WHERE id = '$deleteNoteId' ");
    header("location:index.php?pagename=manage_dashboard");
}

$btnPackSlipSearch = filter_input(INPUT_POST, "btnPackSlipSearch");

$salesorderid = filter_input(INPUT_GET, "salesorderid");
$poid = $_SESSION["poid"];

$custnote = MysqlConnection::fetchCustom("SELECT * FROM `customer_notes` WHERE `isRead`= 'N' ORDER BY reminderdate LIMIT 0,20");
$todayscustnote = MysqlConnection::fetchCustom(""
                . "SELECT *,(SELECT cust_companyname FROM customer_master WHERE id = cn.cust_id ) as companyname "
                . "FROM `customer_notes` cn WHERE `isRead`= 'N'  AND reminderdate = '" . date("Y-m-d") . "'");

$todaypurch = MysqlConnection::fetchCustom("SELECT "
                . " (SELECT companyname FROM supplier_master WHERE supp_id = po.supplier_id ) as companyname, "
                . " po.id, po.remindnotes , po.reminderdate FROM `purchase_order` po WHERE po.`reminderdate` = '" . date("Y-m-d") . "'");



if (count($todayscustnote) > 0 || count($todaypurch) > 0) {
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#todaysnotecounter').modal('show');
        });
    </script>
    <div id="todaysnotecounter" class="modal hide" style="top: 20%;left: 40%;width: 900px;">
        <div class="modal-header"><h3>Reminder!!!</h3></div>
        <form name="frmDeleteNote" id="frmDeleteNote" method="post">
            <div class="modal-body">
                <h6 style="text-align: center">Customer note reminder</h6>
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 30px">#</th>
                        <th style="width: 30px">READ</th>
                        <th style="width: 150px">CUSTOMER NAME</th>
                        <th>NOTE</th>
                    </tr>
                    <?php
                    $inote = 1;
                    foreach ($todayscustnote as $tnote) {
                        ?>
                        <tr>
                            <td><?php echo $inote++ ?></td>
                            <td><input type="checkbox" name="notecheck[]" value="<?php echo $tnote["id"] ?>" id="notecheck"></td>
                            <td><?php echo $tnote["companyname"] ?></td>
                            <td><?php echo $tnote["note"] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <h6 style="text-align: center">Receiving Purchase order reminder</h6>
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 30px">#</th>
                        <th style="width: 30px">READ</th>
                        <th style="width: 150px">CUSTOMER NAME</th>
                        <th>NOTE</th>
                    </tr>
                    <?php
                    $ipnote = 1;
                    foreach ($todaypurch as $tpnote) {
                        ?>
                        <tr>
                            <td><?php echo $ipnote++ ?></td>
                            <td><input type="checkbox" name="notepcheck[]" value="<?php echo $tpnote["id"] ?>" id="notecheck"></td>
                            <td><?php echo $tpnote["companyname"] ?></td>
                            <td><?php echo $tpnote["remindnotes"] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            <div class="modal-footer"> 
                <input type="button" name="btnReadNote" id="btnReadNote" class="btn btn-info" value="CONFIRM">
                <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">SNOOZE</a> 
            </div>
        </form>
    </div>
    <?php
}
?>
<script src="index.js"></script>
<link href="css/tablelayout.css" rel="stylesheet" type="text/css">
<script src="js/bootstrap.min.js"></script> 
<script src="js/maruti.popover.js"></script>
<script src="index.js"></script>
<script src="js/script.js"></script>
<script>
        $(function() {
            $("#datepicker").datepicker({
                dateFormat: '<?php echo $dateformat ?>',
                minDate: -20,
                showOn: "button",
                buttonImage: "images/calender.png",
                buttonImageOnly: true,
                buttonText: "Select date",
            });
        });

        $("#btnReadNote").click(function() {
            var dataString = $('input:checkbox').serialize();
            $.ajax({
                type: 'POST',
                url: 'customermaster/read_notes.php',
                data: dataString
            }).done(function(data) {
                window.location = "index.php?pagename=manage_dashboard";
            }).fail(function() {
            });
        });


</script>
<style>

    .hr{
        border: solid 2px #2C3E50;
    }

    .quick-actions {
        display: inline-block;
        list-style: none outside none;
        margin: 0px 0 0px;
        text-align: center;
    }

    .quick-actions li a {
        padding: 1px 1px;
    }
    .stat-boxes li, .quick-actions li, .quick-actions-horizontal li {
        background: white;
        border: none;
        /* border: 1px solid #D5D5D5; */
        box-shadow: 0 1px 0 0 #FFFFFF inset, 0 1px 0 rgba(255, 255, 255, 0.4);
        display: inline-block;
        line-height: 20px;
        margin: 0 0px 0px;
        padding: 0 3px;
    }

    .itemtable{
        border-right: solid 1px #F6F6F6;
        border-bottom: solid 1px #F6F6F6;
    }
    .itemtable thead{
        height: 40px;
        /*        background-image: linear-gradient(to bottom right, #5F677C, #9A6651);*/
        border: 0px;

    }
    .itemtable tbody {
        border: solid 1px #F6F6F6;

    }

    .table table-bordered{
        border: solid 1px #F6F6F6;
    }
    .table-bordered{
        border: solid 1px #F6F6F6;
    }

    .table tr td{
        border: solid 1px #F6F6F6;
    }

    .highlight {
        background-color: #fff34d;
        -moz-border-radius: 5px; /* FF1+ */
        -webkit-border-radius: 5px; /* Saf3-4 */
        border-radius: 5px; /* Opera 10.5, IE 9, Saf5, Chrome */
        -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* FF3.5+ */
        -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Saf3.0+, Chrome */
        box-shadow: 0 1px 4px rgba(0, 0, 0, 0.7); /* Opera 10.5+, IE 9.0 */
    }

</style>


<div style="margin: 30px;margin-top: 70px;">
    <div style="float: left;width: 48%;">
        <table class="table table-bordered" style="width: 100%;border-color: #F6F6F6">
            <tr style="text-align: center;width: 100%;background-color: #A9CDEC">
                <td style="height: 25px;"><b style="font-size: 14px;font-weight: bold;color: white">Retail</b></td>
            </tr>
            <tr >
                <td>
                    <ul style="width: 100%" class="quick-actions">
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Add Item To Inventory" > <a href="index.php?pagename=create_itemmaster"> <i class="icon-book"></i>ITEM ENTRY 'F1'</a> </li>
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Create Sales Order"> <a href="index.php?pagename=create_salesorder"> <i class="icon-graph"></i>SO  'F5'</a></li>
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Create Purchase Order"> <a href="index.php?pagename=create_perchaseorder"> <i class="icon-piechart"></i>PO 'F4'</a> </li>
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Create Cash Order"> <a href="index.php?pagename=createcash_salesorder"> <i class="icon-wallet"></i>CASH ORDER 'F9'</a> </li>
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Add Credit Note"> <a href="index.php?pagename=view_retailinvoice"> <i class="icon-survey"></i>CR NOTE </a> </li>
                    </ul>
                </td>
            </tr>
            <tr >
                <td> 
                    <input style="height: 29px;width: 100px;" type="submit" id="itemDetail" name="itemDetail" value="ITEM DETAIL" class="btn btn-info">
                    &nbsp;
                    <input style="height: 29px;width: 100px;" type="submit" id="soDetail" name="soDetail" value="SO DETAIL" class="btn btn-info">
                    &nbsp;
                    <input style="height: 29px;width: 100px;" type="submit" id="poDetail" name="poDetail" value="PO DETAIL" class="btn btn-info">
                    &nbsp;
                    <a href="index.php?pagename=manage_itemmaster&status=active"><input style="height: 29px;width: 100px;" type="submit" value="ITEM LIST" class="btn btn-info"></a> 
                </td>
            </tr>
        </table>
        <br/>
    </div>
    <div style="float: right;width: 48%;" >
        <table class="table table-bordered" style="width: 100%;border-color: #F6F6F6">
            <tr style="text-align: center;width: 100%;;background-color: #A9CDEC">
                <td style="height: 25px;"><b style="font-size: 14px;font-weight: bold;color: white">Production</b></td></tr>
            <tr style="height: 80px;">
                <td>
                    <ul style="width: 100%" class="quick-actions">
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Create Pack Slip" ><a href="index.php?pagename=select_packingslip"> <i class="icon-book"></i>PACK SLIP  'F6'</a> </li>
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Create Quotation"><a href="index.php?pagename=manage_quotation"> <i class="icon-tag"></i>QUOTATION  'F7'</a></li>
                        <li style="width: 15%;float: left" data-toggle="tooltip" data-original-title="Create Work Order"> <a href="index.php?pagename=manage_workorder"> <i class="icon-survey"></i>WORK ORDER  'F8'</a></li>
                    </ul>
                </td>
            </tr>
            <tr  style="height: 85px;">

                <td>
                    <?php foreach ($arraycategory as $key => $value) { ?>
                        <button style="height: 29px;margin-top: 7px;margin-left: 5px;"  id="<?php echo $value["id"] ?>" name="<?php echo $value["id"] ?>" class="btn btn-info"><?php echo strtoupper($value["name"]) ?></button>
                    <?php } ?>
                </td>
            </tr>
            <tr  style="height: 80px;vertical-align: middle">
                <td style="vertical-align: middle">
                    <input style="height: 29px;width: 90px;" type="submit" id="qoDetail" name="qoDetail" value="QUOT DETAIL" class="btn btn-info">
                    &nbsp;
                    <input style="height: 29px;" type="submit"  id="woDetail" name="woDetail" value="PRODUCTION TRACKING" class="btn btn-info">
                </td>
            </tr>
        </table>
        <br/>
        <div style="width: 100%;min-height: 220px;height: auto;background-color: rgb(255,252,191);border:solid 1px rgb(255,249,117);">
            <br/>
            <form name="frmQuickChat" id="frmQuickChat" method="POST">
                <table style="width: 100%">
                    <tr>
                        <td colspan="2">
                            <b style="font-size: 16px;font-weight: bold;padding: 8px;margin-top: 5px;height: 45px;">Quick Note</b>
                        </td>
                    </tr>
                    <tr style="vertical-align: top">
                        <td colspan="2">
                            <div  class="chat-message well" style="height: 200px;overflow: auto;background-color: white;">
                                <table style="width: 100%;" class="table table-bordered"  border="1">
                                    <tr style="height: 25px;border-color: #A9CDEC;background-color: rgb(240,240,240)">
                                        <td style="width: 18px;">#</td>
                                        <td style="width: 18px;">#</td>
                                        <td><b>Note</b></td>
                                        <td style="width: 90px;"><b>Date</b></td>
                                    </tr>
                                    <?php
                                    $indexquicknote = 1;
                                    $iduser = $_SESSION["user"]["user_id"];
                                    $resultsetqc = MysqlConnection::fetchCustom("SELECT * FROM `quicknote` WHERE userid = '$iduser' ORDER BY id DESC LIMIT 0,20");
                                    foreach ($resultsetqc as $key => $value) {
                                        ?>
                                        <tr style="height: 35px;">
                                            <td><?php echo $indexquicknote++ ?></td>
                                            <td>
                                                <a href="#" onclick="return deleteNote('<?php echo $value["id"] ?>')">
                                                    <i class="icon-remove"></i>
                                                </a>
                                            </td>
                                            <td><p><?php echo $value["message"] ?></p></td>
                                            <td><?php echo $value["date"] ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </td>
                    </tr> 
                    <tr  style="vertical-align: top">
                        <td><span class="input-box"><input   type="text" name="msg-box" id="msg-box" style="height: 27px;width: 99%"></span> </td>
                        <td style="width: 80px;"><button class="btn" name="btnQuickChat" id="btnQuickChat" style="background-color: rgb(255,249,117);width: 80px;">SAVE</button></td>
                    </tr>
                </table>
            </form>
            <?php
            if (isset($_POST["btnQuickChat"])) {
                $arrayquickchat = array();
                $arrayquickchat["userid"] = $_SESSION["user"]["user_id"];
                $arrayquickchat["message"] = filter_input(INPUT_POST, "msg-box");
                $arrayquickchat["date"] = date("Y-m-d");
                MysqlConnection::insert("quicknote", $arrayquickchat);
                header("location:index.php");
            }
            ?>
        </div>
    </div>
</div>
<hr/>
<br/>
<div style="clear: both"></div>
<div id="itemmodel" class="modal hide" style="top: 26%;left: 42%;width: 50%;height: 60%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Item Details</h3>
    </div>
    <div style="width: 100%;">
        <form name="frmSearch" id="frmSearch" method="post">
            <table class="table table-bordered"  style="width: 100%;border-color: #F6F6F6;">
                <tr>
                    <td colspan="2" style="height: 12px;vertical-align: middle;width: 100px;"><b>Item Name / Code :</b>
                        <input type="text" name="searchbyitem" placeholder="Enter Item Code/Name" value="<?php echo $searchbyitem ?>"  id="searchbyitem" style="width: 70%;height: 25px;margin: 15px;margin-right: -3px;">

                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table id="example" class="itemtable" style="width:100%;display:inline-block;margin-bottom: 100px">
                            <thead style="line-height: 13px;background-color: #A9CDEC;">
                                <tr >
                                    <td style="width: 29px;color: white;line-height: 12px;width: 100px">#</td>
                                    <td style="width: 519px;color: white;line-height: 12px"><i class="fa fa-fw fa-sort"></i>Item Code</td>
                                    <td style="width: 164px;color: white;line-height: 12px"><i class="fa fa-fw fa-sort"></i>On Hand</td>
                                    <td style="width: 130px;color: white;line-height: 12px"><i class="fa fa-fw fa-sort"></i>On PO</td>
                                    <td style="width: 100px;color: white;line-height: 12px"><i class="fa fa-fw fa-sort"></i>Price</td>
                                </tr>
                            </thead>
                            <tbody id="itemsearchtableid" style="height:300px;display:block;overflow:scroll;">

                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div id="somodel" class="modal hide" style="top: 26%;left: 42%;width: 50%;height: 60%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>SO Details</h3>
    </div>
    <div >
        <form name="frmSearch" id="frmSearch" method="post">
            <table class="table table-bordered" style="width: 100%">
                <tr>
                    <td colspan="2" style="height: 12px;vertical-align: middle;width: 100px;"><b>PO/SO :</b>
                        <input type="text" name="searchbyposo" placeholder="ENTER PO NUMBER/ SO NUMBER/ CUSTOMER NAME" value="<?php echo $searchbyposo ?>" id="searchbyposo" style="width: 70%;height: 25px;margin: 15px;margin-right: -3px;">

                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table id="example" class="itemtable" style="width:100%;display:inline-block;margin-bottom:90%">
                            <thead style="background-color: #A9CDEC;">
                                <tr>
                                    <td style="color: white;line-height: 12px;width: 93px">#</td>
                                    <td style="color: white;line-height: 12px;width: 95px" ><i class="fa fa-fw fa-sort"></i>SO NO</td>
                                    <td style="color: white;line-height: 12px;width: 84px"><i class="fa fa-fw fa-sort"></i>PO No</td>
                                    <td style="color: white;line-height: 12px;width: 365px">Customer Name</td>
                                    <td style="color: white;line-height: 12px;width: 112px">PO IS?</td>
                                    <td style="color: white;line-height: 12px;width: 113px">Delivery Date</td>
                                </tr>
                            </thead>
                            <tbody id="sosearchtableid" style="height:300px;display:block;overflow:scroll;">
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</div>
<div id="pomodel" class="modal hide" style="top: 26%;left: 42%;width: 50%;height: 60%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>PO Details</h3>
    </div>

    <div style="width: 100%;">
        <form name="frmSearch" id="frmSearch" method="post">
            <table class="table table-bordered" style="width: 100%" >

                <tr>
                    <td colspan="2" style="height: 12px;vertical-align: middle;width: 100px;"><b>PO/SO :</b>
                        <input type="text" name="searchbyposopurchase" placeholder="ENTER PO NUMBER/ SO NUMBER/ VENDOR NAME" value="<?php echo $searchbyposopurchase ?>" id="searchbyposopurchase" style="width: 70%;height: 25px;margin: 15px;margin-right: -3px;">

                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table id="example" class="itemtable" style="width:100%;display:inline-block;margin-bottom: 200px">
                            <thead  style="background-color: #A9CDEC;">
                                <tr>
                                    <td style="color: white;line-height: 12px;width: 99px">#</td>
                                    <td style="color: white;line-height: 12px;width: 102px"><i class="fa fa-fw fa-sort"></i>SO NO</td>
                                    <td style="color: white;line-height: 12px;width: 105px"><i class="fa fa-fw fa-sort"></i>PO No</td>
                                    <td style="color: white;line-height: 12px;width: 394px">Vendor Name</td>
                                    <td style="color: white;line-height: 12px;width: 116px">PO IS?</td>
                                    <td style="color: white;line-height: 12px;width: 124px">Entry Date</td>
                                </tr>
                            </thead>
                            <tbody id="posearchtableid" style="height:300px;display:block;overflow:scroll;">

                            </tbody>
                        </table>

                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div id="quotmodel" class="modal hide" style="top: 26%;left: 42%;width: 50%;height: 60%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Quotation Details</h3>
    </div>
    <div style="width: 100%;">
        <form name="frmSearch" id="frmSearch" method="post">
            <table class="table table-bordered" style="width: 100%">
                <tr>
                    <td colspan="2" style="height: 12px;vertical-align: middle;width: 100px;"><b>QuotId / SO :</b>
                        <input type="text" name="searchbyquot" placeholder="ENTER QUOTATION ID / CUSTOMER NAME / SO NUMBER /PO NUMBER" value="<?php echo $searchbyquot ?>" id="searchbyquot" style="width: 70%;height: 25px;margin: 15px;margin-right: -3px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <table id="example" class="itemtable" style="width:100%;display:inline-block;margin-bottom: 100px">
                            <thead style="line-height: 13px ;background-color: #A9CDEC;">
                                <tr >
                                    <td style="width: 10%;color: white;line-height: 13px;width: 117px">#</td>
                                    <td style="width: 15%;color: white;line-height: 13px;width: 110px"><i class="fa fa-fw fa-sort"></i>Quot Id</td>
                                    <td style="width: 20%;color: white;line-height: 13px;width: 465px"><i class="fa fa-fw fa-sort"></i>Company Name</td>
                                    <td style="width: 15%;color: white;line-height: 13px;width: 117px"><i class="fa fa-fw fa-sort"></i>SO</td>
                                    <td style="width: 15%;color: white;line-height: 13px;width: 143px"><i class="fa fa-fw fa-sort"></i>PO</td>
                                </tr>
                            </thead>
                            <tbody id="quotsearchtableid" style="height:300px;display:block;overflow:scroll;">

                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>
<div id="womodel" class="modal hide" style="top: 26%;left: 42%;width: 50%;height: 60%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Work Order Details</h3>
    </div>
    <div style="width: 100%;">
        <form name="frmSearch" id="frmSearch" method="post">
            <table class="table table-bordered" style="width: 100%">

                <tr>
                    <td colspan="3">
                        <table class="table table-bordered" style="width: 100%;overflow: scroll">
                            <tr>
                                <td colspan="2" style="height: 12px;vertical-align: middle;width: 100px;"><b>Work Order Id / SO :</b>
                                    <input type="text" name="searchbywork" placeholder="ENTER WO NUMBER / PO NUMBER/ SO NUMBER/  CUSTOMER NAME" value="<?php echo $searchbywork ?>" id="searchbywork" style="width: 70%;height: 25px;margin: 15px;margin-right: -3px;">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <table id="example" class="itemtable" style="width:100%;display:inline-block;margin-bottom: 100px">
                                        <thead style="line-height: 13px;background-color: #A9CDEC;">
                                            <tr >
                                                <td style="color: white;line-height: 13px;width: 84px">#</td>
                                                <td style="color: white;line-height: 13px;width: 88px"><i class="fa fa-fw fa-sort"></i>WO No</td>
                                                <td style="color: white;line-height: 13px;width: 331px"><i class="fa fa-fw fa-sort"></i>Company Name</td>
                                                <td style="color: white;line-height: 13px;width: 90px"><i class="fa fa-fw fa-sort"></i>SO</td>
                                                <td style="color: white;line-height: 13px;width: 110px"><i class="fa fa-fw fa-sort"></i>PO</td>
                                                <td style="color: white;line-height: 13px;width: 182px"><i class="fa fa-fw fa-sort"></i>STATUS</td>
                                            </tr>
                                        </thead>
                                        <tbody class="itemtable" id="worksearchtableid" style="height:300px;display:block;overflow:scroll;">

                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
        </form>
    </div>
</div>





<script src="dashboard/dashboardjs.js"></script>

<script>
                                                    function deleteNote(id) {
                                                        $('#duplicatecustomer').modal('show');
                                                        $("#deleteNoteId").val(id);
                                                        return false;
                                                    }
</script>
<div id="duplicatecustomer" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header"><h3>ALERT !!!</h3></div>
    <form name="frmDeleteNote" id="frmDeleteNote" method="post">
        <div class="modal-body">
            <h5 style="color: red">Do you really want to delete this note?<br/></h5>
            <input type="hidden" name="deleteNoteId" id="deleteNoteId">
        </div>
        <div class="modal-footer"> 
            <input type="submit" name="btnDeleteNote" id="btnDeleteNote" class="btn btn-info" value="YES">
            <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
        </div>
    </form>
</div>



<!-- all production popup -->
<?php
foreach ($arraycategory as $key => $value) {
    $querypvc = "SELECT `id`,`portfolio_name`,`profile_name` FROM `tbl_portfolioprofile` WHERE `portfolio_name` = '" . urlencode($value["name"]) . "' AND profile_name!='' ORDER BY `profile_name`";
    $resultsetpvc = MysqlConnection::fetchCustom($querypvc);
    ?>
    <div id="<?php echo $value["id"] . "model" ?>" class="modal hide" style="top: 4%;left: 42%;width: 50%;height: 60%; overflow: hidden">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">×</button>
            <h3>CREATE PACKING SLIP</h3>
        </div>
        <div style="width: 100%;">
            <div style="height: 400px;overflow: auto;">
                <table class="table table-bordered" style="width: 100%">
                    <tr>
                        <td>
                            <table id="example" class="itemtable" style="width:100%">
                                <thead style="line-height: 13px;background-color: #A9CDEC;">
                                    <tr >
                                        <td style="width: 10%;color: white;line-height: 13px;">#</td>
                                        <td style="color: white;line-height: 13px;width: 118px"><i class="fa fa-fw fa-sort"></i>Profile Name</td>
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php
                                    $index1 = 0;
                                    foreach ($resultsetpvc as $pvcprofile) {
                                        $index1++;
                                        ?>
                                        <tr>
                                            <td><?php echo $index1 ?> </td>
                                            <td >
                                                <a href="index.php?pagename=create_packingslip&profilefor=<?php echo $pvcprofile["id"] ?>&profilename=<?php echo urlencode($pvcprofile["profile_name"])?>">
                                                    <?php echo $pvcprofile["profile_name"] ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <?php
}
?>
<!-- all production popup -->

<script>
<?php foreach ($arraycategory as $key => $value) { ?>
        $("#<?php echo $value["id"] ?>").click(function() {
            $('#<?php echo $value["id"] . "model" ?>').modal('show');
        });
<?php } ?>
</script>

