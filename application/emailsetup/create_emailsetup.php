<?php
$emailid = filter_input(INPUT_GET, "id");
if (!empty($emailid)) {
    $resultset = MysqlConnection::fetchCustom("SELECT * FROM email_setup WHERE id =  '$emailid'");
    $email = $resultset[0];
    $original = $email["body"];
    $removetag = strip_tags($email["body"]);
}

$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
if (isset($btnSubmitFullForm)) {
    unset($_POST["btnSubmitFullForm"]);
    if (isset($emailid)) {
        $_POST["body"] = str_replace("\n", "<br/>", $_POST["body"]);
        $_POST["body"] = "<html><body><br/><br/>" . $_POST["body"] . "</body></html>";
        MysqlConnection::edit("email_setup", $_POST, " id = '$emailid' ");
    } else {
        $emailid = MysqlConnection::insert("email_setup", $_POST);
    }
    activeDeactive($emailid);
    header("location:index.php?pagename=manage_emailsetup");
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">MANAGE EMAIL SETUP</h5>
    </div>
    <div class="widget-box" >
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Setup Information </a></li>
            </ul>
        </div>
        <form name="frmCmpSubmit"  enctype="multipart/form-data" id="frmCmpSubmit" method="post" >
            <div class="widget-content tab-content">
                
                    <table class="display nowrap sortable" style="width: 80%;vertical-align: top" border="0">
                        <tr>
                            <td><label class="control-label">Host Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input type="text" name="hostname" autofocus="" value="<?php echo $email["hostname"] ?>" required="True" id="cmpName" minlenght="2" maxlength="30"></td>
                            <td><label class="control-label">Port Number<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input type="text" name="portnumber" id="portnumber" maxlength="20" required="True"  value="<?php echo $email["portnumber"] ?>" ></td>
                            <td><label class="control-label">Username<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input type="text" name="username" id="username" minlenght='2' required="True"  maxlength="30"  value="<?php echo trim($email["username"]) ?>" ></td>
                        </tr> 
                        <tr>
                            <td><label class="control-label">Password<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input type="text" name="pswd" id="pswd" maxlength="20" required="True" value="<?php echo trim($email["pswd"]) ?>" ></td>
                            <td><label class="control-label">Send from<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input type="text" name="sendfrom" id="sendfrom" minlenght="2"  maxlength="30" required="True" value="<?php echo $email["sendfrom"] ?>" ></td>
                            <td><label class="control-label">Send From Label<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td><input type="text" name="sendlable" id="sendlable"  minlenght="2" maxlength="30" required="True" value="<?php echo $email["sendlable"] ?>" ></td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Subject<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td colspan="5">
                                <input type="text" style="width: 100%"  name="subjectline" id="subjectline" maxlength="50" value="<?php echo $email["subjectline"] ?>">
                            </td>
                        </tr>
                        <tr>
                            <td><label class="control-label">Body<?php echo MysqlConnection::$REQUIRED ?></label></td>
                            <td colspan="5"><textarea style="width: 100%;height: 150px;line-height: 20px;resize: none" name="body" id="body" required="true" ><?php echo $removetag ?></textarea></td>
                        </tr>
                    </table>
              
                <hr/>
                <input type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm"  class="btn btn-info" value="SAVE"/>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div> 
<?php

function activeDeactive($insert) {
    MysqlConnection::delete("UPDATE `email_setup` SET `active` = 'N' WHERE `id` != '$insert';");
    MysqlConnection::delete("UPDATE `email_setup` SET `active` = 'Y' WHERE `id` = '$insert';");
}
