<?php
$listofemail = MysqlConnection::fetchCustom("SELECT * FROM email_setup");
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">EMAIL SETUP</h5>
    </div>
    <br/>
    <table>
        <tr >
            <td ><a class="btn btn-info"  href="index.php?pagename=create_emailsetup" ><i class="icon icon-list"></i>&nbsp;&nbsp;SETUP EMAIL </a></td>
        </tr>
    </table>
    <br/>

    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr class="header">
                <td  style="text-align: center;width:10px;">#</td>
                <td >Host Name</td>
                <td >Port Number</td>
                <td >User Name</td>
                <td >Send From</td>
                <td style="widtd: 200px">Send From Label</td>
                <td >Subject Line</td>
                <td >Test</td>
                <td >Active</td>
            </tr>
        </thead>
        <tbody style="border-color: #76323F">
            <?php
            $index = 1;
            foreach ($listofemail as $key => $value) {
                if ($index % 2 == 0) {
                    $bg = $even;
                } else {
                    $bg = $odd;
                }
                if ($value["active"] == "Y") {
                    $bg = "background-color: rgb(226,250,234)";
                }
                ?>
                <tr id="<?php echo $value["id"] ?>" style="<?php echo $bg ?>;border-bottom: solid 1px rgb(220,220,220);text-align: left;height: 35px;"  class="context-menu-one">
                    <td style="text-align: center">&nbsp;<?php echo $index ?></td>
                    <td >&nbsp;&nbsp;<?php echo $value["hostname"] ?></td>
                    <td ><?php echo $value["portnumber"] ?></td>
                    <td ><?php echo $value["username"] ?></td>
                    <td ><?php echo $value["sendfrom"] ?></td>
                    <td ><?php echo $value["sendlable"] ?></td>
                    <td ><?php echo $value["subjectline"] ?></td>
                    <td ><a href='index.php?pagename=test_emailsetup&testid=<?php echo $value["id"] ?>'>TEST</a></td>
                    <td ><?php echo $value["active"] ?></td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
</div>
