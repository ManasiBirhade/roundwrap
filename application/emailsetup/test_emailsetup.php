<?php
$testemaildata = MysqlConnection::fetchCustom("SELECT * FROM `email_setup` WHERE `id` = '" . filter_input(INPUT_GET, "testid") . "'");
$email = $testemaildata[0];

if (isset($_POST["btnSubmitFullForm"])) {
    $subjectline = filter_input(INPUT_POST, "subjectline");
    $body = filter_input(INPUT_POST, "body");
    $body = str_replace("\n", "<br/>", $body);
    $body = "<html><body><br/><br/>" . $body . "</body></html>";
    $emailtosend = explode(",", filter_input(INPUT_POST, "username"));
    foreach ($emailtosend as $value) {
        MysqlConnection::sendEmail($value, $attachment = "", $subjectline, $body, "TEST EMAIL");
    }
    header("location:index.php?pagename=manage_emailsetup");
}
?>
<div class="container-fluid" id="tabs">
    <div class="widget-box" >
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">TEST EMAIL SETUP</a></li>
            </ul>
        </div>
        <form name="frmCmpSubmit"  enctype="multipart/form-data" id="frmCmpSubmit" method="post">
            <div class="widget-content tab-content">
                <fieldset class="well the-fieldset">
                    <table  style="width: 70%;vertical-align: top" border="0">
                        <tr>
                            <td style="text-align: left;width: 80px;"><label  class="control-label" style="font-weight: bold">From</label></td>
                            <td><input style="width: 100%" type="text" readonly="" value="<?php echo $email["sendfrom"] ?>"></td>
                        </tr> 
                        <tr>
                            <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Send To</label></td>
                            <td><input style="width: 100%" autofocus="" required="" type="email" name="username" id="username"   ></td>
                        </tr>
                        <tr>
                            <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Subject</label></td>
                            <td><input style="width: 100%"  type="text" name="subjectline" id="subjectline"  value="<?php echo trim($email["subjectline"]) ?>" ></td>
                        </tr>
                        <tr >
                            <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Body</label></td>
                            <td colspan="5">
                                <textarea style="width: 100%;height: 250px;line-height: 22px;" name="body">&#13;&#10;&#13;&#10;<?php echo trim(strip_tags($email["body"])) ?></textarea>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <hr/>
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SEND</button>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div> 