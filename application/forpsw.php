<?php
error_reporting(0);
session_start();
ob_start();

include './MysqlConnection.php';
include './ApplicationUtil.php';

$error = base64_decode(filter_input(INPUT_GET, "msg"));
if (isset($_POST["go"])) {
    $email = filter_input(INPUT_POST, "email");
    $data = MysqlConnection::fetchCustom("SELECT user_id,email  FROM `user_master` WHERE `email` = '$email'");
    $userdata = $data[0];
    if (count($userdata) == 0) {
        $error1 = "Sorry username not found!!!";
    } else {
        $attachment = "";
        $subject = "Reset password - RoundWrap Industries";
        $emailconfig = MysqlConnection::getEmailConfigDetail();
        $bodymatter = "<html>"
                . "<body>"
                . "Hi " . $userdata["firstName"] . ",<br/>"
                . "<br/>"
                . "Please find password reset link.<br/><br/>"
                . "<b>http://35.183.37.135/application/confirmpsw.php?token=" . base64_encode($email) . "@@" . $userdata["user_id"] . "</b><br/>"
                . "<br/><br/>" . str_replace("\n", "<br/>", strip_tags($emailconfig["body"]))
                . "</body>"
                . "<html>";
        MysqlConnection::sendEmail($email, $attachment, $subject, $bodymatter, $title);
        $error1 = "Email has been sent to registered EmailId.";
    }
    header("location:forpsw.php?msg=" . base64_encode($error1));
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
        <title>LOGIN | ROUND WRAP INDUSTRIES LTD RICHMOND BC</title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css" />
    </head>
    <body>
        <section class="container" style="margin-top: 80px;">
            <section class="login-form">
                <form method="post" role="login">
                    <h2>Please Enter Your Email Address</h2>
                    <p>To Reset Password</p>
                    <p style="color: red"><?php echo $error ?></p>

                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="text-primary glyphicon glyphicon-envelope"></span></div>
                            <input type="email" name="email" placeholder="Email address" required class="form-control" />
                        </div>
                    </div>
                    <button type="submit" name="go" class="btn btn-block btn-success">SEND</button>
                    <a href="login.php">Back</a>
                </form>
            </section>
        </section>
    </body>
</html>
<?php $_SESSION["error"] = ""; ?>