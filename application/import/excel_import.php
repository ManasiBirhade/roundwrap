<?php
$process = filter_input(INPUT_GET, "process");
$postvalue = filter_input(INPUT_POST, "btnSubmitFullForm");
if (isset($postvalue)) {
    $importfor = filter_input(INPUT_POST, "importfor");

    $export = MysqlConnection::uploadFile($_FILES["filepath"], "import/");

    if ($importfor == "customer") {
        include 'import/customerimport.php';
//        header("location:index.php?pagename=manage_customermaster&status=active");
    } else if ($importfor == "vendor") {
        include 'import/vendorimport.php';
//        header("location:index.php?pagename=manage_suppliermaster&status=active");
    } else if ($importfor == "items") {
        include 'import/itemimport.php';
//        header("location:index.php?pagename=manage_itemmaster&status=active");
    }
    $serror = "Data imported successfully !!!";
}
?>
<?php if (isset($process)) { ?>
    <script>
        $(document).ready(function() {
            $("div#divLoading").addClass('show');
            $.ajax({
                type: 'GET',
                url: 'import/sampleajax.php',
                data: ""
            }).done(function(data) {
                $('#result').append(data);
                $("div#divLoading").removeClass('show');
            }).fail(function() {
            });
        });
    </script>
<?php } ?>


<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">Import Excel Sheet</h5>
    </div>
    <div class="widget-box" style="border-bottom: solid 1px #CDCDCD;background-color: white">
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Import Data From Excel Sheet</a></li>
            </ul>
        </div>
        <form name="frmUserSubmit"  enctype="multipart/form-data" id="frmUserSubmit" method="post" >
            <div class="widget-content tab-content" style="background-color: white">
                <p style="color: green;font-size: 14px;" id="error"><?php echo $serror ?></p>
                <p style="color: red;font-size: 14px;" id="serror"><?php echo $error ?></p>
                <table  style="width: 50%;vertical-align: top" border="0" class="display nowrap sortable" >
                    <tr>
                        <td style="width: 15%"><label class="control-label">Import For</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td>
                            <select style="width: 300px;" name="importfor" id="importfor" >
                                <option>-</option>
                                <option value="customer">CUSTOMER</option>
                                <option value="vendor">VENDOR</option>
                                <option value="items">ITEMS</option>
                            </select>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">File Path</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td><input type="file" name="filepath"   style="width: 300px;;border: solid 1px rgb(220,220,220);height: 25px;" id="filepath" minlenght="2" maxlength="30"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr> 
                </table>
                <br/>
                <div id="result"></div>
            </div>
            <div class="modal-footer ">
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SAVE</button>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div>