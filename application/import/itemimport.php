<?php

error_reporting(0);
$taxresult = MysqlConnection::fetchCustom("SELECT `id`,`taxname`,`taxvalues` FROM `taxinfo_table`");
$taxmapping = array();
foreach ($taxresult as $key => $value) {
    $taxmapping[$value["taxname"]] = $value;
}
$exceldata = getExcelData($export, $taxmapping);
processItems($exceldata);
unlink($export);

function getExcelData($excelpath, $taxmapping) {
    try {
        $spreadsheet = new SpreadsheetReader($excelpath);
        $sheets = $spreadsheet->Sheets();
        $arrayextracteddata = array();
        $dbheader = array();
        $header = array();
        $data = array();
        $restcounter = 0;
        foreach ($sheets as $index => $name) {
            $spreadsheet->ChangeSheet($index);
            $indexfor = 1;
            foreach ($spreadsheet as $key => $row) {
                if ($row[0] != "") {
                    if ($indexfor == 1) {
                        array_push($dbheader, $row);
                    } else if ($indexfor == 2) {
                        array_push($header, $row);
                    } else {
                        $data[] = getValidData($row, $taxmapping, $restcounter . "" . $indexfor);
                    }
                    $indexfor++;
                }
            }
            $restcounter++;
        }
        array_push($arrayextracteddata, $dbheader);
        array_push($arrayextracteddata, $header);
        array_push($arrayextracteddata, $data);
    } catch (Exception $E) {
        //echo $E->getMessage();
    }
    return $arrayextracteddata;
}

function getValidData($row, $taxmapping, $counter) {
    $data = array();
    $data["item_code"] = $item_code = $row[0];
    $data["item_name"] = $item_name = $row[1];
    $data["unit"] = $unit = $row[2];
    $data["purchase_rate"] = $purchase_rate = $row[3];
    $data["purch_code"] = $purch_code = $row[4];
    $data["purchase_rate_tax"] = $purchase_rate_tax = $row[5];
    $data["item_desc_purch"] = $item_desc_purch = $row[6];
    $data["sell_rate"] = $sell_rate = $row[7];
    $data["sales_code"] = $sales_code = $row[8];
    $data["sell_rate_tax"] = $sell_rate_tax = $row[9];
    $data["item_desc_sales"] = $item_desc_sales = $row[10];
    $data["onhand"] = $onhand = $row[11];
    $data["reorder"] = $reorder = $row[12];
    $data["totalvalue"] = $totalvalue = $row[13];
    $data["asof"] = $asof = $row[14];

    $data["total_purch_rate"] = $total_purch_rate = "0.0";
    $data["total_sales_rate"] = $total_sales_rate = "0.0";
    $data["id"] = $id = md5((time() * rand(1000, 9999) * ( $counter)));

    if ($purch_code == "" || $purch_code == "0" || $purch_code == "0.0") {
        $data["purch_code"] = $taxmapping["No Tax"]["id"];
    } else {
        $data["purch_code"] = $taxmapping[strtoupper($purch_code)]["id"];
        $taxvalue = $taxmapping[strtoupper($purch_code)]["taxvalues"];
        $data["purchase_rate_tax"] = $purchase_rate + ($purchase_rate * ($taxvalue / 100));
        $data["total_purch_rate"] = $purchase_rate + ($purchase_rate * ($taxvalue / 100));
    }

    if ($sales_code == "" || $sales_code == "0" || $sales_code == "0.0") {
        $data["sell_rate"] = $taxmapping["No Tax"]["id"];
    } else {
        $data["sales_code"] = $taxmapping[strtoupper($sales_code)]["id"];
        $taxvalue = $taxmapping[strtoupper($sales_code)]["taxvalues"];
        $data["sell_rate_tax"] = $sell_rate + ($sell_rate * ($taxvalue / 100));
        $data["total_sales_rate"] = $sell_rate + ($sell_rate * ($taxvalue / 100));
    }
    return "'" . implode("','", $data) . "'";
}

function processItems($exportdata) {
    $data = $exportdata[2];
    $datacount = count($data);
    $bachsize = round($datacount / 1000) + 1;
    $bach = array();
    for ($import = 0; $import < $bachsize; $import++) {
        $bachdata = array();
        $batchminindex = 1000 * $import;
        $batchmaxindex = 1000 + $batchminindex;
        for ($batchindex = $batchminindex; $batchindex < $batchmaxindex; $batchindex++) {
            if (count($data[$batchindex]) != 0) {
                if ($data[$batchindex][0] != "") {
                    array_push($bachdata, "(" . $data[$batchindex] . ")");
                }
            }
        }
        $impload = implode(",", $bachdata);
        $sqlquery = "INSERT INTO `item_master` ( "
                . "item_code,item_name,unit,purchase_rate,purch_code,purchase_rate_tax,item_desc_purch,sell_rate,sales_code,sell_rate_tax,item_desc_sales,onhand,reorder,totalvalue,asof,total_purch_rate,total_sales_rate,item_id"
                . ") VALUES " . $impload;
        MysqlConnection::executeQuery($sqlquery);
    }
}
