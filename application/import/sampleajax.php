<?php

session_start();
ob_start();

$process = filter_input(INPUT_GET, "process");
$importfor = filter_input(INPUT_GET, "importfor");

if (isset($process) && isset($importfor)) {
    if ($importfor == "customer") {
        include 'import/customerimport.php';
    } else if ($importfor == "vendor") {
        include 'import/vendorimport.php';
    } else if ($importfor == "items") {
        include 'import/itemimport.php';
    }
}