<?php

error_reporting(0);
$taxresult = MysqlConnection::fetchCustom("SELECT `id`,`taxname`,`taxvalues` FROM `taxinfo_table`");
$taxmapping = array();
foreach ($taxresult as $key => $value) {
    $taxmapping[$value["taxname"]] = $value;
}
$exceldata = getExcelData($export, $taxmapping);
processItems($exceldata);
unlink($export);

function getExcelData($excelpath, $taxmapping) {
    try {
        $spreadsheet = new SpreadsheetReader($excelpath);
        $sheets = $spreadsheet->Sheets();
        $arrayextracteddata = array();
        $dbheader = array();
        $header = array();
        $data = array();
        $restcounter = 0;
        foreach ($sheets as $index => $name) {
            $spreadsheet->ChangeSheet($index);
            $indexfor = 1;
            foreach ($spreadsheet as $key => $row) {
                if ($row[3] != "") {
                    if ($indexfor == 1) {
                        array_push($dbheader, $row);
                    } else if ($indexfor == 2) {
                        array_push($header, $row);
                    } else {
                        $data[] = getValidData($row, $taxmapping, $restcounter . "" . $indexfor);
                    }
                    $indexfor++;
                }
            }
            $restcounter++;
        }
        array_push($arrayextracteddata, $dbheader);
        array_push($arrayextracteddata, $header);
        array_push($arrayextracteddata, $data);
    } catch (Exception $E) {
        //echo $E->getMessage();
    }
    return $arrayextracteddata;
}

function getValidData($row, $taxmapping, $counter) {
    $data = array();
    $data["salutation"] = str_replace("'", "", $row[0]);
    $data["firstname"] = str_replace("'", "", $row[1]);
    $data["lastname"] = str_replace("'", "", $row[2]);
    $data["companyname"] = str_replace("'", "", $row[3]);
    $data["supp_email"] = str_replace("'", "", $row[4]);
    $data["supp_phoneNo"] = str_replace("'", "", $row[5]);
    $data["taxInformation"] = $taxmapping[$row[6]]["id"];
    $data["shippingtaxinfo"] = $taxmapping[$row[7]]["id"];
    $data["supp_streetNo"] = str_replace("'", "", $row[8]);
    $data["postal_code"] = str_replace("'", "", $row[9]);
    $data["supp_city"] = str_replace("'", "", $row[10]);
    $data["supp_province"] = str_replace("'", "", $row[11]);
    $data["supp_country"] = str_replace("'", "", $row[12]);
    $data["id"] = $id = md5((time() * rand(1000, 9999) * ( $counter)));
    return "'" . implode("','", $data) . "'";
}

function processItems($exportdata) {
    $columns = $exportdata[0];
    $data = $exportdata[2];
    $datacount = count($data);
    $bachsize = round($datacount / 1000) + 1;
    $bach = array();
    for ($import = 0; $import < $bachsize; $import++) {
        $bachdata = array();
        $batchminindex = 1000 * $import;
        $batchmaxindex = 1000 + $batchminindex;
        for ($batchindex = $batchminindex; $batchindex < $batchmaxindex; $batchindex++) {
            if (count($data[$batchindex]) != 0) {
                if ($data[$batchindex][0] != "") {
                    array_push($bachdata, "(" . $data[$batchindex] . ")");
                }
            }
        }
        $impload = implode(",", $bachdata);
        echo $sqlquery = "INSERT INTO `supplier_master` (" . implode(",", $columns[0]) . "supp_id) VALUES " . $impload;
        MysqlConnection::executeQuery($sqlquery);
    }
}
