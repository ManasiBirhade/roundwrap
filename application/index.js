$(function() {
    setTimeout(function() {
        $("#successMessage").hide('blind', {}, 100)
    }, 5000);
});

function convertFormToJSON(form) {
    var array = $(form).serializeArray();
    var json = {};

    jQuery.each(array, function() {
        json[this.name] = this.value || '';
    });

    return json;
}
function chkNumericKey(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode === 46 || charCode === 45) {
        return true;
    } else {
        return false;
    }
}

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'bottom'
    });
});



$("#data tr").click(function() {
    var selected = $(this).hasClass("highlight");
    $("#data tr").removeClass("highlight");
    if (!selected)
        $(this).addClass("highlight");
});
$("#data").delegate("tr", "contextmenu", function(e) {
    var selected = $(this).hasClass("highlight");
    $("#data tr").removeClass("highlight");
    if (!selected)
        $(this).addClass("highlight");
});

function goPage(newURL) {
    if (newURL !== "") {
        if (newURL === "-") {
            resetMenu();
        } else {
            document.location.href = newURL;
        }
    }
}

function resetMenu() {
    document.gomenu.selector.selectedIndex = 2;
}
$(window).load(function() {
    $('#overlay1').fadeOut();
});

function removeDefaultFunction()
{
    window.onhelp = function() {
        return false;
    }
}
/** use keydown event and trap only the F-key, 
 but not combinations with SHIFT/CTRL/ALT **/
$(window).bind('keydown', function(e) {
    //This is the F1 key code, but NOT with SHIFT/CTRL/ALT
    var keyCode = e.keyCode || e.which;
    if ((keyCode === 112 || e.key === 'F1') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Open help window here instead of alert
        window.location = "index.php?pagename=create_itemmaster";
    }
    // Add other F-keys here:
    else if ((keyCode === 113 || e.key === 'F2') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_customermaster";
    } else if ((keyCode === 113 || e.key === 'F3') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_suppliermaster";
    } else if ((keyCode === 113 || e.key === 'F4') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_perchaseorder";
    } else if ((keyCode === 113 || e.key === 'F5') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_salesorder";
    } else if ((keyCode === 113 || e.key === 'F6') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_packingslip";
    } else if ((keyCode === 113 || e.key === 'F7') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=manage_quotation";
    } else if ((keyCode === 113 || e.key === 'F8') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=manage_workorder";
    } else if ((keyCode === 113 || e.key === 'F9') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=createcash_salesorder";
    }
});


