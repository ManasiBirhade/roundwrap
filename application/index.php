<?php
error_reporting(0);
session_start();
ob_start();

include './MysqlConnection.php';
include './ApplicationUtil.php';

require('./spreadsheetmaster/php-excel-reader/excel_reader2.php');
require('./spreadsheetmaster/SpreadsheetReader.php');

$pagename = filter_input(INPUT_GET, "pagename");
$explode = explode("_", $pagename);
$include = "";
$module = "";
$page = "";
if (count($explode) >= 2) {
    $include = $explode[1] . "/" . $pagename;
    $module = $explode[1];
    $page = $explode[0] . " " . $explode[1];
} else {
    $include = "dashboard/manage_dashboard";
    $module = "Home";
    $page = "Dashboard";
}
//print_r($_SESSION["user"]);
if ($_SESSION["user"] == "") {
    header("location:logout.php");
}
$ownaddress = $_SESSION["user"]["cmpName"] . ", " .
        $_SESSION["user"]["streetNo"] . " " .
        $_SESSION["user"]["streetName"] . " " .
        $_SESSION["user"]["postalCode"] . ", " .
        $_SESSION["user"]["province"] . " " .
        $_SESSION["user"]["city"] . " " .
        $_SESSION["user"]["country"] . " ";


$genericresultset = MysqlConnection::fetchCustom("SELECT code,name FROM `generic_entry` WHERE `type` = 'dateformat' AND `active` = 'Y' LIMIT 0,1");
$dateformatvalueformat = $genericresultset[0]["name"];
switch ($dateformatvalueformat) {
    case "Y-m-d":
        $dateformatvalue = "yy-mm-dd";
        break;
    case "d-m-Y":
        $dateformatvalue = "dd-mm-yy";
        break;
    default:
        $dateformatvalue = "yy-mm-dd";
}
$dateformat = count($genericresultset[0]) == 0 ? "yy-mm-dd" : $dateformatvalue;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Welcome to RoundWrap Industries Richmond</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="icon" href="img/fevicon.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <link rel="stylesheet" href="css/maruti-style.css" />
        <link rel="stylesheet" href="css/maruti-media.css" class="skin-color" />
        <link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/loder.css" />
        <link href="css/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="tablemodel/jquery.dataTables.min.css" rel="stylesheet">

        <!-- JS START FROM HERE -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="tablemodel/jquery.dataTables.min.js"></script>

        <script src="js/jquery.contextMenu.js" type="text/javascript"></script>
        <script src="js/jquery.mask.js"></script> 
        <script src="index.js"></script>

        <script>
            var dateme = new Date();
            document.cookie = "dateme=" + format_date(dateme);

            function format_date(date)
            {
                if (typeof date === "string")
                {
                    date = new Date(date);
                }

                var year = date.getFullYear();
                var month = (1 + date.getMonth()).toString();
                month = month.length > 1 ? month : '0' + month;

                var day = date.getDate().toString();
                day = day.length > 1 ? day : '0' + day;

                return year + '-' + month + '-' + day;
            }


            var timeout = setInterval(reloadChat, 1000);
            function reloadChat() {
                $('#liveconter').load('dashboard/time.php');
            }
            $(document).on('keydown', 'input[pattern]', function(e) {
                var input = $(this);
                var oldVal = input.val();
                var regex = new RegExp(input.attr('pattern'), 'g');

                setTimeout(function() {
                    var newVal = input.val();
                    if (!regex.test(newVal)) {
                        input.val(oldVal);
                    }
                }, 0);
            });
        </script>
    </head>
    <style>
        .customfooterdis {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #565656;
            color: white;
            text-align: center;
            height: 10px;
        }
    </style>
    <body >
        <table style="width: 100%;height: 100%;background-color: white;" border='0'>
            <tr style="height: 25%">
                <td>
                    <div class=".navbar">
                        <div style="float: left;width: 48%; height: 70px">
                            <img src="assets/images/download.png" style="width: 220px;height: 50px;margin: 0 auto;margin: 20px;margin-left: 0px;">
                        </div>
                        <div style="float: left">
                            <div id="overlay1" style="margin: 0 auto;">
                                <img src="css/3.gif" alt="Loading" style="margin-top: 10px;" />
                            </div>
                        </div>
                    </div>
                    <div style="float: right">
                        <h4 id="liveconter" style=";padding: 15px;text-align: center;font-family: verdana;font-size: 14px;margin-top: -2px;margin-right: -2px;text-transform: uppercase;text-align: left">
                            <div id="clock" class="clock" ></div>
                        </h4>
                    </div>
                </td>
            </tr>
            <tr >
                <td style="padding: 0px;margin: 0px;"><div id="sidebar" style="margin-top: 70px;clear: both;"><?php include './leftmenu.php'; ?></div></td>
            </tr>
            <tr  style="height: auto">
                <td>
                    <img src="img/ajaxloading.gif" id="img" style="display:none" />
                    <hr/>
                    <div id="divLoading"></div>

                    <div id="content"><?php include '' . $include . ".php"; ?></div>
                </td>
            </tr>
        </table>
        <div class="customfooterdis">
            <p>&nbsp;</p>
        </div>
    </body>
    <script src="js/bootstrap.min.js"></script> 
    <script src="js/jquery.uniform.js"></script> 
</body>
</html>
