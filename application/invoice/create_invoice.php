<?php
$customerlist = MysqlConnection::fetchCustom("SELECT id,cust_companyname FROM `customer_master` WHERE status = 'Y' ORDER BY `cust_companyname` ASC");

$btnSaveSalesOrder = filter_input(INPUT_POST, "btnSaveSalesOrder");
$search = filter_input(INPUT_POST, "searchme");
if (isset($search)) {
    $customer_id = filter_input(INPUT_POST, "customer_id");
    $listPackingSlip = MysqlConnection::fetchCustom("SELECT * FROM `packslip` WHERE `cust_id` = '$customer_id' AND isInvoiced = 'N' ");
}

if ($btnSaveSalesOrder != "") {
    $checkedPS = $_POST["checkedPS"];
    $invoiceprimary = saveToDatabase($checkedPS);
    generatePDF($checkedPS, $invoiceprimary);
    $_SESSION["invoiceid"] = $invoiceprimary;
    header("location:index.php?pagename=manage_invoice");
}
?>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">PRODUCTION INVOICE</a></li>
                </ul>
            </div>
            <br/>

            <div style="padding: 5px;">
                <table border="0" style="width: 100%" class="display nowrap sortable">
                    <tr style="vertical-align: middle">
                        <td style="width: 120px;">CUSTOMER&nbsp;NAME</td>
                        <td style="width: 85%">
                            <select name="customer_id" id="customer_id" style="width: 100%;height: 28px">
                                <option value="">SELECT</option>
                                <?php
                                foreach ($customerlist as $key => $value) {
                                    $selected = $value["id"] == $customer_id ? "selected" : "";
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $value["id"] ?>"><?php echo $value["cust_companyname"] ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td><input type="submit" id="searchme" name="searchme" class="btn btn-info" style="height: 28px;" value="SEARCH" ></td>
                    </tr>
                </table>
                <hr/>
                <table id="example" class="display nowrap sortable table-bordered" style="width:100%">
                    <thead >
                        <tr >
                            <td style="text-align: center;width: 10px;">CHECK</td>
                            <td>Profile Name</td>
                            <td>SO No</td>
                            <td>PO No</td>
                            <td>PO Date</td>

                            <td>Pieces</td>
                            <td>Cut Tape Cost</td>
                            <td>Total SqFt</td>
                            <td>Profile Cost</td>
                            <td>Sub Total</td>
                            <td>Tax</td>
                            <td>Net Total</td>
                        </tr>
                    </thead>
                    <tbody style="border-color: #76323F;background-color: white">
                        <?php
                        $index = 1;
                        foreach ($listPackingSlip as $key => $value) {
                            $bg = MysqlConnection::generateBgColor($index);
                            $customername = MysqlConnection::fetchCustom("SELECT cust_companyname FROM customer_master WHERE id = '" . $value["cust_id"] . "'");
                            $portfolioname = MysqlConnection::fetchCustom("select portfolio_name,profile_name from tbl_portfolioprofile where id = '" . $value["prof_id"] . "'");
                            ?>
                            <tr id="<?php echo $value["ps_id"] ?>" class="context-menu-one" onclick="setId('<?php echo $value["ps_id"] ?>')" ondblclick ="viewPO('<?php echo $value["id"] ?>')" style="<?php echo $bg ?> ;border-bottom: solid 1px rgb(220,220,220);text-align: left;vertical-align: central;height: 35px;">
                                <td style="text-align: left"><input type="checkbox" name="checkedPS[]" value="<?php echo $value["ps_id"] ?>" id="checkedPS<?php echo $index ?>" style="height:15px; width:15px;"></td>
                                <td>&nbsp;<?php echo $portfolioname[0]["profile_name"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["so_no"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["po_no"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo MysqlConnection::convertToPreferenceDate($value["rec_date"]) ?></td>

                                <td style="text-align: left">&nbsp;<?php echo $value["total_pieces"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["cut_tape_cost"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["billable_fitsquare"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["profilecost"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["total_cost"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["taxname"] ?></td>
                                <td style="text-align: left">&nbsp;<?php echo $value["nettotal"] ?></td>

                            </tr>
                            <?php
                            $index++;
                        }
                        ?>
                        <?php
                        if (count($listPackingSlip) == 0) {
                            for ($index = 0; $index <= 5; $index++) {
                                ?>
                                <tr >
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                    <td ></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer "> 
                <center>
                    <input type="submit" id="btnSaveSalesOrder" name="btnSaveSalesOrder" class="btn btn-info" value="CREATE INVOICE">
                    <a href="index.php?pagename=manage_invoice" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                </center>
            </div>

        </div>
    </div>

</form>
<?php

function saveToDatabase($checkedps) {
    $invoice["invoicenumber"] = time();
    $invoice["customername"] = "";
    $invoice["invoicedate"] = date("Y-m-d");

    $invoice["totaltax"] = "0.0";
    $invoice["subtotal"] = "0.0";
    $invoice["nettotal"] = "0.0";
    $invoice["active"] = "Y";

    foreach ($checkedps as $packslipid) {
        $packlsip = MysqlConnection::getPackSlipFromId($packslipid);
        $invoice["totaltax"] = $invoice["totaltax"] + $packlsip["taxvalue"];
        $invoice["subtotal"] = $invoice["subtotal"] + $packlsip["total_cost"];
        $invoice["nettotal"] = $invoice["nettotal"] + $packlsip["nettotal"];


        $customerdetails = MysqlConnection::fetchCustom("SELECT cust_companyname FROM `customer_master` where id = '" . $packlsip["cust_id"] . "' ");
        $invoice["customername"] = $customerdetails[0]["cust_companyname"];
        MysqlConnection::delete("UPDATE packslip SET isInvoiced = 'Y' WHERE ps_id = '$packslipid'  ");
    }
    $invoice["poids"] = implode(",", $checkedps);
    return MysqlConnection::insert("tbl_invoice", $invoice);
}

function generatePDF($checkedps, $invoiceprimary) {
    include 'pdflib/invoice.php';
}
?>