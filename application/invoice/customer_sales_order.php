<?php

$salesorderid = filter_input(INPUT_GET, "salesorderid");

if (file_exists("../download/salesorder/$salesorderid.pdf")) {
    header('Content-type: application/pdf');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: bytes');
    readfile("../download/salesorder/$salesorderid.pdf");
} else {
    echo "<p align='center' style='color:red'>No Any Sales Order Created</p><br/>";
}
