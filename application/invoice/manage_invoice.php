<?php
$listofinvoice = MysqlConnection::fetchCustom("SELECT * FROM tbl_invoice ");
$invoiceid = $_SESSION["invoiceid"];
if ($invoiceid != "") {
    echo "<script language='javascript'>window.open('invoice/print_invoice.php?invoiceid=$invoiceid');</script>";
}
$_SESSION["invoiceid"] = "";
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">INVOICE LIST</h5>
    </div>
    <br/>
    <table style="width: 100%" >
        <tr >
            <td style="float: left">
                <a class="btn btn-info"  href="index.php?pagename=create_invoice" ><i class="icon icon-user"></i>&nbsp;CREATE INVOICE</a>
            </td>

        </tr>
    </table>
    <br/>

    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td >#</td>
                <td >Invoice No</td>
                <td >Customer Name</td>
                <td >Invoice Date</td>
                <td >Total Tax</td>
                <td >Amount</td>
                <td >Net Amount</td>
                <td >Status</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listofinvoice as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                if ($value["status"] == "N") {
                    $back = $inavtivecolor;
                } else {
                    $back = "";
                }
                ?>
                <tr id="<?php echo $value["id"] ?>" style="background-color: <?php echo $bgcolor ?>;"  class="context-menu-one">
                    <td >&nbsp;<?php echo $index++ ?></td>
                    <td ><?php echo $value["invoicenumber"] ?></td>
                    <td ><?php echo $value["customername"] ?></td>
                    <td ><?php echo $value["invoicedate"] ?></td>
                    <td ><?php echo $value["totaltax"] ?></td>
                    <td ><?php echo $value["subtotal"] ?></td>
                    <td ><?php echo $value["nettotal"] ?></td>
                    <td ><?php echo $value["status"] == "NO" ? "<span class='badge badge-info'>NO</span>" : "<span class='badge badge-success'>YES</span>" ?></td>

                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <!--            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" style="margin-left:10px;">EMAIL</button>-->
        </div> 
    </div>
</div>


<script type="text/javascript">
    $('#example tbody tr').click(function(e) {
        var id = $(this).attr('id');
        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var del = '<a href="invoice/print_invoice.php?invoiceid=' + id + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + del + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });





//    $(function() {
//        $.contextMenu({
//            selector: '.context-menu-one',
//            callback: function(key, options) {
//                var m = "clicked row: " + key;
//                var id = $(this).attr('id');
//                switch (key) {
//                    case "view_vendor":
//                        window.location = "index.php?pagename=view_suppliermaster&supplierid=" + id;
//                        break;
//                    case "create_vendor":
//                        window.location = "index.php?pagename=create_suppliermaster";
//                        break;
//                    case "edit_vendor":
//                        window.location = "index.php?pagename=create_suppliermaster&supplierid=" + id;
//                        break;
//                    case "delete_vendor":
//                        window.location = "index.php?pagename=view_suppliermaster&supplierid=" + id + "&flag=yes";
//                        break;
//                    case "create_perchase_order":
//                        window.location = "index.php?pagename=create_perchaseorder&supplierid=" + id;
//                        break;
//                    case "active_vendor":
//                        window.location = "index.php?pagename=manage_suppliermaster&supplierid=" + id;
//                        break;
//                    case "create_note":
//                        window.location = "index.php?pagename=note_suppliermaster&supplierid=" + id;
//                        break;
//                    case "create_payment":
//                        window.location = "index.php?pagename=vendor_payment&supplierid=" + id;
//                        break;
//                    case "quit":
//                        window.location = "index.php?pagename=manage_dashboard";
//                        break;
//
//                    default:
//                        window.location = "index.php?pagename=manage_suppliermaster";
//                }
//                //window.console && console.log(m) || alert(m+"    id:"+id); 
//            },
//            items: {
//                "view_vendor": {name: "VIEW VENDOR", icon: ""},
//                "edit_vendor": {name: "EDIT VENDOR", icon: ""},
//                "delete_vendor": {name: "DELETE VENDOR", icon: ""},
//                "active_vendor": {name: "ACTIVE/IN ACTIVE VENDOR", icon: ""},
//                "sep0": "---------",
//                "create_note": {name: "CREATE NOTE", icon: ""},
//                "create_perchase_order": {name: "CREATE PURCHASE ORDER", icon: ""},
//                "create_payment": {name: "MAKE PAYMENT", icon: ""}
//            }
//        });
//
//        //        $('.context-menu-one').on('click', function(e){
//        //            console.log('clicked', this);
//        //       })    
//    });
//    $('tr').dblclick(function() {
//        var id = $(this).attr('id');
//        if (id !== undefined) {
//            window.location = "index.php?pagename=view_suppliermaster&supplierid=" + id;
//        }
//    });

    function showNotes(supplierid) {
        var dataString = "supplierid=" + supplierid;
        $("div#divLoading").addClass('show');
        $('#noteinformation').modal('show');
        var dataString = "supplierid=" + supplierid;
        $.ajax({
            type: 'POST',
            url: 'suppliermaster/ajaxsuppliernotes.php',
            data: dataString
        }).done(function(data) {
            $("#notedata").html(data);
            $("div#divLoading").removeClass('show');
        }).fail(function() {
        });
    }
</script>



