<?php

include '../MysqlConnection.php';
$purchaseorderid = filter_input(INPUT_GET, "purchaseorderid");

if (file_exists("../download/purchaseorder/$purchaseorderid.pdf")) {
    header('Content-type: application/pdf');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: bytes');
    readfile("../download/purchaseorder/$purchaseorderid.pdf");
} else {
    echo "<p align='center' style='color:red'>No Any Purchase Order found</p><br/>";
}
?>
