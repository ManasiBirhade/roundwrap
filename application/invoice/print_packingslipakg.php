<?php

error_reporting(0);
session_start();
ob_start();

$packslipid = filter_input(INPUT_GET, "packslipid");

if (file_exists("../download/packingslipakg/$packslipid.pdf")) {
header('Content-type: application/pdf');
header('Content-Transfer-Encoding: binary');
header('Accept-Ranges: bytes');
readfile("../download/packingslipakg/" . $packslipid . ".pdf");
} else {
    echo "<p align='center' style='color:red'>No Any Packing Slip Acknowledgement Created</p><br/>";
}
