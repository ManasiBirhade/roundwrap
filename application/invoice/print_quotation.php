<?php

error_reporting(0);
session_start();
ob_start();

$packslipid = filter_input(INPUT_GET, "psid");

if (file_exists("../download/quotation/$packslipid.pdf")) {
    header('Content-type: application/pdf');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: bytes');
    readfile("../download/quotation/$packslipid.pdf");
} else {
    echo "<p align='center' style='color:red'>No Pdf Any Quotation Created </p><br/>";
}
?>
