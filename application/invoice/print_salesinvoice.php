<?php

include '../MysqlConnection.php';

$salesorderid = filter_input(INPUT_GET, "salesorderid");
$invoiceno = filter_input(INPUT_GET, "invoiceno");
$invoice = "";
if ($salesorderid == "") {
    $invoice = $invoiceno;
} else {
    $resultset = MysqlConnection::fetchCustom("SELECT invoiceno FROM sales_order WHERE id = '$salesorderid' ");
    $invoice = $resultset[0]["invoiceno"];
}

if (file_exists("../download/salesinvoice/$invoice.pdf")) {
header('Content-type: application/pdf');
header('Content-Transfer-Encoding: binary');
header('Accept-Ranges: bytes');
readfile("../download/salesinvoice/$invoice.pdf");
} else {
    echo "<p  align='center' style='color:red;'>No Any Invoice Created</p><br/>";
}
?>
