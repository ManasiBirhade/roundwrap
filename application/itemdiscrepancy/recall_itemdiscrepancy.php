<?php
$itemId = filter_input(INPUT_GET, "itemId");

$resultset = MysqlConnection::fetchCustom("SELECT * FROM `sales_item` WHERE `id` = '$itemId'");
$soitem = $resultset[0];

$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit) && $btnSubmit == "YES") {
    MysqlConnection::updateItem($soitem["item_id"], $soitem["backQty"], "Y");
    MysqlConnection::executeQuery("UPDATE sales_item SET discrepancy = '' WHERE id = '$itemId' ");
    header("location:index.php?pagename=view_itemdiscrepancy");
}
?>

<form name="frmrecall" id="frmrecall" method="POST">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-error" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>Recall Inventory!</strong> 
                    Do you want to Recall Inventory ??
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><input type="submit" name="btnSubmit" id="btnSubmit" value="YES" class="btn btn-info"/></td>
                            <td><a href="index.php?pagename=view_itemdiscrepancy" id="btnSubmitFullForm" class="btn btn-info">BACK</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
</form>