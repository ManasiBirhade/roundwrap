<?php
$discrepancarray = MysqlConnection::fetchCustom("SELECT * FROM `sales_item` WHERE discrepancy != '' ORDER BY `idindex` DESC");
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">ITEM DISCREPANCY</h5>
    </div>
    <br/>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Date</td>
                <td>SO.NO</td>
                <td>PO.NO</td>
                <td>Customer Name</td>
                <td>Stock Loss</td>
                <td>Item Discrepancy</td>
                <td>PO</td>
            </tr>
        </thead>
        <tbody >
            <?php
            $index = 1;
            foreach ($discrepancarray as $value) {
                $itemid = $value["item_id"];
                $sodetails = getSalesOrder($value["so_id"]);
                $date = explode(" ", $value["adddate"]);
                ?>
                <tr id="<?php echo $value["id"] ?>"   class="context-menu-one"  >
                    <td style="width: 25px"><?php echo $index++ ?></td>
                    <td><?php echo $date[0] ?></td>
                    <td><?php echo $sodetails["sono"] ?></td>
                    <td><?php echo $sodetails["pono"] ?></td>
                    <td><?php echo getCustomerName($sodetails["customer_id"]) ?></td>
                    <td><?php echo $value["discrepancycount"] ?></td>
                    <td><?php echo $value["discrepancy"] ?></td>
                    <td>-</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_item":
                        window.location = "index.php?pagename=recall_itemdiscrepancy&itemId=" + id;
                        break;
                }
            },
            items: {
                "view_item": {name: "RECALL ITEM", icon: ""},
            }
        });
    });
</script>

<?php

function getSalesOrder($sonumber) {
    $query = "SELECT sono,pono,customer_id FROM `sales_order` WHERE `id` = '$sonumber' ";
    $result = MysqlConnection::fetchCustom($query);
    return $result[0];
}

function getCustomerName($customer) {
    $query = "SELECT cust_companyname FROM `customer_master` WHERE `id` = '$customer' ";
    $result = MysqlConnection::fetchCustom($query);
    return $result[0]["cust_companyname"];
}
