<?php
$itemid = filter_input(INPUT_GET, "itemId");
$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
if ($btnSubmitFullForm == "SAVE") {
    unset($_POST["btnSubmitFullForm"]);
    $_POST["asof"] = MysqlConnection::convertToDBDate($_POST["asof"]);
    if (isset($itemid) && trim($itemid) != "") {
        if ($_FILES["pic"]["name"] != "") {
            $imagepath = MysqlConnection::uploadFile($_FILES["pic"], "../itemimage/");
            $_POST["itempic"] = trim($imagepath, "../");
        }
        $insert = MysqlConnection::edit("item_master", $_POST, " item_id = '$itemid' ");
    } else {
        $insert = MysqlConnection::insert("item_master", $_POST);
    }
   header("location:index.php?pagename=success_itemmaster");
} else if (!empty($itemid)) {
    $item = MysqlConnection::getItemDataById($itemid);
}
?>
<div class="container-fluid"  >
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">MANAGE ITEM</h5>
    </div>
    <?php include 'itemmaster/iteminfo.php'; ?>
</div>