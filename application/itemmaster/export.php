<?php

error_reporting(0);
include '../MysqlConnection.php';

$query = "SELECT COLUMN_NAME
  FROM INFORMATION_SCHEMA.COLUMNS
  WHERE TABLE_SCHEMA = 'rw'
  AND TABLE_NAME = 'item_master';";
$resultset = MysqlConnection::fetchCustom($query);

$metadata = array();
$metatext = array();
for ($index = 1; $index <= count($resultset); $index++) {
    array_push($metadata, $resultset[$index]["COLUMN_NAME"]);
    array_push($metatext, strtoupper(str_replace("_", " ", $resultset[$index]["COLUMN_NAME"])));
}

$metadatarow = implode(",", $metadata);
$metadatatext = implode(",", $metatext);



$queryitems = "SELECT * FROM item_master LIMIT 0,5";
$resultsetitems = MysqlConnection::fetchCustom($queryitems);
$nextrow = "";
foreach ($resultsetitems as $value) {
    unset($value["item_id"]);
    $nextrow .= implode(",", str_replace(",", " ", $value)) . "\n";
}


header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=items.csv");
header("Pragma: no-cache");
header("Expires: 0");
echo "$metadatarow\n$metadatatext\n$nextrow\n";
