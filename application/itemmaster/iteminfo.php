<?php
$itemlist = MysqlConnection::fetchCustom("SELECT item_id,item_code FROM item_master;");
$sqlvendorinformation = MysqlConnection::fetchCustom("SELECT supp_id,companyname FROM `supplier_master` WHERE status = 'Y' ORDER BY companyname  ;");
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM `taxinfo_table` ORDER BY `id` DESC");


$postedarrayimport = filter_input_array(INPUT_POST);
//btnImport importFile
$btnImport = $postedarrayimport["btnImport"];
if ($btnImport == "IMPORT") {
    $fileuploaded = MysqlConnection::uploadFile($_FILES["importFile"], "itemmaster/upload/");
    $arrayofline = readAndImportCSV($fileuploaded);
    $header = $arrayofline[0];
    $headertext = $arrayofline[1];
    for ($importindex = 2; $importindex <= count($arrayofline); $importindex++) {
        $current = $arrayofline[$importindex];
        $innerarray = array();
        for ($indexinnerrow = 0; $indexinnerrow < count($header); $indexinnerrow++) {
            $innerarray[$header[$indexinnerrow]] = $current[$indexinnerrow];
        }
        MysqlConnection::insert("item_master", $innerarray);
    }
    header("location:index.php?pagename=manage_itemmaster&status=active");
}
?>

<style>
    tbody {
        height: auto;
    }
    select {
        width: 212px;
        height: 24px;
    }
    tr{
        /*background-color: rgb(240,240,240);*/
    }
</style>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>

<form name="frmItemsSubmit" id="frmItemsSubmit" enctype="multipart/form-data" method="post">
    <div class="widget-box" style="border-bottom: solid 1px #CDCDCD;background-color: white">
        <table > 
            <tr style="vertical-align: top">
                <td style="vertical-align: top;width: 1%;">
                    <table   class="display nowrap sortable">
                        <tr >
                            <td >
                                <label class="control-label">Currency</label>
                                <select style="width: 225px;height: 25px" name="currency"  id="currency" >
                                    <?php
                                    $currencyarr = getcurrency();
                                    foreach ($currencyarr as $key => $value) {
                                        ?>
                                        <option value="<?php echo $key ?>" <?php echo $item["currency"] == $key ? "selected" : "" ?> ><?php echo $value ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td ><label class="control-label">Preferred Vendor</label>
                                <select style="width: 225px;height: 25px" name="vendorid"  id="vendorid" >
                                    <option value="0"></option>
                                    <?php foreach ($sqlvendorinformation as $key => $value) { ?>
                                        <option  <?php echo $item["vendorid"] == $value["supp_id"] ? "selected" : "" ?>
                                            value="<?php echo $value["supp_id"] ?>"> 
                                                <?php echo $value["companyname"] ?>
                                        </option> 
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>  
                            <td ><label class="control-label">Item Code <?php echo MysqlConnection::$REQUIRED ?></label><input style="width: 220px"  type="text" name="item_code" id="item_code" value="<?php echo $item["item_code"] ?>" onfocusout="validateDuplicate()"  required="true" minlenght="2" maxlength="30" /></td>
                        </tr>
                        <tr>
                            <td ><label class="control-label">Unit of Measures <?php echo MysqlConnection::$REQUIRED ?></label><input style="width: 220px" required="" type="text" name="unit" id="unit" maxlength="15" value="<?php echo $item["unit"] ?>"/></td>
                        </tr>
                        <tr> 
                            <td ><label class="control-label">Sub Item of</label>
                                <select style="width: 220px" name="subitemof" id="subitemof">
                                    <option value="">&nbsp;&nbsp;</option>
                                    <?php
                                    foreach ($itemlist as $key => $value) {
                                        ?>
                                        <option <?php echo $item["subitemof"] == $value["item_id"] ? "selected" : "" ?> 
                                            value="<?php echo $value["item_id"] ?>"><?php echo $value["item_code"] ?></option>
                                            <?php
                                        }
                                        ?>
                                </select>
                            </td>
                        </tr>
                    </table> 
                </td>
                <td style="width: 17%;">
                    <table class="display nowrap sortable" border="0">
                        <tr >
                            <td style="height: 47px;" ><label class="control-label">Purchase Price <?php echo MysqlConnection::$REQUIRED ?></label>
                                <input style="width: 220px" type="text" name="purchase_rate" pattern="^\d*(\.\d{0,2})?$" onkeypress="return chkNumericKey(event)" onkeyup="calculatePurchaseTax()" id="purchase_rate" value="<?php echo $item["purchase_rate"] ?>" autofocus="" required="true" >
                            </td>
                            <td></td>
                        </tr>

                        <tr style="vertical-align: top">
                            <td colspan="2">
                                <input style="width: 220px" type="hidden" name="total_purch_rate" onkeypress="return chkNumericKey(event)" id="total_purch_rate" value="<?php echo $item["total_purch_rate"] ?>" autofocus="" required="true" minlenght="2" maxlength="5" />
                                <label class="control-label">Description on Purchase Transactions <?php echo MysqlConnection::$REQUIRED ?></label>
                                <textarea style="height: 30px;;line-height: 20px; width: 220px;height: 75px;resize: none" name="item_desc_purch" id="item_desc_purch"   autofocus="" required="true" ><?php echo $item["item_desc_purch"] ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href="#" onclick="copyDescription()" title="Copy purchase description to sales description">Copy >> </a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 17%;">
                    <table class="display nowrap sortable" border="0">
                        <tr >
                            <td style="height: 47px;" ><label class="control-label">Sales Price</label>
                                <input style="width: 220px" type="text" name="sell_rate" id="ipsell_rate"  pattern="^\d*(\.\d{0,2})?$"  onkeypress="return chkNumericKey(event)"  onkeyup="calculateSalesTax()" value="<?php echo $item["sell_rate"] ?>" autofocus=""  ></td>   
                            <td></td>
                        </tr>

                        <tr >
                            <td colspan="2">
                                <input style="width: 220px" type="hidden" name="total_sales_rate" onkeypress="return chkNumericKey(event)" id="total_sales_rate" value="<?php echo $item["total_sales_rate"] ?>" autofocus=""  minlenght="2" maxlength="5" />
                                <label class="control-label">Description on Sales Transactions <?php echo MysqlConnection::$REQUIRED ?></label>
                                <textarea style="height: 30px;;line-height: 20px; width: 220px;height: 75px;resize: none" name="item_desc_sales" id="item_desc_sales"   autofocus=""  ><?php echo $item["item_desc_sales"] ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href="#" onclick="removeDescription()" title="Remove sales description"> << Remove</a>
                            </td>
                        </tr>
                    </table>

                </td>
                <td>
                    <table style="width: 264px;" class="display nowrap sortable" border="0">
                        <tr ><td style="height: 47px;"><label class="control-label">Reorder Point</label><input style="width: 220px" type="text" name="reorder" id="reorder"  value="<?php echo $item["reorder"] ?>" autofocus=""  minlenght="2" onkeypress="return chkNumericKey(event)" maxlength="6" ></td>   </tr>
                        <tr ><td style="height: 47px;"><label class="control-label">On Hand</label><input style="width: 220px" type="text" name="onhand" id="onhand" onkeypress="return chkNumericKey(event)" value="<?php echo $item["onhand"] ?>" autofocus="" minlenght="2" maxlength="6" ></td></tr>
                        <tr ><td style="height: 47px;"> <label class="control-label">On Sales</label><input style="width: 220px" type="text" onkeypress="return chkNumericKey(event)" name="totalvalue" id="totalvalue"  value="<?php echo $item["totalvalue"] ?>" autofocus="" minlenght="2" maxlength="6" ></td>   </tr>
                        <tr ><td style="height: 47px;"><label class="control-label">As of</label><input style="width: 220px" style="vertical-align: bottom" type="text" name="asof"   id="datepicker"  value="<?php echo MysqlConnection::convertToPreferenceDate($item["asof"]) ?>" autofocus=""  minlenght="2" maxlength="30" ></td>   </tr>
                    </table>
                </td>
            </tr>
        </table>

        <div class="modal-footer ">
            <input type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info"   value="SAVE"/>
            <a href="index.php?pagename=manage_itemmaster&status=active" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
        </div> 
    </div>

</form>
<form name="frmImport" id="frmImport" enctype="multipart/form-data" method="post">
    <br/>
    OR
    <br/>
    <br/>
    <table class="display nowrap sortable" border="0">
        <tr>
            <td><label class="control-label">IMPORT</label></td> 
            <td><input style="width: 220px;border:solid 1px gray"  type="file" name="importFile" id="importFile" /></td>
            <td><input type="submit" id="btnImport" name="btnImport" class="btn btn-info"   value="IMPORT"/></td>
        </tr>
    </table>
</form>


<style>
    .ui-datepicker-trigger
    {
        padding-bottom: 5px;
    }

</style>
<script>
<?php if ($item["type"] == "") { ?>
        $(document).ready(function () {
            $('#inventorypartfrom').addClass('hide');
            $('#inventorypartfrom').removeClass('show');
        });
<?php } ?>

    $("#type").click(function () {
        var valueModel = $("#type").val();
        if (valueModel === "Service") {
            $('#serviceform').addClass('show');
            $('#inventorypartfrom').addClass('hide');
            $('#inventorypartfrom').removeClass('show');
        } else if (valueModel === "InventoryPart") {
            $('#serviceform').removeClass('show');
            $('#serviceform').addClass('hide');
            $('#inventorypartfrom').addClass('show');
        }
    });
</script>

<!-- this is custom model dialog --->
<div id="duplicateitem" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <h3>DUPLICATION !!!</h3>
    </div>
    <div class="modal-body">
        <h5 style="color: red">
            Item with this code already exist.<br/>
        </h5>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
    </div>
</div>
<!-- this is model dialog --->


<script>
    function validateDuplicate() {
        var dataString = "item_code=" + $("#item_code").val();
        $.ajax({
            type: 'POST',
            url: 'itemmaster/getitemajax.php',
            data: dataString
        }).done(function (data) {
            var obj = JSON.parse(data);
            var isavailable = obj.item_code;
            if (isavailable !== "" && isavailable !== undefined && isavailable !== "undefined") {
                $('#duplicateitem').modal('show');
                $("#item_code").val("");
                $("#item_code").focus();
            }
        }).fail(function () {
        });
    }

    jQuery(function () {
        var counter = 1;
        jQuery('a.icon-plus').click(function (event) {
            event.preventDefault();
            var newRow = jQuery('<tr>'
                    + '<td><input type="text" name="taxcode[]" style="width: 25px;" id="taxtaxname[]" ></td>'
                    + '<td><input type="text" name="taxtaxname[]" style="width: 75px;" id="taxtaxname[]" ></td>'
                    + '<td><input type="text" name="taxtaxvalues[]" id="taxtaxvalues[]" ></td>'
                    + '<td><input  type="checkbox" name="taxisExempt[]" id="taxisExempt[]"></td>'
                    + '<td><a class="icon-trash" href="#"  ></a></td>'
                    + '</tr>');
            counter++;
            jQuery('#addtax').append(newRow);
        });
    });
    $(document).ready(function () {
        $("#addtax").on('click', 'a.icon-trash', function () {
            $(this).closest('tr').remove();
        });
    });
    $("#taxInformation1").click(function () {
        var valueModel = $("#taxInformation1").val();
        if (valueModel === "0") {
            $('#addTaxInformation').modal('show');
        }
    });
    $("#sales_code").click(function () {
        var valueModel = $("#sales_code").val();
        if (valueModel === "0") {
            $('#addTaxInformation').modal('show');
        }
    });
    $("#purch_code").click(function () {
        var valueModel = $("#purch_code").val();
        if (valueModel === "0") {
            $('#addTaxInformation').modal('show');
        }
    });
    $("#saveTaxInformation").click(function () {
        var taxcode = $("input[name='taxcode[]']").map(function () {
            return $(this).val();
        }).get();
        var taxtaxname = $("input[name='taxtaxname[]']").map(function () {
            return $(this).val();
        }).get();
        var taxtaxvalues = $("input[name='taxtaxvalues[]']").map(function () {
            return $(this).val();
        }).get();
        var taxisExempt = $("input[name='taxisExempt[]']").map(function () {
            return $(this).val();
        }).get();
        var dataString = "taxcode=" + taxcode + "&taxtaxname=" + taxtaxname + "&taxtaxvalues=" + taxtaxvalues + "&taxisExempt=" + taxisExempt;
        $.ajax({
            type: 'POST',
            url: 'customermaster/savetaxinfo_ajax.php',
            data: dataString
        }).done(function (data) {
            $("input[name='taxcode[]']").val("");
            $("input[name='taxtaxname[]']").val("");
            $("input[name='taxtaxvalues[]']").val("");
            $("input[name='taxisExempt[]']").val("");
            $('#taxInformation1').append(data);
            if ($("#purch_code").val() !== "") {
                $('#purch_code').append(data);
            }
            if ($("#sales_code").val() !== "") {
                $('#sales_code').append(data);
            }
        }).fail(function () {
        });
    });
    $("#cancelti").click(function () {
        $("#taxInformation1").val("");
        $("#purch_code").val("");
        $("#sales_code").val("");
    });</script>

<script>
    function calculatePurchaseTax() {
        var purchaseRate = document.getElementById("purchase_rate").value;
        if (purchaseRate === "") {
            purchaseRate = "0.0";
        }
        var purchaseRate = parseFloat(purchaseRate);
        var purchTaxCode = document.getElementById("purch_code").selectedOptions[0].text;
        if (purchTaxCode.trim() !== "" && purchTaxCode !== undefined && purchTaxCode !== "0") {
            var codesplit = purchTaxCode.split("-");
            if (codesplit.length > 1) {
                var percent = parseFloat(codesplit[2]);
                var finalTotal = (purchaseRate + (purchaseRate * (percent / 100)));
                document.getElementById("total_purch_rate").value = parseFloat(Math.round(finalTotal * 100) / 100).toFixed(2);
            } else {
                document.getElementById("total_purch_rate").value = "0.0";
            }
        }
    }
    function calculateSalesTax() {
        var sellRate = document.getElementById("ipsell_rate").value;
        console.log("SELL----------> " + sellRate);
        if (sellRate === "") {
            sellRate = "0.0";
        }
        var sellRate = parseFloat(sellRate);
        var salesTaxCode = document.getElementById("sales_code").selectedOptions[0].text;
        if (salesTaxCode.trim() !== "" && salesTaxCode !== undefined && salesTaxCode !== "0") {
            var codesplit = salesTaxCode.split("-");
            if (codesplit.length > 1) {
                var percent = parseFloat(codesplit[2]);
                var finalTotal = (sellRate + (sellRate * (percent / 100)));
                document.getElementById("total_sales_rate").value = parseFloat(Math.round(finalTotal * 100) / 100).toFixed(2);
            } else {
                document.getElementById("total_sales_rate").value = "0.0";
            }
        }
    }

    function copyDescription() {
        document.getElementById("item_desc_sales").value = document.getElementById("item_desc_purch").value;
    }
    function removeDescription() {
        document.getElementById("item_desc_sales").value = "";
    }
</script>


<?php

function readAndImportCSV($fileuploaded) {
    $file_handle = fopen($fileuploaded, 'r');
    while (!feof($file_handle)) {
        $line_of_text[] = fgetcsv($file_handle, 1024);
    }
    fclose($file_handle);
    return $line_of_text;
}
?>