<?php
$status = filter_input(INPUT_GET, "status");

$searchbyname = filter_input(INPUT_POST, "searchbyname");
$search = filter_input(INPUT_POST, "search");
$searchbyname = trim($searchbyname);

if ($status == "active") {
    $query = "SELECT * FROM `item_master` WHERE status = 'Y' ";
} else if ($status == "inactive") {
    $query = "SELECT * FROM `item_master` WHERE  status = 'N'  ";
} else if ($status == "all") {
    $query = "SELECT * FROM `item_master` WHERE item_id != ''  ";
} else {
    $query = "SELECT * FROM `item_master`  WHERE item_id != ''  ";
}
if ($search == "SEARCH") {
    $orquery = " AND `item_code` LIKE '%$searchbyname%' OR `item_desc_purch` LIKE '%$searchbyname%' OR `item_desc_sales` LIKE '%$searchbyname%' ";
    $query . " $orquery  LIMIT 0,200 ";
}
$listofitems = MysqlConnection::fetchCustom($query . " $orquery  LIMIT 0,200 ");
$itemid = filter_input(INPUT_GET, "itemId");


if (isset($itemid) && $itemid != "") {
    $searcheditem = MysqlConnection::getItemDataById($itemid);
    if ($searcheditem["status"] == "Y") {
        MysqlConnection::delete("UPDATE `item_master` SET status = 'N' WHERE `item_id` = '$itemid' "); // this is for update
        header("location:index.php?pagename=manage_itemmaster&status=inactive");
    } else {
        MysqlConnection::delete("UPDATE `item_master` SET status = 'Y' WHERE `item_id` = '$itemid' "); // this is for update
        header("location:index.php?pagename=manage_itemmaster&status=active");
    }
}


$totalonpo = 0;
$totalonporeceived = 0;
$onpo = 0;
foreach ($resultpo as $key => $valueonpo) {
    $totalonpo = $totalonpo + $valueonpo["qty"];
    $totalonporeceived = $totalonporeceived + $valueonpo["rqty"];
    $onpo = $totalonpo - $totalonporeceived;
}
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader"><h5 style="font-family: verdana;font-size: 12px;clear: both">ITEM LIST</h5></div>
    <br/>
    <form name="searchfrom" id="searchfrom" method="post">
        <table  style="width: 100%" >
            <tr>
                <td>
                    <input type="text" autocomplete="" name="searchbyname" value="<?php echo $searchbyname ?>" style="width: 80%;height: 25px;margin-top: 5px;" autofocus="" placeholder="Enter item code or name to search item" id="searchbyname">
                    <input type="submit" value="SEARCH" name="search" class="btn btn-info"/>
                    <a class="btn btn-info" href="index.php?pagename=create_itemmaster" >ADD ITEM </a>
                </td>
                <td style="float: right">
                    <a href="index.php?pagename=manage_itemmaster&status=active" id="btnSubmitFullForm" class="btn btn-info">VIEW ACTIVE</a>
                    <a href="index.php?pagename=manage_itemmaster&status=inactive" id="btnSubmitFullForm" class="btn btn-info">VIEW INACTIVE</a>
                    <a href="index.php?pagename=manage_itemmaster&status=all" id="btnSubmitFullForm" class="btn btn-info">VIEW ALL</a>
                    <a href="itemmaster/export.php" target="_blank" id="btnSubmitFullForm" class="btn btn-info">EXPORT</a>
                </td>
            </tr>
        </table>
    </form>
    <hr/>
 
       <table id="example" class="display nowrap sortable" style="width:100%">
            <thead>
                <tr>
                    <td style="width: 2%">#</td>
                    <td >Name/Code</td>
                    <td style="width: 60%">Description</td>
                    <td>Unit</td>
                    <td >On Hand</td>
                    <td >On Sales</td>
                    <td >On PO</td>
                    <td >Currency</td>
                    <td >Price</td>
                </tr>
            </thead>
            <tbody >
                <?php
                $index = 1;
                foreach ($listofitems as $key => $value) {
                    if ($value["onhand"] <= $value["reorder"]) {
                        $reorderclr = "color: red;font-weight: bold";
                    } else {
                        $reorderclr = "";
                    }
                    $bgcolor = MysqlConnection::generateBgColor($index);
                    if ($value["status"] == "N") {
                        $bgcolor = "rgb(253,232,217)";
                    }


                    $resultpo = getPurchaseItemsDetails($value["item_id"]);
                    $totalonpo = 0;
                    $totalonporeceived = 0;
                    $onpo = 0;
                    foreach ($resultpo as $key => $valueonpo) {
                        $totalonpo = $totalonpo + $valueonpo["qty"];
                        $totalonporeceived = $totalonporeceived + $valueonpo["rqty"];
                        $onpo = $totalonpo - $totalonporeceived;
                    }
                    ?>
                    <tr id="<?php echo $value["item_id"] ?>" class="context-menu-one"  style="background-color: <?php echo $bgcolor ?>;vertical-align: middle" >
                        <td ><?php echo $index ?></td>
                        <td  style="text-transform: uppercase" >&nbsp;<?php echo $value["item_code"] ?>&nbsp;<?php echo $value["item_name"] ?></td>
                        <td style="text-transform: capitalize" > 
                            <p style="padding: 5px;line-height: 25px;text-align: justify">
                                <?php echo $value["item_desc_purch"] == "" ? $value["item_desc_sales"] : $value["item_desc_purch"] ?> 
                            </p>
                        </td>
                        <td style="text-transform: uppercase">&nbsp;<?php echo $value["unit"] ?></td>
                        <td style="text-align: right;background-color: #D7CEC7;<?php echo $reorderclr ?>"><?php echo $value["onhand"]; ?>&nbsp;&nbsp;</td>
                        <td style="text-align: right"><?php echo $value["totalvalue"]; ?>&nbsp;&nbsp;</td>
                        <td style="text-align: right"><a href="index.php?pagename=view_itemmaster&itemId=<?php echo $value["item_id"]  ?>&flags=po"><?php echo $onpo ?>&nbsp;&nbsp;</a></td>
                        <td style="text-align: right"><?php echo $value["currency"]; ?>&nbsp;&nbsp;</td>
                        <td style="text-align: right">&nbsp;<?php echo $value["sell_rate"]; ?>&nbsp;</td>
                    </tr>
                    <?php
                    $index++;
                }
                ?>
            </tbody>
        </table>
    
</div>

<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_item":
                        window.location = "index.php?pagename=view_itemmaster&itemId=" + id;
                        break;
                    case "add_item":
                        window.location = "index.php?pagename=create_itemmaster";
                        break;
                    case "edit_item":
                        window.location = "index.php?pagename=create_itemmaster&itemId=" + id;
                        break;
                    case "delete_item":
                        window.location = "index.php?pagename=view_itemmaster&itemId=" + id + "&flag=yes";
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    case "inactive":
                        window.location = "index.php?pagename=manage_itemmaster&itemId=" + id;
                        break;
                    case "changeprice":
                        window.location = "index.php?pagename=create_itemmaster&itemId=" + id + "&flag=price";
                        break;
                    case "adjustquantity":
                        window.location = "index.php?pagename=create_itemmaster&itemId=" + id + "&flag=qty";
                        break;
                    case "purchase_order":
                        window.location = "index.php?pagename=create_perchaseorder&itemId=" + id + "&flag=purchase";
                        break;

                    case "sales_order":
                        window.location = "index.php?pagename=createitem_salesorder&itemId=" + id + "&flag=sales";
                        break;

                    case "received_items":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    case "create_invoice":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_itemmaster";
                }
            },
            items: {
                "view_item": {name: "VIEW ITEM", icon: ""},
                "edit_item": {name: "EDIT ITEM", icon: ""},
                "delete_item": {name: "DELETE ITEM", icon: ""},
                "inactive": {name: "ITEM ACTIVE/INACTIVE", icon: ""},
                "sep0": "---------",
                "changeprice": {name: "CHANGE PRICE", icon: ""},
                "adjustquantity": {name: "ADJUST QUANTITY", icon: ""},
                "purchase_order": {name: "CREATE PURCHASE ORDER", icon: ""},
                "sales_order": {name: "CREATE SALES ORDER", icon: ""}
            }
        });
    });

    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_itemmaster&itemId=" + id;
        }
    });


</script>
<?php

function getPurchaseItemsDetails($itemid) {
    $query = "SELECT"
            . " pi.po_id ,"
            . " pi.qty ,"
            . " pi.rqty ,"
            . " po.supplier_id ,"
            . " po.purchaseOrderId ,"
            . " po.isOpen "
            . "FROM"
            . " purchase_item pi ,"
            . " purchase_order po"
            . " WHERE pi.po_id = po.id"
            . " AND pi.item_id = '$itemid'"
            . " AND po.isOpen = 'Y'"
            . " ORDER BY po.purchasedate DESC LIMIT 0,10";
    return MysqlConnection::fetchCustom($query);
}
?>