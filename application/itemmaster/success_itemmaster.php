
<?php
$flag = filter_input(INPUT_GET, "flag");
?>
<?php
if ($flag == "noitems") {
    ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-error" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>ERROR!</strong> 
                    SORRY :( !! YOU DON'T HAVE ANY ITEMS IN STOCK, PLEASE ADD ITEMS. 
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=create_itemmaster" id="btnSubmitFullForm" class="btn btn-info">ADD MORE ITEMS</a></td>

                            <td><a href="index.php?pagename=manage_itemmaster&status=active" id="btnSubmitFullForm" class="btn btn-info">GO TO ITEM LIST</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>

    <?php
} else {
    ?>

    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Item data has been added successfully!!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=create_itemmaster" id="btnSubmitFullForm" class="btn btn-info">ADD MORE ITEMS</a></td>
                            <td><a href="index.php?pagename=manage_itemmaster&status=active" id="btnSubmitFullForm" class="btn btn-info">GO TO ITEM LIST</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>

    <?php
}
?>