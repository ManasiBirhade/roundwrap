<?php
$itemid = filter_input(INPUT_GET, "itemId");
$flag = filter_input(INPUT_GET, "flag");
$flags = filter_input(INPUT_GET, "flags");
$item = MysqlConnection::getItemDataById($itemid);

if (isset($_POST["deleteItem"])) {
    MysqlConnection::delete("DELETE FROM `item_master` WHERE item_id = '$itemid'");
    header("location:index.php?pagename=manage_itemmaster&status=active");
}

if ($flags == "po") {
    $active1 = "";
    $activelast = "active";
} else {
    $active1 = "active";
    $activelast = "";
    $activesecond = "";
}

$purchasetaxcode = MysqlConnection::getTaxInfoById($item["purch_code"]);
$saletaxcode = MysqlConnection::getTaxInfoById($item["sales_code"]);
$vendorname = MysqlConnection::getSupplierDetails($item["vendorid"]);

$resultpo = getPurchaseItemsDetails($itemid);
$totalonpo = 0;
$totalonporeceived = 0;
$onpo = 0;
foreach ($resultpo as $key => $valueonpo) {
    $totalonpo = $totalonpo + $valueonpo["qty"];
    $totalonporeceived = $totalonporeceived + $valueonpo["rqty"];
    $onpo = $totalonpo - $totalonporeceived;
}
?>

<style>
    tbody {
        height: auto;
    }
    select {
        width: 212px;
        height: 24px;
    }
    tr{
        /*background-color: rgb(240,240,240);*/
    }
    .sortable input,textarea{
        background-color: white;
    }
</style>
<div class="container-fluid"  >
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">ITEM VIEW</h5>
    </div>
    <?php
    if ($flag == "yes") {
        echo MysqlConnection::$DELETE;
    }
    ?>
    <div class="widget-box" style="border-bottom: solid 1px #CDCDCD;background-color: white">
        <div class="widget-title">
            <ul class="nav nav-tabs">
                <li class="<?php echo $active1 ?>"><a data-toggle="tab" href="#tab5" >ITEM DETAIL</a></li>
                <li class="<?php echo $activesecond ?>"><a data-toggle="tab" href="#tab6">ON SALES ORDER</a></li>
                <li class="<?php echo $activelast ?>"><a data-toggle="tab" href="#tab7">ON PURCHASE ORDER</a></li>
            </ul>
        </div>
        <div class="widget-content tab-content">
            <div id="tab5" class="tab-pane <?php echo $active1 ?>">
                <table border="0"> 
                    <tr style="vertical-align: top">
                        <td >
                            <table class="display nowrap sortable"   style="vertical-align: top" border="0">
                                <tr style="vertical-align: top"><td >Currency<br/><input style="width: 220px"  type="text"  value="<?php echo getcurrency($item["currency"]) . " (" . $item["currency"] . ")" ?>" readonly=""/></td></tr>
                                <tr style="vertical-align: top"><td >Preferred Vendor<br/><input style="width: 220px"  type="text" value="<?php echo $vendorname[0]["companyname"] ?>" readonly=""/></td></tr>
                                <tr style="vertical-align: top"><td >Item Code<br/><input style="width: 220px"   type="text"  value="<?php echo $item["item_code"] ?>" readonly="" /></td></tr>
                                <tr style="vertical-align: top"><td >Unit of Measures<br/><input style="width: 220px"  type="text"  value="<?php echo $item["unit"] ?>" readonly=""/></td></tr>
                                <tr style="vertical-align: top"><td >Sub Item of<br/><input style="width: 220px"  type="text"  value="<?php echo implode(",", $subitemof[0]) ?>" readonly=""/></td></tr>
                            </table> 
                        </td>
                        <td>
                            <table  border="0" class="display nowrap sortable" >
                                <tr >
                                    <td style="width: 40%; "><label class="control-label">Purch Price</label>
                                </tr>
                                <tr >
                                    <td>
                                        <input style="width: 220px;background-color: white"   type="text" value="<?php echo $item["purchase_rate"] ?>" readonly=""/>
                                    </td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td colspan="2"><label class="control-label">Description on Purchase Transactions</label></td>
                                </tr>
                                <tr style="vertical-align: top">
                                    <td>
                                        <textarea style="height: 60px;;line-height: 20px; width: 220px;height: 75px;resize: none" readonly=""><?php echo $item["item_desc_purch"] ?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table  border="0" class="display nowrap sortable" >
                                <tr >
                                    <td style="width: 40%"><label class="control-label">Sales Price</label></td> 
                                </tr>
                                <tr >
                                    <td><input style="width: 220px"  type="text"  value="<?php echo $item["sell_rate"] ?>" readonly=""/></td>
                                </tr>
                                <tr >
                                    <td colspan="2">
                                        <label class="control-label">Description on Sales Transactions</label>
                                    </td>
                                </tr>
                                <tr >
                                    <td>
                                        <textarea style="height: 60px;;line-height: 20px; width: 220px;height: 75px;resize: none"  readonly=""><?php echo $item["item_desc_sales"] ?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table  border="0" class="display nowrap sortable" >

                                <tr >
                                    <td>
                                        <label class="control-label">Reorder Point</label>
                                        <br/>
                                        <input style="width: 220px"  type="text"  value="<?php echo $item["reorder"] ?>" readonly=""/>
                                    </td>   
                                </tr>
                                <tr >
                                    <td>
                                        <label class="control-label">On Hand</label><br/>
                                        <input style="width: 220px"  type="text"  value="<?php echo $item["onhand"] ?>" readonly="" />
                                    </td>   
                                </tr>
                                <tr >
                                    <td>
                                        <label class="control-label">On Sales</label><br/>
                                        <input style="width: 220px"  type="text"  value="<?php echo $item["totalvalue"] ?>" readonly="" />
                                    </td>   
                                </tr>
                                <tr>
                                    <td>
                                        <label class="control-label">On PO</label><br/>
                                        <input style="width: 220px"  type="text" value="<?php echo $onpo ?>" readonly="" />
                                    </td>   
                                </tr>
                                <tr >
                                    <td>
                                        <label class="control-label">As of</label><br/>
                                        <input style="width: 220px"  type="text"   value="<?php echo MysqlConnection::convertToPreferenceDate($item["asof"]) ?>" readonly="" />
                                    </td>   
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="tab6"  class="tab-pane <?php echo $activesecond ?>">
                <table id="example" class="display nowrap sortable" style="width:100%">
                    <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white">
                        <td>SO NO</td>
                        <td>PO NO</td>
                        <td>CUSTOMER NAME</td>
                        <td>ORDERED</td>
                        <td>RECEIVED</td>
                        <td>BACK</td>
                    </tr>
                    <?php
                    $resultset = getSalesItemsDetails($itemid);
                    $totalordered = 0;
                    $totalreceived = 0;
                    $totalbackorder = 0;
                    foreach ($resultset as $key => $value) {
                        $customername = getCustomerName($value["customer_id"]);

                        $totalordered = $totalordered + $value["qty"];
                        $totalreceived = $totalreceived + $value["rQty"];
                        $totalbackorder = $totalbackorder + abs($value["backQty"]);
                        ?>
                        <tr>
                            <td>
                                <a target="_blank" href="index.php?pagename=view_salesorder&salesorderid=<?php echo $value["so_id"] ?>">
                                    <?php echo $value["sono"] ?>
                                </a>
                            </td>
                            <td>
                                <a target="_blank" href="index.php?pagename=view_salesorder&salesorderid=<?php echo $value["so_id"] ?>">
                                    <?php echo $value["pono"] ?>
                                </a>
                            </td>
                            <td><?php echo $customername ?></td>
                            <td><?php echo $value["qty"] ?></td>
                            <td><?php echo $value["rQty"] ?></td>
                            <td><?php echo abs($value["backQty"]) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr style="background-color: rgb(240,240,240);font-weight: bold">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><?php echo $totalordered ?></td>
                        <td><?php echo $totalreceived ?></td>
                        <td><?php echo $totalbackorder ?></td>
                    </tr>
                </table>
            </div>
            <div id="tab7" class="tab-pane <?php echo $activelast ?> ">
                <table id="example" class="display nowrap sortable" style="width:100%">
                    <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white">
                        <td>PO NO</td>
                        <td>SUPPLIER NAME</td>
                        <td>ORDERED</td>
                        <td>RECEIVED</td>
                    </tr>
                    <?php
                    $resultsetpo = getPurchaseItemsDetails($itemid);
                    $totalpoordered = 0;
                    $totalporeceived = 0;
                    $totalpobackorder = 0;
                    foreach ($resultsetpo as $key => $valuepo) {
                        $suppliername = getSupplierName($valuepo["supplier_id"]);
                        $totalpoordered = $totalpoordered + $valuepo["qty"];
                        $totalporeceived = $totalporeceived + $valuepo["rqty"];
                        ?>
                        <tr>
                            <td>
                                <a target="_blank" href="index.php?pagename=view_perchaseorder&purchaseorderid=<?php echo $valuepo["po_id"] ?>">
                                    <?php echo $valuepo["purchaseOrderId"] ?>
                                </a>
                            </td>
                            <td><?php echo $suppliername ?></td>
                            <td><?php echo $valuepo["qty"] ?></td>
                            <td><?php echo $valuepo["rqty"] ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr style="background-color: rgb(240,240,240);font-weight: bold">
                        <td></td>
                        <td></td>
                        <td><?php echo $totalpoordered ?></td>
                        <td><?php echo $totalporeceived ?></td>
                    </tr>
                </table>
            </div>
        </div>


        <div class="modal-footer ">
            <?php
            if (isset($flag) && $flag != "") {
                ?>
                <form name="frmDeleteItem" id="frmDeleteItem" method="post">
                    <a href="index.php?pagename=manage_itemmaster&status=active" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                    <input  type="submit" value="DELETE" name="deleteItem" class="btn btn-info"/>
                    <input type="hidden" name="itemid" value="<?php echo $itemid ?>"/>
                </form>
                <?php
            } else {
                echo '<a href="index.php?pagename=manage_itemmaster&status=active" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>';
            }
            ?>
        </div>
    </div>
</div>

<?php

function getSalesItemsDetails($itemid) {
    $query = "SELECT"
            . " si.so_id ,"
            . " si.qty ,"
            . " si.rQty ,"
            . " si.backQty ,"
            . " si.discrepancycount ,"
            . " si.discrepancy ,"
            . " so.customer_id ,"
            . " so.sono ,"
            . " so.pono "
            . "FROM"
            . " sales_item si ,"
            . " sales_order so"
            . " WHERE si.so_id = so.id"
            . " AND si.item_id = '$itemid'"
            . " AND so.isOpen = 'Y'"
            . " ORDER BY so.entrydate DESC LIMIT 0,10";
    return MysqlConnection::fetchCustom($query);
}

function getCustomerName($customerid) {
    $query = "SELECT cust_companyname FROM `customer_master` WHERE `id` = '$customerid'";
    $resultset = MysqlConnection::fetchCustom($query);
    return $resultset[0]["cust_companyname"];
}

function getSupplierName($supplierid) {
    $query = "SELECT companyname FROM `supplier_master` WHERE `supp_id` = '$supplierid'";
    $resultset = MysqlConnection::fetchCustom($query);
    return $resultset[0]["companyname"];
}

function getPurchaseItemsDetails($itemid) {
    $query = "SELECT"
            . " pi.po_id ,"
            . " pi.qty ,"
            . " pi.rqty ,"
            . " po.supplier_id ,"
            . " po.purchaseOrderId ,"
            . " po.isOpen "
            . "FROM"
            . " purchase_item pi ,"
            . " purchase_order po"
            . " WHERE pi.po_id = po.id"
            . " AND pi.item_id = '$itemid'"
            . " AND po.isOpen = 'Y'"
            . " ORDER BY po.purchasedate DESC LIMIT 0,10";
    return MysqlConnection::fetchCustom($query);
}
?>