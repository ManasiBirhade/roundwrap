



$(function() {
    setTimeout(function() {
        $("#successMessage").hide('blind', {}, 100)
    }, 5000);
});

function convertFormToJSON(form) {
    var array = $(form).serializeArray();
    var json = {};

    jQuery.each(array, function() {
        json[this.name] = this.value || '';
    });

    return json;
}


function chkNumericKey(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode === 46 || charCode === 45) {
        return true;
    } else {
        return false;
    }
}

function deleteRow(rowid)  
{   
    var row = document.getElementById(rowid);
    row.parentNode.removeChild(row);
}
//
//$(function() {
//    $("#datepicker").datepicker({
//        minDate: -20,
//        maxDate: "+1M +360D"
//    });
//    $("#datepicker").datepicker("option", "dateFormat", "yy-mm-dd");
//});
//
//$(function() {
//    $("#datepicker1").datepicker({
//        minDate: -20,
//        maxDate: "+1M +360D"
//    });
//    $("#datepicker1").datepicker("option", "dateFormat", "yy-mm-dd");
//});
//
//$(function() {
//    $("#datepicker2").datepicker({
//        minDate: -20,
//        maxDate: "+1M +360D"
//    });
//    $("#datepicker2").datepicker("option", "dateFormat", "yy-mm-dd");
//});
//
//$(function() {
//    $("#datepickerAllowAny1").datepicker({
//        maxDate: "+1M +360D"
//    });
//    $("#datepickerAllowAny1").datepicker("option", "dateFormat", "yy-mm-dd");
//});
//
//$(function () {
//    $("#datepickerAllowAny2").datepicker({
//        maxDate: "+1M +360D"
//    });
//    $("#datepickerAllowAny2").datepicker("option", "dateFormat", "yy-mm-dd");
//});

$("#data tr").click(function() {
    var selected = $(this).hasClass("highlight");
    $("#data tr").removeClass("highlight");
    if (!selected)
        $(this).addClass("highlight");
});
$("#data").delegate("tr", "contextmenu", function(e) {
    var selected = $(this).hasClass("highlight");
    $("#data tr").removeClass("highlight");
    if (!selected)
        $(this).addClass("highlight");
});


// This function is called from the pop-up menus to transfer to
// a different page. Ignore if the value returned is a null string:
function goPage(newURL) {

    // if url is empty, skip the menu dividers and reset the menu selection to default
    if (newURL !== "") {

        // if url is "-", it is this page -- reset the menu:
        if (newURL === "-") {
            resetMenu();
        }
        // else, send page to designated URL            
        else {
            document.location.href = newURL;
        }
    }
}

// resets the menu selection upon entry to this page:
function resetMenu() {
    document.gomenu.selector.selectedIndex = 2;
}
$(window).load(function() {
    $('#overlay1').fadeOut();
});


$(document).ready(function() {
    $('input').keyup(function(e) {
//                if (e.which === 39) { // right arrow
//                    $(this).closest('td').next().find('input').focus();
//
//                } else if (e.which === 37) { // left arrow
//                    $(this).closest('td').prev().find('input').focus();
//
//                } else if (e.which === 40) { // down arrow
//                    $(this).closest('tr').next().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();
//
//                } else if (e.which === 38) { // up arrow
//                    $(this).closest('tr').prev().find('td:eq(' + $(this).closest('td').index() + ')').find('input').focus();
//                }
    });
});


//uncomment to prevent on startup
//removeDefaultFunction();          
/** Prevents the default function such as the help pop-up **/
function removeDefaultFunction()
{
    window.onhelp = function() {
        return false;
    }
}
/** use keydown event and trap only the F-key, 
 but not combinations with SHIFT/CTRL/ALT **/
$(window).bind('keydown', function(e) {
    //This is the F1 key code, but NOT with SHIFT/CTRL/ALT
    var keyCode = e.keyCode || e.which;
    if ((keyCode == 112 || e.key == 'F1') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Open help window here instead of alert
        window.location = "index.php?pagename=create_itemmaster";
    }
    // Add other F-keys here:
    else if ((keyCode == 113 || e.key == 'F2') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_customermaster";
    }
    else if ((keyCode == 113 || e.key == 'F3') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_suppliermaster";
    }
    else if ((keyCode == 113 || e.key == 'F4') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_perchaseorder";
    }
    else if ((keyCode == 113 || e.key == 'F5') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_salesorder";
    }
    else if ((keyCode == 113 || e.key == 'F6') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_packingslip";
    }
    else if ((keyCode == 113 || e.key == 'F7') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_quotation";
    }
    else if ((keyCode == 113 || e.key == 'F8') &&
            !(event.altKey || event.ctrlKey || event.shiftKey || event.metaKey))
    {
        // prevent code starts here:
        removeDefaultFunction();
        e.cancelable = true;
        e.stopPropagation();
        e.preventDefault();
        e.returnValue = false;
        // Do something else for F2
        window.location = "index.php?pagename=create_workorder";
    }
});


