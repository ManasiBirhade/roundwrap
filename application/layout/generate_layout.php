<?php
include '../MysqlConnection.php';
error_reporting(0);
        const KERFVALUE = 4.7625;
$packslipid = filter_input(INPUT_GET, "packslipid");

$resulset = MysqlConnection::getPackSlipFromId($packslipid);
$profileresult = MysqlConnection::getPortfolioProfileById($resulset["prof_id"]);

if ($profileresult["islam"] != 'Y') {
    echo "<p align='center' style='color:red'>Layout is not available for this profile</p><br/>";
    exit();
}



$sql = "SELECT islam FROM `tbl_portfolioprofile` WHERE id = '8c7442a81f5b539b96c59619d173521d'";

function getImperialList() {
    $arraymax[0] = "381.00";
    $arraymax[1] = "609.6";
    $arraymax[2] = "812.8";
    $arraymax[3] = "1066.8";
    $arraymax[4] = "1219.2";
    $arraymax[5] = "1320.8";
    $arraymax[6] = "1625.6";
    $arraymax[7] = "1828.8";
    $arraymax[8] = "2082.8";
    $arraymax[9] = "2438.4";
    return $arraymax;
}

function getWidths($psid) {
    $query = "SELECT * FROM `packslipdetail` WHERE `ps_id` = '$psid' ORDER BY `mm_w` asc";
    $data = MysqlConnection::fetchCustom($query);
    $totalps = 0;
    $final = array();
    foreach ($data as $key => $value) {
        $filterarray = array();
        $filterarray["mm_w"] = $mm_w = round($value["mm_w"], 1);
        $filterarray["mm_h"] = $mm_h = round($value["mm_h"], 1);
        $filterarray["kerfheight"] = $kerfheight = round(KERFVALUE + $mm_h, 1);
        $filterarray["pcs"] = $pcs = $value["pcs"];
        $filterarray["calculatedsize"] = $calculatedsize = round(($pcs * ($mm_h + $kerfheight)), 1);
        $emperial = getImperialList();
        $valge = 0;
        foreach ($emperial as $key => $value) {
            if ($value >= $calculatedsize) {
                $valge = $value;
                break;
            }
        }
        $filterarray["valge"] = $valge;
        array_push($final, $filterarray);
    }
    return $final;
}

if ($packslipid != "") {
    $datavalue = getWidths($packslipid);
} else {
    exit;
}
?>
<style>
    *{
        font-size: 12px;
        padding: 0px;
        margin: 0px;
    }
</style>
<body style="font-size: 10px;" onload="window.print()" >
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <table border="0" style="width: 100%;margin: 10px;" >
                    <?php
                    $forslize = (count($datavalue) / 4);
                    for ($index = 0; $index <= $forslize; $index++) {
                        $innerInitInd = $index * 4;
                        $outInitInd = ($index * 4) + 4;
                        $fixCount = 1;
                        ?>
                        <tr>
                            <?php
                            $totpc = 0;
                            for ($indexFor = $innerInitInd; $indexFor < $outInitInd; $indexFor++) {
                                $extractedvalue = $datavalue[$index + $indexFor];
                                $totpc = $extractedvalue["pcs"] + $totpc;
                                ?>
                                <td style="font-weight: bold;">
                                    <table border="0">
                                        <tr>
                                            <td></td>
                                            <td style="text-align: right;"><p style=""><?php echo $extractedvalue["mm_w"] ?></p></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top; text-align: right;"><?php echo $extractedvalue["pcs"] ?>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            <td>
                                                <div style="float: left; width: 50px; height: 100px; border: solid 1px; margin: 2px; border-right: solid 0px;">
                                                    <div style="text-align: right"><?php echo $extractedvalue["mm_h"] ?></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="color: green;"><?php echo $extractedvalue["calculatedsize"] ?>&nbsp;</td>
                                            <td style="color: blue;text-align: right"><?php echo $extractedvalue["valge"] ?></td>
                                        </tr>
                                    </table>
                                </td>
                                <?php
                            }
                            ?>
                            <td style="width: 5px;font-weight: bold;text-align: right;"><?php echo $totpc ?>&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</body>