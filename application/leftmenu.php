<?php
$userid = $_SESSION["user"]["user_id"];
$mastermenus = MysqlConnection::fetchCustom("SELECT submenu FROM `tbl_user_menu_mapping` WHERE `userid` = '$userid' AND `mainmenu` = 'master' ORDER BY submenu");
$retailmenus = MysqlConnection::fetchCustom("SELECT submenu FROM `tbl_user_menu_mapping` WHERE `userid` = '$userid' AND `mainmenu` = 'retial' ORDER BY submenu");
$productionmenus = MysqlConnection::fetchCustom("SELECT submenu FROM `tbl_user_menu_mapping` WHERE `userid` = '$userid' AND `mainmenu` = 'production' ORDER BY submenu");
$systemmenus = MysqlConnection::fetchCustom("SELECT submenu FROM `tbl_user_menu_mapping` WHERE `userid` = '$userid' AND `mainmenu` = 'system' ORDER BY submenu");

function showMenus($menus, $type) {

    $arraymenu = getMenuSettingArray($type);

    foreach ($arraymenu as $displaykey => $display) {
        foreach ($menus as $dbmenu) {
            if ($displaykey == $dbmenu["submenu"]) {

                $menuurl = $display[0];
                $menuname = ucwords(str_replace("_", " ", $dbmenu["submenu"]));
                echo "<li><a href=index.php?pagename=$menuurl>$menuname</a></li>";
            }
        }
    }
}
?>
<link rel="stylesheet" href="css/maruti-style.css" type="text/css"/>
<a href="index.php?pagename=manage_" class="visible-phone"><i class="icon icon-home"></i> Main Screen</a>
<ul >
    <li style="text-transform: capitalize;" >
        <a href="#"><i class="icon  icon-user"></i><span>Hi <?php echo $_SESSION["user"]["firstName"] ?>&nbsp; <?php echo $_SESSION["user"]["lastName"] ?></span></a>
    </li>
    <li class="active"><a href="index.php?pagename=manage_dashboard"><i class="icon icon-home"></i> <span> Main Screen</span></a> </li>

    <li > 
        <a><i class="icon icon-th-large"></i> <span>Masters</span></a> 
        <ul><?php echo showMenus($mastermenus, "master") ?></ul>
    </li>
    <li > 
        <a ><i class="icon icon-inbox"></i> <span>Retail</span></a> 
        <ul><?php echo showMenus($retailmenus, "retail") ?></ul>
    </li>
    <li> 
        <a ><i class="icon icon-inbox"></i> <span>Production</span></a> 
        <ul><?php echo showMenus($productionmenus, "production") ?></ul>
    </li>
    <li >
        <a><i class="icon icon-list-alt"></i> <span>System</span></a>
        <ul><?php echo showMenus($systemmenus, "system") ?></ul>
    </li>
    <li > <a ><i class="icon icon-inbox"></i> <span>Reports</span></a> 
        <ul>
            <li><a href="index.php?pagename=so_report">Sales Order Report</a></li>
            <li><a href="index.php?pagename=po_report">Purchase Order Report</a></li>
            <li><a href="index.php?pagename=ps_report">Packing Slip Report</a></li>
            <li><a href="index.php?pagename=qo_report">Quotation Report</a></li>
            <li><a href="index.php?pagename=wo_report">Work Order Report</a></li>
        </ul>
    </li>
    <li   ><a href="logout.php"><i class="icon  icon-off"></i><span>Logout</span> </a></li>

</ul>

<?php

function getMenuSettingArray($menu) {
    $array = array();
    $array["master"]["customer_master"] = ["manage_customermaster&status=active"];
    $array["master"]["vendor_master"] = ["manage_suppliermaster&status=active"];
    $array["master"]["item_master"] = ["manage_itemmaster&status=active"];
    $array["master"]["profile_master"] = ["manage_profilemaster"];
    $array["master"]["tax_info_master"] = ["manage_taxinfo"];
    $array["master"]["edit_master"] = ["manage_preferencemaster"];
    $array["master"]["step_integration"] = ["manage_profilesteps"];
    $array["master"]["profile_price"] = ["manage_profileprice"];
    $array["master"]["pvc_preferencemaster"] = ["manage_pcvpreferencemaster"];

    $array["retail"]["sales_order"] = ["manage_salesorder&status=N"];
    $array["retail"]["purchase_orders"] = ["manage_perchaseorder"];
    $array["retail"]["back_order"] = ["manage_salesorder&isBackOrder=Y"];
    $array["retail"]["retail_invoice"] = ["view_retailinvoice"];
    $array["retail"]["item_discrepancy"] = ["view_itemdiscrepancy"];

    $array["production"]["packing_slip"] = ["manage_packingslip&status=active"];
    $array["production"]["quotation"] = ["manage_quotation"];
    $array["production"]["work_order"] = ["manage_workorder"];
    $array["production"]["production_invoice"] = ["manage_invoice"];

    $array["system"]["user_management"] = ["manage_usermanagement"];
    $array["system"]["company_information"] = ["manage_companymaster"];
    $array["system"]["scanner_details"] = ["manage_scannerdetail"];
    $array["system"]["email_setup"] = ["manage_emailsetup"];
    $array["system"]["excel_import"] = ["excel_import"];
    $array["system"]["update_password"] = ["password_usermanagement"];

    return $array[$menu];
}
?>