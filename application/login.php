<?php
error_reporting(0);
session_start();
ob_start();

include './MysqlConnection.php';
include './ApplicationUtil.php';

if ($_SESSION["user"]["user_id"] != "") {
    header("location:index.php");
}
if (isset($_POST["go"])) {
    $username = filter_input(INPUT_POST, "email");
    $password = md5(filter_input(INPUT_POST, "password"));
    $sqllogin = "SELECT * from user_master  where username = '$username' and password = '$password';";
    $sqlloginresultset = MysqlConnection::fetchCustom($sqllogin);
    $user = $sqlloginresultset[0];
    if ($user["user_id"] == "" && $user["roleName"] == "") {
        $error = "Invalid username or password!!!";
    } else {
        $isLogin = $user["isLogin"];
        $userrole = $user["roleName"];

        if ($userrole == "ddb3876b85296e1dbf6ea823287dacf5") {
            if ($isLogin == "Y") {
                $error = "You have already logged in other browser,<br/>please logout and then try again!.<br/>"
                        . "<a href='forcelogout.php?email=" . $user["username"] . "'>Logout??<a>";
            } else {
//                $_SESSION["user"] = $user;
//                $_SESSION["time"] = date("d-M-Y h:i:s a");
                MysqlConnection::delete("UPDATE `user_master` SET `isLogin` = 'Y' WHERE `username` = '$username'");

                $arrayhistory = array();
                $arrayhistory["accountname"] = $user["username"];
                $arrayhistory["logintime"] = date("Y-m-d h:i:s");
                $arrayhistory["logouttime"] = date("Y-m-d h:i:s");
                $arrayhistory["duration"] = "0";
                $arrayhistory["logindate"] = date("Y-m-d");
                $arrayhistory["ipaddress"] = $_SERVER['REMOTE_ADDR'];
                $resultset = MysqlConnection::fetchCustom("SELECT id FROM tbl_login_history WHERE logindate = '" . date("Y-m-d") . "' AND  accountname = '$username' ");
                if ($resultset[0]["id"] == "") {
                    MysqlConnection::insert($TBL_LOGIN_HISTORY, $arrayhistory);
                }
                header("location:index.php");
            }
            header("location:../user/index.php");
        } else {
            if ($isLogin == "Y") {
                $error = "You have already logged in other browser,<br/>please logout and then try again!.<br/>"
                        . "<a href='forcelogout.php?email=" . $user["username"] . "'>Logout??<a>";
            } else {
                $_SESSION["user"] = $user;
                $_SESSION["time"] = date("d-M-Y h:i:s a");
                MysqlConnection::delete("UPDATE `user_master` SET `isLogin` = 'Y' WHERE `username` = '$username'");

                $arrayhistory = array();
                $arrayhistory["accountname"] = $user["username"];
                $arrayhistory["logintime"] = date("Y-m-d h:i:s");
                $arrayhistory["logouttime"] = date("Y-m-d h:i:s");
                $arrayhistory["duration"] = "0";
                $arrayhistory["logindate"] = date("Y-m-d");
                $arrayhistory["ipaddress"] = $_SERVER['REMOTE_ADDR'];
                $resultset = MysqlConnection::fetchCustom("SELECT id FROM tbl_login_history WHERE logindate = '" . date("Y-m-d") . "' AND  accountname = '$username' ");
                if ($resultset[0]["id"] == "") {
                    MysqlConnection::insert($TBL_LOGIN_HISTORY, $arrayhistory);
                }
                header("location:index.php");
            }
        }
    }
}
?>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
        <title>LOGIN | ROUND WRAP INDUSTRIES LTD RICHMOND BC</title>
        <link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/styles.css" />
    </head>
    <body>
        <section class="container" style="margin-top: 80px;">
            <section class="login-form">
                <form method="post" role="login">
                    <h2>Please sign in</h2>
                    <p>To enter in your private dashboard</p>
                    <p style="color: red"><?php echo $error ?></p>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="text-primary glyphicon glyphicon-arrow-down"></span></div>
                            <select class="form-control">
                                <option>Round Wrap</option>
                                <option>--</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="text-primary glyphicon glyphicon-envelope"></span></div>
                            <input type="email" name="email" placeholder="Email address" required class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="text-primary glyphicon glyphicon-lock"></span></div>
                            <input type="password" name="password" placeholder="Password" required class="form-control" />
                        </div>
                    </div>
                    <button type="submit" name="go" class="btn btn-block btn-success">Sign in</button>
                    <a href="forpsw.php">Forgot Password?</a>
                </form>
            </section>
        </section>
    </body>
</html>