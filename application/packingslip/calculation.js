function getDecimalFromFraction(fraction) {
    var num = "";
    var numr = "1";
    var denum = "1";
    var fract = "1";
    var decimal = "";
    var fractchng = $.trim(fraction);
    if (fractchng.indexOf(" ") > -1) {
        var arr = fractchng.split(" ");
        num = arr[0];
        fract = arr[1];
    } else {
        fract = fractchng;
    }
    if (fract.indexOf("/") > -1) {
        var arr1 = fract.split("/");
        numr = arr1[0];
        denum = arr1[1];
        if (num !== "") {
            decimal = ((num * denum) + Number(numr)) / denum;
        } else {
            decimal = (Number(numr)) / denum;
        }
    }
    return decimal;
}
function getFractionFromDecimal(decimal) {
    console.log("Decivalue::" + decimal);
    var curDeci = Math.floor(decimal * 100);
    console.log("CurrDeci::" + curDeci);
    var gcd = findGcd(curDeci, 100);
    var numer = curDeci / gcd;
    var denumer = 100 / gcd;
    var num1 = Math.floor(numer / denumer);
    var num2 = numer % denumer;
    if (num2 === 0) {
        return (num1 + "/" + denumer);
    } else {
        return (num1 + " " + num2 + "/" + denumer);
    }

}
function findGcd(a, b) {
    console.log("FindGcd::" + a + " " + b);
    if (a === 0)
        return b;
    while (b !== 0) {
        if (a > b)
            a = a - b;
        else
            b = b - a;
    }
    console.log("FindGcdvalue::" + a);
    return a;
}
