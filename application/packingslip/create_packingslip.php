<?php
error_reporting(0);

$packslipid = filter_input(INPUT_GET, "packslipId");
$profilefor = filter_input(INPUT_GET, "profilefor");
$profilename = urldecode(filter_input(INPUT_GET, "profilename"));
$profiledetails = MysqlConnection::getlabelsandpriceByProfId($profilefor);
$newdummy = MysqlConnection::getProfilePrice($profilefor);


$profileresult = MysqlConnection::getPortfolioProfileById($profilefor);
$customerauto = MysqlConnection::fetchCustom("SELECT id,cust_companyname FROM `customer_master` WHERE status = 'Y' ORDER BY `cust_companyname` ASC");
$packslipauto = packslipauto(MysqlConnection::fetchCustom("SELECT DISTINCT(`type`) as type  FROM `packslipdetail`"));
$mysqlresultset = MysqlConnection::fetchCustom("SELECT * FROM `tbl_portfolioprofile`");
$arrsalutations = MysqlConnection::fetchCustom("SELECT distinct(`salutation`) as salutation FROM `customer_master` WHERE salutation!=''");
$color = MysqlConnection::fetchCustom("SELECT * FROM `profile_price` WHERE portfolio_id = '$profilefor' ");

$customer = MysqlConnection::getCustomerDetails(filter_input(INPUT_GET, "customerId"));
$adprofilevalues = MysqlConnection::fetchCustom("SELECT * FROM `ad_door_types` WHERE `TypeID` LIKE '%$profilename%'");
//$prodcap = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'productioncapacity'");


$customselect = "SELECT * FROM `generic_entry` WHERE `name` =(SELECT portfolio_name FROM tbl_portfolioprofile WHERE id = '$profilefor' )";
$prodcap1 = MysqlConnection::fetchCustom($customselect);
$capacity = $prodcap1[0];


$action = filter_input(INPUT_GET, "action");
if (!empty($packslipid)) {
    $packslip = MysqlConnection::getPackSlipFromId($packslipid);
    $packslipdetails = MysqlConnection::getPackSlipDetailsFromId($packslipid);
    $profiledetails = MysqlConnection::getPortfolioProfileByProfId($packslip["prof_id"]);
    $portfolioscan = MysqlConnection::getPortfolioProfileById($packslip["prof_id"]);
    $profilefor = $portfolioscan["id"];
    $profilename = $portfolioscan["profile_name"];
} else {
    $packslip["so_no"] = MysqlConnection::generateNumber("prodso");
}
saveOrEditPackingSlip($profilefor);
$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit)) {
    saveCustomer(filter_input_array(INPUT_POST));
}
?>
<?php

function saveCustomer($filterarray) {
    unset($filterarray["btnSubmit"]);
    if ($filterarray["salutation"] != null && salutation !== '') {
        unset($filterarray["salutation1"]);
    } else {
        $filterarray["salutation"] = $filterarray["salutation1"];
        unset($filterarray["salutation1"]);
    }
    MysqlConnection::insert("customer_master", $filterarray);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="packingslip/packingslipjs.js"></script>
<script src="js/script.js"></script>

<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
        $('#datepicker').change(function () {
            //$("div#divLoading").addClass('show');
            //var dataString = "datepicker=" + $("#datepicker").val();
            //$.ajax({
            //type: 'POST',
            //url: 'packingslip/packslipajaxcount.php',
            //data: dataString
            //}).done(function(data) {
            //var obj = JSON.parse(data);
            //console.log(obj);
            //if (obj.limit === 'exceeded') {
            //$('#productionmodel').modal('show');
            //}
            //}
            //).fail(function() {
            //});
            //$('#productionmodel').modal('show');
        });
    });
    $(function () {
        var packslipauto = [<?php echo $packslipauto ?>];
        for (var index = 1; index <= 100; index++) {
            $("#type" + index).autocomplete({source: packslipauto, minLength: 0}).focus(function () {
                $(this).autocomplete("search");
            });
        }
    });
    $(document).ready(function ($) {
        $("#phno").mask("+999-999-999-9999");
        $("#cust_fax").mask("+999-999-999-9999");
    });</script>

<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <?php
        if ($action == "quotation") {
            echo "<BR/><div class='alert alert-error'><strong>Do you want to edit this quotation ??</strong></div>";
        } elseif ($action == "workorder") {
            echo "<br/>"
            . "<div class='alert alert-error' style='line-height:22px;'>"
            . "<strong>You have requested to edit work order. Do you want to procced for edit work order? </strong>"
            . "<br/>First you need to edit packing slip.
               <br/>You must acknowledge customer by sending pack slip
               <br/>You must generate new quotation and confirm from customer
               <br/>You must confirm pack slip for work order generation" . "</div>";
        }
        ?>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white"> 
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#tab1">CREATE PACKING SLIP FOR <?php echo strtoupper($profilename) ?></a>
                    </li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%" border="0" >
                <tr>
                    <td>
                        <table class="display nowrap sortable" style="width: 100%" >
                            <tr style="vertical-align: top">
                                <td style="width: 10%"><label>CUSTOMER&nbsp;DETAILS<?php echo MysqlConnection::$REQUIRED ?></label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>
                                    <select autofocus="" tabindex="1" style="width: 225px;height: 25px" name="companyname" id="companyname" required="true">
                                        <option value="">SELECT</option>
                                        <option value="1"><< ADD NEW >></option>
                                        <?php
                                        foreach ($customerauto as $key => $value) {
                                            if (isset($customer)) {
                                                $selected = $value["id"] == $customer["id"] ? "selected" : "";
                                            } else {
                                                $selected = $packslip["cust_id"] == $value["id"] ? "selected" : "";
                                            }
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $value["id"] ?>">
                                                <?php echo strtoupper($value["cust_companyname"]) ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td style="font-weight: bold; " ><label >PO&nbsp;NO<?php echo MysqlConnection::$REQUIRED ?></label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" name="po_no" value="<?php echo $packslip["po_no"] ?>" id="po_no" maxlength="150" required="" style="width: 250px;" ></td>

                                <td ><label>REQUIRED&nbsp;DATE<?php echo MysqlConnection::$REQUIRED ?></label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" style="width: 83%"  type="text"  name="req_date" id="datepicker" value="<?php echo MysqlConnection::convertToPreferenceDate($packslip["req_date"]) ?>" required=""  style="width: 250px;" ></td>


                            </tr>
                            <tr >
                                <?php if ($profileresult["isSquare"] == "Y") { ?>
                                    <td ><label >SQUARE<?php echo MysqlConnection::$REQUIRED ?></label></td>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td>
                                        <select style="width: 225px" name="square" id="square"  value="<?php echo $packslip["square"] ?>" style="height: 25px;width: 250px" required="">
                                            <option value=""></option>
                                            <option value="yes" <?php echo strtolower($packslip["square"]) == "yes" ? "selected" : "" ?>>Yes</option>
                                            <option value="no" <?php echo strtolower($packslip["square"]) == "no" ? "selected" : "" ?>>No</option>
                                        </select>
                                    </td>
                                    <?php
                                } else {
                                    echo "<td></td><td></td><td  style='width: 225px'></td>";
                                }
                                ?>
                                <td>CUSTOMER DETAILS: </td>
                                <td>&nbsp;:&nbsp;</td>
                                <td style="font-size: 14px;text-transform: capitalize">
                                    <p id="customerDetails1"></p>
                                    <p id="customerDetails2"></p>
                                    <p id="customerDetails3"></p>
                                    <p id="customerDetails4"></p>
                                </td>
                                <td ><label >PACK&nbsp;DATE</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" name="rec_date" required="" readonly="" minlength="3" maxlength="15" value="<?php echo filter_input(INPUT_COOKIE, "dateme") ?>" style="width: 250px;" ></td>
                            </tr>

                            <tr style="vertical-align: top">

                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 25%;float: left">
                            <table class="display nowrap sortable" id="profiletable" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <tr style="font-weight: bold;">
                                    <td>Portfolio</td>
                                    <td style="text-transform: uppercase;font-size: 14px;">
                                        <?php echo $capacity["name"] ?>
                                        <input type="hidden" name="baseprice" id="baseprice" value="<?php echo $profileresult["baseprice"] ?>">
                                    </td>
                                </tr>
                                <tr style="font-weight: bold;">
                                    <td>Profile</td>
                                    <td  style="text-transform: uppercase;font-size: 14px;">
                                        <?php echo $profilename ?>
                                    </td>
                                </tr>
                                <?php
                                $profileindex = 1;
                                foreach ($profiledetails as $value) {
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $value["profilelabel"] ?>
                                            <input type="hidden" name="profilelabel[]" value="<?php echo $value["profilelabel"] ?>"  id="profilelabel[]">
                                        </td>
                                        <td><?php getProfileLabelPrice($profilename, $value["profilelabel"], $profileindex, $packslip) ?></td>
                                    </tr>
                                    <?php
                                    $profileindex++;
                                }
                                ?>
                                <?php
                                if (count($packlebels) != 0) {
                                    ?>
                                    <input type="hidden" id="profilelabelcount" value="<?php echo count($packlebels) ?>"/>        
                                    <input type="hidden" id="baseprice" value="<?php echo $baseprice ?>"/>        
                                    <?php
                                }
                                ?>
                            </table>
                            <div id="ajaxdata"></div>
                        </div>
                        <div style="width: 73%;float: right">
                            <textarea name="packingnote" placeholder="PLEASE ENTER PACKING NOTE HERE" style="padding: 5px; line-height: 22px;width: 99%;resize: none"><?php echo $packslip["packingnote"] ?></textarea>
                            <br/>
                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white">
                                    <td style="width: 25px;"><b>#</b></td>
                                    <td style="width: 100px;"><b>Pcs</b></td>
                                    <td style="width: 250px;"><b>Type</b></td>
                                    <td style="width: 80px;"><b>(W) MM</b></td>
                                    <td style="width: 80px;"><b>(H) MM</b></td>
                                    <td style="width: 80px;"><b>(W) INCH</b></td>
                                    <td style="width: 80px;"><b>(H) INCH</b></td>
                                    <td><b>SQ-FT</b></td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="display nowrap sortable" id="itemTable" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $index1 = 1;
                                    foreach ($packslipdetails as $value) {
                                        ?>
                                        <tr id="<?php echo $index1 ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="deleteRow('<?php echo $index1 ?>')"></a>
                                            </td>
                                            <td style="width: 100px"><input type="text" onkeypress="return chkNumericKey(event)" value="<?php echo $value["pcs"] ?>" name="pcs[]" id="pc<?php echo $index1 ?>" maxlength="4"     style="padding: 0px;margin: 0px;width: 100% " ></td>
                                            <td style="width: 250px;"><input type="text"   name="type[]" value="<?php echo $value["type"] ?>"  maxlength="30" id="type<?php echo $index1 ?>"      style="padding: 0px;margin: 0px;width: 100%;text-transform: uppercase" ></td>
                                            <td style="width: 80px;"><input type="text"  name="mm_w[]" value="<?php echo $value["mm_w"] ?>"    id="txtMMW<?php echo $index1 ?>"     style="padding: 0px;margin: 0px;width: 100%" ></td>
                                            <td style="width: 80px;"><input type="text"  name="mm_h[]" value="<?php echo $value["mm_h"] ?>"   id="txtMMH<?php echo $index1 ?>"   style="padding: 0px;margin: 0px;width: 100%" ></td>
                                            <td style="width: 80px;"><input type="text"   name="fract_w[]"  value="<?php echo $value["fract_w"] ?>"  id="mmInchW<?php echo $index1 ?>" onfocusout="calculate('<?php echo $index1 ?>','mmInchW<?php echo $index1 ?>','txtMMW<?php echo $index1 ?>')"     style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td style="width: 80px;"><input type="text"   name="fract_h[]"  value="<?php echo $value["fract_h"] ?>"  id="mmInchH<?php echo $index1 ?>"  onfocusout="calculate('<?php echo $index1 ?>','mmInchH<?php echo $index1 ?>','txtMMH<?php echo $index1 ?>')"    style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><input type="text" readonly=""  id="sqFt<?php echo $index1 ?>"  name="sqFeet[]" value="<?php echo $value["sqFeet"] ?>" onfocusout="shiftFocus()"   style="padding: 0px;margin: 0px;width: 100%" ></div></td>
                                        </tr>
                                        <?php
                                        $index1++;
                                    }
                                    ?>
                                    <?php
                                    for ($index = $index1; $index <= 20; $index++) {
                                        if (count($packslipdetails) == 0) {
                                            if ($index == 1) {
                                                $required = "required";
                                            } else {
                                                $required = "";
                                            }
                                        }
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="deleteRow('<?php echo $index ?>')"></a>
                                            </td>
                                            <td style="width: 100px"><input type="text" <?php echo $required ?> onkeypress="return chkNumericKey(event)" name="pcs[]" id="pc<?php echo $index ?>" maxlength="4"     style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td style="width: 250px;"><input type="text" <?php echo $required ?> name="type[]" maxlength="30" id="type<?php echo $index ?>"      style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td style="width: 80px;"><input type="text" <?php echo $required ?>  name="mm_w[]"   id="txtMMW<?php echo $index ?>"     style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td style="width: 80px;"><input type="text" <?php echo $required ?>  name="mm_h[]"  id="txtMMH<?php echo $index ?>"  style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td style="width: 80px;"><input type="text" <?php echo $required ?>  name="fract_w[]"  id="mmInchW<?php echo $index ?>"  onfocusout="calculate('<?php echo $index ?>','mmInchW<?php echo $index ?>','txtMMW<?php echo $index ?>')"     style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td style="width: 80px;"><input type="text" <?php echo $required ?> name="fract_h[]"  id="mmInchH<?php echo $index ?>"  onfocusout="calculate('<?php echo $index ?>','mmInchH<?php echo $index ?>','txtMMH<?php echo $index ?>')"    style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><input type="text" readonly=""  tabIndex="-1" id="sqFt<?php echo $index ?>" <?php echo $required ?> name="sqFeet[]"  style="padding: 0px;margin: 0px;width: 100%"></div></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                            <br/>
                            <table class="table table-bordered" style="width: 30%;float: right">

                                <tr>
                                    <td>TOTAL PC</td>
                                    <td><input type="text" style="width: 97%" name="total_pieces" id="total_pieces" readonly="" value="<?php echo $packslip["total_pieces"] ?>"></td>
                                </tr>

                                <tr>
                                    <td>TOTAL SQFT</td>
                                    <td><input type="text" style="width: 97%"  name="billable_fitsquare" id="billable_fitsquare"  value="<?php echo $packslip["billable_fitsquare"] ?>"  readonly=""></td>
                                </tr>
                            </table>

                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>
                    <input type="submit" id="submit" name="submit"  class="btn btn-info" value="SAVE">
                    <input type="submit" id="btnsavenext" name="submitNext"  class="btn btn-info" value="SAVE AND NEW">
                    <input type="submit" id="printButton" name="printButton" class='btn btn-info' value="PRINT" />
                    <input type="submit" id="mailButton"  name="mailButton"  class='btn btn-info' value="EMAIL" style="margin-left:4px;"/>
                    <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">&nbsp;CANCEL&nbsp;</a>
                </center>
            </div>
        </div>
    </div>
</form>

<div id="custtypemodel" class="modal hide" style="top: 3%;left: 25%;width: 90%; overflow: hidden">

    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD NEW CUSTOMER</h3>
    </div>
    <?php include 'customdialogs/customerdialog.php'; ?>
</div>

<!-- this is custom model dialog --->
<div id="productionmodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3 style="color: red">Production Capacity Alert!!!</h3>
    </div>
    <div class="modal-body">
        <p style="font-size: 12px">Total scheduled production capacity is <b><?php echo $prodcap1[0]["productioncapacity"]; ?> </b>!!!</p>
    </div>
    <div class="modal-footer"> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->

<script>
    $(document).ready(function () {
        $('#companyname').select2();
    });
//    $("datepicker").blur(function() {
//        alert("This input field has lost its focus.");
//    });


    $("#cancelct").click(function () {
        $("#companyname").val("");
    });
    $("#companyname").click(function () {
        var valueModel = $("#companyname").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
    $("#cancelct").click(function () {
        $("#companyname").val("");
    });
    $("#companyname").click(function () {
        var valueModel = $("#companyname").val();
        if (valueModel === "1") {
            $('#creditlimitmodel').modal('show');
        }
    });
    $("#companyname").change(function () {
        $("div#divLoading").addClass('show');
        var dataString = "companyname=" + $("#companyname").val();
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function (data) {
            var obj = JSON.parse(data);
            $('#customerDetails1').text(obj.cust_companyname);
            $('#customerDetails2').text(obj.streetNo + " " + obj.city);
            $('#customerDetails3').text(obj.cust_province + " " + obj.country);
            $('#customerDetails4').text(obj.postal_code);
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    });
    $("#companyname").click(function () {
        var valueModel = $("#companyname").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
    function getProfile(name) {
        var dataString = "id=" + name;
        $("div#divLoading").addClass('show');
        $.ajax({
            type: 'POST',
            url: 'profilemaster/searchprofile_ajax.php',
            data: dataString
        }).done(function (data) {
            showTable(data);
            $("div#divLoading").removeClass("show");
        }).fail(function () {
        });
    }

    $('#addprofile').on('change', function () {
        var profileid = $("#addprofile option:selected").val();
        removeTable();
        getProfile(profileid);
    });
    function removeTable() {
    }


    function validateproduction() {
        $('#productionmodel').modal('show');
    }


    function showTable(data) {
        jQuery('#ajaxdata').html(data);
    }
    $("#cancelti").click(function () {
        $("#companyname").val("");
    });
    $("#cancelct").click(function () {
        $("#suppid").val("");
    });

</script>
<?php

function packslipauto($packslipauto) {
    $option = "";
    foreach ($packslipauto as $key => $value) {
        $option .= " \" " . preg_replace('!\s+!', ' ', str_replace("\"", "", $value["type"])) . "\",";
    }
    return $option;
}

function saveOrEditPackingSlip($profilefor = "") {
    $packslipid = filter_input(INPUT_GET, "packslipId");
    $arraypackslip["cust_id"] = $custid = filter_input(INPUT_POST, "companyname");
    if ($packslipid != "") {
        $arraypackslip["prof_id"] = $profilefor;
    } else {
        $arraypackslip["prof_id"] = filter_input(INPUT_GET, "profilefor");
    }

    $arraypackslip["po_no"] = filter_input(INPUT_POST, "po_no");

    $arraypackslip["so_no"] = $sonumber = MysqlConnection::generateNumber("prodso");
    MysqlConnection::delete("UPDATE `order_number` SET prodso = $sonumber WHERE identity = 'Y' ");

//   $arraypackslip["so_no"] = filter_input(INPUT_POST, "so_no");
    $arraypackslip["seq_no"] = filter_input(INPUT_POST, "seq_no");
    $arraypackslip["square"] = filter_input(INPUT_POST, "square");
    $reqdate = filter_input(INPUT_POST, "req_date");
    $arraypackslip["req_date"] = MysqlConnection::convertToDBDate($reqdate);
    $recdate = filter_input(INPUT_POST, "rec_date");
    $arraypackslip["rec_date"] = MysqlConnection::convertToDBDate($recdate);
    $arraypackslip["packingnote"] = filter_input(INPUT_POST, "packingnote");

    //calculations
    $arraypackslip["total_pieces"] = filter_input(INPUT_POST, "total_pieces");
    $arraypackslip["billable_fitsquare"] = filter_input(INPUT_POST, "billable_fitsquare");
    $arraypackslip["profilecost"] = filter_input(INPUT_POST, "profilecost");

    $arraypackslip["cut_tape_charge"] = filter_input(INPUT_POST, "cut_tape_charge");
    $arraypackslip["cut_tape_cost"] = filter_input(INPUT_POST, "cut_tape_cost");
    $arraypackslip["total_cost"] = filter_input(INPUT_POST, "total_cost");
    $arraypackslip["taxname"] = filter_input(INPUT_POST, "taxname");
    $arraypackslip["taxvalue"] = filter_input(INPUT_POST, "taxvalue");
    $arraypackslip["nettotal"] = filter_input(INPUT_POST, "nettotal");

    $arraypackslip["packlebels"] = implode(",", $_POST["profilelabel"]);
    $arraypackslip["packvalues"] = implode(",", $_POST["labelvalueprice"]);

    $arraypackslip["workOrd_Id"] = "";
    $arraypackslip["quot_id"] = "";

    $arrpcs = $_POST["pcs"];
    $arrtype = $_POST["type"];
    $arrmmw = $_POST["mm_w"];
    $arrmmh = $_POST["mm_h"];
    $arrfractw = $_POST["fract_w"];
    $arrfracth = $_POST["fract_h"];
    $arrsqFeet = $_POST["sqFeet"];

    $btnsubmit = filter_input(INPUT_POST, "submit");
    $btnsubmitnext = filter_input(INPUT_POST, "submitNext");
    $btnsubmitprint = filter_input(INPUT_POST, "printButton");
    $btnsubmitemail = filter_input(INPUT_POST, "mailButton");

    if (isset($btnsubmit) || isset($btnsubmitnext) || isset($btnsubmitprint) || isset($btnsubmitemail)) {

        $packslipid = MysqlConnection::insert("packslip", $arraypackslip);

        //this is tracking array
        $arraytrack = array();
        $arraytrack["cust_id"] = $custid;
        $arraytrack["packslipId"] = $packslipid;
        $arraytrack["workOrId"] = "-";
        $arraytrack["workInCode"] = "-";
        $arraytrack["workOutCode"] = "-";
        $arraytrack["scannerId"] = "-";
        $arraytrack["scannerCode"] = "-";
        $arraytrack["phase"] = "PACK SLIP CREATED";
        $arraytrack["phase_description"] = "PACK SLIP CREATED";
        $arraytrack["phase_date"] = date("Y-m-d");
        $arraytrack["phase_time"] = date("g:i:s");
        $arraytrack["finished"] = "Y";
        MysqlConnection::insert("workorder_phase_history", $arraytrack);
        //this is tracking array

        for ($item = 0; $item <= count($arrpcs); $item++) {
            if ($arrpcs[$item] != "" && $arrtype[$item] != "" && $arrsqFeet[$item] != "") {
                $psitem["pcs"] = $arrpcs[$item];
                $psitem["type"] = $arrtype[$item];
                $psitem["mm_w"] = $arrmmw[$item];
                $psitem["mm_h"] = $arrmmh[$item];
                $psitem["fract_w"] = $arrfractw[$item];
                $psitem["fract_h"] = $arrfracth[$item];
                $psitem["sqFeet"] = $arrsqFeet[$item];
                $psitem["rtor_value"] = strtolower($arraypackslip["square"]) == "yes" ? $psitem["mm_w"] - 36 : $psitem["mm_w"];
                $psitem["height"] = strtolower($arraypackslip["square"]) == "yes" ? $psitem["mm_h"] - 1 : $psitem["mm_h"];
                $psitem["ps_id"] = $packslipid;
                MysqlConnection::insert("packslipdetail", $psitem);
            }
        }
        doNavigation($packslipid, $arraypackslip["so_no"]);
        doPrint($packslipid, $arraypackslip["so_no"]);
        doPsPrint($packslipid, $arraypackslip["so_no"]);
    }
}

function doNavigation($packslipid, $sono) {
    $btnsubmit = filter_input(INPUT_POST, "submit");
    $btnsubmitnext = filter_input(INPUT_POST, "submitNext");
    $btnsubmitprint = filter_input(INPUT_POST, "printButton");
    $btnsubmitemail = filter_input(INPUT_POST, "mailButton");

    if (isset($btnsubmit)) {
        header("location:index.php?pagename=manage_packingslip&status=active");
    } else if (isset($btnsubmitnext)) {
        header("location:index.php?pagename=create_packingslip");
    } else if (isset($btnsubmitprint)) {
        $_SESSION["packslipid"] = $packslipid;
        header("location:index.php?pagename=manage_packingslip&status=active");
    } else if ($btnsubmitemail) {
        header("location:index.php?pagename=email_packingslip&packslipId=$packslipid");
    }
}

function doPrint($packslipid, $sono) {
    include 'pdflib/packingslipakg.php';
}

function doPsPrint($packslipid, $sono) {
    include 'pdflib/packingslip.php';
}

function getProfileLabelPrice($profilelabel, $profilelabelname, $profileindex, $packslip) {

    $packlebelsexp = explode(",", $packslip["packlebels"]);
    $packvaluesexp = explode(",", $packslip["packvalues"]);

    $array = array();
    for ($labcount = 0; $labcount < count($packlebelsexp); $labcount++) {
        $array[$packlebelsexp[$labcount]] = $packvaluesexp[$labcount];
    }
    $resultset = MysqlConnection::getProfileLabelValuePrice($profilelabel, $profilelabelname);

    $select = "<select name='labelvalueprice[]' id='labelvalueprice[]'>"
            . "<option value=''>Please select</option>";

    $selected = $array[$profilelabelname];

    $temparray = array();
    foreach ($resultset as $values) {

        $baseprice = $values["profile_label_name"] . " -- " . $values["profile_label_price"];
        $display = $values["profile_label_name"];

        if (!in_array($display, $temparray)) {
            if ($selected == $baseprice) {
                $choose = "selected";
            } else {
                $choose = "";
            }

            $select = $select . "<option $choose  value='$baseprice'>$display</option>";
        }
        array_push($temparray, $display);
    }
    $select = $select . "</select>";
    echo $select;
}
?>