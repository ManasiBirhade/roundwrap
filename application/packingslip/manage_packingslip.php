<?php
error_reporting(0);
$basequery = "SELECT * FROM `packslip`  ";
$status = filter_input(INPUT_GET, "status");
$searchPSID = filter_input(INPUT_GET, "psid");



if ($status == "active") {
    $basequery = $basequery . " ";
} else if ($status == "inactive") {
    $basequery = $basequery . " ";
} else if ($status == "search") {
    $basequery = $basequery . " WHERE ps_id = '" . $searchPSID . "'";
} else {
    
}
$packslip = MysqlConnection::fetchCustom($basequery . "   ORDER BY indexid DESC");

$packslipid = $_SESSION["packslipid"];
if ($packslipid != "") {
    echo "<script language='javascript'>window.open('invoice/print_packslip.php?packslipid=$packslipid');</script>";
}
$_SESSION["packslipid"] = "";

$action = filter_input(INPUT_GET, "action");
$flag = filter_input(INPUT_GET, "flag");
$packslipId = filter_input(INPUT_GET, "packslipId");
$purchaseid = filter_input(INPUT_GET, "purchaseorderid");


if ($flag == "ackrecv") {

    $arraytrack = array();
    $arraytrack["psid"] = $packslipId;
    $arraytrack["mode"] = "PACKING SLIP ACKNOWLEDGMENT RECEIVED";
    MysqlConnection::insert("tbl_track_ps", $arraytrack);


    //this is tracking array
    $arraytrack = array();
    $arraytrack["cust_id"] = '';
    $arraytrack["packslipId"] = $packslipId;
    $arraytrack["workOrId"] = "-";
    $arraytrack["workInCode"] = "-";
    $arraytrack["workOutCode"] = "-";
    $arraytrack["scannerId"] = "-";
    $arraytrack["scannerCode"] = "-";
    $arraytrack["phase"] = "ACKNOWLEDGEMENT";
    $arraytrack["phase_description"] = "PACKING SLIP ACKNOWLEDGMENT RECEIVED";
    $arraytrack["phase_date"] = date("Y-m-d");
    $arraytrack["phase_time"] = date("g:i:s");
    $arraytrack["finished"] = "Y";
    MysqlConnection::insert("workorder_phase_history", $arraytrack);
    //this is tracking array


    MysqlConnection::delete("UPDATE `packslip` SET `ackrecv` = 'YES' WHERE `ps_id` = '$packslipId'");
    header("location:index.php?pagename=success_packingslip&packslipid=$packslipId&flag=ackrecv");
}
if ($flag == "activeinactive") {
    $resultset = MysqlConnection::fetchCustom("SELECT status FROM  `packslip` WHERE `ps_id` = '$packslipId'");
    $packslip = $resultset[0]["status"];
    $status = "";
    if ($packslip == "Y") {
        $status = "inactive";
        MysqlConnection::delete("UPDATE `packslip` SET `status` = 'N' WHERE  `ps_id` = '$packslipId';");
    } else {
        $status = "active";
        MysqlConnection::delete("UPDATE `packslip` SET `status` = 'Y' WHERE  `ps_id` = '$packslipId';");
    }
    header("location:index.php?pagename=success_packingslip&packslipid=$packslipId&flag=activeorinactive&status=$status");
}
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 320,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <br/>

    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PACKING SLIP LIST</h5>
    </div>
    <br/>
    <table style="width: 100%" border="0">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=select_packingslip" ><i class="icon icon-user"></i>&nbsp;ADD PACKING SLIP</a>
                <a href="index.php?pagename=manage_packingslip&status=active" id="btnSubmitFullForm" class="btn btn-info">VIEW ACTIVE</a>
                <a href="index.php?pagename=manage_packingslip&status=inactive" id="btnSubmitFullForm" class="btn btn-info">VIEW INACTIVE</a>
                <a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">VIEW ALL</a>
            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead >
            <tr>
                <td>#</td>
                <td>Company Name</td>
                <td>Profile Name</td>
                <td>SO No</td>
                <td>PO No</td>
                <td>Rec Date</td>
                <td>Req Date</td>
                <td>Email</td>
                <td>Mat.Rec.</td>
                <td>Acknowledged</td>
                <td>Quotation ?</td>
                <td>IS WorkOrder ?</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($packslip as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $customername = MysqlConnection::fetchCustom("SELECT cust_companyname FROM customer_master WHERE id = '" . $value["cust_id"] . "'");
                $portfolioname = MysqlConnection::getPortfolioProfileById($value["prof_id"]);
                if ($action == "search") {
                    
                }
                ?>
                <tr id="<?php echo $value["ps_id"] ?>" style="<?php echo $bgcolor ?> ;"  class="context-menu-one">
                    <td ><?php echo $index ?></td>
                    <td >
                        <a href="index.php?pagename=view_customermaster&customerId=<?php echo $value["cust_id"] ?>">
                            <?php echo ucwords($customername[0]["cust_companyname"]) ?>
                        </a>
                    </td>
                    <td ><?php echo $portfolioname["portfolio_name"] . " - " . $portfolioname["profile_name"] ?></td>
                    <td ><?php echo $value["so_no"] ?></td>
                    <td ><?php echo $value["po_no"] ?></td>
                    <td ><?php echo MysqlConnection::convertToPreferenceDate($value["rec_date"]) ?></td>
                    <td ><?php echo MysqlConnection::convertToPreferenceDate($value["req_date"]) ?></td>
                    <td title="<?php echo $value["emaildate"] == "0000-00-00" ? " " : $value["emaildate"] ?>">
                        <?php echo $value["isMail"] == "Y" ? "<span class='badge badge-success'>YES</span>" : "<span class='badge badge-info'>NO</span>" ?>
                        <span style="font-size: 11px;"></span>
                    </td>
                    <td  style="margin-left: 10px" title="<?php echo $value["mat_rec_date"] == "0000-00-00" ? " " : $value["mat_rec_date"] ?>">
                        <?php
                        if ($portfolioname["portfolio_name"] != "PVC") {
                            ?>
                            <?php echo $value["mat_rec_confirm"] == "Y" ? "<span class='badge badge-success'>YES</span>" : "<span class='badge badge-info'>NO</span>" ?>
                            <span style="font-size: 11px;"></span>
                            <?php
                        } else {
                            echo "   -";
                        }
                        ?>
                    </td>

                    <td ><?php echo $value["ackrecv"] == "NO" ? "<span class='badge badge-info'>NO</span>" : "<span class='badge badge-success'>YES</span>" ?></td>
                    <td ><?php echo $value["quot_id"] == "" ? "<span class='badge badge-info'>NO</span>" : "<span class='badge badge-success'>YES</span>" ?></td>
                    <td ><?php echo $value["workOrd_Id"] == "" ? "<span class='badge badge-info'>NO</span>" : "<span class='badge badge-success'>YES</span>" ?></td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" style="margin-left:10px;">EMAIL</button>
        </div> 
    </div>

</div>

<script>
    $('#example tbody tr').click(function(e) {
        var id = $(this).attr('id');
        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var del = '<a href="invoice/print_packslip.php?packslipid=' + id + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var em = '<a href="index.php?pagename=email_packingslip&packslipId=' + id + '&flag=email" ><button type="button" class=" btn btn-info" style="margin-left:10px;">EMAIL</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + del + em + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });



    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }

</script>
<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_packingslip":
                        window.location = "index.php?pagename=view_packingslip&packslipId=" + id;
                        break;
                    case "edit_packingslip":
                        window.location = "index.php?pagename=edit_packingslip&packslipId=" + id;
                        break;
                    case "delete_packslip":
                        window.location = "index.php?pagename=view_packingslip&packslipId=" + id + "&flag=yes";
                        break;
                    case "print_packingslip":
                        window.open("invoice/print_packslip.php?packslipid=" + id, '_blank');
                        break;
                    case "email_packingslip":
                        window.location = "index.php?pagename=create_packingslip&packslipId=" + id + "&flag=email";
                        break;
                    case "send_acknowledgement":
                        window.open("invoice/print_packingslipakg.php?packslipid=" + id, '_blank');
                        break;
                    case "confirm_acknowledgement":
                        window.location = "index.php?pagename=manage_packingslip&packslipId=" + id + "&flag=ackrecv";
                        break;
                    case "active_packslip":
                        window.location = "index.php?pagename=manage_packingslip&packslipId=" + id + "&flag=activeinactive";
                        break;
                    case "create_workorder":
                        window.location = "index.php?pagename=fromps_workorder&packslipId=" + id;
                        break;
                    case "create_quotation":
                        window.location = "index.php?pagename=create_quotation&psid=" + id;
                        break;
                    case "generate_layout":
                        window.open("layout/generate_layout.php?packslipid=" + id, '_blank');
                        break;
                    case "mat_received":
                        window.location = "index.php?pagename=receive_packingslip&packslipId=" + id;
                        break;

                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_packingslip&status=active";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_packingslip": {name: "VIEW PACKING SLIP", icon: ""},
                "edit_packingslip": {name: "EDIT PACKING SLIP", icon: ""},
                "delete_packslip": {name: "DELETE PACKING SLIP", icon: ""},
                "sep0": "---------",
                "send_acknowledgement": {name: "PRINT ACKNOWLEDGMENT", icon: ""},
                "confirm_acknowledgement": {name: "CONFIRM ACKNOWLEDGMENT", icon: ""},
                "sep1": "---------",
                "create_quotation": {name: "CREATE QUOTATION", icon: ""},
                "create_workorder": {name: "CREATE WORK ORDER", icon: ""},
                "sep2": "---------",
                "active_packslip": {name: "ACTIVE/INACTIVE ", icon: ""},
                "generate_layout": {name: "GENERATE LAYOUT ", icon: ""},
                "sep3": "---------",
                "mat_received": {name: "MATERIAL RECEIVED", icon: ""},
            }
        });

        //        $('.context-menu-one').on('click', function(e){
        //            console.log('clicked', this);
        //       })    
    });
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_packingslip&packslipId=" + id;
        }
    });
</script>
