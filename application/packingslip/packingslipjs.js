function calculate(count, index, target) {
    var pccount = document.getElementById("pc" + count).value;
    if (pccount !== "") {
        var finalVale = document.getElementById(index).value;
        if (finalVale.indexOf("-") !== -1 && finalVale.indexOf("/")) {
            doOperation(finalVale, target);
        } else if (finalVale.indexOf(" ") !== -1 && finalVale.indexOf("/")) {
            doOperation(finalVale, target);
        } else if (finalVale.indexOf("\\.") !== -1 && finalVale.indexOf("/")) {
            doOperation(finalVale, target);
        } else if (finalVale.indexOf("/") === -1 && finalVale !== "") {
            var result = parseFloat(finalVale) * 25.4;
            document.getElementById(target).value = (result).toFixed(3);
        } else {
//            alert("Please enter proper fraction value. (e.g 12 15/17) ");
            document.getElementById("pc" + count).focus();
        }
    } else {
//        alert("Please enter pc count");
//        document.getElementById("pc" + count).focus();
    }

    var txtMMW = document.getElementById("txtMMW" + count).value;
    var txtMMH = document.getElementById("txtMMH" + count).value;
    if (txtMMW !== "" && txtMMH !== "") {
        var mmW = parseFloat(txtMMW) / 25.4;
        var mmH = parseFloat(txtMMH) / 25.4;
        var sqFeetValue = (((mmW * mmH) / 144) * parseFloat(pccount));

        document.getElementById("mmInchW" + count).value = mmW.toFixed(3);
        document.getElementById("mmInchH" + count).value = mmH.toFixed(3);

        if (sqFeetValue <= 1.5) {
            document.getElementById("sqFt" + count).value = "1.5";
        } else {
            document.getElementById("sqFt" + count).value = sqFeetValue.toFixed(3);
        }
    }
    document.getElementById("pc" + (parseInt(count) + parseInt(1))).focus();
    shiftFocus();
}

function calculateUsingPC(count, index, target) {
    var pccount = document.getElementById("pc" + count).value;
    if (pccount !== "") {
        var finalVale = document.getElementById(index).value;
        if (finalVale !== "") {
            if (finalVale.indexOf("-") !== -1 && finalVale.indexOf("/")) {
                doOperation(finalVale, target);
            } else if (finalVale.indexOf(" ") !== -1 && finalVale.indexOf("/")) {
                doOperation(finalVale, target);
            } else if (finalVale.indexOf("\\.") !== -1 && finalVale.indexOf("/")) {
                doOperation(finalVale, target);
            } else if (finalVale.indexOf("/") === -1 && finalVale !== "") {
                var result = parseFloat(finalVale) * 25.4;
                document.getElementById(target).value = (result).toFixed(3);
            } else {
                document.getElementById("pc" + count).focus();
            }
        }
    } else {
//        document.getElementById("pc" + count).focus();
    }

    var txtMMW = document.getElementById("txtMMW" + count).value;
    var txtMMH = document.getElementById("txtMMH" + count).value;
    if (txtMMW !== "" && txtMMH !== "") {
        var mmW = parseFloat(txtMMW) / 25.4;
        var mmH = parseFloat(txtMMH) / 25.4;
        var sqFeetValue = (((mmW * mmH) / 144) * parseFloat(pccount));
        if (sqFeetValue <= 1.5) {
            document.getElementById("sqFt" + count).value = "1.5";
        } else {
            document.getElementById("sqFt" + count).value = sqFeetValue.toFixed(3);
        }

    }
    calculateCutTapeCharges();
}

function shiftFocus() {
    var totalPc = 0;
    var totalsqft = 0.0;
    for (var index = 1; index <= 30; index++) {
        var docpc = document.getElementById("pc" + index + "");
        if (docpc !== undefined && docpc !== null && docpc.value !== null && docpc.value !== "" && docpc.value !== undefined) {
            totalPc = parseInt(totalPc) + parseInt(docpc.value);
        }

        var docsqFt = document.getElementById("sqFt" + index + "");
        if (docsqFt !== undefined && docsqFt !== null && docsqFt.value !== null && docsqFt.value !== "" && docsqFt.value !== undefined) {
            totalsqft = parseFloat(totalsqft) + parseFloat(docsqFt.value);
        }
    }
    document.getElementById("total_pieces").value = totalPc;
    document.getElementById("billable_fitsquare").value = totalsqft;
}

function doOperation(finalVale, target) {
    var finalSplit = "";
    if (finalVale.indexOf("-") !== -1) {
        finalSplit = finalVale.split("-");
    } else {
        finalSplit = finalVale.split(" ");
    }

    var val1 = parseFloat(finalSplit[0]);
    var val2 = finalSplit[1];

    var split2 = val2.split("/");
    var var3 = parseFloat(split2[0]);
    var var4 = parseFloat(split2[1]);

    val1 = val1 + (var3 / var4);
    var finalVal = (val1 * 25.4);
    document.getElementById(target).value = (finalVal).toFixed(3);
}


function calculateCutTapeCharges() {
    var cutTapeCharge = 0.0;
    if (document.getElementById("cutTapeCharge") !== undefined || document.getElementById("cutTapeCharge") !== "") {
        cutTapeCharge = document.getElementById("cutTapeCharge").value;
    }


    var pcsTotal = 0;
    for (i = 1; i <= 30; i++) {
        if (document.getElementById("pc" + i) !== null) {
            var pcvalue = document.getElementById("pc" + i).value;
            if (pcvalue !== "" && pcvalue !== undefined && pcvalue !== null) {
                pcsTotal = pcsTotal + parseInt(pcvalue);
            }
        }
    }
    document.getElementById("pcsTotal").value = pcsTotal;
    if (cutTapeCharge !== "" && pcsTotal !== "") {
        document.getElementById("cutTapeCost").value = parseFloat(cutTapeCharge) * parseFloat(pcsTotal);
    }
    calculateQuotationTotal();
}
function calculateQuotationTotal(index) {
    var dollerFeetSq = 0.0;
    for (i = 0; i < 30; i++) {
        if (document.getElementById("dollerPerFit" + (i + 1)) !== null) {
            var val1 = document.getElementById("dollerPerFit" + (i + 1)).value;
            var val2 = document.getElementById("sqFt" + (i + 1)).value;

            if (val1 !== "" && val2 !== "" && val1 !== undefined && val2 !== undefined && val1 !== null && val2 !== null) {
                dollerFeetSq = dollerFeetSq + (parseFloat(val1) * parseFloat(val2));
            }
        }
    }
    //document.getElementById("dollerFtSquareCost").value = dollerFeetSq.toFixed(2);
    var cutTapeCost = 0.0;
    if (document.getElementById("cutTapeCost") !== undefined) {
        cutTapeCost = document.getElementById("cutTapeCost").value;

    }
    if (cutTapeCost === "") {
        cutTapeCost = 0.0;
    }
    var dollerFtSquareCost = 0;
    document.getElementById("totalCost").value = (parseFloat(cutTapeCost) + parseFloat(dollerFtSquareCost)).toFixed(2);
}

function calculateProfileCost() {
    var profilelabelcount = document.getElementById("profilelabelcount").value;
    var baseprice = document.getElementById("baseprice").value;
    if (baseprice === "") {
        baseprice = 0.0;
    }
    baseprice = parseFloat(baseprice);
    for (var index = 1; index <= parseInt(profilelabelcount); index++) {
        var labvar = document.getElementById("profilelabelvalues" + index)
        if (labvar !== null && labvar !== undefined && labvar !== "") {
            var labelvalues = labvar.value;
            var splitlabelvalues = labelvalues.split("--");
            if (splitlabelvalues[1] !== "" && splitlabelvalues[1] !== undefined) {
                baseprice = baseprice + parseFloat(splitlabelvalues[1]);
            }
        }
    }
    document.getElementById("profilecost").value = baseprice;

    //square feet calculation
    var totalSqFt = 0.0;
    for (var sqftindex = 1; sqftindex <= 50; sqftindex++) {
        var sqFt = document.getElementById("sqFt" + sqftindex).value;
        if (sqFt !== "" && sqFt !== undefined) {
            totalSqFt = totalSqFt + parseFloat(sqFt);
        }
    }
    document.getElementById("billable_fitsquare").value = totalSqFt.toFixed(3);


    //pc calculation
    var totalPc = 0.0;
    for (var pcindex = 1; pcindex <= 50; pcindex++) {
        var pc = document.getElementById("pc" + pcindex).value;
        if (pc !== "" && pc !== undefined) {
            totalPc = totalPc + parseFloat(pc);
        }
    }
    document.getElementById("total_pieces").value = parseInt(totalPc);


    var total_pieces = document.getElementById("total_pieces").value;
    if (total_pieces === "") {
        total_pieces = 0.0;
    }
    var billable_fitsquare = document.getElementById("billable_fitsquare").value;
    if (billable_fitsquare === "") {
        billable_fitsquare = 0.0;
    }
    var profilecost = document.getElementById("profilecost").value;
    if (profilecost === "") {
        profilecost = 0.0;
    }
    var cut_tape_charge = document.getElementById("cut_tape_charge").value;
    if (cut_tape_charge === "") {
        cut_tape_charge = 0.0;
    }


    var cut_tape_cost = parseFloat(total_pieces) * parseFloat(cut_tape_charge);
    document.getElementById("cut_tape_cost").value = cut_tape_cost.toFixed(3);
    if (cut_tape_cost === 0 && cut_tape_cost === undefined) {
        cut_tape_cost = 0.0;
    }

    var total_cost = (parseFloat(profilecost) * parseFloat(billable_fitsquare)) + cut_tape_cost;
    document.getElementById("total_cost").value = total_cost.toFixed(3);


    var taxvalue = document.getElementById("taxvalue").value;
    if (taxvalue === "") {
        taxvalue = 0.0;
    }

    var nettotal = parseFloat(total_cost) + (parseFloat(total_cost) * (parseFloat(taxvalue) / 100));
    document.getElementById("nettotal").value = nettotal.toFixed(3);
}