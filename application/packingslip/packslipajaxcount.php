<?php

include '../MysqlConnection.php';


$datepicker = filter_input(INPUT_POST, "datepicker");
$databasedate = MysqlConnection::convertToDBDate($datepicker);

$query = "SELECT count(pd.pcs) as pccount FROM packslip p , packslipdetail pd WHERE p.ps_id = pd.ps_id AND p.req_date <= '$databasedate' AND workOrd_Id = ''";
$resultset = MysqlConnection::fetchCustom($query);

$counter = $resultset[0]["pccount"];

$productionlimit = MysqlConnection::fetchCustom("SELECT name FROM `generic_entry` WHERE `type` = 'productioncapacity' ");
$limit = $productionlimit[0]["name"];

if ($counter > $limit) {
    $array = array();
    $array["limit"] = "exceeded";
    $array["key"] = $counter;
    echo json_encode($array);
} else {
    $array = array();
    $array["limit"] = "not";
    $array["key"] = $counter;
    echo json_encode($array);
}

