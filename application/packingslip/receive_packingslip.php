<?php
$packslipid = filter_input(INPUT_GET, "packslipId");
$submit = filter_input(INPUT_POST, "btnSubmit");
$btnNoSubmit = filter_input(INPUT_POST, "btnNoSubmit");


if ($submit == "YES") {
    $mat_rec_confirm = filter_input(INPUT_POST, "mat_rec_confirm");
    $mat_rec_date = filter_input(INPUT_POST, "mat_rec_date");
    if ($mat_rec_confirm != "" && $mat_rec_date != "") {
        $sql = "UPDATE packslip SET mat_rec_confirm = '$mat_rec_confirm' , mat_rec_date =  '$mat_rec_date' WHERE ps_id = '$packslipid'   ";
        MysqlConnection::delete($sql);
    }
    header("location:index.php?pagename=manage_packingslip&status=active");
}

if ($btnNoSubmit == "NO") {
    $mat_rec_confirm = filter_input(INPUT_POST, "mat_rec_confirm");
    $mat_rec_date = filter_input(INPUT_POST, "mat_rec_date");
    if ($mat_rec_confirm != "" && $mat_rec_date != "") {
        $sql = "UPDATE packslip SET mat_rec_confirm = 'N' , mat_rec_date =  '$mat_rec_date' WHERE ps_id = '$packslipid'   ";
        MysqlConnection::delete($sql);
    }
    header("location:index.php?pagename=manage_packingslip&status=active");
}
?>

<form name="frmDate" id="frmDate" method="POST">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong></strong> 
                    CLICK ON 'YES' TO CONFIRM ALL ITEMS ARE RECEIVED...!
                    <b>On Date : <?php echo date("Y-m-d") ?></b>
                    <input type="hidden" name="mat_rec_confirm" id="mat_rec_confirm" value="Y">
                    <input type="hidden" name="mat_rec_date" id="mat_rec_date" value="<?php echo date("Y-m-d") ?>">
                </div>
            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td>
                                <input type="submit" name="btnSubmit" value="YES"  id="btnSubmitFullForm" class="btn btn-info">
                            </td>
                            <td>
                                <input type="submit" name="btnNoSubmit" value="NO"  id="btnNoSubmit" class="btn btn-info">
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>

</form>

