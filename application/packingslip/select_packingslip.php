<?php

$arraycategory = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'productioncategory' ORDER BY `indexid`");
?>

<style>

    .table table-bordered{
        border: solid 1px #F6F6F6;
    }

    .itemtable{
        border-right: solid 1px #F6F6F6;
        border-bottom: solid 1px #F6F6F6;
    }
    .itemtable thead{
        height: 40px;
        /*        background-image: linear-gradient(to bottom right, #5F677C, #9A6651);*/
        border: 0px;

    }
    .itemtable tbody {
        border: solid 1px #F6F6F6;

    }
</style>
<div class="container-fluid" >
    <div class="cutomheader"><h5 style="font-family: verdana;font-size: 12px;">Select Packing Slip Type</h5></div>
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
      
            <div class="widget-content tab-content"  style="margin: 0 auto">
                <table class="display nowrap sortable" style="width: 100%;">
                    <tr  style="height: 80px;">
                        <td>
                            <?php foreach ($arraycategory as $key => $value) { ?>
                                <button style="height: 29px;margin-top: 6px;margin-left: 5px"  id="<?php echo $value["id"] ?>" name="<?php echo $value["id"] ?>" class="btn btn-info"><?php echo strtoupper($value["name"]) ?></button>
                            <?php } ?>
                        </td>
                    </tr>
                </table>
            </div>  
      
    </div>
</div>

<!-- all production popup -->
<?php
foreach ($arraycategory as $key => $value) {
    $querypvc = "SELECT `id`,`portfolio_name`,`profile_name` FROM `tbl_portfolioprofile` WHERE `portfolio_name` = '" . trim($value["name"]) . "' AND profile_name!='' ORDER BY `profile_name`";
    $resultsetpvc = MysqlConnection::fetchCustom($querypvc);
    ?>
    <div id="<?php echo $value["id"] . "model" ?>" class="modal hide" style="top: 4%;left: 42%;width: 50%;height: 60%; overflow: hidden">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">×</button>
            <h3>CREATE PACKING SLIP</h3>
        </div>
        <div style="width: 100%;">
            <div style="height: 400px;overflow: auto;">
                <table class="table table-bordered" style="width: 100%">
                    <tr>
                        <td>
                            <table id="example" class="itemtable" style="width:100%">
                                <thead style="line-height: 13px;background-color: #A9CDEC;">
                                    <tr >
                                        <td style="width: 10%;color: white;line-height: 13px;">#</td>
                                        <td style="color: white;line-height: 13px;width: 118px"><i class="fa fa-fw fa-sort"></i>Profile Name</td>
                                    </tr>
                                </thead>
                                <tbody >
                                    <?php
                                    $index1 = 0;
                                    foreach ($resultsetpvc as $pvcprofile) {
                                        $index1++;
                                        ?>
                                        <tr>
                                            <td><?php echo $index1 ?> </td>
                                            <td >
                                                <a href="index.php?pagename=create_packingslip&profilefor=<?php echo $pvcprofile["id"] ?>&profilename=<?php echo urlencode($pvcprofile["profile_name"])?>">
                                                    <?php echo $pvcprofile["profile_name"] ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <?php
}
?>
<!-- all production popup -->

<script>
<?php foreach ($arraycategory as $key => $value) { ?>
        $("#<?php echo $value["id"] ?>").click(function() {
            $('#<?php echo $value["id"] . "model" ?>').modal('show');
        });
<?php } ?>
</script>

