<?php
$packslipid = filter_input(INPUT_GET, "packslipid");
$flag = filter_input(INPUT_GET, "flag");
$status = filter_input(INPUT_GET, "status");
$status1 == "Y" ? "Active" : "In Active";
?>

<?php if ($flag == "ackrecv") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Your Acknowledgment Received Successfully !!! Do you want to create work order ???
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=fromps_workorder&packslipId=<?php echo $packslipid ?>" id="btnSubmitFullForm" class="btn btn-info">YES</a></td>
                            <td><a href="index.php?pagename=manage_packingslip&status=active" id="btnSubmitFullForm" class="btn btn-info">NO</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($flag == "activeorinactive") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Your Packing Slip updated as <?php echo ucwords($status1) ?>
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">GO TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>


<?php if ($flag == "email") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Email has been sent Successfully!!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">GO TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>


<?php if ($flag == "yes") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Packing Slip has been Deleted Successfully !!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">GO TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>

<?php } ?>