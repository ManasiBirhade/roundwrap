<?php
error_reporting(0);
$packslipid = filter_input(INPUT_GET, "packslipId");
$flag = filter_input(INPUT_GET, "flag");
$packslip = MysqlConnection::getPackSlipFromId($packslipid);
$resultsetdetails = MysqlConnection::getPackSlipDetailsFromId($packslipid);


$customerid = $packslip["cust_id"];
$profid = $packslip["prof_id"];

$customerdetails = MysqlConnection::fetchCustom("SELECT cust_companyname FROM `customer_master` WHERE  id = '$customerid' ");

if (isset($_POST["deleteItem"])) {
    MysqlConnection::delete("DELETE FROM packslip WHERE ps_id = '$packslipid' ");
    MysqlConnection::delete("DELETE FROM packslipdetail WHERE ps_id = '$packslipid' ");
    header("location:index.php?pagename=success_packingslip&packslipid=$packslipId&flag=yes");
}
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="packingslip/packingslipjs.js"></script>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form   method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">PACKING SLIP VIEW</a></li>
                </ul>
            </div>
            <?php
            if ($flag == "yes") {
                
                echo "<BR/><div class='alert alert-error'><strong>Do you want to delete this record ??</strong>"
               ."<br/><br/>"
                . "<strong>This will delete all related quotation and work orders as well </strong>"
                . "</div>";
            }
            ?>
            <table style="width: 100%" class="display nowrap sortable">
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr >
                                <td style="width: 10%"><label>CUSTOMER DETAILS</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td>
                                    <input  type="text" value="<?php echo $customerdetails[0]["cust_companyname"] ?>"readonly=""/>
                                </td>
                                <td style="font-weight: bold; " ><label >SO&nbsp;NO</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input type="text"   id="so_no" value="<?php echo $packslip["so_no"] ?>" readonly="" ></td>
                                <td style="font-weight: bold; " ><label >PO&nbsp;NO</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input type="text" name="po_no" id="po_no" value="<?php echo $packslip["po_no"] ?>" readonly=""></td>
                            </tr>

                            <tr >
                                <td ><label >Square</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input type="text" readonly="" name="square" value="<?php echo $packslip["square"] ?>" ></td>
                                <td ><label >PACK&nbsp;DATE</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input type="text" name="rec_date" readonly="" value="<?php echo MysqlConnection::convertToPreferenceDate(date("Y-m-d")) ?>" readonly=""></td>
                                <td ><label c>REQUIRED&nbsp;DATE</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input type="text" name="req_date"  value="<?php echo MysqlConnection::convertToPreferenceDate($packslip["req_date"]) ?>" readonly=""></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 25%;float: left">
                            <table class="table-bordered" id="profiletable" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <?php
                                $portfolioname = MysqlConnection::getPortfolioProfileById($packslip["prof_id"]);
                                $packlebels = explode(",", $packslip["packlebels"]);
                                $packvalues = explode(",", $packslip["packvalues"]);
                                ?> 
                                <tr style="height: 30px;font-weight: bold">
                                    <td style="width: 50%;">Profile</td>
                                    <td ><?php echo $portfolioname["portfolio_name"] . " - " . $portfolioname["profile_name"] ?></td>
                                </tr>
                                <?php
                                for ($inx = 0; $inx < count($packlebels); $inx++) {
                                    $explebval = explode("--", $packvalues[$inx]);
                                    ?> 
                                    <tr style="height: 30px;font-weight: bold">
                                        <td style="width: 50%;"><?php echo ucwords(str_replace("_", " ", $packlebels[$inx])) ?></td>
                                        <td><?php echo ucwords($explebval[0]) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?> 
                            </table>
                        </div>
                        <div style="width: 73%;float: right">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 100px;">Pcs</td>
                                    <td style="width: 250px;">Type</td>
                                    <td style="width: 80px;">(W) MM</td>
                                    <td style="width: 80px;">(H) MM</td>
                                    <td style="width: 80px;">(W) INCH</td>
                                    <td style="width: 80px;">(H) INCH</td>
                                    <td>SQ-FT</td>

                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $index = 0;
                                    foreach ($resultsetdetails as $key => $value) {
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px"><?php echo ++$index ?></td>
                                            <td style="width: 100px"><?php echo $value["pcs"] ?></td>
                                            <td style="width: 250px;"><?php echo $value["type"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["mm_w"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["mm_h"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["fract_w"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["fract_h"] ?></td>
                                            <td><?php echo $value["sqFeet"] ?></td>
                                        </tr>
                                    <?php } ?>

                                </table>
                            </div>

                            <br/>
                            <b style="color: red">
                                Packing note : <?php echo $packslip["packingnote"] ?>
                            </b>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>

                    <?php
                    $packslipId = filter_input(INPUT_GET, "packslipId");
                    if (isset($_GET["flag"])) {
                        ?>
                        <a href="javascript:history.back()" class=" btn btn-info">CANCEL</a>
                        <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info"/>
                        <input type="hidden" name="packslipId" value="<?php echo $packslipId ?>"/>
                        <?php
                    } else {
                        ?>
                        <a href="index.php?pagename=fromps_workorder&packslipId=<?PHP echo $packslipId ?>" class=" btn btn-info">CREATE WORK ORDER</a>
                        <a href="index.php?pagename=create_quotation&psid=<?PHP echo $packslipId ?>" class=" btn btn-info">CREATE QUOTATION</a>
                        <a href="javascript:history.back()" class=" btn btn-info">CANCEL</a>
                        <?php
                    }
                    ?>
                </center>
            </div>
        </div>
    </div>
</form>



