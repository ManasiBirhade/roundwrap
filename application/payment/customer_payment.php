<style>
    table tbody {

    }
    table tr td{
        padding: 5px;
    }
</style>
<?php
$customerid = filter_input(INPUT_GET, "customerId");

$customer = MysqlConnection::getCustomerDetails($customerid);
$customercontactarray = MysqlConnection::getCustomerContactDetails($customerid);
$_SESSION["msg"] = "";
if (isset($_POST["paidAmount"]) && $_POST["paidAmount"] != "") {
    $_POST["cust_id"] = $customerid;
    $_POST["supp_id"] = "0";
    $_POST["active"] = "Y";
    $_POST["oriamt"] = $_POST["balanceAmount"];
    unset($_POST["paidDate"]);
    unset($_POST["balanceAmount"]);
    MysqlConnection::insert("customer_balancepayment", $_POST);
    MysqlConnection::delete("UPDATE customer_master SET balance = " . $_POST["balance"] . " WHERE id =  '$customerid'");
    $_SESSION["msg"] = "Payment of " . $_POST["paidAmount"] . " added ";
    header("location:index.php?pagename=customer_payment&customerId=" . $customer["id"]);
}
$arrreceiptno = MysqlConnection::fetchCustom("SELECT id FROM `customer_balancepayment` ORDER BY id desc");
$receiptno = "CR-" . time();
$resultset = MysqlConnection::fetchCustom("SELECT * FROM `customer_balancepayment` where cust_id = '$customerid' ORDER BY paidDate DESC");
?> 

<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 0px;margin: 0px;}
</style>
<form name="purchaseorder" method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">CUSTOMER PAYMENT</a></li>
                </ul>
            </div>
            <br/>
            <?php if ($_SESSION["msg"] != "") { ?>
                <center >
                    <br/>
                    <div class='alert alert-error'><strong>
                            <?php echo $_SESSION["msg"] ?>
                        </strong>
                    </div>
                </center>
            <?php } ?>
            <table style="width: 100%">
                <tr>
                    <td>
                        <div style="width: 70%;float: right">
                            <table class="display nowrap sortable table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #76323F;color: white">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 120px;">Receipt.No</td>
                                    <td style="width: 120px">Balance.Amount</td>
                                    <td style="width: 120px;">Paid.Date</td>
                                    <td style="width: 120px;">Cheque.No/DD.No</td>
                                    <td style="width: 120px;">Paid.Amount</td>
                                    <td>Remark</td>
                                </tr>
                                <?php
                                $index = 1;
                                foreach ($resultset as $key => $value) {
                                    ?>
                                    <tr style="border-bottom: solid 1px  #CDCDCD;background-color: white;">
                                        <td style="width: 25px"><?php echo $index ++ ?></td>
                                        <td style="width: 120px;"><?php echo $value["receiptNo"] ?></td>
                                        <td style="width: 120px"><?php echo $value["oriamt"] ?></td>
                                        <td style="width: 120px;"><?php echo $value["paidDate"] ?></td>
                                        <td style="width: 120px;"><?php echo $value["chequeNoDDNo"] ?></td>
                                        <td style="width: 120px;"><?php echo $value["paidAmount"] ?></td>
                                        <td ><?php echo $value["remark"] ?></td>
                                    </tr>
                                <?php } ?>

                            </table>
                        </div>
                        <div style="width: 28%;float: left">
                            <table class="display nowrap sortable table-bordered" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <tr >
                                    <td><b>Receipt No</b></td>
                                    <td><input type="text" readonly="" name="receiptNo" id="receiptNo" value="<?php echo $receiptno ?>"></td>
                                </tr>
                                <tr >
                                    <td><b>Customer Name</b></td>
                                    <td><input type="text" readonly="" value="<?php echo $customer["cust_companyname"] ?>"/></td>
                                </tr>

                                <tr >
                                    <td><b>Balance Amount</b></td>
                                    <td><input type="text" readonly="" name="balanceAmount" id="balanceAmount"  value="<?php echo $customer["balance"] ?>"></td>
                                </tr>

                                <tr >
                                    <td><b>Paid Date</b></td>
                                    <td><input type="text" readonly="" name="paidDate" id="paidDate"  value="<?php echo date("D-M-Y") ?>"></td>
                                </tr>
                                <tr >
                                    <td><b>Cheque No/DD No</b></td> 
                                    <td><input type="text" name="chequeNoDDNo" id="chequeNoDDNo" maxlength="10"></td>
                                </tr>
                                <tr >
                                    <td><b>Currency</b></td>
                                    <td><input type="text" readonly="" value="<?php echo $customer["currency"] ?>" ></td>
                                </tr>
                                <tr >
                                    <td><b>Paid Amount</b></td>
                                    <td><input type="text" autofocus=""  onkeypress="return chkNumericKey(event)" onkeyup="calculateAmount()" name="paidAmount" id="paidAmount"  ></td>
                                </tr>
                                <tr >
                                    <td><b>Balance</b></td>
                                    <td><input type="text" readonly="" name="balance" id="balance"  value=""></td>
                                </tr>
                                <tr >
                                    <td><b>Remark</b></td>
                                    <td><input type="text"   name="remark" id="remark" ></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer " style="text-align: center"> 
                <a id="save" class="btn btn-info" onclick=" createPurchaseOrder()">SAVE</a> 
                <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
            </div>
        </div>
    </div>
</form>
<script>
    function createPurchaseOrder() {
        var x = document.getElementsByTagName("form");
        x[0].submit();
    }

    function calculateAmount() {
        var balanceAmount = parseFloat($("#balanceAmount").val());
        var paidAmount = parseFloat($("#paidAmount").val());
        if (balanceAmount !== "" && paidAmount !== "" && !isNaN(paidAmount)) {
            if (paidAmount > balanceAmount) {
                $("#paidAmount").val("");
                $("#balance").val("");
                $("#paidAmount").focus();

            } else {
                var newbalance = balanceAmount - paidAmount;
                $("#balance").val(newbalance.toFixed(2));
            }
        } else {
            $("#balance").val("");
        }
    }
</script>
