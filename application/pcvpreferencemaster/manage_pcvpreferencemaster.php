<?php
$status = filter_input(INPUT_GET, "status");
$preferenceid = filter_input(INPUT_GET, "preferenceid");

if ($status == "category") {
    $sqlquery = "SELECT * FROM `pvcgeneric_entry` WHERE `type` = 'productioncategory'";
} else {
    $sqlquery = "SELECT * FROM `pvcgeneric_entry` ORDER BY `type` ASC";
}

$resultset = MysqlConnection::fetchCustom($sqlquery);
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PVC PREFERENCE LIST</h5>
    </div>
    <br/>
    <table  style="width: 100%">
        <tr>
            <td style="float: left">
                <a class="btn btn-info"  href="index.php?pagename=create_pcvpreferencemaster" ><i class="icon icon-list"></i>&nbsp;&nbsp;SETUP PVC PREFERENCE </a>
<!--                <a href="index.php?pagename=manage_pcvpreferencemaster&status=category" id="btnOrder" class="btn btn-info">VIEW PRODUCTION CATEGORY</a>
                <a href="index.php?pagename=manage_pcvpreferencemaster" id="btnOrder" class="btn btn-info">VIEW ALL DATA</a>-->
            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Type</td>
                <td>Code</td>
                <td>Name</td>
                <td>Description</td>
                <td>Active</td>
            </tr>
        </thead>
        <tbody>
<?php
$index = 1;
foreach ($resultset as $key => $value) {
    $bgcolor = MysqlConnection::generateBgColor($index);
    ?>
                <tr id="<?php echo $value["id"] ?>" style="background-color: <?php echo $bgcolor ?>;" class="context-menu-one">
                    <td ><?php echo $index ?></td>
                    <td ><?php echo ucwords(str_replace("_", " ", $value["type"])) ?></td>
                    <td ><?php echo $value["code"] ?></td>
                    <td ><?php echo $value["name"] ?></td>
                    <td style="width: 40%">
    <?php echo $value["description"] ?>
                    </td>
                    <td >
    <?php echo $value["active"] == "Y" ? "<span class='badge badge-success'>YES</span>" : "<span class='badge badge-info'>NO</span>" ?>
                    </td>
                </tr>
    <?php
    $index++;
}
?>
        </tbody>
    </table>
</div>

<script type="text/javascript">//
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_setup":
                        window.location = "index.php?pagename=view_pcvpreferencemaster&preferenceid=" + id;
                        break;
                    case "create_setup":
                        window.location = "index.php?pagename=create_pcvpreferencemaster";
                        break;
                    case "edit_setup":
                        window.location = "index.php?pagename=create_pcvpreferencemaster&preferenceid=" + id;
                        break;
                    case "delete_setup":
                        window.location = "index.php?pagename=view_pcvpreferencemaster&preferenceid=" + id + "&flag=yes";
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_pcvpreferencemaster";
                }
            },
            items: {
                "view_setup": {name: "VIEW PVC PREFERENCE", icon: ""},
                "edit_setup": {name: "EDIT PVC PREFERENCE", icon: ""},
                "delete_setup": {name: "DELETE PVC PREFERENCE", icon: ""}
            }
        });
    });

    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_pcvpreferencemaster&preferenceid=" + id;
        }
    });
</script>