<?php
$preferenceid = filter_input(INPUT_GET, "preferenceid");
$flag = filter_input(INPUT_GET, "flag");
if (isset($preferenceid)) {
    $resultset = MysqlConnection::fetchCustom("SELECT * FROM pvcgeneric_entry WHERE id = '$preferenceid'");
    $date = $resultset[0];
}

$array = array();
$array["type"] = filter_input(INPUT_POST, "type");
$array["code"] = filter_input(INPUT_POST, "code");
$array["name"] = filter_input(INPUT_POST, "name");
$array["description"] = filter_input(INPUT_POST, "description");
$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
if (isset($btnSubmitFullForm)) {
    $array["active"] = "Y";
    if (isset($preferenceid)) {
        MysqlConnection::edit("pvcgeneric_entry", $array, " id = '$preferenceid' ");
    } else {
        MysqlConnection::insert("pvcgeneric_entry", $array);
    }
    header("location:index.php?pagename=manage_pcvpreferencemaster");
}

$arrpreferencetype = array();
$arrpreferencetype["dateformat"] = "Date format";
$arrpreferencetype["kerfvalue"] = "Kerf value";
$arrpreferencetype["imperial"] = "Imperial";
$arrpreferencetype["representative"] = "Representative";
$arrpreferencetype["customer_type"] = "Customer Type";
$arrpreferencetype["paymentterm"] = "Payment Term";
$arrpreferencetype["usertype"] = "User Type";
$arrpreferencetype["ipaddress"] = "IP Address";
$arrpreferencetype["scannerstep"] = "Scanner Step";
$arrpreferencetype["shipvia"] = "Ship Via";

if (isset($_POST["deleteUser"])) {
    $id = $_POST["id"];
    MysqlConnection::delete("DELETE FROM `pvcgeneric_entry` WHERE id = '$preferenceid' ");
    header("location:index.php?pagename=manage_pcvpreferencemaster");
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">MANAGE PREFERENCE</h5>
    </div>
    <?php
    if ($flag == "yes") {
        echo MysqlConnection::$DELETE;
    }
    ?>
    <div class="widget-box" >
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Setup Information </a></li>
            </ul>
        </div>
        <form name="frmCmpSubmit"  enctype="multipart/form-data" id="frmCmpSubmit" method="post" >
            <input type="hidden" name="id" id="id" value="<?php echo $date["id"] ?>" />
            <div class="widget-content tab-content">

                <table class="display nowrap sortable table-bordered"  style="width: 60%;vertical-align: top" border="0">
                    <tr>
                        <td style="width: 150px;"><label class="control-label"><b>Preference Type</b></label></td>
                        <td><?php echo ucwords(str_replace("_", " ", $date["type"])) ?></td>
                    </tr>
                    <tr>
                        <td style="width: 150px;"><label class="control-label"><b>Code</b></label></td>
                        <td><?php echo $date["code"] ?></td>
                    </tr> 
                    <tr>
                        <td style="width: 150px;"><label class="control-label"><b>Name / Value</b></label></td>
                        <td><?php echo $date["name"] ?></td>
                    </tr> 
                    <tr>
                        <td><label class="control-label"><b>Description</b></label></td>
                        <td ><?php echo trim($date["description"]) ?></td>
                    </tr>
                </table>

            </div>
            <div class="modal-footer" style="border-bottom: solid 1px gray;" > 



                <?php
                if (isset($flag) && $flag != "") {
                    ?>
                    <form name="frmDeleteUser" id="frmDeleteUser" method="post">
                        <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                        <input  type="submit" value="DELETE" name="deleteUser" class="btn btn-info"/>
                        <input type="hidden" name="id" value="<?php echo $id ?>"/>
                    </form>
                    <?php
                } else {
                    echo '  <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>';
                }
                ?>
            </div>
        </form>
    </div>
</div> 