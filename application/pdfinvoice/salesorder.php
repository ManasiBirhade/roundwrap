<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 0px solid black; }
    #items th { background: #eee; }
    table{border: solid 0px;}
    table td, table th { border: 0px; padding: 5px; }

    end_last_page div
    {
        height: 27mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 75%;text-align: left;border: solid 0px;" >
                <img src="http://localhost/roundwrap/trunk/roundwrapnew/assets/images/download.png" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;">
                    RoundWrap Industries 1680 Savage Rd, Richmond, BC V6V 3A9, Canada <br/><b>Phone:</b> +1 604-278-1002
                </p>
            </td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2">SALES ORDER</td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>SO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b>PO6713</b></td>
                    </tr>

                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>Date:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b>2018-06-13</b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table style="width: 100%;height: 200px;">
        <tr style="vertical-align: top">
            <td style="width: 45%;line-height: 20px;border: solid 0px;">
                <b>Bill To</b>
                <br/>
                <b>3M Canada Co.</b>,<br/>N5V 4M9 300 Tartan Dr London canada<br/>British Columbia <br/>(800) 265-1840  canadacustomerorders@mmm.com                            
            </td>
            <td style="width: 10%;border: solid 0px ;"></td>
            <td style="width: 45%;line-height: 20px;text-align: right;border: solid 0px;">
                <b>Ship To</b>
                <br/>
                <b>RoundWrap Industries</b><br/>1680 Savage Rd, Richmond, BC V6V 3A9, <br/><b>Phone:</b> +1 604-278-1002
            </td>
        </tr>
    </table>
    <br/>
    <table id="items" style="margin-top: 0px;text-align: center" border='0'>
        <tr  style="border: solid 0px;">
            <th style="width: 35%;border: solid .2px;">SHIP.DATE</th>
            <th style="width: 35%;border: solid .2px;">SHIP.VIA</th>
            <th style="width: 30%;border: solid .2px;">REP</th>
        </tr>
        <tr style="height: 30px;border: solid 1px;">
            <td style="border: solid .2px;">2018-06-14&nbsp;&nbsp;</td>
            <td style="border: solid .2px;">First Flight</td>
            <td style="border: solid .2px;">1</td>
        </tr>
    </table>
    <br/>
    <table>
        <col style="width: 25%">
        <col style="width: 35%">
        <col style="width: 10%">
        <col style="width: 10%">
        <col style="width: 10%">
        <col style="width: 10%">
        <thead>
            <tr style="text-align: left;height: 30px;text-align: center" class="item-row">
                <th style=";border: solid .2px;">Item</th>
                <th style="border: solid .2px;">Description</th>
                <th style="border: solid .2px;">Quantity</th>
                <th style="border: solid .2px;">Unit</th>
                <th style="border: solid .2px;">Rate</th>
                <th style="border: solid .2px;">Amount</th>
            </tr>
        </thead>
        <?php
        for ($k = 0; $k < 40; $k++) {
            if ($k % 2 == 0) {
                $back = "rgb(240,240,240)";
            } else {
                $back = "white";
            }
            ?>
            <tr class="item-row" style="background-color:<?php echo $back ?> ">
                <td style="border-left:  solid .2px;">&nbsp;11-2111-P48GL</td>
                <td style="border: solid 0px;"><p style="text-align: justify;line-height: 20px;">Lamitech Polar White Gloss 48x96</p></td>
                <td style="text-align:center;border-left:  solid 0px">&nbsp;10</td>
                <td style="text-align:center;border-left:  solid 0px">&nbsp;Each</td>
                <td style="text-align:right;border-left:  solid 0px">30.60&nbsp;</td>
                <td style="text-align: right;border-right:   solid .2px">306.00&nbsp;</td>
            </tr>
            <?php
        }
        ?>
    </table>
    <table style="margin: 0px;margin-top: -.2px;">
        <tr style="text-align: left;height: 30px;text-align: center" class="item-row">
            <th style="width: 80%;border-right:  solid .1px;"></th>
            <th style="width: 10%;border: solid .2px;">Subtotal</th>
            <th style="width: 10%;border: solid .2px;"></th>
        </tr>
        <tr style="text-align: left;height: 30px;text-align: center" class="item-row">
            <th style="width: 80%;border-right:  solid .1px;"></th>
            <th style="width: 10%;border: solid .2px;">Subtotal</th>
            <th style="width: 10%;border: solid .2px;"></th>
        </tr>
        <tr style="text-align: left;height: 30px;text-align: center" class="item-row">
            <th style="width: 80%;border-right:  solid .1px;"></th>
            <th style="width: 10%;border: solid .2px;">Subtotal</th>
            <th style="width: 10%;border: solid .2px;"></th>
        </tr>
    </table>
    <end_last_page end_height="35mm">
        <div>
            <table >
                <tr>
                    <td style="width: 100%;border: solid .2px;line-height: 20px;font-size: 8px;">
                        <p>Terms: A Service charge will be charged to you on any balance outstanding at the end of each month at the rate of 1.5% per month calculated and compounded monthly(19.57% per annum)</p>
                        <p>
                            All manufacturer's names,number,symbols and descriptions are used for reference purposes only and it is not implied hat any part listed is the product of these manufacturers.
                            All goods listed above are the property of  until fully paid for.
                        </p>
                        <h2>Thank You for Your Continued patronage!</h2>
                        
                    </td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>