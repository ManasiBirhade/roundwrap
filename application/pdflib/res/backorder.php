<?php
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
//$pdfheader = "RoundWrap Industries 1680 Savage Rd, Richmond, BC V6V 3A9, Canada <br/><b>Phone:</b> +1 604-278-1002";
$pdftitle = "SALES ORDER";

$salesorder = MysqlConnection::getSalesOrderDetailsById($sono);
$itemsinformation1 = MysqlConnection::getSalesItemsDetailsById($sono);

$customerid = $salesorder["customer_id"];
$customer = MysqlConnection::getCustomerDetails($customerid);
$busno = $customer["businessno"];
$promo = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'promotionnote' ");
$paymentterm = $customer["paymentterm"];
$arrpayterm = MysqlConnection::getGenericById($paymentterm);


$sonumber = $salesorder["sono"];
$ponumber = $salesorder["pono"];

$date = $salesorder["soorderdate"];
$billtoaddress = $salesorder["billTo_address"];
$shiptoaddress = $salesorder["shipping_address"];
$shipdate = $salesorder["expected_date"];
$shipvia = $salesorder["shipvia"];
$shiptype = $salesorder["radios"];
$rep = $salesorder["representative"];
?>
<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0;  }
    #items th { background: #eee; }
    table td, table th {  padding: 5px; }

    end_last_page div
    {
        height: 27mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>SO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $salesorder["sono"] ?></b></td>
                    </tr>

                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>Date:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $date ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table id="items1" style="width: 100%;height: 200px;">
        <tr style="vertical-align: top;border: solid 0px;" >
            <td style="width: 40%;line-height: 15px;border: solid .2px;">
                <b>Bill To</b><br/><?php echo $billtoaddress ?>                     
                <b>Phone No :</b> <?php echo $customer["phno"] ?> <br/>                      
                <b>Fax No :</b> <?php echo $customer["cust_fax"] ?>                           
            </td>
            <td style="width: 20%;border: solid 0px ;border-right:solid .2px; "></td>
            <td style="width: 40%;line-height: 15px;text-align: left;border: solid .2px;">
                <b>Ship To</b><br/><?php echo $shiptoaddress ?>
            </td>
        </tr>
    </table>
    <br/>
    <table id="items" style="margin-top: 0px;text-align: center">
        <tr  >
            <th style="width: 15%;border: solid .2px;">PO NO</th>
            <th style="width: 20%;border: solid .2px;">Rep</th>
            <th style="width: 15%;border: solid .2px;">Term</th>
            <th style="width: 20%;border: solid .2px;">SHIP.DATE</th>
            <th style="width: 15%;border: solid .2px;">SHIP.VIA</th>
            <th style="width: 15%;border: solid .2px;">SHIP.TYPE</th>
        </tr>
        <tr style="height: 30px;">
            <td style="border: solid .2px;"><?php echo $ponumber ?></td>
            <td style="border: solid .2px;"><?php echo $rep ?></td>
            <td style="border: solid .2px;"><?php echo $arrpayterm["code"] . " " . $arrpayterm["name"] ?></td>
            <td style="border: solid .2px;"><?php echo $shipdate ?></td>
            <td style="border: solid .2px;"><?php echo $shipvia ?></td>
            <td style="border: solid .2px;"><?php echo $shiptype ?></td>

        </tr>
    </table>

    <table id="items">
        <col style="width: 20%">
        <col style="width: 35%">
        <col style="width: 10%">
        <col style="width: 10%">
        <col style="width: 15%">
        <col style="width: 10%">
        <thead>
            <tr style="text-align: left;height: 30px;text-align: center" >
                <th style="border: solid .2px;">Item</th>
                <th style="border: solid .2px;">Description</th>
                <th style="border: solid .2px;">Unit</th>
                <th style="border: solid .2px;">ORD.QTY</th>
                <th style="border: solid .2px;">SHIPPED.QTY</th>
                <th style="border: solid .2px;">BACK.QTY</th>

            </tr>
        </thead>
        <?php
        $k = 1;
        foreach ($itemsinformation1 as $key => $value1) {
            $itemid = $value1["item_id"];
            $items = MysqlConnection::getItemDataById($itemid);
            if ($k % 2 == 0) {
                $back = "rgb(240,240,240)";
            } else {
                $back = "white";
            }
            ?>
            <tr >
                <td style="border: solid .2px;">&nbsp;<?php echo $items["item_code"] ?></td>
                <td style="border: solid .2px;"><p style="text-align: justify;line-height: 20px;"><?php echo $items["item_desc_sales"] ?></p></td>
                <td style="border: solid .2px;text-align:center;">&nbsp;<?php echo $items["unit"] ?></td>
                <td style="border: solid .2px;text-align:center;">&nbsp;<?php echo $value1["qty"] - $value1["returnQty"] ?></td>
                <td style="border: solid .2px;text-align:right;"></td>
                <td style="border: solid .2px;text-align: right;"></td>
            </tr>
            <?php
            $k++;
        }
        ?>
        <?php if ($busno != "") { ?>
            <tr>
                <td colspan="6">&nbsp;Bussiness no.: <?php echo $busno ?></td>
            </tr>
        <?php } ?>
    </table>

    <end_last_page end_height="50mm">
        <div>
            <table >
                <tr  style="border: solid 0px;">
                    <th style="width: 50%;border: solid .2px;">Remark/ Note</th>
                    <th style="width: 50%;border: solid .2px;"><p>

                </p></th>
                </tr>
                <tr  style="border: solid 0px;vertical-align: top">
                    <td style="width: 50%;height:20%;border: solid .2px;"><p ><b><i style="font-size: 16px"><?php echo $salesorder["remark"] ?></i></b></p></td>
                    <td style="width: 50%;height: 5%;border: solid .2px;"><p style="font-size: 25px"><b><?php echo $promo[0]["description"] ?></b></p></td>
                </tr>
            </table>
            <table >
                <tr  >
                    <td style="border: solid 1px;height:7%">Pcs:</td>
                    <td style="border: solid 1px" >Picked by:</td>
                    <td style="border: solid 1px; text-align: center"></td>
                </tr>
                <tr>
                    <td style="border: solid 1px; width: 58%" colspan="2"> <p style="text-align: justify">Refunded goods will not be accepted unless by prior arrangement and accompanied by the original packing slip. Goods returned which were furnished according to order may be accepted by us,less a 25% restocking charge.
                            All goods listed above are the property of RoundWrap Industries until fully paid for.</p></td>

                    <td style="border: solid 1px" ><br/><br/>Received in good order _______________________________</td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>