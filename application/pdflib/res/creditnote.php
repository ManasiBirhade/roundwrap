<?php
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0]["description"];



$salesorder = MysqlConnection::getSalesOrderDetailsById($salesorderid);
$isRestock = $salesorder["isRestock"];
$pdftitle = "CREDIT NOTE";
//$itemsinfo = MysqlConnection::getSalesItemsDetailsById($salesorderid);

$promo = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'promotionnote' ");
$invoiceid = $salesorder["invoiceno"];

$customerid = $salesorder["customer_id"];
$customer = MysqlConnection::getCustomerDetails($customerid);

$paymentterm = $customer["paymentterm"];
$busno = $customer["businessno"];
$arrpayterm = MysqlConnection::getGenericById($paymentterm);


$sonumber = $salesorder["sono"];
$ponumber = $salesorder["pono"];
$salesorder["soorderdate"] = $salesorder["soorderdate"];
$date = $salesorder["soorderdate"];
$billtoaddress = $salesorder["billTo_address"];
$shiptoaddress = $salesorder["shipping_address"];
$salesorder["expected_date"] = $salesorder["expected_date"];
$shipdate = $salesorder["expected_date"];
$shipvia = $salesorder["shipvia"];
$rep = $salesorder["representative"];
$shiptype = $salesorder["radios"];
$discount = $salesorder["discount"];
?>
<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
    #items th { background: #eee; }
    #items1 { clear: both; width: 100%; margin: 30px 0 0 0; border: none;}

    table td, table th { padding: 5px; }

    end_last_page div
    {
        height: 27mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->

    #background{
        position:absolute;
        z-index:0;
        display:block;
        min-height:20%; 
        min-width:20%;
        color:yellow;
    }

    #content{
        position:absolute;
        z-index:1;
    }

    #bg-text
    {
        color:red;
        font-size:30px;
        transform:rotate(300deg);
        -webkit-transform:rotate(300deg);
    }
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>INVOICE NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $invoiceid ?></b></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>SO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $salesorder["sono"] ?></b></td>
                    </tr>

                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>Date:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $date ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table id="items1" style="width: 100%;height: 200px;">
        <tr style="vertical-align: top;border: solid 0px;" >
            <td style="width: 40%;line-height: 18px;border: solid .2px;">
                <b>Bill To</b><br/><?php echo MysqlConnection::formatToBRAddress($billtoaddress) ?>  
                <b>Phone No :</b> <?php echo $customer["phno"] ?> <br/>                      
                <b>Fax No :</b> <?php echo $customer["cust_fax"] ?> 
            </td>
            <td style="width: 20%;border: solid 0px ;border-right:solid .2px; "></td>
            <td style="width: 40%;line-height: 18px;text-align: left;border: solid .2px;">
                <b>Ship To</b><br/><?php echo MysqlConnection::formatToBRAddress($shiptoaddress) ?>
            </td>
        </tr>
    </table>
    <br/>
    <table id="items" style="margin-top: 0px;text-align: center" border='0'>
        <tr  style="border: solid 0px;">
            <th style="width: 15%;border: solid .2px;">PO NO</th>
            <th style="width: 20%;border: solid .2px;">Rep</th>
            <th style="width: 15%;border: solid .2px;">Term</th>
            <th style="width: 20%;border: solid .2px;">SHIP.DATE</th>
            <th style="width: 15%;border: solid .2px;">SHIP.VIA</th>
            <th style="width: 15%;border: solid .2px;">SHIP.TYPE</th>
        </tr>
        <tr style="height: 30px;border: solid 1px;">
            <td style="border: solid .2px;"><?php echo $ponumber ?></td>
            <td style="border: solid .2px;"><?php echo $rep ?></td>
            <td style="border: solid .2px;"><?php echo $arrpayterm["code"] . " " . $arrpayterm["name"] ?></td>
            <td style="border: solid .2px;"><?php echo $shipdate ?></td>
            <td style="border: solid .2px;"><?php echo $shipvia ?></td>
            <td style="border: solid .2px;"><?php echo $shiptype ?></td>
        </tr>
    </table>

    <table style="border: solid" id="items">
        <col style="width: 25%">
        <col style="width: 35%">
        <col style="width: 10%">
        <col style="width: 10%">
        <col style="width: 10%">
        <col style="width: 10%">
        <thead>
            <tr style="text-align: left;height: 30px;text-align: center" >
                <th style="border: solid .2px;">Item</th>
                <th style="border: solid .2px;">Description</th>
                <th style="border: solid .2px;">Quantity</th>
                <th style="border: solid .2px;">Unit</th>
                <th style="border: solid .2px;">Rate</th>
                <th style="border: solid .2px;">Amount</th>
            </tr>
        </thead>
        <?php
        $k = 1;
        $totalGoods = 0;
        $fetchqyery = "SELECT * FROM `sales_item` where so_id = '$salesorderid' AND restockQty !=0";
        $itemsinfo = MysqlConnection::fetchCustom($fetchqyery);
        foreach ($itemsinfo as $key => $value) {
            $itemid = $value["item_id"];
            $items = MysqlConnection::getItemDataById($value["item_id"]);
            $sellqtyfromhistory = $value["restockQty"];

            $sqlpriceofsalesitem = "SELECT price FROM `tbl_selling_history` WHERE so_id = '$salesorderid' AND item_id = '$itemid'  ";
            $priceofsaleitem = MysqlConnection::fetchCustom($sqlpriceofsalesitem);
            $itemprice = $priceofsaleitem[0]["price"];

            if ($k % 2 == 0) {
                $back = "rgb(240,240,240)";
            } else {
                $back = "white";
            }
            $totalGoods = $totalGoods + ($itemprice * $sellqtyfromhistory);
            ?>
            <tr>
                <td style="border:  solid .2px;">&nbsp;<?php echo $items["item_code"] ?></td>
                <td style="border: solid .2px;"><p style="text-align: justify;line-height: 20px;"><?php echo $items["item_desc_sales"] ?></p></td>
                <td style="text-align:center;border:  solid .2px">&nbsp;-<?php echo $sellqtyfromhistory ?></td>
                <td style="text-align:center;border:  solid .2px">&nbsp;<?php echo $items["unit"] ?></td>
                <td style="text-align:right;border:  solid .2px"><?php echo $itemprice ?>&nbsp;</td>
                <td style="text-align: right;border:   solid .2px"><?php echo number_format((float) ($itemprice * $sellqtyfromhistory), 2, '.', ''); ?>&nbsp;&nbsp;</td>
            </tr>
            <?php
            $k++;
        }
        ?>

    </table>
    <table style="margin: 0px;margin-top: -.2px;border: solid 1px">
        <tr style="text-align: left;height: 30px;border: solid .2px" >
            <td style="width: 70%;border-right: solid .2px">
                <?php if ($busno != "") { ?>
                    &nbsp;Business no.: <?php echo $busno ?>
                <?php } ?>
            </td>
            <td style="width: 20%;text-align: right;border: solid .2px">Total</td>
            <td style="width: 10%;text-align: right;border: solid .2px"><?php echo $salesorder["sub_total"]; ?>&nbsp;</td>
        </tr>
        <?php
        if ($discount != 0) {
            ?>
            <tr style="text-align: left;height: 30px;border: solid .2px" >
                <td style="width: 70%;border-right: solid .2px"></td>
                <td style="width: 20%;text-align: right;border: solid .2px">Discount</td>
                <td style="width: 10%;text-align: right;border: solid .2px"><?php echo round($salesorder["discount"], 2); ?>&nbsp;</td>
            </tr>
        <?php } ?>

        <?php
        $total = $totalGoods - $salesorder["discount"];
        $taxid = $salesorder["taxid"];
        $taxinformation = MysqlConnection::getTaxInfoById($taxid);
        $taxvalue = $taxinformation["taxvalues"];
        $taxamount = $total * $taxvalue / 100;
        ?>   
        <tr style="text-align: left;height: 30px;border: solid .2px"  >
            <td style="width: 70%;border-right: solid .2px"></td>
            <td style="width: 20%;text-align: right;border: solid .2px">Tax <?php echo $salesorder["taxname"]; ?></td>
            <td style="width: 10%;text-align: right;border: solid .2px"><?php echo $salesorder["taxamount"] ?>&nbsp;</td>
        </tr>

        <?php if ($salesorder["ship_charge"] != "0.00") { ?>
            <tr style="text-align: left;height: 30px;border: solid .2px" >
                <td style="width: 70%;border-right: solid .2px"></td>
                <td style="width: 20%;text-align: right;border: solid .2px">Shipping Charge</td>
                <td style="width: 10%;text-align: right;border: solid .2px"><?php echo $salesorder["ship_charge"]; ?>&nbsp;</td>
            </tr>

            <tr style="text-align: left;height: 30px;border: solid .2px"  >
                <td style="width: 70%;border-right: solid .2px;border: solid .2px"></td>
                <td style="width: 20%;text-align: right;border: solid .2px">Shipping Tax-<?php echo $salesorder["staxname"] == "" ? "GST" : $salesorder["staxname"]; ?></td>
                <td style="width: 10%;text-align: right;border: solid .2px"><?php echo $salesorder["staxpercent"]; ?>&nbsp;</td>
            </tr>
        <?php } ?>
        <tr style="text-align: left;height: 30px;border: solid .2px"  >
            <td style="width: 70%;border-right: solid .2px"></td>
            <td style="width: 20%;text-align: right;border: solid .2px">Restock Charges</td>
            <td style="width: 10%;text-align: right;border: solid .2px"><?php echo $salesorder["restockamount"]; ?>&nbsp;</td>
        </tr>
        <tr style="text-align: left;height: 30px;border: solid .2px"  >
            <td style="width: 70%;border-right: solid .2px"></td>
            <td style="width: 20%;text-align: right;border: solid .2px">Net Total</td>
            <td style="width: 10%;text-align: right;border: solid .2px"><?php echo $salesorder["total"] ?>&nbsp;</td>
        </tr>
    </table>
    <end_last_page end_height="35mm">
        <div>
            <table >
                <tr>
                    <td style="width: 100%;border: solid .2px;line-height: 20px;font-size: 8px;">
                        <p>Terms: A Service charge will be charged to you on any balance outstanding at the end of each month at the rate of 1.5% per month calculated and compounded monthly(19.57% per annum)</p>
                        <p>
                            All manufacturer's names,number,symbols and descriptions are used for reference purposes only and it is not implied hat any part listed is the product of these manufacturers.
                            All goods listed above are the property of  until fully paid for.
                        </p>
                        <h2>Thank You for Your Continued patronage!</h2>
                    </td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>