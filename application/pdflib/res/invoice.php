<?php
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
//$pdfheader = "RoundWrap Industries 1680 Savage Rd, Richmond, BC V6V 3A9, Canada <br/><b>Phone:</b> +1 604-278-1002";
$pdftitle = "INVOICE";


$customer = "";
$arraypackslip = array();
$invoicedetails = MysqlConnection::fetchCustom("SELECT * FROM `tbl_invoice` where id = '$invoiceprimary'");
$invoice = $invoicedetails[0];
foreach ($checkedps as $packslipid) {
    $packingslip = MysqlConnection::getPackSlipFromId($packslipid);
    array_push($arraypackslip, $packingslip);
    $customer = MysqlConnection::getCustomerDetails($packingslip["cust_id"]);
}
?>
<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 0px solid black; }
    #items th { background: #eee; }
    table{border: solid 0px;}
    table td, table th { border: 0px; padding: 5px; }

    end_last_page div
    {
        height: 27mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>Invoice Number:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $invoice["invoicenumber"] ?></b></td>
                    </tr>

                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>Date :&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo MysqlConnection::convertToPreferenceDate($invoice["invoicedate"]) ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table style="width: 100%;height: 200px;">
        <tr style="vertical-align: top">
            <td style="width: 45%;line-height: 20px;border: solid 0px;">
                <b>BILLING ADDRESS</b><br/><?php echo $customer["billto"] ?>                         
            </td>
            <td style="width: 10%;border: solid 0px ;"></td>
            <td style="width: 45%;line-height: 20px;text-align: right;border: solid 0px;"></td>
        </tr>
    </table>
    <br/>
    <table id="items">
        <col style="width: 20%">
        <col style="width: 15%">
        <col style="width: 15%">
        <col style="width: 15%">
        <col style="width: 15%">
        <col style="width: 20%">
        <thead>
            <tr style="text-align: left;height: 30px;text-align: center" >
                <th style="border: solid .2px;">PROFILE</th>
                <th style="border: solid .2px;">SO NUMBER</th>
                <th style="border: solid .2px;">PO NUMBER</th>
                <th style="border: solid .2px;">CREATE DATE</th>
                <th style="border: solid .2px;">TAX</th>
                <th style="border: solid .2px;">NET TOTAL</th>

            </tr>
        </thead>
        <?php
        $k = 1;
        $nettotal = 0.0;
        foreach ($arraypackslip as $key => $value1) {
            $profiledata = MysqlConnection::fetchCustom("SELECT profile_name FROM `tbl_portfolioprofile`");
            ?>
            <tr >
                <td style="border: solid .2px;"><?php echo $profiledata[0]["profile_name"] ?></td>
                <td style="border: solid .2px;"><?php echo $value1["so_no"] ?></td>
                <td style="border: solid .2px;text-align:center;"><?php echo $value1["po_no"] ?></td>
                <td style="border: solid .2px;text-align:center;"><?php echo MysqlConnection::convertToPreferenceDate($value1["rec_date"]) ?></td>
                <td style="border: solid .2px;text-align:right;"><?php echo $value1["taxname"] ?>-<?php echo $value1["taxvalue"] ?></td>
                <td style="border: solid .2px;text-align: right;"><?php echo $value1["nettotal"] ?></td>
            </tr>
            <?php
            $k++;
            $nettotal = $nettotal + $value1["nettotal"];
        }
        ?>
        <tr >
            <td style="border: solid .2px;"></td>
            <td style="border: solid .2px;"></td>
            <td style="border: solid .2px;text-align:center;"></td>
            <td style="border: solid .2px;text-align:center;"></td>
            <td style="border: solid .2px;text-align:right;"></td>
            <td style="border: solid .2px;text-align: right;"><?php echo $nettotal ?></td>
        </tr>
    </table>

    <end_last_page end_height="35mm">
        <div>
<!--            <table >
                <tr  >
                    <td style="border: solid 1px">Pcs:</td>
                    <td style="border: solid 1px" >Picked by:</td>
                    <td style="border: solid 1px; text-align: center"><b>Ask us about our monthly specials!</b></td>
                </tr>
                <tr>
                    <td style="border: solid 1px; width: 58%" colspan="2"> <p style="text-align: justify">
                            Refunded goods will not be accepted unless by prior arrangement and accompanied by the original packing slip. 
                            Goods returned which were furnished according to order may be accepted by us,less a 25% restocking charge.
                            All goods listed above are the property of RoundWrap Industries until fully paid for.</p></td>
                    <td style="border: solid 1px" ><br/><br/>Received in good order _______________________________</td>
                </tr>
            </table>-->
        </div>
    </end_last_page>
</page>