<?php
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
//$pdfheader = "RoundWrap Industries 1680 Savage Rd, Richmond, BC V6V 3A9, Canada <br/><b>Phone:</b> +1 604-278-1002";
$pdftitle = "PACKING SLIP";

$packdetails = MysqlConnection::getPackSlipFromId($packslipid);


$packitemdetails = MysqlConnection::getPackSlipDetailsFromId($packslipid);
$customerdetails = MysqlConnection::getCustomerDetails($packdetails["cust_id"]);
$portfoliodetails = MysqlConnection::getPortfolioProfileById($packdetails["prof_id"]);




foreach ($packitemdetails as $caldetails) {
    $totalPs = $totalPs + $caldetails["pcs"];
    $totalsqft = $totalsqft + $caldetails["sqFeet"];
}
?>

<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 15px 0 0 0; border: 0px solid black; }
    #items th { background: #eee; }
    table{border: solid 0px;}
    table td, table th { border: 0px; padding: 5px; }

    end_last_page div
    {
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>PO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $packdetails["po_no"] ?></b></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>SO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $packdetails["so_no"] ?></b></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table id="items" style="width: 100%;border: solid 1px;"  >
        <tr  >
            <td style="width: 10%">Account</td>
            <td style="width: 12%;text-align: center">&nbsp;:</td>
            <td style="width: 40%"><?php echo $customerdetails["cust_companyname"] ?></td>
            <td style="width: 10%">Term</td>
            <td style="width: 12%;text-align: center">&nbsp;:</td>
            <td>
                <?php
                $resultset = MysqlConnection::fetchCustom("SELECT name FROM generic_entry WHERE id = " . $customerdetails["paymentterm"]);
                echo $resultset[0]["name"];
                ?>
            </td>
        </tr>
        <tr style="text-align: left;">
            <td >Pack Date</td>
            <td style="text-align: center">&nbsp;:</td>
            <td ><?php echo $packdetails["rec_date"] ?></td>
            <td >Required Date</td>
            <td style="text-align: center">&nbsp;:</td>
            <td ><?php echo $packdetails["req_date"] ?></td>
        </tr>
        <tr style="text-align: left;vertical-align: top;text-align: justify;">
            <td >Address</td>
            <td style="text-align: center">&nbsp;:</td>
            <td style="line-height: 15px"><?php echo MysqlConnection::formatToBRAddress($customerdetails["billto"]) ?><?php echo $customerdetails["phno"] ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table style="width: 100%">
        <tr style="vertical-align: top">
            <td style="width: 25%;vertical-align: top">
                <table style="width: 100%" >

                    <tr>
                        <td>Portfolio:</td>
                        <td>  <?php echo $portfoliodetails["portfolio_name"]; ?> </td>
                    </tr>
                    <tr>
                        <td>Profile:</td>
                        <td>  <?php echo $portfoliodetails["profile_name"]; ?> </td>
                    </tr>

                    <?php
                    $packlebels = explode(",", $packdetails["packlebels"]);
                    $packvalues = explode(",", $packdetails["packvalues"]);
                    for ($inx = 0; $inx < count($packlebels); $inx++) {
                        $expvalues = explode("--", $packvalues[$inx]);
                        ?> 
                        <tr>
                            <td><?php echo ucwords($packlebels[$inx]) ?>:</td>
                            <td><?php echo ucwords(trim($expvalues[0])) ?></td>
                        </tr>
                        <?php
                    }
                    ?> 
                </table>
                <br/>
            </td>
            <td style="width: 2%"></td>
            <td style="width: 90%;vertical-align: top">
                <table style="float: right;width: 100%" >
                    <col style="width: 5%">
                    <col style="width: 25%">
                    <col style="width: 10%">
                    <col style="width: 12%">
                    <col style="width: 12%">
                    <col style="width: 10%">
                    <thead>
                        <tr style="text-align: left;height: 30px;text-align: center" class="item-row">
                            <th style=";border: solid .2px;">Pcs</th>
                            <th style="border: solid .2px;">Type</th>
                            <th style="border: solid .2px;">(W) MM</th>
                            <th style="border: solid .2px;">(H) MM</th>
                            <th style="border: solid .2px;">(W) INCH</th>
                            <th style="border: solid .2px;">(H) INCH</th>
                            <th style="border: solid .2px;">SQ-FT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $index = 0;
                        foreach ($packitemdetails as $key => $value) {
                            ?>
                            <tr style="text-align: left;height: 30px;" class="item-row">
                                <td style="border: solid .2px;">&nbsp;<?php echo $value["pcs"]; ?></td>
                                <td style="border: solid .2px;"><p style="text-align: justify;line-height: 20px;"> <?php echo $value["type"]; ?> </p></td>
                                <td style="text-align:center;border:  solid .2px">&nbsp;<?php echo round($value["mm_w"], 1); ?></td>
                                <td style="text-align:center;border:  solid .2px">&nbsp;<?php echo round($value["mm_h"], 1); ?> </td>
                                <td style="text-align:right;border:  solid .2px"><?php echo $value["fract_w"]; ?> </td>
                                <td style="text-align:right;border:  solid .2px"><?php echo $value["fract_h"]; ?> </td>
                                <td style="text-align: right;border:   solid .2px"><?php echo round($value["sqFeet"], 2); ?>&nbsp;</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>


    <end_last_page end_height="50mm">
        <div style="clear: both">
            <table style="width: 80%" >
                <tr>
                    <td style="width: 100%">
                        <h3>Signature:</h3>
                        <br/><br/>
                        <h3><b>   X</b> ______________________________</h3>
                        <br/>
                        <p>(PICK UP DATE:                        )</p>
                        <br/>
                        <b>All veneer products delivered unfinished. Customer applies finishing coasts, UV-Restrictors etc.</b>
                        <br/>
                    </td>
                    <td>
                        <table style="margin: 0px;margin-top: -.2px;">
                            <tr style="text-align: left;height: 30px;" class="item-row">

                                <th style=";border: solid .2px;text-align: left">Total Pieces</th>
                                <th style=";border: solid .2px;text-align: right"><?php echo $packdetails["total_pieces"] ?></th>
                            </tr>
                            <tr style="text-align: left;height: 30px;" class="item-row">

                                <th style=";border: solid .2px;text-align: left">Total Weight(Ibs)</th>
                                <th style=";border: solid .2px;text-align: right">&nbsp;<?php echo "-"; ?></th>
                            </tr>
                            <tr style="text-align: left;height: 30px;" class="item-row">

                                <th style=";border: solid .2px;text-align: left">Total Ft2</th>
                                <th style=";border: solid .2px;text-align: right">&nbsp;<?php echo round($packdetails["billable_fitsquare"] ,2) ?></th>
                            </tr>

                        </table>

                    </td>
                </tr>
            </table>
            <table >
                <tr>
                    <td style="width: 100%;border: solid .2px;line-height: 20px;font-size: 8px;">
                        <p style="text-align: justify">
                            The above mentioned items are received in good condition and it is the responsibility of the customer to verify the missing or damaged item(s) within 24 hours of receipt. 
                            All items listed on this packing slip remains the property of until <?php echo $customerdetails["cust_companyname"] ?> the invoice is paid in full.
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>