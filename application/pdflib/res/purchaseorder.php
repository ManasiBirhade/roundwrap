<?php
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";

$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
//        . "<br/><b>Phone:</b> +1 604-278-1002";
$pdftitle = "PURCHASE ORDER";


$purchaseorder = MysqlConnection::getPurchaseOrderData($poid);
$itemsinformation = MysqlConnection::getPurchaseItemData($poid);
$supplierdetails = MysqlConnection::getSupplierDetails($purchaseorder["supplier_id"]);
$ponumber = $purchaseorder["purchaseOrderId"];
$date = $purchaseorder["purchasedate"];
$billtoaddress = $purchaseorder["billing_address"];
$shiptoaddress = $purchaseorder["shipping_address"];
$purchaseorder["expected_date"] = MysqlConnection::convertToPreferenceDate($purchaseorder["expected_date"]);
$shipdate = $purchaseorder["expected_date"];
$shipvia = $purchaseorder["ship_via"];
$rep = $purchaseorder["added_by"];
?>

<style type="text/css">
    <!--
    *{padding: 0px;font-size: 11px;margin: 0; padding: 0;margin-left: 20%}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 0px solid black; }
    #items th { background: #eee; }

    table td, table th {  padding: 5px; }

    end_last_page div
    {
        /*        height: 27mm;*/
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 10%"></td>
            <td style="width: 40%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>PO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $ponumber ?></b></td>
                    </tr>

                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>Date:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $date ?></b></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>SO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $purchaseorder["c_sono"]; ?></b></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>CUSTOMER NAME:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $purchaseorder["c_name"]; ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table id="items1" style="width: 100%;height: 200px;">
        <tr style="vertical-align: top;border: solid 0px;" >
            <td style="width: 40%;line-height: 18px;border: solid .2px;">

                <b>Bill To</b><br/><?php echo $billtoaddress ?>                         
            </td>
            <td style="width: 20%;border: solid 0px ;border-right:solid .2px; "></td>
            <td style="width: 40%;line-height: 18px;text-align: left;border: solid .2px;">
                <b>Ship To</b><br/><?php echo $shiptoaddress ?>
            </td>
        </tr>
    </table>
    <br/>
    <table id="items" style="margin-top: 0px;text-align: center;width: 100%" border='0'>
        <tr  style="border: solid 1px;">
            <th style="width: 55%;border: solid .2px;">PICK UP ADDRESS</th>
            <th style="width: 25%;border: solid .2px;">SHIP.DATE</th>
            <th style="width: 20%;border: solid .2px;">SHIP.VIA</th>

        </tr>
        <tr style="height: 30px;border: solid 1px;">
            <td style="border: solid .2px;width: 35%;text-align: justify"><?php echo $supplierdetails["shipto"] ?></td>
            <td style="border: solid .2px;"><?php echo $shipdate ?>&nbsp;&nbsp;</td>
            <td style="border: solid .2px;"><?php echo $shipvia ?></td>
        </tr>
    </table>

    <table id="items">
        <col style="width: 15%">
        <col style="width: 53%">
        <col style="width: 8%">
        <col style="width: 8%">
        <col style="width: 8%">
        <col style="width: 8%">
        <thead>
            <tr style="text-align: left;height: 30px;text-align: center" >
                <th style="border: solid .2px;">Item</th>
                <th style="border: solid .2px;">Description</th>
                <th style="border: solid .2px;">Quantity</th>
                <th style="border: solid .2px;">Unit</th>
                <th style="border: solid .2px;">Rate</th>
                <th style="border: solid .2px;">Amount</th>
            </tr>
        </thead>
        <?php
        $k = 1;
        foreach ($itemsinformation as $key => $value) {
            $itemid = $value["item_id"];
            $items = MysqlConnection::getItemDataById($itemid);
            if ($k % 2 == 0) {
                $back = "rgb(240,240,240)";
            } else {
                $back = "white";
            }
            ?>
            <tr  >
                <td style="border: solid .2px;">&nbsp;<?php echo $items["item_code"] ?></td>
                <td style="border: solid .2px;"><p style="text-align: justify;line-height: 20px;"> <?php echo $items["item_desc_purch"] ?></p></td>
                <td style="text-align:center;border: solid .2px">&nbsp;<?php echo $value["qty"] ?></td>
                <td style="text-align:center;border: solid .2px">&nbsp;<?php echo $items["unit"] ?></td>
                <td style="text-align:right;border:  solid .2px"><?php echo $value["price"] ?>&nbsp;</td>
                <td style="text-align: right;border: solid .2px"><?php echo number_format((float) ($value["price"] * $value["qty"]), 2, '.', ''); ?>&nbsp;</td>
            </tr>
            <?php
            $k++;
        }
        ?>
        <tr>
            <td colspan="3" style="border-right:solid .2px;"></td>
            <td colspan="2" style="border: solid .2px;text-align: right;border-left:solid .2px;">Total</td>
            <td style="border: solid .2px;text-align: right"><?php echo $purchaseorder["sub_total"]; ?></td>

        </tr>
        <?php if ($purchaseorder["discount"] != "" || $purchaseorder["discount"] != "0.00") { ?>
            <tr>
                <td colspan="3" style="border-right:solid .2px;" ></td>
                <td colspan="2" style="border: solid .2px;text-align: right;">Discount <?php echo $purchaseorder["discount"]; ?>%</td>
                <td style="border: solid .2px;text-align: right"><?php echo $purchaseorder["discountvalue"]; ?></td>

            </tr>
        <?php } ?>
        <tr>
            <td colspan="3" style="border-right:solid .2px;"></td>
            <td colspan="2" style="border: solid .2px;text-align: right;">Tax -<?php echo $purchaseorder["taxname"]; ?></td>
            <td style="border: solid .2px;text-align: right"><?php echo $purchaseorder["taxamount"]; ?></td>

        </tr>
        <?php if ($purchaseorder["ship_charge"] != "0.00") { ?>
            <tr>
                <td colspan="3" style="border-right:solid .2px;"></td>
                <td colspan="2" style="border: solid .2px;text-align: right;">Shipping charges</td>
                <td style="border: solid .2px;text-align: right"><?php echo $purchaseorder["ship_charge"]; ?></td>

            </tr>
            <tr>
                <td colspan="3" style="border-right:solid .2px;"></td>
                <td colspan="2" style="border: solid .2px;text-align: right;">Shipping Tax-<?php echo $purchaseorder["staxname"] == "" ? "GST" : $purchaseorder["staxname"]; ?></td>
                <td style="border: solid .2px;text-align: right"><?php echo $purchaseorder["staxpercent"]; ?></td>

            </tr>
        <?php } ?>
        <tr>
            <td colspan="3" style="border-right:solid .2px;" ></td>
            <td colspan="2" style="border: solid .2px;text-align: right;">Net Total</td>
            <td style="border: solid .2px;text-align: right"><?php echo $purchaseorder["total"]; ?></td>

        </tr>
    </table>

    <end_last_page end_height="55mm">
        <div>
            <table style="border: solid;" >
                <tr  style="border: solid 0px; background: #eee; ">
                    <th style="width: 50%;border: solid .2px;">Remark/ Note</th>
                </tr>
                <tr  style="border: solid 0px;vertical-align: top">
                    <td style="width: 50%;height: 5%;border: solid .2px;"><P><b><i style="font-size: 18px"><?php echo $purchaseorder["remark"] ?></i></b></p></td>

                </tr>

                <tr>
                    <td style="width: 100%;border: solid .2px;line-height: 20px;font-size: 8px;">
                        <p>Please confirm order by return fax to 604-278-14. </p>
                        <p>Purchase Order number must be referenced on ALL paperwork. </p>
                        <p style="text-align: justify">If Purchase order pricing is incorrect, please request a revised PO prior to processing the order.</p>
                        <br/>
                        <p>Authorized by:_____________________________</p>
                    </td>

                </tr>
            </table>
        </div>
    </end_last_page>
</page>