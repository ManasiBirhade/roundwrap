<?php
include '../../MysqlConnection.php';
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
//$pdfheader = "RoundWrap Industries 1680 Savage Rd, Richmond, BC V6V 3A9, Canada <br/><b>Phone:</b> +1 604-278-1002";
$pdftitle = "QUOTATION";

$packdetails = MysqlConnection::getPackSlipFromId($packslipid);
$packitemdetails = MysqlConnection::getPackSlipDetailsFromId($packslipid);
$customerdetails = MysqlConnection::getCustomerDetails($packdetails["cust_id"]);
$taxinformationtdetails = MysqlConnection::getTaxInfoById($customerdetails["taxInformation"]);
$portfolioname = MysqlConnection::getPortfolioProfileById($packdetails["prof_id"]);
?>

<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 15px 0 0 0; border: 0px solid black; }
    #items th { background: #eee; }
    table{border: solid 0px;}
    table td, table th { border: 0px; padding: 5px; }

    end_last_page div
    {
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>PO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $packdetails["po_no"] ?></b></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>QUOT ID:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $packdetails["quot_id"] ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table id="items" style="width: 100%;border: solid 1px;"  >
        <tr  >
            <td style="width: 10%">Account</td>
            <td style="width: 12%;text-align: center">&nbsp;:</td>
            <td style="width: 40%"><?php echo $customerdetails["cust_companyname"] ?></td>
            <td style="width: 10%">Term</td>
            <td style="width: 12%;text-align: center">&nbsp;:</td>
            <td>
                <?php
                $resultset = MysqlConnection::fetchCustom("SELECT name FROM generic_entry WHERE id = " . $customerdetails["paymentterm"]);
                echo $resultset[0]["name"];
                ?>
            </td>
        </tr>
        <tr style="text-align: left;">
            <td >Pack Date</td>
            <td style="text-align: center">&nbsp;:</td>
            <td ><?php echo $packdetails["rec_date"] ?></td>
            <td >Required Date</td>
            <td style="text-align: center">&nbsp;:</td>
            <td ><?php echo $packdetails["req_date"] ?></td>
        </tr>
        <tr style="text-align: left;vertical-align: top;text-align: justify;">
            <td >Address</td>
            <td style="text-align: center">&nbsp;:</td>
            <td style="line-height: 15px"><?php echo MysqlConnection::formatToBRAddress($customerdetails["billto"]) ?><?php echo $customerdetails["phno"] ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table style="width: 100%">
        <tr style="vertical-align: top">
            <td style="width: 25%;vertical-align: top">
                <table style="width: 100%" >
                    <?php ?>
                    <tr>
                        <td>Portfolio:</td>
                        <td><?php echo $portfolioname["portfolio_name"] ?></td>
                    </tr>
                    <tr>
                        <td>Profile :</td>
                        <td><?php echo $portfolioname["profile_name"] ?></td>
                    </tr>
                    <?php
                    $calprofilecost = 0;
                    $packlebels = explode(",", $packdetails["packlebels"]);
                    $packvalues = explode(",", $packdetails["packvalues"]);
                    for ($inx = 0; $inx < count($packlebels); $inx++) {
                        $expvalleb = explode("--", $packvalues[$inx]);
                        $calprofilecost = $calprofilecost + $expvalleb[1];
                        ?> 
                        <tr style="height: 30px;">
                            <td ><?php echo ucwords(str_replace("_", " ", $packlebels[$inx])) ?></td>
                            <td><?php echo ucwords(trim($expvalleb[0])) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <br/><br/>
                <h3>Signature:</h3>
                <br/>
                <h3><b>   X</b> ______________________________</h3>
                <br/>
                <p style="text-align: center;width: 200px">
                    By signing and fax this back to Round wrap Industries.you are confirming that all details of this Quote are accurate, and that you request to have this order produced as indicated here.
                </p>
            </td>
            <td style="width: 2%"></td>
            <td style="width: 83%;vertical-align: top">
                <table style="float: right;width: 100%" >
                    <col style="width: 10%">
                    <col style="width: 15%">
                    <col style="width: 12%">
                    <col style="width: 12%">
                    <col style="width: 12%">
                    <col style="width: 12%">
                    <col style="width: 12%">
                    <thead>
                        <tr style="text-align: left;height: 35px;text-align: center" class="item-row">
                            <th style="border: solid .2px;">Pcs</th>
                            <th style="border: solid .2px;">Type</th>
                            <th style="border: solid .2px;">(W) MM</th>
                            <th style="border: solid .2px;">(H) MM</th>
                            <th style="border: solid .2px;">(W) INCH</th>
                            <th style="border: solid .2px;">(H) INCH</th>
                            <th style="border: solid .2px;">SQ-FT</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $index = 0;
                        $totalps = 0;
                        $totalsqft = 0;
                        foreach ($packitemdetails as $key => $value) {
                            ?>
                            <tr style="text-align: left;height: 60px;text-align: center" class="item-row">
                                <td style="border:  solid .2px;">&nbsp;<?php echo $value["pcs"]; ?></td>
                                <td style="border: solid .2px;"><p style="text-align: justify;line-height: 20px;"> <?php echo $value["type"]; ?> </p></td>
                                <td style="text-align:center;border:solid .2px;">&nbsp;<?php echo round($value["mm_w"], 1); ?></td>
                                <td style="text-align:center;border: solid .2px;">&nbsp;<?php echo round($value["mm_h"], 1); ?> </td>
                                <td style="text-align:right;border:solid .2px;"><?php echo $value["fract_w"]; ?> </td>
                                <td style="text-align:right;border:solid .2px;"><?php echo $value["fract_h"]; ?> </td>
                                <td style="text-align: right;border: solid .2px"><?php echo round($value["sqFeet"], 2); ?>&nbsp;</td>
                            </tr>
                            <?php
                            $totalps = $totalps + $value["pcs"];
                            $totalsqft = $totalsqft + $value["sqFeet"];
                        }
                        ?>
                        <tr style="height: 35px" class="item-row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Total Pieces:</td>
                            <td style="border: solid .2px;text-align: right" ><?php echo $totalps; ?></td>
                        </tr>
                        <tr style="height: 35px" class="item-row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Billable Ft2:</td>
                            <td style="border: solid .2px;text-align: right" ><?php echo $packdetails["billable_fitsquare"]; ?></td>
                        </tr>
                        <tr style="height: 35px" class="item-row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Profile Cost:</td>
                            <td style="border: solid .2px;text-align: right" ><?php echo $profilecost = $portfolioname["baseprice"] + $calprofilecost ?></td>
                        </tr>
<!--                        <tr style="height: 35px" class="item-row">
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Cut/Tape Charge:</td>
                            <td style="border: solid .2px;text-align: right" ><?php echo $packdetails["cut_tape_charge"]; ?></td>
                        </tr>-->

<!--                        <tr style="height: 35px" class="item-row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Cut/Tape Cost:</td>
                            <td style="border: solid .2px;text-align: right" ><?php echo $packdetails["cut_tape_cost"]; ?></td>
                        </tr>-->
                        <tr style="height: 35px" class="item-row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Total Cost:</td>
                            <td style="border: solid .2px;text-align: right" ><?php echo $subtotalcost = round(($profilecost * $totalsqft), 2); ?></td>
                        </tr>
                        <tr style="height: 35px" class="item-row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Tax :<?php echo $taxinformationtdetails["tax"] . " " . $taxinformationtdetails["taxvalues"]; ?> </td>
                            <td style="border: solid .2px;text-align: right" >
                                <?php echo $subtotalcost * ($taxinformationtdetails["taxvalues"] / 100); ?>
                            </td>
                        </tr>
                        <tr style="height: 35px" class="item-row">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="border-right: solid .2px;"></td>
                            <td style="border: solid .2px;" colspan="2">Net Total:</td>
                            <td style="border: solid .2px;text-align: right" >
                                <?php
                                echo round((($subtotalcost * ($taxinformationtdetails["taxvalues"] / 100) ) + $subtotalcost), 2);
                                ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>

    <end_last_page end_height="40mm">
        <div>
            <table >
                <tr>
                    <td style="width: 100%;border: solid .2px;line-height: 20px;font-size: 8px;">
                        <p style="text-align: justify;">Above Pricing does not include taxes.Delivery/Freight or off cut charges </p>
                        <b>Proceed with order? [ ]Yes [ ]No</b>
                        <br/>
                        <p style="text-align: justify;">
                            We are sending this quote to you as requested. Please check it in detail, and contact us with any questions you may have
                        </p>
                        <p style="text-align: center;">
                            <b>Fax Back To:&nbsp; +1 604-278-1002</b>
                        </p>
                        <p style="text-align: justify;">
                            PLEASE NOTE- We will continue to process your order, once a complete and signed confirmation is returned to us.
                        </p>
                        <p style="text-align: center;">
                            <b>We hope to hear from you!</b>
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>