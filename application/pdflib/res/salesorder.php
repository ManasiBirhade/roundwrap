<?php
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
//$pdfheader = "RoundWrap Industries 1680 Savage Rd, Richmond, BC V6V 3A9, Canada <br/><b>Phone:</b> +1 604-278-1002";
$pdftitle = "SALES ORDER";

$salesorder = MysqlConnection::getSalesOrderDetailsById($salesorderid);
$itemsinfo = MysqlConnection::getSalesItemsDetailsById($salesorderid);

$promo = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'promotionnote' ");

$customerid = $salesorder["customer_id"];
$customer = MysqlConnection::getCustomerDetails($customerid);

$busno = $customer["businessno"];
$paymentterm = $customer["paymentterm"];
$arrpayterm = MysqlConnection::getGenericById($paymentterm);


$sonumber = $salesorder["sono"];
$ponumber = $salesorder["pono"];
$enterbyid = $salesorder["added_by"];
$enterby = MysqlConnection::getAddedBy($enterbyid);
$salesorder["soorderdate"] = $salesorder["soorderdate"];
$date = $salesorder["soorderdate"];
$billtoaddress = $salesorder["billTo_address"];
$shiptoaddress = $salesorder["shipping_address"];
$salesorder["expected_date"] = $salesorder["expected_date"];
$shipdate = $salesorder["expected_date"];
$shipvia = $salesorder["shipvia"];
$shiptype = $salesorder["radios"];
$rep = $salesorder["representative"];
?>
<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {    padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0;  }
    #items th { background: #eee; }
    #items1 { clear: both; width: 100%; margin: 30px 0 0 0; }
    /*    #items1 th { background: #eee; }*/
    /*    table{border: solid px;}*/
    table td, table th {  padding: 5px; }

    end_last_page div
    {
        height: 27mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->

    #background{
        position:absolute;
        z-index:0;
        display:block;
        min-height:20%; 
        min-width:20%;
        color:yellow;
    }

    #content{
        position:absolute;
        z-index:1;
    }

    #bg-text
    {
        color:red;
        font-size:40px;
        transform:rotate(300deg);
        -webkit-transform:rotate(300deg);
    }
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer>
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>SO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $salesorder["sono"] ?></b></td>
                    </tr>

                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>Date:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $date ?></b></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table id="items1" style="width: 100%;height: 200px;">
        <tr style="vertical-align: top;border: solid 0px;" >
            <th style="width: 40%;line-height: 15px;border: solid .2px;">

                <b>Bill To</b><br/><?php echo $billtoaddress ?> <br/>                     
                <b>Mob No :</b> <?php echo $customer["phno"] ?> <br/>                      
                <b>Fax No :</b> <?php echo $customer["cust_fax"] ?>                       
            </th>
            <th style="width: 20%;border: solid 0px ;border-right:solid .2px; ">
                <?php if ($salesorder["isOpen"] == "N") { ?>
            <div id="background">
                <p id="bg-text">CLOSED</p>
            </div>
        <?php } ?>
        </th>
        <th style="width: 40%;line-height: 15px;text-align: left;border: solid .2px;">
            <b>Ship To</b><br/><?php echo $shiptoaddress ?>
        </th>
        </tr>
    </table>
    <br/>
    <table id="items" style="margin-top: 0px;text-align: center" border='0'>
        <tr  style="border: solid 0px;">
            <th style="width: 15%;border: solid .2px;">PO NO</th>
            <th style="width: 15%;border: solid .2px;">Rep</th>
            <th style="width: 15%;border: solid .2px;">Term</th>
            <th style="width: 15%;border: solid .2px;">SHIP.DATE</th>
            <th style="width: 15%;border: solid .2px;">SHIP.VIA</th>
            <th style="width: 15%;border: solid .2px;">SHIP.TYPE</th>

        </tr>
        <tr style="height: 30px;border: solid 1px;">
            <td style="border: solid .2px;"><?php echo $ponumber ?></td>
            <td style="border: solid .2px;"><?php echo $rep ?></td>
            <td style="border: solid .2px;"><?php echo $arrpayterm["code"] . " " . $arrpayterm["name"] ?></td>
            <td style="border: solid .2px;"><?php echo $shipdate ?></td>
            <td style="border: solid .2px;"><?php echo $shipvia ?></td>
            <td style="border: solid .2px;"><?php echo $shiptype ?></td>

        </tr>
    </table>

    <table style="border: solid;height: 100%;min-height: 900px;height: auto" id="items">
        <col style="width: 20%">
        <col style="width: 55%">
        <col style="width: 15%">
        <col style="width: 10%">
        <thead>
            <tr style="text-align: left;height: 30px;text-align: center" >
                <th style="border: solid .2px;">Item</th>
                <th style="border: solid .2px;">Description</th>
                <th style="border: solid .2px;">Quantity</th>
                <th style="border: solid .2px;">Unit</th>
            </tr>
        </thead>
        <?php
        $k = 1;
        foreach ($itemsinfo as $key => $value) {
            $items = MysqlConnection::getItemDataById($value["item_id"]);
            if ($k % 2 == 0) {
                $back = "rgb(240,240,240)";
            } else {
                $back = "white";
            }
            ?>
            <tr>
                <td style="border:  solid .2px;">&nbsp;<?php echo $items["item_code"] ?></td>
                <td style="border: solid .2px;"><p style="text-align: justify;line-height: 20px;"><?php echo $items["item_desc_sales"] ?></p></td>
                <td style="text-align:center;border:  solid .2px">&nbsp;<?php echo $value["qty"] - $value["returnQty"] ?></td>
                <td style="text-align:center;border:  solid .2px">&nbsp;<?php echo $items["unit"] ?></td>
            </tr>
            <?php
            $k++;
        }
        ?>
		<?php  if($busno !=""){ ?>
		<tr>
            <td colspan="4">&nbsp;Bussiness no.: <?php echo $busno?></td>
        </tr>
		<?php } ?>
    </table>

    <end_last_page end_height="60mm">

        <div>
            <table style="border: solid;height: 100%;min-height: 900px;height: auto" id="items">
                <tr  style="border: solid 0px;">
                    <th style="width: 50%;border: solid .2px;">Remark/ Note</th>
                    <th style="width: 50%;border: solid .2px;"></th>

                </tr>
                <tr  style="border: solid 0px;vertical-align: top">
                    <td style="width: 50%;height: 5%;border: solid .2px;"><p><b><i style="font-size: 18px"><?php echo $salesorder["remark"] ?></i></b></p></td>
                    <td style="width: 50%;height: 5%;border: solid .2px;"><p><b><?php echo $promo[0]["description"] ?></b></p></td>

                </tr>
            </table>
            <br/>
            <table >
                <tr>
                    <td style="width: 100%;border: solid .2px;line-height: 20px;font-size: 8px;">
                        <p>Terms: A Service charge will be charged to you on any balance outstanding at the end of each month at the rate of 1.5% per month calculated and compounded monthly(19.57% per annum)</p>
                        <p>
                            All manufacturer's names,number,symbols and descriptions are used for reference purposes only and it is not implied hat any part listed is the product of these manufacturers.
                            All goods listed above are the property of  until fully paid for.
                        </p>
                        <h2>Thank You for Your Continued patronage!</h2>

                    </td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>