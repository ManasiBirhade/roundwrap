<?php
error_reporting(0);
$imgurl = "http://35.183.37.135/application/assets/images/download.png";
if ($_SERVER["HTTP_HOST"] == "localhost") {
    $barcodeurl = "http://localhost/roundwrap/trunk/application/barcode/barcodegenerate.php";
} else {
    $barcodeurl = "http://35.183.37.135/application/barcode/barcodegenerate.php";
}

$compaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'companyaddress' ");
$pdfheader = $compaddress[0][description];
$pdftitle = "WORK ORDER";
$pack = MysqlConnection::getPackSlipFromId($packslipid);
$packitemdetails = MysqlConnection::getPackSlipDetailsFromId($packslipid);
$customer = MysqlConnection::getCustomerDetails($pack["cust_id"]);
$portfolioname = MysqlConnection::getPortfolioProfileById($pack["prof_id"]);

$genericentry = MysqlConnection::fetchCustom("SELECT code FROM generic_entry WHERE type = 'printvalues' ");
$genericevalue = $genericentry[0]["code"];
$expload = explode(",", $genericevalue);
?>

<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 15px 0 0 0; border: 0px solid black; }
    #items th { background: #eee; }
    table{border: solid 0px;}
    table td, table th { border: 0px; padding: 5px; }

    end_last_page div
    {
        height: 1mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <page_footer>
        <table style="width: 100%; border: solid 1px black;">
            <tr>
                <td style="text-align: left; width: 50%">Roundwrap</td>
                <td style="text-align: right; width: 50%">Page [[page_cu]]/[[page_nb]]</td>
            </tr>
        </table>
    </page_footer> 
    <table style="width: 100%; border: solid 1px black;">
        <tr style="vertical-align: top">
            <td style="width: 50%;text-align: left;border: solid 0px;" >
                <img src="<?php echo $imgurl ?>" style="width: 180px;height: 40px;">
                <br/><br/>
                <p style="line-height: 18px;"><?php echo $pdfheader ?></p>
                <div style="float: left">REMAKE</div>
                <div style="float: left;margin-top: 2px">
                    <img style="height: 45px;width: 200px;border: solid 0px" 
                         src="<?php echo $barcodeurl ?>?text=<?php echo $pack["inCode"] ?>" alt="<?php echo $pack["inCode"] ?>" />
                </div>
            </td>
            <td style="width: 25%;text-align: left;border: solid 0px;" ></td>
            <td style="width: 25%;text-align: right;border: solid 0px;line-height: 20px;" >
                <table  style="border: solid 1px;margin-top: 2px;float: right" >
                    <tr style="height: 35px;border:solid .2px ">
                        <td style="border: solid 0px ;font-size: 14pt" colspan="2"><?php echo $pdftitle ?></td>
                    </tr>
                    <tr style="height: 25px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>SO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $pack["so_no"] ?></b></td>
                    </tr>
                    <tr style="height: 25px;border:solid .2px ">
                        <td style="border: solid 0px ;"><b>PO NO:&nbsp;&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $pack["po_no"] ?></b></td>
                    </tr>
                    <tr style="height: 25px;">
                        <td style="border: solid 0px ;"><b>WorkOr Id:&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b><?php echo $pack["workOrd_Id"] ?></b></td>
                    </tr>
                    <tr style="height: 25px;">
                        <td style="border: solid 0px ;"><b style="font-size: 16px">Seq No:&nbsp;</b></td>
                        <td style="border: solid 0px ;"><b style="font-size: 16px"><?php echo $pack["seq_no"] ?></b></td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
    <hr style="border: solid .2px;"/>
    <table id="items" style="width: 100%;border: solid 1px;"  >
        <tr  >
            <td style="width: 10%">Account</td>
            <td style="width: 12%;text-align: center">&nbsp;:</td>
            <td style="width: 40%">
                <?php
                if (in_array("account", $expload)) {
                    echo $customer["cust_companyname"];
                } else {
                    echo "-";
                }
                ?>
            </td>
            <td style="width: 10%">Term</td>
            <td style="width: 12%;text-align: center">&nbsp;:</td>
            <td>
                <?php
                $resultset = MysqlConnection::fetchCustom("SELECT name FROM generic_entry WHERE id = " . $customer["paymentterm"]);
                if (in_array("term", $expload)) {
                    echo $resultset[0]["name"];
                } else {
                    echo "-";
                }
                ?>
            </td>
        </tr>
        <tr style="text-align: left;">
            <td >Pack Date</td>
            <td style="text-align: center">&nbsp;:</td>
            <td ><?php echo $pack["rec_date"] ?></td>
            <td >Required Date</td>
            <td style="text-align: center">&nbsp;:</td>
            <td ><?php echo $pack["req_date"] ?></td>
        </tr>
        <tr style="text-align: left;vertical-align: top;text-align: justify;">
            <td >Address</td>
            <td style="text-align: center">&nbsp;:</td>
            <td style="line-height: 15px"><?php
                if (in_array("address", $expload)) {
                    echo MysqlConnection::formatToBRAddress($customer["billto"]);
                    echo $customer["phno"];
                } else {
                    echo "-";
                }
                ?>
            </td>
            <td >Received  Date</td>
            <td style="text-align: center">&nbsp;:</td>
            <td ><?php echo $pack["rec_date"] ?></td>
        </tr>
        <tr>
            <td>Ship Via</td>
            <td style="text-align: center">&nbsp;:</td>
            <td><?php echo $pack["shipvia"] ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <br/>
    <hr style="border: solid .2px;"/>
    <table style="width: 100%">
        <tr style="vertical-align: top">
            <td style="width: 20%;vertical-align: top">
                <table style="font-size: 10px;">
                    <tr style="font-size: 10px;">
                        <td style="font-size: 10px;">Portfolio:</td>
                        <td style="font-size: 10px;"><?php echo $portfolioname["portfolio_name"] ?></td>
                    </tr>
                    <tr>
                        <td>Profile:</td>
                        <td><?php echo $portfolioname["profile_name"] ?></td>
                    </tr>
                    <?php
                    $packlebels = explode(",", $pack["packlebels"]);
                    $packvalues = explode(",", $pack["packvalues"]);
                    for ($inx = 0; $inx < count($packlebels); $inx++) {
                        $expvalleb = explode("--", $packvalues[$inx]);
                        ?> 
                        <tr style="height: 30px">
                            <td style="width: 50%;"><?php echo ucwords(str_replace("_", " ", $packlebels[$inx])) ?></td>
                            <td><?php echo ucwords(trim($expvalleb[0])) ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>

            </td>

            <td style="width: 78%;vertical-align: top">
                <table style="width: 100%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <col style="width: 11%">
                    <thead>
                        <tr>
                            <td style="border: solid .2px;text-align: center" colspan="2"><b>Size as Ordered</b></td>
                            <td style="border: solid .2px;text-align: center" colspan="2"></td>
                            <td style="border: solid .2px;text-align: center" colspan="2"><b>Finished Size(mm)</b></td>
                            <td style="border: solid .2px;text-align: center" colspan="2"><b>Cut Size(mm)</b></td>
                            <td style="border: solid .2px;text-align: center"></td>
                        </tr>
                        <tr style="text-align: left;height: 30px;text-align: center">
                            <td style="border: solid .2px;">R-to-R</td>
                            <td style="border: solid .2px;">Height</td>
                            <td style="border: solid .2px;">Pcs</td>
                            <td style="border: solid .2px;">Type</td>
                            <td style="border: solid .2px;">R-to-R</td>
                            <td style="border: solid .2px;">Height</td>
                            <td style="border: solid .2px;">R-to-R</td>
                            <td style="border: solid .2px;">Height</td>
                            <td style="border: solid .2px;">SQ-FT</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($packitemdetails as $key => $value) {
                            ?>
                            <tr style="text-align: left;height: 30px;text-align: center">
                                <td style="border: solid .2px;"><?php echo $value["mm_w"] ?></td>
                                <td style="border: solid .2px;"><?php echo $value["mm_h"] ?></td>
                                <td style="border: solid .2px;"><?php echo $value["pcs"] ?></td>
                                <td style="border: solid .2px;"><?php echo $value["type"] ?></td>
                                <td style="border: solid .2px;"><?php echo $value["mm_w"] ?></td>
                                <td style="border: solid .2px;"><?php echo $value["mm_h"] ?></td>
                                <td style="border: solid .2px;"><?php echo $value["mm_w"] - 36 ?></td>
                                <td style="border: solid .2px;"><?php echo $value["mm_h"] - 1 ?></td>
                                <td style="border: solid .2px;"><?php echo round($value["sqFeet"], 2) ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    <br/>
    <table  style="width: 100%">
        <tr >
            <td style="border: solid .2px;text-align: center"><b>Production Notes </b>
            </td>
        </tr>
        <tr >
            <td style="width: 100%;height: 40px;border: solid .2px">
                <p style="text-align: justify"><i style="font-size: 18px">
                        &nbsp; <?php echo $pack["packingnote"] ?> </i>
                </p>
            </td>
        </tr>
    </table>
    <br/>
    <table style="width: 100%">
        <tr style="border: solid .2px;">
            <td style="border: solid .2px;text-align: center" colspan="6"><b>Office Use Only</b></td></tr>
        <tr style="border: solid .2px;"></tr>
        <tr style="border: solid .2px;">
            <td style="border: solid .2px;width: 15%">Wrapped by:</td>
            <td style="border: solid .2px;width: 18%"></td>
            <td style="border: solid .2px;width: 15%">Date:</td>
            <td style="border: solid .2px;width: 18%"></td>
            <td style="border: solid .2px;width: 15%">Total Pcs:</td>
            <td style="border: solid .2px;width: 19%"></td>
        </tr>
        <tr>
            <td style="border: solid .2px;">Supplier Stock :</td>
            <td style="border: solid .2px;"></td>
            <td style="border: solid .2px;">Laminate Ord Date: </td>
            <td style="border: solid .2px;"></td>
            <td style="border: solid .2px;">Laminate FTA:</td>
            <td style="border: solid .2px;"></td>
        </tr>
    </table> 


    <end_last_page end_height="30mm">
        <div style="clear: both">


            <table style="width: 56%">
                <tr>
                    <td><div style="float: left">START</div></td>
                    <td><div style="float: right">FINISH</div></td>
                </tr>
                <tr>
                    <td><div style="float: left">
                            <img style="height: 70px;width: 250px;border: solid 0px" 
                                 src="<?php echo $barcodeurl ?>?text=<?php echo $pack["inCode"] ?>" alt="<?php echo $pack["inCode"] ?>" />
                        </div>
                    </td>
                    <td>
                        <div style="float: right">
                            <img style="height: 70px;width: 250px;border: solid 0px" src="<?php echo $barcodeurl ?>?text=<?php echo $pack["outCode"] ?>" alt="<?php echo $pack["outCode"] ?>" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </end_last_page>
</page>