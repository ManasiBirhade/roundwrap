<?php

error_reporting(0);
session_start();
ob_start();

require_once dirname(__FILE__) . '/vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

try {
    ob_start();
    include 'res/workorder.php';
    $content = ob_get_clean();
    $html2pdf = new Html2Pdf('P', 'A4', 'en');
    $html2pdf->setTestIsImage(false);
    $html2pdf->writeHTML($content);
    echo $html2pdf->output(MysqlConnection::$SERVERPATH."workorder\\$packslipid.pdf", 'F');
} catch (Html2PdfException $e) {
    $html2pdf->clean();
    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}
