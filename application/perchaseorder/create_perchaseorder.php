<style>
    table tbody {

    }
    table tr td{
        padding: 5px;
    }
</style>
<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">
<?php
if (MysqlConnection::validateItems()) {
    header("location:index.php?pagename=success_itemmaster&flag=noitems");
}

$flag = filter_input(INPUT_GET, "flag");
$purchaseorderid = filter_input(INPUT_GET, "purchaseorderid");
$arrsalutations = MysqlConnection::fetchCustom("SELECT distinct(`salutation`) as salutation FROM `supplier_master` WHERE salutation!=''");
$supparray = MysqlConnection::fetchCustom("SELECT supp_id,companyname FROM `supplier_master` ORDER BY companyname");

$poaddress = MysqlConnection::fetchCustom("SELECT description FROM `generic_entry` WHERE type = 'poaddress' ");


if (isset($purchaseorderid)) {
    $purchaseorderdata = MysqlConnection::getPurchaseOrderData($purchaseorderid);
    $purchaseitemsdata = MysqlConnection::getPurchaseItemData($purchaseorderid);

    $ponumber = $purchaseorderdata["purchaseOrderId"];
    $isOpen = $purchaseorderdata["isOpen"];
    if ($isOpen == "N") {
        header("location:index.php?pagename=manage_perchaseorder&status=Y&action=poclosed");
    }
} else {
    $ponumber = MysqlConnection::generateNumber("prodpo");
}


if ($flag == "purchase") {
    $itemid = filter_input(INPUT_GET, "itemId");
    $reqQty = filter_input(INPUT_GET, "reqQty");

    $cust = filter_input(INPUT_GET, "cust");
    $so = filter_input(INPUT_GET, "so");


    $vendorinfo = MysqlConnection::getItemDataById($itemid);
    $supplierid = $vendorinfo["vendorid"];

    $bestvendorresult = getBestVendorResult();
} else {
    $supplierid = filter_input(INPUT_GET, "supplierid");
}
$supplier = MysqlConnection::getSupplierDetails($supplierid);

$suppliergoodstax = MysqlConnection::getTaxInfoById($supplier["taxInformation"]);

$suppliershippingtax = MysqlConnection::getTaxInfoById($supplier["shippingtaxinfo"]);

$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY id DESC ;");

$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");

saveOrUpdatePurchaseOrder();

$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit)) {
    saveVendor(filter_input_array(INPUT_POST));
}

function saveVendor($filterarray) {

    unset($filterarray["btnSubmit"]);
    if ($filterarray["salutation"] != null && salutation !== '') {
        unset($filterarray["salutation1"]);
    } else {
        $filterarray["salutation"] = $filterarray["salutation1"];
        unset($filterarray["salutation1"]);
    }
    $customerid = MysqlConnection::insert("supplier_master", $filterarray);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>

<script>



    $(function() {
        for (var index = 1; index <= 30; index++) {
            $("#tags" + index).autocomplete({
                source: "itemmaster/autoitemajax.php",
            });
        }
    });

    $(document).ready(function($) {
        $("#supp_phoneNo").mask("(999) 999-9999");
        $("#cust_fax").mask("(999) 999-9999");
    });

</script>
<script src="js/script.js"></script>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });

</script>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 0px;margin: 0px;}
</style>
<form name="purchaseorder" method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a  data-toggle="tab" href="#tab1"> MANAGE PURCHASE ORDER</a></li>
                </ul>
            </div>
            <br/>
            <table  style="width: 100%">
                <tr>
                    <td>
                        <table  class="display nowrap sortable" style="width: 100%">
                            <tr style=" color: red">
                                <td >VENDOR NAME<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <?php
                                    if ($purchaseorderid == "") {
                                        ?>
                                        <span  data-toggle="tooltip" data-original-title="Please Select Vendor in the list">
                                            <select style="width: 225px" name="suppid" id="suppid" required="">
                                                <option value="">SELECT</option>
                                                <option value="1"><< ADD NEW >> </option>
                                                <?php
                                                foreach ($supparray as $key => $value) {
                                                    if (isset($supplier)) {
                                                        $selected = $supplier["supp_id"] == $value["supp_id"] ? "selected" : "";
                                                    } else {
                                                        $selected = $purchaseorderdata["supplier_id"] == $value["supp_id"] ? "selected" : "";
                                                    }
                                                    ?>
                                                    <option <?php echo $selected ?> value="<?php echo $value["supp_id"] ?>"><?php echo $value["companyname"] ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </span>
                                        <?php
                                    } else {
                                        $suppliername = "";
                                        foreach ($supparray as $supplier) {
                                            if ($supplier["supp_id"] == $purchaseorderdata["supplier_id"]) {
                                                $suppliername = $supplier["companyname"];
                                            }
                                        }
                                        ?>
                                        <input style="width: 220px" type="text" value="<?php echo $suppliername ?>" readonly="">
                                        <?php
                                    }
                                    ?>
                                </td>
<!--                                <td>PO NUMBER</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><b><input style="color: red; width: 220px" type="text" name="purchaseOrderId" onkeypress="return chkNumericKey(event)" value="<?php echo $ponumber ?>" readonly=""></b></td>-->

                                <td>ORDER DATE</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><b><input style="width: 220px" type="text" name="purchasedate" value="<?php echo MysqlConnection::convertToPreferenceDate(filter_input(INPUT_COOKIE, "dateme")) ?>" readonly=""></b></td>
                                <td>SALES PERSON</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><b> <input style="width: 220px" type="text" name="enterby" value="<?php echo $_SESSION["user"]["firstName"] . " " . $_SESSION["user"]["lastName"] ?>" readonly=""></b></td>
                            </tr>
                            <tr>
                                <td>SO NUMBER</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><b><input style=" width: 220px"  type="text"  value="<?php echo $purchaseorderdata["c_sono"] == "" ? $so : $purchaseorderdata["c_sono"] ?>"  name="c_sono"></b></td>
                                <td style="width: 10%">SHIP VIA<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <span  data-toggle="tooltip" data-original-title="Please select delivery service from the list">
                                        <select style="width: 225px" name="ship_via" id="ship_via" required="true"> 
                                            <option value="">&nbsp;&nbsp;</option>
                                            <option value="0" ><< ADD NEW >></option>
                                            <?php foreach ($shipviainfo as $key => $value) { ?>
                                                <option <?php echo $value["name"] == $purchaseorderdata["ship_via"] ? "selected" : "" ?>
                                                    value="<?php echo $value["name"] ?>"  ><?php echo $value["name"] ?></option>
                                                <?php } ?>
                                        </select>
                                    </span>
                                </td>
                                <td style="width: 10%">EXPECTED&nbsp;DELIVERY<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><input style="width: 220px" type="text" required="true" name="expected_date" id="datepicker" value="<?php echo MysqlConnection::convertToPreferenceDate($purchaseorderdata["expected_date"]) ?>"></td>
                            </tr>
                            <tr >

                                <td>CUSTOMER NAME</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><b><input style="width: 220px" type="text" name="c_name" value="<?php echo $purchaseorderdata["c_name"] == "" ? $cust : $purchaseorderdata["c_name"] ?>" ></b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td >BILLING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea readonly="" style="line-height: 20px;width: 240px;height: 75px;resize: none" name="billing" ><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $poaddress[0]["description"])) ?></textarea></td>
                                <td>SHIPPING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea required="" style="line-height: 20px;width: 240px;height: 75px;resize: none" name="shipping" ><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $poaddress[0]["description"])) ?></textarea></td>
                                <td >REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea  style="width: 220px;line-height: 20px; color: red;height: 75px;resize: none" name="remark" ><?php echo $purchaseorderdata["remark"] ?></textarea></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 70%;float: left">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(250,250,250)">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 440px;">ITEM CODE / DESCRIPTION</td>
                                    <td style="width: 80px;">UNIT</td>
                                    <td style="width: 80px;">PRICE</td>
                                    <td style="width: 80px;">QTY</td>
                                    <td>AMOUNT</td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    if (count($vendorinfo) == 0) {
                                        $preindex = 0;
                                        foreach ($purchaseitemsdata as $value) {
                                            $preindex++;
                                            $itemdetails = MysqlConnection::getItemDataById($value["item_id"]);
                                            ?>
                                            <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                                <td style="width: 25px"><a class="icon  icon-remove" onclick="deleteRow('<?php echo $preindex ?>')"></a></td>
                                                <td style="width: 440px;">
                                                    <input type="text" name="items[]" id="tags<?php echo $preindex ?>" onfocusout="setDetails('<?php echo $preindex ?>')" value="<?php echo $itemdetails["item_code"] ?> __ <?php echo $itemdetails["item_desc_purch"] ?>" style="padding: 0px;margin: 0px;width: 100%">
                                                </td>
                                                <td style="width: 80px;">
                                                    <div id="unit<?php echo $preindex ?>"><?php echo $itemdetails["unit"] ?></div>
                                                </td>
                                                <td style="width: 80px;">
                                                    <input type="text"  onkeypress="return chkNumericKey(event)"  name="price[]" id="price<?php echo $preindex ?>" value="<?php echo $value["price"] ?>" />
                                                </td>
                                                <td style="width: 80px;">
                                                    <input type="text"   name="itemcount[]" maxlength="5" onkeypress="return chkNumericKey(event)" onfocusout="calculateAmount()" value="<?php echo $reqQty == "" ? $value["qty"] : $reqQty ?>" id="amount<?php echo $preindex ?>" style="padding: 0px;margin: 0px;width: 100%">
                                                </td>
                                                <td >
                                                    <div id="total<?php echo $preindex ?>"><?php echo $value["qty"] * $value["price"] ?></div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        for ($index = 1 + $preindex; $index <= 25; $index++) {
                                            if (count($purchaseitemsdata) == 0) {
                                                if ($index == 1) {
                                                    $required = "required";
                                                } else {
                                                    $required = "";
                                                }
                                            }
                                            ?>
                                            <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                                <td style="width: 25px">
                                                    <a class="icon  icon-remove" onclick="deleteRow('<?php echo $index ?>')"></a>
                                                </td>
                                                <td style="width: 440px;">
                                                    <input type="text" name="items[]" <?php echo $required ?> id="tags<?php echo $index ?>" onfocusout="setDetails('<?php echo $index ?>')"  style="padding: 0px;margin: 0px;width: 100%">
                                                </td>
                                                <td style="width: 80px;"><div id="unit<?php echo $index ?>"></div></td>
                                                <td style="width: 80px;"><input type="text"  onkeypress="return chkNumericKey(event)"  name="price[]" id="price<?php echo $index ?>" value="<?php echo $value["total_purch_rate"] ?>" /></td>
                                                <td style="width: 80px;"><input type="text"   name="itemcount[]" maxlength="5" onkeypress="return chkNumericKey(event)" onfocusout="calculateAmount()" id="amount<?php echo $index ?>" style="padding: 0px;margin: 0px;width: 100%"></td>
                                                <td ><div id="total<?php echo $index ?>"></div></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        $preindex = 0;
                                        ?>
                                        <tr id="1" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px"><a class="icon  icon-remove" onclick="deleteRow('1')"></a></td>
                                            <td style="width: 440px;">
                                                <input type="text" name="items[]" readonly="" value="<?php echo $vendorinfo["item_code"] . " __ " . $vendorinfo["item_desc_sales"] ?>" style="padding: 0px;margin: 0px;width: 100%">
                                            </td>
                                            <td style="width: 80px;"><div id="unit1"><?php echo $vendorinfo["unit"] ?></div></td>
                                            <td style="width: 80px;"><input type="text" onkeypress="return chkNumericKey(event)"  name="price[]" id="price1" value="<?php echo $vendorinfo["total_purch_rate"] ?>" /></td>
                                            <td style="width: 80px;"><input type="text" name="itemcount[]" value="<?php echo $reqQty ?>" onkeypress="return chkNumericKey(event)" onfocusout="calculateAmount()" id="amount1" style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><div id="total1"></div></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <div style="width: 28%;float: right">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <tr >
                                    <td style="width: 150px;"><b>Total Amount</b></td>
                                    <td colspan="2"><input  type="text" id="finaltotal" required="" value="<?php echo round($purchaseorderdata["sub_total"], 2) ?>"  onkeypress="return chkNumericKey(event)" name="finaltotal" ></td>
                                </tr>
                                <tr >
                                    <td  style="width: 100px;"><b>Discount %</b></td>
                                    <td>
                                        <input  style="width: 98%" type="text" id="discount"  value="<?php echo $purchaseorderdata["discount"] ?>" name="discount" autocomplete="off" onkeyup="calculateAmount()"  maxlength="5" name="discount" >

                                    </td>
                                    <td>
                                        <input  style="width: 78%" type="text" id="discountvalue"  value="<?php echo $purchaseorderdata["discountvalue"] ?>" name="discountvalue" readonly="">
                                    </td>
                                </tr>

                                <tr >
                                    <td><b>Goods Tax</b></td>
                                    <td><select name="taxid" id="taxid" required="" onchange="calculateAmount()" style="width: 100%">
                                            <option value="">SELECT</option>
                                            <?php
                                            foreach ($sqltaxinfodata as $key => $value) {
                                                if ($value["id"] == $purchaseorderdata["taxid"]) {
                                                    $selected = "selected";
                                                } else {
                                                    $selected = "";
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>">
                                                    <?php echo ucwords($value["taxtype"]) ?> 
                                                    <?php echo $value["taxname"] . "-" . $value["taxvalues"] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    <td>
                                        <input type="text" id="taxamount" readonly="" value="<?php echo $purchaseorderdata["taxamount"] ?>" name='taxamount' style="width: 78%" >
                                    </td>

                                    </td> 
                                </tr>
                                <tr >
                                    <td><b>Sub Total</b></td>
                                    <td colspan="2"><input  type="text" id="totalTax" required="" value="<?php echo round($purchaseorderdata["totalTax"], 2) ?>"   name="totalTax" readonly=""></td>
                                </tr>
                                <tr >
                                    <td><b>Shipping Charges</b></td>
                                    <td colspan="2"><input type="text" id="ship_charge" value="<?php echo $purchaseorderdata["ship_charge"] == "" ? "0" : $purchaseorderdata["ship_charge"] ?>"name="ship_charge" onfocusout="calculateAmount()"  maxlength="5" onkeypress="return chkNumericKey(event)"  ></td>
                                </tr>
                                <tr >
                                    <td><b>Shipping Tax</b></td>
                                    <td colspan="2">
                                        <select id="staxname" name='staxname' style="height: 25px;width: 57%" >
                                            <option value="">SELECT</option>
                                            <option value="GST-5" <?php echo $purchaseorderdata["staxname"] == "GST-5" ? "selected" : "" ?> >GST-5</option>
                                        </select>
                                        <input  type="text" style="width: 32%" id="staxpercent"  value="<?php echo $purchaseorderdata["staxpercent"] == "" ? ( $suppliergoodstax["taxvalues"]) : $purchaseorderdata["staxpercent"] ?>" readonly="" name="staxpercent" >
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Net Total</b></td>
                                    <td colspan="2"><input type="text" id="nettotal" required="" value="<?php echo $purchaseorderdata["total"] ?>"  readonly="" name="nettotal" name="nettotal" ></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>
                    <table>
                        <tr>
                            <td>
                                <input class="btn btn-info"  type="submit" name="submit" id="submit" value="SAVE"/>
                                <input class="btn btn-info"  type="submit" name="btnsavenext"  id="btnsavenext"  value="SAVE AND NEW"/>
                                <input class="btn btn-info"  type="submit" name="printButton"  id="printButton"  value="PRINT"/>
                                <input class="btn btn-info"  type="submit" name="mailButton"  id="mailButton"  value="EMAIL"/>
                                <a href="index.php?pagename=manage_perchaseorder" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div> 
</form>
<div id="custtypemodel" class="modal hide" style="top: 3%;left: 25%;width: 90%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD NEW VENDOR</h3>
    </div>
    <?php include 'customdialogs/vendordialog.php'; ?>
</div>
<!-- this is custom model dialog --->
<div id="shipviamodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD SHIP VIA</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="ship_via" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>SHIP VIA NAME</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="name" id="name" required="true"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipviabtn" data-dismiss="modal" class="btn btn-info">SAVE </a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->


<script src="js/maruti.form_common.js"></script>
<script src="perchaseorder/purchasejs.js"></script>
<?php

function buildauto($itemarray) {
    $option = "";
    foreach ($itemarray as $value) {
        $option .= "\"" . $value["item_code"] . " __ " . preg_replace('!\s+!', ' ', str_replace("\"", "", $value["item_desc_purch"])) . "\",";
    }
    return $option;
}

function buildVendorAutoComplete($vendorarray) {
    $option = "";
    foreach ($vendorarray as $value) {
        $option .= " \" " . $value["companyname"] . "\",";
    }
    return $option;
}

function getPostedData() {
    $purchase_order = array();
     
    $purchase_order["purchaseOrderId"] = MysqlConnection::getNextValue("PO");
//    $purchase_order["purchaseOrderId"] = filter_input(INPUT_POST, "purchaseOrderId");
    if (filter_input(INPUT_GET, "purchaseorderid") == "") {
        $purchase_order["supplier_id"] = filter_input(INPUT_POST, "suppid");
    }

    $shipping = filter_input(INPUT_POST, "shipping");
    $billing = filter_input(INPUT_POST, "billing");
    $purchase_order["shipping_address"] = MysqlConnection::formatToBRAddress($shipping);
    $purchase_order["billing_address"] = MysqlConnection::formatToBRAddress($billing);


    $purchase_order["remark"] = filter_input(INPUT_POST, "remark");
    $purchase_order["ship_via"] = filter_input(INPUT_POST, "ship_via");
    $purchase_order["expected_date"] = filter_input(INPUT_POST, "expected_date");
    $purchase_order["expected_date"] = MysqlConnection::convertToDBDate($purchase_order["expected_date"]);
    $purchase_order["purchasedate"] = filter_input(INPUT_POST, "purchasedate");
    $purchase_order["added_by"] = $_SESSION["user"]["user_id"];
    $purchase_order["sub_total"] = filter_input(INPUT_POST, "finaltotal");
    $purchase_order["ship_charge"] = filter_input(INPUT_POST, "ship_charge");
    $discount = filter_input(INPUT_POST, "discount");
    $purchase_order["discount"] = $discount == "" ? "0.00" : $discount;
    $purchase_order["discountvalue"] = filter_input(INPUT_POST, "discountvalue");
    $purchase_order["total"] = filter_input(INPUT_POST, "nettotal");

    $purchase_order["c_name"] = filter_input(INPUT_POST, "c_name");
    $purchase_order["c_sono"] = filter_input(INPUT_POST, "c_sono");



    $purchase_order["taxid"] = $taxid = filter_input(INPUT_POST, "taxid");
    $taxdetails = MysqlConnection::getTaxInfoById($taxid);
    $purchase_order["taxname"] = $taxdetails["taxname"];
    $purchase_order["taxpercent"] = $taxdetails["taxvalues"];

    $purchase_order["taxamount"] = filter_input(INPUT_POST, "taxamount");

    $purchase_order["staxname"] = filter_input(INPUT_POST, "staxname");
    $purchase_order["staxpercent"] = filter_input(INPUT_POST, "staxpercent");

    $purchase_order["totalTax"] = filter_input(INPUT_POST, "totalTax");

    return $purchase_order;
}

function savePurchaseItems($itemsarray, $poid) {
    $items = $itemsarray["items"];
    for ($index = 0; $index < count($items); $index++) {
        $itemname = explode("__", $itemsarray["items"][$index]);
        $itemcode = trim($itemname[0]);
        $itemcount = $itemsarray["itemcount"][$index];
        $purchaseprice = $itemsarray["price"][$index];
        if ($itemcode != "" && $itemcount != "") {
            $purchase_items["po_id"] = $poid;
            $arritem = MysqlConnection::fetchCustom("SELECT item_id  FROM  `item_master`  WHERE  `item_code` =  '$itemcode'");
            $purchase_items["item_id"] = $arritem[0]["item_id"];
            $purchase_items["qty"] = $itemcount;
            $purchase_items["rqty"] = 0;
            $purchase_items["price"] = $purchaseprice;
            MysqlConnection::insert("purchase_item", $purchase_items);
        }
    }
}

function saveOrUpdatePurchaseOrder() {
    $purchaseorderid = filter_input(INPUT_GET, "purchaseorderid");

    $posteddata = getPostedData();
//    echo '<pre>';
//    print_r($posteddata);
//    echo '</pre>';
    $submit = filter_input(INPUT_POST, "submit");
    $btnsavenext = filter_input(INPUT_POST, "btnsavenext");
    $printButton = filter_input(INPUT_POST, "printButton");
    $mailButton = filter_input(INPUT_POST, "mailButton");

    if (isset($submit) || isset($btnsavenext) || isset($printButton) || isset($mailButton)) {
        if (isset($purchaseorderid)) {
            $poid = filter_input(INPUT_GET, "purchaseorderid");
            MysqlConnection::edit("purchase_order", $posteddata, " id = '$purchaseorderid' ");
            MysqlConnection::delete("DELETE FROM purchase_item WHERE  po_id = '$purchaseorderid'  ");
        } else {
            $poid = MysqlConnection::insert("purchase_order", $posteddata);
        }
        savePurchaseItems(filter_input_array(INPUT_POST), $poid);
        generatePDF($poid);
    }
    if (isset($submit)) {
          header("location:index.php?pagename=manage_perchaseorder&action=update");
    } else if (isset($btnsavenext)) {
        header("location:index.php?pagename=create_perchaseorder");
    } else if (isset($printButton)) {
        header("location:index.php?pagename=manage_perchaseorder&action=print&printid=" . $poid);
    } else if (isset($mailButton)) {
        header("location:index.php?pagename=email_perchaseorder&purchaseorderid=" . $poid);
    }
}

function generatePDF($poid) {
    include 'pdflib/purchaseorder.php';
}
?>
<script>

                                        $(document).ready(function() {
                                            $('#suppid').select2();
                                        });


                                        $("#suppid").change(function() {
                                            $("div#divLoading").addClass('show');
                                            var dataString = "suppid=" + $("#suppid").val();
                                            $.ajax({
                                                type: 'POST',
                                                url: 'suppliermaster/searchsupplierbyid_ajax.php',
                                                data: dataString
                                            }).done(function(data) {
                                                if (data !== "") {
                                                    var obj = JSON.parse(data);
                                                    $("#taxid").val(obj.taxInformation);
                                                    $("#staxname").val(obj.staxname + "-5");
                                                    calculateAmount();
                                                    $("div#divLoading").removeClass('show');
                                                }
                                            }).fail(function() {
                                            });
                                            $("div#divLoading").removeClass('show');
                                        });

                                        $("#cancelct").click(function() {
                                            $("#suppid").val("");
                                        });

                                        $("#suppid").click(function() {
                                            var valueModel = $("#suppid").val();
                                            if (valueModel === "1") {
                                                $('#custtypemodel').modal('show');
                                            }
                                        });

                                        //////////  SHIP VIA INFORMATION //////////  
                                        $("#ship_via").click(function() {
                                            var valueModel = $("#ship_via").val();
                                            if (valueModel === "0") {
                                                $('#shipviamodel').modal('show');
                                            }
                                        });
                                        $("#cancelshipvia").click(function() {
                                            $("#ship_via").val("");
                                        });
                                        $("#saveshipviabtn").click(function() {
                                            var type = "shipvia";
                                            var name = $("#name").val();
                                            var code = name;
                                            var dataString = "type=" + type + "&code=" + code + "&name=" + name;
                                            $.ajax({
                                                type: 'POST',
                                                url: 'preferencemaster/save_preferencemaster.php',
                                                data: dataString
                                            }).done(function(data) {
                                                $('#ship_via').append(data);
                                                $("#code").val("");
                                                $("#name").val("");
                                            }).fail(function() {
                                            });
                                        });
                                        //////////  SHIP VIA INFORMATION //////////
                                        $("select#staxname").change(function() {
                                            calculateAmount();
                                        });

</script>



<!-- this is custom model dialog --->
<div id="itemcreationmodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD ITEM INFORMATION</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="ship_via" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>SHIP VIA NAME</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="name" id="name" required="true"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipviabtn" data-dismiss="modal" class="btn btn-info">SAVE </a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->

<?php
if (count($bestvendorresult) != 0) {
    ?>
    <script>
        $(document).ready(function() {
            $('#bestvendor').modal('show');
        });
    </script>
    <?php
}
?>

<!-- this is custom model dialog --->
<div id="bestvendor" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>BEST VENDOR</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="ship_via" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>VENDOR NAME</td>
                    <td>PRICE</td>
                </tr>
                <?php
                foreach ($bestvendorresult as $key => $value) {
                    ?>
                    <tr>
                        <td>
                            <a href="#" onclick="chooseBestVendor('<?php echo $value["supp_id"] ?>', '<?php echo $value["price"] ?>')">
                                <?php echo $value["companyname"] ?>
                            </a>
                        </td>
                        <td><?php echo $value["price"] ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </form>
    </div>
    <div class="modal-footer"> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->
<script>
    function chooseBestVendor(suppid, price) {
        $("#suppid").val(suppid);
        $('#suppid option[value=val2]').attr('selected', 'selected');
        $("#price1").val(price);
        $('#bestvendor').modal('hide');
    }
</script>

<?php

function getBestVendorResult() {
    $itemId = filter_input(INPUT_GET, "itemId");
    $poids = MysqlConnection::fetchCustom("SELECT `po_id`,`price` FROM `purchase_item`"
                    . " WHERE `item_id` = '$itemId' ORDER BY `price` ASC LIMIT 0,3");

    $suppkey = array();
    $bestvendorarraypush = array();

    foreach ($poids as $value) {
        $poid = $value["po_id"];
        $price = $value["price"];

        $supplierresult = MysqlConnection::fetchCustom("SELECT sm.companyname, sm.supp_id FROM purchase_order po , supplier_master sm where po.supplier_id = sm.supp_id AND po.id = '$poid' ");
        if (!in_array($suppkey, $supplierresult["supp_id"])) {
            $bestvendorarray["po_id"] = $poid;
            $bestvendorarray["price"] = $price;
            $bestvendorarray["companyname"] = $supplierresult[0]["companyname"];
            $bestvendorarray["supp_id"] = $supplierresult[0]["supp_id"];

            array_push($suppkey, $supplierresult["supp_id"]);
            array_push($bestvendorarraypush, $bestvendorarray);
        }
    }
    return $bestvendorarraypush;
}
