<?php
$status = filter_input(INPUT_GET, "status");
$action = filter_input(INPUT_GET, "action");
if (isset($status)) {
    $listPerchaseOrders = MysqlConnection::fetchCustom("SELECT * FROM `purchase_order` where isOpen = 'N' ORDER BY idindex DESC ");
} else {
    $listPerchaseOrders = MysqlConnection::fetchCustom("SELECT * FROM `purchase_order` where isOpen = 'Y' ORDER BY idindex DESC ");
}
$poid = $_SESSION["poid"];
if ($action == "print") {
    $poid = filter_input(INPUT_GET, "printid");
    echo "<script language='javascript'>window.open('invoice/po_invoice.php?purchaseorderid=$poid');</script>";
}
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 320,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <br/>
    <?php
    if ($action == "email") {
        echo MysqlConnection::$MSG_SU;
    } elseif ($action == "delete") {
        echo MysqlConnection::$MSG_DE;
    } else if ($action == "update") {
        echo MysqlConnection::$MSG_UP;
    } elseif ($action == "add") {
        echo MysqlConnection::$MSG_AD;
    } elseif ($action == "poclosed") {
        echo MysqlConnection::$MSG_CL;
    } elseif ($action == "received") {
        echo MysqlConnection::$MSG_RV;
    }
    ?>

    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PURCHASE ORDER LIST</h5>
    </div>
    <br/>
    <table style="width: 100%" border="0">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=create_perchaseorder" ><i class="icon icon-user"></i>&nbsp;CREATE PURCHASE ORDER</a>
                <a href="index.php?pagename=manage_perchaseorder" id="btnSubmitFullForm" class="btn btn-info">&nbsp;ACTIVE&nbsp;PO'S</a>
                <a href="index.php?pagename=manage_perchaseorder&status=Y" id="btnSubmitFullForm" class="btn btn-info">&nbsp;CLOSED PO'S</a>
            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>
                <td>Customer Name</td>
                <td>SO.NO</td>
                <td><i class="fa fa-fw fa-sort"></i>PO.NO</td>
                <td>Vendor Name</td>
                <td>PO IS?</td>
                <td>Ship Via</td>
                <td>Total Amount</td>
                <td>Delivery Date</td>
                <td>Entered By</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listPerchaseOrders as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $isOpen = $value["isOpen"] == "Y" ? "Open" : "Close";
                $isOpenclt = $value["isOpen"] == "Y" ? "btn btn-success  btn-mini" : "btn btn-warning  btn-mini";
                $supplierdetails = MysqlConnection::getSupplierDetails($value["supplier_id"]);
                ?>
                <tr id="<?php echo $value["id"] ?>" class="context-menu-one" onclick="setId('<?php echo $value["id"] ?>')" ondblclick ="viewPO('<?php echo $value["id"] ?>')" style="background-color: <?php echo $bgcolor ?>;" >
                    <td><?php echo $index ?></td>
                    <td ><?php echo $value["c_name"] ?></td>
                    <td><?php echo $value["c_sono"] ?></td>
                    <td><span  data-toggle="tooltip" data-original-title="View Purchase Order"><a href="index.php?pagename=view_perchaseorder&purchaseorderid=<?php echo $value["id"] ?>"><?php echo $value["purchaseOrderId"] ?></a></span></td>
                    <td><span  data-toggle="tooltip" data-original-title="Go to vendor details">
                        <a href="index.php?pagename=view_suppliermaster&supplierid=<?php echo $value["supplier_id"] ?>"> <?php echo $supplierdetails["companyname"] ?>&nbsp;</a>
                        <a href="tel:<?php echo $supplierdetails["supp_phoneNo"] ?>">
                            <?php echo $supplierdetails["supp_phoneNo"] ?>
                        </a></span>
                    </td>
                    <td ><i class="<?php echo $isOpenclt ?>" ><?php echo $isOpen ?></i></td>
                    <td ><?php echo $value["ship_via"] ?></td>
                    <td><?php echo $value["total"] ?></td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($value["expected_date"])?></td>
                    <td><a href="index.php?pagename=view_usermanagement&userid=<?php echo $value["added_by"] ?>"><?php echo MysqlConnection::getAddedBy($value["added_by"]) ?></a> </td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" style="margin-left:10px;">EMAIL</button>
        </div> 
    </div>
</div>
<script>
    $('#example tbody tr').click(function (e) {
        var id = $(this).attr('id');
        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var del = '<a href="invoice/po_invoice.php?purchaseorderid=' + id + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var em = '<a href="index.php?pagename=email_perchaseorder&purchaseorderid=' + id + '" ><button type="button" class=" btn btn-info" style="margin-left:10px;">EMAIL</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + del + em + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });



    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }

    function setId(val) {
        document.getElementById("rightClikId").value = val;
    }
    $("#myElem").show().delay(3000).fadeOut();
</script>
<script>
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_purchaseorder":
                        window.location = "index.php?pagename=view_perchaseorder&purchaseorderid=" + id;
                        break;
                    case "create_purchaseorder":
                        window.location = "index.php?pagename=create_perchaseorder";
                        break;
                    case "create_receiving":
                        window.location = "index.php?pagename=create_receivingorder&purchaseorderid=" + id;
                        break;
                    case "edit_purchaseorder":
                        window.location = "index.php?pagename=create_perchaseorder&purchaseorderid=" + id;
                        break;
                    case "delete_purchaseorder":
                        window.location = "index.php?pagename=view_perchaseorder&purchaseorderid=" + id + "&flag=true";
                        break;

                    case "print_invoice":
                        window.location = "invoice/po_invoice.php?purchaseorderid=" + id;
//                        window.open("download/purchaseorder/PO254482.pdf", '_blank');
                        break;
                    case "mail_purchaseorder":
                        window.location = "index.php?pagename=email_perchaseorder&purchaseorderid=" + id;
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    default:
                        window.location = "index.php?pagename=manage_perchaseorder";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_purchaseorder": {name: "VIEW PURCHASE ORDER", icon: "+"},
                "edit_purchaseorder": {name: "EDIT PURCHASE ORDER", icon: "context-menu-icon-add"},
                "delete_purchaseorder": {name: "DELETE PURCHASE ORDER", icon: ""},
                "sep1": "---------",
                "create_receiving": {name: "CREATE RECEIVING ORDER", icon: ""}
            }
        });
    });
    function viewPO(id) {
        window.location = "index.php?pagename=view_perchaseorder&purchaseorderid=" + id;
    }
</script>