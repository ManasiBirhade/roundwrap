function setDetails(count) {
    var item_code = $("#tags" + count).val();
    var dataString = "item_code=" + item_code;
    $.ajax({
        type: 'POST',
        url: 'itemmaster/getitemajax.php',
        data: dataString
    }).done(function(data) {
        if (data !== "") {
            var jsonobj = JSON.parse(data);
            if (jsonobj.unit !== null) {
                $("#unit" + count).text(jsonobj.unit);
                if (parseFloat(jsonobj.total_purch_rate) === parseFloat("0.0")) {
                    $("#price" + count).val(jsonobj.purchase_rate);
                } else {
                    $("#price" + count).val(jsonobj.purchase_rate);
                }
            }
        } else {
            $("#tags" + count).val("");
            //$("#tags" + count).focus();
            //alert("No item available for the code");
        }
    }).fail(function() {
    });
    //getLessPrice(count);
    $("#price" + count).focus();
}


function getLessPrice(count) {
    var item_code = $("#tags" + count).val();
    var dataString = "item_code=" + item_code;
    $.ajax({
        type: 'POST',
        url: 'itemmaster/getbestitems.php',
        data: dataString
    }).done(function(data) {
        var jsonobj = JSON.parse(data);
        console.log(jsonobj);
        if (jsonobj.length !== 0) {

        }
        if (jsonobj !== null && jsonobj !== "") {
            //createTableFromJSON(jsonobj)
        }
    }).fail(function() {
    });
}



function clearValue(count) {
//    $("#desc" + count).text("");
    $("#unit" + count).text("");
    $("#price" + count).val("");
    $("#tags" + count).val("");
    $("#total" + count).text("");
    finalTotal();
}

function calculateAmount() {
    var ship_charge = $("#ship_charge").val();
    var discount = $("#discount").val();

    if (ship_charge === "") {
        ship_charge = 0.0;
    }




    var totalAmount = 0.0;
    for (var count = 1; count <= 30; count++) {
        var price = $("#price" + count).val();
        var amount = $("#amount" + count).val();
        if (price !== "" && price !== undefined && amount !== "" && amount !== undefined) {
            var total = parseFloat(price) * parseFloat(amount);
            $("#total" + count).text((Math.round(total * 100) / 100));
            totalAmount = parseFloat(totalAmount) + (parseFloat(price) * parseFloat(amount));
        }
    }
    var discountvalue = 0.0;
    if (discount !== "" && discount !== undefined) {
        discountvalue = (parseFloat(totalAmount) * (parseFloat(discount) / 100));
        $("#discountvalue").val(discountvalue.toFixed(2));
    }


    var finaltotal = totalAmount;
    if (discount === "") {
        discount = 0.0;
    } else {
        totalAmount = totalAmount - discountvalue;
    }

    $("#finaltotal").val(finaltotal.toFixed(2));


    var taxname = $("#taxid option:selected").text();
    if (taxname !== undefined && taxname !== "" && taxname !== "SELECT") {
        var goodstotal = $("#finaltotal").val();
        var splitselect = taxname.split("-");
        if (goodstotal !== undefined && goodstotal !== "" && splitselect !== undefined && splitselect !== "") {
            var taxAmount = ((parseFloat(goodstotal) - parseFloat(discount)) * parseFloat(splitselect[1]) / 100);
            $("#taxamount").val(taxAmount.toFixed(2));
        }
    }

    var taxamount = $("#taxamount").val();
    if (taxamount === "" || taxamount === undefined) {
        taxamount = 0.0;
    }

    var staxname = $("#staxname").val();
    var staxamount = 0.0;
    if (staxname === "GST-5") {
        var ship_charge = $("#ship_charge").val();
        if (ship_charge !== undefined && ship_charge !== "" && ship_charge !== "0") {
            staxamount = 0.05 * parseFloat(ship_charge);
        } else {
            staxamount = 0.0;
        }
        $("#staxpercent").val(staxamount.toFixed(2));
    } else {
        $("#staxpercent").val(staxamount.toFixed(2));
    }

    $("#totalTax").val((parseFloat(totalAmount) + parseFloat(taxamount)).toFixed(2));



    var finalTotal = parseFloat(totalAmount) + parseFloat(taxamount) + parseFloat(staxamount) + parseFloat(ship_charge);

    $("#nettotal").val(finalTotal.toFixed(2));
}
