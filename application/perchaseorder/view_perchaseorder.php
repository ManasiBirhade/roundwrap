<style>
    table tbody {

    }
    table tr td{
        padding: 5px;
    }
</style>
<?php
$purchaseid = filter_input(INPUT_GET, "purchaseorderid");
$flag = filter_input(INPUT_GET, "flag");
$result = MysqlConnection::fetchCustom("SELECT * , "
                . "( SELECT companyname FROM supplier_master WHERE supp_id = po.`supplier_id` ) AS companyname,discountvalue FROM purchase_order po, purchase_item pi WHERE po.id = pi.po_id AND pi.po_id ='$purchaseid'");
$purchaseorder = $result[0];


$salesperson = MysqlConnection::getGenericById($purchaseorder["added_by"]);

if (isset($_POST["purchaseorderid"]) && isset($_GET["flag"])) {
    $poid = $_POST["purchaseorderid"];
    $vendorid = MysqlConnection::fetchCustom("SELECT supplier_id FROM purchase_order WHERE id = '$poid'");
    $selectbal = "SELECT `supp_balance` as supp_balance  FROM `supplier_master` where `supp_id` = '" . $vendorid[0]["supplier_id"] . "'";
    $balancedetails = MysqlConnection::fetchCustom($selectbal);
    $balance = $balancedetails[0]["supp_balance"];
    $pototal = "SELECT `total` as total  FROM `purchase_order` where `id` = '$poid'";
    $pobalance = MysqlConnection::fetchCustom($pototal);
    $newbalance = $balance - $pobalance[0]["total"];
    MysqlConnection::delete("UPDATE `supplier_master` SET `supp_balance` = '$newbalance' WHERE `supp_id` = '" . $vendorid[0]["supplier_id"] . "'");
    MysqlConnection::delete("DELETE FROM purchase_order WHERE id = '$poid' ");
    MysqlConnection::delete("DELETE FROM purchase_item WHERE po_id = '$poid' ");
    //header("location:index.php?pagename=success_perchaseorder&purchaseorderid=$poid&flag=yes");
    header("location:index.php?pagename=manage_perchaseorder&action=delete");
}
?>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 0px;margin: 0px;}
</style>
<form  method="post">

    <div class="container-fluid" style="" >
        <?php
        if ($flag == "true") {
            echo MysqlConnection::$DELETE;
        }
        ?>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">

            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">VIEW PURCHASE ORDER</a></li>
                </ul>
            </div>

            <div style="text-align: center;font-size: 12px;font-weight: bold;color: red;font-family: verdana">
                <?php echo $_SESSION["msg"] ?>

            </div>
            <table style="width: 100%"  class="display nowrap sortable" >
                <tr>
                    <td>

                        <div class="widget-box">
                            <div class="widget-title">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab1">ORDER DETAILS</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab2">ADDRESS DETAILS</a></li>
                                </ul>
                            </div>
                            <div class="widget-content tab-content">
                                <div id="tab1" class="tab-pane active">
                                    <table style="width: 100%"  class="table table-bordered display nowrap sortable">
                                        <tr >
                                            <td style="width: 13%;background-color: white">PO NUMBER</td>
                                            <td style="width: 22%"><?php echo $purchaseorder["purchaseOrderId"] ?></td>
                                            <td style="width: 13%;background-color: white">ORDER DATE</td>
                                            <td style="width: 22%"><?php echo $purchaseorder["purchasedate"] ?></td>
                                            <td style="width: 13%;background-color: white">SALES PERSON</td>
                                            <td style="width: 10%"><?php echo $salesperson["name"] ?></td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: white">VENDOR NAME</td>
                                            <td>
                                                <?php echo $purchaseorder["companyname"] ?>
                                                <div id="error" style="color: red"></div>
                                            </td>
                                            <td style="width: 10%;background-color: white">SHIP VIA</td>
                                            <td><?php echo $purchaseorder["ship_via"] ?></td>
                                            <td style="width: 10%;background-color: white">SHIP&nbsp;DATE</td>
                                            <td><?php echo MysqlConnection::convertToPreferenceDate($purchaseorder["expected_date"]) ?></td>
                                        </tr>
                                        <tr >
                                            <td style="width: 13%;background-color: white">SO NUMBER</td>
                                            <td style="width: 22%"><?php echo $purchaseorder["c_sono"] ?></td>
                                            <td style="width: 13%;background-color: white">CUSTOMER NAME</td>
                                            <td style="width: 22%"><?php echo $purchaseorder["c_name"] ?></td>
                                            <td style="width: 13%;background-color: white"></td>
                                            <td style="width: 10%"></td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="tab2" class="tab-pane" >
                                    <table style="width: 100%" class="table table-bordered display nowrap sortable">
                                        <tr style="vertical-align: top">
                                            <td style="width: 12%;background-color: white">BILLING&nbsp;ADDRESS</td>
                                            <td style="width: 23%"><?php echo $purchaseorder["billing_address"] ?></td>
                                            <td style="width: 12%;background-color: white">SHIPPING&nbsp;ADDRESS</td>
                                            <td style="width: 23%"><?php echo $purchaseorder["shipping_address"] ?></td>
                                            <td style="width: 12%;background-color: white">REMARK&nbsp;/&nbsp;NOTE</b></td>
                                            <td style="width: 23%"><?php echo $purchaseorder["remark"] ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>                            
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>

                        <div class="widget-box">
                            <div class="widget-title">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab5">ITEM DETAIL</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab6">RECEIVING HISTORY</a></li>
                                </ul>
                            </div>
                            <div class="widget-content tab-content">
                                <div id="tab5" class="tab-pane active">
                                    <div style="width: 100%;">
                                        <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                            <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white">
                                                <td style="width: 25px;">#</td>
                                                <td style="width: 350px;">ITEM DETAIL</td>
                                                <td style="width: 80px;">UNIT</td>
                                                <td style="width: 80px;text-align: right">PRICE&nbsp;</td>
                                                <td style="width: 80px;text-align: right">QTY&nbsp;</td>
                                                <td style="width: 120px;text-align: right;">REC.QTY&nbsp;</td>
                                                <td style="text-align: right">AMOUNT&nbsp;</td>
                                            </tr>
                                            <?php
                                            $index = 1;
                                            $totalOrderItem = 0;
                                            $totalReceivedItem = 0;
                                            foreach ($result as $key => $value) {
                                                $items = MysqlConnection::getItemDataById($value["item_id"]);

                                                $totalOrderItem = $totalOrderItem + $value["qty"];
                                                $totalReceivedItem = $totalReceivedItem + $value["rqty"];
                                                ?>
                                                <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                                    <td style="width: 25px"><?php echo $index++ ?></td>
                                                    <td >
                                                        <a href="index.php?pagename=view_itemmaster&itemId=<?php echo $value["item_id"] ?>"> <?php echo $items["item_code"] ?> <?php echo $items["item_desc_purch"] ?> </a>
                                                    </td>
                                                    <td style="width: 100px;" ><div id="unit"></div><?php echo $items["unit"] ?></td>
                                                    <td style="width: 100px;text-align: right"><div id="price"></div><?php echo $value["price"] ?>&nbsp;</td>
                                                    <td style="width:100px;text-align: right"><?php echo $value["qty"] ?>&nbsp;</td>
                                                    <td style="width:100px;text-align: right;background-color: rgb(247,252,231)"><?php echo $value["rqty"] ?>&nbsp;</td>
                                                    <td style="width: 100px;text-align: right">
                                                        <?php echo number_format((float) ($value["price"] * $value["qty"]), 2, '.', ''); ?>&nbsp;
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr >
                                                <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                                <td  style="background-color: white"><b>Total Amount</b></td>
                                                <td  style="background-color: white;text-align: right"><?php echo $purchaseorder["sub_total"]; ?></td>
                                            </tr>
                                            <tr >
                                                <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                                <td style="background-color: white"><b>Discount <?php echo $purchaseorder["discount"]; ?>%</b></td>
                                                <td style="background-color: white;text-align: right"><?php echo $purchaseorder["discountvalue"]; ?></td>
                                            </tr>
                                            <tr >
                                                <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                                <td style="background-color: white"><b>Goods Tax</b> ( <?php echo $value["taxname"] . "-" . $value["taxpercent"] ?>)</td>
                                                <td style="background-color: white;text-align: right"> <?php echo $purchaseorder["taxamount"]; ?></td>
                                            </tr>
                                            <tr >
                                                <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                                <td style="background-color: white"><b>Sub Total</b></td>
                                                <td style="background-color: white;text-align: right"><?php echo $purchaseorder["totalTax"]; ?></td>
                                            </tr>
                                            <tr >
                                                <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                                <td style="background-color: white"><b>Shipping Charges</b></td>
                                                <td style="background-color: white;text-align: right"><?php echo $purchaseorder["ship_charge"] ?></td>
                                            </tr>
                                            <tr >
                                                <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                                <td style="background-color: white"><b>Ship Tax</b> ( <?php echo $purchaseorder["staxname"]; ?> ) </td>
                                                <td style="background-color: white;text-align: right"><?php echo $purchaseorder["staxpercent"]; ?></td>
                                            </tr>


                                            <tr >
                                                <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                                <td style="background-color: white"><b>Net Total</b></td>
                                                <td style="background-color: white;text-align: right"><?php echo $purchaseorder["total"]; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div id="tab6" class="tab-pane">
                                    <table class="display nowrap sortable table-bordered" style="width: 80%;border-collapse: collapse" border="1">
                                        <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white">
                                            <td style="width: 25px;">#</td>
                                            <td style="width: 450px;">ITEM DETAIL</td>
                                            <td style="width: 100px;">UNIT</td>
                                            <td style="width: 150px;">RECEIVED QUANTITY</td>
                                            <td >RECEIVED DATE</td>
                                        </tr>
                                        <?php
                                        $resultreceiving = MysqlConnection::getReceivingOrderByPoId($purchaseid);
                                        $indexr = 1;
                                        foreach ($resultreceiving as $key => $valuer) {
                                            $items = MysqlConnection::getItemDataById($valuer["item_id"]);
                                            ?>
                                            <tr  style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                                <td style="width: 25px;"><?php echo $indexr++ ?></td>
                                                <td style="width: 450px;"><?php echo $items["item_code"] ?> <?php echo $items["item_desc_purch"] ?></td>
                                                <td style="width: 100px;"><?php echo $items["unit"] ?></td>
                                                <td style="width: 150px;"><?php echo $valuer["rqty"] ?></td>
                                                <td ><?php echo MysqlConnection::convertToPreferenceDate($valuer["date"]) ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                            </div>                            
                        </div>
                    </td>
                </tr>      
            </table>
            <div class="modal-footer " style="text-align: center"> 
                <a  href="index.php?pagename=manage_perchaseorder" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                <?php
                if (isset($_GET["flag"])) {
                    ?>
                    <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info"/>
                    <input type="hidden" name="purchaseorderid" value="<?php echo $_GET["purchaseorderid"] ?>"/>
                    <?php
                } else {
                    if ($totalOrderItem != $totalReceivedItem) {
                        ?>
                        <a   class="btn btn-info"  href="index.php?pagename=create_receivingorder&purchaseorderid=<?php echo $purchaseid ?>">CREATE RECEIVING ORDER</a> 
                        <?php
                    }
                    ?>
                    <a   class="btn btn-info" target="_blank" href="invoice/po_invoice.php?purchaseorderid=<?php echo $purchaseid ?>">PRINT</a>   
                    <?php
                }
                ?>
            </div>
            <hr/>
        </div>
    </div>
</form>
<?php $_SESSION["msg"] = ""; ?>
<script src="js/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/maruti.js"></script> 
<script src="js/maruti.form_common.js"></script>
