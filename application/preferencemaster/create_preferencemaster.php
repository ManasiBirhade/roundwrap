<?php
$preferenceid = filter_input(INPUT_GET, "preferenceid");
if (isset($preferenceid)) {
    $resultset = MysqlConnection::fetchCustom("SELECT * FROM generic_entry WHERE id = '$preferenceid'");
    $date = $resultset[0];
}

$array = array();
$array["type"] = $type = filter_input(INPUT_POST, "type");
$array["code"] = filter_input(INPUT_POST, "code");
if ($array["type"] != "dateformat") {
    $array["name"] = filter_input(INPUT_POST, "name1");
} else {
    $array["name"] = filter_input(INPUT_POST, "name2");
}
$description = filter_input(INPUT_POST, "description");
$array["description"] = MysqlConnection::formatToBRAddress($description);

$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
if (isset($btnSubmitFullForm)) {

    $array["active"] = "Y";
    if (filter_input(INPUT_POST, "type") == "printvalues") {
        $formvalue = $_POST["formvalue"];
        $array["code"] = implode(",", $formvalue);
        $array["name"] = implode(",", $formvalue);
    }
    if (isset($preferenceid)) {
        $active = filter_input(INPUT_POST, "active");
        $array["active"] = $active == "Y" ? "Y" : "N";
        MysqlConnection::edit("generic_entry", $array, " id = '$preferenceid' ");
    } else {
        MysqlConnection::insert("generic_entry", $array);
    }
    header("location:index.php?pagename=manage_preferencemaster");
}
$arrpreferencetype = array();
$arrpreferencetype["dateformat"] = "Date format";
$arrpreferencetype["kerfvalue"] = "Kerf value";
$arrpreferencetype["imperial"] = "Imperial";
$arrpreferencetype["representative"] = "Representative";
$arrpreferencetype["customer_type"] = "Customer Type";
$arrpreferencetype["paymentterm"] = "Payment Term";
$arrpreferencetype["usertype"] = "Designation";
$arrpreferencetype["ipaddress"] = "IP Address";
$arrpreferencetype["scannerstep"] = "Scanner Step";
$arrpreferencetype["shipvia"] = "Ship Via";
$arrpreferencetype["promotionnote"] = "Promotion Note";
$arrpreferencetype["inventorydiscrepancy"] = "Inventory Discrepancy Reasons";
$arrpreferencetype["companyaddress"] = "Company Address";
$arrpreferencetype["logo"] = "Company Logo";
$arrpreferencetype["poaddress"] = "PO Billing Address";
$arrpreferencetype["customervalue"] = "Walk In Customer Discount";
$arrpreferencetype["rwvalue"] = "Round Wrap Customer Discount";
$arrpreferencetype["printvalues"] = "Show In Print";
$arrpreferencetype["productioncapacity"] = "Production Capacity";
$arrpreferencetype["productioncategory"] = "Production Category";
?>
<?php
if ($date["type"] == "printvalues") {
    $expload = explode(",", $date["code"]);
    $account = in_array("account", $expload) ? "checked" : "";
    $address = in_array("address", $expload) ? "checked" : "";
    $term = in_array("term", $expload) ? "checked" : "";
    ?>
    <script>
        $(document).ready(function () {
            $('#code').remove();
            $('#name').remove();
            $('#description').remove();
            $('#myTable tr:last').after('<tr><td>Account </td><td><input name="formvalue[]" <?php echo $account ?> value="account" type="checkbox"></td></tr>');
            $('#myTable tr:last').after('<tr><td>Address </td><td><input name="formvalue[]" <?php echo $address ?>  value="address"  type="checkbox"></td></tr>');
            $('#myTable tr:last').after('<tr><td>Term    </td><td><input name="formvalue[]" <?php echo $term ?>  value="term"  type="checkbox"></td></tr>');
        });
    </script>
    <?php
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">MANAGE PREFERENCE</h5>
    </div>
    <div class="widget-box" >
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Setup Information </a></li>
            </ul>
        </div>
        <form name="frmCmpSubmit"  enctype="multipart/form-data" id="frmCmpSubmit" method="post" >
            <input type="hidden" name="id" id="id" value="<?php echo $date["id"] ?>" />
            <div class="widget-content tab-content">

                <table  style="width: 60%;vertical-align: top" border="0" class="display nowrap sortable" id="myTable">
                    <tr style="height: 35px;">
                        <td style="width: 150px;"><label class="control-label">Active</label></td>
                        <td>
                            <?php
                            if ($date["active"] == "N") {
                                $checked = "";
                            } else {
                                $checked = "checked";
                            }
                            ?>
                            <input  type="checkbox" value="Y" name="active" <?php echo $checked ?> style="height: 15px;width: 15px;margin-top: -5px;">
                        </td>
                    </tr> 
                    <tr>
                        <td style="width: 150px;"><label class="control-label">Preference Type</label></td>
                        <td> 
                            <select style="width: 225px;" name="type" id="type">
                                <option value="">Select</option>
                                <?php
                                foreach ($arrpreferencetype as $key => $value) {
                                    $selected = $date["type"] == $key ? "selected" : "";
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $key ?>"><?php echo $value ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr id="code">
                        <td style="width: 150px;"><label class="control-label" id="cntlabel">Code</label></td>
                        <td><input style="width: 220px;" type="text" name="code" required="" value="<?php echo $date["code"] ?>" id="code" minlenght="2" maxlength="30"></td>
                    </tr> 
                    <tr id="name"> 
                        <td style="width: 150px;"><label class="control-label" id="cntvalue">Name / Value</label></td>
                        <td id="inputOption"><input style="width: 220px;" type="text" name="name1" value="<?php echo $date["name"] ?>" id="name1" minlenght="2" maxlength="30"></td>
                        <td id="selectOption">
                            <select style="width: 225px;" name="name2" id="name2">
                                <option <?php echo $date["name"] == "Y-m-d" ? "selected" : "" ?> value="Y-m-d">YYYY-MM-DD [<?php echo date("Y-m-d") ?>]</option>
                                <option <?php echo $date["name"] == "d-m-Y" ? "selected" : "" ?> value="d-m-Y">DD-MM-YYYY [<?php echo date("d-m-Y") ?>]</option>
                            </select>
                        </td>
                    </tr> 
                    <tr id="description">
                        <td><label class="control-label">Description</label></td>
                        <td ><textarea style="width: 100%;height: 75px;line-height: 25px;resize: none"  required=""  name="description" id="description"  ><?php echo MysqlConnection::formatToSlashNAddress(trim($date["description"])) ?></textarea></td>
                    </tr>
                </table>
                <hr/>
                <table border="0">
                    <tr>
                        <td><button type="submit" name="btnSubmitFullForm" id="btnSubmitFullForm" class="btn btn-info">SAVE</button></td>
                        <td>  <a href="index.php?pagename=manage_preferencemaster" class="btn btn-info">CANCEL</a></td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        var edittype = document.getElementById('type').value;
        if (edittype === 'dateformat') {
            $("#inputOption").hide();
            $("#selectOption").show();
        } else {
            $("#selectOption").hide();
            $("#inputOption").show();
        }
        $("#type").change(function () {
            var type = document.getElementById('type').value;
            if (type === 'dateformat') {
                $("#inputOption").hide();
                $("#selectOption").show();
            } else if (type === "inventorydiscrepancy") {
                $("#code").val("InvDis");
                $("#name1").val("Inventory Discrepancy");
            } else if (type === "promotionnote") {
                $("#code").val("PC");
                $("#name1").val("PROMO CODE");
            } else if (type === "customervalue") {
                $("#code").val("Increase");
                $("#cntlabel").text("Price Level");
                $("#cntvalue").text("Price By");
                $("#name1").focus();
            } else if (type === "rwvalue") {
                $("#code").val("Discount");
                $("#cntlabel").text("Price Level");
                $("#cntvalue").text("Discount By %");
                $("#name1").focus();
            } else if (type === "printvalues") {
                $('#code').remove();
                $('#name').remove();
                $('#description').remove();
                $('#myTable tr:last').after('<tr><td>Account </td><td><input name="formvalue[]" value="account" type="checkbox"></td></tr>');
                $('#myTable tr:last').after('<tr><td>Address </td><td><input name="formvalue[]" value="address"  type="checkbox"></td></tr>');
                $('#myTable tr:last').after('<tr><td>Term    </td><td><input name="formvalue[]" value="term"  type="checkbox"></td></tr>');
            } else {
                $("#selectOption").hide();
                $("#inputOption").show();
                $("#code").val("");
                $("#name1").val("");
            }
        });
    });
//    $("#type").change(function () {
//        
//    });
</script>
