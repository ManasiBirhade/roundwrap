<?php
$status = filter_input(INPUT_GET, "status");
$preferenceid = filter_input(INPUT_GET, "preferenceid");

if ($status == "category") {
    $sqlquery = "SELECT * FROM `generic_entry` WHERE `type` = 'productioncategory'";
} else {
    $sqlquery = "SELECT * FROM `generic_entry` ORDER BY `type` ASC";
}

$resultset = MysqlConnection::fetchCustom($sqlquery);

$btnSave = filter_input(INPUT_POST, "btnSave");
if ($btnSave == "SAVE") {
    $preferenceid = filter_input(INPUT_POST, "modelworkorderid");
    $productioncapacity = filter_input(INPUT_POST, "productioncapacity");
    $query = "UPDATE generic_entry SET productioncapacity = '$productioncapacity' WHERE id = '$preferenceid' ";
    MysqlConnection::delete($query);
    header("location:index.php?pagename=manage_preferencemaster&status=category");
}
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PREFERENCE LIST</h5>
    </div>
    <br/>
    <table  style="width: 100%">
        <tr>
            <td style="float: left">
                <a class="btn btn-info"  href="index.php?pagename=create_preferencemaster" ><i class="icon icon-list"></i>&nbsp;&nbsp;SETUP PREFERENCE </a>
                <a href="index.php?pagename=manage_preferencemaster&status=category" id="btnOrder" class="btn btn-info">VIEW PRODUCTION CATEGORY</a>
                <a href="index.php?pagename=manage_preferencemaster" id="btnOrder" class="btn btn-info">VIEW ALL DATA</a>
            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Type</td>
                <td>Code</td>
                <td>Name</td>
                <td>Description</td>
                <?php if ($status == "category") { ?>
                    <td>Capacity</td>
                <?php } ?>
                <td>Active</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($resultset as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                ?>
                <tr id="<?php echo $value["id"] ?>" style="background-color: <?php echo $bgcolor ?>;" class="context-menu-one">
                    <td ><?php echo $index ?></td>
                    <td ><?php echo ucwords(str_replace("_", " ", $value["type"])) ?></td>
                    <td ><?php echo $value["code"] ?></td>
                    <td ><?php echo $value["name"] ?></td>
                    <td style="width: 35%">
                        <?php echo $value["description"] ?>
                    </td>
                    <?php if ($status == "category") { ?>
                        <td><?php echo $value["productioncapacity"] ?></td>
                    <?php } ?>
                    <td >
                        <?php echo $value["active"] == "Y" ? "<span class='badge badge-success'>YES</span>" : "<span class='badge badge-info'>NO</span>" ?>
                    </td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
</div>
<div id="sequencemodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <form class="form-horizontal" method="post"  id="workOrd_Id" novalidate="novalidate">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">×</button>
            <h3>ADD PRODUCTION CAPACITY</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" id="modelworkorderid" name="modelworkorderid">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>CAPACITY</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="productioncapacity" id="productioncapacity" required="true" ></td>

                </tr>
            </table>
        </div>
        <div class="modal-footer"> 
            <input type="submit" name="btnSave" class="btn btn-info"   value="SAVE">
            <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
        </div>
    </form>
</div>

<script type="text/javascript">//
    $(function() {
        $.contextMenu({
        selector: '.context-menu-one',
                callback: function(key, options) {
                    var m = "clicked row: " + key;
                    var id = $(this).attr('id');
                    switch (key) {
                        case "view_setup":
                            window.location = "index.php?pagename=view_preferencemaster&preferenceid=" + id;
                            break;
                        case "create_setup":
                            window.location = "index.php?pagename=create_preferencemaster";
                            break;
                        case "edit_setup":
                            window.location = "index.php?pagename=create_preferencemaster&preferenceid=" + id;
                            break;
                        case "delete_setup":
                            window.location = "index.php?pagename=view_preferencemaster&preferenceid=" + id + "&flag=yes";
                            break;
<?php if ($status == "category") { ?>
                            case "productionalert":
                                $('#modelworkorderid').val(id);
                                $('#sequencemodel').modal('show');
                                break;
<?php } ?>
                        case "quit":
                            window.location = "index.php?pagename=manage_dashboard";
                            break;

                        default:
                            window.location = "index.php?pagename=manage_preferencemaster";
                    }
                },
                items: {
                "view_setup": {name: "VIEW PREFERENCE", icon: ""},
                        "edit_setup": {name: "EDIT PREFERENCE", icon: ""},
                        "delete_setup": {name: "DELETE PREFERENCE", icon: ""},
<?php if ($status == "category") { ?>
                    "productionalert": {name: "PRODUCTION ALERT", icon: ""}
<?php } ?>
                }
    });
    });
            $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_preferencemaster&preferenceid=" + id;
        }
    });
</script>
