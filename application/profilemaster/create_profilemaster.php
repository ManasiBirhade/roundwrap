<?php
$flag = filter_input(INPUT_GET, "flag");
$id = filter_input(INPUT_GET, "id");
$operation = filter_input(INPUT_GET, "operation");

$arraycategory = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'productioncategory' ORDER BY `indexid`");
$userstring = MysqlConnection::fetchCustom("SELECT * FROM `pvcgeneric_entry` WHERE `type` LIKE 'userstringvalue' ORDER BY indexid ASC");

$profile = MysqlConnection::getPortfolioProfileById($id);
$uservariablestring = $profile["uservariablestring"];
$userdescriptionstring = $profile["userdescriptionstring"];

if (trim($uservariablestring) != "" && trim($userdescriptionstring) != "") {
    $userstring = array();
    $expload1 = explode(";", $uservariablestring);
    $expload2 = explode(";", $userdescriptionstring);
    for ($indexcode = 0; $indexcode <= count($expload2); $indexcode++) {
        $codevalue = array();
        $codevalue["code"] = $expload1[$indexcode];
        $codevalue["name"] = $expload2[$indexcode];
        array_push($userstring, $codevalue);
    }
}

$nameprofile = $profile["portfolio_name"];

$resultset = MysqlConnection::getPortfolioProfileByProfId($id);
$resultuserstring = MysqlConnection::getPortfolioProfileById($id);

saveOrEditProfiles($userstring);

$btnSubmitCategory = filter_input(INPUT_POST, "btnSubmitCategory");
if ($btnSubmitCategory == "SAVE") {
    $categoryname = filter_input(INPUT_POST, "categoryname");
    if ($categoryname != "") {
        $array = array();
        $array["type"] = "productioncategory";
        $array["code"] = $categoryname;
        $array["name"] = $categoryname;
        $array["description"] = $categoryname;
        $array["active"] = "Y";
        MysqlConnection::insert("generic_entry", $array);
        header("location:index.php?pagename=create_profilemaster");
    }
}
?>
<style>
    input,textarea,select,date{ width: 90%;height: 28px; }
    input[type="text"]{ height: 24px; width: 89%;}
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    table.sortable tr td{
        padding: 5px;
    }
</style>

<form  method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="container-fluid" style="" >
        <?php
        if ($id != "") {
            echo "<BR/><div class='blink'><div class='alert alert-error'><strong>Your changes will affect quotation</strong></div></div>";
        }
        ?>
        <div class="cutomheader">
            <h5 style="font-family: verdana;font-size: 12px;">PROFILE MANAGEMENT</h5>
        </div>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANAGE PROFILE</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 50%;margin-top: 2px">
                <tr>
                    <td style="vertical-align: top">
                        <fieldset  class="well the-fieldset">
                            <table id="addlabel" style="width: 80%" class="display nowrap sortable" > 
                                <tr>
                                    <td><label class="control-label">Category&nbsp;:</label></td>
                                    <td>
                                        <select name="portfolio_name" id="portfolio_name" style="width: 92%">
                                            <option></option>
                                            <option value="1"><< ADD NEW >></option>
                                            <?php
                                            foreach ($arraycategory as $value) {
                                                if (strtoupper($nameprofile) == strtoupper($value["name"])) {
                                                    $selected = "selected";
                                                } else {
                                                    $selected = "";
                                                }
                                                ?>
                                                <option <?php echo $selected ?>  value="<?php echo $value["name"] ?>"><?php echo $value["name"] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label class="control-label">Profile Name&nbsp;:</label></td>
                                    <td><input type="text" style="width: 90%" required="" maxlength="20" minlength="2"  value="<?php echo $profile["profile_name"] ?>" name="profile_name" id="profile_name"/></td>
                                </tr>
                                <tr>
                                    <td><label class="control-label">Profile Type&nbsp;:</label></td>
                                    <td><input type="text" style="width: 90%" placeholder="Enter value Door/Drawer Front/Window " required="" maxlength="20" minlength="2"  value="<?php echo $profile["profile_type"] ?>" name="profile_type" id="profile_type"/></td>
                                </tr>
                                <tr>
                                    <td><label class="control-label">Base Price&nbsp;:</label></td>
                                    <td><input type="text" style="width: 90%" required="" maxlength="20" minlength="2"  value="<?php echo $profile["baseprice"] ?>" placeholder="Put 0.00 if no base price" maxlength="7" onkeypress="return chkNumericKey(event)" name="baseprice" id="baseprice"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table class="display nowrap sortable" id="addrow" style="width: 400px" border="1">
                                            <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>Label</td>
                                                    <td>Display Order</td>
                                                    <td><a class="icon-plus" href="#" id="icon-plus"  ></a></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $index = 1;
                                                foreach ($resultset as $value) {
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center"><?php echo $index ?></td>
                                                        <td style="width: 45%">
                                                            <input type="text" name="labelField<?php echo $index ?>" 
                                                                   value="<?php echo ucwords(str_replace("_", " ", $value["profilelabel"])) ?>">
                                                        </td>
                                                        <td style="width: 48%">
                                                            <select name="labelstep<?php echo $index ?>">
                                                                <option value="0">SKIP</option>
                                                                <?php
                                                                for ($indexp = 1; $indexp <= 20; $indexp++) {
                                                                    if ($indexp == $value["profilecounter"]) {
                                                                        $selected = "selected";
                                                                    } else {
                                                                        $selected = "";
                                                                    }
                                                                    ?>
                                                                    <option <?php echo $selected ?> value="<?php echo $indexp ?>"><?php echo $indexp ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                        <td><a class="icon-trash" id="icon-trash" href="#"  ></a></td>
                                                    </tr>
                                                    <?php
                                                    $index++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table> 
                        </fieldset>
                    </td>
                    <td style="vertical-align: top">
                        <fieldset>
                            <table class="display nowrap sortable" style="width: 500px;" border="1">

                                <tr>
                                    <td colspan="3" style="color: red">
                                        <input type="checkbox" name="isSquare" <?php echo $profile["isSquare"] == "Y" ? "checked" : "" ?> id="isSquare" value="Y">
                                        &nbsp;
                                        Your Selection will display is square? in packing slip and quotations 
                                    </td>
                                </tr>
                                <tr style="background-color: white">
                                    <td colspan="3">Square</td>
                                </tr>

                                <tr>
                                    <td colspan="3" style="color: red">
                                        <input type="checkbox" name="hasTax" <?php echo $profile["hasTax"] == "Y" ? "checked" : "" ?> id="hasTax" value="Y">
                                        &nbsp;
                                        Your Selection will display tax information in packing slip and quotations 
                                    </td>
                                </tr>
                                <tr style="background-color: white">
                                    <td>Tax Name</td>
                                    <td>Tax Value</td>
                                    <td>Tax Price</td>
                                </tr>
                                <tr>
                                    <td colspan="3"  style="color: red">
                                        <input type="checkbox" name="hasQuot"  <?php echo $profile["hasQuot"] == "Y" ? "checked" : "" ?>  id="hasQuot"  value="Y">
                                        Your Selection will display below information in packing slip and quotation
                                    </td>
                                </tr>
                                <tr style="background-color: white">
                                    <td>Cut Tape Charge</td>
                                    <td>Cut Tape Cost</td>
                                    <td>Total Price</td>
                                </tr>
                                <tr >
                                    <td colspan="3" style="color: red">
                                        <input type="checkbox"  name="islam" <?php echo $profile["islam"] == "Y" ? "checked" : "" ?> value="Y">
                                        &nbsp;
                                        Your Selection will generate Layout for packing slip 
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                        <br/>
                        <table class="display nowrap sortable" id="dbexport" style="width: 500px;" border="1" >
                            <thead>
                                <tr>
                                    <td style="width: 40%">Code</td>
                                    <td style="width: 40%">Value</td>
                                    <td style="width: 2%"><a class="icon-plus" id="icon-plus1" href="#"   ></a></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $index = 1;
                                foreach ($userstring as $value) {
                                    ?>
                                    <tr>
                                        <td><input type="text" name="userstring[]" id="userstring<?php echo $index ?>" value="<?php echo $value["code"] ?>"></td>
                                        <td><input type="text" name="stringvalues[]" id="stringvalues<?php echo $index ?>" value="<?php echo $value["name"] ?>"></td>
                                        <td><a href="#" class="icon-trash" id="icon-trash1"  onclick="clearRow('userstring<?php echo $index ?>', 'stringvalues<?php echo $index ?>')"></a></td>
                                    </tr>
                                    <?php
                                    $index++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td><input type="submit" name="btnSubmit" id="btnSubmit" value="SAVE "  class="btn btn-info"></td>
                    <td><a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a></td>
                </tr>
            </table>
        </div>
    </div>
</form> 
<script>
    function blink_text() {
        $('.blink').fadeOut(500);
        $('.blink').fadeIn(500);
    }
    setInterval(blink_text, 3000);

    function clearRow(userstring, stringvalues) {
        $("#" + userstring).val("");
        $("#" + stringvalues).val("");
    }

    jQuery(function () {
        var counter = <?php echo count($resultset) + 1 ?>;
        jQuery('#icon-plus').click(function (event) {
            event.preventDefault();
            var newRow = jQuery('<tr>'
                    + ' <td style="text-align: center">' + counter + '</td>'
                    + ' <td style="width: 45%"><input type="text" name="labelField' + counter + '"></td>'
                    + ' <td style="width: 48%">' + buildRow(counter) + '</td>'
                    + '<td><a class="icon-trash" href="#"  ></a></td>'
                    + '</tr>');
            counter++;
            jQuery('#addrow').append(newRow);
        });
    });

    $(document).ready(function () {
        $("#addrow").on('click', 'a.icon-trash', function () {
            $(this).closest('tr').remove();
        });

        $('.black').hide();

    });

    jQuery(function () {
        var counter = <?php echo count($userstring) + 1 ?>;
        jQuery('#icon-plus1').click(function (event) {
            event.preventDefault();
            var newRow = jQuery('<tr>'
                    + ' <td style="width: 45%"><input type="text" name="userstring[]"></td>'
                    + ' <td style="width: 48%"><input type="text" name="stringvalues[]"></td>'
                    + '<td><a class="icon-trash" id="icon-trash1" href="#"  ></a></td>'
                    + '</tr>');
            counter++;
            jQuery('#dbexport').append(newRow);
        });
    });

    $(document).ready(function () {
        $("#dbexport").on('click', '#icon-trash1', function () {
            $(this).closest('tr').remove();
        });

        $('.black').hide();

    });

    $("#portfolio_name").click(function () {
        var valueModel = $("#portfolio_name").val();
        if (valueModel === "1") {
            $('#profilemodel').modal('show');
        }
    });
    $("#cancelportfolio_name").click(function () {
        $("#portfolio_name").val("");
    });

    function buildRow(counter) {
        var tr = "<select name='labelstep" + counter + "'><option>SKIP</option>";
<?php
for ($indexp = 1; $indexp <= 20; $indexp++) {
    ?>
            tr = tr + "<option value='<?php echo $indexp ?>'><?php echo $indexp ?></option>";
    <?php
}
?>
        tr = tr + "</select>";
        return tr;
    }
</script>
<div id="profilemodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <form class="form-horizontal" method="post"  id="portfolio_name" novalidate="novalidate">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">×</button>
            <h3>ADD CATEGORY NAME</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" id="portfolio_name" name="portfolio_name">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>Category Name</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="categoryname" id="categoryname" required="true" ></td>

                </tr>
            </table>
        </div>
        <div class="modal-footer"> 
            <input type="submit" name="btnSubmitCategory" id="btnSubmitCategory" value="SAVE" class="btn btn-info">
            <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
        </div>
    </form>
</div>
<?php

function saveOrEditProfiles($userstring) {
    $filterarray = filter_input_array(INPUT_POST);
    $getid = filter_input(INPUT_GET, "id");
    $operation = filter_input(INPUT_GET, "operation");


    if (trim($filterarray["btnSubmit"]) == "SAVE") {
        unset($filterarray["btnSubmit"]);

        $arrportfolioprofile = array();
        $arrportfolioprofile["portfolio_name"] = $filterarray["portfolio_name"];
        $arrportfolioprofile["profile_name"] = $filterarray["profile_name"];
        $arrportfolioprofile["profile_type"] = $filterarray["profile_type"];
        $arrportfolioprofile["baseprice"] = $filterarray["baseprice"];
        $arrportfolioprofile["hasTax"] = $filterarray["hasTax"] == "" ? "N" : "Y";
        $arrportfolioprofile["hasQuot"] = $filterarray["hasQuot"] == "" ? "N" : "Y";
        $arrportfolioprofile["isSquare"] = $filterarray["isSquare"] == "" ? "N" : "Y";
        $arrportfolioprofile["islam"] = $filterarray["islam"] == "" ? "N" : "Y";

        $arrportfolioprofile["uservariablestring"] = implode(";", $filterarray["userstring"]);
        $arrportfolioprofile["userdescriptionstring"] = implode(";", $filterarray["stringvalues"]);


        $mincount = count($userstring);
        $maxcount = count($filterarray["userstring"]);
        for ($index = $mincount; $index < $maxcount; $index++) {
            $arrayof = array();
            $arrayof["type"] = "userstringvalue";
            $arrayof["code"] = $filterarray["userstring"][$index];
            $arrayof["name"] = $filterarray["stringvalues"][$index];
            $arrayof["active"] = "Y";
            MysqlConnection::insert("pvcgeneric_entry", $arrayof);
        }
        if (isset($getid) && trim($getid) != "" && $operation == "") {
            $ppprimary = $getid;
            MysqlConnection::edit("tbl_portfolioprofile", $arrportfolioprofile, " id =  '$getid' ");
            MysqlConnection::executeQuery("DELETE FROM tbl_mapping_profile_label WHERE ppprimary = '$getid' ");
        } else {

            $ppprimary = MysqlConnection::insert("tbl_portfolioprofile", $arrportfolioprofile);
        }

        for ($ppindex = 1; $ppindex <= 20; $ppindex++) {
            $arrmpl = array();
            $arrmpl["ppprimary"] = $ppprimary;
            $arrmpl["profilename"] = $arrportfolioprofile["portfolio_name"];
            $arrmpl["profilelabel"] = $filterarray["labelField$ppindex"];
            $arrmpl["profilecounter"] = $filterarray["labelstep$ppindex"];
            if (trim($arrmpl["profilelabel"]) != "") {
                MysqlConnection::insert("tbl_mapping_profile_label", $arrmpl);
            }
        }
        header("location:index.php?pagename=manage_profilemaster");
    }
}
