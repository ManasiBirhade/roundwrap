<?php

$flag = filter_input(INPUT_GET, "flag");
$id = filter_input(INPUT_GET, "id");
$portfolioid = filter_input(INPUT_GET, "id");

$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit)) {
    $profileimport = $_FILES["profileimport"];
    $_SESSION["profileimport"] = $filepath = MysqlConnection::uploadFile($_FILES["profileimport"], "profilemaster/import/");
    $exportdata = getExcelData($filepath);
    processData($exportdata);
    header("location:index.php?pagename=mapping_profilemaster");
}

$arraycategory = array();
$arraycategory["laminate"] = "Laminate";
$arraycategory["PVC"] = "PVC";
$arraycategory["Veneer"] = "Veneer";
$arraycategory["Acrylic"] = "Acrylic";
?>
<style>
    input,textarea,select,date{ width: 90%;height: 28px; }
    input[type="text"]{ height: 24px; width: 89%;}
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    table.sortable tr td{
        padding: 5px;
    }
</style>

<form  method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="container-fluid" style="" >
        <div class="cutomheader">
            <h5 style="font-family: verdana;font-size: 12px;">PROFILE MANAGEMENT</h5>
        </div>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">IMPORT PROFILE</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 50%">
                <tr>
                    <td>
                        <fieldset  class="well the-fieldset">
                            <table id="addlabel" style="width: 80%" class="display nowrap sortable" > 
                                <tr>
                                    <td><label class="control-label">Upload&nbsp;:</label></td>
                                    <td><input type="file" style="width: 90%" required="" maxlength="20" minlength="2" name="profileimport" id="profileimport"/></td>
                                </tr>
                            </table> 
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td><input type="submit" name="btnSubmit" id="btnSubmit" value="UPLOAD "  class="btn btn-info"></td>
                    <td><a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a></td>
                </tr>
            </table>
        </div>
    </div>
</form> 
<?php

function getExcelData($excelpath) {
    try {
        $spreadsheet = new SpreadsheetReader($excelpath);
        $sheets = $spreadsheet->Sheets();
        $arrayextracteddata = array();
        $header = array();
        $data = array();
        $resetcounter = 0;
        foreach ($sheets as $index => $name) {
            $spreadsheet->ChangeSheet($index);
            $indexfor = 0;
            foreach ($spreadsheet as $key => $row) {
                if ($indexfor == 0) {
                    array_push($header, $row);
                } else {
                    if ($row[0] != "") {
                        $escapedata = array();
                        for ($expindex = 0; $expindex < count($row); $expindex++) {
                            array_push($escapedata, addslashes($row[$expindex]));
                        }
                        $escapedata[count($escapedata) + 1] = md5((time() * rand(1000, 9999) * ( $resetcounter + $indexfor)));
                        $string = "'" . implode("','", $escapedata) . "'";
                        array_push($data, $string);
                    }
                }
                $indexfor++;
                $resetcounter++;
            }
        }
        array_push($arrayextracteddata, $header);
        array_push($arrayextracteddata, $data);
    } catch (Exception $E) {
        //echo $E->getMessage();
    }
    return $arrayextracteddata;
}

function processData($exportdata) {
    $header = $exportdata[0];
    $data = $exportdata[1];
    $datacount = count($data);
    $bachsize = round($datacount / 1000) + 1;
    $bach = array();
    for ($import = 0; $import < $bachsize; $import++) {
        $bachdata = array();
        $batchminindex = 1000 * $import;
        $batchmaxindex = 1000 + $batchminindex;
        for ($batchindex = $batchminindex; $batchindex < $batchmaxindex; $batchindex++) {
            if (count($data[$batchindex]) != 0) {
                if ($data[$batchindex][0] != "") {
                    array_push($bachdata, "(" . $data[$batchindex] . ")");
                }
            }
        }
        $impload = implode(",", $bachdata);
        $sqlquery = "INSERT INTO `tbl_profilelabel` (`color`, `ccost`, `finish`, `fcost`, `backing`, `bcost`, `board_thickness`, `btcost`, `bin_number`, `bncost`, `edge_tape_color`, `etccost`, `edge_tape_thickness`, `ettcost`, `grain_direction`, `gdcost`, `blank_material`, `bmcost`,`id`) VALUES " . $impload;
        MysqlConnection::executeQuery($sqlquery);
    }
}
?>