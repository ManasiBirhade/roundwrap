<?php
$listofprofiles = MysqlConnection::fetchCustom("SELECT * FROM `tbl_portfolioprofile` ORDER BY `portfolio_name` ASC LIMIT 0,10");
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PROFILE LIST</h5>
    </div>
    <br/>
    <table  style="width: 100%">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=create_profilemaster"  data-toggle="modal">ADD PROFILE</a>
                &nbsp;
                <a class="btn btn-info" href="index.php?pagename=import_profilemaster"  data-toggle="modal">IMPORT DATA</a>
            </td>

        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Category Name</td>
                <td>Profile</td>
                <td>Type</td>
                <td>Label Values</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listofprofiles as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $arrlabels = MysqlConnection::getPortfolioProfileByProfId($value["id"]);
                $imparrlabels = array();
                foreach ($arrlabels as $mappingvalue) {
                    array_push($imparrlabels, $mappingvalue["profilelabel"]);
                }
                ?>
                <tr id="<?php echo $value["id"] ?>" style="background-color: <?php echo $bgcolor ?>;" class="context-menu-one">
                    <td><?php echo $index ?></td>
                    <td><?php echo $value["portfolio_name"] ?></td>
                    <td><?php echo $value["profile_name"] ?></td>
                    <td><?php echo $value["profile_type"] ?></td>
                    <td><?php echo implode(" , ", $imparrlabels) ?></td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var profile = $(this).attr('id');
                switch (key) {
                    case "add_profile":
                        $("#addData").modal();
                        break;
                    case "edit_profile":
                        window.location = "index.php?pagename=create_profilemaster&id=" + profile;
                        break;
                    case "copy_profile":
                        window.location = "index.php?pagename=create_profilemaster&id=" + profile+"&operation=copy";
                        break;
                    case "view_profile":
                        window.location = "index.php?pagename=view_profilemaster&id=" + profile;
                        break;
                    case "delete_profile":
                        window.location = "index.php?pagename=view_profilemaster&id=" + profile + "&flag=yes";
                        break;
                    case "print_profile":
                        window.location = "index.php?pagename=print_profile&id=" + profile;
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    default:
                        window.location = "index.php?pagename=manage_profilemaster";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "edit_profile": {name: "EDIT PROFILE ", icon: ""},
                "copy_profile": {name: "COPY PROFILE ", icon: ""},
                "view_profile": {name: "VIEW PROFILE", icon: ""},
                "delete_profile": {name: "DELETE PROFILE", icon: ""}
            }
        });

        $('tr').dblclick(function () {
            var id = $(this).attr('id');
            if (id !== undefined) {
                window.location = "index.php?pagename=view_profilemaster&id=" + id;
            }
        });
    });

    $(function () {

        $("#portfolio_name").change(function () {
            var status = this.value;
            if (status === "1") {
                $("#profileSelection2").show();
            } else if (status === "0" || status === "") {
            } else {
                $("#profileSelection2").hide();
            }
        });

    });
</script>