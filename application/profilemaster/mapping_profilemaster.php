<?php
$flag = filter_input(INPUT_GET, "flag");
$id = filter_input(INPUT_GET, "id");
$portfolioid = filter_input(INPUT_GET, "id");

$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit)) {
    saveOrEditProfiles(filter_input_array(INPUT_POST));
//    header("location:index.php?pagename=manage_profilemaster");
} else {
    if (trim($portfolioid) != "") {
        $portfolio = MysqlConnection::getPortfolioProfileByProfId($portfolioid);
        $profilelabelarray = MysqlConnection::getPortfolioLabelsByProfId($portfolioid);
    }
}

$arraycategory = array();
$arraycategory["laminate"] = "Laminate";
$arraycategory["PVC"] = "PVC";
$arraycategory["Veneer"] = "Veneer";
$arraycategory["Acrylic"] = "Acrylic";

$lablesvalues = array();
$lablesvalues["laminateBrand"] = "Brand";
$lablesvalues["laminateColor"] = "Color";
$lablesvalues["finish"] = "Finish";
$lablesvalues["backing"] = "Backing";
$lablesvalues["boardThickness"] = "Board Thickness";
$lablesvalues["binNumber"] = "Bin Number";
$lablesvalues["edgeTapecolor"] = "Edge Tape Color";
$lablesvalues["edgeTapethickness"] = "Edge Tapethickness";
$lablesvalues["grainDirection"] = "Grain Direction";
$lablesvalues["blankMaterial"] = "Blank Material";
?>
<style>
    input,textarea,select,date{ width: 90%;height: 28px; }
    input[type="text"]{ height: 24px; width: 89%;}
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    table.sortable tr td{
        padding: 5px;
    }
</style>

<form  method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="container-fluid" style="" >
        <div class="cutomheader">
            <h5 style="font-family: verdana;font-size: 12px;">PROFILE MANAGEMENT</h5>
        </div>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANAGE PROFILE</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 50%">
                <tr>
                    <td>
                        <fieldset  class="well the-fieldset">
                            <table id="addlabel" style="width: 80%" class="display nowrap sortable" > 
                                <tr>
                                    <td><label class="control-label">Category&nbsp;:</label></td>
                                    <td>
                                        <select name="portfolio_name" id="portfolio_name" style="width: 92%">
                                            <option></option>
                                            <?php
                                            foreach ($arraycategory as $value) {
                                                ?>
                                                <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label class="control-label">Excel Profile Name&nbsp;:</label></td>
                                    <td><input type="text" style="width: 90%" required="" maxlength="20" minlength="2"  value="<?php echo $portfolio["profile_name"] ?>" name="profile_name" id="profile_name"/></td>
                                </tr>
                                <tr>
                                    <td><label class="control-label">Upload&nbsp;:</label></td>
                                    <td><input type="file" style="width: 90%" required="" maxlength="20" minlength="2"  value="<?php echo $portfolio["profile_name"] ?>" name="profile_name" id="profile_name"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table class="display nowrap sortable" style="width: 100%" border="1">
                                            <thead>
                                                <tr>
                                                    <td>#</td>
                                                    <td>Label</td>
                                                    <td>Display Order</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $index = 1;
                                                foreach ($lablesvalues as $value) {
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center"><?php echo $index ?></td>
                                                        <td style="width: 45%"><?php echo $value ?></td>
                                                        <td style="width: 48%">
                                                            <select name="profileselection<?php echo $index ?>">
                                                                <option value="0">SKIP</option>
                                                                <?php for ($indexp = 1; $indexp <= count($lablesvalues); $indexp++) { ?>
                                                                    <option value="<?php echo $indexp ?>"><?php echo $indexp ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $index++;
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table> 
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td><input type="submit" name="btnSubmit" id="btnSubmit" value="SAVE / UPDATE"  class="btn btn-info"></td>
                    <td><a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a></td>
                </tr>
            </table>
        </div>
    </div>
</form> 

<?php

function saveOrEditProfiles($filterarray) {
    $getid = filter_input(INPUT_GET, "id");
    unset($filterarray["btnSubmit"]);

    $arraymaster["portfolio_name"] = $filterarray["portfolio_name"];
    $arraymaster["profile_name"] = $filterarray["profile_name"];
    if ($_FILES["profilepic"]["name"] != "") {
        $arraymaster["imgpath"] = MysqlConnection::uploadFile($_FILES["profilepic"], "profileimages/");
    }
    if (isset($getid)) {
        MysqlConnection::edit("tbl_portfolioprofile", $arraymaster, " id = '$getid'");
    } else {
        $portfolioid = MysqlConnection::insert("tbl_portfolioprofile", $arraymaster);
    }

    $arrlabelname = $filterarray["label_name"];
    $arrlabelvalue = $filterarray["label_value"];
    $arrrate = $filterarray["rate"];

    if (trim($getid) != "") {
        MysqlConnection::delete("DELETE FROM tbl_profilelabel WHERE portfolio_id = '$getid' ");
    }

    for ($index = 0; $index <= count($arrlabelname); $index++) {
        if (isset($getid)) {
            $arrayvalues["portfolio_id"] = $getid;
        } else {
            $arrayvalues["portfolio_id"] = $portfolioid;
        }
        $arrayvalues["label_name"] = $arrlabelname[$index];
        $arrayvalues["label_value"] = $arrlabelvalue[$index];
        $arrayvalues["rate"] = $arrrate[$index];
        if (trim($arrayvalues["label_name"]) != "") {
            MysqlConnection::insert("tbl_profilelabel", $arrayvalues);
        }
    }
}
?>
