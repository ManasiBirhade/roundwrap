<?php
error_reporting(0);
include '../MysqlConnection.php';

$profilename = $_POST["id"];
$profilelabelarray = MysqlConnection::getPortfolioProfileByProfId($profilename);
$array = json_decode($profilelabelarray["mapping"]);

$filterarray = array();

$labels = array();
foreach ($array as $lebelsvalue) {
    $expleb = explode("@", $lebelsvalue);
    array_push($labels, $expleb[0]);
}


$data = array();
$data["color"] = pimplode(MysqlConnection::fetchCustom("SELECT `color` as value,`ccost` as cost FROM `tbl_profilelabel` ORDER BY color"));
$data["finish"] = pimplode(MysqlConnection::fetchCustom("SELECT `finish` as value,`fcost`  as cost FROM `tbl_profilelabel` ORDER BY finish"));
$data["backing"] = pimplode(MysqlConnection::fetchCustom("SELECT `backing` as value,`bcost` as cost FROM `tbl_profilelabel` ORDER BY backing"));
$data["board_thickness"] = pimplode(MysqlConnection::fetchCustom("SELECT `board_thickness` as value,`btcost`  as cost FROM `tbl_profilelabel` ORDER BY board_thickness"));
$data["bin_number"] = pimplode(MysqlConnection::fetchCustom("SELECT `bin_number`  as value,`bncost`  as cost  FROM `tbl_profilelabel` ORDER BY bin_number"));
$data["edge_tape_color"] = pimplode(MysqlConnection::fetchCustom("SELECT `edge_tape_color` as value,`etccost`  as cost FROM `tbl_profilelabel` ORDER BY edge_tape_color"));
$data["edge_tape_thickness"] = pimplode(MysqlConnection::fetchCustom("SELECT `edge_tape_thickness` as value,`ettcost`  as cost FROM `tbl_profilelabel` ORDER BY edge_tape_thickness"));
$data["grain_direction"] = pimplode(MysqlConnection::fetchCustom("SELECT `grain_direction` as value,`gdcost` as cost  FROM `tbl_profilelabel` ORDER BY grain_direction"));
$data["blank_material"] = pimplode(MysqlConnection::fetchCustom("SELECT `blank_material` as value,`bmcost`  as cost FROM `tbl_profilelabel` ORDER BY blank_material"));

function pimplode($resultset) {
    $array = array();
    foreach ($resultset as $value) {
        if ($value["value"] != "") {
            array_push($array, $value["value"] . " -- " . $value["cost"]);
        } else {
            array_push($array, $value["value"]);
        }
    }
    return implode(",", $array);
}
?>
<table class="table-bordered" id="profiletable" style="width: 100%;border-collapse: collapse;background-color: white;margin-top: -1px;" border="1">
    <input type="hidden" name="baseprice" id="baseprice" value="<?php echo $profilelabelarray["baseprice"] ?>"/>
    <input type="hidden" name="profilelabelcount" id="profilelabelcount" value="<?php echo count($data) ?>"/>
    <?php
    $indexp = 1;
    foreach ($data as $key => $value) {
        if (in_array($key, $labels)) {
            ?>
            <tr style="font-weight: bold;font-family: 'Calibri';">
                <th style="width: 150px;text-align: left">
                    <input type="hidden" name="profilelabel[]" value="<?php echo $key ?>"> 
                    &nbsp;&nbsp;<?php echo ucwords(str_replace("_", " ", $key)) ?> 
                </th>
                <th>
                    <?php
                    if ($value["value"] == "") {
                        ?>
                        <input type="text" name="profilelabelvalues[]" id="profilelabelvalues<?php echo $indexp ?>"  style="margin-top: 5px;width: 215px">
                        <?php
                    } else {
                        $profiledata = explode(",", $value);
                        ?>
                        <select name="profilelabelvalues[]" id="profilelabelvalues<?php echo $indexp ?>"   onchange="calculateProfileCost()" style="margin-top: 5px;width: 220px;font-family: 'Calibri';">
                            <option value="">PLEASE SELECT</option>
                            <?php
                            foreach ($profiledata as $datavalue) {
                                if (trim($datavalue) != "") {
                                    ?>
                                    <option value="<?php echo $datavalue ?>"><?php echo ucwords($datavalue) ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                        <input type="hidden" name="profilelablevalues[]" id="profilelablevalues[]" value="15.0">
                        <?php
                    }
                    ?>
                </th>
            </tr>
            <?php
            $indexp++;
        }
    }
    ?>
</table>