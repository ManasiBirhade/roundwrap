<?php
$flag = filter_input(INPUT_GET, "flag");
$portfolioid = filter_input(INPUT_GET, "id");

$portfolio = MysqlConnection::getPortfolioProfileById($portfolioid);
$mapping = MysqlConnection::getPortfolioProfileByProfId($portfolioid);

if (filter_input(INPUT_POST, "deleteItem") == "DELETE") {
    MysqlConnection::delete("DELETE FROM `tbl_portfolioprofile` WHERE id = '$portfolioid'");
    MysqlConnection::delete("DELETE FROM `tbl_mapping_profile_label` WHERE ppprimary = '$portfolioid'");
    header("location:index.php?pagename=manage_profilemaster");
}
?>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form   method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">VIEW PROFILE</a></li>
                </ul>
            </div>

            <?php
            if ($flag == "yes") {
                echo MysqlConnection::$DELETE;
            }
            ?>
            <br/>
            <table id="addlabel" class="display nowrap sortable table-bordered" style="margin: 10px;background-color: white">
                <tr>
                    <td><label class="control-label"><b>Category Name&nbsp;:</b></label></td>
                    <td><?php echo $portfolio["portfolio_name"] ?></td>
                </tr>
                <tr>
                    <td><label class="control-label"><b>Profile Name&nbsp;:</b></label></td>
                    <td><?php echo $portfolio["profile_name"] ?></td>
                </tr>
                <?php
                $index = 1;
                foreach ($mapping as $key => $value) {
                    ?>
                    <tr>
                        <td><label class="control-label"><?php echo $value["profilelabel"]?></label></td>
                        <td><?php echo $value["profilecounter"]?></td>
                    </tr>
                    <?php
                }
                ?>
            </table> 
            <div class="modal-footer " style="text-align: center">
                <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                <?php
                if ($flag != "") {
                    ?>
                    <input type="submit" name="deleteItem" value="DELETE"  class="btn btn-info" />
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</form>