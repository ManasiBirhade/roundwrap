<?php
$profileid = filter_input(INPUT_GET, "profileid");
$counterc = filter_input(INPUT_GET, "counter");
$status = filter_input(INPUT_GET, "status");

$arraycategory = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'productioncategory' ORDER BY `indexid`");
$profilename = filter_input(INPUT_GET, "profilename");

$profilelabels = MysqlConnection::getlabelsandpriceByProfId($profileid);
$profilelabelsmaster = MysqlConnection::getPortfolioProfileById($profileid);

$postedarray = filter_input_array(INPUT_POST);
$profileprice = MysqlConnection::getProfilePrice($profilename);

$arraykeys = array();
foreach ($profilelabels as $arrvalue) {
    array_push($arraykeys, $arrvalue["profilelabel"]);
}

$portfolioid = $profilelabelsmaster["id"];
$profilenamefetch = $profilelabelsmaster["profile_name"];
$profiletype = $profilelabelsmaster["profile_type"];

$count = getCount($portfolioid);

if ($postedarray["btnImportProfile"] == "CSV Import") {
    $file = fopen($_FILES["inputFile"]["tmp_name"], 'r');
    $indexof = 0;
    $profileidexcel = "";
    $basearray = array();
    $buildarraymaster = array();
    while (($line = fgetcsv($file)) !== FALSE) {
        //$line is an array of the csv elements
        if ($indexof == 0) {
            $basearray = $line;
        } else {
            for ($rindex = 3; $rindex <= count($line); $rindex++) {
                if ($line[$rindex] != "") {
                    $buildarray[$basearray[$rindex]] = $line[$rindex];
                }
            }
            array_push($buildarraymaster, $buildarray);
        }
        $indexof++;
    }
    $iiindex = 0;
    foreach ($buildarraymaster as $key => $value) {
        $time = time();
        $counter = $iiindex + 1 + $count;
        foreach ($value as $key1 => $value1) {
            if ($key1 != "PRICE") {
                $dataarray = array();
                $dataarray["portfolio_id"] = $basearray[0];
                $dataarray["profileTypeValue"] = $postedarray["profileTypeValue"];
                $dataarray["forrow"] = $time + 1 + $iiindex;
                $dataarray["portfolio_name"] = strtolower($basearray[2]);
                $dataarray["portfolio_label"] = strtolower($key1);
                $dataarray["counter"] = $counter;
                $dataarray["profile_label_name"] = $value1;
                $dataarray["profile_label_price"] = $value["PRICE"];
                MysqlConnection::insert("profile_price", $dataarray);
            }
        }
        $iiindex++;
    }
    fclose($file);
}


if ($postedarray["btnSubmit"] == "SAVE") {
    foreach ($arraykeys as $v) { //columns wise 
        for ($index = 0; $index <= count($postedarray[str_replace(" ", "_", $v)]); $index++) { /// row cell wise 
            $time = time();
            $arrayprice["forrow"] = $time + $index + 1 + $count;
            $arrayprice["counter"] = $index + 1 + $count;  //1 .. 2... 
            $arrayprice["portfolio_id"] = $portfolioid;
            $arrayprice["profileTypeValue"] = $postedarray["profileTypeValue"];
            $arrayprice["portfolio_name"] = strtolower($profilenamefetch);
            $arrayprice["portfolio_label"] = strtolower($v);
            $arrayprice["profile_label_name"] = $profilelabelname = $postedarray[str_replace(" ", "_", $v)][$index];
            $arrayprice["profile_label_price"] = $profilelabelprice = $postedarray["profile_label_price"][$index];
            if ($profilelabelprice != "" && $profilelabelname != "") {
                MysqlConnection::insert("profile_price", $arrayprice);
            }
        }
    }

//    header("location:index.php?pagename=add_profileprice&profileid=$portfolioid");
}
if ($counterc != "") {
    $datafromcounter = getDataForCopy($profileid, $counterc);
}

if ($status == "delete") {
    MysqlConnection::delete("DELETE FROM profile_price WHERE portfolio_id = '$profileid' AND counter = $counterc ");
    header("location:index.php?pagename=add_profileprice&profileid=$portfolioid");
}
?>

<style>
    input,textarea,select,date{ width: 90%;height: 28px; }
    input[type="text"]{ height: 24px; width: 89%;}
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    table.sortable tr td{
        padding: 5px;
    }
    .txtInput{
        width: 320px;
        background-color: white;
        text-transform: uppercase;
        font-weight: bold;
    }

</style>

<form  method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="container-fluid" style="" >
        <div class="cutomheader">
            <h5 style="font-family: verdana;font-size: 12px;">PROFILE LABEL PRICE MANAGEMENT</h5>
        </div>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANAGE LABEL PRICE </a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <fieldset  class="well the-fieldset">
                            <table id="addlabel" style="width: 40%" class="display nowrap sortable" > 
                                <tr>
                                    <td><label class="control-label">CATEGORY&nbsp;:</label></td>
                                    <td>
                                        <input type="text" readonly="" value="<?php echo $profilelabelsmaster["portfolio_name"] ?>" class="txtInput" style="background-color: white"/>
                                        <input type="hidden" name="portfolio_id" value="<?php echo $profileid ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label class="control-label">PROFILE&nbsp;:</label></td>
                                    <td><input type="text" readonly="" value="<?php echo $profilenamefetch ?>"  class="txtInput" readonly="" style="background-color: white"/></td>
                                </tr>
                                <tr>
                                    <td><label class="control-label">TYPE&nbsp;:</label></td>
                                    <td><input autocomplete="" autofocus="" required="" minlength="2" maxlength="25" type="text" readonly="" value="<?php echo $profiletype ?>" placeholder="Enter values like Door / Door Front / Drawer" name="profileTypeValue" id="profileTypeValue"  class="txtInput" style="background-color: white"/></td>
                                </tr>
                            </table>
                            <br/>
                            <b>OR</b>
                            <br/>
                            <br/>
                            <table id="addlabel" style="width: 40%" class="display nowrap sortable" > 
                                <tr>
                                    <td ><label class="control-label">CATEGORY&nbsp;:</label></td>
                                    <td>
                                        <input type="file" name="inputFile"  id="inputFile" style="width: 70%;border: solid 1px rgb(200,200,200);"/>
                                        <input type="submit"  class="btn btn-info" name="btnImportProfile" id="btnImportProfile" value="CSV Import"/>
                                    </td>
                                </tr>
                            </table>
                            <br/>
                            <table id="addlabel" style="width: 100%" class="display nowrap sortable" > 
                                <tr>
                                    <td colspan="2">
                                        <table class="display nowrap sortable" id="addrow" style="width: 100%" border="1">
                                            <thead>
                                                <tr>
                                                    <td style="width: 20px">#</td>
                                                    <?php foreach ($profilelabels as $key => $value) { ?>
                                                        <td ><?php echo $value["profilelabel"] ?></td>
                                                    <?php } ?>
                                                    <td>PRICE</td>
                                                    <td><a class="icon-plus" href="#" id="icon-plus"  ></a></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td style="text-align: center;width:20px">1</td>
                                                    <?php foreach ($profilelabels as $key => $value) { ?>
                                                        <td style=""><input type="text" name="<?php echo $value["profilelabel"] ?>[]" value="<?php echo $datafromcounter[strtolower($value["profilelabel"])] ?>" ></td>
                                                    <?php } ?>
                                                    <td><input type="text" maxlength="6" name="profile_label_price[]"  value="<?php echo $datafromcounter["profile_label_price"] ?>" ></td>
                                                    <td><a class="icon-trash" id="icon-trash" href="#"  ></a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table border="0" style="float: right">
                                            <tr>
                                                <td><input type="submit" name="btnSubmit" id="btnSubmit" value="SAVE"  class="btn btn-info"></td>
                                                <td><a href="index.php?pagename=add_profileprice&profileid=<?php echo $profileid ?>" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a></td>
                                                <td><a href="index.php?pagename=manage_profileprice" id="btnSubmitFullForm" class="btn btn-info">BACK</a></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table> 
                            <br/>
                            <table id="addlabel" style="width: 100%" class="display nowrap sortable" > 
                                <tr>
                                    <td colspan="2">
                                        <?php
                                        $counter = getCounter($profileid);
                                        $metadata = getMetadata($profileid);
                                        ?>
                                        <table class="display nowrap sortable" id="addrow1" style="width: 100%" border="1">
                                            <thead>
                                                <tr>
                                                    <td>Type</td>
                                                    <?php foreach ($profilelabels as $key => $value) { ?>
                                                        <td ><?php echo $value["profilelabel"] ?></td>
                                                    <?php } ?>
                                                    <td>PRICE</td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                for ($index = 1; $index <= $counter; $index++) {
                                                    if ($metadata["profile_label_price" . $index] != "") {
                                                        ?>
                                                        <tr>
                                                            <td> <?php echo $metadata["profileTypeValue" . $index] ?></td>
                                                            <?php
                                                            foreach ($profilelabels as $key => $value) {
                                                                ?>
                                                                <td >
                                                                    <?php echo $metadata[strtolower($value["profilelabel"]) . $index] ?>
                                                                </td>
                                                                <?php
                                                            }
                                                            ?>
                                                            <td> <?php echo $metadata["profile_label_price" . $index] ?></td>
                                                            <td style="text-align: center">
                                                                <a data-toggle="tooltip" data-original-title="Copy"  href="index.php?pagename=add_profileprice&profileid=<?php echo $profileid ?>&counter=<?php echo $index ?>"class=" icon-upload"></a>
                                                                <a style="margin-left: 15px"  data-toggle="tooltip" data-original-title="Delete"  href="index.php?pagename=add_profileprice&profileid=<?php echo $profileid ?>&counter=<?php echo $index ?>&status=delete" onclick="confirm_delete();" class="icon-trash" id="icon-trash"></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table> 
                        </fieldset>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</form> 
<script>
    $(document).ready(function () {
        $("#addrow1").on('click', '#icon-trash', function () {
            $(this).closest('tr').remove();
        });
    });

    function confirm_delete() {
        var txt;
        var r = confirm("Are you sure you want to delete?");
        if (r === true) {
            txt = "You pressed OK!";
        } else {
            txt = "You pressed Cancel!";
            event.preventDefault();
        }
        console.log(txt);
    }

    jQuery(function () {
        var counter = 2;
        var rowcount = <?php echo count($profilelabels) ?>;
        jQuery('#icon-plus').click(function (event) {
            event.preventDefault();
            var newRow = jQuery('<tr>'
                    +' <td style="text-align: center">' + counter + '</td>'
<?php foreach ($profilelabels as $key => $value) { ?>

                + ' <td ><input type="text" name="<?php echo $value["profilelabel"] ?>[]"  ></td>'
<?php } ?>

            + ' <td ><input type="text" maxlength="6" name="profile_label_price[]"></td>'
                    + '<td><a class="icon-trash" href="#"  ></a></td>'
            + '</tr>'
                    );
            counter++;
            jQuery('#addrow').append(newRow);
        });
    });

    $(document).ready(function () {
        $("#addrow").on('click', 'a.icon-trash', function () {
            $(this).closest('tr').remove();
        });

        $('.black').hide();

    });


    $("#portfolio_name").click(function () {
        var valueModel = $("#portfolio_name").val();
        if (valueModel === "1") {
            $('#profilemodel').modal('show');
        }
    });
    $("#cancelportfolio_name").click(function () {
        $("#portfolio_name").val("");
    });

</script>
<?php

function getCounter($profileid) {
    $sqlcounter = "select DISTINCT `counter` FROM  profile_price where `portfolio_id` = '$profileid' ORDER BY `counter` DESC LIMIT 0,1 ";
    $resultsetcounter = MysqlConnection::fetchCustom($sqlcounter);
    return $resultsetcounter[0]["counter"];
}

function getMetadata($profileid) {
    $metaarray = array();
    $counterss = getCounter($profileid);
    for ($index = 1; $index <= $counterss; $index++) {
        $sql = "SELECT * FROM `profile_price` where `portfolio_id` = '$profileid' AND counter = $index  ";
        $resultset = MysqlConnection::fetchCustom($sql);
        foreach ($resultset as $valuecountes) {
            $metaarray[$valuecountes["portfolio_label"] . $index] = $valuecountes["profile_label_name"];
            $metaarray["profile_label_price" . $index] = $valuecountes["profile_label_price"];
            $metaarray["profileTypeValue" . $index] = $valuecountes["profileTypeValue"];
        }
    }
    return $metaarray;
}

function getDataForCopy($profileid, $index) {
    $sql = "SELECT * FROM `profile_price` where `portfolio_id` = '$profileid' AND counter = $index  ";
    $resultset = MysqlConnection::fetchCustom($sql);
    $array = array();
    foreach ($resultset as $value) {
        $array[$value["portfolio_label"]] = $value["profile_label_name"];
        $array["profile_label_price"] = $value["profile_label_price"];
    }
    return $array;
}

function getCount($portfolioid) {
    $sqlcounter = "SELECT DISTINCT(counter) as counter FROM `profile_price`"
            . " WHERE portfolio_id = '$portfolioid'"
            . " ORDER BY counter DESC LIMIT 0,1";
    $resultsetcounter = MysqlConnection::fetchCustom($sqlcounter);
    return $resultsetcounter[0]["counter"] + 1;
}
?>