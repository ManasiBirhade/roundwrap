<?php
$portfolios = MysqlConnection::getPortfolios();
$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");


$btnExport = filter_input(INPUT_POST, "btnExport");
$submit = filter_input(INPUT_POST, "submit");

if ($submit == "CREATE") {

    $profileid = filter_input(INPUT_POST, "profileid");
    header("location:index.php?pagename="
            . "add_profileprice&"
            . "profileid=$profileid");
}
if ($btnExport == "EXPORT") {
    $postarray = filter_input_array(INPUT_POST);
    $profileid = $postarray["profileid"];
    header("location:profileprice/export.php?profileid=" . $profileid);
}
?>
<div class="container-fluid" >
    <div class="cutomheader"><h5 style="font-family: verdana;font-size: 12px;">PROFILE PRICE</h5></div>
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
        <form name="frmItemsSubmit" id="frmItemsSubmit"  method="post" action="">
            <div class="widget-content tab-content"  style="margin: 0 auto">
                <table class="display nowrap sortable" style="width: 40%;">
                    <tr>
                        <td style="width: 25%;"><label  class="control-label">SELECT CATEGORY&nbsp;:</label></td>
                        <td style="width: 60%;">
                            <select name="profileid" id="profileid" style="width: 100%" >
                                <option>NONE</option>
                                <?php
                                foreach ($portfolios as $value) {
                                    ?>
                                    <option value="<?php echo $value["id"] ?>">
                                        <?php echo strtoupper($value["portfolio_name"] . " - " . $value["profile_name"] . " - " . $value["profile_type"]) ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                        <td><input type="submit" id="submit" name="submit"  class="btn btn-info" value="CREATE"></td>
                        <td>
                            <input type="submit" id="btnExport" name="btnExport"  class="btn btn-info" value="EXPORT">
                        </td>
                    </tr>
                </table>
            </div>  
        </form>
    </div>
</div>

<?php

function exportToCSV($profileid, $array) {
    
}
