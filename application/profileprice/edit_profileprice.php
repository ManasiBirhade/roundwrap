<?php
$id = filter_input(INPUT_GET, "id");
$rsset = MysqlConnection::fetchCustom("SELECT * FROM `profile_price` WHERE forrow = '$id' ");


$profileid = $rsset[0]["id"];
$profilelabels = MysqlConnection::getProfilePriceById($profileid);

$postedarray = filter_input_array(INPUT_POST);

if ($postedarray["btnSubmit"] == "SAVE" && isset($postedarray["btnSubmit"])) {
    unset($postedarray["btnSubmit"]);
    for ($ind = 0; $ind < count($postedarray["profile_label_name"]); $ind++) {
        $portfolio_label = $postedarray["profile_label_name"][$ind];
        $profile_label_price = $postedarray["profile_label_price"];
        $update = "UPDATE `profile_price` SET"
                . " `profile_label_name` = '$portfolio_label'"
                . ", `profile_label_price` = '$profile_label_price'"
                . " WHERE `id` = '" . $postedarray["ppid"][$ind] . "';";
        MysqlConnection::executeQuery($update);
    }
    header("location:index.php?pagename=manage_profileprice");
}
?>

<style>
    input,textarea,select,date{ width: 90%;height: 28px; }
    input[type="text"]{ height: 24px; width: 89%;}
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    table.sortable tr td{
        padding: 5px;
    }
</style>

<form  method="post" autocomplete="off" enctype="multipart/form-data">
    <div class="container-fluid" style="" >
        <div class="cutomheader">
            <h5 style="font-family: verdana;font-size: 12px;">PROFILE LABEL PRICE MANAGEMENT</h5>
        </div>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANAGE LABEL PRICE </a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 50%">
                <tr>
                    <td>
                        <fieldset  class="well the-fieldset">
                            <table id="addlabel" style="width: 80%" class="display nowrap sortable" > 
                                <tr>
                                    <td><label class="control-label">PORTFOLIO NAME&nbsp;:</label></td>
                                    <td><input type="text" value="<?php echo $profilelabels["portfolio_name"] ?>" disabled=""></td>
                                </tr>

                                <?php
                                foreach ($rsset as $value) {
                                    ?>
                                    <tr>
                                        <td><label class="control-label"><?php echo strtoupper($value["portfolio_label"]) ?>&nbsp;:</label></td>
                                        <td>
                                            <input type="text" required="" value="<?php echo strtoupper($value["profile_label_name"]) ?>" name="profile_label_name[]" >
                                            <input type="hidden" name="ppid[]" value="<?php echo $value["id"] ?>">
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td><label class="control-label">VALUE PRICE&nbsp;:</label></td>
                                    <td><input type="text" required="" value="<?php echo $rsset[0]["profile_label_price"] ?>" name="profile_label_price"></td>
                                </tr>
                            </table> 
                        </fieldset>
                    </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td><input type="submit" name="btnSubmit" id="btnSubmit" value="SAVE"  class="btn btn-info"></td>
                    <td><a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a></td>
                </tr>
            </table>
        </div>
    </div>
</form>  