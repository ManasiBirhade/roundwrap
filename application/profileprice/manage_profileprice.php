<?php
error_reporting(E_ALL);
$arrayprofileprices = MysqlConnection::getProfilePrice();

$resultset = MysqlConnection::fetchCustom("SELECT DISTINCT(`forrow`) as forrow FROM `profile_price`");
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PROFILE LABEL PRICES</h5>
    </div>
    <br/>
    <table  style="width: 100%">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=create_profileprice"  data-toggle="modal">ADD PROFILE PRICE</a></td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Profile</td>
                <td>Type</td>
                <td>Label Name</td>
                <td>Label Value</td>
                <td>Label Price</td>
            </tr>
        </thead>
        <tbody >
            <?php
            foreach ($resultset as $timevalue) {
                $value = getProfilePriceDetails($timevalue);
                ?>
                <tr id="<?php echo $value["forrow"] ?>"  class="context-menu-one">
                    <td style="width: 25px">#</td>
                    <td><?php echo $value["portfolio_name"] ?></td>
                    <td><?php echo $value["profileTypeValue"] ?></td>
                    <td><?php echo $value["portfolio_label"] ?></td>
                    <td><?php echo $value["profile_label_name"] ?></td>
                    <td><?php echo $value["profile_label_price"] ?></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var profile = $(this).attr('id');
                switch (key) {
                    case "edit_profileprice":
                        window.location = "index.php?pagename=edit_profileprice&id=" + profile;
                        break;
                }
            },
            items: {
                "edit_profileprice": {name: "EDIT PROFILE PRICE ", icon: ""},
            }
        });
    });
</script>
<?php

function getProfilePriceDetails($timevalue) {
    $forrow = $timevalue["forrow"];
    $resultset = MysqlConnection::fetchCustom("SELECT * FROM profile_price WHERE forrow = '$forrow' ORDER BY portfolio_label, profile_label_name");
    $label = array();
    $values = array();
    $row = array();
    foreach ($resultset as $value) {
        array_push($label, $value["portfolio_label"]);
        array_push($values, $value["profile_label_name"]);
        $row = $value;
    }
    $row["portfolio_label"] = implode(",", $label);
    $row["profile_label_name"] = implode(",", $values);

    return $row;
}
?>