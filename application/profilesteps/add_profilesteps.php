<?php
$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
if (isset($btnSubmitFullForm)) {
    saveOrEditProfileSteps();
} else {
    $portfolionames = MysqlConnection::fetchCustom("SELECT DISTINCT(portfolio_name) as portfolio_name FROM `tbl_portfolioprofile` ORDER BY `profile_name` ASC");
    $profileid = urldecode(filter_input(INPUT_GET, "profileid"));
    if ($profileid != "") {
        $resultset = MysqlConnection::fetchCustom("SELECT stepname as name,sequence FROM `tbl_profile_scanner_integration` WHERE profileid = '$profileid' ");
        $readonly = "readonly";
    } else {
        $resultset = MysqlConnection::fetchCustom("SELECT name FROM `generic_entry` WHERE type = 'scannerstep' ");
    }
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;"> MANAGE PROFILE STEPS</h5>
    </div>
    <div class="widget-box" >
        <p style="color: red"><?php echo $error ?></p>
        <form name="frmUserSubmit"  enctype="multipart/form-data" id="frmUserSubmit" method="post" >
            <div class="widget-content tab-content">
                <table  class="display nowrap sortable" >
                    <tr>
                        <td><label class="control-label"  style="width: 150px;">Profile Name</label></td>
                        <td>
                            <select name="profileid" style="width: 250px;" <?php echo $readonly?> >
                                <option value="">Select</option>
                                <?php
                                foreach ($portfolionames as $key => $value) {
                                    if (trim($profileid) == trim($value["portfolio_name"])) {
                                        $selected = "selected";
                                    } else {
                                        $selected = "";
                                    }
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $value["portfolio_name"] ?>"><?php echo $value["portfolio_name"] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                    $index = 1;
                    foreach ($resultset as $key => $value) {
                        $sequence = $value["sequence"];
                        ?>
                        <tr>
                            <td><label class="control-label" style="width: 150px;"><?php echo $value["name"] ?></label></td>
                            <td >
                                <select name="chooseStep[]" id="chooseStep[]"  style="width: 250px;">
                                    <option value="">Select</option>
                                    <?php
                                    for ($index1 = 1; $index1 < 10; $index1++) {
                                        if ($sequence == $index1) {
                                            $selected = "selected";
                                        } else {
                                            $selected = "";
                                        }
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $value["name"] . "-" . $index1 ?>"><?php echo $index1 ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr> 
                        <?php
                        $index++;
                    }
                    ?>
                </table>
                <hr/>
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SAVE</button>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            
        </form>
    </div>
</div> 
<?php

function saveOrEditProfileSteps() {
    $btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
    $profileid = urldecode(filter_input(INPUT_GET, "profileid"));
    if ($profileid != "") {
        MysqlConnection::delete("DELETE FROM tbl_profile_scanner_integration WHERE profileid = '$profileid'  ");
    }
    if (isset($btnSubmitFullForm)) {
        foreach ($_POST["chooseStep"] as $value) {
            $explode = explode("-", $value);
            $array = array();
            $array["stepname"] = $explode[0];
            $array["sequence"] = $explode[1];
            $array["profileid"] = filter_input(INPUT_POST, "profileid");
            $array["isavailable"] = "Y";
            if (trim($array["stepname"]) != "") {
                MysqlConnection::insert("tbl_profile_scanner_integration", $array);
            }
        }
        header("location:index.php?pagename=manage_profilesteps");
    }
}
