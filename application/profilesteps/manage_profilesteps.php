<?php
$profileid = filter_input(INPUT_GET, "profileid");
if ($profileid != "") {
    MysqlConnection::delete("DELETE FROM tbl_profile_scanner_integration WHERE profileid = '$profileid' ");
    header("location:index.php?pagename=manage_profilesteps");
} else {
    $resultset = MysqlConnection::fetchCustom("SELECT DISTINCT(portfolio_name) as portfolio_name FROM `tbl_portfolioprofile` ORDER BY portfolio_name");
}
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PROFILE SCANNER INTEGRATION</h5>
    </div>
    <br/>
    <table  style="width: 100%">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=add_profilesteps"  data-toggle="modal">PROFILE STEPS</a></td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                 <td style="width: 25px">#</td>
                <td>Portfolio Name</td>
                <td>Scanning Steps</td>
            </tr>
        </thead>
        <tbody >
            <?php
            $index0= 1;
            foreach ($resultset as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index0);
                $profileid = $value["portfolio_name"];
                $resultsetof = MysqlConnection::fetchCustom("SELECT stepname FROM `tbl_profile_scanner_integration` WHERE profileid = '$profileid' AND stepname!=''  ORDER BY sequence");
                $arrsteps = array();
                $index = 1;
                foreach ($resultsetof as $key => $value1) {
                    $colorarray = colorarray();
                    $index++;
                    $display = '<i  style="padding: 5px 15px 5px 15px;background-color:' . $colorarray[$index] . '">' . strtoupper($value1["stepname"]) . '</i>';
                    array_push($arrsteps, $display);
                }
                ?>
            
                <tr id="<?php echo urlencode($value["portfolio_name"]) ?>" style="background-color: <?php echo $bgcolor ?>;" class="context-menu-one">
                     <td ><?php echo $index0 ?></td>
                    <td ><?php echo $value["portfolio_name"] ?></td>
                    <td ><?php echo implode(" &DoubleLongRightArrow; ", $arrsteps) ?></td>
                </tr>
                <?php
                $index0++;
            }
            ?>
        </tbody>
    </table>
</div>

<script>
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_profile":
                        window.location = "index.php?pagename=view_profilesteps&profileid=" + id;
                        break;
                    case "edit_profile":
                        window.location = "index.php?pagename=add_profilesteps&profileid=" + id;
                        break;
                    case "delete_profile":
                        window.location = "index.php?pagename=view_profilesteps&profileid=" + id + "&flag=yes";
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    default:
                        window.location = "index.php?pagename=manage_profilesteps";
                }
            },
            items: {
                "view_profile": {name: "VIEW PROFILE STEP", icon: ""},
                "edit_profile": {name: "EDIT PROFILE STEP", icon: "context-menu-icon-add"},
                "delete_profile": {name: "DELETE PROFILE STEP", icon: ""}
            }
        });

        $('tr').dblclick(function () {
            var id = $(this).attr('id');
            if (id !== undefined) {
                window.location = "index.php?pagename=view_profilesteps&profileid=" + id;
            }
        });

    });

</script>
<?php

function colorarray() {
    $color = array();
    $color[] = "";
    $color[] = "#bce8f1";
    $color[] = "#d9edf7";
    $color[] = "#d6e9c6";
    $color[] = "#dff0d8";
    $color[] = "#fbeed5";
    $color[] = "#fcf8e3";
    $color[] = "#bce8f1";
    $color[] = "#d9edf7";
    $color[] = "#d6e9c6";
    $color[] = "#dff0d8";
    $color[] = "#fbeed5";
    $color[] = "#fcf8e3";
    return $color;
}
?>