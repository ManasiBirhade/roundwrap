<?php
$flag = filter_input(INPUT_GET, "flag");
if (isset($btnSubmitFullForm)) {
    saveOrEditProfileSteps();
} else {
    $portfolionames = MysqlConnection::fetchCustom("SELECT DISTINCT(portfolio_name) as portfolio_name FROM `tbl_portfolioprofile` ORDER BY `profile_name` ASC");
    $profileid = urldecode(filter_input(INPUT_GET, "profileid"));
    if ($profileid != "") {
        $resultset = MysqlConnection::fetchCustom("SELECT stepname as name,sequence FROM `tbl_profile_scanner_integration` WHERE profileid = '$profileid' ");
    } else {
        $resultset = MysqlConnection::fetchCustom("SELECT name FROM `generic_entry` WHERE type = 'scannerstep' ");
    }
}

if (filter_input(INPUT_POST, "deleteItem") == "DELETE") {
    MysqlConnection::delete("DELETE FROM tbl_profile_scanner_integration WHERE profileid = '$profileid'");
    header("location:index.php?pagename=manage_profilesteps");
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;"> MANAGE PROFILE STEPS</h5>
    </div>
    <?php
    if ($flag == "yes") {
        echo MysqlConnection::$DELETE;
    }
    ?>
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
        <p style="color: red"><?php echo $error ?></p>
        <form name="frmUserSubmit"  enctype="multipart/form-data" id="frmUserSubmit" method="post" >
            <div class="widget-content tab-content">
                <table  class="display nowrap sortable table-bordered">
                    <tr>
                        <td><label class="control-label" style="width: 150px;">Profile Name</label></td>
                        <td>
                            <select name="profileid"  style="width: 250px;" disabled="">
                                <option value="" >Select</option>
                                <?php
                                foreach ($portfolionames as $key => $value) {
                                    if (trim($profileid) == trim($value["portfolio_name"])) {
                                        $selected = "selected";
                                    } else {
                                        $selected = "";
                                    }
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $value["portfolio_name"] ?>" ><?php echo $value["portfolio_name"] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php
                    $index = 1;
                    foreach ($resultset as $key => $value) {
                        $sequence = $value["sequence"];
                        ?>
                        <tr>
                            <td><label class="control-label" style="width: 150px;"><?php echo $value["name"] ?></label></td>
                            <td >
                                <select name="chooseStep[]" id="chooseStep[]"  disabled=""  style="width: 250px;">
                                    <option value="">Select</option>
                                    <?php
                                    for ($index1 = 1; $index1 < 10; $index1++) {
                                        if ($sequence == $index1) {
                                            $selected = "selected";
                                        } else {
                                            $selected = "";
                                        }
                                        ?>
                                        <option <?php echo $selected ?> value="<?php echo $value["name"] . "-" . $index1 ?>"><?php echo $index1 ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr> 
                        <?php
                        $index++;
                    }
                    ?>
                </table>
                <hr/>
            </div>
            <div class="modal-footer " style="text-align: center">
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                <?php
                if ($flag != "") {
                    ?>
                    <input type="submit" name="deleteItem" value="DELETE"  class="btn btn-info" />
                    <?php
                }
                ?>
            </div>
        </form>

    </div>
</div> 
