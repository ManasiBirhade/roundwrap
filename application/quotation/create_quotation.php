<?php
$psid = filter_input(INPUT_GET, "psid");
$quotationid = MysqlConnection::generateNumber("prodquot");
$packsliparray = MysqlConnection::getPackSlipFromId($psid);

$packslipdetailsarray = MysqlConnection::getPackSlipDetailsFromId($psid);
$customerdetails = MysqlConnection::getCustomerDetails($packsliparray["cust_id"]);
$taxinfor = MysqlConnection::getTaxInfoById($customerdetails["taxInformation"]);
$portfolioname = MysqlConnection::getPortfolioProfileById($packsliparray["prof_id"]);

$packvalues = $packsliparray["packvalues"];
$arrexploadvalues = explode(",", $packvalues);

submitOrEdit($quotationid);
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="packingslip/packingslipjs.js"></script>
<script src="js/arrow.js"></script>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    .sortable input{background-color: white}
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1"> MANAGE QUOTATION</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="display nowrap sortable table-bordered" style="width: 100%;background-color: white;">
                            <tr style="text-align: left;vertical-align: top">
                                <td style="width: 150px;"><label  class="control-label" >CUSTOMER&nbsp;DETAILS&nbsp;</label></td>
                                <td ><?php echo $customerdetails["cust_companyname"] ?></td>
                                <td ><label  class="control-label" >QUOT.&nbsp;ID&nbsp;:&nbsp</label></td>
                                <td ><?php echo $quotationid ?></td>
                                <td ><label  class="control-label">QUOT.&nbsp;DATE&nbsp;:&nbsp</label></td>
                                <td><?php echo MysqlConnection::convertToPreferenceDate(date("Y-m-d")) ?></td>
                            </tr>
                            <tr style="text-align: left;vertical-align: top;text-align: justify;">
                                <td><label  class="control-label" >BILL TO</label></td>
                                <td colspan="5"><?php echo MysqlConnection::formatToBRAddress($customerdetails["billto"]) ?></td>
                            </tr>
                        </table>  

                    </td>
                </tr>
                <tr>

                    <td>
                        <div style="width: 28%;float: left">
                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <?php
                                ?>
                                <tr style="font-weight: bold;height: 35px;  ">
                                    <td style="width: 150px;"><b> Profile Name</b></td>
                                    <td>
                                        <?php
                                        echo ucwords($portfolioname["profile_name"])
                                        ?>
                                    </td>
                                </tr>
                                <?php
                                $packlebels = explode(",", $packsliparray["packlebels"]);
                                $packvalues = explode(",", $packsliparray["packvalues"]);

                                $calprofilecost = calculateForEachType($packslipdetailsarray, $portfolioname, $packlebels, $packvalues);

                                for ($inx = 0; $inx < count($packlebels); $inx++) {
                                    $expval = explode("--", $packvalues[$inx]);
                                    ?> 
                                    <tr style="height: 30px;font-weight: bold">
                                        <td style="width: 150px;"><?php echo ucwords(str_replace("_", " ", $packlebels[$inx])) ?></td>
                                        <td><?php echo ucwords(trim($expval[0])) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <br/>
                        <div style="width: 70%;float: right">
                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(250,250,250)">
                                    <td style="width: 50px;"><b>Pcs</b></td>
                                    <td style="width: 200px;"><b>Type</b></td>
                                    <td style="width: 100px;"><b>(W) MM</b></td>
                                    <td style="width: 100px;"><b>(H) MM</b></td>
                                    <td style="width: 100px;"><b>(W) INCH</b></td>
                                    <td style="width: 100px;"><b>(H) INCH</b></td>
                                    <td><b>SQ-FT</b></td>
                                </tr>
                                <?php
                                $index = 1;
                                $totalsqft = 0;
                                foreach ($packslipdetailsarray as $key => $value) {
                                    $totalsqft = $totalsqft + $value["sqFeet"];
                                    ?>
                                    <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                        <td style="width: 50px">
                                            <input type="hidden" name="detailid[]" value="<?php echo $value["psd_id"] ?>">
                                            <?php echo $value["pcs"] ?>
                                        </td>
                                        <td style="width: 200px;"><?php echo $value["type"] ?></td>
                                        <td style="width: 100px;"><?php echo $value["mm_w"] ?></td>
                                        <td style="width: 100px;"><?php echo $value["mm_h"] ?></td>
                                        <td style="width: 120px;"><?php echo $value["fract_w"] ?></td>
                                        <td style="width: 120px;"><?php echo $value["fract_h"] ?></td>
                                        <td style=""><?php echo $value["sqFeet"] ?></td>
                                    </tr>
                                    <?php
                                    $index++;
                                }
                                ?>
                                <tr >
                                    <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD" ></td>
                                    <td style="background-color: white"><b>Total Pieces</b></td>
                                    <td style="background-color: white"><?php echo $packsliparray["total_pieces"] ?></td>
                                </tr>
                                <tr >
                                    <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD" ></td>
                                    <td style="background-color: white"><b>Total SqFt</b></td>
                                    <td style="background-color: white"><?php echo round($totalsqft, 2) ?></td>
                                </tr>

                                <tr>
                                    <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                    <td style="background-color: white"><b>Profile Cost</b></td>
                                    <td style="background-color: white">
                                        <?php echo $profilecost = round($portfolioname["baseprice"] + $calprofilecost, 2) ?>
                                        <input type="hidden" name="profilecost" value="<?php echo $profilecost ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                    <td style="background-color: white"><b>Item Cost</b></td>
                                    <td style="background-color: white"><?php echo round($calprofilecost, 2) ?></td>
                                </tr>

                                <tr>
                                    <td colspan="5"  style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                    <td style="background-color: rgb(255,255,255)"><b>Sub Total</b></td>
                                    <td style="background-color: rgb(255,255,255)">
                                        <?php echo $subtotal = ($portfolioname["baseprice"] + $calprofilecost) * $totalsqft ?>
                                        <input type="hidden" name="total_cost" value="<?php echo $subtotal ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                    <td style="background-color: white"><b>Tax-  <?php echo $taxdescription = $taxinfor["taxdescription"] ?> </b></td>
                                    <td style="background-color: white">
                                        <?php echo $taxvalue = round($subtotal * ($taxinfor["taxvalues"] / 100), 2) ?>&nbsp;&nbsp;(<?php echo $taxinfor["taxvalues"] ?>)%
                                        <input type="hidden" name="taxname" value="<?php echo $taxdescription ?>">
                                        <input type="hidden" name="taxvalue" value="<?php echo $taxvalue ?>">
                                    </td>
                                </tr>
                                <tr >
                                    <td colspan="5"  style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                    <td style="background-color: rgb(240,240,240)"><b>Net Total</b></td>
                                    <td style="background-color: rgb(240,240,240)">
                                        <?php
                                        echo $nettotalof = round(($subtotal * ($taxinfor["taxvalues"] / 100) ) + $subtotal, 2);
                                        ?>
                                        <input type="hidden" name="nettotal" value="<?php echo $nettotalof ?>">
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>
            </table>
            <div class="modal-footer " > 
                <center>
                    <input type="submit" id="btnsav" class="btn btn-info" name="btnsave" value="SAVE">
                    <input type="submit" id="printButton" class='btn btn-info' name="print" value="PRINT">
                    <input type="submit" id="mailButton" class='btn btn-info' name="email" value="EMAIL" style="margin-left:4px;">
                    <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                </center>
            </div>
        </div>
    </div>
</form>
<script>

    function copyValues(index) {
        if (index === "1") {
            var indexValue = document.getElementById("dollerPerFit" + index).value;
            for (i = 1; i <= 30; i++) {
                if (document.getElementById("dollerPerFit" + i) !== null) {
                    document.getElementById("dollerPerFit" + i).value = indexValue;
                }
            }
            calculateCutTapeCharges();
            calculateQuotationTotal();
        }
    }
</script>
<?php

function submitOrEdit($quotationid) {
    $psid = filter_input(INPUT_GET, "psid");
    $edit = filter_input(INPUT_GET, "edit");
    if (!isset($edit)) {
        $editpackslipquotation["quot_id"] = $quotationid;
    }
    $btnsave = filter_input(INPUT_POST, "btnsave");
    $print = filter_input(INPUT_POST, "print");
    $email = filter_input(INPUT_POST, "email");



    if (isset($btnsave) || isset($print) || isset($email)) {
        $psid = filter_input(INPUT_GET, "psid");
        $edit = "UPDATE packslip SET "
                . "profilecost = '" . filter_input(INPUT_POST, "profilecost") . "'"
                . ",total_cost =  '" . filter_input(INPUT_POST, "total_cost") . "'"
                . ",taxname =  '" . filter_input(INPUT_POST, "taxname") . "'"
                . ",taxvalue =  '" . filter_input(INPUT_POST, "taxvalue") . "'"
                . ",nettotal =  '" . filter_input(INPUT_POST, "nettotal") . "'"
                . " WHERE ps_id = '$psid' ";
        MysqlConnection::executeQuery($edit);
        $arraytrack["psid"] = $psid;
        //this is tracking array
        $arraytrack = array();
        $arraytrack["cust_id"] = '';
        $arraytrack["packslipId"] = $psid;
        $arraytrack["workOrId"] = "-";
        $arraytrack["workInCode"] = "-";
        $arraytrack["workOutCode"] = "-";
        $arraytrack["scannerId"] = "-";
        $arraytrack["scannerCode"] = "-";
        $arraytrack["phase"] = "QUOTATION CREATED";
        $arraytrack["phase_description"] = "QUOTATION FOR PACKING SLIP CREATED";
        $arraytrack["phase_date"] = date("Y-m-d");
        $arraytrack["phase_time"] = date("g:i:s");
        $arraytrack["finished"] = "Y";
        MysqlConnection::insert("workorder_phase_history", $arraytrack);
        MysqlConnection::edit("packslip", $editpackslipquotation, " ps_id = '$psid' ");
        MysqlConnection::delete("UPDATE `order_number` SET prodquot = $quotationid WHERE identity = 'Y' ");
        generatePDF($psid);
    }

    if (isset($btnsave)) {
        header("location:index.php?pagename=manage_quotation");
    } else if ($print) {
        $_SESSION["psid"] = $psid;
        header("location:index.php?pagename=manage_quotation");
    } else if ($email) {
        header("location:index.php?pagename=email_quotation&packslipId=$psid&status=email_quotation");
    }
}

function generatePDF($packslipid) {
    include 'pdflib/quotation.php';
}

function calculateForEachType($packslipdetailsarray, $portfolioname, $packlebels, $packvalues) {
    $portfolioid = $portfolioname["id"];
    $profile_name = $portfolioname["profile_name"];
    $arraytype = array();
    foreach ($packslipdetailsarray as $value) {
        if (!in_array($value["type"], $arraytype)) {
            array_push($arraytype, $value["type"]);
        }
    }
    $costof = 0;
    for ($index = 0; $index < count($packlebels); $index++) {
        $label = $packlebels[$index];
        $labvalue = $packvalues[$index];
        $labacvalue = explode("--", $labvalue);
        foreach ($arraytype as $value) {
            $sql = "SELECT profile_label_price FROM profile_price"
                    . " WHERE portfolio_id = '$portfolioid'"
                    . " AND portfolio_name LIKE '%$profile_name%'"
                    . " AND profileTypeValue = '$value'"
                    . " AND portfolio_label = '$label'"
                    . " AND profile_label_name = '" . trim($labacvalue[0]) . "'  ";
            $resultset = MysqlConnection::fetchCustom($sql);
            $costof = $costof + $resultset[0]["profile_label_price"];
        }
        return $costof;
    }
}
