<?php
$packslipresult = MysqlConnection::fetchCustom("SELECT * FROM `packslip` WHERE quot_id != '' ORDER BY indexid DESC ");
$basequery = "SELECT * FROM `packslip`  ";
$status = filter_input(INPUT_GET, "status");
$searchPSID = filter_input(INPUT_GET, "psid");

if ($status == "search") {
    $basequery = $basequery . " WHERE ps_id = '". $searchPSID . "'";

}else{
    
}
$packslip = MysqlConnection::fetchCustom($basequery . "  ");

$psid = $_SESSION["psid"];
if ($psid != "") {
    echo "<script language='javascript'>window.open('invoice/print_quotation.php?psid=$psid');</script>";
}
$_SESSION["psid"] = "";
?>

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 340,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">     
        <h5 style="font-family: verdana;font-size: 12px;">QUOTATION LIST</h5>
    </div>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>
                <td>Quot No</td>
                <td>Company Name</td>
                <td>Profile Name</td>
                <td>SO No</td>
                <td>PO No</td>
                <td>Rec Date</td>
                <td>Req Date</td>
                <td>Email??</td>
              
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($packslipresult as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $customername = MysqlConnection::fetchCustom("SELECT cust_companyname FROM customer_master WHERE id = '" . $value["cust_id"] . "'");
                $portfolioname = MysqlConnection::fetchCustom("select portfolio_name,profile_name from tbl_portfolioprofile where id = '" . $value["prof_id"] . "'");
                ?>
                <tr id="<?php echo $value["ps_id"] ?>" style="<?php echo $bgcolor ?> ;"  class="context-menu-one">
                    <td><?php echo $index ?></td>
                    <td><?php echo $value["quot_id"] ?></td>
                    <td><a href="index.php?pagename=view_customermaster&customerId=<?php echo $value["cust_id"] ?>"><?php echo $customername[0]["cust_companyname"] ?></a></td>
                    <td><?php echo $portfolioname[0]["portfolio_name"] . " - " . $portfolioname[0]["profile_name"] ?></td>
                    <td><a href="index.php?pagename=manage_packingslip&psid=<?php echo $value["ps_id"] ?>&status=search"><?php echo $value["so_no"] ?></a></td>
                    <td><?php echo $value["po_no"] ?></td>
                    <td><?php echo $value["rec_date"] ?></td>
                    <td><?php echo $value["req_date"] ?></td>
                    <td><?php echo $value["isQmail"] == "N" ? "<span class='badge badge-info'>NO</span>" : "<span class='badge badge-success'>YES</span>" ?> </td>
                    
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" style="margin-left:10px;">EMAIL</button>
        </div> 
    </div>
</div>
<script>
    $('#example tbody tr').click(function(e) {
        var id = $(this).attr('id');
        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var del = '<a href="invoice/print_quotation.php?psid=' + id + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var em = '<a href="index.php?pagename=email_quotation&packslipId=' + id + '&status=email_quotation" ><button type="button" class=" btn btn-info" style="margin-left:10px;">EMAIL</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + del + em + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });
    
    $("#deleteThis").click(function() {
        var dataString = "deleteId=" + $('#deleteId').val();
        $.ajax({
            type: 'POST',
            url: 'quotation/quotation_ajax.php',
            data: dataString
        }).done(function(data) {
        }).fail(function() {
        });
        location.reload();
    });

    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }
    $("#save").click(function() {
        var json = convertFormToJSON("#basic_validate");
        $.ajax({
            type: 'POST',
            url: 'quotation/savequotation_ajax.php',
            data: json
        }).done(function(data) {
        }).fail(function() {
        });
        location.reload();
    });
</script>
<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_quotation":
                        window.location = "index.php?pagename=view_quotation&psid=" + id;
                        break;
                    case "create_quotation":
                        window.location = "index.php?pagename=create_quotation";
                        break;
                    case "print_quotation":
                        window.open("invoice/print_quotation.php?psid=" + id, '_blank');
                        break;
                    case "email_quotation":
                        window.location = "index.php?pagename=email_quotation&packslipId=" + id + "&status=email_quotation";

                        break;
                    case "edit_quotation":
                        window.location = "index.php?pagename=create_packingslip&packslipId=" + id + "&action=quotation";
                        break;
                    case "delete_quotation":
                        window.location = "index.php?pagename=view_quotation&psid=" + id + "&flag=yes";
                        break;
                    case "create_workorder":
                        window.location = "index.php?pagename=fromps_workorder&packslipId=" + id;
                        break;
                    case "active_quote":
                        window.location = "index.php?pagename=manage_quotation&packslipId=" + id;
                        break;
                    case "create_note":
                        window.location = "index.php?pagename=note_suppliermaster&supplierid=" + id;
                        break;
                    case "create_payment":
                        window.location = "index.php?pagename=vendor_payment&supplierid=" + id;
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_suppliermaster";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_quotation": {name: "VIEW QUOTATION", icon: ""},
                "edit_quotation": {name: "EDIT QUOTATION", icon: ""},
                "delete_quotation": {name: "DELETE QUOTATION", icon: ""},
                "sep0": "---------",
                "create_workorder": {name: "CREATE WORK ORDER", icon: ""},
                "active_quote": {name: "ACTIVE/IN ACTIVE QUOTATION", icon: ""}
            }
        });

        //        $('.context-menu-one').on('click', function(e){
        //            console.log('clicked', this);
        //       })    
    });
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_quotation&psid=" + id;
        }
    });
</script>