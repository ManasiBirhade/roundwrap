<?php
$packslipid = base64_decode(filter_input(INPUT_GET, "packslipid"));
$flag = filter_input(INPUT_GET, "flag");
session_start();
ob_start();
$_SESSION["packslipid"] = $packslipid;
?>


<?php if ($flag == "yes") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Quotation has been Deleted Successfully !!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_quotation&status=all" id="btnSubmitFullForm" class="btn btn-info">GO BACK TO QUOTATION</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($flag == "email") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Email has been sent Successfully!!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_quotation&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO QUOTATION</a></td>

                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($flag == "update") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Your Quotation has been updated Successfully!!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_quotation&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO QUOTATION</a></td>

                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>