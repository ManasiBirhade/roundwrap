<?php
$appriveqtion = filter_input(INPUT_GET, "appriveqtion");
$flag = filter_input(INPUT_GET, "flag");
$packslipid = $packslip = filter_input(INPUT_GET, "psid");

$packsliparray = MysqlConnection::fetchCustom("SELECT * FROM packslip WHERE ps_id ='$packslipid'");
$packslip = $packsliparray[0];
$customerid = $packslip["cust_id"];
$profid = $packslip["prof_id"];
$customerdetails = MysqlConnection::fetchCustom("SELECT cust_companyname FROM `customer_master` WHERE  id = '$customerid' ");

$portfolioname = MysqlConnection::getPortfolioProfileById($packslip["prof_id"]);

if (isset($_POST["deleteItem"])) {
    MysqlConnection::delete("DELETE FROM packslip WHERE ps_id = '$packslipid' ");
    header("location:index.php?pagename=success_quotation&packslipid=$packslipid&flag=yes");
}
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="packingslip/packingslipjs.js"></script>
<script>
    $(function() {
        var availableCustomer = [<?php echo $customerauto ?>];
        $("#companyname").autocomplete({source: availableCustomer});
    });
</script>

<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">QUOTATION VIEW</a></li>
                </ul>
            </div>
            <?php
            if ($flag == "yes") {
                echo MysqlConnection::$DELETE;
            }
            ?>
            <table style="width: 100%" class="display nowrap sortable" >
                <tr>
                    <td>

                        <table class="display nowrap sortable" style="width: 100%">
                            <tr >
                                <td ><label>CUSTOMER&nbsp;DETAILS</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td ><input style="width: 220px"type="text" value="<?php echo $customerdetails[0]["cust_companyname"] ?>" readonly=""/></td>
                                <td  ><label ><b>QUOT.&nbsp;ID</b></label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td ><input style="width: 220px" type="text" name="quot_id" id="quot_id" value="<?php echo $packslip["quot_id"] ?>" readonly="" ></td>
                                <td ><label >QUOT.&nbsp;DATE</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td ><input style="width: 220px" type="text" name="rec_date" readonly="" value="<?php echo date("Y-m-d") ?>" ></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 28%;float: left">
                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse;background-color: white" border="1">

                                <tr style="font-weight: bold;height: 30px;">
                                    <td style="width: 35%"><b> Portfolio</b></td>
                                    <td><?php echo $portfolioname["portfolio_name"] ?></td>
                                </tr>
                                <tr style="font-weight: bold;height: 30px;">
                                    <td style="width: 35%"><b> Profile</b></td>
                                    <td><?php echo $portfolioname["profile_name"] ?></td>
                                </tr>
                                <?php
                                $packlebels = explode(",", $packslip["packlebels"]);
                                $packvalues = explode(",", $packslip["packvalues"]);
                                for ($inx = 0; $inx < count($packlebels); $inx++) {
                                    $explod = explode("--", $packvalues[$inx]);
                                    ?> 
                                    <tr style="height: 30px;font-weight: bold">
                                        <td style="width: 50%;"><?php echo ucwords(str_replace("_", " ", $packlebels[$inx])) ?></td>
                                        <td><?php echo ucwords($explod[0]) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <div style="width: 70%;float: right">
                            <table class="display nowrap sortable"style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(250,250,250)">
                                    <td style="width: 25px;"><b>#</b></td>
                                    <td style="width: 100px;"><b>Pcs</b></td>

                                    <td style="width: 250px;"><b>Type</b></td>
                                    <td style="width: 80px;"><b>(W) MM</b></td>
                                    <td style="width: 80px;"><b>(H) MM</b></td>
                                    <td style="width: 80px;"><b>(W) INCH</b></td>
                                    <td style="width: 80px;"><b>(H) INCH</b></td>
                                    <td><b>SQ-FT</b></td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $resultsetdetails = MysqlConnection::fetchCustom("SELECT * FROM `packslipdetail` WHERE `ps_id` =  '" . filter_input(INPUT_GET, "psid") . "'");
                                    $index = 0;
                                    foreach ($resultsetdetails as $key => $value) {
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px"><?php echo ++$index ?></td>
                                            <td style="width: 100px"><?php echo $value["pcs"] ?></td>

                                            <td style="width: 250px;"><?php echo $value["type"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["mm_w"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["mm_h"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["fract_w"] ?></td>
                                            <td style="width: 80px;"><?php echo $value["fract_h"] ?></td>
                                            <td><?php echo $value["sqFeet"] ?></td>
                                        </tr>
                                    <?php } ?>

                                    <tr>
                                        <td colspan="6" style="background-color: white"></td>
                                        <td ><b>Total Pieces</b></td>
                                        <td><?php echo $packslip["total_pieces"] ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="background-color: white"></td>
                                        <td ><b>Total SqFt</b></td>
                                        <td><?php echo $packslip["billable_fitsquare"] ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <hr/>
            <div class="modal-footer "> 
                <center>
                    <?php
                    if (isset($flag)) {
                        ?>
                        <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info"/>
                        <input type="hidden" name="packslipid" value="<?php echo $packslipid ?>"/>
                        <a href="javascript:history.back()" class=" btn btn-info">CANCEL</a>
                    <?php } else { ?>
                        <a href="javascript:history.back()" class=" btn btn-info">CANCEL</a>
                    <?php } ?>
                </center>
            </div>
        </div>
    </div>
</form>

