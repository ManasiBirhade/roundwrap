<style>
    table tbody {

    }
    table tr td{
        padding: 5px;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="js/script.js"></script>

<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });

</script>

<?php
$purchaseid = filter_input(INPUT_GET, "purchaseorderid");
$result = MysqlConnection::fetchCustom(""
                . "SELECT * , pi.id as poitemid , (SELECT companyname FROM supplier_master WHERE supp_id = po.`supplier_id` ) "
                . "AS companyname FROM purchase_order po, purchase_item pi "
                . "WHERE po.id = pi.po_id AND pi.po_id = '$purchaseid' ");

$podetails = $result[0];
$action = filter_input(INPUT_GET, "action");

if ($podetails["isOpen"] == "N") {
    header("location:index.php?pagename=manage_perchaseorder&action=poclosed");
}

saveOrEditReceivingOrder();
?>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 0px;margin: 0px;}
</style>
<form  name="purchaseorder" method="post">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title"><ul class="nav nav-tabs"><li class="active"><a data-toggle="tab" href="#tab1">RECEIVING ORDER ENTRY</a></li></ul></div>
            <br/>
            <table style="width: 100%" >
                <tr>
                    <td>
                        <table style="width: 100%" class="display nowrap sortable">
                            <tr>
                                <td style="width: 10%"><label class="control-label"   class="control-label">SUPPLIER NAME</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input  type="text" style="width: 85%" readonly="" value="<?php echo $podetails["companyname"] ?>" /></td>
                                <td style="width: 10%"><label class="control-label">SHIP VIA&nbsp;</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input  type="text"  style="width: 85%" value="<?php echo $podetails["ship_via"] ?>" readonly="" /></td>
                                <td style="width: 10%"><label class="control-label">EXPECTED&nbsp;DELIVERY&nbsp;:&nbsp</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input type="text" readonly="" value="<?php echo $podetails["expected_date"] ?>"  data-date-format="mm-dd-yyyy"  ></td>
                            </tr>
                            <tr>
                                <td ><label  class="control-label"  class="control-label">BILLING&nbsp;ADDRESS</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><textarea style="line-height: 18px;resize: none;height: 80px;width: 85%" readonly=""><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $podetails["shipping_address"])) ?></textarea></td>
                                <td><label class="control-label">SHIPPING&nbsp;ADDRESS&nbsp;</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><textarea style="line-height: 18px;resize: none;height: 80px;width: 85%" readonly=""><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $podetails["billing_address"])) ?></textarea></td>
                                <td ><label class="control-label">REMARK&nbsp;/&nbsp;NOTE&nbsp;</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><textarea   style="line-height: 18px;resize: none;height: 80px;" readonly=""><?php echo $podetails["remark"] ?></textarea></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 70%;float: left">

                            <div class="accordion" id="collapse-group">
                                <div class="accordion-group widget-box">
                                    <div class="accordion-heading">
                                        <div class="widget-title">
                                            <a data-parent="#collapse-group" href="#collapseGOne" data-toggle="collapse" class="">
                                                <span class="icon"><i class="icon-ok"></i></span><h5>Enter Receiving Order</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="accordion-body in collapse" id="collapseGOne" style="height: auto;">
                                        <div class="widget-content">
                                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white;height: 30px;">
                                                    <td style="width: 450px;">ITEM DETAIL</td>
                                                    <td style="width: 120px;">ORDERED</td>
                                                    <td style="width: 120px;">RECEIVED</td>
                                                    <td >RECEIVING</td>
                                                </tr>
                                            </table>
                                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                                <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                                    <?php
                                                    $index = 1;
                                                    foreach ($result as $key => $value) {
                                                        $items = MysqlConnection::getItemDataById($value["item_id"]);
                                                        if (($value["qty"] - $value["rqty"]) != 0) {
                                                            ?>
                                                            <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                                                <td style="width: 450px;"><?php echo $items["item_code"] ?> <?php echo $items["item_desc_purch"] ?></td>
                                                                <td style="width: 120px;">
                                                                    <?php echo ($value["qty"]) ?>
                                                                    <input type="hidden" value="<?php echo ($value["qty"] - $value["rqty"]) ?>" id="lastreceived<?php echo $index ?>">
                                                                </td>
                                                                <td style="width: 120px;;background-color: rgb(247,252,231)"><?php echo $value["rqty"] ?></td>
                                                                <td >
                                                                    <input type="hidden" name="poitemid[]" value="<?php echo $value["item_id"] ?>"  >
                                                                    <input type="text" onkeypress="return chkNumericKey(event)" value="<?php echo ($value["qty"] - $value["rqty"]) ?>" name="received[]" id="received<?php echo $index ?>" onfocusout="validateQty('<?php echo $index ?>')" >
                                                                </td>
                                                            </tr>
                                                            <?php
                                                            $index++;
                                                        }
                                                    }
                                                    ?>
                                                </table>
                                            </div>     
                                        </div>
                                    </div>
                                </div> 
                                <div class="accordion-group widget-box">
                                    <div class="accordion-heading">
                                        <div class="widget-title">
                                            <a data-parent="#collapse-group" href="#collapseGTwo" data-toggle="collapse">
                                                <span class="icon"><i class="icon-circle-arrow-right"></i></span><h5>Receiving Order History</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="accordion-body collapse" id="collapseGTwo" style="height: 0px;">
                                        <div class="widget-content">
                                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">
                                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white;height: 30px;">
                                                    <td style="width: 450px;">ITEM DETAIL</td>
                                                    <td style="width: 120px;">RECEIVED</td>
                                                    <td >DATE</td>
                                                </tr>
                                                <?php
                                                $resultreceiving = MysqlConnection::getReceivingOrderByPoId($purchaseid);
                                                foreach ($resultreceiving as $key => $valuer) {
                                                    $items = MysqlConnection::getItemDataById($valuer["item_id"]);
                                                    ?>
                                                    <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                                        <td style="width: 450px;"><?php echo $items["item_code"] ?> <?php echo $items["item_desc_purch"] ?></td>
                                                        <td style="width: 120px;"><?php echo $valuer["rqty"] ?></td>
                                                        <td ><?php echo MysqlConnection::convertToPreferenceDate($valuer["date"]) ?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div style="width: 28%;float: right">
                            <table   style="width: 100%;border-collapse: collapse;background-color: white" class="display nowrap sortable" border="1">
                                <tr style="font-weight: bold; color: red">
                                    <td style="width: 35%"><b>PO NUMBER</b></td>
                                    <td><input type="text" value="<?php echo $podetails["purchaseOrderId"] ?>"  readonly=""></td>
                                </tr>
                                <tr >
                                    <td><b>ORDER DATE</b></td>
                                    <td><input type="text" value="<?php echo $podetails["purchasedate"] ?>" readonly=""></td>
                                </tr>
                                <tr >
                                    <td style="font-weight: bold; color: red"><b>RECEIVING DATE</b></td>
                                    <td><input type="text" value="<?php echo date("Y-m-d") ?>" readonly=""></td>
                                </tr>
                                <tr >
                                    <td><b>ENTER BY</b></td>
                                    <td><input type="text" value="<?php echo MysqlConnection::getAddedBy($podetails["added_by"]); ?>" readonly="" ></td>
                                </tr>
                                <tr>
                                    <td><b>REMIND ME ON &nbsp;</b></td>
                                    <td>  
                                        <input  style="width: 200px" type="text" value="<?php echo $podetails["reminderdate"] ?>" name="reminderdate"  id="datepicker" class="datepicker" >
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>NOTE / COMMENT</b></td>
                                    <td>
                                        <textarea autofocus="" required=""  name="remindnotes" id="remindnotes" style="height: 70px;line-height: 18px;overflow: auto;resize: none;width: 97.5%"><?php echo $podetails["remindnotes"] ?></textarea>
                                    </td>
                                </tr>


                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer" style="margin-bottom: -2px;text-align: center"> 
                <input type="submit" name="btnSubmit" class="btn btn-info" id="btnSubmit" value="SAVE"/>
                <a href="index.php?pagename=manage_perchaseorder" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
            </div>
            <hr>
        </div>
    </div>

</form>


<script>
    function validateQty(id) {
        var lastreceived = parseInt($("#lastreceived" + id).val());
        var received = parseInt($("#received" + id).val());
        if (received > lastreceived) {
            $("#received" + id).val("");
            $("#received" + id).focus();
        }
    }
</script>

<?php

function saveOrEditReceivingOrder() {
    $btnSubmit = filter_input(INPUT_POST, "btnSubmit");
    if ($btnSubmit == "SAVE") {
        $purchaseorderid = filter_input(INPUT_GET, "purchaseorderid");
        $poitemid = $_POST["poitemid"];
        $received = $_POST["received"];
        $reminderdate = MysqlConnection::convertToDBDate(filter_input(INPUT_POST, "reminderdate"));

        $remindnotes = filter_input(INPUT_POST, "remindnotes");
        echo $sqlquery = "UPDATE `purchase_order` SET "
        . " `reminderdate` = '$reminderdate',"
        . " `remindnotes` = '$remindnotes'"
        . " WHERE `id` = '$purchaseorderid'";
        MysqlConnection::delete($sqlquery);

        $shipping_address = filter_input(INPUT_POST, "shipping_address");
        $billing_address = filter_input(INPUT_POST, "billing_address");

        $podetails["shipping_address"] = MysqlConnection::formatToBRAddress($shipping_address);
        $podetails["billing_address"] = MysqlConnection::formatToBRAddress($billTo_address);




        $index = 0;
        foreach ($poitemid as $itemid) {
            $receivedqty = $received[$index];
            $poitems = MysqlConnection::fetchCustom("SELECT rqty FROM `purchase_item` WHERE `po_id` LIKE '$purchaseorderid' AND `item_id` = '$itemid' ");
            $itemdetails = MysqlConnection::getItemDataById($itemid);
            //update received in po
            MysqlConnection::delete("UPDATE purchase_item SET rqty = " . ($poitems[0]["rqty"] + $receivedqty ) . " WHERE `item_id` = '$itemid' AND po_id =  '$purchaseorderid' ");
            //update received in items
            MysqlConnection::delete("UPDATE item_master SET onhand = " . ($itemdetails["onhand"] + $receivedqty ) . " WHERE `item_id` = '$itemid'    ");
            $index++;

            $recitemarray["po_id"] = $purchaseorderid;
            $recitemarray["item_id"] = $itemid;
            $recitemarray["rqty"] = $receivedqty;
            $recitemarray["date"] = MysqlConnection::convertToDBDate(date("Y-m-d"));
            MysqlConnection::insert("tbl_receiving_items", $recitemarray);
        }

        $poclosed = "N";
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `purchase_item` WHERE po_id = '$purchaseorderid'");
        foreach ($resultset as $value) {
            $qty = $value["qty"];
            $rqty = $value["rqty"];
            if (($qty - $rqty) != 0) {
                $poclosed = "Y";
            }
        }

        MysqlConnection::delete("UPDATE purchase_order SET isOpen = '$poclosed' WHERE id = '$purchaseorderid' ");
        if ($poclosed == "Y") {
//            header("location:index.php?pagename=manage_perchaseorder&status=Y");
            header("location:index.php?pagename=manage_perchaseorder&action=received");
        } else {
            header("location:index.php?pagename=manage_perchaseorder");
        }
    }
}
?>

