<?php
$query = "SELECT * FROM `purchase_order` WHERE id !='' ";

$customer = filter_input(INPUT_POST, "customer");
$fromDate = filter_input(INPUT_POST, "fromDate");
$toDate = filter_input(INPUT_POST, "toDate");

$orderDate = $fromDate == "" ? "" : MysqlConnection::convertToDBDate($fromDate);
$deliveryDate = $toDate == "" ? "" : MysqlConnection::convertToDBDate($toDate);

$today = date("Y-m-d");
if (isset($customer) && $customer != "") {
    $query = $query . " AND supplier_id = '$customer'";
}
if (isset($orderDate) && $orderDate != "" && isset($deliveryDate) && $deliveryDate != "") {
    $query = $query . " AND purchasedate BETWEEN '$orderDate' AND '$deliveryDate'";
} else {
    if (isset($orderDate) && $orderDate != "") {
        $query = $query . " AND purchasedate BETWEEN '$orderDate' AND '$today'";
    }
    if (isset($deliveryDate) && $deliveryDate != "") {
        $query = $query . " AND purchasedate BETWEEN '$today' AND '$deliveryDate'";
    }
}
$listPerchaseOrders = MysqlConnection::fetchCustom($query);
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<script>
    $(function() {
        $("#datepicker1").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
        $("#datepicker2").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>
<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PURCHASE ORDER REPORT</h5>
    </div>
    <br/>

    <form name="frm" id="frm" method="post">
        <table class="display nowrap sortable" border="0" style="width: 100%">
            <tr>
                <td>Vendor Name</td>
                <td>&nbsp;:&nbsp;</td>
                <td>
                    <span  data-toggle="tooltip" data-original-title="Please select vendor in the list">
                        <select  style="width: 220px;height: 25px" name="customer" id="customer">
                            <option value="">SELECT</option>
                            <?php
                            $resultset = MysqlConnection::fetchCustom("SELECT supp_id,companyname FROM `supplier_master`");
                            foreach ($resultset as $key => $value) {

                                if ($value["supp_id"] == $customer) {
                                    $selected = "selected";
                                } else {
                                    $selected = "";
                                }
                                ?>
                                <option <?php echo $selected ?>  value="<?php echo $value["supp_id"] ?>"><?php echo $value["companyname"] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </span>
                </td>
                <td>From Date</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input style="width: 220px" placeholder="<?php echo $fromDate ?>" type="text" id="datepicker1" name="fromDate"/></td>
                <td>To Date</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input style="width: 220px" placeholder="<?php echo $toDate ?>"  type="text" id="datepicker2" name="toDate"/></td>
                <td  >
                    <input type="submit" value="Search" name="btnSearch"  class="btn btn-info">
                </td>
            </tr>
        </table>
    </form>

    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>

                <td ><i class="fa fa-fw fa-sort"></i>SO NO</td>
                <td >Client Name</td>

                <td ><i class="fa fa-fw fa-sort"></i>PO No</td>
                <td >Vendor Name</td>
                <td >PO IS?</td>
                <td >Ship Via</td>
                <td >Total Amount</td>
                <td >Delivery Date</td>
                <td >Entered By</td>
            </tr>
        </thead>

        <tbody>
            <?php
            $index = 1;
            foreach ($listPerchaseOrders as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $isOpen = $value["isOpen"] == "Y" ? "Open" : "Close";
                $isOpenclt = $value["isOpen"] == "Y" ? "btn-success" : "btn-warning";
                $supplierdetails = MysqlConnection::getSupplierDetails($value["supplier_id"]);
                ?>
                <tr id="<?php echo $value["id"] ?>" class="context-menu-one" onclick="setId('<?php echo $value["id"] ?>')" ondblclick ="viewPO('<?php echo $value["id"] ?>')" style="<?php echo $bgcolor ?> ;">

                    <td><?php echo $index ?></td>
                    <td><?php echo $value["c_sono"] ?></td>
                    <td><?php echo $value["c_name"] ?></td>
                    <td> <span  data-toggle="tooltip" data-original-title="View purchse order"><a href="index.php?pagename=view_perchaseorder&purchaseorderid=<?php echo $value["id"] ?>"><?php echo $value["purchaseOrderId"] ?></a></span></td>
                    <td > <span  data-toggle="tooltip" data-original-title="Go to vendor detail">
                        <a href="index.php?pagename=view_suppliermaster&supplierid=<?php echo $value["supplier_id"] ?>"> <?php echo $supplierdetails["companyname"] ?>&nbsp;</a>
                        </span>
                        <a href="tel:<?php echo $supplierdetails["supp_phoneNo"] ?>">
                            <?php echo $supplierdetails["supp_phoneNo"] ?>
                        </a>
                    </td>
                    <td><i class="<?php echo $isOpenclt ?>" style="padding: 2px 15px 2px 15px;"><?php echo $isOpen ?></i></td>
                    <td><?php echo $value["ship_via"] ?></td>
                    <td><?php echo $value["total"] ?>&nbsp;&nbsp;</td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($value["expected_date"]) ?>&nbsp;&nbsp;</td>
                    <td><a href="index.php?pagename=view_usermanagement&userid=<?php echo $value["added_by"] ?>"><?php echo MysqlConnection::getAddedBy($value["added_by"]) ?></a> </td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
    <hr/>
</div>
<script>
    function viewPO(id) {
        window.location = "index.php?pagename=view_perchaseorder&purchaseorderid=" + id;
    }

    $(document).ready(function() {
        $('#customer').select2();
    });

    $("#cancelct").click(function() {
        $("#customer").val("");
    });
    $("#customer").click(function() {
        var valueModel = $("#customer").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
</script>
