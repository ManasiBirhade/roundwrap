<?php
$query = "SELECT * FROM `packslip` WHERE quot_id != '' AND workOrd_Id = '' ";

$customer = filter_input(INPUT_POST, "customer");
$fromDate = filter_input(INPUT_POST, "fromDate");
$toDate = filter_input(INPUT_POST, "toDate");

$orderDate = $fromDate == "" ? "" : MysqlConnection::convertToDBDate($fromDate);
$deliveryDate = $toDate == "" ? "" : MysqlConnection::convertToDBDate($toDate);

$today = date("Y-m-d");
if (isset($customer) && $customer != "") {
    $query = $query . " AND cust_id = '$customer'";
}
if (isset($orderDate) && $orderDate != "" && isset($deliveryDate) && $deliveryDate != "") {
    $query = $query . " AND rec_date BETWEEN '$orderDate' AND '$deliveryDate'";
} else {
    if (isset($orderDate) && $orderDate != "") {
        $query = $query . " AND rec_date BETWEEN '$orderDate' AND '$today'";
    }
    if (isset($deliveryDate) && $deliveryDate != "") {
        $query = $query . " AND rec_date BETWEEN '$today' AND '$deliveryDate'";
    }
}

$listPackingSlip = MysqlConnection::fetchCustom($query);
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">
<script>
    $(function() {
        $("#datepicker1").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
        $("#datepicker2").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">QUOTATION REPORT</h5>
    </div>
    <br/>

    <form name="frm" id="frm" method="post">
        <table class="display nowrap sortable" border="0" style="width: 100%">
            <tr>
                <td>Customer Name</td>
                <td>&nbsp;:&nbsp;</td>
                <td>
                    <span  data-toggle="tooltip" data-original-title="Please select customer in the list">
                        <select style="width: 220px;height: 25px" name="customer" id="customer">
                            <option value="">SELECT</option>
                            <?php
                            $resultset = MysqlConnection::fetchCustom("SELECT id,cust_companyname FROM `customer_master`");
                            foreach ($resultset as $key => $value) {
                                if ($customer == $value["id"]) {
                                    $selected = "selected";
                                } else {
                                    $selected = "";
                                }
                                ?>
                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>"><?php echo $value["cust_companyname"] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </span>
                </td>
                <td>From Date</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input style="width: 220px" type="text" id="datepicker1" name="fromDate"/></td>
                <td>To Date</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input style="width: 220px" type="text" id="datepicker2" name="toDate"/></td>
                <td>
                    <input type="submit" value="Search" name="btnSearch"  class="btn btn-info">
                </td>
            </tr>
        </table>
    </form>

    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>
                <td >Company Name</td>
                <td >Profile Name</td>
                <td >SO No</td>
                <td >PO No</td>
                <td >Rec Date</td>
                <td >Req Date</td>
                <td >Is Active?</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listPackingSlip as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $customername = MysqlConnection::fetchCustom("SELECT cust_companyname FROM customer_master WHERE id = '" . $value["cust_id"] . "'");
                $portfolioname = MysqlConnection::fetchCustom("select portfolio_name,profile_name from tbl_portfolioprofile where id = '" . $value["prof_id"] . "'");
                ?>
                <tr id="<?php echo $value["ps_id"] ?>" class="context-menu-one" onclick="setId('<?php echo $value["ps_id"] ?>')" ondblclick ="viewPO('<?php echo $value["id"] ?>')" style="<?php echo $bgcolor ?> ;">

                    <td ><?php echo $index ?></td>
                    <td ><span  data-toggle="tooltip" data-original-title="Go to customer details">
                            <a href="index.php?pagename=view_customermaster&customerId=<?php echo $value["cust_id"] ?>"><?php echo $customername[0]["cust_companyname"] ?></a>
                        </span>
                    </td>
                    <td ><span  data-toggle="tooltip" data-original-title="Go to profile details">
                            <a href="index.php?pagename=view_profilemaster&id=<?php echo $value["prof_id"] ?>"><?php echo $portfolioname[0]["portfolio_name"] . " - " . $portfolioname[0]["profile_name"] ?></a>
                        </span>
                    </td>
                    <td ><?php echo $value["so_no"] ?></td>
                    <td ><?php echo $value["po_no"] ?></td>
                    <td ><?php echo MysqlConnection::convertToPreferenceDate($value["rec_date"]) ?></td>
                    <td ><?php echo MysqlConnection::convertToPreferenceDate($value["req_date"]) ?></td>
                    <td ><?php echo $value["status"] == "Y" ? "<span class='badge badge-success'>YES</span>" : "<span class='badge badge-info'>NO</span>" ?></td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
</div>
<script>
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_quotation&psid=" + id;
        }
    });

    $(document).ready(function() {
        $('#customer').select2();
    });

    $("#cancelct").click(function() {
        $("#customer").val("");
    });
    $("#customer").click(function() {
        var valueModel = $("#customer").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
</script>