<?php
$query = "SELECT * FROM `sales_order` WHERE id !='' ";

$customer = filter_input(INPUT_POST, "customer");
$fromDate = filter_input(INPUT_POST, "fromDate");
$toDate = filter_input(INPUT_POST, "toDate");




$orderDate = $fromDate == "" ? "" : MysqlConnection::convertToDBDate($fromDate);
$deliveryDate = $toDate == "" ? "" : MysqlConnection::convertToDBDate($toDate);

$today = date("Y-m-d");
if (isset($customer) && $customer != "") {
    $query = $query . " AND customer_id = '$customer'";
}
if (isset($orderDate) && $orderDate != "" && isset($deliveryDate) && $deliveryDate != "") {
    $query = $query . " AND soorderdate BETWEEN '$orderDate' AND '$deliveryDate'";
} else {
    if (isset($orderDate) && $orderDate != "") {
        $query = $query . " AND soorderdate BETWEEN '$orderDate' AND '$today'";
    }
    if (isset($deliveryDate) && $deliveryDate != "") {
        $query = $query . " AND soorderdate BETWEEN '$today' AND '$deliveryDate'";
    }
}

$listSalesOrders = MysqlConnection::fetchCustom($query);
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<script>
    $(function() {
        $("#datepicker1").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
        $("#datepicker2").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });


</script>
<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">SALES ORDER REPORT</h5>
    </div>
    <br/>

    <form name="frm" id="frm" method="post">
        <table class="display nowrap sortable" border="0" style="width: 100%">
            <tr>
                <td style="width: 10%">CUSTOMER NAME</td>
                <td>&nbsp;:&nbsp</td>
                <td >
                    <span  data-toggle="tooltip" data-original-title="Please select customer in the list">
                        <select autofocus="" style="width: 225px;height: 25px" name="customer" id="customer" >
                            <option value="">SELECT</option>
                            <option value="">SELECT</option>
                            <?php
                            $resultset = MysqlConnection::fetchCustom("SELECT id,cust_companyname FROM `customer_master`");
                            foreach ($resultset as $key => $value) {
                                if ($customer == $value["id"]) {
                                    $selected = "selected";
                                } else {
                                    $selected = "";
                                }
                                ?>
                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>"><?php echo $value["cust_companyname"] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </span>
                </td>
                <td>From Date</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input style="width: 220px" placeholder="<?php echo $fromDate ?>" type="text" <?php echo $fromDate ?> id="datepicker1" name="fromDate"/></td>
                <td>To Date</td>
                <td>&nbsp;:&nbsp;</td>
                <td><input style="width: 220px"  placeholder="<?php echo $toDate ?>"   type="text" <?php echo $toDate ?> id="datepicker2" name="toDate"/></td>
                <td  >
                    <input  type="submit" value="Search" name="btnSearch"  class="btn btn-info">
                </td>
            </tr>
        </table>
    </form>

    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>
                <td><i class="fa fa-fw fa-sort"></i>SO No</td>
                <td><i class="fa fa-fw fa-sort"></i>PO No</td>
                <td>Customer Name</td>
                <td>SO IS?</td>
                <td>Ship Via</td>
                <td>Total Amount</td>
                <td>Delivery Date</td>
                <td>Entered By</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listSalesOrders as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $isOpen = $value["isOpen"] == "Y" ? "Open" : "Close";
                $isOpenclt = $value["isOpen"] == "Y" ? "btn-success" : "btn-warning";
                $customerdetails = MysqlConnection::getCustomerDetails($value["customer_id"]);
                ?>
                <tr id="<?php echo $value["id"] ?>" class="context-menu-one"  ondblclick ="viewPO('<?php echo $value["id"] ?>')" style="<?php echo $bgcolor ?> ;">
                    <td><?php echo $index ?></td>
                    <td> <span  data-toggle="tooltip" data-original-title="View sales order"><a href="index.php?pagename=view_salesorder&salesorderid=<?php echo $value["id"] ?>"><?php echo $value["sono"] ?></a></span></td>
                    <td><?php echo $value["pono"] ?></td>
                    <td> <span  data-toggle="tooltip" data-original-title="Go to customer detail">
                        <a href="index.php?pagename=view_customermaster&customerId=<?php echo $value["customer_id"] ?>"> <?php echo $customerdetails["cust_companyname"] ?>&nbsp;</a>
                        </span>
                        <a href="tel:<?php echo $customerdetails["phno"] ?>">
                            <?php echo $customerdetails["phno"] ?>
                        </a>
                    </td>
                    <td><i class="<?php echo $isOpenclt ?>" style="padding: 2px 15px 2px 15px;"><?php echo $isOpen ?></i></td>
                    <td><?php echo $value["shipvia"] ?></td>
                    <td><?php echo $value["total"] ?></td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($value["expected_date"]) ?></td>
                    <td><a href="index.php?pagename=view_usermanagement&userid=<?php echo $value["added_by"] ?>"><?php echo MysqlConnection::getAddedBy($value["added_by"]) ?></a> </td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
    <hr/>
</div>
<script>
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_salesorder&salesorderid=" + id;
        }
    });

    $(document).ready(function() {
        $('#customer').select2();
    });

    $("#cancelct").click(function() {
        $("#customer").val("");
    });
    $("#customer").click(function() {
        var valueModel = $("#customer").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
</script>
