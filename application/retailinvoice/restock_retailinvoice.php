<?php
$salesorderid = filter_input(INPUT_GET, "salesorderid");
$invoiceno = filter_input(INPUT_GET, "invoiceno");


$sqlquery = "SELECT DISTINCT(reinvoice) as reinvoice FROM `tbl_restockinvoice_history`"
        . " WHERE `invoice` = '" . $invoiceno . "' AND `so_id` = '$salesorderid'";
$invoicearray = MysqlConnection::fetchCustom($sqlquery);

?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">CREDIT NOTES</h5>
    </div>
    <br/>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Invoice Number</td>
                <td>Credit Note</td>
                <td>SO NO</td>
                <td>PO NO</td>
                <td>Customer Name</td>
                <td>Date</td>
            </tr>
        </thead>
        <tbody >
            <?php
            $index = 1;
            foreach ($invoicearray as $value1) {
                $reinvoice = $value1["reinvoice"];
                $resultset = MysqlConnection::fetchCustom("SELECT reinvoice,invoice FROM `tbl_restockinvoice_history` WHERE reinvoice = '$reinvoice'  ");
                $value = $resultset[0];


                $reinvoice = $value["reinvoice"];
                $invoice = $value["invoice"];
                $resultofinvoice = MysqlConnection::fetchCustom("SELECT so_id,date FROM tbl_selling_history WHERE invoice = '$invoice' ");
                $soid = $resultofinvoice[0]["so_id"];
                $date = explode(" ", $resultofinvoice[0]["date"]);

                $retockinvoicecount = getRestockInvoiceCount($value, $soid);

                $salesorder = getSalesOrder($soid);
                $isRestock = $salesorder["isRestock"];
                $customername = getCustomerName($salesorder["customer_id"]);
                ?>
                <tr id="<?php echo $soid ?>"  class="context-menu-one">
                    <td style="width: 25px"><?php echo $index++ ?></td>
                    <td><span  data-toggle="tooltip" data-original-title="click here to print invoice">
                            <a target="_blank" href="invoice/print_salesinvoice.php?invoiceno=<?php echo $invoice ?>">
                                <?php echo $invoice ?>
                            </a>
                        </span>
                    </td>
                    <td>
                        <span  data-toggle="tooltip" data-original-title="click here to print credit note">
                            <a target="_blank" href="invoice/print_creditnote.php?invoiceno=<?php echo $reinvoice ?>">
                                <?php echo $value["reinvoice"] ?>
                            </a>
                        </span>
                    </td>
                    <td><?php echo $salesorder["sono"] ?></td>
                    <td><?php echo $salesorder["pono"] ?></td>
                    <td><?php echo $customername ?></td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($date[0]) ?></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" style="margin-left:10px;">EMAIL</button>
        </div> 
    </div>
</div>

<?php

function getSalesOrder($sonumber) {
    $query = "SELECT sono,pono,customer_id,isRestock FROM `sales_order` WHERE `id` = '$sonumber' ";
    $result = MysqlConnection::fetchCustom($query);
    return $result[0];
}

function getCustomerName($customer) {
    $query = "SELECT cust_companyname FROM `customer_master` WHERE `id` = '$customer' ";
    $result = MysqlConnection::fetchCustom($query);
    return $result[0]["cust_companyname"];
}
?>
<script>
    $('#example tbody tr').click(function(e) {
        var id = $(this).attr('id');
        var crnumber = $(this).find("td:eq(2)").text();
        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var del = '<a href="invoice/print_creditnote.php?invoiceno=' + crnumber.trim() + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var em = '<a href="index.php?pagename=emailcr_retailinvoice&invoiceno=' + crnumber.trim() + '&status=email_retailinvoice" ><button type="button" class=" btn btn-info" style="margin-left:10px;">EMAIL</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + del + em + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });


//    $(function() {
//        $.contextMenu({
//            selector: '.context-menu-one',
//            callback: function(key, options) {
//                var m = "clicked row: " + key;
//                var id = $(this).attr('id');
//                switch (key) {
//
//                    case "return_goods":
//                        window.location = "index.php?pagename=restock_salesorder&salesorderid=" + id;
//                        break;
//
//                    case "print_creditnote":
//                        window.open("invoice/print_creditnote.php?id=" + id, '_blank');
//                        break;
//
//                    case "quit":
//                        window.location = "index.php?pagename=manage_dashboard";
//                        break;
//                    default:
//                        window.location = "index.php?pagename=manage_salesorder";
//                }
//                //window.console && console.log(m) || alert(m+"    id:"+id); 
//            },
//            items: {
////                "view_salesorder": {name: "VIEW ORDER", icon: "+"},
//                //                "edit_salesorder": {name: "EDIT ORDER", icon: "context-menu-icon-add"},
//                //                "delete_salesorder": {name: "DELETE ORDER", icon: ""},
////                "sep1": "---------"       "view_salesorder": {name: "VIEW ORDER", icon: "+"},
////                "edit_salesorder": {name: "EDIT ORDER", icon: "context-menu-icon-add"},
//                //                "delete_salesorder": {name: "DELETE ORDER", icon: ""},,
//                //"create_salesorder": {name: "ENTER BACK ORDER", icon: ""},
//                "return_goods": {name: "RETURN GOODS", icon: ""},
//                //                "print_creditnote": {name: "PRINT CREDIT NOTE", icon: ""},
//                //                "return_salesorder": {name: "CANCEL SALES ORDER", icon: ""},
//                //                "sep3": "---------",
//                //"print_back": {name: "PRINT BACK ORDER", icon: ""},
//
//                //                "create_invoice": {name: "CREATE INVOICE", icon: ""},
//                "sep2": "---------",
//                "quit": {name: "QUIT", icon: function() {
//                        return '';
//                    }}
//            }
//        });
//    });

    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_salesorder&salesorderid=" + id;
        }
    });

</script>

<?php

function getRestockInvoiceCount($value, $soid) {
    $sqlquery = "SELECT count(id) as count FROM `tbl_restockinvoice_history` WHERE `invoice` = '" . $value["invoice"] . "' AND `so_id` = '$soid'";
    $resultset = MysqlConnection::fetchCustom($sqlquery);
    return $resultset[0]["count"];
}
