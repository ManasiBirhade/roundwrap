<?php
$salesorderid = filter_input(INPUT_GET, "salesorderid");

if (isset($salesorderid) && $salesorderid != "") {
    $query = "SELECT DISTINCT(invoice) as invoice"
            . " FROM `tbl_selling_history`"
            . " WHERE `so_id` = '$salesorderid' "
            . " ORDER BY `tbl_selling_history`.`date`"
            . " DESC LIMIT 0,100";
} else {
    $query = "SELECT DISTINCT(invoice) as invoice"
            . "  FROM `tbl_selling_history`"
            . " ORDER BY `tbl_selling_history`.`date`"
            . " DESC LIMIT 0,100";
}
$invoicearray = MysqlConnection::fetchCustom($query);
$action = filter_input(INPUT_GET, "action");
$invoiceno = filter_input(INPUT_GET, "invoiceno");

if ($action == "print") {
    echo "<script language='javascript'>window.open('invoice/print_creditnote.php?invoiceno=$invoiceno');</script>";
} else {
    
}
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">RETAIL INVOICE</h5>
    </div>
    <br/>
    <table style="width: 100%" border="0">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=manualrestock_salesorder" >&nbsp;CREATE CREDIT NOTE</a>
            </td>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=view_retailinvoice" >&nbsp;REFRESH</a>
            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%;">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Invoice Number</td>
                <td>Credit Note</td>
                <td>SO NO</td>
                <td>PO NO</td>
                <td>Customer Name</td>
                <td>Date</td>

            </tr>
        </thead>
        <tbody >
            <?php
            $index = 1;
            $array = array();
            foreach ($invoicearray as $value) {
                $invoice = $value["invoice"];
                //if (!in_array($invoice, $array)) {
                array_push($array, $invoice);
                $resultofinvoice = MysqlConnection::fetchCustom("SELECT so_id,date FROM tbl_selling_history WHERE invoice = '$invoice' ");
                $soid = $resultofinvoice[0]["so_id"];
                $date = explode(" ", $resultofinvoice[0]["date"]);

                $retockinvoicecount = getRestockInvoiceCount($value, $soid);

                $salesorder = getSalesOrder($soid);
                $isRestock = $salesorder["isRestock"];
                $isOpen = $isRestock == "Y" ? "Yes" : "No";
                $isOpenclt = $isRestock == "Y" ? "btn btn-success btn-mini" : "btn-warning  btn-mini";
                $customername = getCustomerName($salesorder["customer_id"]);
                ?>
                <tr id="<?php echo $soid ?>_<?php echo $invoice ?>"  class="context-menu-one">
                    <td style="width: 25px"><?php echo $index++ ?></td>
                    <td>
                        <span  data-toggle="tooltip" data-original-title="Click here to print invoice">
                            <a target="_blank" href="invoice/print_salesinvoice.php?invoiceno=<?php echo $invoice ?>">
                                <?php echo $invoice ?>
                            </a>
                        </span>
                    </td>
                    <td><span  data-toggle="tooltip" data-original-title="View Credit note">
                            <a href="index.php?pagename=restock_retailinvoice&salesorderid=<?php echo $soid ?>&invoiceno=<?php echo $invoice ?>">
                                <?php echo $retockinvoicecount ?>
                            </a></span>
                    </td>
                    <td><?php echo $salesorder["sono"] ?></td>
                    <td><?php echo $salesorder["pono"] ?></td>
                    <td><?php echo $customername ?></td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($date[0]) ?></td>

                </tr>
                <?php
            }
            //}
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" style="margin-left:10px;">EMAIL</button>
        </div> 
    </div>
</div>

<?php

function getSalesOrder($sonumber) {
    $query = "SELECT sono,pono,customer_id,isRestock FROM `sales_order` WHERE `id` = '$sonumber' ";
    $result = MysqlConnection::fetchCustom($query);
    return $result[0];
}

function getCustomerName($customer) {
    $query = "SELECT cust_companyname FROM `customer_master` WHERE `id` = '$customer' ";
    $result = MysqlConnection::fetchCustom($query);
    return $result[0]["cust_companyname"];
}
?>
<script>
    $('#example tbody tr').click(function (e) {
        var id = $(this).attr('id');
        var splitnumber = id.split("_");
        var invoicenumber = splitnumber[1];
        var id = splitnumber[0];

        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var del = '<a href="invoice/print_salesinvoice.php?invoiceno=' + invoicenumber + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var em = '<a href="index.php?pagename=email_retailinvoice&salesorderid=' + id + '&invoiceno=' + invoicenumber + '&status=email_retailinvoice" ><button type="button" class=" btn btn-info" style="margin-left:10px;">EMAIL</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + del + em + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });

    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "return_goods":
                        window.location = "index.php?pagename=restock_salesorder&salesorderid=" + id;
                        break;
                    case "print_creditnote":
                        window.open("invoice/print_creditnote.php?id=" + id, '_blank');
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    default:
                        window.location = "index.php?pagename=manage_salesorder";
                }
            },
            items: {
                "return_goods": {name: "RETURN GOODS", icon: ""},
                "sep2": "---------",
                "quit": {name: "QUIT", icon: function () {
                        return '';
                    }}
            }
        });
    });



</script>

<?php

function getRestockInvoiceCount($value, $soid) {
    $sqlquery = "SELECT COUNT(DISTINCT(reinvoice)) as reinvoice FROM `tbl_restockinvoice_history`"
            . " WHERE `invoice` = '" . $value["invoice"] . "' AND `so_id` = '$soid'";
    $resultset = MysqlConnection::fetchCustom($sqlquery);
    return $resultset[0]["reinvoice"];
}
