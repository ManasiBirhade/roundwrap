<?php
error_reporting(0);

$action = filter_input(INPUT_GET, "action");

if (MysqlConnection::validateItems()) {
    header("location:index.php?pagename=success_itemmaster&flag=noitems");
}
$customerlist = MysqlConnection::fetchCustom("SELECT id,cust_companyname FROM `customer_master` WHERE status = 'Y' ORDER BY `cust_companyname` ASC");
$customer = MysqlConnection::getCustomerDetails(filter_input(INPUT_GET, "customerId"));
if (isset($customer)) {
    $representivename = MysqlConnection::getGenericById($customer["sales_person_name"]);
}

$salesorderid = filter_input(INPUT_GET, "salesorderid");
if (!isset($salesorderid) || $salesorderid == "") {
    $sonumber = MysqlConnection::generateNumber("retailso");
}

$sqlitemarray = MysqlConnection::fetchCustom("SELECT count(id) as counter FROM sales_order");
$itemarray = MysqlConnection::fetchCustom("SELECT * FROM item_master;");
$buildauto = buildauto(MysqlConnection::fetchCustom("SELECT item_id ,item_code,item_desc_purch, item_name FROM item_master;"));
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY taxtype ASC ;");

$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit)) {
    saveCustomer(filter_input_array(INPUT_POST));
}


/*
 * Edit sales order logic
 */

$salesorderidget = filter_input(INPUT_GET, "salesorderid");

if (isset($salesorderidget)) {
    $soorderdetails = MysqlConnection::getSalesOrderDetailsById($salesorderidget);

    if ($soorderdetails["isOpen"] == "N") {
        header("location:index.php?pagename=manage_salesorder&status=Y&action=closederror");
    }

    $soitemsdetails = MysqlConnection::getSalesItemsDetailsById($salesorderidget);
    $sonumber = $soorderdetails["sono"];
    $soCustomer = MysqlConnection::getCustomerDetails($soorderdetails["customer_id"]);
    $representivename = MysqlConnection::getGenericById($soCustomer["sales_person_name"]);
}

saveOrEditSalesOrder();

$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");

$_SESSION["navigationpage"] = "index.php?pagename=create_salesorder";
?>
<?php

function saveCustomer($filterarray) {
    unset($filterarray["btnSubmit"]);
    if ($filterarray["salutation"] != null && salutation !== '') {
        unset($filterarray["salutation1"]);
    } else {
        $filterarray["salutation"] = $filterarray["salutation1"];
        unset($filterarray["salutation1"]);
    }
    MysqlConnection::insert("customer_master", $filterarray);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="js/script.js"></script>

<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">

<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });</script>
<script>
    $(function () {
        for (var index = 1; index <= 30; index++) {
            $("#tags" + index).autocomplete({
                source: "itemmaster/autoitemajax.php",
            });
        }
    });


    $(document).ready(function ($) {
        $("#phno").mask("(999) 999-9999");
        $("#cust_fax").mask("(999) 999-9999");

    });

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({
            placement: 'bottom'
        });
    });
</script>
<style>
    input,textarea,date{ width: 80%;height: 30px; }
    select{ width: 81.4%;height: 30px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    .ui-autocomplete {
        max-height: 300px;
        overflow-y: auto;   /* prevent horizontal scrollbar */
        overflow-x: hidden; /* add padding to account for vertical scrollbar */
        z-index:1000 !important;
    }
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <?php
        if ($action == "created") {
            echo MysqlConnection::$MSG_AD;
        }
        ?>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANAGE SALES ORDER</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="display nowrap sortable" style="width: 100%">
                            <tr style="font-weight: bold; ">

                                <td style="width: 10%">CUSTOMER NAME<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td >
                                    <span  data-toggle="tooltip" data-original-title="Please Select Customer in the list">
                                        <?php
                                        if (!isset($salesorderid) || $salesorderid == "") {
                                            ?>

                                            <select autofocus="" style="width: 225px;height: 25px" name="customer_id" id="customer_id" required="" >
                                                <option value="">SELECT</option>
                                                <option value="1"><< ADD NEW >></option>
                                                <?php
                                                foreach ($customerlist as $key => $value) {
                                                    if (isset($soorderdetails)) {
                                                        $selected = $value["id"] == $soorderdetails["customer_id"] ? "selected" : "";
                                                    }
                                                    if (isset($customer)) {
                                                        $selected = $value["id"] == $customer["id"] ? "selected" : "";
                                                    }
                                                    ?>
                                                    <option <?php echo $selected ?> value="<?php echo $value["id"] ?>"><?php echo $value["cust_companyname"] ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php
                                        } else {
                                            $customername = "";
                                            foreach ($customerlist as $customer) {
                                                if ($customer["id"] == $soorderdetails["customer_id"]) {
                                                    $customername = $customer["cust_companyname"];
                                                }
                                            }
                                            ?>
                                            <input style="width: 220px" type="text" value="<?php echo $customername ?>" readonly="">
                                            <?php
                                        }
                                        ?>
                                    </span>
                                </td>
                                <td><b>SALES DATE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <input style="width: 220px" type="text" name="salesdate" value="<?php echo MysqlConnection::convertToPreferenceDate(filter_input(INPUT_COOKIE, "dateme")) ?>" readonly="">
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                            <tr>
                                <td><b>PO NUMBER</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td title="Please enter purchase order number"><input style=" width: 220px" type="text" name="pono" id="pono" value="<?php echo $soorderdetails["pono"] ?>" maxlength="150" autofocus="" ></td>
                                <td style="width: 10%">SHIP VIA<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <span data-toggle="tooltip" data-original-title="Please Select delivery service from the list">
                                        <select style="width: 225px;height: 25px;text-transform: capitalize" name="shipvia" id="shipvia" required="true"> 
                                            <option value="">&nbsp;&nbsp;</option>
                                            <option value="0" ><< ADD NEW >></option>
                                            <?php foreach ($shipviainfo as $key => $value) { ?>
                                                <option <?php echo $value["name"] == $soorderdetails["shipvia"] ? "selected" : "" ?>
                                                    value="<?php echo $value["name"] ?>"  ><?php echo $value["name"] ?></option>
                                                <?php } ?>
                                        </select>
                                    </span>
                                    <div >
                                        <label style="width: 50%">
                                            <input type="radio" name="radios" onclick="checkForShipVia('collect')" <?php echo $soorderdetails["radios"] == "collect" ? "checked" : "" ?> value="collect"  style="">&nbsp;Collect
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <label style="width: 50%">
                                            <input type="radio" name="radios"  onclick="checkForShipVia('prepaid')" checked=""   <?php echo $soorderdetails["radios"] == "prepaid" ? "checked" : "" ?> value="prepaid" >&nbsp;Prepaid
                                        </label>
                                    </div>
                                </td>
                                <td style="width: 10%">EXPECTED&nbsp;DELIVERY<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <?php
                                    if (isset($salesorderid)) {
                                        $id = "id=''";
                                    } else {
                                        $id = "id='datepicker'";
                                    }
                                    ?>
                                    <input  style="width: 220px" type="text" required="true" name="expected_date"  <?php echo $id ?> value="<?php echo MysqlConnection::convertToPreferenceDate($soorderdetails["expected_date"]) ?>" >
                                    <input type="hidden" id="hidexpected_date" value="<?php echo MysqlConnection::convertToPreferenceDate($soorderdetails["expected_date"]) ?>">
                                </td>
                            </tr>
                            <tr>
                                <td >BILLING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea readonly=""  style="line-height: 18px;width: 220px;height: 72px;resize: none" name="billTo_address" id="billTo_address"><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $soorderdetails["billTo_address"])) ?></textarea></td>
                                <td>SHIPPING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea style="line-height: 18px;width: 220px;height: 72px;resize: none" name="shipping_address" id="shipping_address"><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $soorderdetails["shipping_address"])) ?></textarea></td>
                                <td >REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea  style="line-height: 18px; color: red ; width: 220px;height: 72px;resize: none" name="remark"><?php echo $soorderdetails["remark"] ?></textarea></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 70%;float: left">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style=" background-color: #A9CDEC;color: white">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 300px;">ITEM CODE / DESCRIPTION</td>
                                    <td style="width: 70px;">UNIT</td>
                                    <td style="width: 70px;">
                                        <div id="pricediscount">PRICE</div>
                                        <input type="hidden" name="pricediscountvalue" id="pricediscountvalue">
                                    </td>
                                    <td style="width: 80px;">ON HAND</td>
                                    <td style="width: 80px;">ON SALE</td>
                                    <td style="width: 50px;">QTY</td>
                                    <td>AMOUNT</td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 290px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $indexedit = 1;
                                    foreach ($soitemsdetails as $soitem) {
                                        $value = MysqlConnection::getItemDataById($soitem["item_id"]);
                                        ?>
                                        <tr id="<?php echo $indexedit ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $indexedit ?>')"></a>
                                            </td>
                                            <td style="width: 300px" >
                                                <input type="text" name="items[]" value="<?php echo $value["item_code"] . " __ " . $value["item_desc_purch"] ?>" id="tags<?php echo $indexedit ?>" onfocusout="setDetails('<?php echo $indexedit ?>')"  style="padding: 0px;margin: 0px;width: 100%">
                                            </td>
                                            <td style="width: 70px;"><div id="unit<?php echo $indexedit ?>"><?php echo $value["unit"] ?></div></td>
                                            <td style="width: 70px;">
                                                <input type="text" name="price[]" id="price<?php echo $indexedit ?>" value="<?php echo $soitem["price"] ?>">
                                                <div id="price<?php // echo $indexedit             ?>"><?php // echo $value["total_sales_rate"]             ?></div>
                                            </td>
                                            <td style="width: 80px;"><div id="onhand<?php echo $indexedit ?>"><?php echo $value["onhand"] ?></div></td>
                                            <td style="width: 80px;"><div id="totalvalue<?php echo $indexedit ?>"><?php echo $value["totalvalue"] ?></div></td>
                                            <td style="width: 50px;">
                                                <input type="number" name="itemcount[]" value="<?php echo $soitem["qty"] ?>" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="doCalculation('<?php echo $indexedit ?>')" id="amount<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%">
                                                <input type="hidden" name="itemcountoriginal[]" value="<?php echo $soitem["qty"] ?>">
                                            </td>
                                            <td ><div id="total<?php echo $indexedit ?>"><?php echo $soitem["qty"] * $value["total_sales_rate"] ?></div></td>
                                        </tr>
                                        <?php
                                        $indexedit++;
                                    }
                                    for ($index = $indexedit; $index <= 50; $index++) {
                                        if (count($soitemsdetails) == 0) {
                                            if ($index == 1) {
                                                $required = "required";
                                            } else {
                                                $required = "";
                                            }
                                        }
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $index ?>')"></a>
                                            </td>
                                            <td style="width: 300px">
                                                <input type="text" name="items[]" <?php echo $required ?> id="tags<?php echo $index ?>" onfocusout="setDetails('<?php echo $index ?>')"  style="padding: 0px;margin: 0px;width: 100%">
                                            </td>
                                            <td style="width: 70px;"><div id="unit<?php echo $index ?>"></div></td>
                                            <td style="width: 70px;">
                                                <input type="text" name="price[]" pattern="^\d*(\.\d{0,2})?$" onkeypress="return chkNumericKey(event)" onfocusout="checkLessValue('price<?php echo $index ?>')" id="price<?php echo $index ?>" >
                                                <!--<div id="price<?php echo $index ?>"></div>-->
                                            </td>
                                            <td style="width: 80px;"><div id="onhand<?php echo $index ?>"></div></td>
                                            <td style="width: 80px;"><div id="totalvalue<?php echo $index ?>"></div></td>
                                            <td style="width: 50px;"><input type="text" name= "itemcount[]" maxlength="6"  onkeypress="return !(event.charCode == 46)" step="1"  onfocusout="doCalculation('<?php echo $index ?>')" id="amount<?php echo $index ?>" style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><div id="total<?php echo $index ?>"></div></td>
                                        </tr>
                                    <?php } ?>

                                </table>
                            </div>
                        </div>
                        <style>
                            b{
                                color: #000000;
                                font-weight: normal;
                            }
                        </style>
                        <div style="width: 28%;float: right">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <tr style="width: 150px;">
                                    <td><b>Sales Person </b></td>
                                    <td colspan="2">
                                        <input  style="width: 98%" type="text" name="representative" id="representative" value="<?php echo $representivename["name"] . " " . $representivename["description"] ?>" readonly="">
                                    </td>
                                </tr>

                                <tr >
                                    <td  style="width: 100px;"><b>Goods Total</b></td>
                                    <td colspan="2">
                                        <input type="text"  style="width: 98%" id="itemtotal" name="itemtotal" value="<?php echo $soorderdetails["sub_total"] ?>">
                                    </td>
                                </tr>
                                <tr >
                                    <td  style="width: 100px;"><b>Discount %</b></td>
                                    <td>
                                        <input  style="width: 98%" type="text" id="discount"  value="<?php echo $soorderdetails["discount"] ?>" name="discount"  maxlength="3" onfocusout="doCalculation()" name="discount" >
                                    </td>
                                    <td>
                                        <input  style="width: 96%" type="text" id="discountvalue"  value="<?php echo $soorderdetails["discountvalue"] ?>" name="discountvalue" readonly="">
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Goods Tax</b></td>
                                    <td>
                                        <select  name="taxid" id="taxid" required="" onchange="doCalculation()" style="height: 25px;width: 98%">
                                            <option value="">SELECT</option>
                                            <?php
                                            foreach ($sqltaxinfodata as $key => $value) {
                                                if ($soorderdetails["taxid"] == $value["id"]) {
                                                    $selected = "selected";
                                                } else {
                                                    $selected = "";
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>">
                                                    <?php echo ucwords($value["taxtype"]) ?>
                                                    <?php echo $value["taxname"] ?>-<?php echo $value["taxvalues"] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td> 
                                    <td>
                                        <input type="text" id="taxamount" readonly=""   value="<?php echo $soorderdetails["taxamount"] ?>" name='taxamount' style="width: 96%" >
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Sub Total</b></td>
                                    <td colspan="2"><input type="text" id="finaltotal"  style="width: 98%" value="<?php echo $soorderdetails["taxtotal"] ?>" onkeypress="return chkNumericKey(event)" name="sub_total" readonly=""></td>
                                </tr>
                                <tr >
                                    <td><b>Shipping Charges</b></td>
                                    <td colspan="2"><input type="text" id="ship_charge" onfocusout="doCalculation()"  style="width: 98%" value="<?php echo $soorderdetails["ship_charge"] == "" ? "0" : $soorderdetails["ship_charge"] ?>" maxlength="6"  onkeypress="return chkNumericKey(event)" name="ship_charge" value="<?php echo $customer["ship_charge"] ?>"  ></td>
                                </tr>
                                <tr >
                                    <td><b>Ship Tax</b></td>
                                    <td >
                                        <select id="staxname" name='staxname' style="height: 25px;width: 98%" >

                                            <option value="GST-5" <?php echo $soorderdetails["staxname"] == "GST-5" ? "selected" : "" ?> >GST-5</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="staxpercent" readonly=""  style="width: 96%" value="<?php echo $soorderdetails["staxpercent"] ?>" name='staxpercent'  onkeypress="return chkNumericKey(event)" style="width: 27%" >
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Net Total</b></td>
                                    <td colspan="2">
                                        <input type="text" id="nettotal" readonly="" value="<?php echo $soorderdetails["total"] ?>"  name="total" name="nettotal"  style="width: 98%">
                                        <input type="hidden" value="<?php echo MysqlConnection::getAddedBy($_SESSION["user"]["user_id"]); ?>" readonly="" >
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>
                    <input type="submit" id="btnSaveSalesOrder" name="btnSaveSalesOrder" class="btn btn-info" value="SAVE">
                    <input type="submit" id="btnSaveNextSalesOrder" name="btnSaveNextSalesOrder" class="btn btn-info" value="SAVE AND NEW"/>
                    <input type="submit" id="printButton" name="printButton" class='btn btn-info' value="PRINT"/>
                    <input type="submit" id="mailButton" name="mailButton" class='btn btn-info' style="margin-left:4px;" value="EMAIL"/>
                    <a href="index.php?pagename=manage_salesorder" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                </center>
            </div>
            <input type="hidden" name="onhand" id="onhand">

            <input type="hidden" name="tempItemPrice" id="tempItemPrice">
            <input type="hidden" name="isPrepaid" id="isPrepaid">
            <input type="hidden" name="isCollect" id="isCollect">
            <input type="hidden" name="creditlimit" id="creditlimit">

            <input type="hidden" name="oldPrice" id="oldPrice">
            <input type="hidden" name="newPrice" id="newPrice">
            <input type="hidden" name="currentIndex" id="currentIndex">

        </div>
    </div>

</form>

<?php

function buildauto($itemarray) {
    $option = "";
    foreach ($itemarray as $value) {
        $item_desc_purch1 = str_replace("\"", "", $value["item_desc_purch"]);
        $item_desc_purch2 = str_replace("'", "", $item_desc_purch1);
        $item_desc_purch3 = str_replace("''", "", $item_desc_purch2);
        $item_code1 = str_replace("\"", "", $value["item_code"]);
        $item_code2 = str_replace("'", "", $item_code1);
        $item_code3 = str_replace("''", "", $item_code2);
        $option .= "\"" . $item_code3 . " __ " . preg_replace('!\s+!', ' ', trim($item_desc_purch3)) . "\",";
    }

    return $option;
}
?>
<div id="duplicatecustomer" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <h3>ALERT !!!</h3>
    </div>
    <div class="modal-body">
        <h5 style="color: red">
            <div id="nextprice"></div><br/>
        </h5>
    </div>
    <div class="modal-footer"> 
        <a  onclick="setPrice('old')" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">SET ORIGINAL</a> 
        <a  onclick="setPrice('new')"  data-dismiss="modal" data-dismiss="modal" class="btn btn-info">SET NEW</a> 
        <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
    </div>
</div>
<!-- this is custom model dialog --->
<div id="shipviamodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD SHIP VIA</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="ship_via" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>SHIP VIA NAME<?php echo MysqlConnection::$REQUIRED ?></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="name" id="name" required="true" ></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" class="btn btn-info">SAVE </a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->

<!-- this is customer model dialog --->
<div id="custtypemodel" class="modal hide" style="top: 3%;left: 25%;width: 90%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD NEW CUSTOMER</h3>
    </div>
    <?php include 'customdialogs/customerdialog.php'; ?>
</div>
<!-- this is customer model dialog --->

<!-- this is custom model dialog --->
<div id="creditlimitmodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Credit Limit Alert!!!</h3>
    </div>
    <div class="modal-body">
        <h5 style="color: red">Total amount exceeding credit limit !!!</h5>
    </div>
    <div class="modal-footer"> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->


<!-- this is custom model dialog --->
<div id="onholdtmodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>Wait!! This customer is on hold.</h3>
    </div>
    <div class="modal-body">
        <h5 style="color: red" id="onholdtmodeltext"></h5>
    </div>
    <div class="modal-footer"> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->


<script>

    $(document).ready(function () {
        $('#customer_id').select2();
    });
    $("#cancelct").click(function () {
        $("#customer_id").val("");
    });
    $("#customer_id").click(function () {
        var valueModel = $("#customer_id").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
    function deleteRow(rowid)
    {
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
        finalTotal();
    }

    $("#paymentTerm").click(function () {
        var valueModel = $("#paymentTerm").val();
        if (valueModel === "0") {
            $('#addPaymentTerm').modal('show');
        }
    });
    $("#savePaymentT").click(function () {
        var type = "paymentterm";
        var code = $("#termcode").val();
        var name = $("#termname").val();
        var description = $("#termdescription").val();
        $("div#divLoading").addClass('show');
        var dataString = "type=" + type + "&name=" + name + "&description=" + description + "&code=" + code;
        $.ajax({
            type: 'POST',
            url: 'customermaster/savepayterm_ajax.php',
            data: dataString
        }).done(function (data) {
            $('#paymentTerm').append(data);
            $("#termcode").val("");
            $("#termname").val("");
            $("#termdescription").val("");
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    });
    $("#taxInformation").click(function () {
        var valueModel = $("#taxInformation").val();
        if (valueModel === "0") {
            $('#addTaxInformation').modal('show');
        }
    });
    $("#saveTaxInformation").click(function () {
        var taxcode = $("input[name='taxcode[]']").map(function () {
            return $(this).val();
        }).get();
        var taxtaxname = $("input[name='taxtaxname[]']").map(function () {
            return $(this).val();
        }).get();
        var taxtaxvalues = $("input[name='taxtaxvalues[]']").map(function () {
            return $(this).val();
        }).get();
        var taxisExempt = $("input[name='taxisExempt[]']").map(function () {
            return $(this).val();
        }).get();
        var dataString = "taxcode=" + taxcode + "&taxtaxname=" + taxtaxname + "&taxtaxvalues=" + taxtaxvalues + "&taxisExempt=" + taxisExempt;
        $("div#divLoading").addClass('show');
        $.ajax({
            type: 'POST',
            url: 'customermaster/savetaxinfo_ajax.php',
            data: dataString
        }).done(function (data) {
            $("input[name='taxcode[]']").val("");
            $("input[name='taxtaxname[]']").val("");
            $("input[name='taxtaxvalues[]']").val("");
            $("input[name='taxisExempt[]']").val("");
            $('#taxInformation').append(data);
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    });
//////////  SHIP VIA INFORMATION //////////  
    $("#shipvia").click(function () {
        var valueModel = $("#shipvia").val();
        if (valueModel === "0") {
            $('#shipviamodel').modal('show');
        }
    });
    $("#cancelshipvia").click(function () {
        $("#shipvia").val("");
    });
    $("#saveshipvia").click(function () {
        var type = "shipvia";
        var name = $("#name").val();
        var code = name;
        var dataString = "type=" + type + "&code=" + code + "&name=" + name;
        $.ajax({
            type: 'POST',
            url: 'preferencemaster/save_preferencemaster.php',
            data: dataString
        }).done(function (data) {
            $('#shipvia').append(data);
            $("#code").val("");
            $("#name").val("");
        }).fail(function () {
        });
    });
    //////////  SHIP VIA INFORMATION //////////
    $("#customer_id").change(function () {
        $("div#divLoading").addClass('show');
        var dataString = "companyname=" + $("#customer_id").val();
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function (data) {
            var obj = JSON.parse(data);
            $("#billTo_address").val(obj.billto);
            $("#shipping_address").val(obj.shipto);
            $("#representative").val(obj.sales_person_name);
            var isExempt = obj.isExempt;
            if (isExempt === "Y") {
                $("#taxid").prop("disabled", true);
            } else {
                $("#taxid").val(obj.taxInformation);
            }
            //$("#staxname").val(obj.staxid);
            $("#staxname").val(obj.staxname + "-5");
            $("#creditlimit").val(obj.creditlimit);
            $("#staxpercent").val(obj.staxpercent);
            $("#pricediscount").text("PRICE-" + obj.discount + "%");
            $("#pricediscountvalue").text(obj.discount);

            var onhold = obj.onhold;
            if (onhold !== undefined && onhold === "Y") {
                $("#onholdtmodeltext").text(obj.onholdreason);
                $('#onholdtmodel').modal('show');
            }

            doCalculation();
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    });
    $("#cancelpt").click(function () {
        $("#paymentTerm").val("");
    });
    $("#cancelti").click(function () {
        $("#taxInformation").val("");
    });
    $("#myElem").show().delay(3000).fadeOut();
    $("select#staxname").change(function () {
        doCalculation();
    });
</script>

<?php

function saveOrEditSalesOrder() {
    $btnSaveSalesOrder = filter_input(INPUT_POST, "btnSaveSalesOrder");
    $btnSaveNextSalesOrder = filter_input(INPUT_POST, "btnSaveNextSalesOrder");
    $printButton = filter_input(INPUT_POST, "printButton");
    $mailButton = filter_input(INPUT_POST, "mailButton");
    $invoiceButton = filter_input(INPUT_POST, "invoice");
    $salesorderdetails["sub_total"] = filter_input(INPUT_POST, "itemtotal");
    $salesorderdetails["pono"] = filter_input(INPUT_POST, "pono");

    $salesorderdetails["taxid"] = $taxid = filter_input(INPUT_POST, "taxid");
    $taxarray = MysqlConnection::getTaxInfoById($taxid);
    $salesorderdetails["taxname"] = $taxarray["taxname"];
    $salesorderdetails["taxValue"] = $taxarray["taxvalues"];
    $salesorderdetails["taxamount"] = filter_input(INPUT_POST, "taxamount");

    $salesorderdetails["staxname"] = filter_input(INPUT_POST, "staxname");
    $salesorderdetails["staxpercent"] = filter_input(INPUT_POST, "staxpercent");

    $salesorderdetails["taxtotal"] = filter_input(INPUT_POST, "sub_total");
    $salesorderdetails["ship_charge"] = filter_input(INPUT_POST, "ship_charge");

    $salesorderdetails["discount"] = filter_input(INPUT_POST, "discount");
    $salesorderdetails["discountvalue"] = filter_input(INPUT_POST, "discountvalue");

    $salesorderdetails["total"] = filter_input(INPUT_POST, "total");
    $salesorderdetails["representative"] = filter_input(INPUT_POST, "representative");

    $salesorderdetails["sono"] = MysqlConnection::getNextValue("SO");
    if (filter_input(INPUT_GET, "salesorderid") == "") {
        $salesorderdetails["customer_id"] = $customerid = filter_input(INPUT_POST, "customer_id");
    }
    $shipping_address = filter_input(INPUT_POST, "shipping_address");
    $billTo_address = filter_input(INPUT_POST, "billTo_address");

    $salesorderdetails["shipping_address"] = MysqlConnection::formatToBRAddress($shipping_address);
    $salesorderdetails["billTo_address"] = MysqlConnection::formatToBRAddress($billTo_address);


    $salesorderdetails["remark"] = filter_input(INPUT_POST, "remark");
    $salesorderdetails["shipvia"] = filter_input(INPUT_POST, "shipvia");
    $salesorderdetails["radios"] = filter_input(INPUT_POST, "radios");



    $salesorderdetails["isOpen"] = "Y";
    $salesorderdetails["expected_date"] = $expected_date = filter_input(INPUT_POST, "expected_date");
    if ($expected_date == "") {
        $salesorderdetails["expected_date"] = filter_input(INPUT_POST, "hidexpected_date");
    } else {
        $salesorderdetails["expected_date"] = MysqlConnection::convertToDBDate($salesorderdetails["expected_date"]);
    }
    $salesorderdetails["soorderdate"] = date("Y-m-d");
    $salesorderdetails["added_by"] = $_SESSION["user"]["user_id"];
    $salesorderdetails["deleteNote"] = "";
    $salesorderid = filter_input(INPUT_GET, "salesorderid");

    if (isset($btnSaveSalesOrder) || isset($btnSaveNextSalesOrder) || isset($printButton) || isset($mailButton) || isset($invoiceButton)) {
        if (isset($salesorderid) && $salesorderid != "") {
            MysqlConnection::edit("sales_order", $salesorderdetails, " id = '$salesorderid' ");
            MysqlConnection::delete("DELETE FROM `sales_item` WHERE `so_id` = '$salesorderid' ");
        } else {
            $salesorderid = MysqlConnection::insert("sales_order", $salesorderdetails);
            updateBalance($salesorderdetails["customer_id"], $salesorderdetails["total"]);
        }
        saveSalesOrderItems($salesorderid, $customerid);
        include 'pdflib/salesorder.php';
    }

    if ($btnSaveSalesOrder == "SAVE") {
        header("location:index.php?pagename=manage_salesorder&action=update");
    } if ($btnSaveNextSalesOrder == "SAVE AND NEW") {
        header("location:index.php?pagename=create_salesorder&action=created");
    }if ($printButton == "PRINT") {
        header("location:index.php?pagename=manage_salesorder&action=print&salesorderid=$salesorderid");
    }if ($invoiceButton == "CREATE INVOICE") {
        header("location:index.php?pagename=manage_salesorder&action=invoice&salesorderid=$salesorderid");
    } else if ($mailButton == "EMAIL") {
        header("location:index.php?pagename=email_salesorder&salesorderid=$salesorderid");
    }
}

function saveSalesOrderItems($insertid) {

    $itemsarray = filter_input_array(INPUT_POST);
    $items = $itemsarray["items"];
    $pricearray = $itemsarray["price"];
    $itemcount = $itemsarray["itemcount"];
    $itemcountoriginal = $itemsarray["itemcountoriginal"];


    for ($index = 0; $index <= count($items); $index++) {
        $itemname = explode("__", $items[$index]);
        $itemcode = trim($itemname[0]);
        $count = $itemcount[$index];
        $price = $pricearray[$index];
        $oldqty = $itemcountoriginal[$index];

        if ($count != 0) {
            $itemsfromcode = MysqlConnection::fetchCustom("SELECT item_id,onhand,totalvalue"
                            . " FROM `item_master` WHERE item_code = '$itemcode' ");
            $arraysalesitems = array();
            $item_id = $itemsfromcode[0]["item_id"];
            $arraysalesitems["item_id"] = $item_id;
            $arraysalesitems["so_id"] = $insertid;
            $arraysalesitems["qty"] = $count;
            $arraysalesitems["rQty"] = 0;
            $arraysalesitems["backQty"] = 0;
            $arraysalesitems["price"] = $price;
            MysqlConnection::insert("sales_item", $arraysalesitems);
            if ($oldqty == 0 || $oldqty == "") {
                MysqlConnection::updateOnSaleItem($item_id, $count, "Y");
            } else {
                if ($oldqty < $count) {
                    $cnt = abs($oldqty - $count);
                    MysqlConnection::updateOnSaleItem($item_id, $cnt, "Y");
                } else if ($oldqty > $count) {
                    $cnt = abs($oldqty - $count);
                    MysqlConnection::updateOnSaleItem($item_id, $cnt, "N");
                }
            }
        }
    }
}

function updateBalance($customerid, $amount) {
    $query = "SELECT creditlimit FROM `customer_master` WHERE id = '$customerid' ";
    $custom = MysqlConnection::fetchCustom($query);
    $creditlimit = $custom[0]["creditlimit"];
    $newAmount = $creditlimit - $amount;
    MysqlConnection::delete("UPDATE customer_master SET creditlimit = $newAmount   WHERE id = '$customerid' ");
}
