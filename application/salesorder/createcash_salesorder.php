<?php
if (MysqlConnection::validateItems()) {
    header("location:index.php?pagename=success_itemmaster&flag=noitems");
}
$salesorderid = filter_input(INPUT_GET, "salesorderid");
if (!isset($salesorderid) || $salesorderid == "") {
    $sonumber = MysqlConnection::generateNumber("retailso");
}

$sqlitemarray = MysqlConnection::fetchCustom("SELECT count(id) as counter FROM sales_order");
$itemarray = MysqlConnection::fetchCustom("SELECT * FROM item_master;");
$buildauto = buildauto(MysqlConnection::fetchCustom("SELECT item_id ,item_code,item_desc_purch, item_name FROM item_master;"));
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY taxtype ASC ;");

$salesorderidget = filter_input(INPUT_GET, "salesorderid");
if (isset($salesorderidget)) {
    $soorderdetails = MysqlConnection::getSalesOrderDetailsById($salesorderidget);

    if ($soorderdetails["isOpen"] == "N") {
        header("location:index.php?pagename=success_salesorder&salesorderid=$salesorderidget&flag=closederror");
    }

    $soitemsdetails = MysqlConnection::getSalesItemsDetailsById($salesorderidget);
    $sonumber = $soorderdetails["sono"];
    $soCustomer = MysqlConnection::getCustomerDetails($soorderdetails["customer_id"]);
    $representivename = MysqlConnection::getGenericById($soCustomer["sales_person_name"]);
}

saveOrEditSalesOrder();

$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");

$_SESSION["navigationpage"] = "index.php?pagename=create_salesorder";
?>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="js/script.js"></script>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });

</script>
<script>
    $(function () {
        for (var index = 1; index <= 30; index++) {
            $("#tags" + index).autocomplete({
                source: "itemmaster/autoitemajax.php",
            });
        }
    });
    function saleOrderRowDelete(rowid) {
        var row = document.getElementById(rowid);
        doCalculation();
        row.parentNode.removeChild(row);
    }

    $(document).ready(function ($) {
        $("#mobileno").mask("(999) 999-9999");

    });

    function copyOrRemove(flag) {
        if (flag === "1") {
            document.getElementById("shipping_address").value = document.getElementById("billTo_address").value;
        } else {
            document.getElementById("shipping_address").value = "";
        }
    }

</script>
<style>
    input,textarea,date{ width: 80%;height: 30px; }
    select{ width: 81.4%;height: 30px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}

    .ui-autocomplete {
        max-height: 300px;
        overflow-y: auto;   /* prevent horizontal scrollbar */
        overflow-x: hidden; /* add padding to account for vertical scrollbar */
        z-index:1000 !important;
    }
</style>
<form  method="post" autocomplete="off">
    <input type="hidden" name="priceincreseby" id="priceincreaseby" value="<?php echo getPriceIncreseBy() ?>">
    <div class="container-fluid" style="" >
        <?php
        if ($action == "created") {
            echo MysqlConnection::$MSG_AD;
        }
        ?>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANAGE CASH ORDER</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="display nowrap sortable" style="width: 100%">

                            <tr>
                                <td style="width: 10%">CUSTOMER NAME<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td title="Enter Customer Name"><input style="width: 220px"  tabindex="1" autofocus=""  type="text" name="cust_companyname"  id="cust_companyname" value="<?php echo $customername ?>" required="true"></td>
                                <td style="width: 10%">MOBILE NO</td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <input type="text" style="width: 40px;"     name="mobcode" id="mobcode"  value="<?php echo trim($customer["mobcode"]) == "" ? "+1" : $customer["mobcode"] ?>">
                                    <input style="width: 160px" type="text" tabindex="2" name="mobileno"  id="mobileno"    value="<?php echo trim($customer["mobileno"]) ?>"> 
                                </td>
                                <td style="width: 10%">SHIP VIA<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <select  tabindex="3"  style="width: 225px;height: 25px;" name="shipvia" id="shipvia" required="true"> 
                                        <option value="">&nbsp;&nbsp;</option>
                                        <option value="0" ><< ADD NEW >></option>
                                        <?php foreach ($shipviainfo as $key => $value) { ?>
                                            <option <?php echo $value["name"] == $soorderdetails["shipvia"] ? "selected" : "" ?>
                                                value="<?php echo $value["name"] ?>"  ><?php echo $value["name"] ?></option>
                                            <?php } ?>
                                    </select>
                                    <div >
                                        <label style="width: 50%">
                                            <input type="radio" name="radios" onclick="checkForShipVia('collect')" <?php echo $soorderdetails["radios"] == "collect" ? "checked" : "" ?> value="collect"  style="">&nbsp;Collect
                                        </label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                        <label style="width: 50%">
                                            <input type="radio" name="radios"  onclick="checkForShipVia('prepaid')"  <?php echo $soorderdetails["radios"] == "prepaid" ? "checked" : "" ?> value="prepaid" >&nbsp;Prepaid
                                        </label>
                                    </div>
                                </td>
                            </tr>

                            <tr style="font-weight: bold; ">
                                <td style="color: red"><b>SO NUMBER</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><input style="color: red; width: 220px" type="text" name="sono" onkeypress="return chkNumericKey(event)" value="<?php echo $sonumber ?>" readonly=""></td>
                                <td style="color: red"><b>SALES DATE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><input style="width: 220px" type="text" name="salesdate" value="<?php echo MysqlConnection::convertToPreferenceDate(date("Y-m-d")) ?>" readonly=""></td>
                                <td style="width: 10%">EXPECTED&nbsp;DELIVERY<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><input tabindex="4" style="width: 220px" required="true" type="text" name="expected_date" id="datepicker"  value="<?php echo MysqlConnection::convertToPreferenceDate($soorderdetails["expected_date"]) ?>" ></td>
                            </tr>

                            <tr>
                                <td >BILLING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea  tabindex="5"  style="line-height: 18px;width: 220px;height: 72px;resize: none" name="billTo_address" id="billTo_address"><?php echo $soorderdetails["billTo_address"] ?></textarea></td>
                                <td>SHIPPING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea  tabindex="6"  style="line-height: 18px;width: 220px;height: 72px;resize: none" name="shipping_address" id="shipping_address" ><?php echo $soorderdetails["shipping_address"] ?></textarea></td>
                                <td >REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea  tabindex="7"   style="line-height: 18px; color: red ; width: 220px;height: 72px;resize: none" name="remark"><?php echo $soorderdetails["remark"] ?></textarea></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td ><span  data-toggle="tooltip" data-original-title="Copy billing address to shipping address"><a onclick="copyOrRemove('1')" style="cursor: pointer" >COPY >></a></span></td>
                                <td></td>
                                <td></td>
                                <td><span  data-toggle="tooltip" data-original-title="Remove shipping address"><a onclick="copyOrRemove('0')" style="cursor: pointer" ><< REMOVE</a></span></td>
                                <td></td>
                                <td></td>
                                <td></td>

                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 70%;float: left">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(250,250,250)">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 300px;">ITEM CODE / DESCRIPTION</td>
                                    <td style="width: 70px;">UNIT</td>
                                    <td style="width: 70px;">PRICE</td>
                                    <td style="width: 80px;">ON HAND</td>
                                    <td style="width: 80px;">ON SALE</td>
                                    <td style="width: 50px;">QTY</td>
                                    <td>AMOUNT</td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $indexedit = 1;
                                    foreach ($soitemsdetails as $soitem) {
                                        $value = MysqlConnection::getItemDataById($soitem["item_id"]);
                                        ?>
                                        <tr id="<?php echo $indexedit ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $indexedit ?>')"></a>
                                            </td>
                                            <td style="width: 300px">
                                                <input type="text" name="items[]" value="<?php echo $value["item_code"] . " __ " . $value["item_desc_purch"] ?>" id="tags<?php echo $indexedit ?>" onfocusout="setDetails('<?php echo $indexedit ?>')"  style="padding: 0px;margin: 0px;width: 100%">
                                            </td>
                                            <td style="width: 70px;"><div id="unit<?php echo $indexedit ?>"><?php echo $value["unit"] ?></div></td>
                                            <td style="width: 70px;">
                                                <input type="text" name="price[]" pattern="^\d*(\.\d{0,2})?$" onkeypress="return chkNumericKey(event)" id="price<?php echo $indexedit ?>" value="<?php echo $value["price"] ?>">
                                                <div id="price<?php // echo $indexedit                                               ?>"><?php // echo $value["total_sales_rate"]                                             ?></div>
                                            </td>
                                            <td style="width: 80px;"><div id="onhand<?php echo $indexedit ?>"><?php echo $value["onhand"] ?></div></td>
                                            <td style="width: 80px;"><div id="totalvalue<?php echo $indexedit ?>"><?php echo $value["totalvalue"] ?></div></td>
                                            <td style="width: 50px;"><input type="text" name="itemcount[]" value="<?php echo $soitem["qty"] ?>" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="doCalculation()" id="amount<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><div id="total<?php echo $indexedit ?>"><?php echo $soitem["qty"] * $value["total_sales_rate"] ?></div></td>
                                        </tr>
                                        <?php
                                        $indexedit++;
                                    }
                                    for ($index = $indexedit; $index <= 50; $index++) {
                                        if (count($soitemsdetails) == 0) {
                                            if ($index == 1) {
                                                $required = "required";
                                            } else {
                                                $required = "";
                                            }
                                        }
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $index ?>')"></a>
                                            </td>
                                            <td style="width: 300px">
                                                <input type="text" name="items[]" <?php echo $required ?> id="tags<?php echo $index ?>" onfocusout="setDetails('<?php echo $index ?>')"  style="padding: 0px;margin: 0px;width: 100%">
                                            </td>
                                            <td style="width: 70px;"><div id="unit<?php echo $index ?>"></div></td>
                                            <td style="width: 70px;">
                                                <input type="text" name="price[]"  onfocusout="checkLessValue('price<?php echo $index ?>')" id="price<?php echo $index ?>" value="<?php echo $soitem["price"] ?>">
                                                <div id="price<?php // echo $indexedit                                               ?>"><?php // echo $value["total_sales_rate"]                                             ?></div>
                                            </td>
                                            <td style="width: 80px;"><div id="onhand<?php echo $index ?>"></div></td>
                                            <td style="width: 80px;"><div id="totalvalue<?php echo $index ?>"></div></td>
                                            <td style="width: 50px;"><input type="text" name= "itemcount[]" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="doCalculation()" id="amount<?php echo $index ?>" style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><div id="total<?php echo $index ?>"></div></td>
                                        </tr>
                                    <?php } ?>

                                </table>
                            </div>
                        </div>
                        <div style="width: 28%;float: right">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <tr >
                                    <td  style="width: 120px;"><b>Total Amount</b></td>
                                    <td colspan="2"><input  style="width: 98%" type="text" id="itemtotal" name="itemtotal"  value="<?php echo $soorderdetails["sub_total"] ?>"></td>
                                </tr>
                                <tr >
                                    <td  style="width: 120px;"><b>Discount %</b></td>
                                    <td>
                                        <input  style="width: 98%" type="text" id="discount"  value="<?php echo $soorderdetails["discount"] ?>" name="discount"  maxlength="3" onfocusout="doCalculation()" name="discount" >
                                    </td>
                                    <td>
                                        <input  style="width: 96%" type="text" id="discountvalue"  value="<?php echo $soorderdetails["discountvalue"] ?>" name="discountvalue" readonly="">
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Goods Tax</b></td>
                                    <td>
                                        <select  name="taxid" id="taxid" required="" onchange="doCalculation()" style="height: 25px;width: 98%">
                                            <?php
                                            foreach ($sqltaxinfodata as $key => $value) {
                                                if ($soorderdetails["taxid"] == $value["id"]) {
                                                    $selected = "selected";
                                                } else {
                                                    $selected = "";
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>">
                                                    <?php echo ucwords($value["taxtype"]) ?>
                                                    <?php echo $value["taxname"] ?>-<?php echo $value["taxvalues"] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                            <option value="">None</option>
                                        </select>
                                    </td> 
                                    <td>
                                        <input type="text" id="taxamount" readonly="" value="<?php echo $soorderdetails["taxamount"] ?>" name='taxamount' style="width: 97%" >
                                    </td>
                                </tr>

                                <tr >
                                    <td><b>Sub Total</b></td>
                                    <td colspan="2">
                                        <input  style="width: 98%" type="text" id="finaltotal"  value="<?php echo $soorderdetails["taxtotal"] ?>" onkeypress="return chkNumericKey(event)" name="sub_total" readonly="">
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Shipping Charges</b></td>
                                    <td colspan="2">
                                        <input  style="width: 98%" type="text" id="ship_charge"  value="<?php echo $soorderdetails["ship_charge"] == "" ? "0" : $soorderdetails["ship_charge"] ?>" maxlength="6" onkeyup="doCalculation()"  onkeypress="return chkNumericKey(event)" name="ship_charge" value="<?php echo $customer["ship_charge"] ?>" >
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Ship Tax</b></td>
                                    <td >
                                        <select id="staxname" name='staxname' style="height: 25px;width: 98%" >
                                            <option value="GST-5" <?php echo $soorderdetails["staxname"] == "GST-5" ? "selected" : "" ?> >GST-5</option>
                                            <option value="">None</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="text" id="staxpercent" readonly=""  value="<?php echo $soorderdetails["staxpercent"] ?>" name='staxpercent'  onkeypress="return chkNumericKey(event)" style="width: 96%" >
                                    </td>
                                </tr>

                                <tr >
                                    <td><b>Net Total</b></td>
                                    <td colspan="2">
                                        <input  style="width: 98%" type="text" id="nettotal" readonly="" value="<?php echo $soorderdetails["total"] ?>"  name="total" name="nettotal" >
                                        <input   style="width: 90%" type="hidden" value="<?php echo MysqlConnection::getAddedBy($_SESSION["user"]["user_id"]); ?>" readonly="" >
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>
                    <input type="submit" title="Click here to save cash order" id="btnSaveSalesOrder" name="btnSaveSalesOrder" class="btn btn-info" value="SAVE">
                    <input title="Click here to save this cash order and move to create new cash order" type="submit" id="btnSaveNextSalesOrder" name="btnSaveNextSalesOrder" class="btn btn-info" value="SAVE AND NEW"/>
                    <input title="Click here to print cash order" type="submit" id="printButton" name="printButton" class='btn btn-info' value="PRINT"/>
                    <input title="Click here to send an email" type="submit" id="mailButton" name="mailButton" class='btn btn-info' style="margin-left:4px;" value="EMAIL"/>
                    <input title="Click here to create an Invoice" type="submit" id="invoiceButton" name="invoiceButton" class='btn btn-info' onclick="return confirmOpen()" style="margin-left:4px;" value="INVOICE"/>
                    <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                </center>
            </div>
            <input type="hidden" name="onhand" id="onhand">
            <input type="hidden" name="tempItemPrice" id="tempItemPrice">
        </div>
    </div>
    <div id="confirmation" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
        <div class="modal-header"><h3>ALERT !!!</h3></div>
        <div class="modal-body">
            <h5 style="color: red">Do you want to confirm this invoice?<br/></h5>

        </div>
        <div class="modal-footer"> 
            <input type="submit" name="btnConfirmation" id="btnConfirmation" class="btn btn-info" value="YES">
            <input type="submit" name="btnCancle" id="btnCancle" class="btn btn-info" value="NO">
            <a data-dismiss="modal" class="btn btn-info" id="confirmation" href="#">CANCEL</a> 
        </div>
    </div>
</form>

<?php

function buildauto($itemarray) {
    $option = "";
    foreach ($itemarray as $value) {
        $item_desc_purch1 = str_replace("\"", "", $value["item_desc_purch"]);
        $item_desc_purch2 = str_replace("'", "", $item_desc_purch1);
        $item_desc_purch3 = str_replace("''", "", $item_desc_purch2);
        $item_code1 = str_replace("\"", "", $value["item_code"]);
        $item_code2 = str_replace("'", "", $item_code1);
        $item_code3 = str_replace("''", "", $item_code2);
        $option .= "\"" . $item_code3 . " __ " . preg_replace('!\s+!', ' ', trim($item_desc_purch3)) . "\",";
    }

    return $option;
}
?>

<!-- this is custom model dialog --->
<div id="shipviamodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD SHIP VIA</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="ship_via" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>SHIP VIA NAME<?php echo MysqlConnection::$REQUIRED ?></td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="name" id="name" required=""></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" class="btn btn-info">SAVE </a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->


<script>

    $("#staxname").click(function () {
        doCalculation();
    });

    function deleteRow(rowid)
    {
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
        finalTotal();
    }

    function confirmOpen() {
        $('#confirmation').modal('show');
        return false;
    }

//////////  SHIP VIA INFORMATION //////////  
    $("#shipvia").click(function () {
        var valueModel = $("#shipvia").val();
        if (valueModel === "0") {
            $('#shipviamodel').modal('show');
        }
    });
    $("#cancelshipvia").click(function () {
        $("#shipvia").val("");
    });
    $("#saveshipvia").click(function () {
        var type = "shipvia";
        var name = $("#name").val();
        var code = name;
        var dataString = "type=" + type + "&code=" + code + "&name=" + name;
        $.ajax({
            type: 'POST',
            url: 'preferencemaster/save_preferencemaster.php',
            data: dataString
        }).done(function (data) {
            $('#shipvia').append(data);
            $("#code").val("");
            $("#name").val("");
        }).fail(function () {
        });
    });

    $('#billTo_address').change(function () {
        $('#shipping_address').val($(this).val());
    });
</script>

<?php

function saveCustomer() {
    $customer["cust_companyname"] = filter_input(INPUT_POST, "cust_companyname");
    $customer["mobcode"] = filter_input(INPUT_POST, "mobcode");
    $customer["mobileno"] = filter_input(INPUT_POST, "mobileno");
    $customer["cust_type"] = "c04c56f0c0ece87762a7d7778f90933a";

    $billToAddress = filter_input(INPUT_POST, "billTo_address");
    $shippingAddress = filter_input(INPUT_POST, "shipping_address");

    $customer["billto"] = MysqlConnection::formatToBRAddress($billToAddress);
    $customer["shipto"] = MysqlConnection::formatToBRAddress($shippingAddress);

    $customer["isCash"] = "Y";
    return MysqlConnection::insert("customer_master", $customer);
}

function saveOrEditSalesOrder() {
    $btnSaveSalesOrder = filter_input(INPUT_POST, "btnSaveSalesOrder");
    $btnSaveNextSalesOrder = filter_input(INPUT_POST, "btnSaveNextSalesOrder");
    $printButton = filter_input(INPUT_POST, "printButton");
    $mailButton = filter_input(INPUT_POST, "mailButton");
    $invoiceButton = filter_input(INPUT_POST, "invoiceButton");



    $btnConfirmation = filter_input(INPUT_POST, "btnConfirmation");
    $btnCancle = filter_input(INPUT_POST, "btnCancle");



    $salesorderdetails["sub_total"] = filter_input(INPUT_POST, "itemtotal");

    $salesorderdetails["taxid"] = filter_input(INPUT_POST, "taxid");
    $salesorderdetails["taxamount"] = filter_input(INPUT_POST, "taxamount");
    //$salesorderdetails["taxValue"] = filter_input(INPUT_POST, "taxValue");

    $salesorderdetails["staxname"] = filter_input(INPUT_POST, "staxname");
    $salesorderdetails["staxpercent"] = filter_input(INPUT_POST, "staxpercent");

    $salesorderdetails["taxtotal"] = filter_input(INPUT_POST, "sub_total");
    $salesorderdetails["ship_charge"] = filter_input(INPUT_POST, "ship_charge");
    $salesorderdetails["discount"] = filter_input(INPUT_POST, "discount");
    $salesorderdetails["discountvalue"] = filter_input(INPUT_POST, "discountvalue");
    $salesorderdetails["total"] = filter_input(INPUT_POST, "total");


    $shipping_address = filter_input(INPUT_POST, "shipping_address");
    $billTo_address = filter_input(INPUT_POST, "billTo_address");

    $salesorderdetails["shipping_address"] = MysqlConnection::formatToBRAddress($shipping_address);
    $salesorderdetails["billTo_address"] = MysqlConnection::formatToBRAddress($billTo_address);


    $salesorderdetails["remark"] = filter_input(INPUT_POST, "remark");
    $salesorderdetails["shipvia"] = filter_input(INPUT_POST, "shipvia");
    $salesorderdetails["radios"] = filter_input(INPUT_POST, "radios");
    $salesorderdetails["sono"] = filter_input(INPUT_POST, "sono");
    $salesorderdetails["isOpen"] = "Y";
    $salesorderdetails["expected_date"] = filter_input(INPUT_POST, "expected_date");
    $salesorderdetails["expected_date"] = MysqlConnection::convertToDBDate($salesorderdetails["expected_date"]);
    $salesorderdetails["soorderdate"] = date("Y-m-d");
    $salesorderdetails["added_by"] = $_SESSION["user"]["user_id"];
    $salesorderdetails["deleteNote"] = "";
    $salesorderid = filter_input(INPUT_GET, "salesorderid");

    $invoice = time();
    if (isset($btnSaveSalesOrder) ||
            isset($btnSaveNextSalesOrder) ||
            isset($printButton) ||
            isset($mailButton) ||
            isset($btnConfirmation) ||
            isset($btnCancle) ||
            isset($invoiceButton)) {
        if (filter_input(INPUT_GET, "salesorderid") == "") {
            $salesorderdetails["customer_id"] = $customerid = saveCustomer();
        }
        if ($salesorderid != "") {
            MysqlConnection::edit("sales_order", $salesorderdetails, " id = '$salesorderid' ");
            MysqlConnection::delete("DELETE FROM `sales_item` WHERE `so_id` = '$salesorderid' ");
        } else {
            $salesorderid = MysqlConnection::insert("sales_order", $salesorderdetails);
        }
        saveSalesOrderItems($salesorderid, $invoice);
        include 'pdflib/salesorder.php';
    }

    if ($btnSaveSalesOrder == "SAVE") {
        header("location:index.php?pagename=manage_salesorder&action=update");
    } if ($btnSaveNextSalesOrder == "SAVE AND NEW") {
        header("location:index.php?pagename=createcash_salesorder&action=created");
    }if ($printButton == "PRINT") {
        header("location:index.php?pagename=manage_salesorder&action=print&salesorderid=$salesorderid");
    }if ($invoiceButton == "INVOICE" || $btnConfirmation == "YES") {
        salesInvoice($salesorderid, $invoice);
        header("location:index.php?pagename=manage_salesorder&action=update");
    } else if ($mailButton == "EMAIL") {
        header("location:index.php?pagename=email_salesorder&salesorderid=$salesorderid");
    }
}

function saveSalesOrderItems($insertid, $invoice) {
    $itemsarray = filter_input_array(INPUT_POST);
    $items = $itemsarray["items"];
    $pricearray = $itemsarray["price"];
    $itemcount = $itemsarray["itemcount"];

    $invoiceButton = filter_input(INPUT_POST, "invoiceButton");
    $btnConfirmation = filter_input(INPUT_POST, "btnConfirmation");
    $btnCancle = filter_input(INPUT_POST, "btnCancle");


    MysqlConnection::delete("UPDATE sales_order SET invoiceno = '$invoice', isTentetive = 'Y' WHERE id = '$insertid'");

    for ($index = 0; $index <= count($items); $index++) {
        $itemname = explode("__", $items[$index]);
        $itemcode = trim($itemname[0]);
        $count = $itemcount[$index];
        $price = $pricearray[$index];
        if ($count != 0) {
            $itemsfromcode = MysqlConnection::fetchCustom("SELECT item_id,onhand,totalvalue FROM `item_master` WHERE item_code = '$itemcode' ");
            $arraysalesitems = array();
            $item_id = $itemsfromcode[0]["item_id"];
            $arraysalesitems["item_id"] = $item_id;
            $arraysalesitems["so_id"] = $insertid;
            $arraysalesitems["qty"] = $count;
            $arraysalesitems["rQty"] = 0;
            $arraysalesitems["backQty"] = 0;
            $arraysalesitems["price"] = $price;
            MysqlConnection::insert("sales_item", $arraysalesitems);

            if ($invoiceButton == "INVOICE" || $btnConfirmation == "YES" || $btnCancle == "NO") {
                //save to sells history 
                $arraysellinghist["invoice"] = $invoice;
                $arraysellinghist["so_id"] = $insertid;
                $arraysellinghist["qty"] = $count;
                $arraysellinghist["item_id"] = $item_id;
                $arraysellinghist["price"] = $price;
                $arraysellinghist["date"] = date("Y-m-d");
                MysqlConnection::insert("tbl_selling_history", $arraysellinghist);
            }
        }
    }
}

function salesInvoice($salesorderid = "", $invoice) {
    include 'pdflib/salesinvoice.php';
}

function getPriceIncreseBy() {
    $query = "SELECT `code`,`name` FROM `generic_entry` WHERE `type` = 'customervalue' AND active = 'Y' ";
    $resultset = MysqlConnection::fetchCustom($query);
    return $resultset[0]["name"];
}
?>
