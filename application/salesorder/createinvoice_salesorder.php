<?php
$salesorderid = filter_input(INPUT_GET, "salesorderid");
$flag = filter_input(INPUT_GET, "flag");
$customer = MysqlConnection::getSalesOrderDetailsById($salesorderid);
$itemsarrays = MysqlConnection::getSalesItemsDetailsById($salesorderid);
$customerdetails = MysqlConnection::fetchCustom("SELECT cust_companyname FROM `customer_master` WHERE  id = ( SELECT customer_id FROM sales_order WHERE id = '$salesorderid ')");
$reasonarray = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'inventorydiscrepancy' ORDER BY `indexid` DESC ");

saveorupdate();
?>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form action="#" method="post">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title"><ul class="nav nav-tabs"><li class="active"><a data-toggle="tab" href="#tab1">CREATE INVOICE</a></li></ul></div>
            <table  style="width: 100%" class="table table-bordered display nowrap sortable">
                <tr>
                    <td>
                        <table style="width: 100%" class="table table-bordered display nowrap sortable">
                            <tr style="font-weight: bold; color: red">
                                <td style="background-color: white" ><b>SO NUMBER</b></td>
                                <td style="color: red;width: 22%" ><?php echo $sono = $customer["sono"] ?></td>
                                <td style="background-color: white" ><b>SALES DATE</b></td>
                                <td style="width: 22%"> <?php echo date("Y-m-d") ?></td>
                                <td style="background-color: white" ><b>SALES PERSON</b></td>
                                <td style="width: 10%"><?php echo MysqlConnection::getAddedBy($customer["added_by"]) ?></td>
                            </tr>

                            <tr>
                                <td style="background-color: white"><b>CUSTOMER NAME</b></td>
                                <td>
                                    <?php echo $custcompanyname = $customerdetails[0]["cust_companyname"] ?>
                                    <div id="error" style="color: red"></div>
                                </td>
                                <td style="background-color: white"><b>SHIP VIA</b></td>
                                <td><?php echo $customer["shipvia"] ?></td>
                                <td style="background-color: white">EXPECTED&nbsp;DELIVERY</td>
                                <td><?php echo MysqlConnection::convertToPreferenceDate($customer["expected_date"]) ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: white"><b>BILLING&nbsp;ADDRESS</b></td>
                                <td style="line-height: 18px;;resize: none;height: 75px;"><?php echo str_replace("<br/>", "\n", $customer["billTo_address"]) ?></td>
                                <td style="background-color: white"><b>SHIPPING&nbsp;ADDRESS</b></td>
                                <td style="line-height: 18px;;resize: none;height: 75px;" ><?php echo str_replace("<br/>", "\n", $customer["shipping_address"]) ?></td>
                                <td style="background-color: white"><b>REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td style="line-height: 18px;resize: none;height: 75px;"><?php echo $customer["remark"] ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%;">
                            <table class="" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white;">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 40%">ITEM DETAIL</td>
                                    <td style="width: 80px;">UNIT</td>
                                    <td style="width: 80px;">PRICE</td>
                                    <td style="width: 80px;">CUR.STOCK</td>
                                    <td style="width: 80px;">ORD.QTY</td>
                                    <td style="width: 80px;">SHIPPED</td>
                                    <td style="width: 80px;">SHIPPING</td>
                                    <td style="width: 80px;">PENDING</td>
                                    <td >DISCREPANCY REASON</td>
                                </tr>
                                <?php
                                $index = 1;
                                $flot = "yes";
                                foreach ($itemsarrays as $key => $soitem) {
                                    $itemid = $soitem["item_id"];
                                    $value = MysqlConnection::getItemDataById($itemid);

                                    $currentstock = $value["onhand"];
                                    $ordered = $soitem["qty"];
                                    $shipped = $soitem["rQty"];

                                    if ($ordered > $currentstock) { // back order will created
                                        $pending = abs($ordered - $shipped);
                                    } else {
                                        $pending = abs($ordered - $shipped);
                                        $shipping = $ordered;
                                    }
                                    if ($currentstock == 0 || ($ordered - $shipped) > $currentstock) {
                                        $error = "Current stock is low, please create purchase order";
                                        $readonly = "readonly";
                                    } else {
                                        $error = "";
                                        $readonly = "";
                                    }
                                    if ($ordered != $shipped) {
                                        $flot = "no";
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px"><?php echo $index++ ?></td>
                                            <td >
                                                <?php echo $value["item_code"] ?>-<?php echo $value["item_desc_sales"] ?>
                                                <a href="index.php?pagename=create_perchaseorder&itemId=<?php echo $itemid ?>&flag=purchase&reqQty=<?php echo $pending ?>&cust=<?php echo urlencode($custcompanyname) ?>&so=<?php echo urlencode($sono) ?>">
                                                    <p style="color: red"><?php echo $error ?></p>
                                                </a>	
                                            </td>
                                            <td>
                                                <?php echo $value["unit"] ?>
                                                <input type="hidden" name="price[]" id="price<?php echo $index ?>" value="<?php echo $soitem["price"] ?>">
                                            </td>
                                            <td><?php echo $soitem["price"] ?></td>
                                            <td>
                                                <input type="hidden" name="currentstock[]" id="currentstock[]" value="<?php echo $currentstock ?>"/>
                                                <input type="hidden" name="soitemid[]" id="soitemid[]" value="<?php echo $soitem["id"] ?>"/>
                                                <input type="hidden" name="itemid[]" id="item_id" value="<?php echo $itemid ?>"/>
                                                <input type="text" style="text-align: center" value="<?php echo $currentstock ?>" readonly="" name="onhand<?php echo $index ?>" id="onhand<?php echo $index ?>">
                                            </td>
                                            <td><input style="text-align: center"  type="text" name="qty[]" readonly="" id="qty<?php echo $index ?>" required="" value="<?php echo $ordered ?>"></td>
                                            <td><input style="text-align: center" type="text" readonly="" name="shipped" id="shipped<?php echo $index ?>" value="<?php echo $soitem["rQty"] ?>"></td>
                                            <td><input style="text-align: center" <?php echo $readonly ?>  autofocus="" tabindex="<?php echo $index ?>" onfocusout="validateQty('<?php echo $index ?>')" type="text" name="shift[]" id="shift<?php echo $index ?>" required="" autofocus=""></td>
                                            <td>
                                                <input style="text-align: center" 
                                                       type="text" readonly="" name="pending[]"  
                                                       value="<?php echo $pending ?>" id="pending<?php echo $index ?>"
                                                       tabindex="<?php echo $index ?>"
                                                       required=""></td>
                                            <td>
                                                <select name="discrepancy[]"  id="discrepancy">
                                                    <option value=""> - - Skip process - - </option>
                                                    <?php foreach ($reasonarray as $values) { ?>
                                                        <option value="<?php echo $values["description"] ?>"><?php echo $values["description"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?> 
                                <tr style="background-color: white;">
                                    <td colspan="7"></td>
                                    <td colspan="2" style="width: 80px;"><b>GOOD TOTAL</b></td>
                                    <td ><div id="goodtotal"></div></td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td colspan="7"></td>
                                    <td colspan="2"  style="width: 80px;"><b>DISCOUNT <?php echo $customer["discount"] ?> %</b></td>
                                    <td >
                                        <div id="discount"></div>
                                        <input type="hidden" id="discountori" value="<?php echo $customer["discount"] ?>">
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td colspan="7"></td>
                                    <td colspan="2"  style="width: 80px;"><b>TAX <?php echo $customer["taxValue"] ?> %</b></td>
                                    <td >
                                        <div id="tax"></div>
                                        <input type="hidden" id="taxori" value="<?php echo $customer["taxValue"] ?>">
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td colspan="7"></td>
                                    <td colspan="2"  style="width: 80px;"><b>SHIPPING 5%</b></b></td>
                                    <td >
                                        <div id="shipping"><?php echo $customer["ship_charge"] ?></div>
                                        <input type="hidden" id="shippingtax" value="<?php echo $customer["ship_charge"] ?>">
                                    </td>
                                </tr>
                                <tr style="background-color: white;">
                                    <td colspan="7"></td>
                                    <td colspan="2"  style="width: 80px;"><b>NET TOTAL</b></td>
                                    <td ><div id="nettotal"></div></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer " style="text-align: center"> 
                <h4 style="color: red"><?php echo $error1 ?></h4>
                <?php if ($flot == "no") { ?>
                    <input type="submit" value="INVOICE" name="deleteItem" class="btn btn-info" tabindex="<?php echo $index + 1 ?>"/>
                    <input type="submit" id="mailButton" name="mailButton" class='btn btn-info' style="margin-left:4px;" value="EMAIL"/>
                    <?php
                } else {
                    echo "<h4 style='color:red'>Sales order close</h4>";
                }
                ?>
                <a href="index.php?pagename=manage_salesorder&status=Y" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
            </div>
            <hr/>
        </div>
    </div>
</form>
<script src="js/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/maruti.js"></script> 
<script src="js/maruti.form_common.js"></script>

<script>
    function validateQty(index) {
        $("#pending" + index).val("0");
        var o = 0;
        var s = 0;
        var sh = 0;

        var ordered = $("#qty" + index).val();
        var shipped = $("#shipped" + index).val();
        var shift = $("#shift" + index).val();

        if (shift === "") {
            shift = 0;
        }

        var o = parseInt(ordered);
        var s = parseInt(shift);
        var sh = parseInt(shipped);



        if (s > (o - sh)) {
            $("#shift" + index).val("");
            $("#shift" + index).focus();
        } else {
            $("#pending" + index).val(o - shipped - s);
        }
        doCalculation();
    }

    function doCalculation() {
        var goodstotal = 0;

        var taxori = $("#taxori").val();
        var discountori = $("#discountori").val();
        var shippingtax = $("#shippingtax").val();

        if (shippingtax === undefined || shippingtax === "" || shippingtax === 0) {
            shippingtax = 0;
        }

        var taxvalue = 0.0;
        var discountvalue = 0.0;
        var shippingvalue = 0.0;

        for (var index = 1; index <= 30; index++) {
            var shift = $("#shift" + index).val();
            var price = $("#price" + index).val();
            if (shift === undefined || shift === "" || shift === 0) {
                shift = 0;
            }
            if (price === undefined || price === "" || price === 0) {
                price = 0;
            }
            goodstotal = parseFloat(goodstotal) + (parseFloat(price) * parseFloat(shift));
        }
        discountvalue = parseFloat(goodstotal) * (discountori / 100);
        taxvalue = parseFloat(goodstotal) * (taxori / 100);

        var netTotal = parseFloat(goodstotal) - parseFloat(discountvalue) + parseFloat(taxvalue) + parseFloat(shippingtax);
        //discount  tax shipping

        $("#goodtotal").text(goodstotal.toFixed(2));
        $("#discount").text(discountvalue.toFixed(2));
        $("#tax").text(taxvalue.toFixed(2));
        $("#nettotal").text(netTotal.toFixed(2));
    }
</script>

<?php

function saveorupdate() {
    $filterdata = filter_input_array(INPUT_POST);
    $salesorderid = filter_input(INPUT_GET, "salesorderid");
    $invoice = time();
    if ($filterdata["deleteItem"] == "INVOICE" || $filterdata["mailButton"] == "EMAIL") {
        $indexcountr = count($filterdata["qty"]);
//        $pricearray = $filterdata["price"];
        $pendingorder = 0;
        for ($index = 0; $index < $indexcountr; $index++) {

            $arraysellinghist["qty"] = $shipping = $filterdata["shift"][$index];
            if ($shipping != 0) {

                $arraysellinghist["invoice"] = $invoice;
                $arraysellinghist["so_id"] = $salesorderid;
                $arraysellinghist["item_id"] = $filterdata["itemid"][$index];
                $arraysellinghist["price"] = $filterdata["price"][$index];
                $arraysellinghist["date"] = date("Y-m-d");

                unset($arraysellinghist["currentstock"]);
                MysqlConnection::insert("tbl_selling_history", $arraysellinghist);

                $arraysellinghist["currentstock"] = $filterdata["currentstock"][$index];
                $arrayselling["so_id"] = $salesorderid;
                $arrayselling["soitemid"] = $filterdata["soitemid"][$index];
                $arrayselling["item_id"] = $itemid = $arraysellinghist["item_id"];
                $arrayselling["discrepancy"] = $discrepancy = $filterdata["discrepancy"][$index];
                $arrayselling["backQty"] = $backQty = $filterdata["pending"][$index];
                $pendingorder = $pendingorder + $backQty;
                $arrayselling["rQty"] = $filterdata["shift"][$index];

                insertUpdateSalesItem($arraysellinghist, $arrayselling);
                MysqlConnection::updateItem($itemid, $shipping, "N");
                if (trim($discrepancy) != "") {
                    MysqlConnection::executeQuery("UPDATE item_master SET onhand = 0 WHERE `item_id` = '$itemid' ");
                }
            }
        }
        MysqlConnection::executeQuery("UPDATE sales_order SET invoiceno = '$invoice' WHERE id = '$salesorderid'  ");
        salesInvoice($salesorderid, $invoice);

        if ($pendingorder == 0) {
            MysqlConnection::executeQuery("UPDATE sales_order SET isOpen = 'N' , isBackOrder = 'N' WHERE id = '$salesorderid'");
        } else {
            salesBackOrder($salesorderid, "Y", $invoice);
            MysqlConnection::executeQuery("UPDATE sales_order SET isOpen = 'Y' , isBackOrder = 'Y' WHERE id = '$salesorderid'");
        }

        if ($filterdata["mailButton"] == "EMAIL") {
            header("location:index.php?pagename=email_retailinvoice&salesorderid=$salesorderid&invoiceno=$invoice&print=print");
        } else {
            header("location:index.php?pagename=manage_salesorder&status=N&action=invoice&salesorderid=$salesorderid");
        }
    }
}

function salesInvoice($salesorderid = "", $invoice) {
    include 'pdflib/salesinvoice.php';
}

function salesBackOrder($salesorderid = "", $backorder, $invoice) {
    include 'pdflib/salesinvoice.php';
}

function insertUpdateSalesItem($arraysellinghist, $arrayselling) {

    $soitemid = $arrayselling["soitemid"];
    $discrepancy = $arrayselling["discrepancy"];
    $getallsitem = "SELECT qty,rQty,backQty FROM sales_item WHERE id = '$soitemid' ";
    $sodetails = MysqlConnection::fetchCustom($getallsitem);
    $soitem = $sodetails[0];

    $updatedqty = $soitem["rQty"] + $arrayselling["rQty"];
    $updatedbackqty = $soitem["qty"] - ($soitem["rQty"] + $arrayselling["rQty"]);

    $discrepancycount = $arraysellinghist["currentstock"] - $arrayselling["rQty"];

    $updateqty = "UPDATE `sales_item` SET"
            . " `discrepancycount` = '$discrepancycount',"
            . " `rQty` = '$updatedqty',"
            . " `discrepancy` = '$discrepancy',"
            . " `backQty` = '$updatedbackqty'"
            . " WHERE `id` = '$soitemid';";
    MysqlConnection::executeQuery($updateqty);
}
