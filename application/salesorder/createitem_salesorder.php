<?php
if (MysqlConnection::validateItems()) {
    header("location:index.php?pagename=success_itemmaster&flag=noitems");
}
$customerlist = MysqlConnection::fetchCustom("SELECT id,cust_companyname FROM `customer_master` WHERE status = 'Y' ORDER BY `cust_companyname` ASC");
$customer = MysqlConnection::getCustomerDetails(filter_input(INPUT_GET, "customerId"));
if (isset($customer)) {
    $representivename = MysqlConnection::getGenericById($customer["sales_person_name"]);
}

$salesorderid = filter_input(INPUT_GET, "salesorderid");
if (!isset($salesorderid) || $salesorderid == "") {
    $sonumber = MysqlConnection::generateNumber("retailso");
}

$sqlitemarray = MysqlConnection::fetchCustom("SELECT count(id) as counter FROM sales_order");
$itemarray = MysqlConnection::fetchCustom("SELECT * FROM item_master;");
$buildauto = buildauto(MysqlConnection::fetchCustom("SELECT item_id ,item_code,item_desc_purch, item_name FROM item_master;"));
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY id DESC ;");

$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit)) {
    saveCustomer(filter_input_array(INPUT_POST));
}


/*
 * Edit sales order logic
 */

$salesorderidget = filter_input(INPUT_GET, "salesorderid");
if (isset($salesorderidget)) {
    $soorderdetails = MysqlConnection::getSalesOrderDetailsById($salesorderidget);

    if ($soorderdetails["isOpen"] == "N") {
        header("location:index.php?pagename=success_salesorder&salesorderid=$salesorderidget&flag=closederror");
    }

    $soitemsdetails = MysqlConnection::getSalesItemsDetailsById($salesorderidget);
    $sonumber = $soorderdetails["sono"];
    $soCustomer = MysqlConnection::getCustomerDetails($soorderdetails["customer_id"]);
    $representivename = MysqlConnection::getGenericById($soCustomer["sales_person_name"]);
}

saveOrEditSalesOrder();

$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");

$_SESSION["navigationpage"] = "index.php?pagename=create_salesorder";
?>
<?php

function saveCustomer($filterarray) {
    unset($filterarray["btnSubmit"]);
    if ($filterarray["salutation"] != null && salutation !== '') {
        unset($filterarray["salutation1"]);
    } else {
        $filterarray["salutation"] = $filterarray["salutation1"];
        unset($filterarray["salutation1"]);
    }
    MysqlConnection::insert("customer_master", $filterarray);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="js/script.js"></script>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>
<script>
    $(function() {
        for (var index = 1; index <= 30; index++) {
            $("#tags" + index).autocomplete({
                source: "itemmaster/autoitemajax.php",
            });
        }
    });
    function saleOrderRowDelete(rowid) {
        var row = document.getElementById(rowid);
        doCalculation();
        row.parentNode.removeChild(row);
    }

    $(document).ready(function($) {
        $("#phno").mask("(999) 999-9999");
        $("#cust_fax").mask("(999) 999-9999");
    });

</script>
<style>
    input,textarea,date{ width: 80%;height: 30px; }
    select{ width: 81.4%;height: 30px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANAGE SALES ORDER</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="display nowrap sortable" style="width: 100%">
                            <tr style="font-weight: bold; color: red">
                                <td><b>SO NUMBER</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><input style="color: red; width: 220px" type="text" name="sono" onkeypress="return chkNumericKey(event)" value="<?php echo $sonumber ?>" readonly=""></td>
                                <td><b>SALES DATE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><input style="width: 220px" type="text" name="salesdate" value="<?php echo MysqlConnection::convertToPreferenceDate(date("Y-m-d")) ?>" readonly=""></td>
                                <td><b>SALES PERSON </b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><input style="width: 220px" type="text" name="representative" id="representative" value="<?php echo $representivename["name"] . " " . $representivename["description"] ?>" readonly=""></td>
                            </tr>
                            <tr>
                                <td style="width: 10%">CUSTOMER NAME<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>

                                    <?php
                                    if (!isset($salesorderid) || $salesorderid == "") {
                                        ?>
                                        <select style="width: 225px;height: 25px" name="customer_id" id="customer_id" required="true">
                                            <option value="">SELECT</option>
                                            <option value="1"><< ADD NEW >></option>
                                            <?php
                                            foreach ($customerlist as $key => $value) {
                                                if (isset($soorderdetails)) {
                                                    $selected = $value["id"] == $soorderdetails["customer_id"] ? "selected" : "";
                                                }
                                                if (isset($customer)) {
                                                    $selected = $value["id"] == $customer["id"] ? "selected" : "";
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>"><?php echo $value["cust_companyname"] ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php
                                    } else {
                                        $customername = "";
                                        foreach ($customerlist as $customer) {
                                            if ($customer["id"] == $soorderdetails["customer_id"]) {
                                                $customername = $customer["cust_companyname"];
                                            }
                                        }
                                        ?>
                                        <input style="width: 220px" type="text" value="<?php echo $customername ?>" readonly="">
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td style="width: 10%">SHIP VIA<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <select style="width: 225px;height: 25px;" name="shipvia" id="shipvia" required="true"> 
                                        <option value="">&nbsp;&nbsp;</option>
                                        <option value="0" ><< ADD NEW >></option>
                                        <?php foreach ($shipviainfo as $key => $value) { ?>
                                            <option <?php echo $value["name"] == $soorderdetails["shipvia"] ? "selected" : "" ?>
                                                value="<?php echo $value["name"] ?>"  ><?php echo $value["name"] ?></option>
                                            <?php } ?>
                                    </select>
                                </td>
                                <td style="width: 10%">EXPECTED&nbsp;DELIVERY<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <input style="width: 220px" type="text" name="expected_date" id="datepicker"  value="<?php echo MysqlConnection::convertToPreferenceDate($soorderdetails["expected_date"]) ?>" required="true">

                                </td>
                            </tr>
                            <tr>
                                <td >BILLING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea style="line-height: 18px;width: 220px;height: 72px;resize: none" name="billTo_address" id="billTo_address"><?php echo $soorderdetails["billTo_address"] ?></textarea></td>
                                <td>SHIPPING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea style="line-height: 18px;width: 220px;height: 72px;resize: none" name="shipping_address" id="shipping_address"><?php echo $soorderdetails["shipping_address"] ?></textarea></td>
                                <td >REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea  style="line-height: 18px; color: red ; width: 220px;height: 72px;resize: none" name="remark"><?php echo $soorderdetails["remark"] ?></textarea></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 70%;float: left">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(250,250,250)">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 300px;">ITEM CODE / DESCRIPTION</td>
                                    <td style="width: 70px;">UNIT</td>
                                    <td style="width: 70px;">PRICE</td>
                                    <td style="width: 80px;">ON HAND</td>
                                    <td style="width: 80px;">ON SALE</td>
                                    <td style="width: 50px;">QTY</td>
                                    <td>AMOUNT</td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $indexedit = 1;
                                    $value = MysqlConnection::getItemDataById(filter_input(INPUT_GET, "itemId"));
                                    ?>
                                    <tr id="<?php echo $indexedit ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                        <td style="width: 25px">
                                            <a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $indexedit ?>')"></a>
                                        </td>
                                        <td style="width: 300px">
                                            <input type="text" name="items[]" value="<?php echo $value["item_code"] . " __ " . $value["item_desc_purch"] ?>" id="tags<?php echo $indexedit ?>" onfocusout="setDetails('<?php echo $indexedit ?>')"  style="padding: 0px;margin: 0px;width: 100%" readonly="">
                                        </td>
                                        <td style="width: 70px;"><div id="unit<?php echo $indexedit ?>"><?php echo $value["unit"] ?></div></td>
                                        <td style="width: 70px;"><div id="price<?php echo $indexedit ?>"><?php echo $value["total_sales_rate"] ?></div></td>
                                        <td style="width: 80px;"><div id="onhand<?php echo $indexedit ?>"><?php echo $value["onhand"] ?></div></td>
                                        <td style="width: 80px;"><div id="totalvalue<?php echo $indexedit ?>"><?php echo $value["totalvalue"] ?></div></td>
                                        <td style="width: 50px;"><input type="text" name="itemcount[]" value="<?php echo $soitem["qty"] ?>" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="doCalculation()" id="amount<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%"></td>
                                        <td ><div id="total<?php echo $indexedit ?>"><?php echo $soitem["qty"] * $value["total_sales_rate"] ?></div></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div style="width: 28%;float: right">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse;background-color: white" border="1">
                                <tr >
                                    <td  style="width: 150px;"><b>Goods Total</b></td>
                                    <td><input type="text" id="itemtotal" name="itemtotal" readonly="" value="<?php echo $soorderdetails["sub_total"] ?>"></td>
                                </tr>
                                <tr >
                                    <td><b>Goods Tax</b></td>
                                    <td><select style="width: 46%" name="taxname" id="taxname" required="">
                                            <option value="">SELECT</option>
                                            <?php
                                            foreach ($sqltaxinfodata as $key => $value) {
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["taxname"] ?>"><?php echo $value["taxname"] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <select style="width: 35%" name="taxValue" id="taxValue" required="">
                                            <option value="">SELECT</option>
                                            <?php
                                            foreach ($sqltaxinfodata as $key => $value) {
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["taxvalues"] ?>"><?php echo $value["taxvalues"] ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td> 
<!--                                    <td>
                                        <input type="text" id="taxname"  readonly=""  value="<?php echo $soorderdetails["taxname"] ?>" name='taxname' style="width: 45%" >
                                        <input type="text" id="taxvalue"  readonly=""  value="<?php echo $soorderdetails["taxValue"] ?>" name='taxValue' style="width: 30%" >
                                    </td>-->
                                </tr>
                                <tr >
                                    <td><b>Sub Total</b></td>
                                    <td><input type="text" id="finaltotal"  value="<?php echo $soorderdetails["taxtotal"] ?>" onkeypress="return chkNumericKey(event)" name="sub_total" readonly=""></td>
                                </tr>
                                <tr >
                                    <td><b>Shipping Charges</b><?php echo MysqlConnection::$REQUIRED ?></td>
                                    <td><input type="text" id="ship_charge"  value="<?php echo $soorderdetails["ship_charge"] ?>" maxlength="6" onkeyup="doCalculation()"  onkeypress="return chkNumericKey(event)" name="ship_charge" value="<?php echo $customer["ship_charge"] ?>" required="true" ></td>
                                </tr>
                                <tr >
                                    <td><b>Ship Tax</b></td>
                                    <td>
                                        <input type="text" id="staxname"  value="<?php echo $soorderdetails["staxname"] ?>" name='staxname' style="width: 45%" readonly="" >
                                        <input type="text" id="staxpercent"   value="<?php echo $soorderdetails["staxpercent"] ?>" name='staxpercent'  style="width: 33%" readonly="">
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Discount</b></td>
                                    <td><input type="text" id="discount"  value="<?php echo $soorderdetails["discount"] ?>" name="discount"  maxlength="3" onkeyup="doCalculation()"  name="discount" ></td>
                                </tr>
                                <tr >
                                    <td><b>Net Total</b></td>
                                    <td><input type="text" id="nettotal" readonly="" value="<?php echo $soorderdetails["total"] ?>"  name="total" name="nettotal" ></td>
                                </tr>
                                <tr >
                                    <td><b></b></td>
                                    <td><input type="hidden" value="<?php echo MysqlConnection::getAddedBy($_SESSION["user"]["user_id"]); ?>" readonly="" ></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <hr/>

        </div>
    </div>
    <div class="modal-footer "> 
        <center>
            <input type="submit"  class="btn btn-info" value="CREATE INVOICE">
            <input type="submit" id="btnSaveSalesOrder" name="btnSaveSalesOrder" class="btn btn-info" value="SAVE">
            <input type="submit" id="btnSaveNextSalesOrder" name="btnSaveNextSalesOrder" class="btn btn-info" value="SAVE AND NEW"/>
            <input type="submit" id="printButton" name="printButton" class='btn btn-info' value="PRINT"/>
            <input type="submit" id="mailButton" name="mailButton" class='btn btn-info' style="margin-left:4px;" value="EMAIL"/>
            <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
        </center>
    </div>
    <input type="hidden" name="onhand" id="onhand">
</form>

<?php

function buildauto($itemarray) {
    $option = "";
    foreach ($itemarray as $value) {
        $item_desc_purch1 = str_replace("\"", "", $value["item_desc_purch"]);
        $item_desc_purch2 = str_replace("'", "", $item_desc_purch1);
        $item_desc_purch3 = str_replace("''", "", $item_desc_purch2);
        $item_code1 = str_replace("\"", "", $value["item_code"]);
        $item_code2 = str_replace("'", "", $item_code1);
        $item_code3 = str_replace("''", "", $item_code2);
        $option .= "\"" . $item_code3 . " __ " . preg_replace('!\s+!', ' ', trim($item_desc_purch3)) . "\",";
    }

    return $option;
}
?>

<!-- this is custom model dialog --->
<div id="shipviamodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD SHIP VIA</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="ship_via" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>SHIP VIA NAME</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="name" id="name" required="true"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" class="btn btn-info">SAVE </a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->

<!-- this is customer model dialog --->
<div id="custtypemodel" class="modal hide" style="top: 3%;left: 25%;width: 90%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD NEW CUSTOMER</h3>
    </div>
    <?php include 'customdialogs/customerdialog.php'; ?>
</div>
<!-- this is customer model dialog --->



<script>

    $("#cancelct").click(function() {
        $("#customer_id").val("");
    });
    $("#customer_id").click(function() {
        var valueModel = $("#customer_id").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
    function deleteRow(rowid)
    {
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
        finalTotal();
    }

    $("#paymentTerm").click(function() {
        var valueModel = $("#paymentTerm").val();
        if (valueModel === "0") {
            $('#addPaymentTerm').modal('show');
        }
    });
    $("#savePaymentT").click(function() {
        var type = "paymentterm";
        var code = $("#termcode").val();
        var name = $("#termname").val();
        var description = $("#termdescription").val();
        $("div#divLoading").addClass('show');
        var dataString = "type=" + type + "&name=" + name + "&description=" + description + "&code=" + code;
        $.ajax({
            type: 'POST',
            url: 'customermaster/savepayterm_ajax.php',
            data: dataString
        }).done(function(data) {
            $('#paymentTerm').append(data);
            $("#termcode").val("");
            $("#termname").val("");
            $("#termdescription").val("");
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function() {
        });
    });

    $("#taxInformation").click(function() {
        var valueModel = $("#taxInformation").val();
        if (valueModel === "0") {
            $('#addTaxInformation').modal('show');
        }
    });
    $("#saveTaxInformation").click(function() {
        var taxcode = $("input[name='taxcode[]']").map(function() {
            return $(this).val();
        }).get();
        var taxtaxname = $("input[name='taxtaxname[]']").map(function() {
            return $(this).val();
        }).get();
        var taxtaxvalues = $("input[name='taxtaxvalues[]']").map(function() {
            return $(this).val();
        }).get();
        var taxisExempt = $("input[name='taxisExempt[]']").map(function() {
            return $(this).val();
        }).get();
        var dataString = "taxcode=" + taxcode + "&taxtaxname=" + taxtaxname + "&taxtaxvalues=" + taxtaxvalues + "&taxisExempt=" + taxisExempt;
        $("div#divLoading").addClass('show');
        $.ajax({
            type: 'POST',
            url: 'customermaster/savetaxinfo_ajax.php',
            data: dataString
        }).done(function(data) {
            $("input[name='taxcode[]']").val("");
            $("input[name='taxtaxname[]']").val("");
            $("input[name='taxtaxvalues[]']").val("");
            $("input[name='taxisExempt[]']").val("");
            $('#taxInformation').append(data);
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function() {
        });
    });

//////////  SHIP VIA INFORMATION //////////  
    $("#shipvia").click(function() {
        var valueModel = $("#shipvia").val();
        if (valueModel === "0") {
            $('#shipviamodel').modal('show');
        }
    });
    $("#cancelshipvia").click(function() {
        $("#shipvia").val("");
    });
    $("#saveshipvia").click(function() {
        var type = "shipvia";
        var name = $("#name").val();
        var code = name;
        var dataString = "type=" + type + "&code=" + code + "&name=" + name;
        $.ajax({
            type: 'POST',
            url: 'preferencemaster/save_preferencemaster.php',
            data: dataString
        }).done(function(data) {
            $('#shipvia').append(data);
            $("#code").val("");
            $("#name").val("");
        }).fail(function() {
        });
    });
    //////////  SHIP VIA INFORMATION //////////

    $("#customer_id").change(function() {
        $("div#divLoading").addClass('show');
        var dataString = "companyname=" + $("#customer_id").val();
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function(data) {
            var obj = JSON.parse(data);
            $("#billTo_address").val(obj.billto);
            $("#shipping_address").val(obj.shipto);
            $("#representative").val(obj.sales_person_name);
            //   
            $("#taxname").val(obj.taxname);
            $("#taxvalue").val(obj.taxValue);

            $("#staxname").val(obj.staxname);
            $("#staxpercent").val(obj.staxpercent);

            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function() {
        });
    });

    $("#cancelpt").click(function() {
        $("#paymentTerm").val("");
    });
    $("#cancelti").click(function() {
        $("#taxInformation").val("");
    });
</script>

<?php

function saveOrEditSalesOrder() {
    $btnSaveSalesOrder = filter_input(INPUT_POST, "btnSaveSalesOrder");
    $btnSaveNextSalesOrder = filter_input(INPUT_POST, "btnSaveNextSalesOrder");
    $printButton = filter_input(INPUT_POST, "printButton");
    $mailButton = filter_input(INPUT_POST, "mailButton");

    $salesorderdetails["sub_total"] = filter_input(INPUT_POST, "itemtotal");

    $salesorderdetails["taxname"] = filter_input(INPUT_POST, "taxname");
    $salesorderdetails["taxValue"] = filter_input(INPUT_POST, "taxValue");

    $salesorderdetails["staxname"] = filter_input(INPUT_POST, "staxname");
    $salesorderdetails["staxpercent"] = filter_input(INPUT_POST, "staxpercent");

    $salesorderdetails["taxtotal"] = filter_input(INPUT_POST, "sub_total");
    $salesorderdetails["ship_charge"] = filter_input(INPUT_POST, "ship_charge");
    $salesorderdetails["discount"] = filter_input(INPUT_POST, "discount");
    $salesorderdetails["total"] = filter_input(INPUT_POST, "total");

    $salesorderdetails["sono"] = filter_input(INPUT_POST, "sono");
    if (filter_input(INPUT_GET, "salesorderid") == "") {
        $salesorderdetails["customer_id"] = $customerid = filter_input(INPUT_POST, "customer_id");
    }
    $salesorderdetails["shipping_address"] = filter_input(INPUT_POST, "shipping_address");
    $salesorderdetails["billTo_address"] = filter_input(INPUT_POST, "billTo_address");
    $salesorderdetails["remark"] = filter_input(INPUT_POST, "remark");
    $salesorderdetails["shipvia"] = filter_input(INPUT_POST, "shipvia");

    $salesorderdetails["isOpen"] = "Y";
    $salesorderdetails["expected_date"] = filter_input(INPUT_POST, "expected_date");
    $salesorderdetails["expected_date"] = MysqlConnection::convertToDBDate($salesorderdetails["expected_date"]);
    $salesorderdetails["soorderdate"] = date("Y-m-d");
    $salesorderdetails["added_by"] = $_SESSION["user"]["user_id"];
    $salesorderdetails["deleteNote"] = "";
    $salesorderid = filter_input(INPUT_GET, "salesorderid");

    if (isset($btnSaveSalesOrder) || isset($btnSaveNextSalesOrder) || isset($printButton) || isset($mailButton)) {
        if (isset($salesorderid) && $salesorderid != "") {
            MysqlConnection::edit("sales_order", $salesorderdetails, " id = '$salesorderid' ");
            MysqlConnection::delete("DELETE FROM `sales_item` WHERE `so_id` = '$salesorderid' ");
        } else {
            $salesorderid = MysqlConnection::insert("sales_order", $salesorderdetails);
        }
        $backorderstatus = saveSalesOrderItems($salesorderid, $customerid);
        include 'pdflib/salesorder.php';
    }
//
    if ($btnSaveSalesOrder == "SUBMIT") {
        echo $btnSaveSalesOrder;
        header("location:index.php?pagename=manage_salesorder");
    } if ($btnSaveNextSalesOrder == "SUBMIT AND NEXT") {
        echo $btnSaveNextSalesOrder;
        header("location:index.php?pagename=manage_salesorder&isBackOrder=Y");
    }if ($printButton == "PRINT") {
        $_SESSION["salesorderid"] = $salesorderid;
        header("location:index.php?pagename=manage_salesorder");
    }if ($mailButton == "EMAIL") {
        echo $mailButton;
        header("location:index.php?pagename=email_salesorder&salesorderid=$salesorderid");
    }
}

function saveSalesOrderItems($insertid, $customerid) {
    $backorderstatus = "N";
    for ($index = 0; $index <= count($_POST["items"]); $index++) {
        $itemname = explode("__", $_POST["items"][$index]);
        $itemcode = trim($itemname[0]);
        $itemcount = $_POST["itemcount"][$index];
        if ($itemcode != "" && $itemcount != "") {
            $itemsfromcode = MysqlConnection::fetchCustom("SELECT item_id,onhand,totalvalue FROM `item_master` WHERE item_code = '$itemcode' ");
            $arraysalesitems = array();
            $item_id = $itemsfromcode[0]["item_id"];
            $onhand = $itemsfromcode[0]["onhand"];
            $arraysalesitems["item_id"] = $item_id;
            $arraysalesitems["so_id"] = $insertid;
            $arraysalesitems["qty"] = $itemcount;
            $leftstock = $onhand - $itemcount;
            if ($leftstock < 0) {
                $backorderstatus = "Y";
                $arraysalesitems["rQty"] = $itemcount - abs($leftstock);
                $arraysalesitems["backQty"] = $itemcount - $arraysalesitems["rQty"];
                MysqlConnection::insert("sales_item", $arraysalesitems);
                $updateqty = $onhand - $arraysalesitems["rQty"];
                MysqlConnection::delete("UPDATE item_master SET totalvalue = " . ($arraysalesitems["totalvalue"] + abs($leftstock)) . ", onhand = $updateqty  WHERE item_id = '$item_id' ");
            } else {
                $arraysalesitems["rQty"] = $itemcount;
                $arraysalesitems["backQty"] = $itemcount - $arraysalesitems["rQty"];
                MysqlConnection::insert("sales_item", $arraysalesitems);
                $updateqty = $onhand - $arraysalesitems["rQty"];
                MysqlConnection::delete("UPDATE item_master SET onhand = $updateqty  WHERE item_id = '$item_id' ");
            }
        }
    }
    $selectbal = "SELECT `balance` as balance  FROM `customer_master` where `id` =  '$customerid' ";
    $balancedetails = MysqlConnection::fetchCustom($selectbal);
    $balance = $balancedetails[0]["balance"] + filter_input(INPUT_POST, "total");
    MysqlConnection::delete("UPDATE `customer_master` SET `balance` = '$balance' WHERE `id` = '$customerid' ");
    if ($backorderstatus == "Y") {
        MysqlConnection::delete("UPDATE sales_order SET isBackOrder = 'Y' WHERE id = '$insertid'");
    } else {
        MysqlConnection::delete("UPDATE sales_order SET isBackOrder = 'N' , isOpen = 'N' WHERE id = '$insertid'");
    }
    return $backorderstatus;
}
?>