<?php
$salesorderid = filter_input(INPUT_GET, "salesorderid");
$subjectappend = "Sales Order";
$resultset = MysqlConnection::fetchCustom("SELECT sendfrom,`subjectline`,`footer`,`body` FROM `email_setup`");
$email = $resultset[0];

$salesorderdetails = MysqlConnection::getSalesOrderDetailsById($salesorderid);
$primary = MysqlConnection::getCustomerDetails($salesorderdetails["customer_id"]);
$mutiple = MysqlConnection::getCustomerContactDetails($salesorderdetails["customer_id"]);
$emailtosend = array();
foreach ($mutiple as $value) {
    array_push($emailtosend, " " . $value["person_email"] . "_(" . $value["designation"] . ")");
}
array_push($emailtosend, $primary["cust_email"]);
$attachment = "download/salesorder/$salesorderid.pdf";

if (isset($_POST["btnSubmitFullForm"])) {
    $subjectline = filter_input(INPUT_POST, "subjectline");
    $body = filter_input(INPUT_POST, "body");
    $body = str_replace("\n", "<br/>", $body);
    $body = "<html><body><br/><br/>" . $body . "</body></html>";
    $username = filter_input(INPUT_POST, "username");
    $emailtosend = explode(",", $username);

    foreach ($emailtosend as $value) {
        $explode = explode("_", $value);
        MysqlConnection::sendEmail(trim($explode[0]), $attachment, $subjectline, $body, "SALES ORDER");
    }
    header("location:index.php?pagename=manage_salesorder&status=Y&action=email");
}
?>

<div class="container-fluid" id="tabs">
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">EMAIL SALES ORDER</a></li>
            </ul>
        </div>
        <form name="frmCmpSubmit"  enctype="multipart/form-data" id="frmCmpSubmit" method="post">
            <div class="widget-content tab-content">
                <table  style="width: 100%;vertical-align: top" border="0" class="display nowrap sortable table-bordered">
                    <tr>
                        <td style="text-align: left;width: 80px;"><label  class="control-label" style="font-weight: bold">From</label></td>
                        <td><input style="width: 100%" type="text" readonly="" value="<?php echo $email["sendfrom"] ?>"></td>
                    </tr> 
                    <tr>
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Send To</label></td>
                        <td><input style="width: 100%"  type="text" name="username" id="username"  required="true" value="<?php echo implode(",", $emailtosend) ?>" ></td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Subject</label></td>
                        <td><input style="width: 100%"  type="text" name="subjectline" id="subjectline"  value="<?php echo trim($email["subjectline"]) . " | " . $subjectappend ?>" ></td>
                    </tr>
                    <tr style="height: 30px;vertical-align: middle">
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Attachment</label></td>
                        <td>
                            <a target="_blank" href="invoice/so_invoice.php?salesorderid=<?php echo $salesorderid ?>">VIEW ATTACHMENT</a>
                            <input type="hidden" name="emailpath" id="emailpath" value="<?php echo $po_no ?>">
                        </td>
                    </tr>
                    <tr >
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Body</label></td>
                        <td >
                            <textarea style="width: 100%;height: 200px;line-height: 22px;resize: none" name="body">&#13;&#10;&#13;&#10;<?php echo trim(strip_tags($email["body"])) ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SEND</button>
                <a href="index.php?pagename=manage_salesorder&status=Y" class="btn btn-info">CANCEL</a>
            </div> 
        </form>
    </div>
</div> 