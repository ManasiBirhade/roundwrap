<?php
$isBackOrder = filter_input(INPUT_GET, "status");
if ($isBackOrder == "") {
    $isBackOrder = "Y";
}
$action = filter_input(INPUT_GET, "action");
$salesorderid = filter_input(INPUT_GET, "salesorderid");
$from = filter_input(INPUT_GET, "from");
$invoiceno = filter_input(INPUT_GET, "invoiceno");
$isBackOrderFilter = filter_input(INPUT_GET, "isBackOrder");

$sql = "SELECT * FROM `sales_order`";
if ($isBackOrderFilter == "Y") {
    $sql = $sql . " WHERE isBackOrder = 'Y' ";
    $title = "BACK ORDER LIST";
} else {
    $title = "SALES ORDER LIST";
}
$listSalesOrders = MysqlConnection::fetchCustom($sql . "  ORDER BY idindex DESC LIMIT 0,100");

if ($action == "print") {
    echo "<script language='javascript'>window.open('invoice/customer_sales_order.php?salesorderid=$salesorderid');</script>";
} else if ($action == "invoice") {
    echo "<script language='javascript'>window.open('invoice/print_salesinvoice.php?salesorderid=$salesorderid');</script>";
} else if ($action == "from") {
    echo "<script language='javascript'>window.open('invoice/print_creditnote.php?invoiceno=$invoiceno');</script>";
}
?>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<script src="index.js"></script>
<div class="container-fluid">
    <br/>
    <?php
    if ($action == "add") {
        echo MysqlConnection::$MSG_AD;
    } elseif ($action == "delete") {
        echo MysqlConnection::$MSG_DE;
    } elseif ($action == "update") {
        echo MysqlConnection::$MSG_UP;
    } elseif ($action == "closederror") {
        echo MysqlConnection::$MSG_CL;
    } elseif ($action == "email") {
        echo MysqlConnection::$MSG_SU;
    }
    ?>
    <div class="cutomheader">
        <?php
        if ($isBackOrder == "Y") {
            echo '<h5 style="font-family: verdana;font-size: 12px;">SALES ORDER LIST</h5>';
        } else {
            echo '<h5 style="font-family: verdana;font-size: 12px;">SALES ORDER LIST</h5>';
        }
        ?>
    </div>
    <br/>
    <table style="width: 100%" border="0">
        <tr>
            <td style="float: left">
                <a class="btn btn-info" href="index.php?pagename=create_salesorder" >&nbsp;CREATE SALES ORDER</a>
                <a class="btn btn-info" href="index.php?pagename=createcash_salesorder" >&nbsp;CREATE CASH ORDER</a>
                <a class="btn btn-info" href="index.php?pagename=manage_salesorder" >&nbsp;REFRESH</a>
            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 50px">#</td>
                <td style="width: 80px">PO No</td>
                <td style="width: 80px">SO No</td>
                <td style="width: 100px">Invoice</td>
                <td style="width: 250px">Customer Name</td>
                <td style="width: 100px">SO Status</td>
                <td style="width: 100px">Ship Via</td>
                <td style="width: 100px">Gross Amount</td>
                <td style="width: 100px">Discount</td>
                <td style="width: 100px">Net Amount</td>
                <td style="width: 100px">Delivery Date</td>
                <td>Entered By</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listSalesOrders as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $customername = MysqlConnection::fetchCustom("SELECT cust_companyname FROM customer_master WHERE id = '" . $value["customer_id"] . "'");
                $isOpen = $value["isOpen"] == "Y" ? "Open" : "Close";
                $isOpenclt = $value["isOpen"] == "Y" ? "btn btn-success btn-mini" : "btn btn-warning  btn-mini";
                $invoicecount = MysqlConnection::fetchCustom("SELECT DISTINCT(`invoice`) FROM `tbl_selling_history`  WHERE `so_id` = '" . $value["id"] . "'");
                ?>
                <tr id="<?php echo $value["id"] ?>" class="context-menu-one" onclick="setId('<?php echo $value["id"] ?>')" style="background-color: <?php echo $bgcolor ?>;">
                    <td><?php echo $index ?></td>
                    <td><?php echo $value["pono"] ?></td>
                    <td><span  data-toggle="tooltip" data-original-title="View Sales Order"><a href="index.php?pagename=view_salesorder&salesorderid=<?php echo $value["id"] ?>"><?php echo $value["sono"] ?></a></span></td>
                    <td><span  data-toggle="tooltip" data-original-title="View Invoice"> <a href="index.php?pagename=view_retailinvoice&salesorderid=<?php echo $value["id"] ?>&action=search"><?php echo count($invoicecount) ?></a></span></td>
                    <td><span  data-toggle="tooltip" data-original-title="Go To Customer Details"><a href="index.php?pagename=view_customermaster&customerId=<?php echo $value["customer_id"] ?>"><?php echo $customername[0]["cust_companyname"] ?></a></span></td>
                    <td><i class="<?php echo $isOpenclt ?>" ><?php echo $isOpen ?></i></td>
                    <td><?php echo $value["shipvia"] ?></td>
                    <td><?php echo $value["sub_total"] ?></td>
                    <td><?php echo $value["discount"] ?></td>
                    <td><?php echo $value["total"] ?></td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($value["expected_date"]) ?></td>
                    <td ><a href="index.php?pagename=view_usermanagement&userid=<?php echo $value["added_by"] ?>"><?php echo MysqlConnection::getAddedBy($value["added_by"]) ?></a></td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" style="margin-left:10px;">EMAIL</button>
        </div> 
    </div>
    <input type="hidden" id="deleteId" name="cid" value="">
    <input type="hidden" id="flag" name="flag" value="">
    <input type="hidden" id="rightClikId" name="rightClikId" value="">
</div>
<script>
    $('#example tbody tr').click(function (e) {
        var id = $(this).attr('id');
        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var del = '<a href="invoice/customer_sales_order.php?salesorderid=' + id + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var em = '<a href="index.php?pagename=email_salesorder&salesorderid=' + id + '" ><button type="button" class=" btn btn-info" style="margin-left:10px;">EMAIL</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + del + em + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });


    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }

    function setId(val) {
        document.getElementById("rightClikId").value = val;
    }

    $("#myElem").show().delay(3000).fadeOut();
</script>
<script>
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_salesorder":
                        window.location = "index.php?pagename=view_salesorder&salesorderid=" + id;
                        break;
                    case "create_order":
                        window.location = "index.php?pagename=manage_customermaster";
                        break;

                    case "edit_salesorder":
                        window.location = "index.php?pagename=create_salesorder&salesorderid=" + id;
                        break;

                    case "note_salesorder":
                        window.location = "index.php?pagename=note_customermaster&salesorderid=" + id;
                        break;



                    case "return_goods":
                        window.location = "index.php?pagename=return_salesorder&salesorderid=" + id;
                        break;
                    case "delete_salesorder":
                        window.location = "index.php?pagename=view_salesorder&salesorderid=" + id + "&flag=yes";
                        break;

                    case "return_salesorder":
                        window.location = "index.php?pagename=view_salesorder&salesorderid=" + id + "&flag=return";
                        break;

                    case "create_salesorder":
                        window.location = "index.php?pagename=create_salesorderreceiving&salesorderid=" + id;
                        break;
                    case "print_so":
                        window.open("invoice/customer_sales_order.php?salesorderid=" + id, '_blank');
                        break;
                    case "print_back":
                        window.open("invoice/customer_back_order.php?salesorderid=" + id, '_blank');
                        break;
                    case "print_invoice":
                        window.open("invoice/print_salesinvoice.php?salesorderid=" + id, '_blank');
                        break;
                    case "create_invoice":
                        window.location = "index.php?pagename=createinvoice_salesorder&salesorderid=" + id;
                        break;
                    case "mail_salesorder":
                        window.location = "index.php?pagename=email_salesorder&salesorderid=" + id;
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    default:
                        window.location = "index.php?pagename=manage_salesorder";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_salesorder": {name: "VIEW ORDER", icon: "+"},
                "edit_salesorder": {name: "EDIT ORDER", icon: "context-menu-icon-add"},
                "delete_salesorder": {name: "DELETE ORDER", icon: ""},
                "note_salesorder": {name: "CUSTOMER NOTE", icon: ""},
                "sep1": "---------",
                //"create_salesorder": {name: "ENTER BACK ORDER", icon: ""},
//                "return_goods": {name: "RETURN GOODS", icon: ""},
                "return_salesorder": {name: "CANCEL SALES ORDER", icon: ""},
                "sep3": "---------",
                //"print_back": {name: "PRINT BACK ORDER", icon: ""},
                "print_invoice": {name: "PRINT INVOICE", icon: ""},
                "create_invoice": {name: "CREATE INVOICE", icon: ""},
                "sep2": "---------",
                "quit": {name: "QUIT", icon: function () {
                        return '';
                    }}
            }
        });
    });

    $('tr').dblclick(function () {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_salesorder&salesorderid=" + id;
        }
    });

</script>
