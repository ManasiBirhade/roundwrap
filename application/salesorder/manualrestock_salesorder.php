<?php
error_reporting(0);

$action = filter_input(INPUT_GET, "action");

if (MysqlConnection::validateItems()) {
    header("location:index.php?pagename=success_itemmaster&flag=noitems");
}
$customerlist = MysqlConnection::fetchCustom("SELECT id,cust_companyname FROM `customer_master` WHERE status = 'Y' ORDER BY `cust_companyname` ASC");
$customer = MysqlConnection::getCustomerDetails(filter_input(INPUT_GET, "customerId"));
if (isset($customer)) {
    $representivename = MysqlConnection::getGenericById($customer["sales_person_name"]);
}

$salesorderid = filter_input(INPUT_GET, "salesorderid");
if (!isset($salesorderid) || $salesorderid == "") {
    $sonumber = MysqlConnection::generateNumber("retailso");
}

$sqlitemarray = MysqlConnection::fetchCustom("SELECT count(id) as counter FROM sales_order");
$itemarray = MysqlConnection::fetchCustom("SELECT * FROM item_master;");
$buildauto = buildauto(MysqlConnection::fetchCustom("SELECT item_id ,item_code,item_desc_purch, item_name FROM item_master;"));
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY taxtype ASC ;");

$btnSubmit = filter_input(INPUT_POST, "btnSubmit");
if (isset($btnSubmit)) {
    saveCustomer(filter_input_array(INPUT_POST));
}


/*
 * Edit sales order logic
 */

$salesorderidget = filter_input(INPUT_GET, "salesorderid");

if (isset($salesorderidget)) {
    $soorderdetails = MysqlConnection::getSalesOrderDetailsById($salesorderidget);

    if ($soorderdetails["isOpen"] == "N") {
        //header("location:index.php?pagename=success_salesorder&salesorderid=$salesorderidget&flag=closederror");
        header("location:index.php?pagename=manage_salesorder&status=Y&action=closederror");
    }

    $soitemsdetails = MysqlConnection::getSalesItemsDetailsById($salesorderidget);
    $sonumber = $soorderdetails["sono"];
    $soCustomer = MysqlConnection::getCustomerDetails($soorderdetails["customer_id"]);
    $representivename = MysqlConnection::getGenericById($soCustomer["sales_person_name"]);
}

saveOrEditSalesOrder();

$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");

$_SESSION["navigationpage"] = "index.php?pagename=create_salesorder";
?>
<?php

function saveCustomer($filterarray) {
    unset($filterarray["btnSubmit"]);
    if ($filterarray["salutation"] != null && salutation !== '') {
        unset($filterarray["salutation1"]);
    } else {
        $filterarray["salutation"] = $filterarray["salutation1"];
        unset($filterarray["salutation1"]);
    }
    MysqlConnection::insert("customer_master", $filterarray);
    echo "<meta http-equiv='refresh' content='0'>";
}
?>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="js/script.js"></script>

<script src="js/select2.min.js"></script>
<link href="css/select2.css" rel="stylesheet">

<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });</script>
<script>
    $(function () {
        for (var index = 1; index <= 30; index++) {
            $("#tags" + index).autocomplete({
                source: "itemmaster/autoitemajax.php",
            });
        }
    });
    function saleOrderRowDelete(rowid) {
        var row = document.getElementById(rowid);
        doCalculation();
        row.parentNode.removeChild(row);
    }

    $(document).ready(function ($) {
        $("#phno").mask("(999) 999-9999");
        $("#cust_fax").mask("(999) 999-9999");
    });</script>
<style>
    input,textarea,date{ width: 80%;height: 30px; }
    select{ width: 81.4%;height: 30px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    .ui-autocomplete {
        max-height: 300px;
        overflow-y: auto;   /* prevent horizontal scrollbar */
        overflow-x: hidden; /* add padding to account for vertical scrollbar */
        z-index:1000 !important;
    }
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <?php
        if ($action == "created") {
            echo MysqlConnection::$MSG_AD;
        }
        ?>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">MANUAL CREDIT NOTE</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="display nowrap sortable" style="width: 100%">
                            <tr>
                                <td style="width: 10%">CUSTOMER NAME<?php echo MysqlConnection::$REQUIRED ?></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <?php
                                    if (!isset($salesorderid) || $salesorderid == "") {
                                        ?>
                                        <select autofocus="" tabindex="1" style="width: 225px;height: 25px" name="customer_id" id="customer_id" required="true">
                                            <option value="">SELECT</option>
                                            <option value="1"><< ADD NEW >></option>
                                            <?php
                                            foreach ($customerlist as $key => $value) {
                                                if (isset($soorderdetails)) {
                                                    $selected = $value["id"] == $soorderdetails["customer_id"] ? "selected" : "";
                                                }
                                                if (isset($customer)) {
                                                    $selected = $value["id"] == $customer["id"] ? "selected" : "";
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>"><?php echo $value["cust_companyname"] ?></option>
                                            <?php } ?>
                                        </select>
                                        <?php
                                    } else {
                                        $customername = "";
                                        foreach ($customerlist as $customer) {
                                            if ($customer["id"] == $soorderdetails["customer_id"]) {
                                                $customername = $customer["cust_companyname"];
                                            }
                                        }
                                        ?>
                                        <input style="width: 220px" type="text" value="<?php echo $customername ?>" readonly="">
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td><b>INVOICE NUMBER</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <input type="text" name="invoiceno" id="invoiceno" value="<?php echo time() ?>" required="" minlength="3" maxlength="30" style="color: red; width: 220px">
                                    <input style="color: red; width: 220px" type="hidden" name="sono" onkeypress="return chkNumericKey(event)" value="<?php echo $sonumber ?>" readonly="">
                                </td>

                                <td><b>SALES DATE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td>
                                    <input style="width: 220px" type="text" name="salesdate" value="<?php echo MysqlConnection::convertToPreferenceDate(filter_input(INPUT_COOKIE, "dateme")) ?>" readonly="">
                                </td>
                            </tr>
                            <tr>
                                <td >BILLING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea readonly=""  style="line-height: 18px;width: 220px;height: 72px;resize: none" name="billTo_address" id="billTo_address"><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $soorderdetails["billTo_address"])) ?></textarea></td>
                                <td>SHIPPING&nbsp;ADDRESS</td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea style="line-height: 18px;width: 220px;height: 72px;resize: none" name="shipping_address" id="shipping_address"><?php echo MysqlConnection::formatToSlashNAddress(str_replace("<br />", "\n", $soorderdetails["shipping_address"])) ?></textarea></td>
                                <td >REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td>&nbsp;:&nbsp</td>
                                <td><textarea  style="line-height: 18px; color: red ; width: 220px;height: 72px;resize: none" name="remark"><?php echo $soorderdetails["remark"] ?></textarea></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 70%;float: left">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(250,250,250)">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 300px;">ITEM CODE / DESCRIPTION</td>
                                    <td style="width: 70px;">UNIT</td>
                                    <td style="width: 70px;">
                                        <div id="pricediscount">PRICE</div>
                                        <input type="hidden" name="pricediscountvalue" id="pricediscountvalue">
                                    </td>
                                    <td style="width: 80px;">ON HAND</td>
                                    <td style="width: 80px;">ON SALE</td>
                                    <td style="width: 50px;">QTY</td>
                                    <td>AMOUNT</td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $indexedit = 1;
                                    foreach ($soitemsdetails as $soitem) {
                                        $value = MysqlConnection::getItemDataById($soitem["item_id"]);
                                        ?>
                                        <tr id="<?php echo $indexedit ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $indexedit ?>')"></a>
                                            </td>
                                            <td style="width: 300px">
                                                <input type="text" name="items[]" value="<?php echo $value["item_code"] . " __ " . $value["item_desc_purch"] ?>" id="tags<?php echo $indexedit ?>" onfocusout="setDetails('<?php echo $indexedit ?>')"  style="padding: 0px;margin: 0px;width: 100%">
                                            </td>
                                            <td style="width: 70px;"><div id="unit<?php echo $indexedit ?>"><?php echo $value["unit"] ?></div></td>
                                            <td style="width: 70px;">
                                                <input type="text" name="price[]" id="price<?php echo $indexedit ?>" value="<?php echo $soitem["price"] ?>">
                                                <div id="price<?php // echo $indexedit                            ?>"><?php // echo $value["total_sales_rate"]                          ?></div>
                                            </td>
                                            <td style="width: 80px;"><div id="onhand<?php echo $indexedit ?>"><?php echo $value["onhand"] ?></div></td>
                                            <td style="width: 80px;"><div id="totalvalue<?php echo $indexedit ?>"><?php echo $value["totalvalue"] ?></div></td>
                                            <td style="width: 50px;"><input type="text" name="itemcount[]" value="<?php echo $soitem["qty"] ?>" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="doCalculation('<?php echo $indexedit ?>')" id="amount<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><div id="total<?php echo $indexedit ?>"><?php echo $soitem["qty"] * $value["total_sales_rate"] ?></div></td>
                                        </tr>
                                        <?php
                                        $indexedit++;
                                    }
                                    for ($index = $indexedit; $index <= 50; $index++) {
                                        if (count($soitemsdetails) == 0) {
                                            if ($index == 1) {
                                                $required = "required";
                                            } else {
                                                $required = "";
                                            }
                                        }
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 25px">
                                                <a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $index ?>')"></a>
                                            </td>
                                            <td style="width: 300px">
                                                <input type="text" name="items[]" <?php echo $required ?> id="tags<?php echo $index ?>" onfocusout="setDetails('<?php echo $index ?>')"  style="padding: 0px;margin: 0px;width: 100%">
                                            </td>
                                            <td style="width: 70px;"><div id="unit<?php echo $index ?>"></div></td>
                                            <td style="width: 70px;">
                                                <input type="text" name="price[]" id="price<?php echo $index ?>" >
                                                <!--<div id="price<?php echo $index ?>"></div>-->
                                            </td>
                                            <td style="width: 80px;"><div id="onhand<?php echo $index ?>"></div></td>
                                            <td style="width: 80px;"><div id="totalvalue<?php echo $index ?>"></div></td>
                                            <td style="width: 50px;"><input type="text" name= "itemcount[]" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="doCalculation('<?php echo $index ?>')" id="amount<?php echo $index ?>" style="padding: 0px;margin: 0px;width: 100%"></td>
                                            <td ><div id="total<?php echo $index ?>"></div></td>
                                        </tr>
                                    <?php } ?>

                                </table>
                            </div>
                        </div>
                        <style>
                            b{
                                color: #000000;
                                font-weight: normal;
                            }
                        </style>
                        <div style="width: 28%;float: right">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse;background-color: white" border="1">


                                <tr >
                                    <td  style="width: 150px;"><b>Goods Total</b></td>
                                    <td><input type="text" id="itemtotal" name="itemtotal" value="<?php echo $soorderdetails["sub_total"] ?>"></td>
                                </tr>
                                <tr >
                                    <td  style="width: 150px;"><b>Discount %</b></td>
                                    <td>
                                        <input  style="width: 40%" type="text" id="discount"  value="<?php echo $soorderdetails["discount"] ?>" name="discount"  maxlength="3" onfocusout="doCalculation()" name="discount" >
                                        <input  style="width: 40%" type="text" id="discountvalue"  value="<?php echo $soorderdetails["discountvalue"] ?>" name="discountvalue" readonly="">
                                    </td>
                                </tr>
                                <tr >
                                    <td><b>Goods Tax</b></td>
                                    <td><select  name="taxid" id="taxid" required="" onchange="doCalculation()" style="height: 25px;width: 52%">
                                            <option value="">SELECT</option>
                                            <?php
                                            foreach ($sqltaxinfodata as $key => $value) {
                                                if ($soorderdetails["taxid"] == $value["id"]) {
                                                    $selected = "selected";
                                                } else {
                                                    $selected = "";
                                                }
                                                ?>
                                                <option <?php echo $selected ?> value="<?php echo $value["id"] ?>">
                                                    <?php echo ucwords($value["taxtype"]) ?>
                                                    <?php echo $value["taxname"] ?>-<?php echo $value["taxvalues"] ?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                        <input type="text" id="taxamount" readonly="" value="<?php echo $soorderdetails["taxamount"] ?>" name='taxamount' style="width: 27%" >
                                    </td> 
                                </tr>
                                <tr>
                                    <td><b>Sub Total</b></td>
                                    <td><input type="text" id="finaltotal"  value="<?php echo $soorderdetails["taxtotal"] ?>" onkeypress="return chkNumericKey(event)" name="sub_total" readonly=""></td>
                                </tr>
                                <tr >
                                    <td><b>Restocking Charges(%)</b></td>
                                    <td>
                                        <input type="text" id="restockpercent"   name='restockpercent' onkeyup="doCalculation()" style="width: 35%" >
                                        <input type="text" id="restockamount" readonly=""  name='restockamount' style="width: 40%" >
                                    </td> 
                                </tr>
                                <tr >
                                    <td><b>Net Total</b></td>
                                    <td>
                                        <input type="text" id="nettotal" readonly="" value="<?php echo $soorderdetails["total"] ?>"  name="total" name="nettotal" >
                                        <input type="hidden" value="<?php echo MysqlConnection::getAddedBy($_SESSION["user"]["user_id"]); ?>" readonly="" >
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>
                    <input type="submit" id="btnSaveSalesOrder" name="btnSaveSalesOrder" class="btn btn-info" value="SAVE">
                    <input type="submit" id="btnSaveNextSalesOrder" name="btnSaveNextSalesOrder" class="btn btn-info" value="SAVE AND NEW"/>
                    <input type="submit" id="printButton" name="printButton" class='btn btn-info' value="PRINT"/>
                    <input type="submit" id="mailButton" name="mailButton" class='btn btn-info' style="margin-left:4px;" value="EMAIL"/>
                    <a href="index.php?pagename=manage_salesorder" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                </center>
            </div>
            <input type="hidden" name="onhand" id="onhand">
        </div>
    </div>

</form>

<?php

function buildauto($itemarray) {
    $option = "";
    foreach ($itemarray as $value) {
        $item_desc_purch1 = str_replace("\"", "", $value["item_desc_purch"]);
        $item_desc_purch2 = str_replace("'", "", $item_desc_purch1);
        $item_desc_purch3 = str_replace("''", "", $item_desc_purch2);
        $item_code1 = str_replace("\"", "", $value["item_code"]);
        $item_code2 = str_replace("'", "", $item_code1);
        $item_code3 = str_replace("''", "", $item_code2);
        $option .= "\"" . $item_code3 . " __ " . preg_replace('!\s+!', ' ', trim($item_desc_purch3)) . "\",";
    }

    return $option;
}
?>

<!-- this is custom model dialog --->
<div id="shipviamodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD SHIP VIA</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" method="post" action="#" name="ship_via" id="cust_type" novalidate="novalidate">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>SHIP VIA NAME</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="name" id="name" required="true"></td>
                </tr>
            </table>
        </form>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" class="btn btn-info">SAVE </a> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is model dialog --->

<!-- this is customer model dialog --->
<div id="custtypemodel" class="modal hide" style="top: 3%;left: 25%;width: 90%; overflow: hidden">
    <div class="modal-header">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>ADD NEW CUSTOMER</h3>
    </div>
    <?php include 'customdialogs/customerdialog.php'; ?>
</div>
<!-- this is customer model dialog --->



<script>

    $(document).ready(function () {
        $('#customer_id').select2();
    });

    $("#cancelct").click(function () {
        $("#customer_id").val("");
    });
    $("#customer_id").click(function () {
        var valueModel = $("#customer_id").val();
        if (valueModel === "1") {
            $('#custtypemodel').modal('show');
        }
    });
    function deleteRow(rowid)
    {
        var row = document.getElementById(rowid);
        row.parentNode.removeChild(row);
        finalTotal();
    }

    $("#paymentTerm").click(function () {
        var valueModel = $("#paymentTerm").val();
        if (valueModel === "0") {
            $('#addPaymentTerm').modal('show');
        }
    });
    $("#savePaymentT").click(function () {
        var type = "paymentterm";
        var code = $("#termcode").val();
        var name = $("#termname").val();
        var description = $("#termdescription").val();
        $("div#divLoading").addClass('show');
        var dataString = "type=" + type + "&name=" + name + "&description=" + description + "&code=" + code;
        $.ajax({
            type: 'POST',
            url: 'customermaster/savepayterm_ajax.php',
            data: dataString
        }).done(function (data) {
            $('#paymentTerm').append(data);
            $("#termcode").val("");
            $("#termname").val("");
            $("#termdescription").val("");
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    });
    $("#taxInformation").click(function () {
        var valueModel = $("#taxInformation").val();
        if (valueModel === "0") {
            $('#addTaxInformation').modal('show');
        }
    });
    $("#saveTaxInformation").click(function () {
        var taxcode = $("input[name='taxcode[]']").map(function () {
            return $(this).val();
        }).get();
        var taxtaxname = $("input[name='taxtaxname[]']").map(function () {
            return $(this).val();
        }).get();
        var taxtaxvalues = $("input[name='taxtaxvalues[]']").map(function () {
            return $(this).val();
        }).get();
        var taxisExempt = $("input[name='taxisExempt[]']").map(function () {
            return $(this).val();
        }).get();
        var dataString = "taxcode=" + taxcode + "&taxtaxname=" + taxtaxname + "&taxtaxvalues=" + taxtaxvalues + "&taxisExempt=" + taxisExempt;
        $("div#divLoading").addClass('show');
        $.ajax({
            type: 'POST',
            url: 'customermaster/savetaxinfo_ajax.php',
            data: dataString
        }).done(function (data) {
            $("input[name='taxcode[]']").val("");
            $("input[name='taxtaxname[]']").val("");
            $("input[name='taxtaxvalues[]']").val("");
            $("input[name='taxisExempt[]']").val("");
            $('#taxInformation').append(data);
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    });
//////////  SHIP VIA INFORMATION //////////  
    $("#shipvia").click(function () {
        var valueModel = $("#shipvia").val();
        if (valueModel === "0") {
            $('#shipviamodel').modal('show');
        }
    });
    $("#cancelshipvia").click(function () {
        $("#shipvia").val("");
    });
    $("#saveshipvia").click(function () {
        var type = "shipvia";
        var name = $("#name").val();
        var code = name;
        var dataString = "type=" + type + "&code=" + code + "&name=" + name;
        $.ajax({
            type: 'POST',
            url: 'preferencemaster/save_preferencemaster.php',
            data: dataString
        }).done(function (data) {
            $('#shipvia').append(data);
            $("#code").val("");
            $("#name").val("");
        }).fail(function () {
        });
    });
    //////////  SHIP VIA INFORMATION //////////

    $("#customer_id").change(function () {
        $("div#divLoading").addClass('show');
        var dataString = "companyname=" + $("#customer_id").val();
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function (data) {
            var obj = JSON.parse(data);
            $("#billTo_address").val(obj.billto);
            $("#shipping_address").val(obj.shipto);
            $("#representative").val(obj.sales_person_name);
            var isExempt = obj.isExempt;
            if (isExempt === "Y") {
                $("#taxid").prop("disabled", true);
            } else {
                $("#taxid").val(obj.taxInformation);
            }
            //$("#staxname").val(obj.staxid);
            $("#staxname").val(obj.staxname + "-5");
            $("#staxpercent").val(obj.staxpercent);
            $("#pricediscount").text("PRICE-" + obj.discount + "%");
            $("#pricediscountvalue").text(obj.discount);
            doCalculation();
            $("div#divLoading").addClass('show');
            $("div#divLoading").removeClass('show');
        }).fail(function () {
        });
    });
    $("#cancelpt").click(function () {
        $("#paymentTerm").val("");
    });
    $("#cancelti").click(function () {
        $("#taxInformation").val("");
    });

    $("#myElem").show().delay(3000).fadeOut();

    $("select#staxname").change(function () {
        doCalculation();
    });
</script>

<?php

function saveOrEditSalesOrder() {
    $btnSaveSalesOrder = filter_input(INPUT_POST, "btnSaveSalesOrder");
    $btnSaveNextSalesOrder = filter_input(INPUT_POST, "btnSaveNextSalesOrder");
    $printButton = filter_input(INPUT_POST, "printButton");
    $mailButton = filter_input(INPUT_POST, "mailButton");
    $invoiceButton = filter_input(INPUT_POST, "invoice");

    $salesorderdetails["sub_total"] = filter_input(INPUT_POST, "itemtotal");
    $salesorderdetails["pono"] = filter_input(INPUT_POST, "pono");

    $salesorderdetails["invoiceno"] = $invoiceno = filter_input(INPUT_POST, "invoiceno");

    $salesorderdetails["taxid"] = $taxid = filter_input(INPUT_POST, "taxid");
    $taxarray = MysqlConnection::getTaxInfoById($taxid);
    $salesorderdetails["taxname"] = $taxarray["taxname"];
    $salesorderdetails["taxValue"] = $taxarray["taxvalues"];
    $salesorderdetails["taxamount"] = filter_input(INPUT_POST, "taxamount");

    $salesorderdetails["staxname"] = filter_input(INPUT_POST, "staxname");
    $salesorderdetails["staxpercent"] = filter_input(INPUT_POST, "staxpercent");

    $salesorderdetails["taxtotal"] = filter_input(INPUT_POST, "sub_total");
    $salesorderdetails["ship_charge"] = filter_input(INPUT_POST, "ship_charge");

    $salesorderdetails["discount"] = filter_input(INPUT_POST, "discount");
    $salesorderdetails["discountvalue"] = filter_input(INPUT_POST, "discountvalue");

    $salesorderdetails["total"] = filter_input(INPUT_POST, "total");
    $salesorderdetails["representative"] = filter_input(INPUT_POST, "representative");

    $salesorderdetails["sono"] = filter_input(INPUT_POST, "sono");
    if (filter_input(INPUT_GET, "salesorderid") == "") {
        $salesorderdetails["customer_id"] = $customerid = filter_input(INPUT_POST, "customer_id");
    }
    $shipping_address = filter_input(INPUT_POST, "shipping_address");
    $billTo_address = filter_input(INPUT_POST, "billTo_address");

    $salesorderdetails["shipping_address"] = MysqlConnection::formatToBRAddress($shipping_address);
    $salesorderdetails["billTo_address"] = MysqlConnection::formatToBRAddress($billTo_address);


    $salesorderdetails["remark"] = filter_input(INPUT_POST, "remark");
    $salesorderdetails["shipvia"] = filter_input(INPUT_POST, "shipvia");

    $salesorderdetails["isOpen"] = "Y";
    $salesorderdetails["expected_date"] = $expected_date = filter_input(INPUT_POST, "expected_date");
    if ($expected_date == "") {
        $salesorderdetails["expected_date"] = filter_input(INPUT_POST, "hidexpected_date");
    } else {
        $salesorderdetails["expected_date"] = MysqlConnection::convertToDBDate($salesorderdetails["expected_date"]);
    }
    $salesorderdetails["soorderdate"] = date("Y-m-d");
    $salesorderdetails["added_by"] = $_SESSION["user"]["user_id"];
    $salesorderdetails["deleteNote"] = "";
    $salesorderid = filter_input(INPUT_GET, "salesorderid");

    if (isset($btnSaveSalesOrder) || isset($btnSaveNextSalesOrder) || isset($printButton) || isset($mailButton) || isset($invoiceButton)) {
        if (isset($salesorderid) && $salesorderid != "") {
            MysqlConnection::edit("sales_order", $salesorderdetails, " id = '$salesorderid' ");
            MysqlConnection::delete("DELETE FROM `sales_item` WHERE `so_id` = '$salesorderid' ");
        } else {
            $salesorderid = MysqlConnection::insert("sales_order", $salesorderdetails);
        }
        saveSalesOrderItems($salesorderid, $invoiceno);
        //include 'pdflib/salesorder.php';
        salesInvoice($salesorderid, $invoiceno);
        quickSalesInvoice($salesorderid, $invoiceno);
    }

    if ($btnSaveSalesOrder == "SAVE") {
        header("location:index.php?pagename=view_retailinvoice");
    } if ($btnSaveNextSalesOrder == "SAVE AND NEXT") {
        header("location:index.php?pagename=manualrestock_salesorder");
    }if ($printButton == "PRINT") {
        //invoice/print_creditnote.php?invoiceno=1546862442
        //header("location:index.php?pagename=manage_salesorder&action=print&salesorderid=$salesorderid&from=cn&invoiceno=$invoiceno");
        header("location:invoice/print_creditnote.php?invoiceno=$invoiceno'");
//        echo "<script language='javascript'>window.open('invoice/print_creditnote.php?invoiceno=$invoiceno');</script>";
    }if ($invoiceButton == "CREATE INVOICE") {
        header("location:index.php?pagename=manage_salesorder&action=invoice&salesorderid=$salesorderid");
    } else if ($mailButton == "EMAIL") {
        header("location:index.php?pagename=email_salesorder&salesorderid=$salesorderid");
    }
}

function salesInvoice($salesorderid = "", $invoice) {
    include 'pdflib/salesinvoice.php';
}

function quickSalesInvoice($salesorderid, $invoiceno) {
    include 'pdflib/creditnote.php';
}

function saveSalesOrderItems($salesorderid, $invoiceno) {

    $itemsarray = filter_input_array(INPUT_POST);
    $items = $itemsarray["items"];
    $pricearray = $itemsarray["price"];
    $itemcount = $itemsarray["itemcount"];

    for ($index = 0; $index <= count($items); $index++) {

        $itemname = explode("__", $items[$index]);
        $itemcode = trim($itemname[0]);
        $count = $itemcount[$index];
        $price = $pricearray[$index];

        if ($count != 0) {
            $itemsfromcode = MysqlConnection::fetchCustom("SELECT item_id,onhand,totalvalue FROM `item_master` WHERE item_code = '$itemcode' ");
            $arraysalesitems = array();

            $item_id = $itemsfromcode[0]["item_id"];
            $arraysalesitems["item_id"] = $item_id;
            $arraysalesitems["so_id"] = $salesorderid;
            $arraysalesitems["qty"] = $count;
            $arraysalesitems["rQty"] = $count;
            $arraysalesitems["restockQty"] = $count;
            $arraysalesitems["backQty"] = 0;
            $arraysalesitems["price"] = $price;
            MysqlConnection::insert("sales_item", $arraysalesitems);

            $arraysellinghist["invoice"] = $invoiceno;
            $arraysellinghist["so_id"] = $salesorderid;
            $arraysellinghist["qty"] = $count;
            $arraysellinghist["item_id"] = $item_id;
            $arraysellinghist["date"] = date("Y-m-d");
            $arraysellinghist["price"] = $price;
            unset($arraysellinghist["currentstock"]);
            MysqlConnection::insert("tbl_selling_history", $arraysellinghist);

            // update re-stock price
            $arrayrestock = array();
            $arrayrestock["reinvoice"] = $invoiceno;
            $arrayrestock["invoice"] = $invoiceno;
            $arrayrestock["qty"] = $count;
            $arrayrestock["so_id"] = $salesorderid;
            $arrayrestock["item_id"] = $item_id;
            $arrayrestock["date"] = date("Y-m-d");
            $arrayrestock["price"] = $price;
            MysqlConnection::insert("tbl_restockinvoice_history", $arrayrestock);

            MysqlConnection::updateOnSaleItem($item_id, $count, "Y");
        }
    }
}
