<?php
error_reporting(0);
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY taxtype ASC ;");

$salesorderid = filter_input(INPUT_GET, "salesorderid");
$explode = explode("_", $salesorderid);

$salesorderidget = $explode[0];
$invoicenoget = $explode[1];



$invoiceno = "";
if (isset($salesorderidget)) {
    $soorderdetails = MysqlConnection::getSalesOrderDetailsById($salesorderidget);

    $invoiceno = $invoicenoget;
    $isBackOrder = $soorderdetails["isBackOrder"];

    if ($soorderdetails["isOpen"] == "N") {
        //header("location:index.php?pagename=manage_salesorder&status=Y&action=closederror");
    }

    $soitemsdetails = MysqlConnection::fetchCustom("SELECT * FROM `tbl_selling_history` WHERE `invoice` LIKE '$invoicenoget'");
    $sonumber = $soorderdetails["sono"];
    $soCustomer = MysqlConnection::getCustomerDetails($soorderdetails["customer_id"]);
    $representivename = MysqlConnection::getGenericById($soCustomer["sales_person_name"]);
}

saveOrEditSalesOrder($isBackOrder, $invoiceno, $salesorderidget);

$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");

$_SESSION["navigationpage"] = "index.php?pagename=create_salesorder";
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/restocksalesorderjs.js"></script>
<script src="js/script.js"></script>
<script>
    function saleOrderRowDelete(rowid) {
        var row = document.getElementById(rowid);
        doCalculation();
        row.parentNode.removeChild(row);
    }

    $("select#staxname").change(function () {
        doCalculation();
    });
</script>
<style>
    input,textarea,date{ width: 80%;height: 30px; }
    select{ width: 81.4%;height: 30px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">RETURN GOODS</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="table table-bordered display nowrap sortable" style="width: 100%">
                            <tr >
                                <td><b>SO NUMBER</b></td>
                                <td style="background-color: white"><?php echo $sonumber ?></td>
                                <td><b>SALES DATE</b></td>
                                <td style="background-color: white"><?php echo MysqlConnection::convertToPreferenceDate(date("Y-m-d")) ?></td>
                                <td><b>SALES PERSON </b></td>
                                <td style="background-color: white"><?php echo $representivename["name"] . " " . $representivename["description"] ?></td>
                            </tr>
                            <tr>
                                <td style="width: 10%">CUSTOMER NAME</td>
                                <td style="background-color: white"><?php echo $soCustomer["cust_companyname"] ?></td>
                                <td style="width: 10%">SHIP VIA</td>
                                <td style="background-color: white"><?php echo $soorderdetails["shipvia"] ?></td>
                                <td style="width: 10%">EXPECTED&nbsp;DELIVERY</td>
                                <td style="background-color: white"><?php echo MysqlConnection::convertToPreferenceDate($soorderdetails["expected_date"]) ?></td>
                            </tr>
                            <tr>
                                <td >BILLING&nbsp;ADDRESS</td>
                                <td style="background-color: white"><p style="width: 250px;text-align: justify"><?php echo $soorderdetails["billTo_address"] ?></p></td>
                                <td>SHIPPING&nbsp;ADDRESS</td>
                                <td style="background-color: white"><p style="width: 250px;text-align: justify"><?php echo $soorderdetails["shipping_address"] ?></p></td>
                                <td >REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td style="background-color: white"><textarea  style="line-height: 18px; color: red ; width: 220px;height: 72px;resize: none" name="remark"><?php echo $soorderdetails["remark"] ?></textarea></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="table table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                            <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white;">
                                <td style="width: 30%">ITEM CODE / DESCRIPTION</td>
                                <td style="width: 10%;">UNIT</td>
                                <td style="width: 10%;">PRICE</td>
                                <td style="width: 10%;">SHIPPED.QTY</td>
                                <td style="width: 10%;">RETURN.QTY</td>
                                <td style="width: 20%;" colspan="2">AMOUNT</td>
                            </tr>
                            <?php
                            $indexedit = 1;
                            $isexhausted = 0;
                            foreach ($soitemsdetails as $soitem) {
                                $itemid = $soitem["item_id"];
                                $invoice = $soitem["invoice"];
                                $soid = $soitem["so_id"];
                                $value = MysqlConnection::getItemDataById($soitem["item_id"]);
                                $returnqty = getOldReturnQty($invoice, $soid, $itemid);
                                $isexhausted = $isexhausted + ($soitem["qty"] - $returnqty);
                                ?>
                                <tr id="<?php echo $indexedit ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                    <td style="width: 500px"><?php echo $value["item_code"] . " " . $value["item_desc_purch"] ?></td>
                                    <td style="width: 70px;"><div id="unit<?php echo $indexedit ?>"><?php echo $value["unit"] ?></div></td>
                                    <td style="width: 70px;">
                                        <div id="price<?php echo $indexedit ?>"><?php echo $soitem["price"] ?></div>
                                        <input type="hidden" name="priceori[]"  value="<?php echo $soitem["price"] ?>">
                                    </td>
                                    <td style="width: 100px;">
                                        <input type="text"  value="<?php echo $soitem["qty"] - $returnqty ?>"
                                               readonly="" id="shifted<?php echo $indexedit ?>"
                                               style="padding: 0px;margin: 0px;width: 100%;text-align: center">
                                    </td>
                                    <td style="width: 150px;">
                                        <input type="hidden" name="itemid[]"  id="itemid[]" value="<?php echo $itemid ?>">
                                        <input type="text" name="returncount[]" required="" maxlength="6"
                                               onkeypress="return chkNumericKey(event)"
                                               onkeyup="checkReturnQty('<?php echo $indexedit ?>')"
                                               id="returncount<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%;text-align: center">
                                    </td>
                                    <td colspan="2">
                                        <div id="total<?php echo $indexedit ?>"><?php echo number_format(($soitem["returnQty"] * $soitem["price"]), 2) ?></div>
                                    </td>
                                </tr>
                                <?php
                                $indexedit++;
                            }
                            ?>
                            <tr >
                                <td colspan="4" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td ><b>Goods Total</b></td>
                                <td colspan="2">
                                    <input type="text" id="itemtotal" name="itemtotal" style="width: 98%">
                                </td>
                            </tr>


                            <tr >
                                <td colspan="4" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td ><b>Discount</b></td>
                                <td colspan="2">
                                    <input type="text" id="discount"  name="discount"  maxlength="3" onfocusout="doCalculation()" name="discount" style="width: 98%">
                                </td>
                            </tr>
                            <tr >
                                <td colspan="4" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Goods Tax</b></td>
                                <td>
                                    <select  name="taxid" id="taxid" required="" onchange="doCalculation()" style="height: 25px;width: 98%">
                                        <option value="">SELECT</option>
                                        <?php
                                        foreach ($sqltaxinfodata as $key => $value) {
                                            if ($soorderdetails["taxid"] == $value["id"]) {
                                                $selected = "selected";
                                            } else {
                                                $selected = "";
                                            }
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $value["id"] ?>">
                                                <?php echo ucwords($value["taxtype"]) ?>
                                                <?php echo $value["taxname"] ?>-<?php echo $value["taxvalues"] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td> 
                                <td>
                                    <input type="text" id="taxamount" readonly="" name='taxamount' style="width: 96%" >
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Sub Total</b></td>
                                <td colspan="2"><input type="text" id="finaltotal" style="width: 98%"  onkeypress="return chkNumericKey(event)" name="sub_total" readonly=""></td>
                            </tr>
                            <tr >
                                <td colspan="4" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Restocking Charges(%)</b></td>
                                <td>
                                    <input type="text" id="restockpercent"   name='restockpercent' onkeyup="doCalculation()" style="width: 94%" >
                                </td> 
                                <td>
                                    <input type="text" id="restockamount" readonly=""  name='restockamount' style="width: 96%" >
                                </td>
                            </tr>
                            <tr >
                                <td colspan="4" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Net Total</b></td>
                                <td colspan="2">
                                    <input type="text" id="nettotal" readonly=""   name="nettotal" style="width: 98%" >
                                    <input type="hidden" value="<?php echo MysqlConnection::getAddedBy($_SESSION["user"]["user_id"]); ?>" readonly="" >
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>

            <div class="modal-footer "> 
                <center>
                    <?php
                    if ($isexhausted != 0) {
                        ?>
                        <input type="submit" id="btnSaveSalesOrder" name="btnSaveSalesOrder" class="btn btn-info" value="SAVE">
                        <a href="index.php?pagename=view_retailinvoice" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                        <?php
                    } else {
                        ?>
                        <h4 style="color: red">Sorry you do not have item to return !!</h4>
                        <?php
                    }
                    ?>
                </center>
            </div>
            <input type="hidden" name="onhand" id="onhand">
        </div>
    </div>

</form>

<?php

function saveOrEditSalesOrder($isBackOrder, $invoiceno, $salesorderid) {
    $btnSaveSalesOrder = filter_input(INPUT_POST, "btnSaveSalesOrder");
    $btnSaveNextSalesOrder = filter_input(INPUT_POST, "btnSaveNextSalesOrder");
    $printButton = filter_input(INPUT_POST, "printButton");
    $mailButton = filter_input(INPUT_POST, "mailButton");

    $salesorderdetails["sub_total"] = filter_input(INPUT_POST, "itemtotal");

    $salesorderdetails["taxid"] = $taxid = filter_input(INPUT_POST, "taxid");
    $taxdetails = MysqlConnection::getTaxInfoById($taxid);
    $salesorderdetails["taxname"] = $taxdetails["taxname"];
    $salesorderdetails["taxValue"] = $taxdetails["taxvalues"];

    $salesorderdetails["taxamount"] = filter_input(INPUT_POST, "taxamount");

    $salesorderdetails["staxname"] = filter_input(INPUT_POST, "staxname");
    $salesorderdetails["staxpercent"] = filter_input(INPUT_POST, "staxpercent");

    $salesorderdetails["restockpercent"] = filter_input(INPUT_POST, "restockpercent");
    $salesorderdetails["restockamount"] = filter_input(INPUT_POST, "restockamount");

    $salesorderdetails["taxtotal"] = filter_input(INPUT_POST, "sub_total");
    $salesorderdetails["ship_charge"] = filter_input(INPUT_POST, "ship_charge");
    $salesorderdetails["discount"] = filter_input(INPUT_POST, "discount");
    $salesorderdetails["total"] = filter_input(INPUT_POST, "nettotal");

    $salesorderdetails["remark"] = filter_input(INPUT_POST, "remark");
    $salesorderdetails["added_by"] = $_SESSION["user"]["user_id"];


    $postearray = filter_input_array(INPUT_POST);

    $reinvoice = time();

    updateItems($postearray, $invoiceno, $reinvoice, $salesorderid);
//
    if (isset($btnSaveSalesOrder) || isset($btnSaveNextSalesOrder) || isset($printButton) || isset($mailButton)) {
        if (isset($salesorderid) && $salesorderid != "") {
            MysqlConnection::edit("sales_order", $salesorderdetails, " id = '$salesorderid' ");
            MysqlConnection::delete("UPDATE `sales_order` SET `isRestock` = 'Y' WHERE `sales_order`.`id` = '$salesorderid'");
        }
        quickSalesInvoice($salesorderid, $reinvoice);
    }
    if ($btnSaveSalesOrder == "SAVE") {
        header("location:index.php?pagename=view_retailinvoice&action=print&invoiceno=$reinvoice&salesorderid=$salesorderid");
    }
}

function updateItems($postearray, $invoiceno, $reinvoice, $salesid) {
    $count = count($postearray["returncount"]);

    for ($parr = 0; $parr < $count; $parr++) {
        $itemid = $postearray["itemid"][$parr];
        $returnqty = $postearray["returncount"][$parr] + getItemsReturnPrevQty($salesid, $itemid);
        echo $returnsalesorder = "UPDATE sales_item SET restockQty = $returnqty WHERE so_id = '$salesid' AND item_id='$itemid' ";
        MysqlConnection::executeQuery($returnsalesorder);
        updateInventory($itemid, $returnqty);

        // update re-stock price
        $arrayrestock = array();
        $arrayrestock["reinvoice"] = $reinvoice;
        $arrayrestock["invoice"] = $invoiceno;
        $arrayrestock["qty"] = $postearray["returncount"][$parr];
        $arrayrestock["price"] = $postearray["priceori"][$parr];
        $arrayrestock["so_id"] = $salesid;
        $arrayrestock["item_id"] = $itemid;
        $arrayrestock["date"] = date("Y-m-d");
        MysqlConnection::insert("tbl_restockinvoice_history", $arrayrestock);
    }
}

function getItemsReturnPrevQty($salesid, $itemid) {
    $query = "SELECT returnQty FROM sales_item WHERE  so_id = '$salesid' AND item_id='$itemid' ";
    $resultset = MysqlConnection::fetchCustom($query);
    if (count($resultset) != 0) {
        return $resultset[0]["returnQty"];
    }
    return 0;
}

function quickSalesInvoice($salesorderid, $invoiceno) {
    include 'pdflib/creditnote.php';
}

function updateInventory($itemid, $returnqty) {
    $itemdata = MysqlConnection::getItemDataById($itemid);
    $returnupdate = $itemdata["onhand"] + $returnqty;
    $updateInventory = "UPDATE item_master SET onhand = '$returnupdate' WHERE item_id = '$itemid' ";
    MysqlConnection::executeQuery($updateInventory);
}

function getOldReturnQty($invoice, $so_id, $item_id) {
    $query = "SELECT sum( `qty`) as qty"
            . " FROM `tbl_restockinvoice_history`"
            . " WHERE `invoice` = '$invoice'"
            . " AND `so_id` = '$so_id'"
            . " AND `item_id` = '$item_id'";
    $result = MysqlConnection::fetchCustom($query);
    return $result[0]["qty"];
}
