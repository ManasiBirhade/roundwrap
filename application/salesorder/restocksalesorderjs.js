function setDetails(count) {
    var item_code = $("#tags" + count).val();
    var dataString = "item_code=" + item_code;
    $.ajax({
        type: 'POST',
        url: 'itemmaster/getitemajax.php',
        data: dataString
    }).done(function(data) {
        if (data !== null && data !== "") {
            var jsonobj = JSON.parse(data);
            if (jsonobj !== null) {
                $("#unit" + count).text(jsonobj.unit);
                if (parseFloat(jsonobj.total_sales_rate) === parseFloat("0.0")) {
                    $("#price" + count).text(jsonobj.sell_rate);
                    $("#tempItemPrice").text(jsonobj.sell_rate);
                } else {
                    $("#price" + count).text(jsonobj.sell_rate);
                    $("#tempItemPrice").text(jsonobj.sell_rate);
                }
                $("#onhand" + count).text(jsonobj.onhand);
                $("#totalvalue" + count).text(jsonobj.totalvalue);
                //doCalculation();
            }
        } else {
            $("#tags" + count).val("");
        }
    }).fail(function() {
        $("#tags" + count).val("");
    });
}

function checkQty(count) {
    if (count !== undefined && count !== "") {
        var amount = $("#amount" + count).val();
        var onhand = $("#onhand" + count).text();
        var amountcl = parseInt(amount);
        var onhandcl = parseInt(onhand);
        if (onhandcl < amountcl) {
            $('#alertmodel').modal();
        }
    }
}


function checkReturnQty(count) {
    if (count !== undefined && count !== "") {
        var amount = $("#shifted" + count).val();
        var onhand = $("#returncount" + count).val();
        var amountcl = parseInt(amount);
        var onhandcl = parseInt(onhand);
        if (onhandcl > amountcl) {
            $('#returncount' + count).val("");
            $('#returncount' + count).focus();
        }
    }
    doCalculation();
}


function clearValue(count) {
    $("#desc" + count).text("");
    $("#unit" + count).text("");
    $("#price" + count).text("");
    $("#tags" + count).val("");
    $("#total" + count).text("");
    $("#onhand" + count).text("");
    $("#totalvalue" + count).text(jsonobj.totalvalue);
    finalTotal();
}

function doCalculation(count) {

    var ship_charge = $("#ship_charge").val();
    if (ship_charge === undefined || ship_charge === "") {
        ship_charge = "0.0";
    }

    var discount = $("#discount").val();
    if (discount === undefined || discount === "") {
        discount = "0.0";
    }

    var restockpercent = $("#restockpercent").val();
    if (restockpercent === undefined || restockpercent === "") {
        restockpercent = "0.0";
    }

    var totalOfItems = 0.0;
    var restockperprice = 0.0;
    for (var index = 1; index <= 30; index++) {
        var price = $("#price" + index).text();
        var amount = $("#returncount" + index).val();
        //var returncount = $("#returncount" + index).val();
        //if (returncount === "" || returncount === undefined || returncount === 0) {
        //returncount = 0;
        //}
        if (price !== "" && amount !== "") {
            var t = parseFloat(price) * parseFloat(amount);
            $("#total" + index).text(((Math.round(t * 100) / 100)).toFixed(2));
            totalOfItems = parseFloat(totalOfItems) + t;
        }
    }
    $("#itemtotal").val(totalOfItems.toFixed(2));

    restockperprice = ((parseFloat(totalOfItems) * parseFloat(restockpercent) / 100)).toFixed(2);
    $("#restockamount").val(restockperprice);

    var taxname = $("#taxid option:selected").text();
    if (taxname !== undefined && taxname !== "") {
        var goodstotal = $("#itemtotal").val();
        var splitselect = taxname.split("-");
        if (splitselect.length === 2) {
            if (goodstotal !== undefined && goodstotal !== "" && splitselect !== undefined && splitselect !== "") {
                var taxAmount = ((parseFloat(goodstotal) - parseFloat(discount)) * parseFloat(splitselect[1]) / 100);
                $("#taxamount").val(taxAmount.toFixed(2));
            }
        } else {
            $("#taxamount").val("0.0");
        }
    }

    var taxamount = $("#taxamount").val();
    if (taxamount === "" || taxamount === undefined) {
        taxamount = 0.0;
    }

    var subTotalValue = 0.0;
    subTotalValue = (parseFloat(totalOfItems) - parseFloat(discount) + parseFloat(taxamount)).toFixed(2);
    $("#finaltotal").val(subTotalValue);


    var staxname = $("#staxname").val();
    var staxamount = 0.0;
    if (staxname === "GST-5") {
        var ship_charge = $("#ship_charge").val();
        if (ship_charge !== undefined && ship_charge !== "" && ship_charge !== "0") {
            staxamount = 0.05 * parseFloat(ship_charge);
        } else {
            staxamount = 0.0;
        }
        $("#staxpercent").val(staxamount.toFixed(2));
    } else {
        $("#staxpercent").val(staxamount.toFixed(2));
    }

    if (taxAmount === undefined) {
        taxAmount = 0.0;
    }

    $("#nettotal").val((parseFloat(subTotalValue) + parseFloat(staxamount) + parseFloat(ship_charge) - parseFloat(restockperprice)).toFixed(2));

    checkQty(count);

    //calculateCharges();
}


function searchCustomer() {
    var companyname = $("#companyname").val();
    if (companyname !== "") {
        var dataString = "companyname=" + companyname;
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function(data) {
            var jsonobj = JSON.parse(data);
            $("#customer_id").val(jsonobj.id);
            $("#billTo_address").val(jsonobj.billto);
            $("#shipping_address").val(jsonobj.shipto);
            $("#taxname").val(jsonobj.taxname);
            $("#taxvalue").val(jsonobj.taxValue);
            $("#representative").val(jsonobj.sales_person_name);
            if (jsonobj.length === 0) {
                $("#error").text("Customer not available!!!");
            } else {
                $("#error").text("");
            }
        }).fail(function() {
        });
    }
}


function actionCalculation() {

    var tax = $("#taxamount").val();
    var taxAmount = "0.0";
    if (tax !== undefined || tax !== "") {
        taxAmount = $("#taxamount").val();
    }

    var staxAmount = $("#staxpercent").val();
    if (staxAmount === undefined || staxAmount === "") {
        staxAmount = "0.0";
    }

    var ship_charge = $("#ship_charge").val();
    if (ship_charge === undefined || ship_charge === "") {
        ship_charge = "0.0";
    }

    var discount = $("#discount").val();
    if (discount === undefined || discount === "") {
        discount = "0.0";
    }

    var totalOfItems = 0.0;
    for (var index = 1; index <= 30; index++) {
        var price = $("#price" + index).text();
        var amount = $("#amount" + index).val();
        var returncount = $("#returncount" + index).val();
        if (returncount === "" || returncount === undefined || returncount === 0) {
            returncount = 0;
        }
        if (price !== "" && amount !== "") {
            var t = parseFloat(price) * (parseFloat(amount) - parseFloat(returncount));
            $("#total" + index).text((Math.round(t * 100) / 100));
            totalOfItems = parseFloat(totalOfItems) + t;
        }
    }
    $("#itemtotal").val(totalOfItems.toFixed(2));
    //var taxAmount = (parseFloat(taxValue) / 100) * parseFloat(totalOfItems);
    if (taxAmount === undefined) {
        taxAmount = 0.0;
    }
    $("#finaltotal").val((parseFloat(totalOfItems) + parseFloat(taxAmount)).toFixed(2));

    $("#nettotal").val(((parseFloat(totalOfItems) - parseFloat(discount) + parseFloat(ship_charge) + parseFloat(taxAmount)) + parseFloat(staxAmount)).toFixed(2));

}


function searchCustomerModel(companyname) {
    if (companyname !== "") {
        var dataString = "companyname=" + companyname;
        $("div#divLoading").addClass('show');
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomerid_ajax.php',
            data: dataString
        }).done(function(data) {
            var jsonobj = JSON.parse(data);
            $("#customer_id").val(jsonobj.id);
            $("#companyname").val(jsonobj.cust_companyname);
            $("#billTo_address").val(jsonobj.billto);
            $("#shipping_address").val(jsonobj.shipto);
            $("#representative").val(jsonobj.sales_person_name);
            $("#taxname").val(jsonobj.tax);
            $("#taxvalue").val(jsonobj.taxvalues);
            if (jsonobj.length === 0) {
                $("#error").text("Customer not available!!!");
            } else {
                $("#error").text("");
            }
        }).fail(function() {
        });
        $("div#divLoading").removeClass('show');
        $("#shipvia").focus();
        $("#datepicker").focus();
        $('#customerModel').modal('hide');
    }
}

function calculateCharges() {
    var restockpercent = $("#restockpercent").val();
    if (restockpercent === undefined || restockpercent === "") {
        restockpercent = "0.0";
    }

    var goodstotal = $("#itemtotal").val();
    if (goodstotal === undefined || goodstotal === "") {
        goodstotal = "0.0";
    }

    var nettotal = $("#nettotal").val();
    if (nettotal === undefined || nettotal === "") {
        nettotal = "0.0";
    }

    var restockperprice = ((parseFloat(goodstotal) * parseFloat(restockpercent) / 100)).toFixed(2);

    $("#restockamount").val(restockperprice);

    $("#nettotal").val((parseFloat(nettotal) - parseFloat(restockperprice)).toFixed(2));

}




function checkLessValue(id) {
    var tempItemPrice = parseFloat($("#tempItemPrice").val());
    var newPrice = parseFloat($("#" + id).val());
    if (tempItemPrice > newPrice) {
        alert("Price - Less by " + (tempItemPrice - newPrice).toFixed(2));
        $("#" + id).val("");
        $("#" + id).focus();
    }

}