<?php
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY taxtype ASC ;");
$salesorderidget = filter_input(INPUT_GET, "salesorderid");
if (isset($salesorderidget)) {
    $soorderdetails = MysqlConnection::getSalesOrderDetailsById($salesorderidget);

    $isBackOrder = $soorderdetails["isBackOrder"];

    if ($soorderdetails["isOpen"] == "N") {
        //header("location:index.php?pagename=manage_salesorder&status=Y&action=closederror");
    }

    $soitemsdetails = MysqlConnection::getSalesItemsDetailsById($salesorderidget);
    $sonumber = $soorderdetails["sono"];
    $soCustomer = MysqlConnection::getCustomerDetails($soorderdetails["customer_id"]);
    $representivename = MysqlConnection::getGenericById($soCustomer["sales_person_name"]);
}

saveOrEditSalesOrder($isBackOrder);

$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");

$_SESSION["navigationpage"] = "index.php?pagename=create_salesorder";
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="salesorder/salesorderjs.js"></script>
<script src="js/script.js"></script>
<script>
    function saleOrderRowDelete(rowid) {
        var row = document.getElementById(rowid);
        doCalculation();
        row.parentNode.removeChild(row);
    }

    $("select#staxname").change(function() {
        doCalculation();
    });
</script>
<style>
    input,textarea,date{ width: 80%;height: 30px; }
    select{ width: 81.4%;height: 30px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">RETURN GOODS</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="table table-bordered display nowrap sortable" style="width: 100%">
                            <tr >
                                <td><b>SO NUMBER</b></td>
                                <td style="background-color: white"><?php echo $sonumber ?></td>
                                <td><b>SALES DATE</b></td>
                                <td style="background-color: white"><?php echo MysqlConnection::convertToPreferenceDate(date("Y-m-d")) ?></td>
                                <td><b>SALES PERSON </b></td>
                                <td style="background-color: white"><?php echo $representivename["name"] . " " . $representivename["description"] ?></td>
                            </tr>
                            <tr>
                                <td style="width: 10%">CUSTOMER NAME</td>
                                <td style="background-color: white"><?php echo $soCustomer["cust_companyname"] ?></td>
                                <td style="width: 10%">SHIP VIA</td>
                                <td style="background-color: white"><?php echo $soorderdetails["shipvia"] ?></td>
                                <td style="width: 10%">EXPECTED&nbsp;DELIVERY</td>
                                <td style="background-color: white"><?php echo MysqlConnection::convertToPreferenceDate($soorderdetails["expected_date"]) ?></td>
                            </tr>
                            <tr>
                                <td >BILLING&nbsp;ADDRESS</td>
                                <td style="background-color: white"><p style="width: 250px;text-align: justify"><?php echo $soorderdetails["billTo_address"] ?></p></td>
                                <td>SHIPPING&nbsp;ADDRESS</td>
                                <td style="background-color: white"><p style="width: 250px;text-align: justify"><?php echo $soorderdetails["shipping_address"] ?></p></td>
                                <td >REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td style="background-color: white"><textarea  style="line-height: 18px; color: red ; width: 220px;height: 72px;resize: none" name="remark"><?php echo $soorderdetails["remark"] ?></textarea></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                            <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white;">
                                <td style="width: 25px;">#</td>
                                <td style="width: 500px;">ITEM CODE / DESCRIPTION</td>
                                <td style="width: 70px;">UNIT</td>
                                <td style="width: 70px;">PRICE</td>
                                <td style="width: 100px;">ORDERED.QTY</td>
                                <td style="width: 100px;">SHIPPED.QTY</td>
                                <td style="width: 150px;">RETURN.QTY</td>
                                <td>AMOUNT</td>
                            </tr>
                        </table>
                        <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                            <?php
                            $indexedit = 1;
                            foreach ($soitemsdetails as $soitem) {
                                $itemid = $soitem["item_id"];
                                $value = MysqlConnection::getItemDataById($soitem["item_id"]);
                                ?>
                                <tr id="<?php echo $indexedit ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                    <td style="width: 25px"><a class="icon  icon-remove" onclick="saleOrderRowDelete('<?php echo $indexedit ?>')"></a></td>
                                    <td style="width: 500px"><?php echo $value["item_code"] . " " . $value["item_desc_purch"] ?></td>
                                    <td style="width: 70px;"><div id="unit<?php echo $indexedit ?>"><?php echo $value["unit"] ?></div></td>
                                    <td style="width: 70px;"><div id="price<?php echo $indexedit ?>"><?php echo $value["sell_rate"] ?></div></td>
                                    <td style="width: 100px;">
                                        <input type="text" name="itemcount[]" value="<?php echo $soitem["qty"] - $soitem["returnQty"] ?>"readonly="" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="doCalculation()" id="amount<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%;text-align: center">
                                    </td>
                                    <td style="width: 100px;">
                                        <input type="text"  value="<?php echo $soitem["rQty"] - $soitem["returnQty"] ?>" readonly="" id="shifted<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%;text-align: center">
                                    </td>
                                    <td style="width: 150px;">
                                        <input type="hidden" name="itemid[]"  id="itemid[]" value="<?php echo $itemid ?>">
                                        <input type="text" name="returncount[]" required="" maxlength="6" onkeypress="return chkNumericKey(event)" onfocusout="checkReturnQty('<?php echo $indexedit ?>')" id="returncount<?php echo $indexedit ?>" style="padding: 0px;margin: 0px;width: 100%;text-align: center">
                                    </td>
                                    <td ><div id="total<?php echo $indexedit ?>"><?php echo ($soitem["qty"] - $soitem["returnQty"]) * $value["sell_rate"] ?></div></td>
                                </tr>
                                <?php
                                $indexedit++;
                            }
                            ?>


                            <tr >
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td ><b>Goods Total</b></td>
                                <td><input type="text" id="itemtotal" name="itemtotal" value="<?php echo $soorderdetails["sub_total"] ?>"></td>
                            </tr>


                            <tr >
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td ><b>Discount</b></td>
                                <td><input type="text" id="discount"  value="<?php echo $soorderdetails["discount"] ?>" name="discount"  maxlength="3" onfocusout="doCalculation()" name="discount" ></td>
                            </tr>
                            <tr >
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Goods Tax</b></td>
                                <td><select  name="taxid" id="taxid" required="" onchange="doCalculation()" style="height: 25px;width: 52%">
                                        <option value="">SELECT</option>
                                        <?php
                                        foreach ($sqltaxinfodata as $key => $value) {
                                            if ($soorderdetails["taxid"] == $value["id"]) {
                                                $selected = "selected";
                                            } else {
                                                $selected = "";
                                            }
                                            ?>
                                            <option <?php echo $selected ?> value="<?php echo $value["id"] ?>">
                                                <?php echo ucwords($value["taxtype"]) ?>
                                                <?php echo $value["taxname"] ?>-<?php echo $value["taxvalues"] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <input type="text" id="taxamount" readonly="" value="<?php echo $soorderdetails["taxamount"] ?>" name='taxamount' style="width: 27%" >
                                </td> 
                            </tr>
                            <tr>
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Sub Total</b></td>
                                <td><input type="text" id="finaltotal"  value="<?php echo $soorderdetails["taxtotal"] ?>" onkeypress="return chkNumericKey(event)" name="sub_total" readonly=""></td>
                            </tr>
                            <tr >
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Shipping Charges</b></td>
                                <td><input type="text" id="ship_charge" onfocusout="doCalculation()"  value="<?php echo $soorderdetails["ship_charge"] == "" ? "0" : $soorderdetails["ship_charge"] ?>" maxlength="6"  onkeypress="return chkNumericKey(event)" name="ship_charge" value="<?php echo $customer["ship_charge"] ?>"  ></td>
                            </tr>
                            <tr >
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Ship Tax</b></td>
                                <td>
                                    <select id="staxname" name='staxname' style="height: 25px;width: 50%" >
                                        <option value="">SELECT</option>
                                        <option value="GST-5" <?php echo $soorderdetails["staxname"] == "GST-5" ? "selected" : "" ?> >GST-5</option>
                                    </select>
                                    <input type="text" id="staxpercent" readonly=""  value="<?php echo $soorderdetails["staxpercent"] ?>" name='staxpercent'  onkeypress="return chkNumericKey(event)" style="width: 27%" >
                                </td>
                            </tr>
                            <tr >
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Restocking Charges(%)</b></td>
                                <td>
                                    <input type="text" id="restockpercent"  value="<?php echo $soorderdetails["restockpercent"] ?>" name='restockpercent' onfocusout="calculateCharges()" style="width: 35%" >
                                    <input type="text" id="restockamount" readonly="" value="<?php echo $soorderdetails["restockamount"] ?>" name='restockamount' style="width: 40%" >
                                </td> 
                            </tr>
                            <tr >
                                <td colspan="6" style="border: solid 1px #F9F9F9;border-top: solid 1px #CDCDCD;border-right: solid 1px #CDCDCD"></td>
                                <td><b>Net Total</b></td>
                                <td>
                                    <input type="text" id="total" readonly="" value="<?php echo $soorderdetails["total"] ?>"  name="total" name="total" >
                                    <input type="hidden" value="<?php echo MysqlConnection::getAddedBy($_SESSION["user"]["user_id"]); ?>" readonly="" >
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>
                    <input type="submit" id="btnSaveSalesOrder" name="btnSaveSalesOrder" class="btn btn-info" value="SAVE">
                    <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                </center>
            </div>
            <input type="hidden" name="onhand" id="onhand">
        </div>
    </div>

</form>

<?php

function saveOrEditSalesOrder($isBackOrder) {
    $btnSaveSalesOrder = filter_input(INPUT_POST, "btnSaveSalesOrder");
    $btnSaveNextSalesOrder = filter_input(INPUT_POST, "btnSaveNextSalesOrder");
    $printButton = filter_input(INPUT_POST, "printButton");
    $mailButton = filter_input(INPUT_POST, "mailButton");

    $salesorderdetails["sub_total"] = filter_input(INPUT_POST, "itemtotal");

    $salesorderdetails["taxname"] = filter_input(INPUT_POST, "taxname");
    $salesorderdetails["taxValue"] = filter_input(INPUT_POST, "taxValue");
    $salesorderdetails["taxamount"] = filter_input(INPUT_POST, "taxamount");

    $salesorderdetails["staxname"] = filter_input(INPUT_POST, "staxname");
    $salesorderdetails["staxpercent"] = filter_input(INPUT_POST, "staxpercent");

    $salesorderdetails["restockpercent"] = filter_input(INPUT_POST, "restockpercent");
    $salesorderdetails["restockamount"] = filter_input(INPUT_POST, "restockamount");

    $salesorderdetails["taxtotal"] = filter_input(INPUT_POST, "sub_total");
    $salesorderdetails["ship_charge"] = filter_input(INPUT_POST, "ship_charge");
    $salesorderdetails["discount"] = filter_input(INPUT_POST, "discount");
    $salesorderdetails["total"] = filter_input(INPUT_POST, "total");

    $salesorderdetails["remark"] = filter_input(INPUT_POST, "remark");
    $salesorderdetails["added_by"] = $_SESSION["user"]["user_id"];
    $salesorderid = filter_input(INPUT_GET, "salesorderid");

    $postearray = filter_input_array(INPUT_POST);


    updateItems($postearray);

    if (isset($btnSaveSalesOrder) || isset($btnSaveNextSalesOrder) || isset($printButton) || isset($mailButton)) {
        if (isset($salesorderid) && $salesorderid != "") {
            MysqlConnection::edit("sales_order", $salesorderdetails, " id = '$salesorderid' ");
        }
        //include 'pdflib/salesorder.php';
        quickSalesInvoice($salesorderid, $isBackOrder);
        quickSalesOrder($salesorderid, $isBackOrder);
    }

    if ($btnSaveSalesOrder == "SAVE") {
        echo $btnSaveSalesOrder;
        header("location:index.php?pagename=manage_salesorder");
    } if ($btnSaveNextSalesOrder == "SAVE AND NEXT") {
        header("location:index.php?pagename=manage_salesorder&isBackOrder=Y");
    }if ($printButton == "PRINT" || $printButton == "INVOICE") {
        $_SESSION["salesorderid"] = $salesorderid;
        header("location:index.php?pagename=manage_salesorder");
    }if ($mailButton == "EMAIL") {
        echo $mailButton;
        header("location:index.php?pagename=email_salesorder&salesorderid=$salesorderid");
    }
}

function updateItems($postearray) {
    $salesid = filter_input(INPUT_GET, "salesorderid");
    $count = count($postearray["itemcount"]);
    for ($parr = 0; $parr < $count; $parr++) {
        $itemid = $postearray["itemid"][$parr];
        $returnqty = $postearray["returncount"][$parr] + getItemsReturnPrevQty($salesid, $itemid);
        $returnsalesorder = "UPDATE sales_item SET returnQty = $returnqty WHERE so_id = '$salesid' AND item_id='$itemid' ";
        MysqlConnection::executeQuery($returnsalesorder);
        updateInventory($itemid, $returnqty);
    }
}

function getItemsReturnPrevQty($salesid, $itemid) {
    $query = "SELECT returnQty FROM sales_item WHERE  so_id = '$salesid' AND item_id='$itemid' ";
    $resultset = MysqlConnection::fetchCustom($query);
    if (count($resultset) != 0) {
        return $resultset[0]["returnQty"];
    }
    return 0;
}

function quickSalesInvoice($salesorderid, $backorderstatus) {
    include 'pdflib/salesinvoice.php';
}

function quickSalesOrder($salesorderid, $backorderstatus) {
    include 'pdflib/salesorder.php';
}

function updateInventory($itemid, $returnqty) {
    $itemdata = MysqlConnection::getItemDataById($itemid);
    $returnupdate = $itemdata["onhand"] + $returnqty;
    $updateInventory = "UPDATE item_master SET onhand = '$returnupdate' WHERE item_id = '$itemid' ";
    MysqlConnection::executeQuery($updateInventory);
}
