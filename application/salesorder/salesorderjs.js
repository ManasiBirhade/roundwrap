

function setDetails(count) {
    var item_code = $("#tags" + count).val();
    var dataString = "item_code=" + item_code;

    var pricediscountvalue = $("#pricediscountvalue").text();
    var parsepricediscountvalue = 0;

    if (pricediscountvalue !== 0 || pricediscountvalue !== "") {
        parsepricediscountvalue = parseFloat(pricediscountvalue) / 100;
    }

    if (isNaN(parsepricediscountvalue)) {
        parsepricediscountvalue = 0;
    }
    var priceincreseby = $("#priceincreaseby").val();
    if (priceincreseby === "0" || priceincreseby === undefined || priceincreseby === "") {
        priceincreseby = 0;
    }

    $.ajax({
        type: 'POST',
        url: 'itemmaster/getitemajax.php',
        data: dataString
    }).done(function(data) {
        if (data !== null && data !== "") {
            var jsonobj = JSON.parse(data);
            if (jsonobj !== null) {
                $("#unit" + count).text(jsonobj.unit);
                if (parseFloat(jsonobj.total_sales_rate) === parseFloat("0.0")) {
                    var increseprice = ((parseFloat(jsonobj.sell_rate) * (parseFloat(priceincreseby) / parseFloat(100))).toFixed(2));

                    var discountprice = parseFloat(jsonobj.sell_rate) * parsepricediscountvalue;
                    console.log(parseFloat(jsonobj.sell_rate) - parseFloat(discountprice));
                    $("#price" + count).text((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));
                    $("#price" + count).val((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));

                    $("#tempItemPrice").text((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));
                    $("#tempItemPrice").val((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));

                } else {
                    var increseprice = (parseFloat(jsonobj.sell_rate) * (parseFloat(priceincreseby) / parseFloat(100)));

                    var discountprice = parseFloat(jsonobj.sell_rate) * parsepricediscountvalue;
                    console.log(parseFloat(jsonobj.sell_rate) - parseFloat(discountprice));
                    $("#price" + count).text((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));
                    $("#price" + count).val((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));

                    $("#tempItemPrice").text((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));
                    $("#tempItemPrice").val((parseFloat(jsonobj.sell_rate) - parseFloat(discountprice) + parseFloat(increseprice)).toFixed(2));

                }
                $("#onhand" + count).text(jsonobj.onhand);
                $("#totalvalue" + count).text(jsonobj.totalvalue);
                doCalculation();
            }
        } else {
            $("#tags" + count).val("");
        }
    }).fail(function() {
        $("#tags" + count).val("");
    });
}

function checkQty(count) {
    if (count !== undefined && count !== "") {
        var amount = $("#amount" + count).val();
        var onhand = $("#onhand" + count).text();
        var amountcl = parseInt(amount);
        var onhandcl = parseInt(onhand);
        if (onhandcl < amountcl) {
            $('#alertmodel').modal();
        }
    }
}


function checkReturnQty(count) {
    if (count !== undefined && count !== "") {
        var amount = $("#amount" + count).val();
        var onhand = $("#returncount" + count).val();
        var amountcl = parseInt(amount);
        var onhandcl = parseInt(onhand);
        if (onhandcl > amountcl) {
            $('#returncount' + count).val("");
            $('#returncount' + count).focus();
        }
    }
    doCalculation();
}


function clearValue(count) {
    $("#desc" + count).text("");
    $("#unit" + count).text("");
    $("#price" + count).text("");
    $("#tags" + count).val("");
    $("#total" + count).text("");
    $("#onhand" + count).text("");
    $("#totalvalue" + count).text(jsonobj.totalvalue);
    finalTotal();
}
function saleOrderRowDelete(rowid) {
    var row = document.getElementById(rowid);
    doCalculation();
    row.parentNode.removeChild(row);
    doCalculation(rowid);
}
function doCalculation(count) {

    var ship_charge = $("#ship_charge").val();
    if (ship_charge === undefined || ship_charge === "") {
        ship_charge = "0.0";
    }
    var ship_type = $("#radios").val();

    var restockpercent = $("#restockpercent").val();
    if (restockpercent === undefined || restockpercent === "") {
        restockpercent = "0.0";
    }

    var totalOfItems = 0.0;
    var restockperprice = 0.0;
    for (var index = 1; index <= 30; index++) {
        var price = $("#price" + index).val();
        var amount = $("#amount" + index).val();
        var returncount = $("#returncount" + index).val();
        if (returncount === "" || returncount === undefined || returncount === 0) {
            returncount = 0;
        }
        if (price === "" || price === "0" || price === undefined) {
            price = 0;
        }
        if (amount === "" || amount === "0" || amount === undefined) {
            amount = 0;
        }
        if (price !== "" && amount !== "") {
            var t = parseFloat(price) * (parseFloat(amount) - parseFloat(returncount));
            $("#total" + index).text((Math.round(t * 100) / 100));
            totalOfItems = parseFloat(totalOfItems) + t;
        }
    }
    $("#itemtotal").val(totalOfItems.toFixed(2));

    restockperprice = ((parseFloat(totalOfItems) * parseFloat(restockpercent) / 100)).toFixed(2);
    $("#restockamount").val(restockperprice);


    var discount = $("#discount").val();
    if (discount === undefined || discount === "") {
        discount = "0.0";
        $("#discountvalue").val("0.0");
    } else {
        discount = parseFloat(totalOfItems) * (parseFloat(discount) / 100);
        $("#discountvalue").val(discount.toFixed(2));
    }


    var taxname = $("#taxid option:selected").text();
    if (taxname !== undefined && taxname !== "") {
        var goodstotal = $("#itemtotal").val();
        var splitselect = taxname.split("-");
        if (splitselect.length === 2) {
            if (goodstotal !== undefined && goodstotal !== "" && splitselect !== undefined && splitselect !== "") {
                var taxAmount = ((parseFloat(goodstotal) - parseFloat(discount)) * parseFloat(splitselect[1]) / 100);
                $("#taxamount").val(taxAmount.toFixed(2));
            }
        } else {
            $("#taxamount").val("0.0");
        }
    }

    var taxamount = $("#taxamount").val();
    if (taxamount === "" || taxamount === undefined) {
        taxamount = 0.0;
    }

    var subTotalValue = 0.0;
    subTotalValue = (parseFloat(totalOfItems) - parseFloat(discount) + parseFloat(taxamount)).toFixed(2);
    $("#finaltotal").val(subTotalValue);


    var staxname = $("#staxname").val();
    var staxamount = 0.0;
    if (staxname === "GST-5") {
        var ship_charge = $("#ship_charge").val();
        if (ship_charge !== undefined && ship_charge !== "" && ship_charge !== "0") {
            staxamount = 0.05 * parseFloat(ship_charge);
        } else {
            staxamount = 0.0;
        }
        $("#staxpercent").val(staxamount.toFixed(2));
    } else {
        $("#staxpercent").val(staxamount.toFixed(2));
    }

    if (taxAmount === undefined) {
        taxAmount = 0.0;
    }



    var netFinalTotal = (parseFloat(subTotalValue) + parseFloat(staxamount) + parseFloat(ship_charge) - parseFloat(restockperprice)).toFixed(2);
    $("#nettotal").val((parseFloat(subTotalValue) + parseFloat(staxamount) + parseFloat(ship_charge) - parseFloat(restockperprice)).toFixed(2));
    $("#total").val(netFinalTotal);

    var creditlimit = $("#creditlimit").val();
    if (creditlimit !== undefined && creditlimit !== "") {
        if (parseFloat(creditlimit) < parseFloat(netFinalTotal)) {
            $('#creditlimitmodel').modal('show');
        }
    }

    checkQty(count);
}


function searchCustomer() {
    var companyname = $("#companyname").val();
    if (companyname !== "") {
        var dataString = "companyname=" + companyname;
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomer_ajax.php',
            data: dataString
        }).done(function(data) {
            var jsonobj = JSON.parse(data);
            $("#customer_id").val(jsonobj.id);
            $("#billTo_address").val(jsonobj.billto);
            $("#shipping_address").val(jsonobj.shipto);
            $("#taxname").val(jsonobj.taxname);
            $("#taxvalue").val(jsonobj.taxValue);
            $("#representative").val(jsonobj.sales_person_name);
            if (jsonobj.length === 0) {
                $("#error").text("Customer not available!!!");
            } else {
                $("#error").text("");
            }
        }).fail(function() {
        });
    }
}


function actionCalculation() {

    var tax = $("#taxamount").val();
    var taxAmount = "0.0";
    if (tax !== undefined || tax !== "") {
        taxAmount = $("#taxamount").val();
    }

    var staxAmount = $("#staxpercent").val();
    if (staxAmount === undefined || staxAmount === "") {
        staxAmount = "0.0";
    }

    var ship_charge = $("#ship_charge").val();
    if (ship_charge === undefined || ship_charge === "") {
        ship_charge = "0.0";
    }

    var discount = $("#discount").val();
    if (discount === undefined || discount === "") {
        discount = "0.0";
    }

    var totalOfItems = 0.0;
    for (var index = 1; index <= 30; index++) {
        var price = $("#price" + index).val();
        var amount = $("#amount" + index).val();
        var returncount = $("#returncount" + index).val();
        if (returncount === "" || returncount === undefined || returncount === 0) {
            returncount = 0;
        }
        if (price !== "" && amount !== "") {
            var t = parseFloat(price) * (parseFloat(amount) - parseFloat(returncount));
            $("#total" + index).text((Math.round(t * 100) / 100));
            totalOfItems = parseFloat(totalOfItems) + t;
        }
    }
    $("#itemtotal").val(totalOfItems.toFixed(2));
    //var taxAmount = (parseFloat(taxValue) / 100) * parseFloat(totalOfItems);
    if (taxAmount === undefined) {
        taxAmount = 0.0;
    }
    $("#finaltotal").val((parseFloat(totalOfItems) + parseFloat(taxAmount)).toFixed(2));

    $("#nettotal").val(((parseFloat(totalOfItems) - parseFloat(discount) + parseFloat(ship_charge) + parseFloat(taxAmount)) + parseFloat(staxAmount)).toFixed(2));

}


function searchCustomerModel(companyname) {
    if (companyname !== "") {
        var dataString = "companyname=" + companyname;
        $("div#divLoading").addClass('show');
        $.ajax({
            type: 'POST',
            url: 'customermaster/searchcustomerid_ajax.php',
            data: dataString
        }).done(function(data) {
            var jsonobj = JSON.parse(data);
            $("#customer_id").val(jsonobj.id);
            $("#companyname").val(jsonobj.cust_companyname);
            $("#billTo_address").val(jsonobj.billto);
            $("#shipping_address").val(jsonobj.shipto);
            $("#representative").val(jsonobj.sales_person_name);
            $("#taxname").val(jsonobj.tax);
            $("#taxvalue").val(jsonobj.taxvalues);
            if (jsonobj.length === 0) {
                $("#error").text("Customer not available!!!");
            } else {
                $("#error").text("");
            }
        }).fail(function() {
        });
        $("div#divLoading").removeClass('show');
        $("#shipvia").focus();
        $("#datepicker").focus();
        $('#customerModel').modal('hide');
    }
}

function calculateCharges() {
    var restockpercent = $("#restockpercent").val();
    if (restockpercent === undefined || restockpercent === "") {
        restockpercent = "0.0";
    }

    var goodstotal = $("#itemtotal").val();
    if (goodstotal === undefined || goodstotal === "") {
        goodstotal = "0.0";
    }

    var nettotal = $("#nettotal").val();
    if (nettotal === undefined || nettotal === "") {
        nettotal = "0.0";
    }

    var restockperprice = ((parseFloat(goodstotal) * parseFloat(restockpercent) / 100)).toFixed(2);

    $("#restockamount").val(restockperprice);

    $("#nettotal").val((parseFloat(nettotal) - parseFloat(restockperprice)).toFixed(2));

}


function checkLessValue(id) {
    var tempItemPrice = parseFloat($("#tempItemPrice").val());
    var newPrice = parseFloat($("#" + id).val());
    if (tempItemPrice > newPrice) {
        $('#duplicatecustomer').modal('show');
        $('#nextprice').text("Original Price :- " + tempItemPrice.toFixed(2) + " $");
        $('#nextprice').append("<hr/>");
        $('#nextprice').append("Price You entered :- " + newPrice.toFixed(2) + " $<br/>");
        $('#nextprice').append("<hr/>");
        $('#nextprice').append("Price - Less by " + (tempItemPrice - newPrice).toFixed(2) + " $");

        $("#" + id).val("");

        $("#oldPrice").val(tempItemPrice);
        $("#newPrice").val(newPrice);
        $("#currentIndex").val(id);

        $("#" + id).focus();
    }
}

function setPrice(type) {
    if (type === "old") {
        var index = $("#currentIndex").val();
        var v = $("#oldPrice").val();
        $("#" + index).val(v);
        $("#" + index).focus();
    } else {
        var index = $("#currentIndex").val();
        var rowIndex = index.substring(5);
        var v = $("#newPrice").val();
        $("#" + index).val(v);
        $("#amount" + rowIndex).focus();
    }
    $("#oldPrice").val("");
    $("#newPrice").val("");
    $("#currentIndex").val("");
    $("#" + id).focus();
}


function checkForShipVia(value) {

    var nettotal = $("#finaltotal").val();

    var ship_charge = $("#ship_charge").val();
    if (ship_charge === undefined || ship_charge === "") {
        ship_charge = "0.0";
    }
    var staxpercent = $("#staxpercent").val();
    if (staxpercent === undefined || staxpercent === "") {
        staxpercent = "0.0";
    }
    if (value === "prepaid") {
        $("#isPrepaid").val("1");
        $("#nettotal").val(parseFloat(nettotal).toFixed(2));
        $("#nettotal").val((parseFloat(nettotal) + (parseFloat(ship_charge) + parseFloat(staxpercent))).toFixed(2));
    } else {
        $("#isCollect").val("1");
        $("#nettotal").val(nettotal);

    }
} 