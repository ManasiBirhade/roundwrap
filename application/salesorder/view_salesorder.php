<?php
$salesorderid = filter_input(INPUT_GET, "salesorderid");
$flag = filter_input(INPUT_GET, "flag");
$customer = MysqlConnection::getSalesOrderDetailsById($salesorderid);

$isOpen = $customer["isOpen"];
$status = $customer["status"];

if ($isOpen == "N" && $status == "N" && $flag == "return") {
    header("location:index.php?pagename=manage_salesorder");
}

$itemsarrays = MysqlConnection::getSalesItemsDetailsById($salesorderid);
$customerdetails = MysqlConnection::fetchCustom("SELECT cust_companyname FROM `customer_master` WHERE  id = ( SELECT customer_id FROM sales_order WHERE id = '$salesorderid ')");

$returnSalesOrder = filter_input(INPUT_GET, "returnSalesOrder");

if (isset($_POST["returnSalesOrder"]) || isset($_POST["deleteItem"])) {
    if ($flag == "yes") {
        $salesorderid = $_POST["salesorderid"];
        $arrcustomerid = MysqlConnection::fetchCustom("SELECT  balance FROM customer_master WHERE id = ( SELECT customer_id FROM sales_order WHERE id = '$salesorderid ')");
        $supp_balance = $arrcustomerid[0]["balance"];
        $total = $customer["total"];
        $newbalance = $supp_balance - $total;
        MysqlConnection::delete("UPDATE customer_master SET balance = $newbalance WHERE id = ( SELECT customer_id FROM sales_order WHERE id = '$salesorderid ') ");
        MysqlConnection::delete("DELETE FROM sales_order WHERE id = '$salesorderid' ");
        MysqlConnection::delete("DELETE FROM sales_item WHERE so_id = '$salesorderid' ");
        MysqlConnection::delete("DELETE FROM tbl_selling_history WHERE so_id = '$salesorderid' ");
    }
    if ($flag == "return") {
        MysqlConnection::delete("UPDATE sales_order SET isOpen = 'N', status= 'N' WHERE id = '$salesorderid' ");
    }
    header("location:index.php?pagename=manage_salesorder&status=Y&action=delete");
}
?>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}


</style>
<form action="#" method="post">

    <div class="container-fluid" style="" >
        <?php
        if ($flag == "yes") {
            echo MysqlConnection::$DELETE;
        }
        ?>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">SALES ORDER VIEW</a></li>
                </ul>
            </div>


            <table  style="width: 100%" class="table table-bordered display nowrap sortable">
                <tr>
                    <td>
                        <table style="width: 100%">
                            <tr style="font-weight: bold; color: red">
                                <td style="background-color: white" ><b>SO NUMBER</b></td>
                                <td style="color: red;width: 22%" ><?php echo $customer["sono"] ?></td>
                                <td style="background-color: white" ><b>SALES DATE</b></td>
                                <td style="width: 22%"> <?php echo date("Y-m-d") ?></td>
                                <td style="background-color: white" ><b>SALES PERSON</b></td>
                                <td style="width: 10%"><?php echo MysqlConnection::getAddedBy($customer["added_by"]) ?></td>
                            </tr>

                            <tr>
                                <td style="background-color: white"><b>CUSTOMER NAME</b></td>
                                <td>
                                    <?php echo $customerdetails[0]["cust_companyname"] ?>
                                    <div id="error" style="color: red"></div>
                                </td>
                                <td style="background-color: white"><b>SHIP VIA</b></td>
                                <td><?php echo $customer["shipvia"] ?></td>
                                <td style="background-color: white">EXPECTED&nbsp;DELIVERY</td>
                                <td><?php echo MysqlConnection::convertToPreferenceDate($customer["expected_date"]) ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: white"><b>BILLING&nbsp;ADDRESS</b></td>
                                <td style="line-height: 18px;;resize: none;height: 75px;"><?php echo $customer["billTo_address"] ?></td>
                                <td style="background-color: white"><b>SHIPPING&nbsp;ADDRESS</b></td>
                                <td style="line-height: 18px;;resize: none;height: 75px;" ><?php echo $customer["shipping_address"] ?></td>
                                <td style="background-color: white"><b>REMARK&nbsp;/&nbsp;NOTE</b></td>
                                <td style="line-height: 18px;resize: none;height: 75px;"><?php echo $customer["remark"] ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>

                        <div style="width: 100%;">
                            <table class="table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #A9CDEC;color: white;">
                                    <td style="width: 25px;">#</td>
                                    <td >ITEM DETAIL</td>
                                    <td style="width: 100px;">UNIT</td>
                                    <td style="width: 100px;">PRICE</td>
                                    <td style="width: 100px;">ORDERED.QTY</td>
                                    <td style="width: 100px;">RETURNED.QTY</td>
                                    <td style="width: 100px;">SHIPPED.QTY</td>
                                </tr>
                                <?php
                                $index = 1;
                                foreach ($itemsarrays as $key => $soitem) {
                                    $itemid = $soitem["item_id"];
                                    $value = MysqlConnection::getItemDataById($itemid);
                                    ?>
                                    <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                        <td style="width: 25px"><?php echo $index++ ?></td>
                                        <td >
                                            <a href="index.php?pagename=view_itemmaster&itemId=<?php echo $value["item_id"] ?>"><?php echo $value["item_code"] ?> 
                                                <?php echo $value["item_desc_sales"] ?>
                                            </a>
                                        </td>
                                        <td ><?php echo $value["unit"] ?></td>
                                        <td ><?php echo $soitem["price"] ?></td>
                                        <td ><?php echo $soitem["qty"] ?></td>
                                        <td ><?php echo $soitem["returnQty"] ?></td>
                                        <td ><?php echo $soitem["rQty"] - $soitem["returnQty"] ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <?php
            if ($flag == "return") {
                $conform = "Do you want cancel this sales order ?";
            }
            ?>
            <div class="modal-footer " style="text-align: center"> 
                <p style="color: red;font-size:  14px;"><?php echo $conform ?></p>
                <a href="index.php?pagename=manage_salesorder&status=Y" id="btnSubmitFullForm" class="btn btn-info">BACK TO SALES ORDER</a>
                <?php
                if ($flag == "yes") {
                    ?>
                    <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info"/>
                    <input type="hidden" name="salesorderid" value="<?php echo filter_input(INPUT_GET, "salesorderid") ?>"/>
                    <?php
                } else if ($flag == "return") {
                    ?>
                    <input type="submit" value="CANCEL SALES ORDER" name="returnSalesOrder" class="btn btn-danger"/>
                    <?php
                }
                ?>
            </div>
            <hr/>
        </div>
    </div>
</form>
<script src="js/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/bootstrap-colorpicker.js"></script> 
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/select2.min.js"></script> 
<script src="js/maruti.js"></script> 
<script src="js/maruti.form_common.js"></script>

