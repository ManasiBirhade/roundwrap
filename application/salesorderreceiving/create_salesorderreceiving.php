<?php
$salesorderid = filter_input(INPUT_GET, "salesorderid");
$salesorder = MysqlConnection::getSalesOrderDetailsById($salesorderid);
if ($salesorder["isOpen"] == "N") {
   // header("location:index.php?pagename=success_salesorder&salesorderid=$salesorderidget&flag=closederror");
    header("location:index.php?pagename=manage_salesorder&status=Y&action=closederror");
}
$salesitems = MysqlConnection::getSalesItemsDetailsById($salesorderid);
$customerdetails = MysqlConnection::getCustomerDetails($salesorder["customer_id"]);


$save = filter_input(INPUT_POST, "save");
if (isset($save) && $save == "SUBMIT") {
    saveOrEditReceivingOrder(filter_input_array(INPUT_POST), $salesorderid);
}
?>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form  method="post">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">CREATE SALES ORDER</a></li>
                </ul>
            </div>
            <br/>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="table table-bordered display nowrap sortable " style="width: 100%">
                            <?php
                            $sales_person_name = MysqlConnection::getGenericById($customerdetails["sales_person_name"]);
                            ?>
                            <tr>
                                <td style="background-color: white" ><label class="control-label" >SO NUMBER</label></td>
                                <td><?php echo $salesorder["sono"] ?></td>
                                <td style="background-color: white" ><label class="control-label">SALES DATE</label></td>
                                <td><?php echo date("Y-m-d") ?></td>
                                <td style="background-color: white" ><label class="control-label" >SALES PERSON</label></td>
                                <td><?php echo $sales_person_name["code"] == "" ? $sales_person_name["name"] : $sales_person_name["code"] ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: white" ><label class="control-label" >CUSTOMER NAME</label></td>
                                <td><?php echo $customerdetails["cust_companyname"] ?></td>
                                <td style="background-color: white" ><label class="control-label">SHIP VIA</label></td>
                                <td><?php echo $salesorder["shipvia"] ?></td>
                                <td style="background-color: white" ><label class="control-label">EXPECTED&nbsp;DELIVERY</label></td>
                                <td><?php echo $salesorder["expected_date"] ?></td>
                            </tr>
                            <tr>
                                <td style="background-color: white" ><label  class="control-label"  >BILLING&nbsp;ADDRESS</label></td>
                                <td>
                                    <p style="width: 250px;">
                                        <?php echo trim($salesorder["billTo_address"]) ?>
                                    </p>
                                </td>
                                <td style="background-color: white" ><label class="control-label">SHIPPING&nbsp;ADDRESS</label></td>
                                <td>
                                    <p style="width: 250px;">
                                        <?php echo trim($salesorder["shipping_address"]) ?>
                                    </p>
                                </td>
                                <td style="background-color: white" ><label class="control-label">REMARK&nbsp;/&nbsp;NOTE</label></td>
                                <td>
                                    <?php echo trim($salesorder["remark"]) ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 100%;float: left">
                            <table class="display nowrap sortable table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: #76323F;color: white">
                                    <td style="width: 25px;">#</td>
                                    <td style="width: 600px;">ITEM DETAIL</td>
                                    <td style="width: 100px;">CURRENT STOCK</td>
                                    <td style="width: 80px;">ORD.QTY</td>
                                    <td style="width: 80px;">PRE.SALE</td>
									<td style="width: 80px;">PENDING</td>
                                    <td >SALES QTY</td>
                                </tr>
                            </table> 
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="display nowrap sortable table-bordered" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    $index = 1;
                                    foreach ($salesitems as $key => $soitem) {
                                        $value = MysqlConnection::getItemDataById($soitem["item_id"]);
                                        $currentstock = $value["onhand"] - $value["totalvalue"];
                                        if ($soitem["rQty"] - $soitem["qty"] != 0) {
                                            if ($currentstock < 0) {
                                                $back = "rgb(251,210,210)";
                                            } else {
                                                $back = "";
                                            }
											$pending = $soitem["qty"] - $soitem["rQty"];
                                            ?>
                                            <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;height: 30px;;background-color: <?php echo $back ?>">
                                                <td style="width: 25px"><?php echo $index ?></td>
                                                <td style="width: 600px;">  <a href="index.php?pagename=view_itemmaster&itemId=<?php echo $value["item_id"] ?>"><?php echo $value["item_code"] ?> <?php echo $value["item_desc_sales"] ?></a></td>
                                                <td style="width: 100px;"><?php echo $currentstock ?></td>
                                                <td style="width: 80px;"><?php echo $soitem["qty"] ?></td>
                                                <td style="width: 80px;"><?php echo $soitem["rQty"] ?></td>
												<td style="width: 80px;"><?php echo $pending ?></td>
                                                <td >
                                                    <?php
                                                    if ($pending > $currentstock) {
														
                                                        ?>
                                                        <a href="index.php?pagename=create_perchaseorder&itemId=<?php echo $soitem["item_id"] ?>&reqQty=<?php echo $pending?>&flag=purchase">
                                                            CREATE PO
                                                        </a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <input type="hidden" name="itemsid[]" id="itemsid[]" value="<?php echo $soitem["item_id"] ?>"> 
                                                        <input type="hidden" id="orderedqty<?php echo $index ?>" value="<?php echo $soitem["qty"] - $soitem["rQty"] ?>"> 

                                                        <input type="text" name="salesitems[]" onkeypress="return chkNumericKey(event)" 
                                                               value="<?php echo $soitem["qty"] - $soitem["rQty"] ?>"
                                                               onfocusout="validateQty('<?php echo $index ?>')" id="salesitems<?php echo $index ?>">  
                                                               <?php
                                                           }
                                                           ?>
                                                </td>
                                            </tr>
                                            <?php
                                            $index++;
                                        }
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>

                    </td>
                </tr>
            </table>
            <hr/>
            <div class="modal-footer " style="margin: 0 auto;text-align: center"> 
                <input type="submit" name="save" id="save" value="SUBMIT"  class="btn btn-info"/>
                <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
            </div>
            <input type="hidden" name="salesorderid" id="salesorderid" value="<?php echo filter_input(INPUT_GET, "salesorderid") ?>">
        </div>
    </div>

</form>
<script>
    function validateQty(id) {

        var lastreceived = parseInt($("#orderedqty" + id).val());
        var received = parseInt($("#salesitems" + id).val());
        var qty = parseInt($("#qty" + id).val());
        if (received > lastreceived) {
            $("#salesitems" + id).val("");
            $("#salesitems" + id).focus();
        }
        if (qty < received) {
            $("#salesitems" + id).val("");
            $("#salesitems" + id).focus();
        }
    }
</script>

<?php

function saveOrEditReceivingOrder($soreceived, $salesorderid) {
    unset($soreceived["save"]);
    for ($index = 0; $index < count($soreceived["salesitems"]); $index++) {
        $salesitems = $soreceived["salesitems"][$index];
        if ($salesitems != "") {
            $itemsid = $soreceived["itemsid"][$index];

            // UPDATING ITEM MASTER
            $itemdata = MysqlConnection::getItemDataById($itemsid);
            $updatestock = $itemdata["onhand"] - $salesitems;
            MysqlConnection::delete("UPDATE item_master SET onhand = $updatestock WHERE item_id = '$itemsid'");

            // UPDATING SALES ITEMS
            $salesitemsarr = MysqlConnection::fetchCustom("SELECT * FROM `sales_item` WHERE `so_id` = '$salesorderid' AND `item_id` = '$itemsid'");
            $salesitem = $salesitemsarr[0];
            $salesstock = $salesitem["rQty"] + $salesitems;
            MysqlConnection::delete("UPDATE sales_item SET rQty = $salesstock WHERE item_id = '$itemsid' AND so_id = '$salesorderid' ");
            $backqty = $salesitem["qty"] - $salesstock;
            MysqlConnection::delete("UPDATE sales_item SET backQty = $backqty WHERE item_id = '$itemsid' AND so_id = '$salesorderid' ");
        }
    }
    //
    $sumqty = MysqlConnection::fetchCustom("SELECT SUM(`qty`) as qty  FROM  `sales_item` WHERE so_id ='$salesorderid' ");
    $sumrqty = MysqlConnection::fetchCustom("SELECT SUM(`rQty`) as rqty  FROM  `sales_item` WHERE so_id ='$salesorderid' ");
    if ($sumqty[0]["qty"] == $sumrqty[0]["rqty"]) {
        MysqlConnection::delete("UPDATE sales_order SET isOpen = 'N'  where  id = '$salesorderid' ");
        header("location:index.php?pagename=manage_salesorder&status=Y");
    } else {
        MysqlConnection::delete("UPDATE sales_order SET isBackOrder = 'Y'  where  id = '$salesorderid' ");
        header("location:index.php?pagename=manage_salesorder&isBackOrder=Y");
    }
}
?>