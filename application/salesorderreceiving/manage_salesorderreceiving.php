<?php
if (isset($_POST) && count($_POST) != 0) {
    $sono = $_POST["sono"];
    $scustomername = trim($_POST["scustomername"]);
    $susername = trim($_POST["susername"]);
    $shipvia = trim($_POST["shipvia"]);
    $datepicker1 = trim($_POST["datepicker1"]);
    $datepicker2 = trim($_POST["datepicker2"]);

    $andscustomername = $scustomername == "" ? "" : " AND `customer_id` = $scustomername   ";
    $andsusername = $susername == "" ? "" : "AND `added_by` = '$susername' ";
    $andshipvia = $shipvia == "" ? "" : "AND `shipvia` = '$shipvia' ";


    $reportresult = MysqlConnection::fetchCustom("SELECT * FROM `sales_order` WHERE id!= 0  $andscustomername  $andsusername $andshipvia ");
}
?>
<style>
    .customtable{
        width: 100%;
        height: auto;
        min-height: 50%;
        font-family: verdana;
        border: solid 1px gray;
        border-color: gray;
    }
    .customtable tr{
        height: 25px;
        border-color: gray;
    }
    .customtable tr td{
        /*        padding: 5px;*/
        border-color: gray;
    }
    .thead{
        height: 35px;
    }
    .brdright{
        border-right: solid 1px rgb(220,220,220);
    }
</style>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<link href="css/tablelayout.css" rel="stylesheet" type="text/css">
<script>
    $(function () {
        $("#datepicker1").datepicker({
            minDate: -20,
            maxDate: "+1M +10D"
        });
        $("#datepicker1").datepicker("option", "dateFormat", "yy-mm-dd");
    });
    $(function () {
        $("#datepicker2").datepicker({
            minDate: -20,
            maxDate: "+1M +10D"
        });
        $("#datepicker2").datepicker("option", "dateFormat", "yy-mm-dd");
    });
</script>      

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">SALES ORDER REPORT</h5>
    </div>
    <fieldset class="well the-fieldset" style="border-radius: 5px;">
        <form name="frm" method="post">
            <table border='0' style="width: 100%">
                <tr>
                    <td>Sales Person</td>
                    <td>
                        <select name="customer_id" id="customer_id">
                            <option value=""></option>

                        </select>
                    </td>
                    <td>Customer Name</td>
                    <td>
                        <select name="scustomername" id="scustomername">
                            <option value=""></option>
                            <?php
                            $resultset = MysqlConnection::getCustomerData();
                            foreach ($resultset as $key => $value) {
                                $secus = $_POST["scustomername"] == $value["id"] ? "selected" : "";
                                ?>
                                <option value="<?php echo $value["id"] ?>" <?php echo $secus ?>>
                                    <?php echo $value["cust_companyname"] ?>
                                </option>
                            <?php } ?>
                        </select>
                    </td>
                    <td>Entered By</td>
                    <td>
                        <select name="susername" id="susername">
                            <option value=""></option>
                            <?php
                            $resultsetv = MysqlConnection::getUserData();
                            foreach ($resultsetv as $key => $value) {
                                $seuser = $_POST["susername"] == $value["user_id"] ? "selected" : "";
                                ?>
                                <option value="<?php echo $value["user_id"] ?>" <?php echo $seuser ?> >
                                    <?php echo $value["firstName"] ?> <?php echo $value["lastName"] ?>
                                </option>
                            <?php } ?>
                        </select>
                    </td>
                    <td></td>
                </tr> 
                <tr>
                    <td>SO NO</td>
                    <td><input type="text" name="sono" value="<?php echo $_POST["sono"] ?>"></td>
                    <td>From Date</td>
                    <td><input type="text" id="datepicker1" name="datepicker1" value="<?php echo $_POST["datepicker1"] ?>"></td>
                    <td>To Date</td>
                    <td><input type="text" id="datepicker2" name="datepicker2" value="<?php echo $_POST["datepicker2"] ?>"></td>
                    <td>
                        <input type="submit" name="search" value="SEARCH" class="btn btn-info"/>
                    </td>
                </tr> 
            </table>
        </form>
    </fieldset>
    </br>
    <section class="">
        <div class="scroll">
            <table id="data">
                <thead>
                    <tr class="header">
                        <th  style="text-align: center;width: 10px;"><div>#</div></th>
                <th ><div>SO No</div></th>
                <th><div>Customer Name</div></th>
                <th ><div>Ship Via</div></th>
                <th ><div>Net Amt</div></th>
                <th ><div>Delivery Date</div></th>
                <th ><div>Sales Person</div></th>
                <th ><div>Entered By</div></th>
                </tr>
                </thead>
                <tbody style="border-color: #76323F"> 
                    <?php
                    $index = 1;
                    foreach ($reportresult as $key => $value) {
                        $customer = MysqlConnection::fetchCustom("SELECT cust_companyname FROM customer_master WHERE id = " . $value["customer_id"]);
                        ?>
                        <tr id="<?php echo $value["id"] ?>" class="context-menu-one" onclick="setId('<?php echo $value["id"] ?>')" style="border-bottom: solid 1px rgb(220,220,220);text-align: left;vertical-align: central">
                            <td style="text-align: center"><?php echo $index ?></td>
                            <td >&nbsp;&nbsp;<?php echo $value["sono"] ?></td>
                            <td>&nbsp;&nbsp;<?php echo $customer[0]["cust_companyname"]; ?></td>
                            <td >&nbsp;&nbsp;<?php echo $value["shipvia"] ?></td>
                            <td style="text-align: right"><?php echo $value["total"] ?>&nbsp;&nbsp;</td>
                            <td style="text-align: center"><?php echo $value["expected_date"] ?></td>
                            <td >&nbsp;&nbsp;<?php echo MysqlConnection::getSalesPersonName($value["customer_id"]) ?></td>
                            <td >&nbsp;&nbsp;<?php echo MysqlConnection::getAddedBy($value["added_by"]) ?></td>
                        </tr>
                        <?php
                        $index++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </section>
</div>


<input type="hidden" id="deleteId" name="cid" value="">
<input type="hidden" id="flag" name="flag" value="">
<input type="hidden" id="rightClikId" name="rightClikId" value="">

<script type="text/javascript">
    $(function () {
        $('#data').tablesorter();
    });
</script>      

<script>
    $("#deleteThis").click(function () {
        $("div#divLoading").addClass('show');
        var dataString = "deleteId=" + $('#deleteId').val();
        $.ajax({
            type: 'POST',
            url: 'salesorder/salesorder_ajax.php',
            data: dataString
        }).done(function (data) {
        }).fail(function () {
        });
        location.reload();
    });

    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }

    function setId(val) {
        document.getElementById("rightClikId").value = val;
    }
</script>
<script>

    $('tr').dblclick(function () {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_salesorder&salesorderid=" + id;
        }
    });

</script>

<script>
    function searchData() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchinput");
        filter = input.value.toUpperCase();
        table = document.getElementById("data");
        tr = table.getElementsByTagName("tr");
        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>