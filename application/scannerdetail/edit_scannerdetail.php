<?php
if (isset($_POST["btnSubmitFullForm"])) {
    unset($_POST["btnSubmitFullForm"]);
    unset($_POST["scannerid"]);
    MysqlConnection::edit("tbl_scanner", $_POST, " id = '" . $_GET["scannerid"] . "'");
    header("location:index.php?pagename=manage_scannerdetail");
} else {
    $flag = filter_input(INPUT_GET, "flag");
    $scannerid = filter_input(INPUT_GET, "scannerid");

    $arrscanner = MysqlConnection::fetchCustom("SELECT * FROM  `tbl_scanner` WHERE id = '$scannerid' ");
    $scanner = $arrscanner[0];
}
?>
<style>
    .widget-box input{
        background-color: white;
        background: white;
    }
    .widget-box textarea{
        background-color: white;
        background: white;
    }

</style>
<div class="container-fluid"  >
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">SCANNER INFO VIEW</h5>
    </div>
    <br/>

    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
        <div class="widget-title">
            <ul class="nav nav-tabs">
                <li id="siTab1" class="active"><a data-toggle="tab" href="#tab1">Scanner Information </a></li>

            </ul>
        </div>
        <form name="frmUserSubmit"  enctype="multipart/form-data" id="frmUserSubmit" method="post" >
            <input type="hidden" name="scannerid" id="scannerid" value="<?php echo $scanner["scannerId"] ?>" />
            <div class="widget-content tab-content">
                <div id="tab1" class="tab-pane active">
                    <table style="width: 100%; vertical-align: top" class="display nowrap sortable">
                        <tr>
                            <td><label class="control-label">Scanner Portfolio</label></td>
                            <td><input type="text" name="scannerDept" value="<?php echo $scanner["scannerDept"] ?>"/></td>
                            <td><label class="control-label" style="float: left">Scanner Move No</label></td>
                            <td><input type="text" name="scannerMoveNo"   value="<?php echo $scanner["scannerMoveNo"] ?>"  /></td>
                            <td><label class="control-label" style="float: left">Product Id </label></td>
                            <td><input type="text" name="prodId"   value="<?php echo $scanner["prodId"] ?>"  /></td>

                        </tr>
                        <tr>
                            <td><label class="control-label" style="float: left">Product Name </label></td>
                            <td><input type="text" name="prodName"  value="<?php echo $scanner["prodName"] ?>"  /></td>
                            <td><label class="control-label"  style="float: left">Vendor Detail</label></td>
                            <td><input type="text" name="vendorId"  value="<?php echo $scanner["vendorId"] ?>" /></td>
							<td></td>
							<td></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="modal-footer " style="text-align: center"> 
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SAVE</button>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div>
