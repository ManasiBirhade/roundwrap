<?php
$listofscanners = MysqlConnection::fetchCustom("SELECT * FROM tbl_scanner");
?>

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">SCANNER LIST</h5>
    </div>
    <br/>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr >
                <td  style="text-align: center;width: 10px;">#</div></td>
                <td >Scanner Id</td>
                <td style="width: 200px">Scanner Portfolio</td>
                <td style="width: 200px" >Scanner Move No</td>
                <td >Active</td>
                <td>Prod Id</td>
                <td>Prod Name</td>
                <td>Vendor Detail</td>
            </tr>
        </thead>
        <tbody style="border-color: #76323F">
            <?php
            $index = 1;
            foreach ($listofscanners as $key => $value) {
                ?>
                <tr id="<?php echo $value["id"] ?>" style="<?php echo $bg ?>;border-bottom: solid 1px rgb(220,220,220);text-align: left;height: 35px;"  class="context-menu-one">
                    <td style="text-align: center">&nbsp;<?php echo $index ?></td>
                    <td >&nbsp;&nbsp;<?php echo $value["scannerId"] ?></td>
                    <td ><?php echo $value["scannerDept"] ?></td>
                    <td ><?php echo $value["scannerMoveNo"] ?></td>
                    <td ><?php echo $value["active"] ?></td>
                    <td ><?php echo $value["prodId"] ?></td>
                    <td ><?php echo $value["prodName"] ?></td>
                    <td ><?php echo $value["vendorId"] ?></td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_scanner":
                        window.location = "index.php?pagename=view_scannerdetail&scannerid=" + id;
                        break;
                    case "edit_scanner":
                        window.location = "index.php?pagename=edit_scannerdetail&scannerid=" + id;
                        break;
                    case "delete_scanner":
                        window.location = "index.php?pagename=view_scannerdetail&scannerid=" + id + "&flag=yes";
                        break;

                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_scannerdetail";
                }
            },
            items: {
                "view_scanner": {name: "VIEW SCANNER INFO", icon: ""},
                "edit_scanner": {name: "EDIT SCANNER INFO", icon: ""},
                "delete_scanner": {name: "DELETE SCANNER INFO", icon: ""}
            }
        });
    });
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_scannerdetail&scannerid=" + id;
        }
    });
</script>