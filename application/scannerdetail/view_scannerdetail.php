<?php
$flag = filter_input(INPUT_GET, "flag");
$scannerid = filter_input(INPUT_GET, "scannerid");

$arrscanner = MysqlConnection::fetchCustom("SELECT * FROM  `tbl_scanner` WHERE id = '$scannerid' ");
$scanner = $arrscanner[0];
if (isset($_POST["deleteItem"])) {
    MysqlConnection::delete("DELETE FROM `tbl_scanner` WHERE id = '$scannerid'");
    header("location:index.php?pagename=manage_scannerdetail");
}
?>
<style>
    .widget-box input{
        background-color: white;
        background: white;
    }
    .widget-box textarea{
        background-color: white;
        background: white;
    }

</style>
<div class="container-fluid"  >
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">SCANNER INFO VIEW</h5>
    </div>
    <br/>
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
        <div class="widget-title">
            <ul class="nav nav-tabs">
                <li id="siTab1" class="active"><a data-toggle="tab" href="#tab1">Scanner Information </a></li>

            </ul>
        </div>
        <div class="widget-content tab-content">
            <div id="tab1" class="tab-pane active">
                <table style="width: 100%; vertical-align: top" class="display nowrap sortable">
                    <tr>
                        <td><label class="control-label">Scanner Portfolio</label></td>
                        <td><input type="text"  value="<?php echo $scanner["scannerDept"] ?>" readonly="" /></td>
                        <td><label class="control-label" style="float: left">Scanner MoveNo</label></td>
                        <td><input type="text"  value="<?php echo $scanner["scannerMoveNo"] ?>" readonly="" /></td>
                        <td><label class="control-label" style="float: left">Product Id </label></td>
                        <td><input type="text"  value="<?php echo $scanner["prodId"] ?>" readonly="" /></td>

                    </tr>
                    <tr>
                        <td><label class="control-label" style="float: left">Product Name </label></td>
                        <td><input type="text"  value="<?php echo $scanner["prodName"] ?>" readonly="" /></td>
                        <td><label class="control-label"  style="float: left">Vendor Detail</label></td>
                        <td><input type="text"  value="<?php echo $scanner["vendorId"] ?>" readonly=""/></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="modal-footer " style="text-align: center"> 
            <?php
            if (isset($flag) && $flag != "") {
                ?>
                <form name="frmDeleteItem" id="frmDeleteItem" method="post">
                    <input type="submit" value="DELETE" name="deleteItem" style="background-color: #A9CDEC" class="btn btn-info"/>
                    <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                </form>
                <?php
            } else {
                ?>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                <?php
            }
            ?>
        </div>
    </div>
</div>
