<?php
session_start();
ob_start();
error_reporting(0);

header('Content-Type: text/plain');
require('php-excel-reader/excel_reader2.php');
require('SpreadsheetReader.php');

$excelpath = "E:\\ROUNDWRAP\\EXCEL-IMPORT\\Contact Master.xlsx";

$arrayimportfor = array();
$arrayimportfor[] = "Import Customer";
$arrayimportfor[] = "Import Items";
$arrayimportfor[] = "Import Vendor";
$arrayimportfor[] = "Import Profile";

$extracteddata = getExcelData($excelpath);

function getExcelData($excelpath) {
    try {
        $spreadsheet = new SpreadsheetReader($excelpath);
        $sheets = $spreadsheet->Sheets();
        $arrayextracteddata = array();
        $header = array();
        $data = array();
        foreach ($sheets as $index => $name) {
            $spreadsheet->ChangeSheet($index);
            $index = 0;
            foreach ($spreadsheet as $key => $row) {
                if ($index == 0) {
                    array_push($header, $row);
                } else {
                    array_push($data, $row);
                }
                $index++;
            }
        }
        array_push($arrayextracteddata, $header);
        array_push($arrayextracteddata, $data);
    } catch (Exception $E) {
        echo $E->getMessage();
    }
    return $arrayextracteddata;
}
?>

<div style="margin: 0 auto;width: 500p;height: 300px;">

</div>