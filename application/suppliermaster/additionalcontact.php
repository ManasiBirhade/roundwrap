<?php
$arraycontacts = MysqlConnection::fetchCustom("SELECT *  FROM  `supplier_contact` where supp_id = '$supplierid'");
?>
<script>
    $(document).ready(function($) {
        $("#alterno").mask("(999) 999-9999");
        $("#mobno").mask("(999) 999-9999");
        $("#mobno1").mask("(999) 999-9999");
        $("#mobno2").mask("(999) 999-9999");
    });
</script>

<table class="display nowrap sortable table-bordered" style="width: 100%;" id="supplierInfo" vertical-align="top">
    <tr style="background-color: #A9CDEC;color: white">
        <td>Name</td>
        <td>Email</td>
        <td>Mobile No</td>
        <td>Phone No</td>
        <td>Designation</td>

    </tr>
    <?php
    if (count($arraycontacts) != 0) {
        $index = 1;
        foreach ($arraycontacts as $key => $value) {
            ?>
            <tr>
                <td><input style="width: 220px" type="text" name="contact_person[]" value="<?php echo $value["person_name"] ?>"  id="contact_person" minlength="2" maxlength="30"></td>
                <td><input style="width: 220px" type="email" name="email[]" autofocus="" value="<?php echo $value["person_email"] ?>"  id="email" minlength="2" maxlength="30"></td>
                <td>
                    <input type="text" style="width: 50px;" name="mobcode[]" id="code"  value="<?php echo $value["mobcode"] == "" ? "+1" : $value["mobcode"] ?>" >
                    <input style="width: 170px" type="text" name="mobno[]" id="mobno"  value="<?php echo $value["mobno"] ?>" >
                </td>
                <td>
                    <input type="text" style="width: 50px;" name="code[]" id="code"  value="<?php echo $value["code"] == "" ? "+1" : $value["code"] ?>" >
                    <input style="width: 170px" type="text" id="alterno" name="alterno[]" value="<?php echo $value["person_phoneNo"] ?>"   minlength="2" maxlength="20">
                </td>
                <td>
                    <input style="width: 220px" type="text" id="designation" name="designation[]" value="<?php echo $value["designation"] ?>"  minlength="2" maxlength="30">
                    <?php
                    if ($index == 1) {
                        echo '<a  class="icon-plus" href="#" id="icon-plus" ></a>';
                    } else {
                        echo '<a class="icon-trash" href="#" id="icon-trash" ></a>';
                    }
                    ?>
                </td>
            </tr>
            <?php
            $index++;
        }
    } else {
        ?>
        <tr>

            <td><input style="width: 220px" type="text" name="contact_person[]"  id="contact_person" minlength="2" maxlength="30"></td>
            <td><input style="width: 220px" type="email" name="email[]" autofocus=""  id="email" minlength="2" maxlength="30"></td>
            <td>
                <input type="text" style="width: 50px;" name="mobcode[]" id="code"  value="<?php echo $value["mobcode"] == "" ? "+1" : $value["mobcode"] ?>" >
                <input style="width: 170px" type="text" name="mobno[]" id="mobno1"  value="<?php echo $value["mobno"] ?>" >
            </td>
            <td>
                <input type="text" style="width: 50px;" name="code[]" id="code"  value="<?php echo $value["code"] == "" ? "+1" : $value["code"] ?>" >
                <input style="width: 170px" type="text" id="alterno" name="alterno[]"  minlength="2" maxlength="20">
            </td>
            <td>
                <input style="width: 220px" type="text" id="designation" name="designation[]"  minlength="2" maxlength="20">
                <a style="margin-left: 20px;margin-bottom: 10px;" class="icon-plus" href="#"  ></a>

            </td>
        </tr>
        <?php
    }
    ?>
</table>

<hr/>
<input type="button" id="btnVenPrev1" value="PREVIOUS" class="btn btn-info" href="#tab1">
<input type="submit" id="btnSubmiVendor" class="btn btn-info" onclick="validateVendorSubmit()" value="SAVE"/>
<a href="javascript:history.back()" class="btn btn-info">CANCEL</a>

<script type="text/javascript">
    jQuery(function() {
        var counter = 1;
        jQuery('a.icon-plus').click(function(event) {
            event.preventDefault();
            var newRow = jQuery('<tr><td><input style="width: 220px" type="text" name="contact_person[]" value="<?php echo filter_input(INPUT_POST, "contact_person") ?>"  id="contact_person' + counter + '" minlength="2" maxlength="30"></td>' +
                    counter + '<td><input style="width: 220px" type="email" name="email[]" autofocus="" value="<?php echo filter_input(INPUT_POST, "email") ?>" id="email' + counter + '" minlength="2" maxlength="30"></td>' +
                    counter + '<td><input type="text" style="width: 50px;" name="mobcode[]" id="mobcode"  value="+1" > <input style="width: 170px" type="text" id="mobno2' + counter + '" name="mobno[]"  value="<?php echo filter_input(INPUT_POST, "mobno") ?>" minlength="2" maxlength="20"></td>' +
                    counter + '<td><input type="text" style="width: 50px;" name="code[]" id="code"  value="+1" > <input style="width: 170px" type="text" id="alterno' + counter + '" name="alterno[]"  value="<?php echo filter_input(INPUT_POST, "alterno") ?>" minlength="2" maxlength="20"></td>' +
                    counter + '<td><input style="width: 220px" type="text" id="designation" name="designation[]"  value="<?php echo filter_input(INPUT_POST, "designation[]") ?>" minlength="2" maxlength="20"> <a style="margin-left: 20px;float:center;margin-bottom: 10px;" class="icon-trash" href="#"  ></a></td>');
            $("#alterno" + counter).mask("(999) 999-9999");
            $("#mobno2" + counter).mask("(999) 999-9999");
            counter++;
            jQuery('#supplierInfo').append(newRow);
        });
    });

    $(document).ready(function() {
        $("#supplierInfo").on('click', 'a.icon-trash', function() {
            $(this).closest('tr').remove();
        });
    });

    function validateVendorSubmit() {
        var firstname = $("firstname").val();
        var companyname = $("companyname").val();
        var supp_email = $("supp_email").val();
        var taxInformation = $("taxInformation").val();
        var supp_phoneNo = $("supp_phoneNo").val();
        var shippingtaxinfo = $("shippingtaxinfo").val();
        var supp_streetNo = $("supp_streetNo").val();
        var supp_city = $("supp_city").val();
        var supp_province = $("supp_province").val();
        var postal_code = $("postal_code").val();
        var supp_country = $("supp_country").val();

        if (firstname === undefined || companyname === undefined || supp_email === undefined || taxInformation === undefined ||
                supp_phoneNo === undefined || shippingtaxinfo === undefined || supp_streetNo === undefined || supp_city === undefined ||
                supp_province === undefined || postal_code === undefined || supp_country === undefined) {
            $('#siTab1').addClass('active');
            $('#tab1').addClass('active');
            $('#adTab2').removeClass('active');
            $('#tab2').removeClass('active');
        }

    }


</script>
