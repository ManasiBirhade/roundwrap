<?php
include '../MysqlConnection.php';

$supplierid = filter_input(INPUT_POST, "supplierid");
$arrsuppliernote = MysqlConnection::fetchCustom("SELECT * FROM `supplier_notes` WHERE `supp_id` = '$supplierid' ORDER BY adddate  DESC");
?>
<table  style="width: 100%;vertical-align: top" border="0">
    <tr style="height: 30px;background-color: rgb(240,240,240);">
        <th style="width: 100px;">&nbsp;DATE</th>
        <th>&nbsp;LAST NOTES</th>
    </tr>
    <?php
    foreach ($arrsuppliernote as $key => $value) {
        ?>
        <tr style="border-bottom: solid 1px rgb(220,220,220);vertical-align: top;padding: 3px;">
            <td>
                <p style="padding: 3px;">
                    <?php
                    $explod = explode(" ", $value["adddate"]);
                    echo MysqlConnection::convertToPreferenceDate($explod[0]);
                    ?>
                </p>
            </td>
            <td><p style="padding: 3px;text-align: justify"><?php echo $value["note"] ?></p></td>
        </tr>
        <?php
    }
    ?>
</table>