<?php
$status = filter_input(INPUT_GET, "status");
$action = filter_input(INPUT_GET, "action");
if ($status == "active") {
    $query = "SELECT * FROM `supplier_master` WHERE status = 'Y'  ORDER BY companyname ASC";
} else if ($status == "inactive") {
    $query = "SELECT * FROM `supplier_master` WHERE  status = 'N'   ORDER BY companyname ASC ";
} else if ($status == "all") {
    $query = "SELECT * FROM `supplier_master`   ORDER BY companyname ASC  ";
} else {
    $query = "SELECT * FROM `supplier_master`   ORDER BY companyname ASC  ";
}
$listofsupplier = MysqlConnection::fetchCustom($query);

$supplierid = filter_input(INPUT_GET, "supplierid");
if (isset($supplierid) && $supplierid != "") {
    $supplierd = MysqlConnection::fetchCustom("SELECT status FROM supplier_master WHERE supp_id = '$supplierid'");
    if ($supplierd[0]["status"] == "Y") {
        MysqlConnection::delete("UPDATE supplier_master SET status = 'N' WHERE supp_id = '$supplierid' ");
    } else {
        MysqlConnection::delete("UPDATE supplier_master SET status = 'Y' WHERE supp_id = '$supplierid'");
    }
    header("location:index.php?pagename=manage_suppliermaster&action=delete");
}
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>

<div class="container-fluid">
    <br/>
    <?php
    if ($action == "email") {
        echo MysqlConnection::$MSG_SU;
    } elseif ($action == "delete") {
        echo MysqlConnection::$MSG_DE;
    } elseif ($action == "update") {
        echo MysqlConnection::$MSG_UP;
    } elseif ($action == "add") {
        echo MysqlConnection::$MSG_AD;
    }
    ?>
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">VENDOR LIST</h5>
    </div>
    <br/>
    <table style="width: 100%" >
        <tr >
            <td style="float: left">
                <a class="btn btn-info"  href="index.php?pagename=create_suppliermaster" ><i class="icon icon-user"></i>&nbsp;ADD VENDOR</a>
                <a href="index.php?pagename=manage_suppliermaster&status=active" id="btnSubmitFullForm" class="btn btn-info">VIEW ACTIVE</a>
                <a href="index.php?pagename=manage_suppliermaster&status=inactive" id="btnSubmitFullForm" class="btn btn-info">VIEW INACTIVE</a>
                <a href="index.php?pagename=manage_suppliermaster&status=all" id="btnSubmitFullForm" class="btn btn-info">VIEW ALL</a>
            </td>
            <td style="float: right">
                <a href="sample/vendor-master.xlsx" id="btnSubmitFullForm" class="btn btn-info">EXCEL</a>
            </td>
        </tr>
    </table>
    <br/>

    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td >#</td>
                <td >Company Name</td>
                <td >Address</td>
                <td >Contact person</td>
                <td >Mobile No</td>
                <td >Contact No</td>
                <td >Email</td>
                <td >Balance</td>
                <td >Currency</td>
                <td >Note</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listofsupplier as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                $buildaddress = buildAddress($value);
                if ($value["status"] == "N") {
                    $back = $inavtivecolor;
                } else {
                    $back = "";
                }
                ?>
                <tr id="<?php echo $value["supp_id"] ?>" style="background-color: <?php echo $bgcolor ?>;"  class="context-menu-one">
                    <td >&nbsp;<?php echo $index ?></td>
                    <td ><?php echo $value["companyname"] ?></td>
                    <td><?php echo $buildaddress ?></td>
                    <td ><?php echo $value["firstname"] == "" ? "" : "" ?>&nbsp;<?php echo $value["firstname"] ?>&nbsp;<?php echo $value["lastname"] ?></td>
                    <td ><?php echo $value["mobcode"] . " " . $value["mobileno"] ?></td>
                    <td ><?php echo $value["phonecode"] . " " . $value["supp_phoneNo"] ?></td>
                    <td ><a href="mailto:<?php echo $value["supp_email"] ?>?Subject=Welcome, <?php echo ucwords($value["companyname"]) ?> " target="_top">
                            &nbsp;<?php echo $value["supp_email"] ?></a></td>
                    <td style="text-align: right"><?php echo $value["supp_balance"] ?>&nbsp;&nbsp;</td>
                    <td ><?php echo $value["currency"] ?></td>
                    <td >
                        <?php
                        $counter = MysqlConnection::fetchCustom("SELECT count(`id`) as id FROM `supplier_notes` where `supp_id` = '" . $value["supp_id"] . "'");
                        if ($counter[0]["id"] != 0) {
                            ?>
                            <a href="#" onclick="showNotes('<?php echo $value["supp_id"] ?>')">
                                <i class="icon-book"></i>
                            </a>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
</div>


<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_vendor":
                        window.location = "index.php?pagename=view_suppliermaster&supplierid=" + id;
                        break;
                    case "create_vendor":
                        window.location = "index.php?pagename=create_suppliermaster";
                        break;
                    case "edit_vendor":
                        window.location = "index.php?pagename=create_suppliermaster&supplierid=" + id;
                        break;
                    case "delete_vendor":
                        window.location = "index.php?pagename=view_suppliermaster&supplierid=" + id + "&flag=yes";
                        break;
                    case "create_perchase_order":
                        window.location = "index.php?pagename=create_perchaseorder&supplierid=" + id;
                        break;
                    case "active_vendor":
                        window.location = "index.php?pagename=manage_suppliermaster&supplierid=" + id;
                        break;
                    case "create_note":
                        window.location = "index.php?pagename=note_suppliermaster&supplierid=" + id;
                        break;
                    case "create_payment":
                        window.location = "index.php?pagename=vendor_payment&supplierid=" + id;
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_suppliermaster";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_vendor": {name: "VIEW VENDOR", icon: ""},
                "edit_vendor": {name: "EDIT VENDOR", icon: ""},
                "delete_vendor": {name: "DELETE VENDOR", icon: ""},
                "active_vendor": {name: "ACTIVE/IN ACTIVE VENDOR", icon: ""},
                "sep0": "---------",
                "create_note": {name: "CREATE NOTE", icon: ""},
                "create_perchase_order": {name: "CREATE PURCHASE ORDER", icon: ""},
                "create_payment": {name: "MAKE PAYMENT", icon: ""}
            }
        });

        //        $('.context-menu-one').on('click', function(e){
        //            console.log('clicked', this);
        //       })    
    });
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_suppliermaster&supplierid=" + id;
        }
    });
    
    $("#myElem").show().delay(3000).fadeOut();


    function showNotes(supplierid) {
        var dataString = "supplierid=" + supplierid;
        $("div#divLoading").addClass('show');
        $('#noteinformation').modal('show');
        var dataString = "supplierid=" + supplierid;
        $.ajax({
            type: 'POST',
            url: 'suppliermaster/ajaxsuppliernotes.php',
            data: dataString
        }).done(function(data) {
            $("#notedata").html(data);
            $("div#divLoading").removeClass('show');
        }).fail(function() {
        });
    }
</script>

<?php

function buildAddress($value) {
    return $value["supp_streetNo"]
            . " " . $value["supp_city"]
            . " " . $value["supp_province"]
            . " " . $value["postal_code"]
            . " <b>" . $value["supp_country"] . "</b>";
}
?>


<!-- this is note model dialog --->
<div id="noteinformation" class="modal hide" style="top: 10%;left: 50%;">
    <div class="modal-header" style="text-align: center">
        <button data-dismiss="modal" class="close" type="button">×</button>
        <h3>NOTES</h3>
    </div>
    <div class="modal-body">
        <div class="control-group">
            <div id="notedata"></div>
        </div>
    </div>
    <div class="modal-footer" style="text-align: center"> 
        <a data-dismiss="modal" class="btn btn-info" id="cancelti" href="#">CANCEL</a> 
    </div>
</div>
<!-- this is note model dialog --->