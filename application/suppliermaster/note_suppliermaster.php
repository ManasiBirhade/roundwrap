<?php
$supplierid = filter_input(INPUT_GET, "supplierid");
$type = filter_input(INPUT_GET, "type");
$flag = filter_input(INPUT_GET, "flag");
$supplier = MysqlConnection::getSupplierDetails($supplierid);
$arrsuppliernote = MysqlConnection::fetchCustom("SELECT * FROM  `supplier_notes` WHERE supp_id = '$supplierid'  ORDER BY ID DESC LIMIT 0,20");

if (isset($_POST["btnSubmitFullForm"])) {
    unset($_POST["btnSubmitFullForm"]);
    $_POST["supp_id"] = $supplierid;
    $_POST["adddate"] = MysqlConnection::convertToDBDate(date("Y-m-d"));
    MysqlConnection::insert("supplier_notes", $_POST);
    header("location:index.php?pagename=note_suppliermaster&supplierid=$supplierid");
}


$noteid = filter_input(INPUT_GET, "noteid");
if (isset($noteid)) {
    MysqlConnection::delete("DELETE FROM `supplier_notes` WHERE `id` = '$noteid' ");
    header("location:index.php?pagename=note_suppliermaster&supplierid=$supplierid");
}
?>
<style>
    .widget-box input{
        background-color: white;
        background: white;
    }
    .widget-box textarea{
        background-color: white;
        background: white;
    }
    input, textarea{
        width: 90%;
    }
</style>
<form name="frmCustomerNote" id="frmCustomerNote" method="post">
    <div class="container-fluid" id="tabs">
        <div class="cutomheader">
            <h5 style="font-family: verdana;font-size: 12px;">ADD VENDOR NOTE</h5>
        </div>
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title" >
                <ul class="nav nav-tabs" >
                    <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">Company Information </a></li>
                </ul>
            </div>
            <div class="widget-content tab-content">
                <div id="tab1" class="tab-pane active">
                    <table  style="width: 100%;vertical-align: top" border="0" class="display nowrap sortable">
                        <tr>
                            <td><label class="control-label">First Name</label></td>
                            <td><input type="text" readonly="" value="<?php echo $supplier["firstname"] ?>"  /></td>
                            <td><label class="control-label">Last Name</label></td>
                            <td><input type="text" readonly="" value="<?php echo $supplier["lastname"] ?>" /></td>
                            <td><label class="control-label">Company Name</label></td>
                            <td><input type="text"  value="<?php echo $supplier["companyname"] ?>" readonly=""/></td>
                        </tr>
                        <?php if ($type != "notedisplay") { ?>
                        <tr style="vertical-align: middle">
                            <td><label class="control-label">Note / Remark</label></td>
                            <td colspan="5">
                                <textarea autofocus="" required="" name="note" style="height: 70px;line-height: 18px;overflow: auto;width: 97.5%;resize: none"></textarea>
                            </td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="6">
                                <div style="height:  180px;overflow: auto;background: white;width: 100%;float: right">
                                    <table  style="width: 100%;vertical-align: top" border="0">
                                        <tr style="height: 30px;background-color: rgb(240,240,240);">
                                            <th style="width: 100px;">&nbsp;DATE</th>
                                            <th>&nbsp;LAST NOTES</th>
                                            <th style="width: 15px;">#</th>
                                        </tr>
                                        <?php
                                        foreach ($arrsuppliernote as $key => $value) {
                                            ?>
                                            <tr style="border-bottom: solid 1px rgb(220,220,220);">
                                                <td>&nbsp;
                                                    <?php
                                                    $explod = explode(" ", $value["adddate"]);
                                                    echo MysqlConnection::convertToPreferenceDate($explod[0]);
                                                    ?>
                                                </td>
                                                <td><p style="padding: 3px;text-align: justify"><?php echo $value["note"] ?></p></td>
                                                <td style="vertical-align: middle;width: 15px;" >
                                                    <a onclick="return confirm('Do you want to delete this note ??')" href="index.php?pagename=note_suppliermaster&supplierid=<?php echo $supplierid ?>&noteid=<?php echo $value["id"] ?>">
                                                        <i class="icon-remove"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>  
            <div class="modal-footer " style="text-align: center">
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SUBMIT</button>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div> 
        </div>
    </div>
</form>