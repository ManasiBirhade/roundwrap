<?php

error_reporting(0);
include '../MysqlConnection.php';

$suppid = trim($_POST["suppid"]);
if ($suppid != "") {
    $details = MysqlConnection::getSupplierDetails($suppid);
    $detailstax = MysqlConnection::getTaxInfoById($details["taxInformation"]);
    $details["taxname"] = $detailstax["taxname"];
    $details["taxValue"] = $detailstax["taxvalues"];

    $shippingtax = MysqlConnection::getTaxInfoById($details["shippingtaxinfo"]);
    $details["staxname"] = $shippingtax["taxname"];
    $details["staxpercent"] = $shippingtax["taxvalues"];

    if (empty($details)) {
        echo json_encode(array());
    } else {
        echo json_encode($details);
    }
} else {
    echo json_encode(array());
}
