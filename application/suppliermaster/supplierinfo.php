<?php
$arrsalutations = MysqlConnection::fetchCustom("SELECT distinct(`salutation`) as salutation FROM `supplier_master` WHERE salutation!=''");
$sqltaxinfodata = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY id DESC ;");
?>
<script>
    $(document).ready(function($) {
        $("#supp_phoneNo").mask("(999) 999-9999");
        $("#supp_fax").mask("(999) 999-9999"); 
        $("#mobileno").mask("(999) 999-9999");

    });
    $(document).ready(function($) {
        $("#creditcardno").mask("9999-9999-9999-9999");
    });
</script>

<table class="display nowrap sortable" style="width: 100%; vertical-align: top" >
    <tr>
        <td><label class="control-label" style="float: left">First Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input style="width: 220px" type="text" name="firstname" autofocus=""  onkeyup="fillAddress()"  required="" value="<?php echo $supplier["firstname"] ?>"  id="firstname" minlength="2" maxlength="30" ></td>
        <td><label class="control-label" style="float: left">Last Name </label></td>
        <td><input  style="width: 220px"type="text" name="lastname" autofocus=""  onkeyup="fillAddress()"   value="<?php echo $supplier["lastname"] ?>" id="lastname" minlength="2" maxlength="30" ></td>
        <td><label class="control-label"  style="float: left">Company Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input style="width: 220px" type="text" name="companyname"  onkeyup="fillAddress()"  id="companyname" onfocusout="validateDuplicate()"   value="<?php echo $supplier["companyname"] ?>" required=""  minlength="2" maxlength="30" ></td>
    </tr>
    <tr>
        <td><label class="control-label"  style="float: left">Email<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input  style="width: 220px"type="email" name="supp_email" id="supp_email"   required="" value="<?php echo $supplier["supp_email"] ?>"  minlength="2" maxlength="30" ></td>
        <td><label class="control-label">Mobile No</label></label></td>
        <td>
            <input type="text" style="width: 40px;"  name="mobcode1" id="mobcode"  value="<?php echo trim($supplier["mobcode"]) == "" ? "+1" : $supplier["mobcode"] ?>">
            <input style="width: 170px" type="text" name="mobileno1" id="mobileno"    value="<?php echo trim($supplier["mobileno"]) ?>" >
        </td>
        <td><label class="control-label" style="float: left">Phone No</label></td>
        <td>
            <input type="text" style="width: 40px;" value="<?php echo $supplier["phonecode"] == "" ? "+1" : $supplier["phonecode"] ?>" name="phonecode" id="phonecode" >
            <input style="width: 170px" type="tel" name="supp_phoneNo"  value="<?php echo $supplier["supp_phoneNo"] ?>"  id="supp_phoneNo" minlength="2" maxlength="20" >
        </td>
    </tr>
    <tr>
        <td><label class="control-label"  style="float: left">Fax </label></td>
        <td>
            <input type="text" style="width: 40px;"  value="<?php echo $supplier["faxcode"] == "" ? "+1" : $supplier["faxcode"] ?>" name="faxcode" id="faxcode" >
            <input style="width: 170px" type="text" name="supp_fax" id="supp_fax"    value="<?php echo $supplier["supp_fax"] ?>" minlength="2" maxlength="20">
        </td>
        <td><label class="control-label" style="float: left">Website</label></td>
        <td><input style="width: 220px" type="text" name="supp_website" id="supp_website"  value="<?php echo $supplier["supp_website"] == "" ? "-" : $supplier["supp_website"] ?>" minlength="2" maxlength="30"></td>
        <td><label class="control-label" style="float: left">Currency</label></td>
        <td>
            <select style="width: 225px;height: 26px" name="currency"  id="currency" value="">
                <?php
                $currencyarr = getcurrency();
                foreach ($currencyarr as $key => $value) {
                    ?>
                    <option value="<?php echo $key ?>" <?php echo $supplier["currency"] == $key ? "selected" : "" ?> ><?php echo $value ?></option>
                    <?php
                }
                ?>
            </select>
        </td>
    </tr>
    <tr >
        <td><label class="control-label"  style="float: left"> Good Tax<?php echo MysqlConnection::$REQUIRED ?></label> </td>
        <td>
            <select style="width: 225px" name="taxInformation" id="taxInformation" required="">
                <option value="">&nbsp;&nbsp;</option>
                <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                    <option   <?php echo $value["id"] == $supplier["taxInformation"] ? "selected" : "" ?>
                        value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                    <?php } ?>
            </select>
        </td>
        <td> <label class="control-label"  style="float: left">Shipping Tax<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td>
            <select style="width: 225px" name="shippingtaxinfo" id="shippingtaxinfo" required="">
                <option value="">&nbsp;&nbsp;</option>
                <?php foreach ($sqltaxinfodata as $key => $value) { ?>
                    <option   <?php echo $value["id"] == $supplier["shippingtaxinfo"] ? "selected" : "" ?>
                        value='<?php echo $value["id"] ?>'><?php echo $value["taxcode"] ?> - <?php echo $value["taxname"] ?> - <?php echo $value["taxvalues"] ?>%</option>
                    <?php } ?>
            </select>
        </td>

        <td><label class="control-label" style="float: left">Exchange Rate</label></td>
        <td><input style="width: 220px" type="text" name="exchange_rate" id="exchange_rate" onkeypress="return chkNumericKey(event)" value="<?php echo $supplier["exchange_rate"] ?>" maxlength="2"></td>

    </tr>
    <tr>
        <td><label class="control-label">Address<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input style="width: 220px" type="text" name="supp_streetNo" required=""  onkeyup="fillAddress()"  id="supp_streetNo"    minlenght="2" maxlength="70" value="<?php echo $supplier["supp_streetNo"] ?> <?php echo $supplier["supp_streetName"] ?>" ></td>

        <td><label class="control-label">City<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input style="width: 220px" type="text" name="supp_city" id="supp_city"  onkeyup="fillAddress()"  required=""  minlenght="2" maxlength="30" value="<?php echo $supplier["supp_city"] ?>" ></td>

        <td><label class="control-label">Province<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input style="width: 220px" type="text" name="supp_province"  onkeyup="fillAddress()"  id="supp_province"  required=""  minlenght="2" maxlength="30" value="<?php echo $supplier["supp_province"] ?>" ></td>
    </tr>
    <tr>

        <td><label class="control-label">Postal Code<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input style="width: 220px" type="text" name="postal_code" id="postal_code"  onkeyup="fillAddress()"  required=""  minlenght="2" maxlength="30"  value="<?php echo $supplier["postal_code"] ?>" ></td>
        <td><label class="control-label">Country<?php echo MysqlConnection::$REQUIRED ?></label></td>
        <td><input style="width: 220px" type="text" name="supp_country" id="supp_country"   onkeyup="fillAddress()"  required=""  minlenght="2" maxlength="30" value="<?php echo $supplier["supp_country"] ?>" ></td>
        <td></td>
        <td></td>
    </tr>
    <tr style="vertical-align: middle">
        <td><label class="control-label">Address</label></td>
        <td><textarea style="height: 130px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize" name="billto" onfocus="fillAddress()"  id="billto" ><?php echo $supplier["billto"] ?></textarea></td>
        <td><label class="control-label">Pick Up address</label></td>
        <td><textarea   style="height: 130px;resize: none;line-height: 20px;width: 221px;text-transform: capitalize"  name="shipto" id="shipto"><?php echo $supplier["shipto"] ?></textarea></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><a onclick="copyOrRemove('1')" style="cursor: pointer"  title="Copy address to pickup address">COPY >></a></td>
        <td></td>
        <td><a onclick="copyOrRemove('0')" style="cursor: pointer" title="Remove pickup address"><< REMOVE</a></td>
        <td></td>
        <td></td>
    </tr>
</table> 
<hr/>
<?php
if (isset($supplierid) && $supplierid != "") {
    ?>
    <button type="submit" onclick="return validateVendorSubmit()" id="btnSubmitFullForm" class="btn btn-info">SAVE</button>      
    <?php
}
?>
<input type="button" id="btnVenNext1" value="NEXT"   class="btn btn-info" >

<script>
    function validateDuplicate() {
        var dataString = "companyname=" + $("#companyname").val();
        $.ajax({
            type: 'POST',
            url: 'suppliermaster/searchsupplier_ajax.php',
            data: dataString
        }).done(function(data) {
            var obj = JSON.parse(data);
            var isavailable = obj.companyname;
            if (isavailable !== "" && isavailable !== undefined && isavailable !== "undefined") {
                $('#duplicatecustomer').modal('show');
                $("#companyname").val("");
                $("#companyname").focus();
            }
        }).fail(function() {
        });
    }
</script>

<!-- this is custom model dialog --->
<div id="duplicatecustomer" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header">
        <h3>DUPLICATION!!!</h3>
    </div>
    <div class="modal-body">
        <h5 style="color: red">
            Vendor with this company name already exist.<br/>
        </h5>
    </div>
    <div class="modal-footer"> 
        <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
    </div>
</div>
<!-- this is model dialog --->

<script>
    function  fillAddress() {

        document.getElementById("billto").value = "";
        var cust_companyname = document.getElementById("companyname").value;
        var firstname = document.getElementById("firstname").value === "" ? "" : document.getElementById("firstname").value;
        var lastname = document.getElementById("lastname").value === "" ? "" : document.getElementById("lastname").value;

        var streetNo = document.getElementById("supp_streetNo").value === "" ? "" : document.getElementById("supp_streetNo").value + "";
        var city = document.getElementById("supp_city").value === "" ? "" : document.getElementById("supp_city").value + "";
        var cust_province = document.getElementById("supp_province").value === "" ? "" : document.getElementById("supp_province").value + "";
        var country = document.getElementById("supp_country").value === "" ? "" : document.getElementById("supp_country").value + "";
        var postal_code = document.getElementById("postal_code").value === "" ? "" : document.getElementById("postal_code").value + "";

        document.getElementById("billto").value = cust_companyname + "\n" +
                "" + firstname + " " + lastname + ""
                + "\n" + streetNo + "\n" + city + "," + cust_province + "\n" + postal_code + " " + country;

        document.getElementById("shipto").value = document.getElementById("billto").value;
    }
    function copyOrRemove(flag) {
        if (flag === "1") {
            document.getElementById("shipto").value = document.getElementById("billto").value;
        } else {
            document.getElementById("shipto").value = "";
        }
    }
</script>