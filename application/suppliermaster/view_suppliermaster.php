<?php
$flag = filter_input(INPUT_GET, "flag");
$suppid = filter_input(INPUT_GET, "supplierid");

$arrsupplier = MysqlConnection::fetchCustom("SELECT * FROM  `supplier_master` WHERE supp_id = '$suppid' ");
$supplier = $arrsupplier[0];
$suppliercontactarray = MysqlConnection::fetchCustom("SELECT * FROM  `supplier_contact` WHERE supp_id = '$suppid' ");
$supplierrnote = MysqlConnection::fetchCustom("SELECT * FROM  `supplier_notes` WHERE supp_id = '$suppid'  ORDER BY ID DESC LIMIT 0,20");

if (isset($_POST["deleteItem"])) {
    MysqlConnection::delete("DELETE FROM `supplier_contact` WHERE supp_id = '$suppid'");
    MysqlConnection::delete("DELETE FROM `purchase_order` WHERE supplier_id = '$suppid'");
    MysqlConnection::delete("DELETE FROM `supplier_master` WHERE supp_id = '$suppid'");
    header("location:index.php?pagename=manage_suppliermaster&action=delete");
}
?>
<style>
    .widget-box input{
        background-color: white;
        background: white;
    }
    .widget-box textarea{
        background-color: white;
        background: white;
    }

</style>
<div class="container-fluid"  >
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">VIEW VENDOR</h5>
    </div>
    <?php
    if ($flag == "yes") {
        echo MysqlConnection::$DELETE;
    }
    ?>
    <div class="widget-box" style="width: 100%;background-color: white">
        <div class="widget-title">
            <ul class="nav nav-tabs">
                <li id="siTab1" class="active"><a data-toggle="tab" href="#tab1">VENDOR INFORMATION </a></li>
                <li id="adTab2"><a data-toggle="tab" href="#tab2">ADDITIONAL CONTACTS</a></li>
                <li id="noteTab3"><a data-toggle="tab" href="#tab3">NOTES & COMMENTS</a></li>
            </ul>
        </div>
        <div class="widget-content tab-content">
            <div id="tab1" class="tab-pane active">

                <table class="display nowrap sortable  table-bordered" style="width: 100%; vertical-align: top">
                    <tr>
                        <td><label class="control-label" style="float: left"><b>First Name</b></label></td>
                        <td><?php echo $supplier["firstname"] ?></td>
                        <td><label class="control-label" style="float: left"><b>Last Name</b> </label></td>
                        <td><?php echo $supplier["lastname"] ?></td>
                        <td><label class="control-label"  style="float: left"><b>Company Name</b></label></td>
                        <td><?php echo $supplier["companyname"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"  style="float: left"><b>Email</b></label></td>
                        <td><?php echo $supplier["supp_email"] ?></td>
                        <td><label class="control-label"><b>Mobile No</b></label></td>
                        <td><?php echo $supplier["mobcode"] . " " . $supplier["mobileno"] ?></td>
                        <td><label class="control-label"><b>Phone No</b></label></td>
                        <td><?php echo $supplier["phonecode"] . " " . $supplier["supp_phoneNo"] ?></td>


                    </tr>

                    <tr style="vertical-align: top">
                        <td><label class="control-label"  style="float: left"><b>Fax</b> </label></td>
                        <td><?php echo $supplier["faxcode"] . " " . $supplier["supp_fax"] ?></td>
                        <td><label class="control-label"><b>Good Tax</b></label></td>
                        <td><?php
                            $tax = MysqlConnection::getTaxInfoById($supplier["taxInformation"]);
                            echo $tax["taxname"] . " " . $tax["taxvalues"]
                            ?></td>
                        <td><label class="control-label"><b>Shipping Tax</b></label></td>
                        <td><?php
                            $tax = MysqlConnection::getTaxInfoById($supplier["shippingtaxinfo"]);
                            echo $tax["taxname"] . " " . $tax["taxvalues"]
                            ?>
                        </td>

                    </tr>
                    <tr>
                        <td><label class="control-label"  style="float: left"><b>Exchange Rate</b></label></td>
                        <td><?php echo $supplier["exchange_rate"] ?></td>
                        <td><label class="control-label"><b>Website</b></label></td>
                        <td><?php echo $supplier["supp_website"] ?></td>
                        <td><label class="control-label"><b>Currency</b></label> </td>
                        <td><?php echo getcurrency($supplier["currency"]) . " " . ($supplier["currency"]) ?></td>

                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Address</b></label></td>
                        <td><?php echo $supplier["supp_streetNo"] ?> <?php echo $supplier["supp_streetName"] ?></td>
                        <td><label class="control-label"><b>City</b></label></td>
                        <td><?php echo $supplier["supp_city"] ?></td>
                        <td><label class="control-label"><b>Province</b></label></td>
                        <td><?php echo $supplier["supp_province"] ?></td>

                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Postal Code</b></label></td>
                        <td><?php echo $supplier["postal_code"] ?></td>
                        <td><label class="control-label"><b>Country</b></label></td>
                        <td><?php echo $supplier["supp_country"] ?></td>
                        <td colspan="2"></td>

                    </tr>
                    <tr style="vertical-align: top">
                        <td><b>Bill To</b></td>
                        <td colspan="5" style="text-transform: capitalize"><?php echo $supplier["billto"] ?></td>

                    </tr>
                    <tr>
                        <td><b>Ship To</b></td>
                        <td colspan="5" style="text-transform: capitalize"><?php echo $supplier["shipto"] ?></td>
                    </tr>

                </table>
                <?php
                if (isset($flag) && $flag != "") {
                    ?>
                    <form name="frmDeleteCustomer" id="frmDeleteCustomer" method="post">
                        <input type="hidden" value="<?php echo $customerid ?>" name="customerid"/>
                        <a href="javascript:history.back()" class="btn btn-info" >CANCEL</a>
                        <input type="hidden" value="supplierId" value="<?php echo supplierId ?>"/>
                        <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info"  />
                        <input type="button" id="btnVenNext1" value="NEXT" class="btn btn-info"  />
                    </form>
                    <?php
                } else {
                    ?>
                    <br/>
                    <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
                    <input type="button" id="btnVenNext1" value="NEXT" class="btn btn-info" style="background-color: #A9CDEC" href="#tab1"/>
                    <?php
                }
                ?>


            </div>
            <div id="tab2" class="tab-pane " >
                <table id="addcontacts"  border="0" class="display nowrap sortable table-bordered"  style="width: 100%;background-color: white"> 
                    <tr style="background-color: #A9CDEC;color: white">
                        <td><b>Name</b></td>
                        <td><b>Email</b></td>
                        <td><b>Mobile No</b></td>
                        <td><b>Phone No</b></td>
                        <td><b>Designation</b></td>
                    </tr>
                    <?php foreach ($suppliercontactarray as $key => $value) { ?>
                        <tr style="vertical-align: bottom">
                            <td><?php echo $value["person_name"] ?></td>
                            <td><?php echo $value["person_email"] ?></td>
                            <td><?php echo $value["mobcode"] . " " . $value["mobno"] ?></td>
                            <td><?php echo $value["code"] . " " . $value["person_phoneNo"] ?></td>
                            <td><?php echo $value["designation"] ?></td>
                        </tr>
                    <?php } ?>
                </table>
                <hr/>
                <input type="button" id="btnVenPrev1" value="PREVIOUS" class="btn btn-info" href="#tab1"  style="background-color: #A9CDEC">
                <input type="button" id="btnVenNext2" value="NEXT" class="btn btn-info"  style="background-color: #A9CDEC" href="#tab3"/>
            </div>
            <div id="tab3" class="tab-pane">
                <table  style="width: 100%;vertical-align: top" border="0" class="display nowrap sortable table-bordered"  >
                    <tr style="height: 30px;background-color: rgb(240,240,240);">
                        <th style="width: 100px;">&nbsp;DATE</th>
                        <th>&nbsp;LAST NOTES</th>
                    </tr>
                    <?php
                    foreach ($supplierrnote as $key => $value) {
                        ?>
                        <tr style="border-bottom: solid 1px rgb(220,220,220);">
                            <td>&nbsp;
                                <?php
                                $explod = explode(" ", $value["adddate"]);
                                echo $explod[0];
                                ?>
                            </td>
                            <td><p style="padding: 3px;text-align: justify"><?php echo $value["note"] ?></p></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                <br/>
                <br/>
                <input   type="button" id="btnVenPrev2" style="background-color: #A9CDEC" value="PREVIOUS" class="btn btn-info" href="#tab2">
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </div>
    </div>
    <script>
        $('#btnVenNext1').on('click', function() {
            $('#siTab1').removeClass('active');
            $('#adTab2').addClass('active');
            $('#tab1').removeClass('active');
            $('#tab2').addClass('active');
        });
        $('#btnVenPrev1').on('click', function() {
            $('#adTab2').removeClass('active');
            $('#siTab1').addClass('active');
            $('#tab2').removeClass('active');
            $('#tab1').addClass('active');
        });
        $('#btnVenNext2').on('click', function() {
            $('#adTab2').removeClass('active');
            $('#noteTab3').addClass('active');
            $('#tab2').removeClass('active');
            $('#tab3').addClass('active');
        });
        $('#btnVenPrev2').on('click', function() {
            $('#noteTab3').removeClass('active');
            $('#adTab2').addClass('active');
            $('#tab3').removeClass('active');
            $('#tab2').addClass('active');
        });
    </script>