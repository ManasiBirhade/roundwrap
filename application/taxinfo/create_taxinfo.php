<?php
$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
$taxid = filter_input(INPUT_GET, "id");
if (isset($taxid)) {
    $result = MysqlConnection::fetchCustom("SELECT * FROM `taxinfo_table` WHERE `id` = '$taxid'");
    $resultset = $result[0];
}
if (isset($btnSubmitFullForm)) {
    $array = array();
    $array["taxcode"] = filter_input(INPUT_POST, "taxcode");
    $array["taxname"] = filter_input(INPUT_POST, "taxname");
    $array["taxdescription"] = filter_input(INPUT_POST, "taxdescription");
    $array["taxvalues"] = filter_input(INPUT_POST, "taxvalues");
    $array["taxtype"] = filter_input(INPUT_POST, "taxtype");

    $array["tax"] = filter_input(INPUT_POST, "taxcode");
    $array["isExempt"] = filter_input(INPUT_POST, "isExempt");


    if (isset($taxid)) {
        MysqlConnection::edit("taxinfo_table", $array, " id = '$taxid'");
    } else {
        MysqlConnection::insert("taxinfo_table", $array);
    }
    header("location:index.php?pagename=manage_taxinfo");
}
?>

<div class="container-fluid" >
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">TAX INFO</h5>
    </div>
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
        <form name="frmItemsSubmit" id="frmItemsSubmit" enctype="multipart/form-data" method="post" action="">
            <div class="widget-content tab-content"  >
                <table class="display nowrap sortable">
                    <tr>
                        <td><label class="control-label">Tax Type</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td >
                            <select style="width: 207px;height: 25px" name="taxtype"  id="taxtype" >
                                <option>Select</option>
                                <option <?php echo $resultset["taxtype"] == "resale" ? "selected" : "" ?> value="resale">Resale</option>
                                <option <?php echo $resultset["taxtype"] == "regular" ? "selected" : "" ?> value="regular"> Regular</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Tax code</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td><input style="width: 200px" type="text" class="custominput" required="" value="<?php echo $resultset["taxcode"] ?>" autofocus="" name="taxcode" maxlength="30" id="country"></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Tax Name</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td><input style="width: 200px" type="text" class="custominput" required="" value="<?php echo $resultset["taxname"] ?>" name="taxname" maxlength="30" id="province"></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Tax Description</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td><input style="width: 200px" type="text" class="custominput" required="" value="<?php echo $resultset["taxdescription"] ?>" name="taxdescription" id="tax"></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Tax Percent</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <td><input style="width: 200px" type="text" class="custominput"  required="" maxlength="3 " onkeypress="return chkNumericKey(event)" value="<?php echo $resultset["taxvalues"] ?>" name="taxvalues" id="percentage"></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Is Exempt</label></td>
                        <td>&nbsp;&nbsp;:&nbsp;&nbsp;</td>
                        <?php
                        $on = $resultset["isExempt"] == "on" ? "checked" : "";
                        ?>
                        <td><input type="checkbox" name="isExempt" <?php echo $on ?> id="isExempt" style="width:15px;height:15px;"></td>
                    </tr>
                </table>
            </div>  
            <div class="modal-footer " style="text-align: center">
                <input type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info"  value="SAVE"/>
                <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div>
