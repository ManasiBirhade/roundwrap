
<?php
$listoftaxinfo = MysqlConnection::fetchCustom("SELECT * FROM taxinfo_table ORDER BY  taxtype");
$id = filter_input(INPUT_GET, "id");
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 380,
            "scrollX": true
        });
    });
</script>

<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">TAX LIST</h5>
    </div>
    <br/>
    <table>
        <tr >
            <td style="width: 8%"><a class="btn btn-info" href="index.php?pagename=create_taxinfo"  data-toggle="modal">ADD TAX INFO</a></td>
        </tr>
    </table>
    <br/>

    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td style="width: 25px">#</td>
                <td>Tax Code</td>
                <td>Tax Name</td>
                <td>Tax Type</td>
                <td>Values</td>
                <td>Is Exempt</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($listoftaxinfo as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                ?>
                <tr id="<?php echo $value["id"] ?>" style="background-color: <?php echo $bgcolor ?>;" class="context-menu-one">
                    <td style="width: 25px;text-align: center"><?php echo $index ?></td>
                    <td><?php echo $value["taxcode"] ?></td>
                    <td ><?php echo $value["taxname"] ?></td>
                    <td><?php echo  ucwords($value["taxtype"]) ?></td>
                    <td><?php echo $value["taxvalues"] ?></td>
                    <td><?php echo $value["isExempt"] == "on" ? "Yes" : "" ?></td>
                </tr>
                <?php
                $index++;
            }
            ?>

        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var profile = $(this).attr('id');
                switch (key) {

                    case "view_taxinfo":
                        window.location = "index.php?pagename=view_taxinfo&id=" + profile;
                        break;
                    case "edit_taxinfo":
                        window.location = "index.php?pagename=create_taxinfo&id=" + profile;
                        break;
                    case "delete_taxinfo":
                        window.location = "index.php?pagename=view_taxinfo&id=" + profile + "&flag=yes";
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    default:
                        window.location = "index.php?pagename=manage_taxinfo";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_taxinfo": {name: "VIEW TAX INFO ", icon: ""},
                "edit_taxinfo": {name: "EDIT TAX INFO ", icon: ""},
                "delete_taxinfo": {name: "DELETE TAX INFO", icon: ""}
            }
        });
        $('tr').dblclick(function() {
            var id = $(this).attr('id');
            if (id !== undefined) {
                window.location = "index.php?pagename=view_taxinfo&id=" + id;
            }
        });

    });
</script>