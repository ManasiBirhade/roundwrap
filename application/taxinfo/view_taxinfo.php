<?php
$taxid = filter_input(INPUT_GET, "id");
$flag = filter_input(INPUT_GET, "flag");
if (isset($taxid)) {
    $result = MysqlConnection::fetchCustom("SELECT * FROM `taxinfo_table` WHERE `id` = '$taxid'");
    $resultset = $result[0];
}
if (filter_input(INPUT_POST, "deleteItem") == "DELETE") {
    MysqlConnection::delete("DELETE FROM taxinfo_table WHERE id = '$taxid'");
    header("location:index.php?pagename=manage_taxinfo");
}
?>
<style>
    .sortable input{
        background-color: white;
    }
    
</style>
<div class="container-fluid" >
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">TAX INFO</h5>
    </div>
    <?php
    if ($flag == "yes") {
        echo MysqlConnection::$DELETE;
    }
    ?>
    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
        <form name="frmItemsSubmit" id="frmItemsSubmit" enctype="multipart/form-data" method="post" action="">
            <div class="widget-content tab-content"  >
                <table class="display nowrap sortable  table-bordered">
                    <tr>
                        <td><label class="control-label"><b>Tax Type</b></label></td>
                        <td style="width: 250px;"><?php echo $resultset["taxtype"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Tax code</b></label></td>
                        <td style="width: 250px;"><?php echo $resultset["taxcode"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Tax Name</b></label></td>
                        <td><?php echo $resultset["taxname"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Tax Description</b></label></td>
                        <td><?php echo $resultset["taxdescription"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Tax Percent</b></label></td>
                        <td><?php echo $resultset["taxvalues"] ?></td>
                    </tr>
                    <tr>
                        <td><label class="control-label"><b>Is Exempt</b></label></td>
                        <?php
                        $on = $resultset["isExempt"] == "on" ? "checked" : "";
                        ?>
                        <td> <?php echo $on ?></td>
                    </tr>
                </table>
            </div>  
            <div class="modal-footer " style="text-align: center">
                <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                <?php
                if ($flag != "") {
                    ?>
                    <input type="submit" name="deleteItem" value="DELETE"  class="btn btn-info" />
                    <?php
                }
                ?>
            </div>
        </form>

    </div>
</div>
