<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>jQuery UI Dialog Demo - Ashish's Blog</title>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.ui/1.8.6/jquery-ui.min.js"></script>
        <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.6/themes/smoothness/jquery-ui.css">
            <script type="text/javascript">
                $(document).ready(function() {
                    $('a#pop').live('click', function(e) {
                        e.preventDefault();
                        var page = $(this).attr("href")
                        var pagetitle = $(this).attr("title")
                        var $dialog = $('<div></div>')
                                .html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
                                .dialog({
                                    autoOpen: false,
                                    modal: true,
                                    height: 625,
                                    width: 500,
                                    title: pagetitle
                                });
                        $dialog.dialog('open');
                    });
                });
            </script>
    </head>
    <body>
        <a id="pop" href="http://localhost/roundwrap/trunk/application/index.php?pagename=note_customermaster&customerId=be007a58f557150d1e16baeda24ab62b&type=notedisplay" title="Ashish Blog">AshishBlog</a>
    </body>
</html>