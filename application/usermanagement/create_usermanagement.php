<?php
$user = getUserFronLogin();
$arrmaster = getUserMenus("master");
$arrretial = getUserMenus("retial");
$arrproduction = getUserMenus("production");
$arrsystem = getUserMenus("system");


$userid = filter_input(INPUT_GET, "userid");
if (!isset($userid)) {
    $companyinfoarr = MysqlConnection::fetchCustom("SELECT * FROM `user_master`  LIMIT 0,1");
    $user = $companyinfoarr[0];
    unset($user["firstName"]);
    unset($user["lastName"]);
    unset($user["roleName"]);
    unset($user["email"]);
}

$btnSubmitFullForm = filter_input(INPUT_POST, "btnSubmitFullForm");
if (isset($btnSubmitFullForm)) {
    $userid = saveOrEditUser();
    menuMapping($userid);
    header("location:index.php?pagename=manage_usermanagement");
}


$userroles = getUserRoles();
?>
<script>
    $(function() {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
    $(function() {
        $("#datepicker1").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">MANAGE USER</h5>
    </div>
    <div class="widget-box" style="border-bottom: solid 1px #CDCDCD;background-color: white">
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">USER INFORMATION</a></li>
            </ul>
        </div>
        <form name="frmUserSubmit"  enctype="multipart/form-data" id="frmUserSubmit" method="post" >
            <div class="widget-content tab-content">
                <table class="display nowrap sortable" style="width: 100%;vertical-align: top" border="0">
                    <tr>
                        <td><label class="control-label">First Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="firstName" id="firstname"  value="<?php echo $user["firstName"] ?>" autofocus="" required="true" minlenght="2" maxlength="30" ></td>
                        <td><label class="control-label">Last Name</label></td>
                        <td><input type="text" name="lastName" id="lastname"  value="<?php echo $user["lastName"] ?>" minlenght="2" maxlength="30" ></td>
                        <td><label class="control-label">Designation<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td>  
                            <select id="roleName" required="" name="roleName" style="height: 25px;width: 212px">
                                <option value="">Select</option>
                                <?php
                                foreach ($userroles as $key => $value) {
                                    if ($user["roleName"] == $value["id"]) {
                                        $selected = "selected";
                                    } else {
                                        $selected = "";
                                    }
                                    ?>
                                    <option <?php echo $selected ?> value="<?php echo $value["id"] ?>"><?php echo $value["code"] . " " . $value["name"] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Company Name<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="cmpName" required="true"  value="<?php echo $user["cmpName"] ?>" id="cmpName" minlenght="2" maxlength="30"></td>
                        <td><label class="control-label">Email<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="email" name="email" id="email" required="true"  value="<?php echo $user["email"] ?>" ></td>
                        <td><label class="control-label">Phone<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="phone" id="phone" required="true" value="<?php echo trim($user["phone"]) ?>" ></td>

                    </tr> 
                    <tr>
                        <td><label class="control-label">Address<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="streetNo" id="streetNo" required="true" minlenght="2" maxlength="70" value="<?php echo $user["streetNo"] ?>" ></td>
                        <td><label class="control-label">Postal Code<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="postalCode" id="postalCode" minlenght="2" maxlength="30" required="true" value="<?php echo $user["postalCode"] ?>" ></td>
                        <td><label class="control-label">City<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="city" id="city"  minlenght="2" required="true" maxlength="30" value="<?php echo $user["city"] ?>" ></td>

                    </tr>
                    <tr>

                        <td><label class="control-label">Province<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="province" id="cust_province" required="true"  minlenght="2" maxlength="30" value="<?php echo $user["province"] ?>" ></td>
                        <td><label class="control-label">Country<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="country" id="country" required="true" minlenght="2" maxlength="30" value="<?php echo $user["country"] ?>" ></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">Created On<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="createdDate" id="datepicker" required="true"   value="<?php echo $user["createdDate"] ?>" ></td>
                        <td><label class="control-label">Expiry Date<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input type="text" name="expiryDate" id="datepicker1" required="true"  value="<?php echo $user["expiryDate"] ?>" ></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
                <hr/>
                <table   id="data" style="width: 80%;" > 
                    <tr style="background-color: #A9CDEC;height: 30px;">
                        <td style="color: white">&nbsp;MASTER</td>
                        <td style="color: white">&nbsp;RETAIL</td>
                        <td style="color: white">&nbsp;PRODUCTION</td>
                        <td style="color: white">&nbsp;SYSTEM</td>
                    </tr>
                    <?php
                    $settingArray = getSettingArray();
                    $master = $settingArray["master"];
                    $retail = $settingArray["retail"];
                    $production = $settingArray["production"];
                    $system = $settingArray["system"];
                    ?>
                    <tr style="height: 27px;">
                        <td style="vertical-align: top">
                            <table>
                                <?php
                                $index = 1;
                                foreach ($master as $key => $value) {
                                    if (in_array("master$$$value", $arrmaster)) {
                                        $checked1 = "checked";
                                    } else {
                                        $checked1 = "";
                                    }
                                    ?>
                                    <tr style="height: 27px;">
                                        <td>&nbsp;<input <?php echo $checked1 ?> type="checkbox" name="master[]"  id="master<?php echo $index++ ?>"  value="<?php echo "master$$" . $value ?>"></td>
                                        <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                    </tr>
                                <?php } ?>
                            </table>     
                        </td>

                        <td style="vertical-align: top">
                            <table>
                                <?php
                                $index1 = 1;
                                foreach ($retail as $key => $value) {
                                    if (in_array("retial$$$value", $arrretial)) {
                                        $checked2 = "checked";
                                    } else {
                                        $checked2 = "";
                                    }
                                    ?>
                                    <tr style="height: 27px;">
                                        <td>&nbsp;<input <?php echo $checked2 ?> type="checkbox" id="retail<?php echo $index1++ ?>" name="retial[]" value="<?php echo "retial$$" . $value ?>"></td>
                                        <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                    </tr>
                                <?php } ?>
                            </table>     
                        </td>


                        <td style="vertical-align: top">
                            <table>
                                <?php
                                $index2 = 1;
                                foreach ($production as $key => $value) {
                                    if (in_array("production$$$value", $arrproduction)) {
                                        $checked3 = "checked";
                                    } else {
                                        $checked3 = "";
                                    }
                                    ?>
                                    <tr style="height: 27px;">
                                        <td>&nbsp;<input <?php echo $checked3 ?> type="checkbox" id="production<?php echo $index2++ ?>" name="production[]" value="<?php echo "production$$" . $value ?>"></td>
                                        <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                    </tr>
                                <?php } ?>
                            </table>     
                        </td>

                        <td style="vertical-align: top">
                            <table>
                                <?php
                                $index3 = 1;
                                foreach ($system as $key => $value) {
                                    if (in_array("system$$$value", $arrsystem)) {
                                        $checked4 = "checked";
                                    } else {
                                        $checked4 = "";
                                    }
                                    ?>
                                    <tr style="height: 27px;">
                                        <td>&nbsp;<input <?php echo $checked4 ?> type="checkbox" id="system<?php echo $index3++ ?>" name="system[]" value="<?php echo "system$$" . $value ?>"></td>
                                        <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                    </tr>
                                <?php } ?>
                            </table>     
                        </td>
                    </tr>
                </table>
                <hr/>
            </div>
            <div class="modal-footer ">
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm"  class="btn btn-info">SAVE</button>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div> 
<script>
    $(document).ready(function($) {
        $("#phone").mask("+999-999-999-9999");
        $("#fax").mask("+999-999-999-9999");

    });
    function chkNumericKey(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode >= 48 && charCode <= 57) || charCode === 46 || charCode === 45) {
            return true;
        } else {
            return false;
        }
    }
</script>

<?php

function getUserFronLogin() {
    $userid = filter_input(INPUT_GET, "userid");
    if (!empty($userid)) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM user_master WHERE user_id =  '$userid'");
        return $resultset[0];
    }
}

function getUserMenus($type) {
    $userid = filter_input(INPUT_GET, "userid");
    if (!empty($userid)) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `tbl_user_menu_mapping` where mainmenu = '$type' AND userid = '$userid'  ");
        $array = array();
        foreach ($resultset as $value) {
            array_push($array, $value["mainmenu"] . "$$" . $value["submenu"]);
        }
        return $array;
    }
}

function saveOrEditUser() {
    $userdata = filter_input_array(INPUT_POST);
    unset($userdata["btnSubmitFullForm"]);
    $rpassword = random_password(8);

    $userdata["username"] = $username = $userdata["email"];
    $userid = filter_input(INPUT_GET, "userid");

    unset($userdata["master"]);
    unset($userdata["retial"]);
    unset($userdata["production"]);
    unset($userdata["system"]);

    if (isset($userid) && $userid != "") {
        MysqlConnection::edit("user_master", $userdata, " user_id = '$userid' ");
        return $userid;
    } else {
        $userdata["password"] = md5($rpassword);
        sendEailToUser($username, $rpassword, $userdata);
        return MysqlConnection::insert("user_master", $userdata);
    }
}

function getUserRoles() {
    return MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'usertype' ORDER BY `code` ASC");
}

function getSettingArray() {
    $array = array();
    $array["master"] = array("item_master", "customer_master", "vendor_master", "profile_master", "edit_master", "step_integration", "tax_info_master", "profile_price", "pvc_preferencemaster");
    $array["retail"] = array("purchase_orders", "sales_order", "back_order", "retail_invoice", "item_discrepancy");
    $array["production"] = array("packing_slip", "quotation", "work_order", "production_invoice");
    $array["system"] = array("user_management", "scanner_details", "update_password", "email_setup", "excel_import");
    return $array;
}

function random_password($length = 8) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr(str_shuffle($chars), 0, $length);
    return $password;
}

function insertMapping($arrmaster, $userid) {
    foreach ($arrmaster as $value) {
        $explode = explode("$$", $value);
        $array["mainmenu"] = $explode[0];
        $array["submenu"] = $explode[1];
        $array["active"] = "Y";
        $array["userid"] = $userid;
        MysqlConnection::insert("tbl_user_menu_mapping", $array);
    }
}

function menuMapping($userid) {
    MysqlConnection::delete("DELETE FROM tbl_user_menu_mapping WHERE userid = '$userid' ");
    $arrmaster = $_POST["master"];
    $arrretial = $_POST["retial"];
    $arrproduction = $_POST["production"];
    $arrsystem = $_POST["system"];

    insertMapping($arrmaster, $userid);
    insertMapping($arrretial, $userid);
    insertMapping($arrproduction, $userid);
    insertMapping($arrsystem, $userid);
}

function sendEailToUser($username, $password, $userdata) {
    $emailconfig = MysqlConnection::getEmailConfigDetail();
    $attachment = "";
    $subject = "Your new account has been created. Welcome to the Roundwrap Ind.";
    $bodymatter = "<html>"
            . "<body>"
            . "Hi " . $userdata["firstName"] . ",<br/>"
            . "<br/>"
            . "Your account has been created successfully.<br/>"
            . "Please find credentials with application link.<br/><br/>"
            . "<b>Application URL : http://35.183.37.135/application/login.php</b><br/>"
            . "<b>Username : $username</b><br/>"
            . "<b>Password : $password</b><br/>"
            . "<br/><br/>" . str_replace("\n", "<br/>", strip_tags($emailconfig["body"]))
            . "</body>"
            . "<html>";
    $title = "User Registration.";
    MysqlConnection::sendEmail($username, $attachment, $subject, $bodymatter, $title);
}
