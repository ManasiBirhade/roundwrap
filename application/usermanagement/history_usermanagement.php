<?php
$userid = filter_input(INPUT_GET, "userid");
if (isset($userid) && $userid != "") {
    $resultset = MysqlConnection::fetchCustom("SELECT lh.`accountname`, lh.`logintime`, lh.`logouttime`, lh.`duration`, lh.`ipaddress` "
                    . "FROM `tbl_login_history` lh , user_master um "
                    . "WHERE lh.accountname = um.email AND  um.user_id = '$userid'' AND um.email = '" . $_SESSION["user"]["email"] . "' ");
}
?>
<style>
    .customtable{
        width: 100%;
        height: auto;
        min-height: 50%;
        font-family: verdana;
        border: solid 1px gray;
        border-color: gray;
    }
    .customtable tr{
        height: 25px;
        border-color: gray;
    }
    .customtable tr td{
        /*padding: 5px;*/
        border-color: gray;
    }
    .thead{
        height: 35px;
    }
    .brdright{
        border-right: solid 1px rgb(220,220,220);
    }   
    .btn-info{

        background-color:#76323F;
    }
</style>
<link href="css/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
<script src="js/jquery.min_1.11.3.js"></script>
<script src="js/jquery.contextMenu.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<link href="css/tablelayout.css" rel="stylesheet" type="text/css">
<script>
    $("#liveTableSearch").on("keyup", function () {
        var value = $(this).val();
        $("table tr").each(function (index) {
            if (index !== 0) {
                $row = $(this);
                var id = $row.find("td:first").text();
                if (id.indexOf(value) !== 0) {
                    $(this).hide();
                }
                else {
                    $(this).show();
                }
            }
        });
    });
</script>


<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">LOGIN HISTORY</h5>
    </div>
    <br/>
    <table style="width: 100%" border="0">
        <tr>


            <td style="float: right"><a target="_blank" href="usermanagement/print_usermanagement.php?userid=<?php echo $userid ?>" id="btnSubmitFullForm" class="btn btn-info">PRINT</a>

            </td>
        </tr>
    </table>
    <br/>
    <section class="">
        <div class="scroll">
            <table id="data">
                <thead>
                    <tr class="header">
                        <th  style="text-align: center;width: 10px;"><div>#</div></th>
                <th ><div>Account Name</div></th>
                <th ><div>LoginTime</div></th>
                <th ><div>LogoutTime</div></th>
                <th ><div>Duration</div></th>
                <th ><div>IP Address</div></th>


                </tr>
                </thead>
                <tbody style="border-color: #76323F">
                    <?php
                    $index = 1;
                    foreach ($resultset as $key => $value) {
                        if ($index % 2 == 0) {
                            $bg = $even;
                        } else {
                            $bg = $odd;
                        }
                        $buildaddress = getAddress($value);

                        $ts1 = strtotime($value["logintime"]);
                        $ts2 = strtotime($value["logouttime"]);
                        $seconds_diff = $ts2 - $ts1;
                        $time = ($seconds_diff / 3600);
                        ?>
                        <tr id="<?php echo $value["user_id"] ?>" style="<?php echo $bg ?>;border-bottom: solid 1px rgb(220,220,220);text-align: left;height: 35px;"  class="context-menu-one">
                            <td style="text-align: center">&nbsp;<?php echo $index ?></td>
                            <td >&nbsp;&nbsp;<?php echo $value["accountname"] ?></td>
                            <td ><?php echo $value["logintime"] ?>&nbsp;&nbsp;</td>
                            <td ><?php echo $value["logouttime"] ?>&nbsp;&nbsp;</td>
                            <td >&nbsp;&nbsp;<?php echo round(abs($time),2)  ?> Hr</td>
                            <td ><?php echo $value["ipaddress"] ?></td>

                        </tr>
                        <?php
                        $index++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </section>

</div>
<!--
    <div >
        <table>
            <td ><a href="index.php?pagename=manage_usermanagement&status=active" id="btnSubmitFullForm" class="btn btn-info">VIEW ACTIVATE</a></td>
            <td></td>
            <td ><a href="index.php?pagename=manage_usermanagement&status=inactive" id="btnSubmitFullForm" class="btn btn-info">VIEW INACTIVE</a></td>
            <td></td>
            <td ><a href="index.php?pagename=manage_usermanagement&status=all" id="btnSubmitFullForm" class="btn btn-info">VIEW ALL</a></td>
            <td></td>
        </table>
    </div>-->

<script type="text/javascript">
    $(function () {
        $('#data').tablesorter();
    });
</script>
<?php

function getAddress($value) {
    return $value["streetNo"];
}
?>
<script>
    $("#deleteThis").click(function () {
        var dataString = "deleteId=" + $('#deleteId').val();
        $.ajax({
            type: 'POST',
            url: 'usermanagement/usermanagement_ajax.php',
            data: dataString
        }).done(function (data) {
//            location.reload();
        }).fail(function () {
        });
        location.reload();
    });

    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }

</script>
<script type="text/javascript">
    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_user":
                        window.location = "index.php?pagename=view_usermanagement&userid=" + id;
                        break;
                    case "create_user":
                        window.location = "index.php?pagename=create_usermanagement";
                        break;
                    case "edit_user":
                        window.location = "index.php?pagename=create_usermanagement&userid=" + id;
                        break;
                    case "delete_user":
                        window.location = "index.php?pagename=view_usermanagement&userid=" + id + "&flag=yes";
                        break;

                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_usermanagement";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_user": {name: "VIEW USER", icon: ""},
                "create_user": {name: "CREATE USER", icon: ""},
                "edit_user": {name: "EDIT USER", icon: ""},
                "delete_user": {name: "DELETE USER", icon: ""},
                "sep0": "---------",
                "quit": {name: "QUIT", icon: function () {
                        return '';
                    }}
            }
        });

        //        $('.context-menu-one').on('click', function(e){
        //            console.log('clicked', this);
        //       })    
    });
    $('tr').dblclick(function () {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_usermanagement&userid=" + id;
        }
    });
</script>