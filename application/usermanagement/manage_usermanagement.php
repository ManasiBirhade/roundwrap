<?php
$listofuser = MysqlConnection::fetchCustom("SELECT * FROM user_master WHERE user_id !='6151ceac01a407f914f2294159116a15' ");
$userid = filter_input(INPUT_GET, "userid");
if (isset($userid) && $userid != "") {
    $arruser = MysqlConnection::fetchCustom("SELECT status FROM `user_master` WHERE `user_id` = '$userid''");
    $searcheduser = $arruser[0]["status"];
    if ($searcheduser == "Y") {
        MysqlConnection::delete("UPDATE `user_master` SET status = 'N' WHERE `user_id` = '$userid'' "); // this is for update
    } else {
        MysqlConnection::delete("UPDATE `user_master` SET status = 'Y' WHERE `user_id` = '$userid'' "); // this is for update
    }
    header("location:index.php?pagename=manage_usermanagement");
}
?>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });
</script>
<div class="container-fluid">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">USERS LIST</h5>
    </div>
    <br/>
    <table>
        <tr >
            <td ><a style="background-color:#A9CDEC "class="btn btn-info"  href="index.php?pagename=create_usermanagement" ><i class="icon icon-user"></i>&nbsp;ADD USER</a></td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>
                <td >Full Name</td>
                <td >Username</td>
                <td >User Role</td>
                <td >Status</td>
                <td >Created On</td>
                <td >Expiry Date</td>
            </tr>
        </thead>
        <tbody style="border-color: #76323F">
            <?php
            $index = 1;
            foreach ($listofuser as $key => $value) {
                $bgcolor = MysqlConnection::generateBgColor($index);
                ?>
                <tr id="<?php echo $value["user_id"] ?>" style="background-color: <?php echo $bgcolor ?>"  class="context-menu-one">
                    <td><?php echo $index ?></td>
                    <td ><?php echo $value["firstName"] ?>&nbsp;<?php echo $value["lastName"] ?></td>
                    <td ><?php echo $value["email"] ?>&nbsp;&nbsp;</td>
                    <td ><?php echo MysqlConnection::getGenericById($value["roleName"])["name"] ?>&nbsp;&nbsp;</td>
                    <td ><?php echo $value["status"] ?></td>
                    <td ><?php echo $value["createdDate"] ?></td>
                    <td ><?php echo $value["expiryDate"] ?></td>

                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
</div> 
<script type="text/javascript">
    $(function() {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function(key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_user":
                        window.location = "index.php?pagename=view_usermanagement&userid=" + id;
                        break;
                    case "create_user":
                        window.location = "index.php?pagename=create_usermanagement";
                        break;
                    case "edit_user":
                        window.location = "index.php?pagename=create_usermanagement&userid=" + id;
                        break;
                    case "delete_user":
                        window.location = "index.php?pagename=view_usermanagement&userid=" + id + "&flag=yes";
                        break;
                    case "view_history":
                        window.location = "index.php?pagename=history_usermanagement&userid=" + id;
                        break;

                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;

                    default:
                        window.location = "index.php?pagename=manage_usermanagement";
                }
                //window.console && console.log(m) || alert(m+"    id:"+id); 
            },
            items: {
                "view_user": {name: "VIEW USER", icon: ""},
                "edit_user": {name: "EDIT USER", icon: ""},
                "delete_user": {name: "DELETE USER", icon: ""}
            }
        });

        //        $('.context-menu-one').on('click', function(e){
        //            console.log('clicked', this);
        //       })    
    });
    $('tr').dblclick(function() {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_usermanagement&userid=" + id;
        }
    });
</script>