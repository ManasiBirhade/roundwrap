<?php
if (isset($_POST["btnSubmitFullForm"])) {

    $old = md5(filter_input(INPUT_POST, "oldpassword"));
    $new = md5(filter_input(INPUT_POST, "newpassword"));
    $reset = md5(filter_input(INPUT_POST, "repassword"));

    if ($new == $reset) {
        $result = MysqlConnection::fetchCustom("SELECT user_id FROM user_master WHERE username='" . $_SESSION["user"]["username"] . "' AND password = '$old' ");
        if (count($result) != 0) {
            MysqlConnection::delete("UPDATE user_master SET  password = '$new' WHERE  username='" . $_SESSION["user"]["username"] . "'");
            header("location:index.php?pagename=manage_dashboard");
        } else {
            $error = "Invalid old password";
        }
    } else {
        $error = "Your Password Confirmation did not match your password";
    }
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">UPDATE YOUR PASSWORD</h5>
    </div>
    <div class="widget-box" style="border-bottom: solid 1px #CDCDCD;background-color: white">
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">User Information </a></li>
            </ul>
        </div>
        <form name="frmUserSubmit"  enctype="multipart/form-data" id="frmUserSubmit" method="post" >
            <div class="widget-content tab-content">
                <p style="color: red"><?php echo $error ?></p>

                <table class="display nowrap sortable" style="width: 80%;vertical-align: top" border="0">
                    <tr>
                        <td style="width: 15%"><label class="control-label">Old Password<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input style="width: 220px" type="password" name="oldpassword" id="oldpassword" style="width: 30%" value="<?php echo $user["firstName"] ?>" autofocus="" required="true" minlenght="2" maxlength="30" ></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><label class="control-label">New Password<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input style="width: 220px" required="true" type="password" name="newpassword"  style="width: 30%" id="newpassword" minlenght="2" maxlength="30"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr> 
                    <tr>
                        <td><label class="control-label">Confirm Password<?php echo MysqlConnection::$REQUIRED ?></label></td>
                        <td><input style="width: 220px" required="true" type="password" name="repassword" style="width: 30%" id="repassword" minlenght="2" maxlength="30"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr> 
                </table>

                <br/>
            </div>
            <div class="modal-footer ">
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SAVE</button>
                <a href="javascript:history.back()" class="btn btn-info">CANCEL</a>
            </div>
        </form>
    </div>
</div>
</div>  