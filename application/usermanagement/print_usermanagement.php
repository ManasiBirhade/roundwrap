<?php
include '../MysqlConnection.php';
$userid = filter_input(INPUT_GET, "userid");
if (isset($userid) && $userid != "") {
    $resultset = MysqlConnection::fetchCustom("SELECT lh.`accountname`, lh.`logintime`, lh.`logouttime`, lh.`duration`, lh.`ipaddress` "
                    . "FROM `tbl_login_history` lh , user_master um "
                    . "WHERE lh.accountname = um.email AND  um.user_id = '$userid' AND um.email = '" . $_SESSION["user"]["email"] . "' ");
}
?>
<style>
    table{
        border-collapse: collapse;
        width: 100%;
    }
    tr{
        height: 32px;
    }
    
</style>
<body onload="window.print()">
    <title>PRINT USER HISTORY</title>
    <br/>
    <div id="page-wrap" style="width: 99%">
        <section class="">
            <div class="scroll">
                <table id="data" border='1'>
                    <thead>
                        <tr class="header">
                            <th  style="text-align: center;width: 10px;">#</th>
                            <th >Account Name</th>
                            <th >LoginTime</th>
                            <th >LogoutTime</th>
                            <th >Duration</th>
                            <th >IP Address</th>
                        </tr>
                    </thead>
                    <tbody style="border-color: #76323F">
                        <?php
                        $index = 1;
                        foreach ($resultset as $key => $value) {
                            if ($index % 2 == 0) {
                                $bg = $even;
                            } else {
                                $bg = $odd;
                            }
                            $ts1 = strtotime($value["logintime"]);
                            $ts2 = strtotime($value["logouttime"]);
                            $seconds_diff = $ts2 - $ts1;
                            $time = ($seconds_diff / 3600);
                            ?>
                            <tr id="<?php echo $value["user_id"] ?>" style="<?php echo $bg ?>;border-bottom: solid 1px rgb(220,220,220);text-align: left;height: 35px;"  class="context-menu-one">
                                <td style="text-align: center">&nbsp;<?php echo $index ?></td>
                                <td >&nbsp;&nbsp;<?php echo $value["accountname"] ?></td>
                                <td ><?php echo $value["logintime"] ?>&nbsp;&nbsp;</td>
                                <td ><?php echo $value["logouttime"] ?>&nbsp;&nbsp;</td>
                                <td >&nbsp;&nbsp;<?php echo round(abs($time), 2)  ?> Hr</td>
                                <td ><?php echo $value["ipaddress"] ?></td>

                            </tr>
                            <?php
                            $index++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</body>