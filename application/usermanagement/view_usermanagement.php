<?php
$userid = filter_input(INPUT_GET, "userid");
$flag = filter_input(INPUT_GET, "flag");

$user = getUserFronLogin();
$arrmaster = getUserMenus("master");
$arrretial = getUserMenus("retial");
$arrproduction = getUserMenus("production");
$arrsystem = getUserMenus("system");

$user = $_SESSION["user"];
unset($user["email"]);
unset($user["firstName"]);
unset($user["lastName"]);
unset($user["user_id"]);

if (isset($_POST["deleteUser"])) {
    $userId1 = $_POST["user_id"];
    MysqlConnection::delete("DELETE FROM `user_master` WHERE user_id = '$userId1' ");
    header("location:index.php?pagename=manage_usermanagement");
}

if (!empty($userid)) {
    $resultset = MysqlConnection::fetchCustom("SELECT * FROM user_master WHERE user_id =  '$userid' ");
    $user = $resultset[0];

    $result = MysqlConnection::getGenericById($user["roleName"]);
}
?>
<div class="container-fluid" id="tabs">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">VIEW USER</h5>
    </div>
    <?php
    if ($flag == "yes") {
        echo MysqlConnection::$DELETE;
    }
    ?>
    <div class="widget-box" style="border-bottom: solid 1px #CDCDCD;background-color: white">
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">User Information </a></li>
            </ul>
        </div>

        <div class="widget-content tab-content">
            <table  style="width: 100%;vertical-align: top;background-color: white" border="0" class="display nowrap sortable table-bordered" >
                <tr>

                    <td><label class="control-label">First Name</label></td>
                    <td><input type="text" name="firstName" id="firstname" readonly="" value="<?php echo $user["firstName"] ?>" autofocus="" required="true" minlenght="2" maxlength="30" ></td>
                    <td><label class="control-label">Last Name</label></td>
                    <td><input type="text" name="lastName" id="lastname" readonly="" value="<?php echo $user["lastName"] ?>" minlenght="2" maxlength="30" ></td>
                    <td><label class="control-label">Designation</label></td>
                    <td><input type="text" name="roleName" id="roleName" readonly="" value="<?php echo $result["name"] ?>" ></td>
                </tr>
                <tr>
                    <td><label class="control-label">Company Name</label></td>
                    <td><input type="text" name="cmpName" readonly="" value="<?php echo $user["cmpName"] ?>" id="cmpName" minlenght="2" maxlength="30"></td>
                    <td><label class="control-label">Email</label></td>
                    <td><input type="email" name="email" id="email" readonly="" value="<?php echo $user["email"] ?>" ></td>
                    <td><label class="control-label">Phone</label></td>
                    <td><input type="text" name="phone" id="phone" readonly="" value="<?php echo trim($user["phone"]) ?>" ></td>

                </tr> 

                <tr>
                    <td><label class="control-label">Address</label></td>
                    <td><input type="text" name="streetNo" id="streetNo" readonly="" value="<?php echo $user["streetNo"] ?>" ></td>
                    <td><label class="control-label">Postal Code</label></td>
                    <td><input type="text" name="postalCode" id="postalCode" readonly="" minlenght="2" maxlength="30"  value="<?php echo $user["postalCode"] ?>" ></td>
                    <td><label class="control-label">City</label></td>
                    <td><input type="text" name="city" id="city" readonly="" minlenght="2" maxlength="30" value="<?php echo $user["city"] ?>" ></td>
                </tr>
                <tr>

                    <td><label class="control-label">Province</label></td>
                    <td><input type="text" name="province" id="cust_province" readonly="" minlenght="2" maxlength="30" value="<?php echo $user["province"] ?>" ></td>
                    <td><label class="control-label">Country</label></td>
                    <td><input type="text" name="country" id="country" readonly="" minlenght="2" maxlength="30" value="<?php echo $user["country"] ?>" ></td>
                    <td></td>
                    <td></td>
                </tr>

            </table>
            <hr/>
            <table   id="data" style="width: 100%;" > 
                <tr style="background-color: #A9CDEC;height: 30px;">
                    <td style="color: white">&nbsp;MASTER</td>
                    <td style="color: white">&nbsp;RETAIL</td>
                    <td style="color: white">&nbsp;PRODUCTION</td>
                    <td style="color: white">&nbsp;SYSTEM</td>
                </tr>
                <?php
                $settingArray = getSettingArray();
                $master = $settingArray["master"];
                $retail = $settingArray["retail"];
                $production = $settingArray["production"];
                $system = $settingArray["system"];
                ?>
                <tr style="height: 27px;">
                    <td style="vertical-align: top">
                        <table>
                            <?php
                            $index = 1;
                            foreach ($master as $key => $value) {
                                if (in_array("master$$$value", $arrmaster)) {
                                    $checked1 = "checked";
                                } else {
                                    $checked1 = "";
                                }
                                ?>
                                <tr style="height: 27px;">
                                    <td>&nbsp;<input <?php echo $checked1 ?> onclick="return false;" type="checkbox" name="master[]" readonly="" id="master<?php echo $index++ ?>"  value="<?php echo "master$$" . $value ?>"></td>
                                    <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                </tr>
                            <?php } ?>
                        </table>     
                    </td>

                    <td style="vertical-align: top">
                        <table>
                            <?php
                            $index1 = 1;
                            foreach ($retail as $key => $value) {
                                if (in_array("retial$$$value", $arrretial)) {
                                    $checked2 = "checked";
                                } else {
                                    $checked2 = "";
                                }
                                ?>
                                <tr style="height: 27px;">
                                    <td>&nbsp;<input <?php echo $checked2 ?> type="checkbox" readonly="" id="retail<?php echo $index1++ ?>" name="retial[]" value="<?php echo "retial$$" . $value ?>"></td>
                                    <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                </tr>
                            <?php } ?>
                        </table>     
                    </td>


                    <td style="vertical-align: top">
                        <table>
                            <?php
                            $index2 = 1;
                            foreach ($production as $key => $value) {
                                if (in_array("production$$$value", $arrproduction)) {
                                    $checked3 = "checked";
                                } else {
                                    $checked3 = "";
                                }
                                ?>
                                <tr style="height: 27px;">
                                    <td>&nbsp;<input <?php echo $checked3 ?> type="checkbox" readonly="" id="production<?php echo $index2++ ?>" name="production[]" value="<?php echo "production$$" . $value ?>"></td>
                                    <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                </tr>
                            <?php } ?>
                        </table>     
                    </td>

                    <td style="vertical-align: top">
                        <table>
                            <?php
                            $index3 = 1;
                            foreach ($system as $key => $value) {
                                if (in_array("system$$$value", $arrsystem)) {
                                    $checked4 = "checked";
                                } else {
                                    $checked4 = "";
                                }
                                ?>
                                <tr style="height: 27px;">
                                    <td>&nbsp;<input <?php echo $checked4 ?> type="checkbox" readonly="" id="system<?php echo $index3++ ?>" name="system[]" value="<?php echo "system$$" . $value ?>"></td>
                                    <td>&nbsp;<?php echo ucwords(str_replace("_", " ", $value)) ?></td>
                                </tr>
                            <?php } ?>
                        </table>     
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer ">
            <?php
            if (isset($flag) && $flag != "") {
                ?>
                <form name="frmDeleteUser" id="frmDeleteUser" method="post">
                    <a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                    <input  type="submit" value="DELETE" name="deleteUser" class="btn btn-info"/>
                    <input type="hidden" name="user_id" value="<?php echo $userid ?>"/>
                </form>
                <?php
            } else {
                echo '<a href="javascript:history.back()" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>';
            }
            ?>
        </div>
    </div> 
</div>
<script>
    $(document).ready(function($) {
        $("#phone").mask("(999) 999-9999");
        $("#fax").mask("(999) 999-9999");
    });
    function chkNumericKey(event) {
        var charCode = (event.which) ? event.which : event.keyCode;
        if ((charCode >= 48 && charCode <= 57) || charCode == 46 || charCode == 45) {
            return true;
        } else {
            return false;
        }
    }




</script>

<?php

function getUserFronLogin() {
    $userid = filter_input(INPUT_GET, "userid");
    if (!empty($userid)) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM user_master WHERE user_id =  '$userid'");
        return $resultset[0];
    }
}

function getUserMenus($type) {
    $userid = filter_input(INPUT_GET, "userid");
    if (!empty($userid)) {
        $resultset = MysqlConnection::fetchCustom("SELECT * FROM `tbl_user_menu_mapping` where mainmenu = '$type' AND userid = '$userid'  ");
        $array = array();
        foreach ($resultset as $value) {
            array_push($array, $value["mainmenu"] . "$$" . $value["submenu"]);
        }
        return $array;
    }
}

function getSettingArray() {
    $array = array();
    $array["master"] = array("item_master", "customer_master", "vendor_master", "profile_master", "preference_master", "step_integration", "tax_info_master");
    $array["retail"] = array("purchase_orders", "sales_order", "back_order");
    $array["production"] = array("packing_slip", "quotation", "work_order", "layout", "invoice");
    $array["system"] = array("user_management", "company_information", "scanner_details", "update_password", "email_setup");
    return $array;
}
