-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2018 at 11:53 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `rw`
--

-- --------------------------------------------------------

--
-- Table structure for table `company_contact`
--

CREATE TABLE `company_contact` (
  `cc_id` varchar(40) NOT NULL,
  `person_email` varchar(50) DEFAULT '-',
  `person_name` varchar(30) DEFAULT '-',
  `person_status` varchar(50) DEFAULT 'N',
  `person_phoneNo` varchar(20) DEFAULT '-',
  `cmp_id` varchar(40) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `company_master`
--

CREATE TABLE `company_master` (
  `cmp_id` varchar(40) NOT NULL,
  `city` varchar(20) DEFAULT '''-''',
  `cmp_email` varchar(50) DEFAULT '''-''',
  `cmp_fax` varchar(20) DEFAULT '''-''',
  `cmp_name` varchar(100) DEFAULT '''-''',
  `cmp_website` varchar(50) DEFAULT '''-''',
  `country` varchar(30) DEFAULT '''-''',
  `phno` varchar(20) DEFAULT '''-''',
  `postal_code` varchar(15) DEFAULT '''-''',
  `cmp_province` varchar(43) DEFAULT '''-''',
  `streetName` varchar(150) DEFAULT '''-''',
  `streetNo` varchar(50) DEFAULT '''-''',
  `province` varchar(50) DEFAULT '''-''',
  `databasename` varchar(45) DEFAULT '''-''',
  `databaseurl` varchar(500) DEFAULT '''-''',
  `dbusername` varchar(45) DEFAULT '''-''',
  `dbpassword` varchar(45) DEFAULT '''-'''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_balancepayment`
--

CREATE TABLE `customer_balancepayment` (
  `id` varchar(40) NOT NULL DEFAULT '',
  `cust_id` varchar(40) NOT NULL DEFAULT '',
  `supp_id` varchar(40) NOT NULL DEFAULT '',
  `receiptNo` varchar(20) DEFAULT '',
  `oriamt` double(10,2) NOT NULL,
  `paidDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `chequeNoDDNo` varchar(100) DEFAULT '',
  `paidAmount` double(10,2) DEFAULT '0.00',
  `balance` double(10,2) DEFAULT '0.00',
  `remark` longtext,
  `active` char(1) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_contact`
--

CREATE TABLE `customer_contact` (
  `id` varchar(40) NOT NULL,
  `person_email` varchar(50) DEFAULT '',
  `person_name` varchar(50) DEFAULT '',
  `person_status` varchar(10) DEFAULT 'Y',
  `person_phoneNo` varchar(20) DEFAULT '',
  `designation` varchar(50) NOT NULL DEFAULT '-',
  `mail` char(1) NOT NULL DEFAULT 'N',
  `cust_id` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_contact`
--

INSERT INTO `customer_contact` (`id`, `person_email`, `person_name`, `person_status`, `person_phoneNo`, `designation`, `mail`, `cust_id`) VALUES
('6c1d6ce35f1a690c563ec966517ae870', '', '', 'Y', '', '', '', 'c55c5bb6ed493c5529065542d30554cc');

-- --------------------------------------------------------

--
-- Table structure for table `customer_master`
--

CREATE TABLE `customer_master` (
  `id` varchar(40) NOT NULL DEFAULT '',
  `cust_type` varchar(40) DEFAULT '',
  `salutation` varchar(15) DEFAULT NULL,
  `firstname` varchar(45) DEFAULT '',
  `lastname` varchar(45) DEFAULT '',
  `cust_companyname` varchar(150) DEFAULT '',
  `cust_email` varchar(50) DEFAULT '',
  `cust_fax` varchar(20) DEFAULT '',
  `phno` varchar(20) DEFAULT '',
  `website` varchar(200) DEFAULT '',
  `streetName` varchar(30) DEFAULT '',
  `streetNo` varchar(20) DEFAULT '',
  `city` varchar(50) DEFAULT '',
  `cust_province` varchar(100) DEFAULT '',
  `country` varchar(50) DEFAULT '',
  `postal_code` varchar(20) DEFAULT '',
  `billto` varchar(500) DEFAULT '' COMMENT 'This is Billing address of customer',
  `shipto` varchar(500) DEFAULT '' COMMENT 'This is Shipping Address ',
  `taxInformation` varchar(40) NOT NULL DEFAULT '' COMMENT 'This is tax applicable to customer',
  `businessno` varchar(100) NOT NULL DEFAULT '',
  `certificate` varchar(500) NOT NULL DEFAULT '-' COMMENT 'This is customer tax certificate ',
  `creditlimit` int(11) NOT NULL DEFAULT '0',
  `discount` int(11) DEFAULT '0',
  `cust_accnt_no` varchar(50) DEFAULT '',
  `paymentterm` varchar(40) DEFAULT '',
  `balance` double(12,2) DEFAULT '0.00',
  `currency` varchar(45) DEFAULT '',
  `sales_person_name` char(50) DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'Y' COMMENT 'This is status of current record'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_master`
--

INSERT INTO `customer_master` (`id`, `cust_type`, `salutation`, `firstname`, `lastname`, `cust_companyname`, `cust_email`, `cust_fax`, `phno`, `website`, `streetName`, `streetNo`, `city`, `cust_province`, `country`, `postal_code`, `billto`, `shipto`, `taxInformation`, `businessno`, `certificate`, `creditlimit`, `discount`, `cust_accnt_no`, `paymentterm`, `balance`, `currency`, `sales_person_name`, `status`) VALUES
('3a6ebdb055b3b97bb6118349344826eb', '', NULL, '', '', 'Allwood Quality Kitchen Ltd.', 'info@allwoodqualitykitchens.com', '(604) 599-5343', '(604) 599-5333', '', '128th Street', '103-8377 D', 'Surrey', 'British Columbia', 'Canada', 'V3W 4G1', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y'),
('48fd6c1465e58a78267d1dd71e978c37', '', NULL, '', '', 'Affordable Cabinets Doors', 'affordablecabinets@gmail.com', '(604) 597-0982', '(604) 597-0979', '', '', '2-12468 - 82 Avenue', 'Surrey', 'Alberta', 'Canada', 'V3W 3E9', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y'),
('52394ff387b61b1896ce065ff7ef91c5', '', NULL, '', '', 'Aero Kitchen Cabinets', 'sales@aerokitchen.com', '', '(604) 559-8080', '', '3191 Thunerbird Crescent', '109', 'Vancouver', 'British Columbia', 'Canada', 'V5A 3G1', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y'),
('56c060e9fe7191a79b2b5d4e0d352769', '', NULL, '', '', 'A-Tek Kitchen & Millwork Ltd.', 'atek_kitchens@msm.com', '(604) 451-2356', '(604) 451-9233', '', 'Short Street', '5557', 'Vancouver', 'British Columbia', 'Canada', 'V5J 1M1', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y'),
('6bad9d506ff4b66ea133bf822b84dd39', '', NULL, '', '', 'One Stop Kitchens ltd.', 'onestopkitchen@yahoo.com', '(640) 502-9098', '(604) 502-9097', '', '80th Ave', '12989', 'Surrey', 'British Columbia', 'Canada', 'V3W 3B1', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y'),
('ae3b977909ecffb054686847d454390e', '', NULL, '', '', 'AJ Kitchen Cabinets Ltd', '', '(604) 596-6373', '(604) 596-6373', '', '89A Ave', '13887', 'Surrey', 'British Columbia', 'Canada', 'V3V 6K8', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y'),
('c55c5bb6ed493c5529065542d30554cc', '15f5341749e038db92fc385a0ea186hs', '', 'Best', 'Enterprises', 'A Best Enterprises', '', '(77) 7777-7777', '(77) 7777-7777', '', 'Bridgeport Rd', '130-12020', 'Richmond', 'British Columbia', 'Canada', 'V6V 1J3', 'A Best Enterprises\r\nBestEnterprises\r\n130-12020, Bridgeport Rd, Richmond, British Columbia, Canada\r\n (77) 7777-7777, (77) 7777-7777', 'A Best Enterprises\r\nBestEnterprises\r\n130-12020, Bridgeport Rd, Richmond, British Columbia, Canada\r\n (77) 7777-7777, (77) 7777-7777', 'f4651087866808edf4eaee41db45dd25', '', '-', 0, 0, '', '15f5341749e038db92fc385a0ea18s8s', 0.00, 'CAD', 'Niki', 'Y'),
('e2e8a517144b9071c31cfa81f0b0a2a4', '', NULL, '', '', 'A-1 Doors & Mouldinggs Ltd.', 'gurminder@a1doors.ca', '(604) 591-1044', '(604) 591-1044', '', '80th Avenue', '12625', 'Surrey', 'British Columbia', 'Canada', 'V3W 3A4', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y'),
('e5a60d7b7925c14c06b9bc6a8ddb65d2', '', NULL, '', '', 'Acura Classic Custom Kitchen Ltd.', 'info@accurakitchen.com', '(604) 677-6067', '(604) 562-4122', '', '86 Avenue', '12134', 'Surrey', 'British Columbia', 'Canada', 'V3W 3H7', '', '', '', '', '-', 0, 0, '', '', 0.00, '', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `customer_notes`
--

CREATE TABLE `customer_notes` (
  `id` varchar(40) NOT NULL,
  `note` text,
  `adddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'This is date of adding note',
  `cust_id` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer_payment`
--

CREATE TABLE `customer_payment` (
  `id` varchar(40) NOT NULL,
  `cardnumber` varchar(50) DEFAULT '',
  `nameoncard` varchar(50) DEFAULT '',
  `expdate` varchar(20) DEFAULT 'Y',
  `cvvno` varchar(6) DEFAULT '',
  `cust_id` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_payment`
--

INSERT INTO `customer_payment` (`id`, `cardnumber`, `nameoncard`, `expdate`, `cvvno`, `cust_id`) VALUES
('561d45458b26c91f740208a0cf78d32c', '', '', '', '', 'c55c5bb6ed493c5529065542d30554cc');

-- --------------------------------------------------------

--
-- Table structure for table `email_setup`
--

CREATE TABLE `email_setup` (
  `id` varchar(40) NOT NULL DEFAULT '',
  `portnumber` int(6) NOT NULL DEFAULT '0',
  `username` varchar(45) NOT NULL DEFAULT '',
  `sendfrom` varchar(30) NOT NULL DEFAULT '',
  `sendlable` varchar(30) NOT NULL DEFAULT '',
  `subjectline` varchar(50) NOT NULL DEFAULT '',
  `hostname` varchar(30) NOT NULL DEFAULT '',
  `pswd` varchar(30) NOT NULL DEFAULT '',
  `body` longtext NOT NULL,
  `footer` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_setup`
--

INSERT INTO `email_setup` (`id`, `portnumber`, `username`, `sendfrom`, `sendlable`, `subjectline`, `hostname`, `pswd`, `body`, `footer`) VALUES
('fe622d8646b3675c96da1511071bf100', 0, '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `generic_entry`
--

CREATE TABLE `generic_entry` (
  `id` varchar(40) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `code` varchar(50) NOT NULL DEFAULT '',
  `name` varchar(50) DEFAULT '',
  `description` varchar(255) DEFAULT '',
  `active` char(1) DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generic_entry`
--

INSERT INTO `generic_entry` (`id`, `type`, `code`, `name`, `description`, `active`) VALUES
('15f5341749e038db92fc385a0ea1098s', 'imperial', '82', '2082.8', 'Value 2082.8', 'Y'),
('15f5341749e038db92fc385a0ea1111', 'dateformat', 'dd-mm-yyyy', 'Date format', 'Date format', 'Y'),
('15f5341749e038db92fc385a0ea11234', 'imperial', '14', '355.0', 'Value 355.0', 'Y'),
('15f5341749e038db92fc385a0ea11j3j', 'customer_type', 'RC', 'Retailer Customer', 'Retailer Customer', 'Y'),
('15f5341749e038db92fc385a0ea12222', 'kerfvalue', 'KV', '4.7625', 'This is kerf value update', 'Y'),
('15f5341749e038db92fc385a0ea123453', 'imperial', '52', '1320.8', 'Value 1320.8', 'Y'),
('15f5341749e038db92fc385a0ea13987', 'imperial', '48', '1219.2', 'Value 1219.2', 'Y'),
('15f5341749e038db92fc385a0ea145hs', 'representative', 'Niki', 'Niki', 'Roundwrap', 'Y'),
('15f5341749e038db92fc385a0ea154323', 'imperial', '24', '609.6', 'Value 609.6', 'Y'),
('15f5341749e038db92fc385a0ea157sh', 'imperial', '96', '2438.4', 'Value 2438.4', 'Y'),
('15f5341749e038db92fc385a0ea17c7c', 'scannerstep', 'Shipping', 'Shipping', 'Shipping', 'Y'),
('15f5341749e038db92fc385a0ea17f7f', 'scannerstep', '', 'Saw', '', 'Y'),
('15f5341749e038db92fc385a0ea17f8i', 'imperial', '72', '1828.8', 'Value 1828.8', 'Y'),
('15f5341749e038db92fc385a0ea17f9f', 'scannerstep', '', 'Pressing', '', 'Y'),
('15f5341749e038db92fc385a0ea18476', 'imperial', '32', '812.8', 'Value 812.8', 'Y'),
('15f5341749e038db92fc385a0ea186hs', 'customer_type', 'WS', 'Wholesaler', 'Person or firm that buys large quantity of goods from various producers', 'Y'),
('15f5341749e038db92fc385a0ea18s8s', 'paymentterm', 'NET-15', '15', 'Payment must be received in 15 days', 'Y'),
('15f5341749e038db92fc385a0ea197547', 'imperial', '64', '1625.6', 'Value 1625.6', 'Y'),
('15f5341749e038db92fc385a0ea19765', 'imperial', '42', '1066.8', 'Value 1066.8', 'Y'),
('15f5341749e038db92fc385a0ea19s8s', 'ipaddress', 'IP-L', '192.168.1.115', '192.168.1.115', 'Y'),
('15f5341749e038db92fc385a0ea1dfdf', 'scannerstep', '', 'Sanding', '', 'Y'),
('15f5341749e038db92fc385a0ea1ejej', 'scannerstep', '', 'Vinyl Pressing', '', 'Y'),
('15f5341749e038db92fc385a0ea1klal', 'scannerstep', '', 'Cleaning', '', 'Y'),
('15f5341749e038db92fc385a0ea1ksks', 'usertype', 'A', 'Admin', 'Admin', 'Y'),
('15f5341749e038db92fc385a0ea1kvkv', 'scannerstep', '', 'Glue', '', 'Y'),
('15f5341749e038db92fc385a0ea1lsks', 'usertype', 'M', 'Manager', 'Manager', 'Y'),
('15f5341749e038db92fc385a0ea1ovov', 'usertype', 'Admin', 'Admin', 'Admin', 'Y'),
('15f5341749e038db92fc385a0eh1ksks', 'scannerstep', '', 'CNC', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `item_master`
--

CREATE TABLE `item_master` (
  `item_id` varchar(40) NOT NULL,
  `vendorid` varchar(40) NOT NULL DEFAULT '0' COMMENT 'This is primary key of vendor table',
  `itempic` varchar(500) NOT NULL,
  `type` varchar(45) DEFAULT '',
  `subitemof` varchar(50) NOT NULL DEFAULT '',
  `item_code` varchar(250) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT '',
  `unit` varchar(20) DEFAULT '',
  `currency` varchar(20) NOT NULL DEFAULT 'CAD',
  `item_desc_purch` varchar(200) NOT NULL DEFAULT '0',
  `purch_code` varchar(50) NOT NULL DEFAULT '0',
  `purchase_rate` double(10,2) DEFAULT '0.00',
  `item_desc_sales` varchar(70) DEFAULT '',
  `sales_code` varchar(50) NOT NULL DEFAULT '0',
  `sell_rate` double(10,2) DEFAULT '0.00',
  `onhand` int(5) NOT NULL DEFAULT '0',
  `reorder` int(5) NOT NULL,
  `totalvalue` int(5) NOT NULL DEFAULT '0',
  `asof` date NOT NULL,
  `incomeaccount` varchar(200) NOT NULL DEFAULT '0',
  `assetaccount` varchar(200) NOT NULL DEFAULT '0',
  `cogsaccount` varchar(200) NOT NULL,
  `service` char(1) NOT NULL DEFAULT 'Y' COMMENT 'This is service agreement ',
  `status` char(1) NOT NULL DEFAULT 'Y' COMMENT 'This is item status',
  `total_purch_rate` double(10,2) DEFAULT '0.00',
  `total_sales_rate` double(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_master`
--

INSERT INTO `item_master` (`item_id`, `vendorid`, `itempic`, `type`, `subitemof`, `item_code`, `item_name`, `unit`, `currency`, `item_desc_purch`, `purch_code`, `purchase_rate`, `item_desc_sales`, `sales_code`, `sell_rate`, `onhand`, `reorder`, `totalvalue`, `asof`, `incomeaccount`, `assetaccount`, `cogsaccount`, `service`, `status`, `total_purch_rate`, `total_sales_rate`) VALUES
('00f3bb1419462c7a29a26115d6e818a0', '0', '', 'InventoryPart', '', '11-0791-P4GL', '', 'Each', 'CAD', 'Lamitech Hardrock Maple Gloss Finish 48x96', 'f4651087866808edf4eaee41db45dd25', 15.12, 'Lamitech Hardrock Maple Gloss Finish 48x96', 'f4651087866808edf4eaee41db45dd25', 65.00, 10, 10, 0, '2018-06-30', '0', '0', '', 'Y', 'Y', 15.88, 68.25),
('03e7a1909b087d58161bd6ba1282462f', '0', '', 'InventoryPart', '66c529ade14e7eabab8f78c7cebade0b', '001', '001 name', 'BOX', 'CAD', 'This is purchase one mre', 'f4651087866808edf4eaee41db45dd25', 150.00, 'This is purchase one mre', 'f4651087866808edf4eaee41db45dd25', 150.00, 10, 10, 10, '2018-06-30', '0', '0', '', 'Y', 'Y', 157.50, 157.50),
('07403bd2b1b181a13599a758078d4eab', '0', '', 'InventoryPart', '', '05-306683-874', '', 'Each', 'CAD', 'Edgetape Weathered Oak 15/16x1200x.5mm', 'f4651087866808edf4eaee41db45dd25', 1.00, 'Edgetape Weathered Oak 15/16x1200x.5mm', 'a3991faa194b59c94047c2e2f504c307', 84.00, 1, 1, 4, '2018-06-30', '0', '0', '', 'Y', 'Y', 1.05, 88.20),
('084598a5dc258a3c8395a6f5470c1f7c', '0', '', 'InventoryPart', '', '11-1320-P4MT', '', 'Each', 'CAD', 'Lamitech Apple 1320 Matte Finish 48x96', '0', 15.52, '', '0', 60.00, 1, 1, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('0880c2aac81b82acaf2eacf593d6536d', '0', '', '', '', '05-CP20213', '', 'Each', 'CAD', '0', '0', 27.88, 'Edgetape Crossfire 15/16x600x.5mm', '0', 55.00, 1, 1, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('09d8a1f8ccfd0646021f90e8ad01a4d7', '0', '', 'InventoryPart', '', '11-1468-P4LG', '', 'Each', 'CAD', 'Lamitech Segato Miele LG Finish 48x96', '0', 18.72, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('09de5e1c145f2620b36e1b21b03f0062', '0', '', '', '', '03-OM-90.1.12', '', 'Each', 'CAD', '0', '0', 8.50, '', '0', 14.83, 24, 30, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('10848768b7bfa43d3e55bfddbdc9a4e9', '0', '', '', '', '04-MV120', '', 'Sheet', 'CAD', '0', '0', 9.61, 'Teak Veneer 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('15b8562df69690f1b00aaa8988e5836d', '0', '', 'InventoryPart', '', '04-MV100', '', 'Sheet', 'CAD', 'White Oak Semi Straight (No. 18858)', '0', 9.61, 'White Oak Veneer 48x96', '0', 47.70, 100, 250, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('15fc3f9e6677246c8e696c6741e7b1d1', '0', '', 'InventoryPart', '', '05-CP20181', '', 'Each', 'CAD', 'Edgetape Oiled Cherry 15/16x600x.5mm', '0', 29.55, '', '0', 55.00, 1, 1, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('182c1ef20c48cd2af11fe61697d43e3a', '0', '', '', '', '05-C600252', '', 'Each', 'CAD', '0', '0', 22.60, '', '0', 55.00, 5, 5, 15, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('1a063fb086c63a1c0d9730ae5bfbf9b0', '0', '', 'InventoryPart', '', '03-OM-90.1.12', '', 'Each', 'CAD', '1 1/2\" (38mm) 90 Series 1/4\" Narrow Crown Staples 18 Guage  Omer Brand  \r\nBox Qty:  5000\r\nCase Qty:  6 Boxes', '0', 8.50, '', '0', 14.83, 24, 30, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('1a30f5cf55deccf1b594752f48704c20', '0', '', '', '', '11-1465-P4-LG', '', 'Each', 'CAD', '0', '0', 20.93, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('1d6b4e0eb91c479079fcbc2f4e6763ae', '0', '', 'InventoryPart', '', '05-C400147', '', 'Each', 'CAD', 'Edgetape Porcelain Rose 15/16x600x.5mm', '0', 1.00, '', '0', 55.00, 1, 5, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('1f64ec6253392a93bd1707f46e66e200', '0', '', 'InventoryPart', '', '05-306683-953', '', 'Each', 'CAD', 'Edgetape Decora 15/16x1200x.5mm', '0', 1.00, '', '0', 84.00, 1, 1, 8, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('22dc3d4967b84fc3d07acdb08987b7f5', '0', '', '', '', '04-MV170', '', 'Sheet', 'CAD', '0', '0', 9.92, 'Carbon Ash Veneer Sheet 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('24a6c6a5de9668729dedb7975db6978b', '0', '', 'InventoryPart', '', '11-1462-P410-LG', '', 'Each', 'CAD', 'Lamitech Rovere Fiumo LG Finish 48x120', '0', 26.16, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('24d57849d514e6b6f0be9622c3c23274', '0', '', 'InventoryPart', '', '05-303277-874-2mm', '', 'Each', 'CAD', 'Edgetape Weathered Oak 15/16x500x2mm', '0', 0.01, '', '0', 120.00, 1, 1, 31, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('26872abd6787af41a22b0c3eb8ffd499', '0', '', 'InventoryPart', '', '11-1336-P4MT', '', 'Each', 'CAD', 'Lamitech White Oak Matte Finish 48x96', '0', 15.52, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('26a0193fcbb60cf10780adbbb5e49197', '0', '', '', '', '11-2111-P48GL', '', 'Each', 'CAD', '0', '0', 12.50, '', '0', 55.00, 100, 250, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('27f4694883109c2f4a5b2a51eb894c81', '0', '', '', '', '05-CP20085-Flex', '', 'Each', 'CAD', '0', '0', 76.49, '', '0', 135.00, 1, 1, 20, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('2b7488739bd0c6faad31f41215d24795', '0', '', '', '', '11-1462-P410-LG', '', 'Each', 'CAD', '0', '0', 26.16, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('2c1b10916adb2c6a3f131bb1c5b6f173', '0', '', '', '', '05-CP40228', '', 'Roll', 'CAD', '0', '0', 29.55, 'Edgetape Wild apple 15/16x600x0.5mm', '0', 55.00, 1, 5, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('2ce93aebfb800da6175d8dbb0f626aca', '0', '', 'InventoryPart', '', '05-CP40211-Flex', '', 'Roll', 'CAD', 'Edgetape Grove Myrtle 15/16x300x3mm\r\n(4 Rolls/box)', '0', 76.50, 'Edgetape Grove Myrtle 15/16x300x3mm', '0', 165.00, 1, 3, 19, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('32edc881489b07d089bbadc66e5f6cb2', '0', '', '', '', '11-0920-P4GL', '', 'Each', 'CAD', '0', '0', 12.46, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('33f6dad90110a942b9ef601ce6e42f3b', '0', '', 'InventoryPart', '', '11-1468-P4PR', '', 'Each', 'CAD', 'Lamitech  Segato Miele PR Finish 48x96', '0', 1872.00, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('34a2816e7914635aa10a4d18cddb8fbd', '0', '', '', '', '11-0791-P4GL', '', 'Each', 'CAD', '0', '0', 15.12, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('34adb2750ca61f1a68aa0beb2582920d', '0', '', 'InventoryPart', '', '05-303277-004-2mm', '', 'Each', 'CAD', 'Edgetape Golden Oak 15/16x500x2mm', '0', 1.00, '', '0', 120.00, 1, 5, 12, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('37b026f8c10b614bd36ecd20b6e67dce', '0', '', '', '', '11-1465-P410-LG', '', 'Each', 'CAD', '0', '0', 26.16, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('3bf54bc5ef95b3c769392bdfc33e3b8c', '0', '', '', '', '05-306683-950', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 84.00, 1, 1, 8, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('3ebfeffd0a85c8f2d3b333b572a3c20c', '0', '', '', '', '05-CP20181', '', 'Each', 'CAD', '0', '0', 29.55, '', '0', 55.00, 1, 1, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('3f34f09c22fa323a4c6067e69e027342', '0', '', '', '', '11-1465-P4GL', '', 'Each', 'CAD', '0', '0', 15.74, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('4147673b6dda385745500efa3a91493c', '0', '', '', '', '05-CP20124', '', 'Each', 'CAD', '0', '0', 29.55, '', '0', 55.00, 5, 5, 7, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('4156b5945363ea57fe46e2d5c27a9060', '0', '', 'InventoryPart', '', '11-1465-P4-LG', '', 'Each', 'CAD', 'Lamitech Grey Cedar LG Finish 48X96', '0', 20.93, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('43e6819f1f353cf520d3fa63abe2b1ae', '0', '', '', '', '05-C400147', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 55.00, 1, 5, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('4912a72ad5955bd5497e6f748d6b3866', '0', '', '', '', '04-MV130', '', 'Sheet', 'CAD', '0', '0', 10.40, 'Walnut Veneer 48x96', '0', 47.70, 100, 300, 1, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('49adcbdc9b451031e0b7d8fdf2192672', '0', '', 'InventoryPart', '', '11-2111-P48GL', '', 'Each', 'CAD', 'Lamitech Polar White Gloss 48x96', '0', 12.50, '', '0', 55.00, 100, 250, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('4b0a1e671ee0dc68b1b06631e4084aba', '0', '', 'InventoryPart', '', '05-C300084', '', 'Each', 'CAD', 'Edgetape Wineberry 15/16x600x.5mm\r\n(5 Rolls/Box)', '0', 14.00, '', '0', 55.00, 1, 1, 14, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('4dc65211b355b5ab958ed6ecde0a42be', '0', '', '', '', '11-1468-P4LG', '', 'Each', 'CAD', '0', '0', 18.72, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('505168e592b098889a4e28fff319b614', '0', '', '', '', '05-CP20317', '', 'Each', 'CAD', '0', '0', 27.88, 'Edgetape Beigewood 15/16x600x.5mm', '0', 55.00, 5, 5, 8, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('50e7dd17cbf38e89c44d6157fe873a00', '0', '', '', '', '05-C100033-2mm', '', 'Each', 'CAD', '0', '0', 57.24, '', '0', 88.00, 1, 4, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('51d3a3f090f5d197199b1c03bb5e8288', '0', '', 'InventoryPart', '', '05-303277-005-2mm', '', 'Each', 'CAD', 'Edgetape Brown Oak 15/16x500x2mm', '0', 1.00, '', '0', 120.00, 1, 1, 29, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('53bb53b97f0accf2c4c0c6d32b21456e', '0', '', 'InventoryPart', '', '05-CP20317', '', 'Each', 'CAD', 'Edgetape Beigewood 15/16x600x.5mm', '0', 27.88, 'Edgetape Beigewood 15/16x600x.5mm', '0', 55.00, 5, 5, 8, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('54d7e69eff126e6e49ed861dec5cb1fd', '0', '', 'InventoryPart', '', '05-CP40156', '', 'Roll', 'CAD', 'Edgetape Lacewood 15/16x600x.05mm', '0', 22.95, 'Edgetape Lacewood 15/16x600x.05mm', '0', 55.00, 1, 5, 7, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('56a0c57317740587f1aa61fa533f2ea3', '0', '', '', '', '05-CP40211-Flex', '', 'Roll', 'CAD', '0', '0', 76.50, 'Edgetape Grove Myrtle 15/16x300x3mm', '0', 165.00, 1, 3, 19, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('56de079d078254ac5766fccf6da8e6e6', '0', '', 'InventoryPart', '', '05-CP30049', '', 'Each', 'CAD', 'Edgetape Arbor Myrtle 15/16x600x.5mm', '0', 29.55, 'Edgetape Arbor Myrtle 15/16x600x.5mm', '0', 55.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('57f81465e6cec4a411745a0d93b6b208', '0', '', '', '', '11-1468-P4PR', '', 'Each', 'CAD', '0', '0', 1872.00, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('58ce52b1fea681707ae6685c0f1b0fe5', '0', '', '', '', '11-0920-P4MT', '', 'Each', 'CAD', '0', '0', 11.84, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5c7f9e2bf46ec7a300060142dbafedb4', '0', '', 'InventoryPart', '', '05-CP40153', '', 'Roll', 'CAD', 'Edgetape Wild Cherry 15/16x600x0.5mm', '0', 29.53, 'Edgetape Wild Cherry 15/16x600x0.5mm', '0', 55.00, 1, 5, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5d06f082816b22a24365323ec56f94aa', '0', '', '', '', '05-306683-953', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 84.00, 1, 1, 8, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5d14af690756546c7e845daba446e565', '0', '', 'InventoryPart', '', '05-CP40143', '', 'Roll', 'CAD', 'Edgetape Regency Walnut 15/16x600x.5mm', '0', 29.53, 'Edgetape Regency Walnut 15/16x600x.5mm', '0', 55.00, 1, 5, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5d7afaa25588ae6b80093711314e6d62', '0', '', 'InventoryPart', '', '05-C600374', '', 'Each', 'CAD', 'Edgetape Charcoal 15/16x600x.5mm', '0', 22.60, '', '0', 55.00, 5, 5, 7, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5d7fd643978b370bc1b6fb1abc9ee7d0', '0', '', '', '', '05-303277-009-2mm', '', 'Each', 'CAD', '0', '0', 0.01, '', '0', 120.00, 1, 1, 1, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5e50a0c3517b0ebbfb3c75f7c66580c7', '0', '', '', '', '05-C100033.43', '', 'Each', 'CAD', '0', '0', 20.87, '', '0', 26.50, 5, 6, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5f58501a7735f5e8d5582a3d85bb1975', '0', '', 'InventoryPart', '', '05-306683-950', '', 'Each', 'CAD', 'Edgetape Siam Teak 15/16x1200x.5mm', '0', 1.00, '', '0', 84.00, 1, 1, 8, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('5fee6f07407a1def6ace686a8d119822', '0', '', '', '', '11-1461-P4LG', '', 'Each', 'CAD', '0', '0', 18.72, '', '0', 65.00, 10, 24, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('600f0843e22c7d638df3e19edf88cc2e', '0', '', 'InventoryPart', '', '11-1464-P4-LG', '', 'Each', 'CAD', 'Lamitech Larice Rustico Postforming Legno Finish 48X96', '0', 18.52, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('604513446ecf1f27ff6ba51d5010c27c', '0', '', 'InventoryPart', '', '04-MV200', '', 'Sheet', 'CAD', 'Walnut Semi Straight (No. 18831)', '0', 10.40, 'Silver Walnut Veneer Sheet 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('61c2424386aa2f097085059d1e6667b6', '0', '', 'InventoryPart', '', '05-CP20085-Flex', '', 'Each', 'CAD', 'Edgetape Hardrock Maple 15/16x300x3mm Flexedge', '0', 76.49, '', '0', 135.00, 1, 1, 20, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('64bbf98ad89a922f816f26c6e3c887bc', '0', '', 'InventoryPart', '', '05-C100033.43', '', 'Each', 'CAD', 'Edgetape White 3/4x1200x.5mm  (6/Box)', '0', 20.87, '', '0', 26.50, 5, 6, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('64c767b1d27c67351e9f0bdd8e125dda', '0', '', 'InventoryPart', '', '11-1465-P410-LG', '', 'Each', 'CAD', 'Lamitech  Grey Cedar LG Finish  48X120', '0', 26.16, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('66c529ade14e7eabab8f78c7cebade0b', '0', '', 'InventoryPart', '', 'ITEM001', 'Name1', 'BOX', 'CAD', 'This is purchase description', 'f4651087866808edf4eaee41db45dd25', 5.00, 'This is purchase description', 'a3991faa194b59c94047c2e2f504c307', 10.00, 10, 10, 10, '2018-06-30', '0', '0', '', 'Y', 'Y', 5.25, 10.50),
('68b56b48b961ab1e479a3984d60c21cf', '0', '', 'InventoryPart', '', '05-CP40228', '', 'Roll', 'CAD', 'Edgetape Wild apple 15/16x600x0.5mm\r\n(5 Rolls/box)', '0', 29.55, 'Edgetape Wild apple 15/16x600x0.5mm', '0', 55.00, 1, 5, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('693b61582ace108145487a6d04193d49', '0', '', '', '', '05-C600005', '', 'Each', 'CAD', '0', '0', 22.50, '', '0', 55.00, 1, 5, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('6c686d47af0bfdbd7b2fdefd09a89dc9', '0', '', '', '', '11-1469-P410LG', '', 'Each', 'CAD', '0', '0', 23.40, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('6c78d9865e1f505869290dbb693b6e8a', '0', '', '', '', '05-303277-10-2mm', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 120.00, 1, 1, 27, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('6e2d8c006713bec36b25272bab77be74', '0', '', 'InventoryPart', '', '05-C600252', '', 'Each', 'CAD', 'Edgetape 15/16x600x.5mm', '0', 22.60, '', '0', 55.00, 5, 5, 15, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('6e7912123c8c75469e6fb0cc7857a916', '0', '', 'InventoryPart', '', '11-1462-P4-PR', '', 'Each', 'CAD', 'Lamitech  Rovere Fiumo PR Finish 4x96', '0', 18.22, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('6e797fd27f6e895982a5847cb8eeba6e', '0', '', 'InventoryPart', '', '11-0920-P4GL', '', 'Each', 'CAD', 'Lamitech Almond Matte Finish 48x96', '0', 12.46, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('71e017999f68210efdd9dc4f33632de3', '0', '', 'InventoryPart', '', '05-CP40297-Flex', '', 'Roll', 'CAD', 'Edgetape Soprano 15/16x300x3mm Flex Edge\r\n(4 Rolls/box)', '0', 76.50, 'Edgetape Soprano 15/16x300x3mm Flex Edge', '0', 125.00, 1, 1, 31, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('72c7a684c09bb5fe169d21e989152b6f', '0', '', '', '', '05-C600374', '', 'Each', 'CAD', '0', '0', 22.60, '', '0', 55.00, 5, 5, 7, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('736db6a1e20fc895ffe416e5cff43b59', '0', '', '', '', '05-303277-004-2mm', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 120.00, 1, 5, 12, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('7495ab1fcc132663d009bf38c99bd71e', '0', '', 'InventoryPart', '', '05-303277-009-2mm', '', 'Each', 'CAD', 'Edgetape Dessert Recon 15/16x500x2mm', '0', 0.01, '', '0', 120.00, 1, 1, 1, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('75e280ef96412d70d6e180c5abc7b8c2', '0', '', '', '', '05-C600063', '', 'Each', 'CAD', '0', '0', 22.60, '', '0', 55.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('79f7f78a652952c5934ad1c0e9865163', '0', '', '', '', '05-CP40153', '', 'Roll', 'CAD', '0', '0', 29.53, 'Edgetape Wild Cherry 15/16x600x0.5mm', '0', 55.00, 1, 5, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('7b2451d0ded82140ce12b73d6fb836d2', '0', '', '', '', '05-CP30049', '', 'Each', 'CAD', '0', '0', 29.55, 'Edgetape Arbor Myrtle 15/16x600x.5mm', '0', 55.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('7bf68d9098811f01e64d22213ac35c77', '0', '', 'InventoryPart', '', '11-1468-P4GL', '', 'Each', 'CAD', 'Lamitech Segato Miele Gloss Finish 48x96', '0', 16.48, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('812c16e9bdc4ee1fc8935f6106d335d1', '0', '', '', '', '11-1320-P4MT', '', 'Each', 'CAD', '0', '0', 15.52, '', '0', 60.00, 1, 1, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('82e895741967d36b6ef503ed024b84c3', '0', '', '', '', '05-303277-005-2mm', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 120.00, 1, 1, 29, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('83953c7015c38d69464d38bcbe42ea82', '0', '', 'InventoryPart', '', '05-C600202', '', 'Each', 'CAD', 'Edgetape North Sea 15/16x600x.5mm', '0', 22.90, '', '0', 55.00, 5, 5, 10, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('83fdcf491607a66b187e5f4d3179c95d', '0', '', '', '', '11-1462-P4GL', '', 'Each', 'CAD', '0', '0', 15.74, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('846ba4f55ace275b4e82fe05c7692ce6', '0', '', 'InventoryPart', '', '04-MV170', '', 'Sheet', 'CAD', 'Silver Walnut Straight (No. 18889)', '0', 9.92, 'Carbon Ash Veneer Sheet 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('86d3fb0ad9b1322b34b644393c5db6a7', '0', '', '', '', '11-1465-P4MT', '', 'Each', 'CAD', '0', '0', 15.52, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('8b0bca765e7cb1614edff343b426607f', '0', '', '', '', '05-C600202', '', 'Each', 'CAD', '0', '0', 22.90, '', '0', 55.00, 5, 5, 10, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('8e36ccd3afc3370fa555d8c74fe7c296', '0', '', '', '', '02-23-145-320', '', 'Each', 'CAD', '0', '0', 54.72, 'Mirka Softflex Abrasive Pad 320 Grit (200/box)', '0', 99.38, 5, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('8e7bee5bd565649167fe48a60376858b', '0', '', 'InventoryPart', '', '05-STN 384-7/8', '', 'Each', 'CAD', 'White Edgetape 7/8x.5mmx1200', '0', 21.12, '', '0', 26.70, 50, 100, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('8ff4b79774f665cf4a4ca97b78ccc8ef', '0', '', '', '', '04-MV110', '', 'Sheet', 'CAD', '0', '0', 10.08, 'New Oak Veneer 48x96', '0', 47.70, 50, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('909a3004a8a3fc2555b6b94159cf96c7', '0', '', '', '', '11-1462-P4-PR', '', 'Each', 'CAD', '0', '0', 18.22, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('912b514a950f799b3e03b472305f20f0', '0', '', '', '', '05-CP40239', '', 'Roll', 'CAD', '0', '0', 29.55, 'Edgetape Figured Mahogany 15/16x600x0.5mm', '0', 55.00, 1, 5, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('91780f224086f229080cbe9aa3c9f500', '0', '', 'InventoryPart', '', '05-C600061-Flex', '', 'Each', 'CAD', 'Edgetape Pepper Dust 15/16x300x3mm', '0', 90.15, '', '0', 150.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('9262b14b7708a1451d6f75059f31267d', '0', '', 'InventoryPart', '', '04-MV180', '', 'Sheet', 'CAD', 'Dark Walnut Semi Straight (No. 18799)', '0', 10.40, 'Chestnut Veneer sheet 48x96', '0', 47.40, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('95f939377595e692f6dae3a393e57b26', '0', '', 'InventoryPart', '', '05-303277-006-2mm', '', 'Each', 'CAD', 'Edgetape Siam Teak 15/16x500x2mm', '0', 1.00, '', '0', 120.00, 1, 1, 29, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('96bdaa66139ed4f000f2eafd6e0c0b96', '0', '', 'InventoryPart', '', '05-C300101', '', 'Each', 'CAD', 'Edgetape Carmen Red 15/16x600x.5mm\r\n5 Rolls /box', '0', 1.00, '', '0', 55.00, 1, 5, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('9fd5f81ab00e79ee4ee1c440874d668d', '0', '', 'InventoryPart', '', '11-1462-P4LG', '', 'Each', 'CAD', 'Lamitech Rovere Fiumo LG Finish 48x96', '0', 18.72, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('a04ecef27aee28cc18e38cd20c36dda6', '0', '', 'InventoryPart', '', '05-C100033-2mm', '', 'Each', 'CAD', 'Edgetape White 15/16x300x2mm', '0', 57.24, '', '0', 88.00, 1, 4, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('a241d74c0fc7133dbddad8db67c58e0e', '0', '', 'InventoryPart', '', '05-C600063', '', 'Each', 'CAD', 'Edgetape Platinum 15/16x600x.5mm', '0', 22.60, '', '0', 55.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('a28b8bd5d06f0c63e7bde242c7b614f4', '0', '', '', '', '05-C300084', '', 'Each', 'CAD', '0', '0', 14.00, '', '0', 55.00, 1, 1, 14, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('a2923e417bb2fa8a7ebd3f3624b24940', '0', '', '', '', '05-CP30016', '', 'Each', 'CAD', '0', '0', 29.55, 'Brandy Cherry 15/16x600x.5mm', '0', 55.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('a94fb811ec17dace549cc2017b2abbd8', '0', '', '', '', '04-MV150', '', 'Sheet', 'CAD', '0', '0', 10.40, 'Wengue Veneer Sheet 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('a988acf19465a45cd762e592e6f85a42', '0', '', '', '', '11-1323-P4GL', '', 'Each', 'CAD', '0', '0', 15.74, '', '0', 65.00, 3, 5, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('aa5b65d2acc25f575487583dd1b562a2', '0', '', 'InventoryPart', '', '02-23-145-320', '', 'Each', 'CAD', 'Mirka Softflex Abrasive Pad 320 Grit (200/box)', '0', 54.72, 'Mirka Softflex Abrasive Pad 320 Grit (200/box)', '0', 99.38, 5, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('aa94db4ff748f95dc2c088bcba952a56', '0', '', '', '', '05-C600061-Flex', '', 'Each', 'CAD', '0', '0', 90.15, '', '0', 150.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('ac7291875346416f18afd32e60ac90a0', '0', '', '', '', '04-MV200', '', 'Sheet', 'CAD', '0', '0', 10.40, 'Silver Walnut Veneer Sheet 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('aec44749256d7781f5ea33ad892e0670', '0', '', 'InventoryPart', '', '04-MV120', '', 'Sheet', 'CAD', 'Teak Straight (No. 18868', '0', 9.61, 'Teak Veneer 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b0bfa141169308d8f0d565bc7d985d9e', '0', '', '', '', '05-C200138', '', 'Each', 'CAD', '0', '0', 19.91, '', '0', 55.00, 5, 5, 6, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b1018b6e04f7c44fe958440a6fa68289', '0', '', 'InventoryPart', '', '05-CP20213', '', 'Each', 'CAD', 'Edgetape Crossfire 15/16x600x.5mm', '0', 27.88, 'Edgetape Crossfire 15/16x600x.5mm', '0', 55.00, 1, 1, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b2fee0663225a8e23572d286bea470d3', '0', '', 'InventoryPart', '', '05-303277-11-2mm', '', 'Each', 'CAD', 'Edgetape Decora 15/16x500x2mm', '0', 1.00, '', '0', 120.00, 1, 1, 20, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b4274a90b4bebd5da6bcbe37a3c29f94', '0', '', 'InventoryPart', '', '05-CP30056', '', 'Each', 'CAD', 'Edgetape Wild Cherry 15/16x600x.5mm', '0', 29.55, 'Edge Tape Wild Cherry 15/16x600x.5mm', '0', 55.00, 1, 1, 6, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b57f3ff4819668986d4704fe1da5f91c', '0', '', '', '', '11-CD-1205', '', 'Each', 'CAD', '0', '0', 23.40, '', '0', 30.50, 5, 20, 10, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b64e93237f54592611bf70ea99f1649d', '0', '', '', '', '11-1468-P4GL', '', 'Each', 'CAD', '0', '0', 16.48, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b749533595d1f23616cebe56091267e1', '0', '', 'InventoryPart', '', '11-1465-PDL-410', '', 'Each', 'CAD', 'Lamitech  Grey Cadar Deep Line  Finsh  48x120', '0', 26.16, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b83fafe7a1aeb9bbbf5a76785b9743e3', '0', '', 'InventoryPart', '', '05-CP40239', '', 'Roll', 'CAD', 'Edgetape Figured Mahogany 15/16x600x0.5mm\r\n(5 rolls/box)', '0', 29.55, 'Edgetape Figured Mahogany 15/16x600x0.5mm', '0', 55.00, 1, 5, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('b888036f8f48b40bc386222e24e23947', '0', '', '', '', '04-MV180', '', 'Sheet', 'CAD', '0', '0', 10.40, 'Chestnut Veneer sheet 48x96', '0', 47.40, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('bc30f291965d526886ed12213f47ab70', '0', '', '', '', '11-1464-P4-LG', '', 'Each', 'CAD', '0', '0', 18.52, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('bc8288c7eacedf31126eb383b900a7c4', '0', '', 'InventoryPart', '', '11-CD-1205', '', 'Each', 'CAD', 'Lamination', '0', 23.40, '', '0', 30.50, 5, 20, 10, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('bf2bd3bf1cfc601275cd68ce5b49b5be', '0', '', '', '', '05-C600001-2mm', '', 'Each', 'CAD', '0', '0', 57.24, '', '0', 88.00, 1, 2, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('bf5f9085d0b71fce5e2d54770014b162', '0', '', 'InventoryPart', '', '11-0920-P4MT', '', 'Each', 'CAD', 'Lamitech Almond Matte Finish 48x96', '0', 11.84, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('bf9afcc8c6342888c277069e44aa9456', '0', '', '', '', '11-1462-P4LG', '', 'Each', 'CAD', '0', '0', 18.72, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('c006fa3d1f7b639566ca986d760afac2', '0', '', '', '', '05-CP40228', '', 'Roll', 'CAD', '0', '0', 29.55, 'Edgetape Wild apple 15/16x600x0.5mm', '0', 55.00, 1, 5, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('c161acfc7c0d743797e121798d053db0', '0', '', '', '', '05-CP30056', '', 'Each', 'CAD', '0', '0', 29.55, 'Edge Tape Wild Cherry 15/16x600x.5mm', '0', 55.00, 1, 1, 6, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('c20974b0df5f186f0779f498bbc5c7c9', '0', '', '', '', '04-MV100', '', 'Sheet', 'CAD', '0', '0', 9.61, 'White Oak Veneer 48x96', '0', 47.70, 100, 250, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('c4f6ed41298e6b68952b2348ea7f833e', '0', '', 'InventoryPart', '', '04-MV130', '', 'Sheet', 'CAD', 'Light Walnut Semi Straight (No. 18826)', '0', 10.40, 'Walnut Veneer 48x96', '0', 47.70, 100, 300, 1, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('c78ec594353e94b7aa93d6637678fa6c', '0', '', '', '', '05-CP40156', '', 'Roll', 'CAD', '0', '0', 22.95, 'Edgetape Lacewood 15/16x600x.05mm', '0', 55.00, 1, 5, 7, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('c8a54c59982275865c8908ab76c3fdd2', '0', '', 'InventoryPart', '', '11-2111-P48MT', '', 'Each', 'CAD', 'Lamitech Polar White Matte 48x96', '0', 12.50, '', '0', 55.00, 100, 250, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('ca3fa7f99a866528ece8c12d22787e4e', '0', '', '', '', '05-STN 384-7/8', '', 'Each', 'CAD', '0', '0', 21.12, '', '0', 26.70, 50, 100, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('cb5d971f411ff235b4871ee72e99e10f', '0', '', 'InventoryPart', '', '05-CP20124', '', 'Each', 'CAD', 'Edgetape Tan Echo 15/16x600x.5mm', '0', 29.55, '', '0', 55.00, 5, 5, 7, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('cb798023098af6c5b4df58bbec415b67', '0', '', '', '', '05-303277-11-2mm', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 120.00, 1, 1, 20, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('cbe46789e93670bf2580493dd77aafc4', '0', '', 'InventoryPart', '', '11-1323-P4GL', '', 'Each', 'CAD', 'Lamitech Blend Maple Gloss Finish 48x96', '0', 15.74, '', '0', 65.00, 3, 5, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('cbf7228ed858a8b9dab3ea2af411d38f', '0', '', '', '', '11-1466-P4PR', '', 'Each', 'CAD', '0', '0', 18.72, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('cdda05374f8a02aa88e33ae3724de901', '0', '', 'InventoryPart', '', '05-303277-10-2mm', '', 'Each', 'CAD', 'Edgetape Viking Tape 15/16x500x2mm', '0', 1.00, '', '0', 120.00, 1, 1, 27, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('cfee54dcc1fae25f0282ec31bd0fad93', '0', '', '', '', '11-0791-P4PL', '', 'Each', 'CAD', '0', '0', 14.42, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d189716988adbed4194081cd1693df13', '0', '', 'InventoryPart', '', '11-1461-P4LG', '', 'Each', 'CAD', 'Lamitech Larice 3D LG Finish 48x96', '0', 18.72, '', '0', 65.00, 10, 24, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d25e405d1fa69e93ed721c0df2bd1bbc', '0', '', '', '', '11-2111-P48MT', '', 'Each', 'CAD', '0', '0', 12.50, '', '0', 55.00, 100, 250, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d347aa5737db9f2ae1cd42c5f23a05d5', '0', '', 'InventoryPart', '', '11-0791-P4PL', '', 'Each', 'CAD', 'Lamitech Hardrock Maple PL Finish 48x96', '0', 14.42, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d38347908857f107c9e0c182d55f5e17', '0', '', 'InventoryPart', '', '11-1465-P4GL', '', 'Each', 'CAD', 'Lamitech Grey Cedar Gloss Finish 48x96', '0', 15.74, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d3cf8a570f5b07effd3601e791238781', '0', '', '', '', '05-303277-006-2mm', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 120.00, 1, 1, 29, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d57e10d4ea0d66ba7cdafdb70d017d95', '0', '', '', '', '05-C300101', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 55.00, 1, 5, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d5a6e42d487cae220dfbc4d91f2d5b67', '0', '', 'InventoryPart', '', '11-1466-P4PR', '', 'Each', 'CAD', 'Lamitech Zebra Ebano  PR Finish 48x96', '0', 18.72, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d67917cb7c03eeea971945325db2b992', '0', '', '', '', '05-306683-874', '', 'Each', 'CAD', '0', '0', 1.00, '', '0', 84.00, 1, 1, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d76c57bbe9487b9d6f321719b14a0d56', '0', '', 'InventoryPart', '', '05-C600001-2mm', '', 'Each', 'CAD', 'Edgetape Black 15/16x300x2mm\r\n(4 rolls/box)', '0', 57.24, '', '0', 88.00, 1, 2, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('d9e523ca74f559bc21b0f3c593ae53e5', '0', '', 'InventoryPart', '', '05-CP40228', '', 'Roll', 'CAD', 'Edgetape Wild apple 15/16x600x0.5mm\r\n(5 Rolls/box)', '0', 29.55, 'Edgetape Wild apple 15/16x600x0.5mm', '0', 55.00, 1, 5, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('dbde7d4319b0036a77a732fc4abd1140', '0', '', '', '', '11-1464-P4MT', '', 'Each', 'CAD', '0', '0', 15.52, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('de17e1a9eb198d7df6e3bbc53eb82e59', '0', '', '', '', '05-303277-874-2mm', '', 'Each', 'CAD', '0', '0', 0.01, '', '0', 120.00, 1, 1, 31, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('de97620f3bf24f6f6eaa0c5fcfe76635', '0', '', 'InventoryPart', '', '05-C200138', '', 'Each', 'CAD', 'Edgetape  Haze 15/16x600x.5mm\r\n(5 Rolls/box)', '0', 19.91, '', '0', 55.00, 5, 5, 6, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('e14216c483f7435b2b58c28855bf3001', '0', '', 'InventoryPart', '', '11-1464-P4MT', '', 'Each', 'CAD', 'Lamitch Larice Rustice Matte Finish 48x96', '0', 15.52, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('e3e762e721bb61a7d0215fab5367a60c', '0', '', 'InventoryPart', '', '05-C600005', '', 'Each', 'CAD', 'Edgetape Graphite 15/16x600x.5mm', '0', 22.50, '', '0', 55.00, 1, 5, 5, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('e5550fd3fd151ff6059e4cb576411db9', '0', '', 'InventoryPart', '', '11-1466-P4GL', '', 'Each', 'CAD', 'Lamitech Zebra Ebano Gloss 48x96', '0', 15.74, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('e5dfa1d233fa87bf2e01c3b588a17e61', '0', '', 'InventoryPart', '', '05-CP30016', '', 'Each', 'CAD', 'Brandy Cherry 15/16x600x.5mm', '0', 29.55, 'Brandy Cherry 15/16x600x.5mm', '0', 55.00, 1, 1, 3, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('e9a8dacd27f23fc8930c5ba7d6e5466d', '0', '', 'InventoryPart', '', '11-1469-P410LG', '', 'Each', 'CAD', 'Lamitech   Segato  latte  LG Finish 48 120', '0', 23.40, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('ecf8afc09614091bf45c159dfa0b9ce0', '0', '', 'InventoryPart', '', '04-MV110', '', 'Sheet', 'CAD', 'Silver Oak Straight (No. 18694)', '0', 10.08, 'New Oak Veneer 48x96', '0', 47.70, 50, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('ee9ba1777a54b0bb97fe082b215f2868', '0', '', 'InventoryPart', '', '05-CP20567', '', 'Each', 'CAD', 'Edgetape Beigewood 15/16x600x.5mm', '0', 29.55, 'Edgetape Beigewood 15/16x600x.5mm', '0', 55.00, 5, 5, 11, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('efeb7f0e2665a41f22ed09c8840bea52', '0', '', '', '', '05-CP40143', '', 'Roll', 'CAD', '0', '0', 29.53, 'Edgetape Regency Walnut 15/16x600x.5mm', '0', 55.00, 1, 5, 4, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('f16615d9c87358144f4347ece6fae08c', '0', '', '', '', '11-1322-P4MT', '', 'Each', 'CAD', '0', '0', 15.52, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('f36b05fcd696a04cf16589b1ca94fcc0', '0', '', '', '', '11-1336-P4MT', '', 'Each', 'CAD', '0', '0', 15.52, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('f3886724ebb1e01f43847d7fbac1faeb', '0', '', '', '', '11-1465-PDL-410', '', 'Each', 'CAD', '0', '0', 26.16, '', '0', 85.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('f71baf8bd99f87e1a3f7ab41dc01f966', '0', '', 'InventoryPart', '', '11-1462-P4GL', '', 'Each', 'CAD', 'Lamitech RovereFiumo Gloss Finish 48x96', '0', 15.74, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('f78d36b4a7f9d5dddbe2ec445535dc5b', '0', '', '', '', '11-1466-P4GL', '', 'Each', 'CAD', '0', '0', 15.74, '', '0', 65.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('f837ed8ff3bc78b57fa580cf03afa155', '0', '', 'InventoryPart', '', '11-1322-P4MT', '', 'Each', 'CAD', 'Lamitech Bleached Maple Matte Finish 48x96', '0', 15.52, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('f8bcd7eb81cb16a91ce02516cd5f09a0', '0', '', '', '', '05-CP20567', '', 'Each', 'CAD', '0', '0', 29.55, 'Edgetape Beigewood 15/16x600x.5mm', '0', 55.00, 5, 5, 11, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('fafd27fc8933e9c74304e23af1208b66', '0', '', '', '', '05-CP40297-Flex', '', 'Roll', 'CAD', '0', '0', 76.50, 'Edgetape Soprano 15/16x300x3mm Flex Edge', '0', 125.00, 1, 1, 31, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('fbc85a95c68207ded3d7afa0b96908c6', '0', '', 'InventoryPart', '', '11-1465-P4MT', '', 'Each', 'CAD', 'Lamitch Grey Cedart Matte Finish 48x96', '0', 15.52, '', '0', 60.00, 10, 10, 0, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00),
('ffbc9a163b1daa1f8743d261e419f1fc', '0', '', 'InventoryPart', '', '04-MV150', '', 'Sheet', 'CAD', 'Wengue 101', '0', 10.40, 'Wengue Veneer Sheet 48x96', '0', 47.70, 100, 300, 300, '0000-00-00', '0', '0', '', 'Y', 'Y', 0.00, 0.00);

-- --------------------------------------------------------

--
-- Table structure for table `order_number`
--

CREATE TABLE `order_number` (
  `identity` char(1) NOT NULL DEFAULT 'Y',
  `retailpo` int(11) DEFAULT '1000',
  `retailso` int(11) DEFAULT '1000',
  `prodso` int(11) DEFAULT '1000',
  `prodpo` int(11) DEFAULT '1000',
  `prodquot` int(11) NOT NULL DEFAULT '1000',
  `prodwo` int(11) NOT NULL DEFAULT '1000'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_number`
--

INSERT INTO `order_number` (`identity`, `retailpo`, `retailso`, `prodso`, `prodpo`, `prodquot`, `prodwo`) VALUES
('Y', 1001, 1000, 1000, 1000, 1000, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `packslip`
--

CREATE TABLE `packslip` (
  `ps_id` varchar(40) NOT NULL,
  `po_no` varchar(50) DEFAULT '-',
  `prof_id` varchar(50) DEFAULT '-',
  `quot_id` varchar(45) DEFAULT NULL,
  `rec_date` varchar(45) DEFAULT '-',
  `req_date` varchar(45) DEFAULT '-',
  `so_no` varchar(45) DEFAULT NULL,
  `cust_id` varchar(50) DEFAULT NULL,
  `ackrecv` varchar(50) DEFAULT 'YES',
  `status` char(1) DEFAULT 'Y',
  `workOrd_Id` varchar(45) DEFAULT NULL,
  `packMove` text,
  `inCode` text,
  `outCode` text,
  `square` varchar(20) DEFAULT '0',
  `cut_tape_charge` double(10,2) DEFAULT '0.00',
  `total_pieces` int(11) DEFAULT '0',
  `total_weight` double(10,2) DEFAULT '0.00',
  `actual_fitsquare` double(10,2) DEFAULT '0.00',
  `billable_fitsquare` double(10,2) DEFAULT '0.00',
  `cut_tape_cost` double(10,2) DEFAULT '0.00',
  `total_cost` double(10,2) DEFAULT '0.00',
  `wrapped_by` varchar(45) DEFAULT '-',
  `supplier_stock` int(11) DEFAULT '0',
  `our_stock` int(11) DEFAULT '0',
  `quantity_ord` int(11) DEFAULT '0',
  `ou_date` date DEFAULT NULL,
  `laminate_ord_date` date DEFAULT NULL,
  `laminate_fta` varchar(45) DEFAULT '-',
  `laminate_recd` varchar(45) DEFAULT '-',
  `qty_used` int(7) DEFAULT '0',
  `checked_by` varchar(45) DEFAULT '-',
  `correct_design` char(1) DEFAULT 'N',
  `correct_colour` char(1) DEFAULT 'N',
  `correct_size` char(1) DEFAULT 'N',
  `correct_quantity` char(1) DEFAULT 'N',
  `isMail` char(1) DEFAULT 'N',
  `isLayout` char(1) DEFAULT 'N',
  `packingnote` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `packslipdetail`
--

CREATE TABLE `packslipdetail` (
  `psd_id` varchar(40) NOT NULL,
  `pcs` int(11) DEFAULT '0',
  `type` varchar(255) DEFAULT '-',
  `dollerPerFit` double(10,2) DEFAULT '0.00',
  `mm_w` double(10,2) DEFAULT '0.00',
  `mm_h` double(10,2) DEFAULT '0.00',
  `inch_w` double(10,2) DEFAULT '0.00',
  `inch_h` double(10,2) DEFAULT '0.00',
  `sqFeet` double(10,2) DEFAULT '0.00',
  `fract_w` varchar(45) DEFAULT NULL,
  `fract_h` varchar(45) DEFAULT NULL,
  `rtor_value` double(10,2) DEFAULT '0.00',
  `height` double(10,2) DEFAULT '0.00',
  `ps_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profile_master`
--

CREATE TABLE `profile_master` (
  `id` varchar(50) NOT NULL,
  `profile_id` varchar(50) DEFAULT NULL,
  `profile_name` varchar(45) DEFAULT '-',
  `label_name` varchar(50) DEFAULT '-',
  `label_value` varchar(45) DEFAULT '-',
  `portfolio` varchar(50) DEFAULT '-'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_item`
--

CREATE TABLE `purchase_item` (
  `id` varchar(50) NOT NULL,
  `po_id` varchar(50) DEFAULT NULL,
  `item_id` varchar(50) DEFAULT NULL,
  `qty` int(7) DEFAULT '0',
  `rqty` int(7) NOT NULL DEFAULT '0',
  `price` double(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` varchar(50) NOT NULL,
  `purchaseOrderId` varchar(30) DEFAULT NULL,
  `supplier_id` varchar(50) DEFAULT NULL,
  `shipping_address` text,
  `billing_address` varchar(255) DEFAULT '-',
  `ship_via` varchar(50) DEFAULT '-',
  `expected_date` date DEFAULT NULL,
  `added_by` varchar(50) DEFAULT NULL,
  `sub_total` double(10,2) DEFAULT '0.00',
  `total` double(10,2) DEFAULT '0.00',
  `discount` double(10,2) NOT NULL DEFAULT '0.00',
  `totalTax` double(10,2) DEFAULT NULL,
  `isOpen` char(1) DEFAULT 'Y',
  `isActive` char(1) DEFAULT 'Y',
  `remark` text,
  `purchasedate` date NOT NULL,
  `ship_charge` double(10,0) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_item`
--

CREATE TABLE `sales_item` (
  `id` varchar(50) NOT NULL,
  `so_id` varchar(50) DEFAULT NULL,
  `item_id` varchar(50) DEFAULT NULL,
  `qty` int(6) DEFAULT '0',
  `rQty` int(6) DEFAULT '0',
  `backQty` int(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `id` varchar(50) NOT NULL,
  `sono` varchar(30) NOT NULL DEFAULT '',
  `customer_id` varchar(50) DEFAULT '0',
  `shipping_address` text,
  `entrydate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `shipvia` varchar(50) DEFAULT '',
  `expected_date` date DEFAULT NULL,
  `added_by` varchar(50) DEFAULT '0',
  `sub_total` double(8,2) DEFAULT '0.00',
  `total` double(8,2) DEFAULT '0.00',
  `billTo_address` longtext,
  `isOpen` char(1) DEFAULT 'Y',
  `remark` text,
  `discount` double(8,2) DEFAULT '0.00',
  `representative` varchar(20) DEFAULT '',
  `deleteNote` varchar(255) DEFAULT NULL,
  `isBackOrder` char(1) NOT NULL DEFAULT 'N',
  `soorderdate` date NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'Y',
  `ship_charge` double(8,2) NOT NULL DEFAULT '0.00',
  `taxname` varchar(10) NOT NULL DEFAULT '',
  `taxValue` double(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_contact`
--

CREATE TABLE `supplier_contact` (
  `sc_id` varchar(50) NOT NULL,
  `person_email` varchar(255) DEFAULT '-',
  `person_name` varchar(255) DEFAULT '-',
  `person_status` varchar(255) DEFAULT '-',
  `supp_id` varchar(50) NOT NULL,
  `person_phoneNo` varchar(255) DEFAULT '',
  `designation` varchar(200) NOT NULL DEFAULT '-' COMMENT ' This is designation of company contact information'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_master`
--

CREATE TABLE `supplier_master` (
  `supp_id` varchar(50) NOT NULL,
  `salutation` varchar(10) DEFAULT '-',
  `firstname` varchar(45) DEFAULT '-',
  `lastname` varchar(45) DEFAULT '-',
  `companyname` varchar(200) DEFAULT '-',
  `supp_phoneNo` varchar(30) DEFAULT '-',
  `supp_email` varchar(50) DEFAULT NULL,
  `supp_fax` varchar(20) DEFAULT '-',
  `supp_website` varchar(50) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT '-',
  `supp_streetName` varchar(20) DEFAULT '-',
  `supp_streetNo` varchar(20) DEFAULT '-',
  `supp_city` varchar(80) DEFAULT '-',
  `supp_province` varchar(80) DEFAULT '-',
  `supp_country` varchar(50) DEFAULT '-',
  `address` varchar(250) DEFAULT '-',
  `currency` varchar(20) DEFAULT 'CAD',
  `exchange_rate` double DEFAULT '0',
  `print_onCheck` varchar(50) DEFAULT '',
  `supp_balance` double(10,2) DEFAULT '0.00',
  `status` char(1) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_master`
--

INSERT INTO `supplier_master` (`supp_id`, `salutation`, `firstname`, `lastname`, `companyname`, `supp_phoneNo`, `supp_email`, `supp_fax`, `supp_website`, `postal_code`, `supp_streetName`, `supp_streetNo`, `supp_city`, `supp_province`, `supp_country`, `address`, `currency`, `exchange_rate`, `print_onCheck`, `supp_balance`, `status`) VALUES
('5006eb62e1274188fc259da8b88ac8d1', '-', '-', '-', 'CF Rotex Inc', '(780)-465-0637', '', '(780)-468-0044', '', 'T5L 2Y3', '123 Ave NW', '14360', 'Edmonton', 'Alberta', 'Canada', '-', 'CAD', 0, 'CF Rotex Inc', 0.00, 'Y'),
('75236f02e3155846a2c69789a463d489', '-', '-', '-', 'Armson Sales & Distribution', '(604)-861-2035', 'robertdjones@shaw.ca', '(604)-882-2110', '', 'V4N 3X1', '90 Ave', '19112', 'Langley', 'British Columbia', 'Canada', '-', 'CAD', 0, 'Armson Sales & Dist', 0.00, 'Y'),
('a4f5211d1fb00c50f67b7fa6a0622973', '-', '-', '-', 'Bumper Specialties', '(800)-541-2500', 'glee@bumperspecialties.com', '(856)-345-7690', '', '08066', 'Imperial Way West', '1607', 'Deptford', 'Alberta', 'Canada', '-', 'USD', 0, 'Bumper Specialties', 0.00, 'Y'),
('b5ebef2bcc92c2a766f74d8b6b784877', '-', '-', '-', 'DekoraPur GmbH Spielburg', '(495)-035-9789', '', '', '', 'D30890', 'Barsinghausen', 'Spielburg 11 D30890', 'Barsinghhausen', 'Brandenburg', 'Germany', '-', 'EUR', 0, '', 0.00, 'Y'),
('d579c04b0062151dd5f9de6345088c2e', '-', '-', '-', 'Dural', '(604)-467-1186', 'thume@ifscos.com', '(604)-467-1139', '', 'H9P 1C9', 'Marshell Ave', '550', 'Dorval', 'Alberta', 'Canada', '-', 'CAD', 0, 'Dural', 0.00, 'Y'),
('fbc8b96915cbe2339b8ca233f4752131', '-', '-', '-', 'Dafco Filtration Group', '(604)-273-4994', '', '(604)-273-4946', '', 'V6V 1V3', 'Bridgeport Rd', '13940', 'Richmond', 'British Columbia', 'Canada', '-', 'CAD', 0, 'Dafco Filtration Group', 0.00, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_notes`
--

CREATE TABLE `supplier_notes` (
  `id` varchar(50) NOT NULL,
  `note` text,
  `adddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'This is date of adding note',
  `supp_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `taxinfo_table`
--

CREATE TABLE `taxinfo_table` (
  `id` varchar(50) NOT NULL,
  `isExempt` varchar(10) DEFAULT 'N',
  `tax` varchar(30) DEFAULT NULL,
  `taxcode` varchar(11) NOT NULL DEFAULT '-',
  `taxdescription` text NOT NULL,
  `taxname` varchar(30) NOT NULL,
  `taxvalues` double(4,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxinfo_table`
--

INSERT INTO `taxinfo_table` (`id`, `isExempt`, `tax`, `taxcode`, `taxdescription`, `taxname`, `taxvalues`) VALUES
('0980660fd5ef9c57199732054a8ba07f', '', 'BOTH', 'BOTH', 'GST/PST', 'GST/PST', 12.00),
('3c1148b882f2630e4a703471a55902ea', '', 'N', 'N', 'No Tax', 'No Tax', 0.00),
('a3991faa194b59c94047c2e2f504c307', '', 'G', 'G', 'GST', 'GST', 5.00),
('f4651087866808edf4eaee41db45dd25', '', 'P', 'P', 'PST', 'PST', 5.00);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login_history`
--

CREATE TABLE `tbl_login_history` (
  `id` varchar(50) NOT NULL,
  `accountname` varchar(50) DEFAULT 'USER',
  `logintime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logindate` date NOT NULL,
  `logouttime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(2) DEFAULT '1',
  `ipaddress` varchar(50) DEFAULT '0.0.0.0',
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_login_history`
--

INSERT INTO `tbl_login_history` (`id`, `accountname`, `logintime`, `logindate`, `logouttime`, `duration`, `ipaddress`, `date`) VALUES
('35de402f42bfd42e5f771ac8fa17f2a1', 'niki@roundwrap.com', '2018-06-27 04:46:30', '2018-06-27', '2018-06-27 01:16:30', 0, '50.64.172.64', '0000-00-00'),
('f6b307e3885b483daf7c04380cdae3f8', 'niki@roundwrap.com', '2018-06-25 20:28:51', '2018-06-26', '2018-06-25 20:28:51', 0, '::1', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_portfolioprofile`
--

CREATE TABLE `tbl_portfolioprofile` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `portfolio_name` varchar(50) DEFAULT '-',
  `profile_name` varchar(50) DEFAULT '-',
  `imgpath` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profilelabel`
--

CREATE TABLE `tbl_profilelabel` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `label_name` varchar(50) DEFAULT '-',
  `label_value` varchar(50) DEFAULT '-',
  `portfolio_id` varchar(50) DEFAULT NULL,
  `rate` double(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profile_scanner_integration`
--

CREATE TABLE `tbl_profile_scanner_integration` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `sequence` int(2) NOT NULL DEFAULT '0',
  `profileid` varchar(40) DEFAULT '0',
  `stepname` varchar(50) DEFAULT '0',
  `isavailable` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_scanner`
--

CREATE TABLE `tbl_scanner` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `scannerId` varchar(50) DEFAULT '-',
  `scannerDept` varchar(50) DEFAULT '-',
  `scannerMoveNo` varchar(1) DEFAULT '-',
  `active` char(1) DEFAULT 'Y',
  `prodId` varchar(20) DEFAULT '-',
  `prodName` varchar(20) DEFAULT '-',
  `vendorId` varchar(50) DEFAULT '-',
  `phaseId` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_menu_mapping`
--

CREATE TABLE `tbl_user_menu_mapping` (
  `id` varchar(50) NOT NULL,
  `userid` varchar(50) NOT NULL,
  `mainmenu` varchar(50) DEFAULT NULL,
  `submenu` varchar(50) DEFAULT NULL,
  `active` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_menu_mapping`
--

INSERT INTO `tbl_user_menu_mapping` (`id`, `userid`, `mainmenu`, `submenu`, `active`) VALUES
('14692ba67a06d3e5387943232e7abaf8', '6151ceac01a407f914f2294159116a15', 'system', 'scanner_details', 'Y'),
('1898bd7c4706fbb305556b69ff213699', '6151ceac01a407f914f2294159116a15', 'production', 'quotation', 'Y'),
('222b3e482b104e33e7da14205cab6290', '6151ceac01a407f914f2294159116a15', 'system', 'company_information', 'Y'),
('2d5b6b6a70793f819e19497408b0e4da', '6151ceac01a407f914f2294159116a15', 'master', 'customer_master', 'Y'),
('329346ba46377cca5b31a6015a854e8b', '6151ceac01a407f914f2294159116a15', 'production', 'work_order', 'Y'),
('46168c9c99f501c8eb331951ca47c181', '6151ceac01a407f914f2294159116a15', 'retial', 'sales_order', 'Y'),
('53e6fb88c7be16556d2fb9b766f112ab', '6151ceac01a407f914f2294159116a15', 'system', 'update_password', 'Y'),
('57a1b94ed70bef79f6570ee559f03549', '6151ceac01a407f914f2294159116a15', 'system', 'email_setup', 'Y'),
('589c6f7f2513ba0e3ea18c74039da9b0', '6151ceac01a407f914f2294159116a15', 'production', 'invoice', 'Y'),
('84de91fb5c034684f197d34df1cf5320', '6151ceac01a407f914f2294159116a15', 'master', 'step_integration', 'Y'),
('9bb593fc49021b72b43308467c67e29e', '6151ceac01a407f914f2294159116a15', 'system', 'user_management', 'Y'),
('a90e80209143a76ff96ac4e22bfad461', '6151ceac01a407f914f2294159116a15', 'master', 'preference_master', 'Y'),
('a91bb6bd00d043a099d22aec83bcad8b', '6151ceac01a407f914f2294159116a15', 'retial', 'back_order', 'Y'),
('b83a41065232f956d7cee4610d9f0156', '6151ceac01a407f914f2294159116a15', 'master', 'item_master', 'Y'),
('c31331c3442414d36810680b38add4af', '6151ceac01a407f914f2294159116a15', 'retial', 'purchase_orders', 'Y'),
('c6821538f97990ffed92b9129f46ab03', '6151ceac01a407f914f2294159116a15', 'production', 'packing_slip', 'Y'),
('d8bc8ccd1e0da57a81ede599137d4a1d', '6151ceac01a407f914f2294159116a15', 'master', 'vendor_master', 'Y'),
('d9fcdfe5db24669180257c0889aa289c', '6151ceac01a407f914f2294159116a15', 'master', 'tax_info_master', 'Y'),
('e0f43c7b5e3fc89c5c31cb58e38ef34b', '6151ceac01a407f914f2294159116a15', 'master', 'profile_master', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `user_master`
--

CREATE TABLE `user_master` (
  `user_id` varchar(50) NOT NULL,
  `active` char(1) DEFAULT 'Y',
  `city` varchar(50) DEFAULT '-',
  `country` varchar(30) DEFAULT '-',
  `createdDate` date DEFAULT NULL,
  `email` varchar(50) DEFAULT '',
  `expiryDate` date DEFAULT NULL,
  `firstName` varchar(30) DEFAULT '',
  `lastName` varchar(30) DEFAULT '',
  `password` varchar(50) DEFAULT '',
  `phone` varchar(30) DEFAULT '',
  `postalCode` varchar(15) DEFAULT '-',
  `province` varchar(40) DEFAULT '-',
  `streetName` varchar(30) DEFAULT '-',
  `streetNo` varchar(30) DEFAULT '-',
  `username` varchar(50) DEFAULT '-',
  `cmpName` varchar(255) DEFAULT '-',
  `cmpId` int(255) DEFAULT '0',
  `roleName` varchar(45) DEFAULT '-',
  `status` char(1) NOT NULL DEFAULT 'Y',
  `isLogin` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_master`
--

INSERT INTO `user_master` (`user_id`, `active`, `city`, `country`, `createdDate`, `email`, `expiryDate`, `firstName`, `lastName`, `password`, `phone`, `postalCode`, `province`, `streetName`, `streetNo`, `username`, `cmpName`, `cmpId`, `roleName`, `status`, `isLogin`) VALUES
('6151ceac01a407f914f2294159116a15', '1', 'Richmond', 'Canada', '0000-00-00', 'niki@roundwrap.com', '0000-00-00', 'Gurnek132', 'Sandhu', '4b56abbb683b041dec6d2f108ba5f982', '(604) -278', 'V6V 3A9', 'British Columbia', 'Savage Road', '150-1680', 'niki@roundwrap.com', 'MJK Distribution Limited', 4, '15f5341749e038db92fc385a0ea1ovov', 'Y', 'Y'),
('6151ceac01a407f914f6541326116a15', '1', 'Richmond', 'Canada', '2018-06-23', 'arjan.97@roundwrap.com', '0000-00-00', 'Arjan', 'Sandhu', '123456', '(604) 729-1463', 'V6V 3A9', 'British Columbia', 'Savage Road', '150-1680', 'arjan.97@roundwrap.com', 'MJK Distribution Limited', 4, 'ADMIN', 'Y', 'N'),
('c428c7e960da259cbe412814229d0d01', 'Y', '', '', '0000-00-00', 'pravintumsare@gmail.com', '0000-00-00', 'Pravin', 'Tumsare', '974d670bc38f96462d445f06ee556c83', '', '', '', '', '', '-', 'pravintumsare@gmail.com', 0, '15f5341749e038db92fc385a0ea1lsks', 'Y', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `workorder_phase_history`
--

CREATE TABLE `workorder_phase_history` (
  `wphId` varchar(50) NOT NULL DEFAULT '',
  `cust_id` varchar(50) NOT NULL DEFAULT '',
  `packslipId` varchar(50) NOT NULL DEFAULT '',
  `workOrId` varchar(50) DEFAULT '-',
  `workInCode` varchar(50) DEFAULT '-',
  `workOutCode` varchar(50) DEFAULT '-',
  `scannerId` varchar(50) DEFAULT '-',
  `scannerCode` varchar(50) DEFAULT '-',
  `phase` varchar(50) NOT NULL DEFAULT '-',
  `phase_description` text,
  `phase_date` date NOT NULL,
  `phase_time` varchar(20) NOT NULL DEFAULT '-',
  `finished` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company_contact`
--
ALTER TABLE `company_contact`
  ADD PRIMARY KEY (`cc_id`),
  ADD UNIQUE KEY `cc_id_2` (`cc_id`),
  ADD KEY `cc_id` (`cc_id`),
  ADD KEY `cmp_id` (`cmp_id`);

--
-- Indexes for table `company_master`
--
ALTER TABLE `company_master`
  ADD PRIMARY KEY (`cmp_id`),
  ADD UNIQUE KEY `cmp_id_2` (`cmp_id`),
  ADD KEY `cmp_id` (`cmp_id`);

--
-- Indexes for table `customer_balancepayment`
--
ALTER TABLE `customer_balancepayment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `cust_id` (`cust_id`),
  ADD KEY `supp_id` (`supp_id`);

--
-- Indexes for table `customer_contact`
--
ALTER TABLE `customer_contact`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `cust_id` (`cust_id`);

--
-- Indexes for table `customer_master`
--
ALTER TABLE `customer_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `cust_type` (`cust_type`),
  ADD KEY `paymentterm` (`paymentterm`),
  ADD KEY `taxInformation` (`taxInformation`);

--
-- Indexes for table `customer_notes`
--
ALTER TABLE `customer_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `cust_id` (`cust_id`);

--
-- Indexes for table `customer_payment`
--
ALTER TABLE `customer_payment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `cust_id` (`cust_id`);

--
-- Indexes for table `email_setup`
--
ALTER TABLE `email_setup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `generic_entry`
--
ALTER TABLE `generic_entry`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `item_master`
--
ALTER TABLE `item_master`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_id` (`item_id`),
  ADD KEY `item_id_2` (`item_id`),
  ADD KEY `vendorid` (`vendorid`);

--
-- Indexes for table `order_number`
--
ALTER TABLE `order_number`
  ADD PRIMARY KEY (`identity`),
  ADD UNIQUE KEY `identity` (`identity`),
  ADD KEY `identity_2` (`identity`);

--
-- Indexes for table `packslip`
--
ALTER TABLE `packslip`
  ADD PRIMARY KEY (`ps_id`),
  ADD UNIQUE KEY `ps_id` (`ps_id`),
  ADD KEY `ps_id_2` (`ps_id`);

--
-- Indexes for table `packslipdetail`
--
ALTER TABLE `packslipdetail`
  ADD PRIMARY KEY (`psd_id`),
  ADD UNIQUE KEY `psd_id_2` (`psd_id`),
  ADD KEY `psd_id` (`psd_id`);

--
-- Indexes for table `profile_master`
--
ALTER TABLE `profile_master`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `profile_id` (`profile_id`);

--
-- Indexes for table `purchase_item`
--
ALTER TABLE `purchase_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `po_id` (`po_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD UNIQUE KEY `purchaseOrderId` (`purchaseOrderId`),
  ADD KEY `id` (`id`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `sales_item`
--
ALTER TABLE `sales_item`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `so_id` (`so_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `supplier_contact`
--
ALTER TABLE `supplier_contact`
  ADD PRIMARY KEY (`sc_id`),
  ADD UNIQUE KEY `sc_id_2` (`sc_id`),
  ADD KEY `sc_id` (`sc_id`),
  ADD KEY `supp_id` (`supp_id`);

--
-- Indexes for table `supplier_master`
--
ALTER TABLE `supplier_master`
  ADD PRIMARY KEY (`supp_id`),
  ADD UNIQUE KEY `supp_id_2` (`supp_id`),
  ADD KEY `supp_id` (`supp_id`),
  ADD KEY `supp_email` (`supp_email`);

--
-- Indexes for table `supplier_notes`
--
ALTER TABLE `supplier_notes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `supp_id` (`supp_id`);

--
-- Indexes for table `taxinfo_table`
--
ALTER TABLE `taxinfo_table`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tbl_login_history`
--
ALTER TABLE `tbl_login_history`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `tbl_portfolioprofile`
--
ALTER TABLE `tbl_portfolioprofile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `tbl_profilelabel`
--
ALTER TABLE `tbl_profilelabel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `portfolio_id` (`portfolio_id`);

--
-- Indexes for table `tbl_profile_scanner_integration`
--
ALTER TABLE `tbl_profile_scanner_integration`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `tbl_scanner`
--
ALTER TABLE `tbl_scanner`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tbl_user_menu_mapping`
--
ALTER TABLE `tbl_user_menu_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_master`
--
ALTER TABLE `user_master`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `workorder_phase_history`
--
ALTER TABLE `workorder_phase_history`
  ADD PRIMARY KEY (`wphId`),
  ADD UNIQUE KEY `wphId_2` (`wphId`),
  ADD KEY `wphId` (`wphId`),
  ADD KEY `cust_id` (`cust_id`),
  ADD KEY `packslipId` (`packslipId`),
  ADD KEY `workOrId` (`workOrId`),
  ADD KEY `scannerId` (`scannerId`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer_contact`
--
ALTER TABLE `customer_contact`
  ADD CONSTRAINT `customer_contact_ibfk_1` FOREIGN KEY (`cust_id`) REFERENCES `customer_master` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_item`
--
ALTER TABLE `purchase_item`
  ADD CONSTRAINT `purchase_item_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item_master` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales_item`
--
ALTER TABLE `sales_item`
  ADD CONSTRAINT `sales_item_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `item_master` (`item_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_item_ibfk_2` FOREIGN KEY (`so_id`) REFERENCES `sales_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_item_ibfk_3` FOREIGN KEY (`id`) REFERENCES `sales_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supplier_notes`
--
ALTER TABLE `supplier_notes`
  ADD CONSTRAINT `supplier_notes_ibfk_1` FOREIGN KEY (`supp_id`) REFERENCES `supplier_master` (`supp_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;
