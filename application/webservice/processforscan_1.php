<?php

error_reporting(0);
include '../MysqlConnection.php';
$arrget = filter_input_array(INPUT_GET);

$codenumber = trim(urldecode($arrget["codenumber"]));

//$codenumber = "1556173749";
$querypsin = "SELECT ps_id,inCode,outCode FROM `packslip` WHERE `inCode` = '$codenumber' OR `outCode` = '$codenumber'";
$resultsetpsin = MysqlConnection::fetchCustom($querypsin);

$psresut = $resultsetpsin[0];


//$psresut = $resultsetps[0];

$psid = $psresut["ps_id"];

$profileidquery = "SELECT `prof_id`,cust_id,workOrd_Id FROM `packslip` WHERE `ps_id` = '$psid'";
$profilesteps = MysqlConnection::fetchCustom($profileidquery);

$portfoliolabels = MysqlConnection::getPortfolioProfileById($profilesteps[0]["prof_id"]);
$portfolioname = $portfoliolabels["portfolio_name"];

$profilescanner = "SELECT * FROM `tbl_profile_scanner_integration` WHERE profileid = '$portfolioname' ORDER BY `sequence` ASC ";
$resultsetofsteps = MysqlConnection::fetchCustom($profilescanner);


$filterarray = array();
$filterarray[] = "PRODUCTION";

foreach ($resultsetofsteps as $value) {
    $filterarray[$value["sequence"]] = $value["stepname"];
}
$filterarray[] = "CLEAN/ PACK";
$filterarray[] = "ORDER READY";
$filterarray[] = "DELIVERY";



$checkisscan = "SELECT count(iindex) as count FROM `workorder_phase_history` WHERE `packslipId` = '$psid' and `scannerId` != '-'";

$checkscanresult = MysqlConnection::fetchCustom($checkisscan);
$stepno = $checkscanresult[0]["count"];
$checkscanresult[0]["count"];


$currentstatus = "";
foreach ($filterarray as $value1) {
    $checkforin = "SELECT iindex, workInCode, workOutCode FROM workorder_phase_history WHERE scannerCode = '$value1' AND packslipId = '$psid'";

    $resultset = MysqlConnection::fetchCustom($checkforin);
 
    if (count($resultset) != 2) {
        if (count($resultset) == 1) {
            $workcodeIn = $resultset[0]["workInCode"];
//            echo '<pre>';
//            print_r($workcode);
//            echo '<pre>';
//            echo $codenumber;
            if ($workcodeIn != $codenumber) {
                $currentstatus = processOutInsertStep($psid, $profilesteps, "OUT", $codenumber, $value1);
            } else {
                echo "Scan Outcode for $value1";
            }
            break;
        } else if (count($resultset) == 0) {
  
            if ($psresut["outCode"] != $codenumber) {
                $currentstatus = processInInsertStep($psid, $profilesteps, "IN", $codenumber, $value1);
            }else{
               echo "Scan Incode for $value1"; 
            }
            
            
            break;
        }
    }
}

function processInInsertStep($psid, $profilesteps, $inorin, $codenumber, $stepname) {

    $arraytrack = array();
    $arraytrack["packslipId"] = $psid;
    $arraytrack["cust_id"] = $profilesteps[0]["cust_id"];
    $arraytrack["workOrId"] = $profilesteps[0]["workOrd_Id"];
    $arraytrack["status"] = $inorin;
    $arraytrack["workInCode"] = $codenumber;
    $arraytrack["scannerId"] = $stepname;
    $arraytrack["scannerCode"] = $stepname;
    $arraytrack["phase"] = $stepname . " " . $inorin;
    $arraytrack["phase_description"] = $stepname . " " . $inorin;
    $arraytrack["phase_date"] = date("Y-m-d");
    $arraytrack["phase_time"] = date("g:i:s");
    $arraytrack["finished"] = "N";
    MysqlConnection::insert("workorder_phase_history", $arraytrack);
    return $stepname . " " . $inorin;
}

function processOutInsertStep($psid, $profilesteps, $inorin, $codenumber, $stepname) {


    $arraytrack = array();
    $arraytrack["packslipId"] = $psid;
    $arraytrack["cust_id"] = $profilesteps[0]["cust_id"];
    $arraytrack["workOrId"] = $profilesteps[0]["workOrd_Id"];
    $arraytrack["status"] = $inorin;
    $arraytrack["workOutCode"] = $codenumber;
    $arraytrack["scannerId"] = $stepname;
    $arraytrack["scannerCode"] = $stepname;
    $arraytrack["phase"] = $stepname . " " . $inorin;
    $arraytrack["phase_description"] = $stepname . " " . $inorin;
    $arraytrack["phase_date"] = date("Y-m-d");
    $arraytrack["phase_time"] = date("g:i:s");
    $arraytrack["finished"] = "Y";
    MysqlConnection::insert("workorder_phase_history", $arraytrack);
    return $stepname . " " . $inorin;
}

echo $currentstatus;
