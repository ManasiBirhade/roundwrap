<?php
$packslipId = filter_input(INPUT_GET, "packslipId");
$subjectappend = "Work Order";
$resultset = MysqlConnection::fetchCustom("SELECT sendfrom,`subjectline`,`footer`,`body` FROM `email_setup`");
$email = $resultset[0];

$packslipdetails = MysqlConnection::getPackSlipFromId($packslipId);
$customerprimary = MysqlConnection::getCustomerDetails($packslipdetails["cust_id"]);
$customerlater = MysqlConnection::getCustomerContactDetails($packslipdetails["cust_id"]);
$emailtosend = array();
foreach ($customerlater as $value) {
    array_push($emailtosend, " " . $value["person_email"] . "_(" . $value["designation"] . ")");
}
array_push($emailtosend, $customerprimary["cust_email"]);


$attachment = "download/workorder/$packslipId.pdf";

if (isset($_POST["btnSubmitFullForm"])) {
    $subjectline = filter_input(INPUT_POST, "subjectline");
    $body = filter_input(INPUT_POST, "body");
    $body = str_replace("\n", "<br/>", $body);
    $body = "<html><body><br/><br/>" . $body . "</body></html>";
    $emailtosend = explode(",", filter_input(INPUT_POST, "username"));
    foreach ($emailtosend as $value) {
        $explode = explode("_", $value);
        MysqlConnection::sendEmail(trim($explode[0]), $attachment, $subjectline, $body, "WORK ORDER");
    }

    MysqlConnection::delete("UPDATE packslip SET isWMail = 'Y' WHERE ps_id = '$packslipId'");
    $arraytrack = array();
    $arraytrack["cust_id"] = '';
    $arraytrack["packslipId"] = $packslipId;
    $arraytrack["workOrId"] = "-";
    $arraytrack["workInCode"] = "-";
    $arraytrack["workOutCode"] = "-";
    $arraytrack["scannerId"] = "-";
    $arraytrack["scannerCode"] = "-";
    $arraytrack["phase"] = "WORK ORDER EMAIL";
    $arraytrack["phase_description"] = "WORK ORDER EMAIL SENT";
    $arraytrack["phase_date"] = date("Y-m-d");
    $arraytrack["phase_time"] = date("g:i:s");
    $arraytrack["finished"] = "Y";
    MysqlConnection::insert("workorder_phase_history", $arraytrack);
    //this is tracking array

    header("location:index.php?pagename=success_packingslip&packslipid=$packslipId&flag=email&status=Y");
}
?>

<div class="container-fluid" id="tabs">

    <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
        <div class="widget-title" >
            <ul class="nav nav-tabs" >
                <li id="ciTab1" class="active"><a data-toggle="tab" href="#tab1">EMAIL WORK ORDER</a></li>
            </ul>
        </div>
        <form name="frmCmpSubmit"  enctype="multipart/form-data" id="frmCmpSubmit" method="post">
            <div class="widget-content tab-content">
                <table  style="width: 100%;vertical-align: top" border="0"  class="display nowrap sortable table-bordered">
                    <tr>
                        <td style="text-align: left;width: 80px;"><label  class="control-label" style="font-weight: bold">From</label></td>
                        <td><input style="width: 100%" type="text" readonly="" value="<?php echo $email["sendfrom"] ?>"></td>
                    </tr> 
                    <tr>
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Send To</label></td>
                        <td><input style="width: 100%"  type="text" name="username" id="username"  value="<?php echo implode(",", $emailtosend) ?>" ></td>
                    </tr>
                    <tr>
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Subject</label></td>
                        <td><input style="width: 100%"  type="text" name="subjectline" id="subjectline"  value="<?php echo trim($email["subjectline"]) . " | " . $subjectappend ?>" ></td>
                    </tr>
                    <tr style="height: 30px;vertical-align: middle">
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Attachment</label></td>
                        <td>
                            <a target="_blank" href="invoice/print_workorder.php?packslipid=<?php echo $packslipId ?>">VIEW ATTACHMENT</a>
                            <input type="hidden" name="emailpath" id="emailpath" value="<?php echo $po_no ?>">
                        </td>
                    </tr>
                    <tr >
                        <td style="text-align: left"><label class="control-label"  style="font-weight: bold">Body</label></td>
                        <td >
                            <textarea style="width: 100%;height: 200px;line-height: 22px;resize: none" name="body">&#13;&#10;&#13;&#10;<?php echo trim(strip_tags($email["body"])) ?></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btnSubmitFullForm" name="btnSubmitFullForm" class="btn btn-info">SEND</button>
                <a href="index.php?pagename=manage_workorder" class="btn btn-info">CANCEL</a>
            </div> 
        </form>
    </div>
</div> 