<?php
error_reporting(0);
$packslipid = $packslip = filter_input(INPUT_GET, "packslipId");
$packslip = MysqlConnection::getPackSlipFromId($packslip);
if ($packsliparray[0]["ackrecv"] == "NO" || $packsliparray[0]["isMail"] == "N") {
    header("location:index.php?pagename=error_workorder");
}
$customerid = $packslip["cust_id"];
$profid = $packslip["prof_id"];
$profileid = $packslip[""];
$customerdetails = MysqlConnection::fetchCustom("SELECT cust_companyname FROM `customer_master` WHERE  id = '$customerid' ");
$workordernumber = MysqlConnection::generateNumber("prodwo");

$portfolioname = MysqlConnection::getPortfolioProfileById($packslip["prof_id"]);
$profilelabel = MysqlConnection::getPortfolioLabelsByProfId($packslip["prof_id"]);

saveOrGenerateWorkOrder($workordernumber);
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="packingslip/packingslipjs.js"></script>
<script>
    $(function () {
        $("#datepicker").datepicker({
            dateFormat: '<?php echo $dateformat ?>',
            minDate: -20,
            showOn: "button",
            buttonImage: "images/calender.png",
            buttonImageOnly: true,
            buttonText: "Select date",
        });
    });
</script>
<style>
    input,textarea,select,date{ width: 90%; }
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
</style>
<form  method="post" autocomplete="off">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;">
            <div class="widget-title">
                <ul class="nav nav-tabs"><li class="active"><a data-toggle="tab" href="#tab1"> MANAGE WORK ORDER</a></li></ul>
            </div>
            <table class="display nowrap sortable" style="width: 100%">
                <tr>
                    <td>
                        <br/>
                        <div style="width: 25%;float: left">
                            <table class="display nowrap sortable" id="profiletable" style="width: 90%;border-collapse: collapse;background-color: white" border="1">
                                <tr style="font-weight: bold;height: 35px;  ">
                                    <td style="width: 130px"><b> Portfolio</b></td>
                                    <td><?php echo $portfolioname["portfolio_name"] ?></td>
                                </tr>
                                <tr style="font-weight: bold;height: 35px;  ">
                                    <td style="width: 130px"><b> Profile </b></td>
                                    <td><?php echo $portfolioname["profile_name"] ?></td>
                                </tr>
                                <?php
                                $packlebels = explode(",", $packslip["packlebels"]);
                                $packvalues = explode(",", $packslip["packvalues"]);
                                for ($inx = 0; $inx < count($packlebels); $inx++) {
                                    $expvalleb = explode("--", $packvalues[$inx]);
                                    ?> 
                                    <tr style="height: 30px;font-weight: bold">
                                        <td ><?php echo ucwords(str_replace("_", " ", $packlebels[$inx])) ?></td>
                                        <td><?php echo ucwords(trim($expvalleb[0])) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <div style="overflow: auto;height: 350px;border-bottom: solid 1px  #CDCDCD;">

                            <table class="display nowrap sortable">
                                <tr >
                                    <td style="width: 10%"><label>CUSTOMER&nbsp;DETAILS</label></td>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td><input style="width: 220px" type="text" placeholder="Customer Name" readonly="" value="<?php echo $customerdetails[0]["cust_companyname"] ?>" style="width: 90%"/></td>
                                    <td style="font-weight: bold; " ><label class="control-label">WO.&nbsp;NO</label></td>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td><input style="width: 220px" type="text"  value="<?php echo $workordernumber ?>" name="workOrd_Id" readonly=""></td>
                                </tr>

                                <tr >
                                    <td ><label>REQUIRED&nbsp;DATE<?php echo MysqlConnection::$REQUIRED ?></label></td>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td><input style="width: 220px" type="text" id='datepicker' required="true"  value="<?php echo MysqlConnection::convertToPreferenceDate($packslip["req_date"]) ?>" ></td>
                                    <td ><label >PACK&nbsp;DATE</label></td>
                                    <td>&nbsp;:&nbsp;</td>
                                    <td><input style="width: 220px" type="text"  readonly="" value="<?php echo MysqlConnection::convertToPreferenceDate($packslip["rec_date"]) ?>" ></td>
                                </tr>
                            </table>

                            <br>
                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">

                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(250,250,250)">
                                    <td style="width: 25px;"><b>#</b></td>
                                    <td style="width: 100px;"><b>Pcs</b></td>
                                    <td style="width: 350px;"><b>Type</b></td>
                                    <td style="width: 80px;"><b>(W) MM</b></td>
                                    <td style="width: 80px;"><b>(H) MM</b></td>
                                    <td style="width: 80px;"><b>(W) INCH</b></td>
                                    <td style="width: 80px;"><b>(H) INCH</b></td>
                                    <td><b>SQ-FT</b></td>
                                </tr>
                            </table>
                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">
                                <?php
                                $resultsetdetails = MysqlConnection::getPackSlipDetailsFromId($packslipid);
                                $index = 0;
                                $totalpscal = 0;
                                foreach ($resultsetdetails as $key => $value) {
                                    $totalpscal = $totalpscal + $value["pcs"];
                                    ?>
                                    <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white;height: 25px;">
                                        <td style="width: 25px"><?php echo ++$index ?></td>
                                        <td style="width: 100px"><?php echo $value["pcs"] ?></td>
                                        <td style="width: 350px;"><?php echo $value["type"] ?></td>
                                        <td style="width: 80px;"><?php echo $value["mm_w"] ?></td>
                                        <td style="width: 80px;"><?php echo $value["mm_h"] ?></td>
                                        <td style="width: 80px;"><?php echo $value["fract_w"] ?></td>
                                        <td style="width: 80px;"><?php echo $value["fract_h"] ?></td>
                                        <td><?php echo $value["sqFeet"] ?></td>
                                    </tr>
                                <?php } ?>
                            </table>

                        </div>
                    </td>
                </tr> 
            </table>
            <div class="modal-footer "> 
                <center>
                    <table  border="0">
                        <tr>
                            <td> <input type="submit" class="btn btn-info" name="submit" value="SAVE"> </td>
                            <td> <input type="submit" class="btn btn-info" name="print" value="PRINT"> </td>
                            <td> <input type="submit" class="btn btn-info" name="email" value="EMAIL"> </td>
                            <td><a href="index.php?pagename=manage_workorder" id="btnSubmitFullForm" class="btn btn-info">&nbsp;CANCEL&nbsp;</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
</form>

<?php

function saveOrGenerateWorkOrder($workordernumber) {
    $packslipid = filter_input(INPUT_GET, "packslipId");
    $submit = filter_input(INPUT_POST, "submit");
    $print = filter_input(INPUT_POST, "print");
    $email = filter_input(INPUT_POST, "email");
    if (isset($submit) || isset($print) || isset($email)) {
        $workorder["workOrd_Id"] = filter_input(INPUT_POST, "workOrd_Id");
        $workorder["inCode"] = time();
        $workorder["outCode"] = time() + 1000;
        MysqlConnection::delete("UPDATE `order_number` SET prodwo = $workordernumber WHERE identity = 'Y' ");
        MysqlConnection::edit("packslip", $workorder, " ps_id = '$packslipid' ");

        //this is tracking array
        $arraytrack = array();
        $arraytrack["cust_id"] = '';
        $arraytrack["packslipId"] = $packslipid;
        $arraytrack["workOrId"] = "-";
        $arraytrack["workInCode"] = "-";
        $arraytrack["workOutCode"] = "-";
        $arraytrack["scannerId"] = "-";
        $arraytrack["scannerCode"] = "-";
        $arraytrack["phase"] = "WORK ORDER CREATED";
        $arraytrack["phase_description"] = "WORK ORDER FOR PACKING SLIP CREATED";
        $arraytrack["phase_date"] = date("Y-m-d");
        $arraytrack["phase_time"] = date("g:i:s");
        $arraytrack["finished"] = "Y";
        MysqlConnection::insert("workorder_phase_history", $arraytrack);
        //this is tracking array

        generatePDF($packslipid);
    }

    if (isset($submit)) {
        header("location:index.php?pagename=manage_workorder");
    } else if (isset($print)) {
        $_SESSION["psid"] = $packslipid;
        header("location:index.php?pagename=manage_workorder");
    } else if (isset($email)) {
        header("location:index.php?pagename=email_workorder&packslipId=$packslipid");
    }
}

function generatePDF($packslipid) {
    include 'pdflib/workorder.php';
}
?>