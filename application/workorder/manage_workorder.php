<?php
$packslipId = filter_input(INPUT_GET, "packslipId");
$status = filter_input(INPUT_GET, "status");
$basequery = "SELECT * FROM `packslip`";
$searchPSID = filter_input(INPUT_GET, "psid");

//ALTER TABLE `packslip` ADD `shipvia` VARCHAR(50) NOT NULL DEFAULT '' AFTER `Wemaildate`;
$shipviainfo = MysqlConnection::fetchCustom("SELECT * FROM `generic_entry` WHERE `type` = 'shipvia'");
if ($status == "activeinactive") {
    $packslip = MysqlConnection::getPackSlipFromId($packslipId);
    MysqlConnection::delete("UPDATE packslip SET status = '" . ($packslip["status"] == "Y" ? "N" : "Y") . "' WHERE ps_id = '$packslipId' ");
    header("location:index.php?pagename=success_workorder&flag=$status&flagvalue=" . ($packslip["status"] == "Y" ? "In-Active" : "Active"));
}

error_reporting(0);
if ($status == "search") {
    $basequery = $basequery . " WHERE ps_id = '" . $searchPSID . "'";
    $packslip = MysqlConnection::fetchCustom($basequery . "  ORDER BY `workOrd_Id` DESC  ");
} else if ($status == "pending") {
    $sqlquery = "SELECT * FROM `packslip` WHERE workOrd_Id != ''  AND `req_date` < '" . date("Y-m-d") . "'   ORDER BY `workOrd_Id` DESC ";
    $packslip = MysqlConnection::fetchCustom($sqlquery);
} else {
    $packslip = MysqlConnection::fetchCustom("SELECT * FROM `packslip` WHERE workOrd_Id != ''  ORDER BY `workOrd_Id` DESC ");
}
$psid = $_SESSION["psid"];

if ($psid != "") {
    echo "<script language='javascript'>window.open('invoice/print_workorder.php?packslipid=$psid');</script>";
}
$_SESSION["psid"] = "";
if (filter_input(INPUT_GET, "status") == "emailworkorder") {
    header("location:index.php?pagename=email_workorder");
}

$btnSave = filter_input(INPUT_POST, "btnSave");
if ($btnSave == "SAVE") {
    $packslipId = filter_input(INPUT_POST, "modelworkorderid");
    $seqno = filter_input(INPUT_POST, "seq_no");
    $query = "UPDATE packslip SET seq_no= '$seqno' WHERE ps_id = '$packslipId' ";
    MysqlConnection::delete($query);
    generatePDF($packslipId);
    header("location:index.php?pagename=manage_workorder");
}

$btnShipvia = filter_input(INPUT_POST, "btnShipvia");
if ($btnShipvia == "SAVE") {
    $packslipId = filter_input(INPUT_POST, "modelworkorderidforship");
    $shipvia = filter_input(INPUT_POST, "shipvia");
    $query = "UPDATE packslip SET shipvia= '$shipvia' WHERE ps_id = '$packslipId' ";
    MysqlConnection::delete($query);
    generatePDF($packslipId);
    header("location:index.php?pagename=manage_workorder");
}

function generatePDF($packslipid) {
    include 'pdflib/workorder.php';
}
?>
<script src="index.js"></script>
<style type="text/css">
    .bs-example{
        margin: 50px;
    }
    .bs-example a{
        font-size: 22px;        
        text-decoration: none;
        margin: 0 10px;
        background-color: 1px solid #76323F;
    }

</style>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });


</script>
<div class="container-fluid">
    <div class="cutomheader">     
        <h5 style="font-family: verdana;font-size: 12px;">WORK ORDER LIST</h5>
    </div>
    <br/>
    <table style="width: 100%" border="0">
        <tr>
            <td style="float: left">
                <?php
                if ($status == "pending") {
                    ?>
                    <a href="index.php?pagename=manage_workorder" id="btnOrder" class="btn btn-info">VIEW ALL ORDER</a>
                    <a href="workorder/print_pendingworkorder.php?flag=pending" target="_blank" class="btn btn-info">PRINT LIST</a>
                    <?php
                } else {
                    ?>
                    <a href="index.php?pagename=manage_workorder&status=pending" id="btnOrder" class="btn btn-info">VIEW PENDING ORDERS</a>
                    <?php
                }
                ?>

            </td>
        </tr>
    </table>
    <br/>
    <table id="example" class="display nowrap sortable" style="width:100%">
        <thead>
            <tr>
                <td>#</td>
                <td>#</td>
                <td>WO No</td>
                <td>Seq. No</td>
                <td>Company Name</td>
                <td>Profile Name</td>
                <td>SO No</td>
                <td>PO No</td>
                <td>Rec Date</td>
                <td>Req Date</td>
                <td>Email??</td>
                <td>Status</td>
            </tr>
        </thead>
        <tbody>
            <?php
            $index = 1;
            foreach ($packslip as $key => $value) {

                $customername = MysqlConnection::getCustomerDetails($value["cust_id"]);
                $portfolioname = MysqlConnection::getPortfolioProfileById($value["prof_id"]);
                $bgcolor = MysqlConnection::generateBgColor($index);
                ?>
                <tr id="<?php echo $value["ps_id"] ?>" style="<?php echo $bgcolor ?>;" class="context-menu-one">
                    <td style="text-align: center">&nbsp;<?php echo $index ?></td>
                    <td style="text-align: center" title="TRACK WORK ORDER">
                        <a href="index.php?pagename=report_workorder&workorderid=<?php echo $value["ps_id"] ?>">
                            <i class="icon-eye-open"></i>
                        </a>
                    </td>
                    <td><a href="index.php?pagename=view_workorder&psid=<?php echo $value["ps_id"] ?>"><?php echo $value["workOrd_Id"] ?></a></td>
                    <td><?php echo $value["seq_no"] ?></td>
                    <td><a href="index.php?pagename=view_customermaster&customerId=<?php echo $value["cust_id"] ?>"><span  data-toggle="tooltip" data-original-title="Go To Company Info"><?php echo $customername["cust_companyname"] ?></span></a></td>
                    <td><?php echo $portfolioname["portfolio_name"] . " - " . $portfolioname["profile_name"] ?></td>
                    <td><a href="index.php?pagename=manage_packingslip&psid=<?php echo $value["ps_id"] ?>&status=search"><span  data-toggle="tooltip" data-original-title="Go To Packslip"><?php echo $value["so_no"] ?></span></a>

                    </td>
                    <td><?php echo $value["po_no"] ?></td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($value["rec_date"]) ?></td>
                    <td><?php echo MysqlConnection::convertToPreferenceDate($value["req_date"]) ?></td>
                    <td><?php echo $value["isWMail"] == "N" ? "<span class='badge badge-info'>No</span>" : "<span class='badge badge-success'>Yes</span>" ?></td>
                    <td><?php echo $value["status"] == "N" ? "<span class='badge badge-info'>InActive</span>" : "<span class='badge badge-success'>Active</span>" ?></td>
                </tr>
                <?php
                $index++;
            }
            ?>
        </tbody>
    </table>
    <hr/>
    <div id="hiddenButton">
        <div class="button-demo">
            <button type="button" id="layout" class='btn btn-info' disabled="disabled">LAYOUT</button>
            <button type="button" id="printButton" class='btn btn-info' disabled="disabled">PRINT</button>
            <button type="button" id="mailButton" class='btn btn-info' disabled="disabled" >EMAIL</button>
        </div> 
    </div>
</div>
<script>
    $('#example tbody tr').click(function (e) {
        var id = $(this).attr('id');
        if (id !== null && id !== "") {
            var demoDiv = '<div class="button-demo"> ';
            var layout = '<a href="layout/generate_layout.php?packslipid=' + id + '" target="_blank"><button type="button" class=" btn btn-info" >LAYOUT</button> </a>';
            var del = '<a href="invoice/print_workorder.php?packslipid=' + id + '" target="_blank"><button type="button" class=" btn btn-info" >PRINT</button></a>';
            var em = '<a href="index.php?pagename=email_workorder&packslipId=' + id + '&status=emailworkorder" >\n\<button type="button" class=" btn btn-info" >EMAIL</button></a>';
            var demoDivEnd = '</div> ';
            document.getElementById('hiddenButton').innerHTML = "";
            document.getElementById('hiddenButton').innerHTML = demoDiv + layout + del + em + demoDivEnd;
        } else {

        }
        $('#example tbody tr').removeClass('highlighted');
        $(this).addClass('highlighted');
    });

    $("#deleteThis").click(function () {
        var dataString = "deleteId=" + $('#deleteId').val();
        $.ajax({
            type: 'POST',
            url: 'workorder/workorder_ajax.php',
            data: dataString
        }).done(function (data) {
        }).fail(function () {
        });
        location.reload();
    });

    function setDeleteField(deleteId) {
        document.getElementById("deleteId").value = deleteId;
    }
    $("#save").click(function () {
        var json = convertFormToJSON("#basic_validate");
        $.ajax({
            type: 'POST',
            url: 'workorder/saveworkorder_ajax.php',
            data: json
        }).done(function (data) {
        }).fail(function () {
        });
        location.reload();
    });


</script>
<div id="sequencemodel" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <form class="form-horizontal" method="post"  id="workOrd_Id" novalidate="novalidate">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">×</button>
            <h3>ADD SEQUENCE NO</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" id="modelworkorderid" name="modelworkorderid">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td>Seq. No</td>
                    <td>&nbsp;:&nbsp;</td>
                    <td><input type="text" name="seq_no" id="seq_no" required="true" ></td>

                </tr>
            </table>
        </div>
        <div class="modal-footer"> 
            <input type="submit" name="btnSave" class="btn btn-info"   value="SAVE">
            <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
        </div>
    </form>
</div>

<!-- this is custom model dialog --->
<div id="shippingmodel" class="modal hide" style="top: 10%;left: 50%;width: 450px;height: auto">
    <form class="form-horizontal" method="post"  id="workOrd_Id" novalidate="novalidate">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">×</button>
            <h3>ADD SHIPPING DETAIL</h3>
        </div>
        <div class="modal-body">
            <input type="hidden" id="modelworkorderidforship" name="modelworkorderidforship">
            <table class="display nowrap sortable" style="width: 100%"> 
                <tr>
                    <td >SHIP VIA<?php echo MysqlConnection::$REQUIRED ?></td>
                    <td>&nbsp;:&nbsp</td>
                    <td>
                        <select style="width: 225px;text-transform: capitalize" name="shipvia" id="shipvia" required="true"> 
                            <option value="">&nbsp;&nbsp;</option>
                            <?php foreach ($shipviainfo as $key => $value) { ?>
                                <option <?php echo $value["name"] == $packslip["shipvia"] ? "selected" : "" ?>
                                    value="<?php echo $value["name"] ?>"  ><?php echo $value["name"] ?></option>
                                <?php } ?>
                        </select>
                    </td>
                </tr>

            </table>
        </div>
        <div class="modal-footer"> 
            <input type="submit" name="btnShipvia" class="btn btn-info"   value="SAVE">
            <a data-dismiss="modal" class="btn btn-info" id="cancelshipvia" href="#">CANCEL</a> 
        </div>
    </form>
</div>
<script type="text/javascript">

    $(function () {
        $.contextMenu({
            selector: '.context-menu-one',
            callback: function (key, options) {
                var m = "clicked row: " + key;
                var id = $(this).attr('id');
                switch (key) {
                    case "view_workorder":
                        window.location = "index.php?pagename=view_workorder&psid=" + id;
                        break;
                    case "edit_workorder":
                        window.location = "index.php?pagename=create_packingslip&packslipId=" + id + "&action=workorder";
                        break;
                    case "generate_layout":
                        window.open("layout/generate_layout.php?packslipid=" + id, '_blank');
                        break;
                    case "active_workorder":
                        window.location = "index.php?pagename=manage_workorder&packslipId=" + id + "&status=activeinactive";
                        break;
                    case "delete_workorder":
                        window.location = "index.php?pagename=view_workorder&psid=" + id + "&flag=yes";
                        break;
                    case "export_workorder":
                        window.open("workorder/export_workorder.php?psid=" + id, '_blank');
                        break;
                    case "quit":
                        window.location = "index.php?pagename=manage_dashboard";
                        break;
                    case "sequence_workorder":
                        $('#modelworkorderid').val(id);
                        $('#sequencemodel').modal('show');
                        break;
                    case "ship_detail":
                        $('#modelworkorderidforship').val(id);
                        $('#shippingmodel').modal('show');
                        break;
                    default:
                        window.location = "index.php?pagename=manage_workorder";
                }
            },
            items: {
                "view_workorder": {name: "VIEW WORK ORDER", icon: ""},
                "edit_workorder": {name: "EDIT WORK ORDER", icon: ""},
                "delete_workorder": {name: "DELETE WORK ORDER", icon: ""},
                "export_workorder": {name: "EXPORT WORK ORDER", icon: ""},
                "sep0": "---------",
                "generate_layout": {name: "GENERATE LAYOUT", icon: ""},
                "active_workorder": {name: "ACTIVE / INACTIVE", icon: ""},
                "sep1": "---------",
                "sequence_workorder": {name: "ADD SEQUENCE NO", icon: ""},
                "ship_detail": {name: "ADD SHIPPING DETAIL", icon: ""}
            }
        });
    });
    $('tr').dblclick(function () {
        var id = $(this).attr('id');
        if (id !== undefined) {
            window.location = "index.php?pagename=view_workorder&psid=" + id;
        }
    });
</script>
