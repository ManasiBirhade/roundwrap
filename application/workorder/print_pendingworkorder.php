<?php
include '../MysqlConnection.php';

$sqlquery = "SELECT * FROM `packslip` WHERE workOrd_Id != ''  AND `req_date` < '" . date("Y-m-d") . "'  ORDER BY `ps_id`  DESC ";
$packslip = MysqlConnection::fetchCustom($sqlquery);

?>
<script type="text/javascript">
<!--
    window.print();
//-->
</script>
<style type="text/css">
    <!--
    *{padding: 0px;margin: 0px;font-size: 11px;margin: 0; padding: 0;}
    body { font: 12px/1.4  Calibri,serif; }
    table { border-collapse: collapse; }
    table td, table th {  border: 0px black;  padding: 5px;  }
    #items { clear: both; width: 100%; margin: 30px 0 0 0;  }
    #items th { background: #eee; }
    table td, table th {  padding: 5px; }

    end_last_page div
    {
        height: 27mm;
        margin: 0;
        padding: 0;
        text-align: left;
        font-weight: normal;
    }
    -->
</style>
<page style="font-size: 16pt" pageset="old">
    <div class="cutomheader">
        <h5 style="font-family: verdana;font-size: 12px;">PENDING WORK ORDER LIST</h5>
    </div>
    <br/>
    <table  style="width: 100%;border: solid 1px black">
        <tr style="width: 100%;border: solid 1px black">
            <th>WO No</th>
            <th>Seq. No</th>
            <th>Company Name</th>
            <th>Profile Name</th>
            <th>SO No</th>
            <th>PO No</th>
            <th>Rec Date</th>
            <th>Req Date</th>
        </tr>
        <?php
        foreach ($packslip as $value) {
            $customername = MysqlConnection::getCustomerDetails($value["cust_id"]);
            $portfolioname = MysqlConnection::getPortfolioProfileById($value["prof_id"]);
            ?>
            <tr style="width: 100%;border: solid 1px black;text-align: center">
                <td><?php echo $value["workOrd_Id"] ?></td>
                <td><?php echo $value["seq_no"] ?></td>
                <td><?php echo $customername["cust_companyname"] ?></td>
                <td><?php echo $portfolioname["portfolio_name"] . " - " . $portfolioname["profile_name"] ?></td>
                <td><?php echo $value["so_no"] ?></td>
                <td><?php echo $value["po_no"] ?></td>
                <td><?php echo MysqlConnection::convertToPreferenceDate($value["rec_date"]) ?></td>
                <td><?php echo MysqlConnection::convertToPreferenceDate($value["req_date"]) ?></td>
            </tr>
            <?php
        }
        ?>
    </table>
</page>