<?php
$workorderid = filter_input(INPUT_GET, "workorderid");

$paclslipdetails = MysqlConnection::getPackSlipFromId($workorderid);

$customerid = $paclslipdetails["cust_id"];
$profileid = $paclslipdetails["prof_id"];

$portfoliolabels = MysqlConnection::getPortfolioProfileById($profileid);
$portfolioname = $portfoliolabels["portfolio_name"];

$style = $portfoliolabels["profile_name"];
$packlebels = explode(",", $paclslipdetails["packlebels"]);
$packvalues = explode(",", $paclslipdetails["packvalues"]);

$colorindex = "";
foreach ($packlebels as $keyp => $pvalue) {
    if ($pvalue == "Color") {
        $colorindex = $keyp;
        break;
    }
}

$activecolor = explode("--", $packvalues[$colorindex]);

$customermaster = MysqlConnection::getCustomerDetails($customerid);

$resultset = MysqlConnection::fetchCustom("SELECT * FROM `tbl_profile_scanner_integration` WHERE profileid = '$portfolioname' ORDER BY sequence ASC ");

$trackingsteps = MysqlConnection::fetchCustom("SELECT * FROM `tbl_profile_scanner_integration` WHERE profileid = '$portfolioname' ORDER BY sequence ");
$getsingle = $trackingsteps[0];
$trackingstepsformated = array();

array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "PACKSLIP ACK", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "WORK ORDER", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "PRODUCTION", "isavailable" => "Y"));
foreach ($trackingsteps as $tvalue) {
    array_push($trackingstepsformated, $tvalue);
}

array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "CLEAN/ PACK", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "ORDER READY", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "DELIVERY", "isavailable" => "Y"));



$btnSaveNotes = filter_input(INPUT_POST, "btnSaveNotes");

if (isset($btnSaveNotes) && $btnSaveNotes == "SAVE NOTE") {
    $arraynote["note"] = $note = filter_input(INPUT_POST, "note");
    $arraynote["workorderid"] = $workorderid = filter_input(INPUT_GET, "workorderid");
    $arraynote["date"] = date("Y-m-d");
    $arraynote["addedBy"] = $name = $_SESSION["user"]["firstName"] . " " . $_SESSION["user"]["lastName"];

    MysqlConnection::insert("workorder_note", $arraynote);
    header("location:index.php?pagename=report_workorder&workorderid=$workorderid");
}

$deleteNoteId = filter_input(INPUT_POST, "deleteNoteId");
if ($deleteNoteId != "") {
    MysqlConnection::delete("DELETE FROM workorder_note WHERE id = '$deleteNoteId' ");
    header("location:index.php?pagename=report_workorder&workorderid=$workorderid");
}
?>
<style>

    .reportgreen{
        width: 20px;
        height: 20px;
        border:solid 1px gray;
        border-radius: 20px;
        background-color: green;
        margin: 0 auto;
    }
    .reportred{
        width: 20px;
        height: 20px;
        border:solid 1px gray;
        border-radius: 20px;
        background-color: #C90602;
        margin: 0 auto;
    }

    .cust{
        border-collapse: collapse;
        font-size: 14px;
        font-family: 'Calibri';
        border:solid 1px #F6F6F6;
        vertical-align: middle;

    }
    .cust tr td {
        padding: 5px 5px 5px 5px;

    }
</style>
<script>

    function blink_text() {
        $('.blink').fadeOut(500);
        $('.blink').fadeIn(500);
    }
    setInterval(blink_text, 3000);
</script>

<form name="frmSubmit" id="frmSubmit" method="post">
    <div class="container-fluid"  onload="blink();">
        <div class="cutomheader" >     
            <h5 style="font-family: verdana;font-size: 12px;">WORK ORDER TRACKING -<b style="font-size: 15px;color: #000080"> <?php echo strtoupper($portfolioname) ?></b>  [<b style="font-size: 14px;color: red"><?php echo $paclslipdetails["workOrd_Id"] ?></b>]</h5>
        </div>
        <br/>
        <table class="cust" border="0" style="width: 100%">
            <tr>
                <td colspan="4"><h5><?php echo $customermaster["cust_companyname"] ?></h5></td>
            </tr>
            <tr style="border:solid 1px #F6F6F6;">
                <td  style="border:solid 1px #F6F6F6;width: 25%">
                    <p style="font-size: 15px;">Billing Address</p>
                    <p style="width: 80%"><?php echo MysqlConnection::formatToBRAddress($customermaster["billto"]) ?></p>
                </td>
                <td  style="border:solid 1px #F6F6F6;width: 25%">
                    <p  style="font-size: 15px;">Shipping Address</p>
                    <p  style="width: 80%"><?php echo MysqlConnection::formatToBRAddress($customermaster["shipto"]) ?></p>
                </td>
                <td style="vertical-align: top;border:solid 1px #F6F6F6;">
                    <table style="width: 100%;" border="0">
                        <tr>
                            <td>PO NO </td>
                            <td>: </td>
                            <td><?php echo $paclslipdetails["po_no"] ?></td>
                        </tr>
                        <tr>
                            <td>Style </td>
                            <td>: </td>
                            <td><?php echo $style ?></td>
                        </tr>
                        <tr>
                            <td>Color</td>
                            <td>: </td>
                            <td><?php echo $activecolor[0] ?></td>
                        </tr>
                    </table>
                </td>
                <td style="vertical-align: top">
                    <table style="width: 100%;" border="0">
                        <tr>
                            <td>Total Pcs</td>
                            <td>: </td>
                            <td><?php echo $paclslipdetails["total_pieces"] ?></td>
                        </tr>
                        <tr>
                            <td>Rec. Date</td>
                            <td>: </td>
                            <td><b style="color: #228B22;"><?php echo MysqlConnection::convertToPreferenceDate($paclslipdetails["rec_date"]) ?></b></td>
                        </tr>
                        <tr>
                            <td>Req. Date</td>
                            <td>: </td>
                            <td><b style="color: red"><?php echo MysqlConnection::convertToPreferenceDate($paclslipdetails["req_date"]) ?></b></td>
                        </tr>
                        <?php if ($portfolioname == "LAMINATE") { ?>
                            <tr>
                                <td>Mat. Rec. Date</td>
                                <td>: </td>
                                <td><b style="color: #000080"><?php echo MysqlConnection::convertToPreferenceDate($paclslipdetails["mat_rec_date"]) ?></b></td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
            </tr>
        </table>
        <br/> 
        <br/>
        <div style="width: 100%;overflow: auto">
            <table style="text-align: center;width: 100%;border-collapse: collapse;border: 1px solid #ddd;" border="1">
                <tr>
                    <?php
                    foreach ($trackingstepsformated as $value) {
                        $border = 1;

                        $isstepscan = isStepScan($workorderid, ucwords($value["stepname"]));
                        if ($isstepscan == 2) {
                            $bg = "background-color: #7CFC00";
                        } else if ($isstepscan == 1) {
                            $bg = "background-color: #FFFF00";
                            $border = 5;
                        } else {
                            $bg = "background-color: #C90602";
                        }

                        if ($value["stepname"] == "PACKSLIP ACK" || $value["stepname"] == "WORK ORDER") {
                            $bg = "background-color: #7CFC00";
                        }
                        ?>
                        <td style="width: 100px;">
                            <div style="width: 50px;height: 50px;border: solid <?php echo $border ?>px gray;border-radius: 50px;margin: 0 auto;<?php echo $bg ?>">

                            </div>
                        </td>
                    <?php } ?>
                </tr>
                <tr style="height: 35px">
                    <?php foreach ($trackingstepsformated as $value) { ?>
                        <td  style="width: 100px;font-size: 13px"><?php echo strtoupper($value["stepname"]) ?></td>
                    <?php } ?>
                </tr>
            </table>
        </div>


        <br/>
        <table class="display nowrap sortable" border="1" style="width: 100%">
            <thead style="text-align: center">
                <tr>
                    <td>SR.NO</td>
                    <td>STEP</td>
                    <td>DESCRIPTION</td>
                    <td>IN-DATE</td>
                    <td>OUT-DATE</td>
                    <td>DURATION</td>
                </tr>
            </thead>    
            <tbody style="text-align: center">
                <?php
                $indexindex = 1;
                $tracknigsqlsteps = "SELECT DISTINCT(phase) as phase FROM `workorder_phase_history`"
                        . " WHERE packslipId = '$workorderid' AND phase != '-' "
                        . " ORDER BY `iindex`";
                $trackingdetailsprocesssteps = MysqlConnection::fetchCustom($tracknigsqlsteps);

                $totalduration = 0;
                foreach ($trackingdetailsprocesssteps as $valuesteps) {
                    $stepv = $valuesteps["phase"];
                    $getsteps = "SELECT * FROM `workorder_phase_history`"
                            . " WHERE `packslipId` = '$workorderid'"
                            . " AND phase = '$stepv' ORDER BY iindex ASC";
                    $resultsetsteps = MysqlConnection::fetchCustom($getsteps);
                    $set1 = $resultsetsteps[0];
                    $set2 = $resultsetsteps[1];
                    ?>
                    <tr>
                        <td style="text-transform: uppercase"><?php echo $indexindex++ ?></td>
                        <td ><?php echo $set1["phase"] ?></td>
                        <td ><?php echo $set1["phase_description"] ?></td>
                        <td ><?php echo $set1["phase_date"] . " " . $set1["phase_time"] ?></td>
                        <td ><?php echo $set2["phase_date"] . " " . $set2["phase_time"] ?></td>
                        <td>
                            <?php
                            if ($set2["phase_time"] != "") {
                                $time1 = strtotime($set2["phase_time"]);  // 2012-12-06 23:56
                                $time2 = strtotime($set1["phase_time"]);  // 2012-12-06 00:21
//                                $dueration = round(abs($time1 - $time2) / 60, 2);
//                                $totalduration = $totalduration + $dueration;
//
//                                $timeTemp1 = new DateTime($set2["phase_time"]);
//
//                                $timeTemp2 = new DateTime($set2["phase_time"]);
//                                $interval = $timeTemp2->diff($timeTemp1);
//                                $hours = $interval->format('%h');
//                                $minutes = $interval->format('%i');

                                $diff = round(abs($time1 - $time2), 2);
                                $years = floor($diff / (365 * 60 * 60 * 24));
                                $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
                                $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

                                $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
                                $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
                                $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
                                $totalhours = $totalhours + $hours;
                                $totalmin = $totalmin + $minutes;
                                $totalsec = $totalsec + $seconds;

                                printf(" %d:%d:%d ", $hours, $minutes, $seconds);



                                //                                echo $interval->format("%H:%I:%S");
//                                echo $dueration;
                            }
                            ?>
                            <!--                            min-->
                        </td>
                    </tr>
                    <?php
                }
                ?>
<!--            <tfoot>
                <tr style="text-align: center">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td><b>TOTAL DURATION</b></td>
                    <td><b> <?php printf(" %d:%d:%d ", $totalhours, $totalmin, $totalsec); ?></b></td>
                </tr>
            </tfoot>-->

            </tbody>
        </table>
        <br/>
        <div>
            <table class="display nowrap sortable" border="1" style="width: 100%">
                <thead style="text-align: center">
                    <tr>
                        <td>Sr. No</td>
                        <td>Del</td>
                        <td>NOTE</td>
                        <td>DATE</td>
                        <td>ADDED BY</td>
                    </tr>
                </thead>    
                <tbody >
                    <?php
                    $indexindexnote = 1;
                    $resultsetofnote = MysqlConnection::fetchCustom("SELECT * FROM `workorder_note` WHERE workorderid = '$workorderid' ");
                    foreach ($resultsetofnote as $notes) {
                        ?>
                        <tr>
                            <td style="width: 3%;text-align: center"><?php echo $indexindexnote++ ?></td>
                            <td style="width: 5%;text-align: center" >
                                <span data-toggle="tooltip" data-original-title="click here to delete note"> <a href="#" onclick="return deleteNote('<?php echo $notes["id"] ?>')">
                                        <i class="icon-remove"></i>
                                    </a>
                                </span>
                            </td>
                            <td style="width: 55%;color: red;" ><span class="blink"><b style="font-size: 20px"><?php echo $notes["note"] ?></b></span></td>
                            <td style="width: 10%;text-align: center"><?php echo MysqlConnection::convertToPreferenceDate($notes["date"]) ?></td>
                            <td style="width: 10%;text-align: center"><?php echo $notes["addedBy"] ?></td>
                        </tr>
                        <?php
                    }
                    ?>

                </tbody>
            </table>
            <br/>
            <textarea style="width: 100%;height: 60px;line-height: 18px;resize: none" id="note" name="note" placeholder="Enter note here..."></textarea>
        </div>
        <hr/>

        <div id="hiddenButton">
            <center>
                <div class="button-demo">
                    <input type="submit" id="layout" name="btnSaveNotes" class='btn btn-info' value="SAVE NOTE"/>
                    <a href="index.php?pagename=manage_workorder">
                        <button type="button"  class='btn btn-info' >BACK</button>
                    </a>
                </div> 
            </center>
        </div>
    </div>
    <br/><br/>
</form>
<script>
    function deleteNote(id) {
        $('#deletenote').modal('show');
        $("#deleteNoteId").val(id);
        return false;
    }
</script>
<div id="deletenote" class="modal hide" style="top: 10%;left: 50%;width: 420px;">
    <div class="modal-header"><h3>ALERT !!!</h3></div>
    <form name="frmDeleteNote" id="frmDeleteNote" method="post">
        <div class="modal-body">
            <h5 style="color: red">Do you really want to delete this note?<br/></h5>
            <input type="hidden" name="deleteNoteId" id="deleteNoteId">
        </div>
        <div class="modal-footer"> 
            <input type="submit" name="btnDeleteNote" id="btnDeleteNote" class="btn btn-info" value="YES">
            <a id="saveshipvia" data-dismiss="modal" data-dismiss="modal" class="btn btn-info">CLOSE</a> 
        </div>
    </form>
</div>

<?php

function isStepScan($packslipId, $isStepScan) {
    $query = "SELECT `iindex` FROM `workorder_phase_history` WHERE"
            . " packslipId = '$packslipId'"
            . " AND phase = '$isStepScan'";
    return count(MysqlConnection::fetchCustom($query));
}
?>