<?php
$packslipid = base64_decode(filter_input(INPUT_GET, "packslipid"));
$flag = filter_input(INPUT_GET, "flag");
$flagvalue = filter_input(INPUT_GET, "flagvalue");
?>




<?php if ($flag == "email") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Email has been sent Successfully!!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_workorder&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO WORK ORDER</a></td>

                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($flag == "yes") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Work order has been Deleted Successfully !!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_workorder&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO WORK ORDER</a></td>

                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($flag == "update") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Work order has been Updated Successfully !!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_workorder&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO WORK ORDER</a></td>

                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>


<?php if ($flag == "activeinactive") { ?>
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: auto;border-bottom: solid 1px #CDCDCD;text-align: center;">
            <div class="container-fluid" style="" >
                <div class="cutomheader">
                    <h5 style="font-family: verdana;font-size: 12px;text-align: center">ALERT !!!</h5>
                </div>
                <br/>
                <div class="alert alert-success" style="text-align: center;font-size: 15px;height: 50px;line-height: 50px;">
                    <strong>SUCCESS!</strong> 
                    Work order has been <?php echo $flagvalue ?> Successfully !!!
                </div>

            </div>
            <div class="modal-footer " > 
                <center>
                    <table border="0">
                        <tr>
                            <td><a href="index.php?pagename=manage_workorder&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO WORK ORDER</a></td>

                            <td><a href="index.php?pagename=manage_packingslip&status=all" id="btnSubmitFullForm" class="btn btn-info">BACK TO PACKING SLIP</a></td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
<?php } ?>