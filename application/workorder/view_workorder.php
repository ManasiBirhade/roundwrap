<?php
$flag = filter_input(INPUT_GET, "flag");
$packslipid = filter_input(INPUT_GET, "psid");
$packslip = MysqlConnection::getPackSlipFromId($packslipid);
$customerid = $packslip["cust_id"];
$profid = $packslip["prof_id"];
$customerdetails = MysqlConnection::getCustomerDetails($customerid);
$portfolioname = MysqlConnection::getPortfolioProfileByProfId($profid);
$profilelabel = MysqlConnection::getPortfolioLabelsByProfId($profid);
$packslipdetailsarray = MysqlConnection::getPackSlipDetailsFromId($packslipid);


$deleteItem = filter_input(INPUT_POST, "deleteItem");
if (isset($deleteItem) && $deleteItem && $deleteItem == "DELETE") {
    MysqlConnection::delete("DELETE FROM packslip WHERE ps_id = '$packslipid' ");
    header("location:index.php?pagename=success_workorder&psid=$packslipid&flag=yes");
}
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    input,textarea,select,date{ width: 90%;}
    .control-label{ margin-left: 10px; }
    tr,td{ vertical-align: middle; font-size: 12px;padding: 5px;margin: 5px;}
    .table{
        border: solid 0px;
    }
    .table tr{
        border: solid 0px;

    }
    .table td{
        border: solid 0px;
    }
</style>
<form  method="post" autocomplete="off">
    <input type="hidden" name="ps_id" id="ps_id" value="<?php echo filter_input(INPUT_GET, "packslipId") ?>">
    <div class="container-fluid" style="" >
        <div class="widget-box" style="width: 100%;border-bottom: solid 1px #CDCDCD;background-color: white">
            <div class="widget-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab1">WORK ORDER</a></li>
                </ul>
            </div>
            <?php
            if ($flag == "yes") {
                echo MysqlConnection::$DELETE;
            }
            ?>
            <table style="width: 100%">
                <tr>
                    <td>
                        <table class="display nowrap sortable" style="width: 100%">
                            <tr >
                                <td style="width: 10%"><label class="control-label"  >CUSTOMER DETAILS</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" value="<?php echo $customerdetails["cust_companyname"] ?>" readonly=""/></td>
                                <td style="font-weight: bold; " ><label class="control-label">SO&nbsp;NO</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" name="so_no" id="so_no" value="<?php echo $packslip["so_no"] ?>" readonly=""></td>
                                <td style="font-weight: bold; " ><label class="control-label">WO.&nbsp;NO</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" name="workOrd_Id" id="workOrd_Id" value="<?php echo $packslip["workOrd_Id"] ?>" readonly=""></td>
                            </tr>
                            <tr>
                                <td ><label class="control-label">PO&nbsp;NO</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" name="po_no" id="po_no" value="<?php echo $packslip["po_no"] ?>" readonly=""></td>
                                <td ><label class="control-label">RECEIVED&nbsp;DATE</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" name="rec_date" id="rec_date" value="<?php echo $packslip["rec_date"] ?>" readonly=""></td>
                                <td ><label class="control-label">REQUIRED&nbsp;DATE</label></td>
                                <td>&nbsp;:&nbsp;</td>
                                <td><input style="width: 220px" type="text" name="req_date" id="req_date" value="<?php echo $packslip["req_date"] ?>" readonly=""></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 20%;float: left">
                            <table class="display nowrap sortable" id="profiletable" style="border-collapse: collapse;background-color: white;width: 100%" border="1">
                                <?php
                                ?>
                                <tr style="height: 27px;  ">
                                    <td>Profile Name:</td>
                                    <td><?php echo $portfolioname["profile_name"] ?></td>
                                </tr>
                                <?php
                                $packlebels = explode(",", $packslip["packlebels"]);
                                $packvalues = explode(",", $packslip["packvalues"]);
                                for ($inx = 0; $inx < count($packlebels); $inx++) {
                                    $expvalleb = explode("--", $packvalues[$inx]);
                                    ?> 
                                    <tr style="height: 30px;">
                                        <td style="width: 50%;"><?php echo ucwords(str_replace("_", " ", $packlebels[$inx])) ?></td>
                                        <td><?php echo ucwords(trim($expvalleb[0])) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                        <div style="width: 78%;float: right">
                            <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(245,245,245);height: 30px;">
                                    <th  colspan="2">Size as Ordered</th>
                                    <th  colspan="2"></th>
                                    <th  colspan="2">Finished Size(mm)</th>
                                    <th  colspan="2">Cut Size(mm)</th>
                                    <th></th>
                                </tr>
                                <tr style="border-bottom: solid 1px  #CDCDCD;background-color: rgb(245,245,245);height: 30px;">
                                    <td style="width: 90px;">R-to-R</td>
                                    <td style="width: 90px;">Height</td>
                                    <td style="width: 50px;">Pcs</td>
                                    <td style="width: 200px;">Type</td>
                                    <td style="width: 90px;">R-to-R</td>
                                    <td style="width: 90px;">Height</td>
                                    <td style="width: 90px;">R-to-R</td>
                                    <td style="width: 90px;">Height</td>
                                    <td>SQ-FT</td>
                                </tr>
                            </table>
                            <div style="overflow: auto;height: 232px;border-bottom: solid 1px  #CDCDCD;">
                                <table class="display nowrap sortable" style="width: 100%;border-collapse: collapse" border="1">
                                    <?php
                                    foreach ($packslipdetailsarray as $key => $value) {
                                        ?>
                                        <tr id="<?php echo $index ?>" style="border-bottom: solid 1px  #CDCDCD;background-color: white">
                                            <td style="width: 90px"><?php echo $value["mm_w"] ?></td>
                                            <td style="width: 90px"><?php echo $value["mm_h"] ?></td>
                                            <td style="width: 50px;"><?php echo $value["pcs"] ?></td>
                                            <td style="width: 200px;"><?php echo $value["type"] ?></td>
                                            <td style="width: 90px;"><?php echo $value["mm_w"] ?></td>
                                            <td style="width: 90px;"><?php echo $value["mm_h"] ?></td>
                                            <td style="width: 90px;"><?php echo $value["mm_w"] - 36 ?></td>
                                            <td style="width: 90px;"><?php echo $value["mm_h"] - 1 ?></td>
                                            <td ><?php echo $value["sqFeet"] ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="modal-footer "> 
                <center>
                    <a href="index.php?pagename=manage_workorder" id="btnSubmitFullForm" class="btn btn-info">CANCEL</a>
                    <?php
                    if (isset($flag)) {
                        ?>
                        <input type="submit" value="DELETE" name="deleteItem" class="btn btn-info"/>
                        <?php
                    } else {
                        ?>
                        <a href="invoice/print_workorder.php?packslipid=<?php echo $packslipid ?>" target="_balnk" id="btnSubmitFullForm" class="btn btn-info">PRINT WORK ORDER</a>
                        <?php
                    }
                    ?>
                </center>
            </div>
        </div>
    </div>
</form>

<script>
    function createWorkOrder() {
        var x = document.getElementsByTagName("form");
        x[0].submit();
    }
</script>





