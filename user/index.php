<?php
include './MysqlConnection.php';
$packslip = MysqlConnection::fetchCustom("SELECT * FROM `packslip` WHERE workOrd_Id != ''  ORDER BY `workOrd_Id` DESC ");
?>
<style>
    *{
        padding: 0px;
        margin: 0px;
        font-family: verdana;
    }
    table{
        border-collapse: collapse;
        width: 100%;
        height: 100%;
    }
</style>
<script>
    $(document).ready(function () {
        if (window.history) {
            window.history.forward(1);
        }
        function disableBack() {
            window.history.forward();
        }

        window.onload = disableBack();
        window.onpageshow = function (evt) {
            if (evt.persisted)
                disableBack()
        }
    });
    $(document).ready(function () {
        $('#example').DataTable({
            "scrollY": 280,
            "scrollX": true
        });
    });


</script>
<table >
    <tr>
        <td style="height: 10%">
            <img src="http://35.183.37.135/application/assets/images/download.png" style="height: 20%;text-align: center;padding-left: 10px;">
        </td>
    </tr>
    <tr style="padding: 10px">
        <td style="height: 5%;background-color: rgb(240,240,240);border:solid 1px rgb(200,200,200);">
            <p style=";padding-left: 15px;">WORK ORDER PHASE TRANSACTION</p>
        </td>
        <td style="height: 5%;background-color: rgb(240,240,240);border:solid 1px rgb(200,200,200);"> 
            <a href="logout.php"><i class="icon  icon-off"></i><span>Logout</span> </a>
        </td>
    </tr>
    <tr>
        <td style="height: 5%">&nbsp;</td>
    </tr>
    <tr style="vertical-align: top">
        <td style="vertical-align: top">
            <table id="example"  style="width: 98%;;vertical-align: top;margin-left: 15px;border:solid 1px rgb(200,200,200);">
                <thead>
                    <tr style="font-weight: bold;color: white;background-color:#A9CDEC;height: 30px; padding: 15px;">
                        <td>#</td>
                        <td>WO NO</td>
                        <td>SEQ NO</td>
                        <td>SO NO</td>
                        <td>PO NO</td>
                        <td>REC.DATE</td>
                        <td>REQ.DATE</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $index = 1;
                    foreach ($packslip as $key => $value) {
                        ?>
                        <tr style="font-size: 12px;border:solid 1px rgb(200,200,200);">
                            <td style="text-align: center">&nbsp;<?php echo $index ?></td>
                            <td><a href="moveorder.php?psid=<?php echo $value["ps_id"] ?>"><?php echo $value["workOrd_Id"] ?></a></td>
                            <td><?php echo $value["seq_no"] ?></td>
                            <td><?php echo $value["so_no"] ?></td>
                            <td><?php echo $value["po_no"] ?></td>
                            <td><?php echo $value["rec_date"] ?></td>
                            <td><?php echo $value["req_date"] ?></td>
                        </tr>
                        <?php
                        $index++;
                    }
                    ?>
                </tbody>
            </table>
        </td>
    </tr>
</table>