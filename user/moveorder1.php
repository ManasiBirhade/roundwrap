<?php
include './MysqlConnection.php';
error_reporting(0);

$packslip = filter_input(INPUT_GET, "psid");

$psresultset = MysqlConnection::fetchCustom("SELECT * FROM `packslip` WHERE ps_id = '$packslip' ");
$psdetails = $psresultset[0];
$customerid = $psresultset[0]["cust_id"];
$profileid = $psresultset[0]["prof_id"];

$portfoliolabels = MysqlConnection::getPortfolioProfileById($profileid);
$portfolioname = $portfoliolabels["portfolio_name"];
$wsresultset = MysqlConnection::fetchCustom("SELECT * FROM `workorder_phase_history` WHERE packslipId = '$packslip' ");
$wsdetails = $wsresultset[0];

$style = $portfoliolabels["profile_name"];
$packlebels = explode(",", $psresultset[0]["packlebels"]);
$packvalues = explode(",", $psresultset[0]["packvalues"]);

$colorindex = "";
foreach ($packlebels as $keyp => $pvalue) {
    if ($pvalue == "Color") {
        $colorindex = $keyp;
        break;
    }
}

$activecolor = explode("--", $packvalues[$colorindex]);
$trackingsteps = MysqlConnection::fetchCustom("SELECT * FROM `tbl_profile_scanner_integration` WHERE profileid = '$portfolioname' ORDER BY sequence ");
$getsingle = $trackingsteps[0];
$trackingstepsformated = array();

array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "PACKSLIP ACK", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "WORK ORDER", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "PRODUCTION", "isavailable" => "Y"));
foreach ($trackingsteps as $tvalue) {
    array_push($trackingstepsformated, $tvalue);
}

array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "CLEAN/ PACK", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "ORDER READY", "isavailable" => "Y"));
array_push($trackingstepsformated, array("profileid" => $getsingle["profileid"], "stepname" => "DELIVERY", "isavailable" => "Y"));

$postedarray = filter_input_array(INPUT_POST);

if (isset($postedarray["btnPhase"]) || $postedarray["btnPhase"] == "IN" || $postedarray["btnPhase"] == "OUT") {

    $arrscanner = array();
    $arrscanner["cust_id"] = $customerid;
    $arrscanner["packslipId"] = $packslip;
    $arrscanner["workOrId"] = $psdetails["workOrd_Id"];
    $arrscanner["phase_date"] = date("Y-m-d");
    $arrscanner["phase_time"] = date("g:i:s");
    $arrscanner["phase"] = $postedarray["step"];
    $arrscanner["phaseno"] = $postedarray["stepno"];
    $arrscanner["status"] = $postedarray["btnPhase"];

    MysqlConnection::insert("workorder_phase_history", $arrscanner);
}
?>


<style>
    *{
        padding: 0px;
        margin: 0px;
        font-family: verdana;
        overflow-x: hidden;
    }
    table{
        border-collapse: collapse;
        width: 100%;
        height: 100%;
    }
    .btn-info {
        color: #fff;
        background-color: transparent; 
        border-color: transparent; 
    }
    .btn-info:hover {
        color: #fff;
        background-color: transparent; 
        border-color: transparent; 
    }

</style>


<form name="fromSubmit" id="fromSubmit" method="post">
    <table style="vertical-align: top">
        <tr>
            <td style="height: 10%">
                <img src="http://35.183.37.135/application/assets/images/download.png" style="height: 20%;text-align: center;padding: 10px;margin-bottom: 10px">
            </td>
        </tr>
        <tr>
            <td style="height: 5%;background-color: rgb(240,240,240);border:solid 1px rgb(200,200,200);">
                <p style="padding: 10px;">WORK ORDER PHASE TRANSACTION</p>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top">
                <div style="width: 95%;overflow: auto;padding: 10px;vertical-align: top">
                    <br/>
                    <a href="index.php">BACK</a>
                    <br/>
                    <br/>
                    <table style="text-align: center;width: 99%;border-collapse: collapse;border: 1px solid #ddd;" border="1">
                        <tr >
                            <?php
                            foreach ($trackingstepsformated as $value) {
                                $border = 1;

                                $isstepscan = isStepScan($packslip, ucwords($value["stepname"]));
                                if ($isstepscan == 2) {
                                    $bg = "background-color: #7CFC00";
                                } else if ($isstepscan == 1) {
                                    $bg = "background-color: #FFFF00";
                                    $border = 5;
                                } else {
                                    $bg = "background-color: #C90602";
                                }

                                if ($value["stepname"] == "PACKSLIP ACK" || $value["stepname"] == "WORK ORDER") {
                                    $bg = "background-color: #7CFC00";
                                }
                                ?>
                                <td style="width: 100px;">
                                    <div style="width: 50px;height: 50px;border: solid <?php echo $border ?>px gray;border-radius: 50px;margin: 0 auto;<?php echo $bg ?>">
                                    </div>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr style="height: 35px">
                            <?php foreach ($trackingstepsformated as $value) { ?>
                                <td  style="width: 100px;font-size: 13px"><?php echo strtoupper($value["stepname"]) ?>
                                </td>
                            <?php } ?>
                        </tr>
                        <tr style="height: 35px">
                            <?php
                            $indexv = 0;
                            $phase = 0;
                            foreach ($trackingstepsformated as $value1) {
                                $lastphasecount = getLastPhaseCount($packslip, $stepname);
                                $isInOrOut = getLastPhaseForStep($packslip, $value1["stepname"]);
                                if (count($isInOrOut) == 2) {
                                    $phase = 1 + $lastphasecount;
                                } else if (count($isInOrOut) == 1) {
                                    $inorout = "OUT";
                                    $phase = $lastphasecount;
                                } else {
                                    $inorout = "IN";
                                    // $phase = $lastphasecount;
                                }
                                echo '<td  style="width: 100px;font-size: 13px">';
                                if ($value1["stepname"] != "PACKSLIP ACK" && $value1["stepname"] != "WORK ORDER") {
                                    if ($phase == $indexv) {
                                        echo '<input type="submit" name="btnPhase" value="' . $inorout . '" style="border:  solid 1px;padding: 5px;cursor: pointer">';
                                        echo '<input  type="hidden" name="step" value="' . $value1["stepname"] . '">';
                                        echo '<input  type="hidden" name="stepno" value="' . $phase . '">';
                                    }
                                    $indexv++;
                                }
                                echo '</td>';
                            }
                            ?>

                        </tr>
                    </table>
                    <br/>
                    <table style="border: 1px solid #ddd;" border="1" style="width: 100%">
                        <thead style="text-align: center">
                            <tr style="font-weight: bold;color: white;background-color:#A9CDEC;height: 35px; font-size: 12px;">
                                <td style="width: 5%">Sr. No</td>
                                <td>Step</td>
                                <td>In</td>
                                <td>Out</td>
                                <td>Duration</td>
                            </tr>
                        </thead>    
                        <tbody style="text-align: center">
                            <?php
                            $indexindex = 1;
                            $tracknigsqlsteps = "SELECT DISTINCT(phase) as phase FROM `workorder_phase_history`"
                                    . " WHERE packslipId = '$packslip' AND phase != '-' "
                                    . " ORDER BY `iindex`";
                            $trackingdetailsprocesssteps = MysqlConnection::fetchCustom($tracknigsqlsteps);

                            $totalduration = 0;
                            foreach ($trackingdetailsprocesssteps as $valuesteps) {
                                $stepv = $valuesteps["phase"];
                                $getsteps = "SELECT * FROM `workorder_phase_history`"
                                        . " WHERE `packslipId` = '$packslip'"
                                        . " AND phase = '$stepv' ORDER BY iindex ASC";
                                $resultsetsteps = MysqlConnection::fetchCustom($getsteps);
                                $set1 = $resultsetsteps[0];
                                $set2 = $resultsetsteps[1];
                                ?>
                                <tr style="height: 30px">
                                    <td style="text-transform: uppercase"><?php echo $indexindex++ ?></td>
                                    <td ><?php echo $set1["phase"] ?></td>
                                    <td ><?php echo $set1["phase_date"] . " " . $set1["phase_time"] ?></td>
                                    <td ><?php echo $set2["phase_date"] . " " . $set2["phase_time"] ?></td>
                                    <td>
                                        <?php
                                        if ($set2["phase_time"] != "") {
                                            $time1 = strtotime($set2["phase_time"]);  // 2012-12-06 23:56
                                            $time2 = strtotime($set1["phase_time"]);  // 2012-12-06 00:21
                                            $dueration = round(abs($time1 - $time2) / 60, 2);
                                            $totalduration = $totalduration + $dueration;

                                            echo $dueration;
                                        }
                                        ?>

                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr style="height: 30px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>Total Duration</td>
                                <td><?php echo $totalduration ?> sec</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </table>

</form>

<?php

function isStepScan($packslipId, $isStepScan) {
    $query = "SELECT `iindex` FROM `workorder_phase_history` WHERE"
            . " packslipId = '$packslipId'"
            . " AND phase = '$isStepScan'";
    return count(MysqlConnection::fetchCustom($query));
}

function getLastPhaseForStep($packslip, $stepname) {
    $sql = "SELECT `packslipId`,phaseno FROM `workorder_phase_history` WHERE"
            . " packslipId = '$packslip'"
            . " AND `phase` = '$stepname'";
    return MysqlConnection::fetchCustom($sql);
}

function getLastPhaseCount($packslip) {
    $sql = "SELECT phaseno FROM `workorder_phase_history` WHERE"
            . " packslipId = '$packslip'"
            . " ORDER BY iindex DESC LIMIT 0,1";
    $resultset = MysqlConnection::fetchCustom($sql);
    return $resultset[0]["phaseno"];
}
